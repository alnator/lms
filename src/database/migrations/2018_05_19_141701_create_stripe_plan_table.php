<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStripePlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stripe_plan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subscription_model_id')->nullable()->default(0);
            $table->integer('course_id')->nullable()->default(0);
            $table->string('stripe_plan_id')->nullable()->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stripe_plan');
    }
}
