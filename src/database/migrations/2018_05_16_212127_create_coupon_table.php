<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable()->default('');
            $table->string('payment_method_ids')->nullable()->default('');
            $table->string('subscription_model_ids')->nullable()->default('');
            $table->integer('discount_type_id')->nullable()->default(1);
            $table->float('amount',12,3)->nullable()->default(0);
            $table->integer('user_id')->nullable()->default(0);
            $table->string('image_path_ar')->nullable()->default('');
            $table->string('image_path_en')->nullable()->default('');
            $table->boolean('popup')->nullable()->default(false);
            $table->integer('qty')->nullable()->default(1);
            $table->timestamp('start_at')->nullable();
            $table->timestamp('end_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon');
    }
}
