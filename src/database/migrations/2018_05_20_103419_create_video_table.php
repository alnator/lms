<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ar')->nullable()->default('');
            $table->string('description_ar')->nullable()->default('');
            $table->string('name_en')->nullable()->default('');
            $table->string('description_en')->nullable()->default('');
            $table->string('image_path')->nullable()->default('');
            $table->integer('course_id')->nullable()->default(0);
            $table->integer('user_id')->nullable()->default(0);
            $table->integer('estimated_time')->nullable()->default(0);
            $table->string('seo_meta')->nullable()->default('');
            $table->integer('kit_id')->nullable()->default(0);
            $table->integer('order')->nullable()->default(0);
            $table->string('path')->nullable()->default('');
            $table->string('url_identifier')->nullable()->default('');
            $table->boolean('active')->default(false);
            $table->boolean('is_youtube')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video');
    }
}
