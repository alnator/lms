<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->default(0);
            $table->integer('subscription_model_id')->nullable()->default(0);
            $table->integer('payment_id')->nullable()->default(0);
            $table->integer('payment_method_id')->nullable()->default(0);
            $table->integer('course_id')->nullable()->default(0);
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->string('status')->nullable()->default('');
            $table->string('stripe_subscription_id')->nullable()->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription');
    }
}
