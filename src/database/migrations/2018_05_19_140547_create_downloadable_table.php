<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDownloadableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('downloadable', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ar')->nullable()->default('');
            $table->string('name_en')->nullable()->default('');
            $table->string('file_path_ar')->nullable()->default('');
            $table->string('file_path_en')->nullable()->default('');
            $table->integer('video_id')->nullable()->default(0);
            $table->integer('course_id')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('downloadable');
    }
}
