<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('level_id')->nullable()->default(0);
            $table->integer('lang_id')->nullable()->default(0);
            $table->string('title_ar')->nullable()->default('');
            $table->string('overview_ar')->nullable()->default('');
            $table->string('title_en')->nullable()->default('');
            $table->string('overview_en')->nullable()->default('');
            $table->float('price',12,3)->nullable()->default(0);
            $table->integer('user_id')->nullable()->default(0);
            $table->string('seo_meta_title_ar')->nullable()->default('');
            $table->string('seo_meta_title_en')->nullable()->default('');
            $table->string('description_ar')->nullable()->default('');
            $table->string('description_en')->nullable()->default('');
            $table->string('url_identifier')->nullable()->default('');
            $table->string('image_path')->nullable()->default('');
            $table->string('kit_id')->nullable()->default('');
            $table->float('course_estimated_time',12,3)->nullable()->default(0);
            $table->string('intro_video_path')->nullable()->default('');
            $table->boolean('active')->nullable()->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course');
    }
}
