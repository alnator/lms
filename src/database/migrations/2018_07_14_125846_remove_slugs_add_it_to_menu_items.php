<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSlugsAddItToMenuItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('menu_items');
        Schema::table('slugs', function(Blueprint $table){
            $table->string ('title_en');
            $table->string ('title_ar');
            $table->string('permission')->nullable();
            $table->string('href')->nullable();
            $table->integer('parent')->nullable();
            $table->string('slug');
            $table->dropColumn('menu_item_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
