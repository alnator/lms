<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcriptionModelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcription_model', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ar')->nullable()->default('');
            $table->string('name_en')->nullable()->default('');
            $table->string('display_price')->nullable()->default('');
            $table->float('price',12,3)->nullable()->default(0);
            $table->integer('period_in_days')->nullable()->default(0);
            $table->boolean('full_access')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcription_model');
    }
}
