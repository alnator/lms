<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatdByToMainTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course', function (Blueprint $table){
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
        Schema::table('session', function (Blueprint $table){
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
        Schema::table('video', function (Blueprint $table){
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
        Schema::table('level', function (Blueprint $table){
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
