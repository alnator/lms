<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/cms/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
     return "done";
});

Route::get('/','ClientController@index');
Route::get('/subscription','ClientController@subscription');
Route::get('/index','ClientController@index');

Auth::routes();
Route::get('/test/{id}' , 'ShipmentController@createShipment');

// Route::get('/home', 'HomeController@index')->name('home');

							// Admin
Route::get('/admin','AdminController@index')->middleware('auth')->name('admin-panel');

	//Categories
Route::get('/admin/categories' , 'AdminController@getCategoriesPage')->name('categories-admin');
Route::get('/admin/categories/create' , 'AdminController@getCategoriesCreate')->name('categories-create');
Route::get('/admin/categories/{id}/edit' , 'AdminController@getCategoryEdit')->name('categories-edit');

	//Courses
Route::post('/admin/courses/create','AdminController@setCourseEditCreate')->name('course-create-api');
Route::post('/admin/courses/{id}/edit','AdminController@setCourseEditCreate')->name('courses-edit-api');
Route::get('/admin/courses', 'AdminController@getCoursesPage')->name('courses-admin');
// Route::get('/admin/courses/topdf' , 'AdminController@getCoursesPDF')->name('courses-pdf');

Route::get('/admin/courses/create' , 'AdminController@getCourseCreate')->name('course-create');
Route::get('/admin/courses/{id}/edit','AdminController@getCourseEdit');
Route::get('/admin/courses/{id}/users','AdminController@getCourseUsersPage')->name('course-users-admin');    
Route::get('/admin/courses/{id}/adduser' , 'AdminController@getAddCourseUserPage')->name('add-user-admin');
Route::get('/admin/courses/{id}' ,'AdminController@getCourseDetailsPage')->name('course-details-admin');

	//Levels

Route::get('/admin/levels' , 'AdminController@getLevelsPage')->name('levels-admin');
Route::get('/admin/levels/create' , 'AdminController@getLevelCreate')->name('level-create');
Route::get('admin/levels/{id}/edit' , 'AdminController@getLevelEdit')->name('level-edit');
Route::post('admin/levels/{id}/edit' , 'AdminController@setLevelCreateUpdate')->name('level-edit-api');
Route::post('/admin/levels/create' , 'AdminController@setLevelCreateUpdate')->name('level-create-api');

	//Sessions

Route::get('/admin/sessions' , 'AdminController@getSessionPage')->name('sessions-admin');
Route::get('/admin/sessions/{id}/edit' , 'AdminController@getSessionEdit')->name('session-edit');
Route::get('/admin/sessions/create' , 'AdminController@getSessionCreate')->name('session-create');
Route::post('/admin/sessions/{id}/edit' , 'AdminController@setSessionEdit')->name('sessions-edit-api');
Route::post('/admin/sessions/create' , 'AdminController@setSessionCreate')->name('sessions-create-api');

	// Videos 

Route::get('/admin/videos' , 'AdminController@getVideosPage')->name('videos-admin');
Route::get('/admin/videos-error' , 'AdminController@VideosPageHasError')->name('videos-error-admin');
Route::get('/admin/videos/create' , 'AdminController@getVideoCreate')->name('video-create');
Route::get('/admin/videos/{id}/edit' , 'AdminController@getVideoEdit')->name('video-edit');
Route::post('/admin/videos/{id}/edit' , 'AdminController@setVideoCreateEdit')->name('video-edit-api');
Route::post('/admin/videos/create' , 'AdminController@setVideoCreateEdit')->name('video-create-api');
Route::get('/admin/video/{id}/uploadvideo' , 'AdminController@getUploadVideoPage')->name('upload-video');
Route::post('/admin/videos/{id}/videoupdate' , 'AdminController@setVideoUpload')->name('video-upload-api');	
	// Videos Quizes

Route::get('/admin/{type}/{id}/quizes' , 'AdminController@getQuizesPage')->name('quizes-admin');
Route::get('/admin/{type}/{id}/quiz/create' ,'AdminController@getQuizCreate')->name('quiz-create');
Route::get('/admin/{type}/{videoId}/quiz/{quizId}/edit' , 'AdminController@getQuizEdit')->name('quiz-edit');



	//Reports
Route::get('/admin/quizes/reports' , 'AdminController@getQuizReportPage')->name('quiz-reports-admin');
	//Users 

Route::get('/admin/users' , 'AdminController@getUsersPage')->name('users-admin');
Route::get('/admin/users/{id}/courses' ,'AdminController@getUserCoursesPage')->name('user_courses');
Route::get('/admin/users/{userId}/courses/{courseId}' ,'AdminController@getUserCourseProgressPage')->name('user-course-progress');
Route::get('/admin/user/create' , 'AdminController@getUserCreate')->name('user-create');
Route::get('/admin/user/{id}/edit' , 'AdminController@getUserEdit')->name('user-edit');
Route::post('/admin/users' , 'AdminController@getUsersData');
Route::post('/admin/users/{id}/courses' ,'AdminController@getUserCoursesData');
Route::post('/admin/users/delete/{id}', 'AdminController@destroyUser');
Route::post('/admin/users/{userId}/courses/{courseId}' ,'AdminController@getUserCourseProgressData');
Route::post('/admin/users/star/{id}' , 'AdminController@starUser');
Route::post('/admin/user/create' , 'AdminController@setUserCreate')->name('user-create-api');
Route::post('/admin/user/{id}/edit' ,'AdminController@setUserEdit')->name('user-edit-api');

	//Downloadables


Route::get('/admin/files' , 'AdminController@getFilesPage')->name('files-admin');
Route::get('/admin/files/create' , 'AdminController@getFilesCreate')->name('files-create');
Route::get('/admin/files/{id}/edit' , 'AdminController@getFilesEdit')->name('files-edit');


	//Payments

Route::get('/admin/payments' , 'AdminController@getPaymentsPage')->name('payments-admin');
Route::get('/admin/payments/create' , 'AdminController@getPaymentsCreate')->name('payments-create');
Route::get('/admin/payments/{id}/edit', 'AdminController@getPaymentsEdit')->name('payments-edit');
Route::get('/admin/payments/pdf' , 'AdminController@getPaymentDataPDF')->name('payments-pdf');
Route::get('/admin/payments/excel/{type}' , 'AdminController@getPaymentDataExcel')->name('payments-excel');
Route::post('/admin/payments' , 'AdminController@getPaymentsData');
Route::post('/admin/payments/delete/{id}' , 'AdminController@deletePayment');
Route::post('/admin/payments/create', 'AdminController@setPaymentCreate')->name('payment-create-api');
Route::post('/admin/payments/{id}/edit', 'AdminController@setPaymentEdit')->name('payment-edit-api');

	//Teacher Fees

Route::get('/admin/teacher-fees' , 'AdminController@getTeacherFeesPage')->name('teacher-admin');
Route::get('admin/teacher/create' , 'AdminController@getTeacherCreate')->name('teacher-create');
Route::get('admin/teacher/{id}/edit' , 'AdminController@getTeacherEdit')->name('teacher-edit');
Route::get('/admin/teacher/billing' , 'AdminController@getTeacherBillingPage')->name('teacher-billing');
Route::post('/admin/teacher-fees' , 'AdminController@getTeacherFeesData');
Route::post('/admin/teacher/delete/{id}' , 'AdminController@destroyTeacher');
Route::post('admin/teacher/create' , 'AdminController@setTeacherCreate')->name('teacher-create-api');
Route::post('admin/teacher/{id}/edit' , 'AdminController@setTeacherEdit')->name('teacher-edit-api');
Route::post('/admin/teacher/billing' , 'AdminController@getTeacherBillingData')->name('teacher-billing-api');

	//Coupons

Route::get('/admin/coupon' , 'AdminController@getCouponPage')->name('coupon-admin');
Route::get('/admin/coupon/create' , 'AdminController@getCouponCreate')->name('coupon-create');
Route::get('/admin/coupon/{id}/edit' , 'AdminController@getCouponEdit')->name('coupon-edit');
Route::post('/admin/coupon' , 'AdminController@getcouponData');
Route::post('/admin/coupon/delete/{id}' ,'AdminController@destroyCoupon');
Route::post('/admin/coupon/{id}/edit' , 'AdminController@setCouponEdit')->name('coupon-edit-api');
Route::post('/admin/coupon/create' ,'AdminController@setCouponCreate')->name('coupon-create-api');

	//Subscription Models
	
Route::get('/admin/subscription' , 'AdminController@getSubscriptionPage')->name('subscription-admin');
Route::get('/admin/subscription/create' , 'AdminController@getSubscriptionCreate')->name('subscription-create');
Route::get('/admin/subscription/{id}/edit' , 'AdminController@getSubscriptionEdit')->name('subscription-edit');
Route::post('/admin/subscription' , 'AdminController@getSubscriptionData');
Route::post('/admin/subscription/delete/{id}', 'AdminController@destroySubscription');
Route::post('/admin/subscription/create' , 'AdminController@setSubscriptionCreate')->name('subscription-create-api');
Route::post('/admin/subscription/{id}/edit' , 'AdminController@setSubscriptionEdit')->name('subscription-edit-api');

	// Stripe Customers

Route::get('/admin/stripe' , 'AdminController@getStripePage')->name('stripe-admin');
Route::get('/admin/stripe/create' , 'AdminController@getStripeCreate')->name('stripe-create');
Route::get('/admin/stripe/{id}/edit' , 'AdminController@getStripeEdit')->name('stripe-edit');

	//	Strip Plans

Route::get('/admin/stripe-plan' , 'AdminController@getStripePlanPage')->name('stripe-plan-admin');
Route::get('/admin/stripe-plan/create' , 'AdminController@getStripePlanCreate')->name('stripe-plan-create');
Route::get('/admin/stripe-plan/{id}/edit' , 'AdminController@getStripePlanEdit')->name('stripe-plan-edit');


	//	Payment Methods

Route::get('/admin/payment-methods' , 'AdminController@getPaymentMethodPage')->name('payment-methods-admin');
Route::get('/admin/payment-methods/create' , 'AdminController@getPaymentMethodCreate')->name('payment-methods-create');
Route::get('/admin/payment-method/{id}/edit' , 'AdminController@getPaymentMethodEdit')->name('payment-methods-edit');

	// Abandoned Carts

Route::get('/admin/abandoned-carts' , 'AdminController@getAbandondeCartPage')->name('abandoned-carts-admin');
Route::get('/admin/abandoned-carts/create' , 'AdminController@getAbandondeCartCreate')->name('abandoned-carts-create');
Route::get('/admin/abandoned-carts/{id}/edit' , 'AdminController@getAbandondeCartEdit')->name('abandoned-carts-edit');

	//Emails 


Route::get('/admin/emails/contact' ,'AdminController@getEmailsPage')->name('emails-admin');


	// Slugs
Route::get('/admin/slugs' , 'AdminController@getSlugsIndex')->name('slugs-admin');
Route::get('/admin/slugs/create' , 'AdminController@getSlugsCreate')->name('slugs-create');
Route::get('/admin/slugs/{id}/edit' , 'AdminController@getSlugsEdit')->name('slugs-edit');
Route::get('/admin/stars/text' , 'AdminController@getStarsTextEdit')->name('stars-text-admin'); 
Route::get('/admin/footer/content' , 'AdminController@getFooterContentpage')->name('footer-admin');
Route::post('/admin/slugs' ,'AdminController@getSlugsData');
Route::post('/admin/slugs/create' , 'AdminController@setSlugsCreate')->name('slugs-create-api');
Route::post('/admin/slugs/{id}/edit' , 'AdminController@setSlugsEdit')->name('slugs-edit-api');
Route::post('/admin/slugs/delete/{id}', 'AdminController@destroySlug');
Route::post('/admin/about/update' , 'AdminController@updateAbout');
Route::post('/admin/stars/text', 'AdminController@setStarsTextEdit')->name('stars-text-api');
Route::post('/admin/footer/content/update' ,'AdminController@setFooterContent')->name('footer-admin-api');

	//Groups

Route::get('/admin/groups' , 'AdminController@getGroupPage')->name('groups-admin');
Route::get('/admin/groups/craete', 'AdminController@getGroupCreatePage')->name('groups-create');
Route::get('/admin/groups/{id}/edit', 'AdminController@getGroupEditPage')->name('group-edit');
Route::get('/admin/groups/{id}/details', 'AdminController@getGropuDetailsPage');
Route::get('/admin/groups/{id}/add-members', 'AdminController@getGroupAddMembersPage')->name('add-group-members');
Route::post('/admin/groups/create' ,'AdminController@setGroupCreate')->name('groups-create-api');
Route::post('/admin/groups/{id}/edit', 'AdminController@setGroupEdit')->name('groups-edit-api');
Route::post('/admin/groups' , 'AdminController@getGroupsData');
Route::post('/admin/groups/delete/{id}' , 'AdminController@destroyGroup');
Route::post('/admin/groups/{id}/add-members' , 'AdminController@addGroupMembers')->name('add-group-members-api');
Route::post('/admin/groups/{id}/details' , 'AdminController@getGroupDetailsData');
Route::post('/admin/groups/{Gid}/member/{Uid}/remove' , 'AdminController@removeMemberFromGroup');
Route::post('/admin/groups/{id}/menual-add-members' , 'AdminController@addMenualGroupMembers')->name('menual-add-group-members-api');


	//Laboratory

Route::get('/admin/kits' , 'AdminController@getKitsPage')->name('kits-admin');
Route::get('/admin/kits/{id}/items' , 'AdminController@getKitItemsPage')->name('kit-items');
Route::get('/admin/kits/create' , 'AdminController@getCreateKitPage')->name('kits-create');
Route::get('/admin/kits/{id}/edit' , 'AdminController@getEditKitPage')->name('kits-edit');
Route::get('/admin/kits/{id}/add' , 'AdminController@getAddItemPage')->name('kits-add-items');
Route::get('/admin/items' , 'AdminController@getItemsPage')->name('items-admin');
Route::get('/admin/items/create' , 'AdminController@getCreateItemPage')->name('items-create');
Route::post('/admin/items/create' , 'AdminController@setCreateItem')->name('items-create-api');
Route::get('/admin/items/{id}/edit' , 'AdminController@getEditItemPage')->name('items-edit');
Route::post('/admin/items/{id}/edit' , 'AdminController@setEditItem')->name('items-edit-api');
Route::get('/admin/suppliers' , 'AdminController@getSuppliersPage')->name('suppliers-admin');
Route::get('/admin/suppliers/{id}/items' , 'AdminController@getSupplierItemsPage')->name('admin-supplier-items');
Route::get('/admin/orders' , 'AdminController@getOrdersPage')->name('orders-admin');
Route::get('/admin/order/{id}/items' , 'AdminController@getOrderItemsPage');
Route::get('/admin/all-orders' , 'AdminController@getAllOrdersPage')->name('all-orders-admin');
Route::get('/admin/all-orders/pdf' , 'AdminController@getAllOrdersPDF')->name('all-orders-pdf');
Route::get('/admin/all-orders/excel' , 'AdminController@getAllOrdersExcel')->name('all-orders-excel');
Route::post('/admin/order/{id}/confirm' , 'AdminController@ConfirmOrder')->name('confirm-order');
Route::get('/admin/order/{id}/details' , 'AdminController@getOrderDetails');
Route::post('/admin/order/{id}/details' , 'AdminController@getOrderDetailsData');
Route::post('/admin/kits' , 'AdminController@getKitData');
Route::post('/admin/kits/{id}/delete' , 'AdminController@destroyKit');
Route::post('/admin/kits/{id}/items' , 'AdminController@getKitItemsData');
Route::post('/admin/kits/{kid}/item/{iid}/remove' , 'AdminController@removeItemFormKit');
Route::post('/admin/kits/create' ,'AdminController@setCreateKit')->name('kits-create-api');
Route::post('/admin/kits/{id}/edit', 'AdminController@setKitEdit')->name('kits-edit-api');
Route::post('/admin/kits/{id}/add' , 'AdminController@setAddKitItems')->name('kits-add-api');
Route::post('/admin/suppliers' , 'AdminController@getSuppliersData');
Route::post('/admin/suppliers/{id}/items' , 'AdminController@getSupplierItemData');
Route::post('/admin/suppliers/add' , 'AdminController@addSupplier')->name('add-supplier-api');
Route::post('/admin/items', 'AdminController@getItemsData');
Route::post('/admin/items/{id}/delete' , 'AdminController@destroyItem');
Route::post('/admin/orders' , 'AdminController@getOrdersData');
Route::post('/admin/order/{id}/items' , 'AdminController@getOrderItemsData');
Route::post('/admin/all-orders' , 'AdminController@getAllOrdersData')->name('all-orders-admin');


	//Suplier Side

Route::get('/supplier' , 'SupplierController@getSupplierIndex');
Route::get('/supplier/kits' , 'SupplierController@getKitsPage')->name('kits-supplier');
Route::get('/supplier/kits/{id}/items' , 'SupplierController@getKitItemsPage')->name('kits-items-supplier');
Route::get('/supplier/items' , 'SupplierController@getItemsPage')->name('items-supplier');
Route::get('/supplier/items-supplied' ,'SupplierController@getSupplyingItemsPage' )->name('items-supplying');
Route::get('/supplier/items-supplied/add' , 'SupplierController@addSupplyingItemPage')->name('add-item-supplier');
Route::get('/supplier/items-supplied/{id}/update' ,'SupplierController@updateSupplyingItemPage' )->name('update-item-supplier');
Route::get('/supplier/waiting-items' , 'SupplierController@getWaitingPage')->name('waiting-items-supplier');
Route::post('/supplier/waiting-items/' , 'SupplierController@getWaitingData');
Route::post('/supplier/waiting-items/{id}/confirm' , 'SupplierController@packItem');
Route::post('/supplier/items-supplied/add' , 'SupplierController@addSupplyingItem')->name('add-item-api');
Route::post('/supplier/items-supplied/{id}/update' , 'SupplierController@updateSupplyingItem')->name('update-item-api');
Route::post('/supplier/kits' , 'SupplierController@getKitsData');
Route::post('/supplier/kits/{id}/items' ,'SupplierController@getKitItemsData');
Route::post('/supplier/items' , 'SupplierController@getItemsData');
Route::post('/supplier/items-supplied' , 'SupplierController@getSupplyingItemsData');
Route::get('/supplier/shipment-info' , 'SupplierController@getShipmentInfoPage')->name('shipment-info');
Route::post('/supplier/shipment-info/update' , 'SupplierController@updateShipmentInfo')->name('update-shipment-info');
Route::get('/supplier/shipping-list' , 'SupplierController@getShippingListPage')->name('supplier-shipping-list');
Route::post('/supplier/shipping-list' , 'SupplierController@getShippingListData');
Route::post('/supplier/shipping-list/{id}/ship' , 'SupplierController@shipOrder');
Route::get('/supplier/shipping-list/{id}/shipdetails' , 'SupplierController@shipOrderdetails');
//Order 
Route::get('/get-kit-items/{id}', 'ClientController@getKitItems');
Route::post('/confirm-kit/{id}' , 'ClientController@confirmKit');
Route::post('/confirm-custom-kit', 'ClientController@confirmCustomKit');
Route::post('/items-filter/{string}' , 'ClientController@FilterItems');
	//Reports
Route::get('/admin/subscription/report', 'AdminController@getSubscriptionReportPage')->name('subscription-report');
Route::get('/admin/subscription/report/pdf' , 'AdminController@getSubscriptionReportPDF')->name('subscription-to-pdf');
Route::get('/admin/subscription/report/excel' , 'AdminController@getSubscriptionReportExcel')->name('subscription-to-excel');
Route::get('/admin/income/report/pdf','AdminController@getIncomeDataPDF')->name('income-to-pdf');
Route::get('/admin/income/report/excel','AdminController@getIncomeDataExcel')->name('income-to-excel');


	//Email Subsccription

Route::get('/admin/emails' , 'AdminController@getEmailSubscriptionsPage')->name('admin-email-subscription');
Route::post('/admin/emails' , 'AdminController@getEmailSubscriptionsData');
Route::post('/admin/emails/create' , 'AdminController@createEmailSubscription')->name('admin-email-create');	
Route::post('/admin/emails/update' , 'AdminController@updateEmailSubscription')->name('admin-email-update');
Route::post('/admin/email/{id}/delete' , 'AdminController@deleteEmailSubscription');	
Route::get('/admin/emails/{id}/send' , 'AdminController@getSendEmailSubscriptionPage');
Route::post('/admin/email/send' , 'AdminController@sendEmailSubscription')->name('admin-send-subs');

	
	// Gallery 

Route::get('/admin/gallery' , 'AdminController@getGalleryPage')->name('admin-gallery');
Route::post('/admin/gallery' , 'AdminController@getGalleryData');
Route::get('/admin/gallery/create' , 'AdminController@getGalleryCreatePage')->name('admin-create-gallery');
Route::post('/admin/gallery/store' , 'AdminController@createGallery')->name('admin-store-gallery');
Route::get('/admin/gallery/{id}/edit', 'AdminController@getGalleryEditPage')->name('admin-edit-gallery');
Route::post('/admin/gallery/{id}/edit' , 'AdminController@updateGallery')->name('admin-update-gallery');
Route::post('/admin/gallery/{id}/delete' , 'AdminController@deleteGallery');



	// Albums

Route::get('/admin/albums' , 'AdminController@getAlbumsPage')->name('admin-albums');
Route::post('/admin/albums' , 'AdminController@getAlbumsData');
Route::get('/admin/albums/create' , 'AdminController@getAlbumsCreatePage')->name('admin-create-album');
Route::post('/admin/albums/store' , 'AdminController@createAlbums')->name('admin-store-album');
Route::get('/admin/albums/{id}/edit', 'AdminController@getAlbumsEditPage')->name('admin-edit-album');
Route::post('/admin/albums/{id}/edit' , 'AdminController@updateAlbums')->name('admin-update-album');
Route::post('/admin/albums/{id}/delete' , 'AdminController@deleteAlbums');



	// Achievements
Route::get('/admin/achievements' , 'AdminController@getAchievementsPage')->name('admin-achievements');
Route::post('/admin/achievements' , 'AdminController@getAchievementsData');
Route::get('/admin/achievements/create' , 'AdminController@getAchievementsCreatePage')->name('admin-create-achievement');
Route::post('/admin/achievements/store' , 'AdminController@createAchievements')->name('admin-store-achievement');
Route::get('/admin/achievements/{id}/edit', 'AdminController@getAchievementsEditPage')->name('admin-edit-achievement');
Route::post('/admin/achievements/{id}/edit' , 'AdminController@updateAchievements')->name('admin-update-achievement');
Route::post('/admin/achievements/{id}/delete' , 'AdminController@deleteAchievements');


		//Paypal  Integration

 Route::post('/paypal-subscribe', 'PaypalController@index')->name('paypal');
 Route::post('/subscribe-course', 'PaypalController@index')->name('paypal-course');
 Route::post('/paypal','PaypalController@ipn');
 Route::post('/paypal-test', 'PaypalController@test')->name('paypal-test');
// 		//PayFort Integration

 Route::post('/payfort-subscribe' , 'PayfortController@index')->name('payfort');	
 Route::post('/payfort-redirect' , 'PayfortController@processReturn');	
			
		// Contact Us Email
Route::get('/contact-us' , 'ClientController@getcontactUs');
Route::post('/contact-us-front' , 'ClientController@frontcontactUs');
Route::post('/contactUs' , 'ClientController@setcontactUs')->name('contactUs');
		//Client

Route::get('/home' , 'ClientController@index')->name('home');
Route::get('/courses/{name}' , 'ClientController@getCoursePage')->name('course-page');
Route::get('/profile' , 'ProfileController@getProfile')->name('profile');
Route::get('/change-pass' , 'ProfileController@changepass')->name('change-pass');
Route::post('/pass-change-front', 'ProfileController@setchangepass')->name('pass-change-front');

Route::get('/free/{id}' ,'LessonsController@getFreeLesson')->name('free-lesson');
Route::get('/lesson/{id}' , 'LessonsController@getLessonPage')->name('lesson');
Route::get('/session/{id}' , 'LessonsController@getSessionPage')->name('session');
Route::get('/running/{id}' ,'ProfileController@getRunningCourse')->name('running-courses');
Route::get('/free-lesson/try' ,'LessonsController@tryFreeLesson')->name('try-lesson'); 
Route::get('/level/{levelName}' , 'ClientController@levelPage')->name('level');
Route::get('/page/{slug}', 'ClientController@slugRedirector')->name('slug');
Route::get('/courses' , 'ClientController@coursesPage')->name('courses');
Route::get('/free-classes' ,'LessonsController@Freeclasses')->name('free-classes'); 
	//Change Profile Picture

Route::post('/change/picture' ,'ProfileController@changePic')->name('upload-pic');

Route::post('/video/finished' , 'LessonsController@videoFinished')->middleware('auth');
Route::post('/video/check' ,'LessonsController@videoSeen' )->middleware('auth');
Route::post('/quiz/check' , 'LessonsController@quizCheck')->middleware('auth');
Route::post('/upload' , 'AdminController@uploadImage'); // Admin
Route::get('/lang/change/{lang}' ,'ClientController@setLang')->name('change-language');

// Route::get('/test', 'HomeController@test');
// Route::get('/testview', 'HomeController@testview');


	//Subscription Reports

Route::post('/admin/subscription/report' , 'AdminController@getSubscriptionReportsForChart');
Route::post('/admin/subscription/report/month-filter' , 'AdminController@getSubscriptionMonthFilterReport');
Route::post('/admin/income/report', 'AdminController@getIncomeReport');
Route::post('/admin/suscription/{Su}/report/{Mo}/{Ye}' , 'AdminController@getSubscriptionMonthsGrid');
Route::post('/admin/payment/report','AdminController@getIncomeData');

//STS

Route::get('/paymentTest' ,'StsPaymentController@redirectPostRequest')->name('sts-payment');
Route::post('/paymentTest' ,'StsPaymentController@redirectPostRequest');
Route::get('/redirect' , 'StsPaymentController@stsResponse')->name('sts-response');
Route::post('/redirect' , 'StsPaymentController@stsResponse');


Route::post('/validate-coupon' , 'ClientController@validateCoupon');
Route::get('/gallery' , 'ClientController@getGalleryPage');
Route::get('/kits' , 'ClientController@getKitsPage')->name('kits');
Route::get('/kits/{id}' , 'ClientController@getKitsPageWithKit')->name('kit-page');
Route::get('/achievements' , 'ClientController@getAchievementsPage')->name('achievements');

	//Testimonials

Route::get('/admin/testimonials' , 'AdminController@getTestimonialsPage')->name('testimonials-admin');
Route::get('/admin/testimonials/create' , 'AdminController@getTestimonialCreate')->name('testimonial-create');
Route::get('admin/testimonials/{id}/edit' , 'AdminController@getTestimonialEdit')->name('testimonial-edit');
Route::post('admin/testimonials/{id}/edit' , 'AdminController@setTestimonialCreateUpdate')->name('testimonial-edit-api');
Route::post('/admin/testimonials/create' , 'AdminController@setTestimonialCreateUpdate')->name('testimonial-create-api');


	//vision

Route::get('/admin/vision' , 'AdminController@getVisionPage')->name('vision-admin');
Route::get('/admin/vision/create' , 'AdminController@getVisionCreate')->name('vision-create');
Route::get('admin/vision/{id}/edit' , 'AdminController@getVisionEdit')->name('vision-edit');
Route::post('admin/vision/{id}/edit' , 'AdminController@setVisionCreateUpdate')->name('vision-edit-api');
Route::post('/admin/vision/create' , 'AdminController@setVisionCreateUpdate')->name('vision-create-api');


// banner images

Route::get('/admin/bannerimages' , 'AdminController@getBannerimagesPage')->name('banner-images');

Route::get('admin/bannerimages/{id}/edit' , 'AdminController@getBannerimagesEdit')->name('bannerimages-edit');

Route::post('admin/bannerimages/{id}/edit' , 'AdminController@setBannerimagesCreateUpdate')->name('bannerimages-edit-api');

// add to cart
Route::post('addtocart' , 'ClientController@setAddtocart')->name('addtocart');
Route::get('getcart' , 'ClientController@getcart')->name('getcart');
Route::post('updatecart', 'ClientController@updatecart')->name('updatecart');
Route::post('ajaxcart', 'ClientController@ajaxcart')->name('ajaxcart');
Route::post('deletecart', 'ClientController@deletecart')->name('deletecart');

Route::post('checkout', 'ClientController@checkout')->name('checkout');

Route::post('updatecoupon', 'ClientController@updatecoupon')->name('updatecoupon');

Route::get('/admin/order-invoice/{id}' , 'AdminController@getOrderinvoicePage')->name('admin-order-invoice');
Route::post('admin/updateorder' , 'AdminController@getUpdateorder')->name('updateorder'); 

// Email -Template

Route::get('/admin/email-template' , 'AdminController@getEmailTemplateIndex')->name('email-template');
Route::get('/admin/email-template/create' , 'AdminController@getEmailTemplateCreate')->name('email-template-create');

Route::get('/admin/email-template/{id}/edit' , 'AdminController@getEmailTemplateEdit')->name('email-template-edit');
Route::post('/admin/email-template' ,'AdminController@getEmailTemplateData');
Route::post('/admin/email-template/create' , 'AdminController@setEmailTemplateCreate')->name('email-template-create-api');
Route::post('/admin/email-template/{id}/edit' , 'AdminController@setEmailTemplateEdit')->name('email-template-edit-api');
Route::post('/admin/email-template/delete/{id}', 'AdminController@destroyEmailTemplate');



Route::get('/transaction' , 'ProfileController@getTransaction')->name('transaction');
Route::get('/ordermail' , 'ClientController@getOrdermail')->name('ordermail');
Route::get('/order-detail/{id}' , 'ProfileController@getOrder')->name('order-detail');
Route::get('/stores' , 'ClientController@getStores')->name('stores');

Route::get('/item/{id}' , 'ClientController@getGallery')->name('item');


Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('passwordemail');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('resetpassword');

Route::any('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');

Route::post('password/reset', 'Auth\ResetPasswordController@changePassword')->name('change-password');

Route::get('/ordertracking' , 'ClientController@getOrdertracking')->name('ordertracking');

Route::post('moneycollection', 'AdminController@moneycollection')->name('moneycollection');
Route::post('paymentreceive', 'AdminController@paymentreceive')->name('paymentreceive');



