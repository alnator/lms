<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


	//Categories
Route::post('/admin/categories' , 'AdminController@getCategoriesData');
Route::post('/admin/categories/create' , 'AdminController@setCategoriesCreate')->name('categories-create-api');
Route::post('/admin/categories/delete/{id}' , 'AdminController@destroyCategory');
Route::post('/admin/categories/{id}/edit', 'AdminController@setCategoriesEdit')->name('categories-edit-api');

	// Courses
Route::post('/admin/courses', 'AdminController@getCoursesdata');
Route::post('/admin/courses/delete/{id}','AdminController@destroyCourse');
Route::post('/admin/courses/{id}/users','AdminController@getCourseUsersData')->name('course-users-api'); 
Route::post('/admin/courses/{id}/users/delete/{subid}' , 'AdminController@deleteSubscription');
Route::get('/admin/get-sessions' , 'AdminController@getCourseSessions');
Route::post('/admin/courses/{id}/add-user' , 'AdminController@setAddCourseUser')->name('add-user-api');


	//Levels
Route::post('/admin/levels' , 'AdminController@getLevelsData');
Route::post('/admin/levels/delete/{id}', 'AdminController@destroyLevel');
	
	//Sessoins 
Route::post('/admin/sessions' , 'AdminController@getSessionData');
Route::post('/admin/sessions/delete/{id}' , 'AdminController@destroySession');


	// Videos
Route::post('/admin/videos' , 'AdminController@getVideosdata');
Route::post('/admin/videos/delete/{id}' , 'AdminController@destroyVideo');

	//Quizes

Route::post('/admin/{type}/{id}/quizes' , 'AdminController@getQuizesData');
Route::post('/admin/quiz/create' , 'AdminController@createEditQuiz');
Route::post('/admin/quiz/choice/create', 'AdminController@createEditChoice');
Route::post('/admin/quiz/{id}/destroy' , 'AdminController@destroyQuiz');
Route::post('/admin/choice/destroy' , 'AdminController@destroyChoice');

	//Reports

Route::post('/admin/quizes/reports', 'AdminController@getQuizReportData');

	// Dwonloadables

Route::post('/admin/files' , 'AdminController@getFilesData');
Route::post('/admin/files/create' , 'AdminController@setFilesCreateEdit')->name('files-create-api');
Route::post('/admin/files/{id}/edit' , 'AdminController@setFilesCreateEdit')->name('files-edit-api');
Route::post('/admin/files/delete/{id}' , 'AdminController@destroyFile');


	//Stripe Customers

Route::post('/admin/stripe', 'AdminController@getStripeData');
Route::post('/admin/stripe/delete/{id}' , 'AdminController@destroyStripe');
Route::post('/admin/stripe/create', 'AdminController@setStripeCreate')->name('stripe-create-api');
Route::post('/admin/stripe/{id}/edit', 'AdminController@setStripeEdit')->name('stripe-edit-api');


	//Strip Plans

Route::post('/admin/stripe-plan' , 'AdminController@getStripePlanData');
Route::post('/admin/stripe-plan/delete/{id}' , 'AdminController@destroyStripePlan');
Route::post('/admin/stripe-plan/create' , 'AdminController@setStripePlanCreate')->name('stripe-plan-create-api');
Route::post('/admin/stripe-plan/{id}/edit' , 'AdminController@setStripePlanEdit')->name('stripe-plan-edit-api');


	//Payment - Methods
	
Route::post('/admin/payment-methods','AdminController@getPaymentMethodsData');
Route::post('/admin/payment-methods/delete/{id}','AdminController@destroyPaymentMethod');
Route::post('/admin/payment-methods/create' , 'AdminController@setPaymentMethodCreate')->name('payment-method-create-api');
Route::post('/admin/payment-method/{id}/edit' , 'AdminController@setPaymentMethodEdit')->name('payment-method-edit-api');

	//Abandoned Carts

Route::post('/admin/abandoned-carts' ,'AdminController@getAbandonedCartsData');
Route::post('/admin/abandoned-carts/delete/{id}' ,'AdminController@destroyCart');
Route::post('/admin/abandoned-carts/create' ,'AdminController@setAbandonedCartCreate')->name('abandoned-carts-create-api');
Route::post('/admin/abandoned-carts/{id}/edit' ,'AdminController@setAbandonedCartEdit')->name('abandoned-carts-edit-api');

	//Emails
Route::post('/admin/emails', 'AdminController@getEmailsData');
Route::post('/admin/emails/delete/{id}', 'AdminController@destroyEmail');

	//Testimonials 
Route::post('/admin/testimonials' , 'AdminController@getTestimonialsData');
Route::post('/admin/testimonials/delete/{id}', 'AdminController@destroyTestimonial');


	//Vision 
Route::post('/admin/vision' , 'AdminController@getVisionData');
Route::post('/admin/vision/delete/{id}', 'AdminController@destroyVision');



//bannerimage
Route::post('/admin/bannerimagesapi' , 'AdminController@getBannerimagesData');
