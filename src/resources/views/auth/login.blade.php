@extends('client.master')


@section('title', 'Eureka')

@section('body-tag', 'home-page')

@section('body')
@include('client.partials.nav')


<?php $lang = App::getLocale() ?>


<section class="about_eureka inner-development login-page">
	<div class="home-content">
  <div class="container">
  	<div class="login-section header-section-wrapper" id="login" {{isset($register) && $register == 1 ? 'style="display:none"' :'' }}>
    <div class="row align-items-center justify-content-between">
      <div class="col-lg-4 col-md-6">
        <h1 class="comntitle">@lang("$lang.Login to your account")</h1>
        <!-- <h1 class="comntitle">@lang("$lang.join the extreme adventure")</h1> -->
        <form method="POST" action="{{ route('login') }}">
        @csrf
        @if (session('_previous') != '/register')
	        @if ($errors->has('name'))
			    <div class="alert alert-danger" role="alert">
  					{{ $errors->first('name') }} 
  				</div>
			@endif
			@if ($errors->has('email'))
			    <div class="alert alert-danger" role="alert">
			    	{{ $errors->first('email') }}
			    </div>
			@endif
			@if ($errors->has('password'))
			    <div class="alert alert-danger" role="alert">
			    	{{ $errors->first('password') }}
			    </div>
			@endif
		@endif
          <div class="form-group">
            <label for="login-email">@lang("$lang.Email")</label>
            <input type="email" class="form-control" id="login-email" name="email" aria-describedby="emailHelp">
          </div>
          <div class="form-group">
            <label for="login-password">@lang("$lang.Password")</label>
            <input type="password" class="form-control" id="login-password" name="password">

          </div>
          <!-- <div class="form-group">
          	<label for="remember">
          		<input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}> @lang("$lang.Remember Me")
            </label>
          </div> -->
          <button type="submit" class="btn btn-primary">@lang("$lang.LOGIN")</button>
        </form>
        <div class="row">
            <div class="col-6">
                <div class="link"><a href="{{route('try-lesson')}}">@lang("$lang.Try a free lesson")</a></div>
            </div>
            <div class="col-6">
                <div class="link hover"><a href="{{route('register')}}" >@lang("$lang.Create an account")</a></div>
            </div>
            <div class="col-6">
                <div class="link"><a href="{{route('resetpassword')}}">@lang("$lang.Reset Password")</a></div>
            </div>

        </div>
        
      </div>
      <div class="col-lg-6 col-md-6"><img src="<?php echo config('app.url').'/public'.$bannerimages->image_path; ?>" class="d-none d-md-block" alt=""> </div>
    </div>
    </div>
    <div class="register-section header-section-wrapper d-none" id="register">
    	<div class="row align-items-center justify-content-between">
      <div class="col-lg-4 col-md-6">
        <h1 class="comntitle">@lang("$lang.Create new account")</h1>
        {{Form::open(['route' => 'register'])}}
        @csrf
        @if(session('_previous') == '/register')
	        @if ($errors->has('name'))
	            <div class="alert alert-danger" role="alert">{{ $errors->first('name') }}</div>
	        @endif

	        @if ($errors->has('email'))
	            <div class="alert alert-danger" role="alert">{{ $errors->first('email') }}</div>
	        @endif
	        @if ($errors->has('password'))
	            <div class="alert alert-danger" role="alert">{{ $errors->first('password') }}</div>
	        @endif
	    @endif
          <div class="form-group">
            <label for="name">@lang("$lang.Full name")</label>
            <input type="text" class="form-control" id="name" name="name" required>
          </div>
          <div class="form-group">
            <label for="email">@lang("$lang.Email")</label>
            <input type="email" class="form-control" id="email" placeholder="Ex.janedoe@gmail.com" name="email" required>
          </div>
          <div class="form-group">
            <label for="password">@lang("$lang.Password")</label>
            <input type="password" class="form-control" id="password" name="password" required>
          </div>
          <div class="form-group">
            <label for="password-confirm">@lang("$lang.Confirm Password")</label>
            <input type="password" class="form-control" id="password-confirm" name="password_confirmation" required="required">
          </div>
          <div class="form-group">
            <label for="phone">@lang("$lang.Phone Number")</label>
            <input type="number" class="form-control" id="phone" placeholder="+962" name="phone">
          </div>
          <div class="form-group">
            <label for="age">@lang("$lang.Age")</label>
            <input type="number" class="form-control" id="age" name="age">
          </div>
          

                                    
          {{ Form::submit(__("$lang.Sign Up") , ['class'=>'btn btn-primary']) }}
          
        </form>
        <div class="footer">
                                    <div class="row">
                                        <div class="col">
                                            <div class="link">
                                                <a href="{{route('try-lesson')}}">@lang("$lang.Try a free lesson")</a>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="link hover">
                                                <a href="{{route('login')}}">@lang("$lang.Already have an account")</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
      </div>
       <div class="col-lg-6 col-md-6"><img src="<?php echo config('app.url').'/public'.$bannerimagesre->image_path; ?>" class="d-none d-md-block" alt=""> </div>
    </div>

 
    </div>
  </div>
</div>
</section>

@endsection

@section('scripts')
    <script src="{{URL::asset('js/client/index.js')}}"></script>    
    <script type="text/javascript">
        @if(isset($register) && $register == 1 )
            toRegister();
        @endif
    </script>
@endsection