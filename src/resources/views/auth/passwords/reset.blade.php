@extends('client.master')


@section('title', 'Eureka')

@section('body-tag', 'home-page')

@section('body')
@include('client.partials.nav')


<?php $lang = App::getLocale() ?>
<section class="about_eureka inner-development login-page">
    <div class="home-content">
        <div class="container">
            <div class="login-section header-section-wrapper">
                <div class="row align-items-center justify-content-between">
                    <div class="col-lg-6 col-md-6">
                        <h1 class="comntitle">@lang("$lang.Reset Password")</h1>
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif
                        <form method="POST" action="{{ route('change-password') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">
                            <label for="email">@lang("Email Address")</label>

                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required >

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                        <div class="form-group">
                            <label for="password">@lang("Password")</label>

                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group">
                            <label for="password-confirm">@lang("Confirm Password")</label>

                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                @lang("Reset Password")
                            </button>
                        </div>
                    </form> 



                    </div>
                    <div class="col-lg-6 col-md-6"><img src="<?php echo config('app.url').'/public'.$bannerimages->image_path; ?>" class="d-none d-md-block" alt=""> </div>
            </div>
        </div>
    </div>
</section>

@endsection
