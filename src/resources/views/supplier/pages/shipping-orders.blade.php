@extends('supplier.suppliermaster')

@section('title','Items')
@section('styles')

    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo config('app.url'); ?>/css/bootstrap-datepicker3.min.css">
	<style type="text/css">
		  .display 
    {
   		display: flex !important;
    }
    .popup 
    {
		position: absolute;
		left: 20%;
		top: 15%;
		z-index: 20;
	}
	td{
		text-align: center !important;
		
	}
	th {
		text-align: center !important;
	}
	</style>
@endsection

@section('content')
	<div class="col-12 card" id="shipping-list-grid">
		
	</div>


	<div class="modal fade" id="edit-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog  modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Order Items</h5>
	        <button type="button" class="close" onclick="closeModal()" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      		<div class="row">
	      			<div class="col-md-12">
	      				<table class="table table-hover">
	      					<thead>
	      						<th>Item Name</th>
	      						<th style="text-align: center;">Amount</th>
	      					</thead>
	      					<tbody id="fill-here">
	      						
	      					</tbody>
	      				</table>
	      			</div>
	      		</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeModal()">Close</button>
	        <!-- <button type="submit" form="submit-link" id="submit-btn" class="btn btn-primary"></button> -->
	      </div>
	    </div>
	  </div>
	</div>
@endsection


@section('scripts')
<script type="text/javascript">var site_url="<?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
 	<script src="<?php echo config('app.url'); ?>/js/bootstrap-datepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/supplier/shipping-list-grid.js"></script>
@endsection 