@extends('supplier.suppliermaster')

@section ('title','Add Item')
@section ('styles')

    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
	<div class="col-12 card" >
		{!! Form::open(['route'=>'add-item-api']) !!}
		<input type="text" name="id" value="{{Auth::id()}}" hidden>
		<div class="row">
			<div class="col-6">
				{{ Form::label('item_id' , 'Item: ')}}
				<select class="custom-select input-rounded form-control" name="item_id">	
				@foreach($suppliedItems as $item)
					<option value="{{$item->id}}"> {{$item->name_en}}</option>
				@endforeach
				</select>
				<br><br>
				{{ Form::label('price' , 'Price: ')}}
				{{ Form::number('price' , null , ['class' => 'form-control input-rounded']) }}
			</div>
			<div class="col-6">
				{{ Form::label('qty' , 'Quantity: ')}}
				{{ Form::number('qty' , null , ['class' => 'form-control input-rounded']) }}
				<br><br>
				{{Form ::label('active' , 'Active: ')}}
				{{ Form::checkbox('active', null)}}
			</div>
			<div class="col-12">
				<br><br><br>
				<button class="btn btn-block btn-success">Submit</button>
				<a href="{{route('items-supplying')}}" class="btn btn-block btn-default">Cancel</a>
			</div>
		</div>
			{!! Form::close() !!}

	</div>
@endsection


@section ('scripts')
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/supplier/kit-items-grid.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
    
@endsection 