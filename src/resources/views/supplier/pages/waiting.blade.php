@extends('supplier.suppliermaster')

@section ('title','Items')
@section ('styles')

    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo config('app.url'); ?>/css/bootstrap-datepicker3.min.css">
@endsection

@section ('content')
	<div class="col-12 card" id="waiting-list-grid">
		
	</div>
@endsection


@section ('scripts')
<script type="text/javascript">var site_url="<?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
 	<script src="<?php echo config('app.url'); ?>/js/bootstrap-datepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/supplier/waiting-list-grid.js"></script>

<!--     <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
 -->    
@endsection 