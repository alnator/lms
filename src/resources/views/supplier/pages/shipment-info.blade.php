@extends('supplier.suppliermaster')

@section ('title','Shipment Info')
@section ('styles')

    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
		{!! Form::open(['route' => 'update-shipment-info' , 'id'=>'info-form']) !!}
	<div class="col-12 card" >
		
		<div class="row">

			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('city' , 'City: ')}}
					{{ Form::text('city' , isset($supplierInfo->city) ? $supplierInfo->city : '' , ['class' => 'form-control input-rounded', 'required' => 'required'])}}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('name' , 'Name: ')}}
					{{ Form::text('name' , isset($supplierInfo->name) ? $supplierInfo->name : '' , ['class' => 'form-control input-rounded', 'required' => 'required'])}}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('phone_number' , 'Phone Numbers: ')}}
					{{ Form::text('phone_number' , isset($supplierInfo->phone_number) ? $supplierInfo->phone_number : '' , ['class' => 'form-control input-rounded', 'required' => 'required'])}}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('cell_phone_number' , 'Cell Phone Number: ')}}
					{{ Form::text('cell_phone_number' , isset($supplierInfo->cell_phone_number) ? $supplierInfo->cell_phone_number : '', ['class' => 'form-control input-rounded', 'required' => 'required'])}}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('email' , 'Email: ')}}
					{{ Form::email('email' , isset($supplierInfo->email ) ? $supplierInfo->email : '', ['class' => 'form-control input-rounded', 'required' => 'required'])}}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('province_code' , 'Province Code: ')}}
					{{ Form::text('province_code' , isset($supplierInfo->province_code) ? $supplierInfo->province_code : '', ['class' => 'form-control input-rounded'])}}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('post_code' , 'Postal Code: ')}}
					{{ Form::text('post_code' , isset( $supplierInfo->post_code ) ? $supplierInfo->post_code : '', ['class' => 'form-control input-rounded'])}}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="country_code">Country Code: </label> &nbsp;<small><a href="https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes" target="_blank">ISO alpha 2 codes</a></small> 
					{{ Form::text('country_code' , isset($supplierInfo->country_code) ? $supplierInfo->country_code : '', ['class' => 'form-control input-rounded', 'required' => 'required'])}}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('address_line1' , 'Address Line 1: ')}}
					{{ Form::textarea('address_line1' , isset($supplierInfo->address_line1) ? $supplierInfo->address_line1 : '', ['class' => 'form-control input-rounded', 'required' => 'required'])}}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('address_line2' , 'Address Line 2: ')}}
					{{ Form::textarea('address_line2' , isset($supplierInfo->address_line2) ? $supplierInfo->address_line2 : '', ['class' => 'form-control input-rounded', 'required' => 'required'])}}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('address_line3' , 'Address Line 3: ')}}
					{{ Form::textarea('address_line3' , isset($supplierInfo->address_line3) ? $supplierInfo->address_line3 : '', ['class' => 'form-control input-rounded'])}}
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-12 carc">
		<button class="btn btn-success btn-block" >Update Changes</button>
	</div>
	{{ Form::close() }}

@endsection


@section ('scripts')
    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
@endsection 