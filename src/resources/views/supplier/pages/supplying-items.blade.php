@extends('supplier.suppliermaster')

@section ('title','Supplying Items')
@section ('styles')

    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
	<input type="text" id="supplier_id" value="{{$supplierId}}" hidden>
	<div class="col-12 card" id="items-grid">
		
	</div>
	<div class="col-12 card">
		<a class="btn btn-block btn-success" href="{{route('add-item-supplier')}}">Add Item</a>
	</div>


@endsection


@section ('scripts')
<script type="text/javascript">var site_url="<?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/supplier/supplying-items-grid.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
    
@endsection 