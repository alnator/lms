<!-- End Wrapper -->
<!-- All Jquery -->
<script src="<?php echo config('app.url'); ?>/js/lib/jquery/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="<?php echo config('app.url'); ?>/js/lib/bootstrap/js/popper.min.js"></script>
<script src="<?php echo config('app.url'); ?>/js/lib/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?php echo config('app.url'); ?>/js/jquery.slimscroll.js"></script>
<!--Menu sidebar -->
<script src="<?php echo config('app.url'); ?>/js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="<?php echo config('app.url'); ?>/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
<!--Custom JavaScript -->
<script src="<?php echo config('app.url'); ?>/js/custom.min.js"></script>

@yield('scripts')