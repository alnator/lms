<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="nav-label">Home</li>
                <li>
                    <a href="{{route('kits-supplier')}}">
                    <i class="fas fa-briefcase"></i>
                        Kits
                    </a>
                </li>
                <li> <a href="{{route('items-supplier')}}"><i class="fas fa-screwdriver"></i>Items</a></li>
                <li> <a href="{{route('items-supplying')}}"><i class="fas fa-box-open"></i>supplying Items</a></li>
                <li> <a href="{{route('waiting-items-supplier')}}"><i class="far fa-clock"></i>Waiting List</a></li>
                <li> <a href="{{route('supplier-shipping-list')}}"><i class="fas fa-truck-moving"></i>Shipping List</a></li>
                <li> <a href="{{route('shipment-info')}}"><i class="fas fa-info"></i>Shipment Infromation</a></li>
                <!-- <li> <a href="{{route('kits-supplier')}}">Kits</a></li> -->
                
                <!--
                <li> 
                    <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-bar-chart"></i><span class="hide-menu">Charts</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a >Flot</a></li>
                        <li><a >ChartJs</a></li>
                        <li><a >Chartist </a></li>
                        <li><a >AmChart</a></li>
                        <li><a >EChart</a></li>
                        <li><a >Sparkline</a></li>
                        <li><a >Peity</a></li>
                    </ul>
                </li>
                <li class="nav-label">Features</li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu">Bootstrap UI <span class="label label-rouded label-warning pull-right">6</span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a >Alert</a></li>
                        <li><a >Button</a></li>
                        <li><a >Dropdown</a></li>
                        <li><a >Progressbar</a></li>
                        <li><a >Tab</a></li>
                        <li><a >Typography</a></li>
                    </ul>
                </li>
    <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu">Components <span class="label label-rouded label-danger pull-right">6</span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a >Calender</a></li>
                        <li><a >Datamap</a></li>
                        <li><a >Nestedable</a></li>
                        <li><a >Sweetalert</a></li>
                        <li><a >Toastr</a></li>
                        <li><a >Weather</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-wpforms"></i><span class="hide-menu">Forms</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a >Basic Forms</a></li>
                        <li><a >Form Layout</a></li>
                        <li><a >Form Validation</a></li>
                        <li><a >Editor</a></li>
                        <li><a >Dropzone</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-table"></i><span class="hide-menu">Tables</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a >Basic Tables</a></li>
                        <li><a >Data Tables</a></li>
                    </ul>
                </li>
                <li class="nav-label">Layout</li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-columns"></i><span class="hide-menu">Layout</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a >Blank</a></li>
                        <li><a >Boxed</a></li>
                        <li><a >Fix Header</a></li>
                        <li><a >Fix Sidebar</a></li>
                    </ul>
                </li>
                
            </ul>
        </nav>
        <!- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</div>
