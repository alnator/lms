<?php $lang = App::getLocale() ?>

<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>
		@yield('title')
	</title>
	@include('client.partials.header')

</head>
<body>
	
	@csrf
	
	

	@yield('body')

	@include('client.partials.footerbar')

	@include('client.partials.footer')
	
	<div class='cookie-banner' style='display: none;z-index:100;'>
		<p>
		    By using our website, you agree to our 
		    <a href='insert-link'>cookie policy</a>
	  	</p>
	  	<div class="close">x</div>
	</div>

</body>
</html>