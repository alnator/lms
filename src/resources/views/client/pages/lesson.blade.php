@extends('client.master')

@section('title', 'Eureka')

@section('body-tag', 'level-page')

@section('body')
@include('client.partials.linked-nav')

<?php $lang = App::getLocale() ?>

</div>

<section class="about_eureka inner-development">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <h4 class="comntitle">{{$course->title_en}} - @lang("$lang.Session") {{$session->session_number}} </h4>
                <div class="row">
                    <div class="col-lg-7">
                        <h1 class="comntitle">{{$video->name_en}}</h1>
                    </div>
                    <div class="col-lg-5">
                        <div class="inner-quick-links">
                            <?php $download='file_path_'.$lang; ?>
                            @if (isset($course->downloadable->$download))

                                @if($course->downloadable->$download != "")

                                    <a href="{{$course->downloadable->{'file_path_'.$lang} }}">
                                        <img src="<?php echo config('app.url'); ?>/css/client/images/download-arrow.png" alt="">@lang("$lang.Dwonload Attachements")
                                    </a>

                                @else

                                    @if($course->downloadable->file_path_en!="")
                                        <a href="{{$course->downloadable->file_path_en }}">
                                            <img src="<?php echo config('app.url'); ?>/css/client/images/download-arrow.png" alt="">@lang("$lang.Dwonload Attachements")
                                        </a>
                                    @endif


                                @endif
                            @endif
                            @if ($video->quiz != 0)
                                <a href="javascript:void" onclick="Qcommon(0,{{ count($quizes) }} );" style="float: right;"><img src="<?php echo config('app.url'); ?>/css/client/images/take-quiz-icon.png" alt="">@lang("$lang.Solve Quiz")</a>
                            @endif

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">

                    @if($video->is_youtube == 0)
                        <!-- <video class="lessons-video" id="myVideo">
              <source src="{{$video->path}}">
            </video> -->
                            <div class="use-play-1 flowplayer" data-player-id="56058953-2cbd-4858-a915-1253bf7ef7b2">
                                <script src="//cdn.flowplayer.com/players/8dfd6c14-ba3a-445e-8ef5-191d9358ed0a/native/flowplayer.async.js">
                                    { "src": "{{$video->path}}" }
                                </script>
                            </div>
                        @else



                            <iframe src="https://player.vimeo.com/video/{{$video->url_identifier}}?color=ffffff&title=0&byline=0&portrait=0&autoplay=true&speed=true&transparent=false" width="100%" height="409" frameborder="0" allow="autoplay; fullscreen" allowfullscreen ></iframe>




                        @endif

                        <p>{{$video->description_en}}</p>
                        <p><div class="ls-time">{{intval($video->estimated_time /60 )}}:{{intval($video->estimated_time % 60) < 10 ? '0'.intval($video->estimated_time % 60) : intval($video->estimated_time % 60)}} @lang("$lang.min")</div></p>
                        <p>@if(isset($course->kit_id) && $course->kit_id != ''  )
                                <a href="{{ route('kit-page' , $course->kit_id) }}">
                                    @lang("$lang.Get Course's Kit")
                                </a>
                            @endif</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@if(isset($course->getkit))

    <section class="lessons-need-sliders">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h5 class="comntitle">@lang("$lang.You will need")</h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-4">

                <div class="style-full">
                    <div class="main-pic">
                        @if(isset($kits->images))
                            <?php $kk=1;?>
                            @foreach($kits->images as $image)
                                <?php if(count($kits->images)== $kk){ ?>
                                <img src="{{ $image->img_src }}" alt="">
                                <?php }
                                $kk++; ?>
                            @endforeach
                        @else
                            <img src="<?php echo config('app.url'); ?>/css/client/images/lessons-slider-pic1.png" alt="">
                        @endif

                    </div>
                    <hr class="img-btm-part">
                    <div class="product-title">
                        <h4>{{ $kits->name_en }}</h4>
                        <p>{{ $kits->itemsCount }} @lang("$lang.Number of Items")</p>
                        @if($kits->active == '1' && $kits->available != 0)
                            <span>
      				<img src="<?php echo config('app.url'); ?>/css/client/images/right-icon.png" alt="">@lang("$lang.Available in store")
      			</span>
                        @else
                            <span>
      				<img src="<?php echo config('app.url'); ?>/css/client/images/cross-icon.png" alt="">@lang("$lang.Not Available in store")
      			</span>
                        @endif
                        <div class="sub-unsub">
                            <p><span>{{ $kits->maxPrice }}$</span></p>
                        </div>
                    </div>
                    <button onclick="location.href='{{ route('kit-page' , $kits->id) }}'"> @lang("$lang.Continue")</button>

                </div>

            </div>
            <div class="col-xl-8 col-lg-8 col-md-8">
                <div class="owl-carousel owl-theme" id="lessons-two">
                    <?php $k=1; ?>
                    @foreach($items as $kititem)

                        <?php if($k==1){ ?>
                        <div class="item">
                            <?php } ?>

                            <div class="style-full style-half">
                                <div class="main-pic">
                                    @if(isset($kititem->images))
                                        <?php $p=1;?>
                                        @foreach($kititem->images as $image)
                                            <?php if(count($kititem->images)== $p){ ?>
                                            @if(file_exists($image->img_src))
                                                <img src="{{ $image->img_src }}" alt="">
                                            @else
                                                <img src="<?php echo config('app.url'); ?>/css/client/images/lessons-slider-pic2.png" alt="">
                                            @endif
                                            <?php }
                                            $p++; ?>
                                        @endforeach
                                    @else
                                        <img src="<?php echo config('app.url'); ?>/css/client/images/lessons-slider-pic2.png" alt="">
                                    @endif

                                </div>
                                <div class="product-title">
                                    @if(session('lang') == 'ar')
                                        <h4>{{ $kititem->items->name_ar }}</h4>
                                    @else
                                        <h4>{{ $kititem->items->name_en }}</h4>
                                    @endif
                                    <p>&nbsp;</p>

                                    @if($kititem->qty > 0)
                                        <span>
                <img src="<?php echo config('app.url'); ?>/css/client/images/right-icon.png" alt="">@lang("$lang.Available in store")
              </span>
                                    @else
                                        <span>
                <img src="<?php echo config('app.url'); ?>/css/client/images/cross-icon.png" alt="">@lang("$lang.Not Available in store")
              </span>
                                    @endif




                                    <div class="sub-unsub">
                                        <p><span>{{ Helper::showCurrency($kititem->maxPrice) }}</span></p>
                                    </div>
                                </div>
                                @if($kititem->qty > 0)
                                    <form method="POST" action="{{ route('addtocart') }}">
                                        @csrf
                                        <input type="hidden" name="itemid" value="{{ $kititem->id }}">
                                        <input type="hidden" name="type" value="Item">
                                        <input type="hidden" name="price" value="{{$kititem->maxPrice}}">
                                        <input type="hidden" name="supplierId" value="{{$kititem->supplierId}}">
                                        <input type="hidden" name="qty" value="1">

                                        <button type="submit" >@lang("$lang.Order now")</button>
                                    </form>
                                @else
                                    <button onclick="location.href='{{ route('kit-page' , $kititem->kit_id) }}'" disabled="disabled">@lang("$lang.Order now")</button>
                                @endif
                            </div>
                            <?php if($k % 2 == 0 && $k != count($items)){ ?>
                        </div>
                        <div class="item">
                            <?php }
                            if($k == count($items)){ ?>
                        </div>
                        <?php }
                        ?>

                        <?php
                        $k++; ?>
                    @endforeach
                </div>
            </div>

        </div>
    </section>
@endif
<section class="lessons-next-pre">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div id="myCarousel" class="carousel slide next-pre" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item  active">
                            <div class="row">
                                <div class="col-sm-6">
                                    @if(session('lang') == 'ar')
                                        <div class="nxt-pre-slider right">
                                            @else
                                                <div class="nxt-pre-slider left">
                                                    @endif

                                                    @if(isset($previous))
                                                        <p>@lang("$lang.Session") {{ $previous->sessionbyid->session_number }}</p>
                                                        <h4><a href="{{$previous->free ? route('free-lesson',$previous->id) : route('lesson',$previous->id)  }}">{{ $previous->name_en }}</a></h4>
                                                    @else
                                                        <p>@lang("$lang.Get Started")</p>
                                                        <h4><a href="{{route('course-page', $course->slug)}}">@lang("$lang.Get Started")</a></h4>
                                                    @endif
                                                </div>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(session('lang') == 'ar')
                                                <div class="nxt-pre-slider left">
                                                    @else
                                                        <div class="nxt-pre-slider right">
                                                            @endif
                                                            @if(isset($next))
                                                                <p>@lang("$lang.Session") {{ $next->sessionbyid->session_number }}</p>
                                                                <h4><a href="{{$next->free ? route('free-lesson',$next->id) : route('lesson',$next->id)  }}">{{ $next->name_en }}</a></h4>
                                                            @else
                                                                @if ($video->quiz != 0)
                                                                    <p>@lang("$lang.Solve Quiz")</p>
                                                                    <h4><a href="javascript:void" onclick="Qcommon(0,{{ count($quizes) }});" >@lang("$lang.Solve Quiz")</a></h4>
                                                                @endif
                                                            @endif
                                                        </div>
                                                </div>
                                        </div>
                                </div>

                            </div>


                            @if(isset($previous))
                                <a href="{{$previous->free ? route('free-lesson',$previous->id) : route('lesson',$previous->id)  }}" class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"><img src="<?php echo config('app.url'); ?>/css/client/images/right-arrow.png" alt=""></span> </a>
                            @else
                                <a href="{{route('course-page', $course->slug)}}" class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"><img src="<?php echo config('app.url'); ?>/css/client/images/right-arrow.png" alt=""></span> </a>
                            @endif

                            @if(isset($next))
                                <a href="{{$next->free ? route('free-lesson',$next->id) : route('lesson',$next->id)  }}" class="carousel-control-next" href="#myCarousel" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"><img src="<?php echo config('app.url'); ?>/css/client/images/left-arrow.png" alt=""></span> </a>
                            @else
                                @if ($video->quiz != 0)
                                    <a href="javascript:void" class="carousel-control-next" href="#myCarousel" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"><img src="<?php echo config('app.url'); ?>/css/client/images/left-arrow.png" alt=""></span> </a>
                                @endif
                            @endif


                        </div>


                    </div>
                </div>
            </div>
</section>

<!--quiz Modal -->
<?php $counter = 0 ?>
@foreach($quizes as $quiz)
    <?php $counter++ ?>
    <div class="modal fade quiz-modal" id="quiz-modal" tabindex="-1" role="dialog" aria-labelledby="quiz" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">


                <div class="modal-body" id="options{{$counter}}" data-id="{{$quiz['id']}}">
                    <div class="model-header">
                        {{$counter}} @lang("$lang.of") {{ count($quizes) }} @lang("$lang.Questions")
                        <hr>
                    </div>
                    <div class="quiz-body">
                        <p>@if($lang=='en') {{$quiz['quiz_statement'] }} @else {{$quiz['quiz_statement_ar'] }} @endif</p>


                        <?php $counter2 = 0; ?>
                        @foreach($quiz['choice'] as $choice)
                            <?php $counter2++; ?>

                            <div class="radio-part">
                                <input type="radio" name="quiz_radio{{$counter}}" id="exampleRadios{{$counter}}_{{$counter2}}" class="radio form-check-input" value="{{$choice['id']}}"/>
                                <label for="exampleRadios{{$counter}}_{{$counter2}}"> @if($lang=='en') {{ $choice['choice_statement'] }} @else {{ $choice['choice_statement_ar'] }} @endif</label>
                            </div>



                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">

                    <button class="btn main2-border-btn rewatchbtn" data-dismiss="modal" onclick="restartVideo();">@lang("$lang.Re-watch the video")</button>


                    <button class="btn main2-border-btn nextbtn" onclick="Qcommon({{ $counter }},{{ count($quizes) }});">@if( $counter == count($quizes)) @lang("$lang.Submit") @else @lang("$lang.Next") @endif</button>

                </div>
            </div>
        </div>
    </div>
@endforeach

@endsection
@section ('scripts')
    <script src="<?php echo config('app.url'); ?>/js/video-js/video-js/video.js"></script>
    <script src="<?php echo config('app.url'); ?>/js/video-js/videojs-contrib-hls.js"></script>
    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>

    <script type="text/javascript">

        var siteurl = "<?php echo config('app.url'); ?>";

        var numberOfCorrectAnswers = 0 ;
        var video_id = {{ $video->id }};
    </script>
    <script type="text/javascript">
        function FinishVideo(){

            if (numberOfCorrectAnswers < 3){
                swal({
                    type: 'error',
                    title: 'Fail',
                    text: 'You failed the quiz, You should re-watch the video and answer the questions right.',
                });
                numberOfCorrectAnswers = 0;
                restartVideo();
            }
            else {
                $.ajax({
                    type: "POST",
                    url: siteurl+'/video/finished',
                    dataType: "json",
                    data: {'video_id': video_id },
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done(response =>{
                    swal('Good job!','You passed the quiz!','success');
                });

                finishVideoAjax();


            }
        }

        function Qcommon(start,end){

            if(start==0){

                numberOfCorrectAnswers = start;
                $($(".modal")[start]).modal("show");


            }else if(start == end){

                var msg = validate(start);
                if(msg==true){
                    $($(".modal")[start-1]).modal("hide");
                    FinishVideo();
                }else{
                    swal({
                        type: 'error',
                        title: 'Error',
                        text: 'Please Select options.',
                    });
                }

            }else{

                var msg=validate(start);
                if(msg==true){
                    $($(".modal")[start-1]).modal("hide");
                    $($(".modal")[start]).modal("show");
                }else{

                    swal({
                        type: 'error',
                        title: 'Error',
                        text: 'Please Select options.',
                    });

                }
            }



        }

        /*	function Q1(){
                numberOfCorrectAnswers = 0;
                $($(".modal")[0]).modal("show");
            }
            function Q2(){
                validate(1);
                $($(".modal")[0]).modal("hide");
                $($(".modal")[1]).modal("show");
            }
            function Q3(){
                validate(2);
                $($(".modal")[1]).modal("hide");
                $($(".modal")[2]).modal("show");
            }
            function Q4(){
                validate(3);
                $($(".modal")[2]).modal("hide");
                $($(".modal")[3]).modal("show");
            }
            function Q5(){
                validate(4);
                $($(".modal")[3]).modal("hide");
                $($(".modal")[4]).modal("show");
            }
            function Q6(){
                validate(5);
                $($(".modal")[4]).modal("hide");
                FinishVideo();
            }*/
        function restartVideo(){
            var player;
            player.play();
        }
        function validate(Id){
            var msg=true;
            id = $('#options'+Id).data('id');
            checkeditem = -1;
            $('#options'+Id+" .form-check-input").each(function (obj){
                if (this.checked == 1){
                    checkeditem = this.value;
                }
            });

            if(checkeditem == -1){
                msg=false;
                return msg;
            }else{
                $.ajax({
                    type: "POST",
                    url: siteurl+'/quiz/check',
                    dataType: "json",
                    data: {'quiz_id': id , 'choice_id':checkeditem },
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    }
                }).always(response => {
                    if (response.responseText == 'ok'){
                        numberOfCorrectAnswers++;
                    }
                });

                return msg;

            }

        }

        function finishVideoAjax(){
            $.ajax({
                type: "POST",
                url: siteurl+'/video/check',
                dataType: "json",
                data: {'video_id':video_id},
                headers: {
                    "x-csrf-token": $("[name=_token]").val()
                }
            });
        }


                @if($video->is_youtube == 0)
        var player = videojs('lesson-video');
        player.play();
        player.on('ended', function() {
            Qcommon(0,{{count($quizes)}});
            finishVideoAjax();
        });

        @else
        @if (Auth::check())
        $.ajax({
            type: "POST",
            url: siteurl+'/video/finished',
            dataType: "json",
            data: {'video_id':  {{$video->id}} },
            headers: {
                "x-csrf-token": $("[name=_token]").val()
            }
        });
        @endif
        @endif
    </script>
    @if($lang=='ar')
        <script>
            $('#lessons-two').owlCarousel({
                loop:true,
                center: false,
                margin: 30,
                rtl:true,
                nav: true,
                dots:false,
                navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 1
                    },
                    900: {
                        items:2
                    },
                    1000: {
                        items:2
                    },
                    1100: {
                        items: 2
                    }
                }
            })
        </script>
        <script>
            $('#next-pre').owlCarousel({
                loop:true,
                rtl:true,
                center: false,
                margin: 30,
                nav: true,
                dots:false,
                navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    900: {
                        items:3
                    },
                    1000: {
                        items:3
                    },
                    1100: {
                        items: 2
                    }
                }
            })
        </script>
    @else
        <script>
            $('#lessons-two').owlCarousel({
                loop:true,
                center: false,
                margin: 30,
                nav: true,
                dots:false,
                navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 1
                    },
                    900: {
                        items:2
                    },
                    1000: {
                        items:2
                    },
                    1100: {
                        items: 2
                    }
                }
            })
        </script>
        <script>
            $('#next-pre').owlCarousel({
                loop:true,
                center: false,
                margin: 30,
                nav: true,
                dots:false,
                navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    900: {
                        items:3
                    },
                    1000: {
                        items:3
                    },
                    1100: {
                        items: 2
                    }
                }
            })
        </script>
    @endif
    <script>
        $('.lessons-video').parent().click(function () {
            if($(this).children(".lessons-video").get(0).paused){        $(this).children(".lessons-video").get(0).play();   $(this).children(".playpause").fadeOut();
            }else{       $(this).children(".lessons-video").get(0).pause();
                $(this).children(".playpause").fadeIn();
            }
        });

    </script>

@endsection