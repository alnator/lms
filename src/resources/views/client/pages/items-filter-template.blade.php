@foreach($items as $item)
							<div class="col row item">
								<div class="col-10 offset-1 class item-section" >
									<div class="col-12 items-subsection ">
										<h4>{{$item->name_en}}</h4>
									</div>
									<div class="col-12 img-col">
										<center>
											<img src="{{isset($item->images[0]->img_src) ? $item->images[0]->img_src : ''}}"  alt="Item Image" width="200" height="230">
										</center>
									</div>
									<hr>
									<div class="row">
										<div class="col-6 item-details" style=" text-align: left;">
											<span>
											Item Price
											</span>
											<br>
											<span>
											Add to kit
											</span>
											<br><br>
											<span>
											Quantity
											</span>
		 								</div>
										<div class="col-6 item-details" style="text-align: right;">
											@if ($item->maxPrice['MaxAmount'] !=  -1)
											<span>
												{{$item->maxPrice['MaxAmount'] }} JOD 
											</span>
											<br>
											<span class="add-kit-span">
											  </label>
											  Add to kit &nbsp;
											  <input type="checkbox" data-id="{{$item->id}}" data-price="{{$item->maxPrice['MaxAmount']}}" class="option-input checkbox" />
											  </label>
											</span>
											<span>
												<input type="number" value="0" data-price="{{$item->maxPrice['MaxAmount']}}" data-id="{{$item->id}}" class="qty-input" style="width: 63px;" >
											</span>
											@else 
											<span>
												Unavailable 
											</span>
											<br>
											<span class="add-kit-span">
											  </label>
											    Unavailable
											  </label>
											</span>
											@endif
										</div>
									</div>
								</div>
							</div>
						@endforeach 
						<script type="text/javascript">
							$('.option-input.checkbox').change(function(){
								price = $(this).data('price');
								qty = $($(this).parent().parent().find('.qty-input')[0]).val();
								id= $(this).data('id');
								if(this.checked) {
						        	calcTotal(price, qty, id);
						        }
						        else {
						        	removeFromKit(price , id);        	
						        }
							});
							$(".qty-input").on("keypress", function (evt) {
							    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
							    {
							        evt.preventDefault();
							    	return ;
							    }
							    val =  $(this).val() * 10 ;
							    val += Number(evt.key);

							    id = $(this).data('id');
							    price = $(this).data('price');
							    qty = val;
							    flag = $($(this).parent().parent().find('.option-input.checkbox')[0]);
							    if($(flag).is(':checked'))
								    calcTotal(price , qty , id);
							});
							
						</script>