@extends('client.master')


@section('title', 'Eureka')


@section('body-tag' , 'profile-page')
@section('body')

@include('client.partials.linked-nav')


<?php $lang= App::getLocale() ?>

<section class="new-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="#">Courses</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">@lang("$lang.Transaction")</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</section>

<section class="about_eureka inner-development">
  <div class="container">
    
  

   

    <div class="topic-chapter">
      <div class="row">
        <div class="col-md-12">
          <h4>@lang("$lang.Transaction")</h4>
          <div class="sessions-lessons">
			
						<ul class="list-unstyled course-list">
            			
            			@foreach($order as $orderobj)	
								
					<li>
						    <div class="chapter-img">
						        <span class="date1">
<span class="month">
{{$orderobj->created_at->format('M')}}
</span>
<span class="day">
{{$orderobj->created_at->format('d')}}
</span>
</span>
						    </div>
						    <div class="chapter-detail">
						        <div class="capter-title">
						        <h5><a class="lesson-name" href="{{url('order-detail/'.$orderobj->id)}}">@lang("$lang.Order") #{{$orderobj->id}}</a></h5>
						        <span>@if($orderobj->status=='paid') @lang("$lang.Paid") @else @lang("$lang.Unpaid") @endif</span>
							 </div>
						                <p>@lang("$lang.Amount") : {{ Helper::showCurrency($orderobj->total_price) }}</p>
						                
						              </div>
						            </li>

						            @endforeach


						           
													
														</ul>
						
						
						</div>
					</div>
          
          
        </div>
      </div>
    </div> 

</section>
					
<style type="text/css">
	.date1
{
float: left;
    height: 65px;
    width: 62px;
    background: #F44336;
    margin-right: 10px;
    padding-top: 10px;
    line-height: normal;
}

.date1 .month
{
display: block;
text-align: center;
color: #FFF;
font-size: 11px;
padding-top: 4px;
text-transform: uppercase;
}

.date1 .day
{
display: block;
text-align: center;
padding-top: 5px;
color: #fff;
font-size: 18px;
font-weight: bold;
}

.chapter-img{
	padding: 16px 50px;
}


</style>
@endsection
