<?php $lang = App::getLocale() ?>
<div class="modal-header" >
	<div class="col-12 modal-header-col" >
		<h3 class="modal-heading">{{$kit->name_en}} @lang("$lang.Items")</h3>
	</div>
</div>
<div class="modal-body items-modal-body">
	<div class="row  d-flex justify-content-center">
		@foreach($kit->items as $item)
			<div class="col row item">
				<div class="col-10 offset-1 class item-section" >
					<div class="col-12 items-subsection ">
						<h4>{{$item->items->name_en}}</h4>
					</div>
					<div class="col-12 img-col">
						<center>
							<img src="{{isset($item->images[0]->img_src) ? $item->images[0]->img_src : ''}}" width="200" height="200">
						</center>
					</div>
					<hr>
					<div class="row">
						<div class="col-6 item-details" style=" text-align: left;">
							<span>
							@lang("$lang.Item Price")
							</span>
							<br>
							<span>
							@lang("$lang.Add to kit")
							</span>
							</div>
						<div class="col-6 item-details" style="text-align: right;">
							@if ($item->maxPrice['MaxAmount'] !=  -1)
							<span>
								{{$item->maxPrice['MaxAmount'] }} JOD
							</span>
							<br>
							<span class="add-kit-span">
							  </label>
							    @lang("$lang.Add to kit")
							    <input type="checkbox" class="option-input checkbox" checked disabled="true" />
							  </label>
							</span>
							@else 
							<span>
								@lang("$lang.Unavailable") 
							</span>
							<br>
							<span class="add-kit-span">
							  <label>
							    @lang("$lang.Unavailable")
							  </label>
							</span>
							@endif
						</div>
					</div>
				</div>
			</div>
		@endforeach
	</div>
	<div class="col-12 modal-footer-details">
		<hr>
		<div class="row">
				<div class="col-12" style="color: #fff">
					<div class="row">
						<div class="col-12">
							<center>
								<h2>@lang("$lang.Shipping Information")</h2>
							</center>
						</div>
						<div class="col-md-6">
							<label for="city-input1">@lang("$lang.City") *</label>
							<input class="form-control" type="text" name="city" id="city-input1"><br>
						</div>
						<div class="col-md-6">
							<label for="name-input1">@lang("$lang.Name") *</label>
							<input class="form-control" type="text" name="name" id="name-input1"><br>
						</div>
						<div class="col-md-6">
							<label for="phone-input1">@lang("$lang.Phone Number") *</label>
							<input class="form-control" type="number" name="phone" id="phone-input1"><br>
						</div>
						<div class="col-md-6">
							<label for="cell-phone-input1">@lang("$lang.Cell Phone Number") *</label>
							<input class="form-control" type="number" name="cell_phone" id="cell-phone-input1"><br>
						</div>

						<div class="col-md-6">
							<label for="email-input1">@lang("$lang.Email") *</label>
							<input class="form-control" type="text" name="email" id="email-input1"><br>
						</div>

						<div class="col-md-6">
							<label for="province-input1">@lang("$lang.Province")</label>
							<input class="form-control" type="text" name="province" id="province-input1"><br>
						</div>

						<div class="col-md-6">
							<label for="postal-input1">@lang("$lang.Postal Code")</label>
							<input class="form-control" type="text" name="postal" id="postal-input1"><br>
						</div>

						<div class="col-md-6">
							<label for="country-input1">@lang("$lang.Country") *</label>
							<select class="form-control" type="text" name="country_code" id="country-input1" style="width:100%;"></select><br>
						</div>

						<div class="col-md-6">
							<label for="line1-input1">@lang("$lang.Address Line") 1 *</label>
							<input class="form-control" type="text" name="line1" id="line1-input1"><br>
						</div>

						<div class="col-md-6">
							<label for="line2-input1">@lang("$lang.Address Line") 2</label>
							<input class="form-control" type="text" name="line2" id="line2-input1"><br>
						</div>

						<div class="col-md-6">
							<label for="line3-input1">@lang("$lang.Address Line") 3</label>
							<input class="form-control" type="text" name="line3" id="line3-input1"><br>
						</div>
					</div>

					<div class="col-4 offset-4">
						<div class="row">
							<div class="col-9 checked-items-details" >
								<span>
									@lang("$lang.Number of items")
								</span>
								<br>
								<span>
									@lang("$lang.Total Price") 
								</span>
							</div>
							<div class="col-3 checked-items-details">
								<span>
									{{$kit->itemsCount}}
								</span>
								<br>
								<span>
									<span>{{$kit->maxPrice}}</span> JOD
								</span>
							</div>

							<div class="col-12 note-div">
								@lang("$lang.Payment on Delivery")
							</div>
						</div>
					</div>	
				</div>
				<div class="col-12 purchase-button-div">
					<button class="btn purchase-btn main2-border-btn-dark w-100" onclick="confirmKit({{$kit->id}})" >@lang("$lang.Confirm Order")</button>
				</div>

		
	</div>
</div>