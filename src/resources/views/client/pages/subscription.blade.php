@extends('client.master')

@section('styles')
<style type="text/css">


@keyframes click-wave {
  0% {
    height: 22px;
    width: 22px;
    opacity: 0.35;
    position: relative;
  }

  100% {
    height: 200px;
    width: 200px;
    margin-left: -80px;
    margin-top: -80px;
    opacity: 0;
  }
}

	.price
	{
    	min-height: 1.4em;
	}
	.courses 
	{
		padding-top: 4.5em;
	}
	.paypal-img {
		cursor: pointer;
		margin-top: 9px;

	}
	.items-subsection {
		text-align: center ;
	}
	.visa-img {
		    width: 85px;
		    
			cursor: pointer;
	}
	
	footer .follow img {
	    height: 47px;
	}
	footer .title {
		text-align: center;
	}

	.clickable {
		border-radius: 10px;
    	box-shadow: 0 5px 20px rgba(160, 156, 156, 0.5);
    	    width: 141%;
    	    height: 40px;

	}
	.payment-section .visa-img{
		height:39px;
	}
	.visa-img-div{
		width: 119%;
	}
	section.home {
		min-height: 100vh;
		background: none;
	}
	@media (min-width: 576px) and (max-width: 1200px) {
		.paypal-img-div {
		    width: 167%;
		    margin-left: -30px;
		}
		.visa-img-div{
			width: 110%;
		}
		.clickable {
			height: 61px;
		}
		.paypal-img {
			margin-top: 15px;
		}
	}
	
</style>
@endsection

@section('title', 'Eureka')

@section('body-tag', 'home-page')

@section('body')
<?php $lang = App::getLocale() ?>
	<header class="all-nav">
		@include('client.partials.nav')
	</header>
	<div class="top-logo">
		<a href="{{route('home')}}">
			<img src="<?php echo config('app.url'); ?>/images/logo.png" alt="" width="100px">
		</a>
	</div>

	<section id="home" class="home" style="min-height: 120vh">
		<div class="home-content">
			<div class="container">
				<!--  login  -->
			<!-- 

				@if(!Auth::check())
				<div class="row login-section header-section-wrapper" id="login">
					<div class="login-title col-12">
						@lang("$lang.join the extreme adventure")
					</div>
					<div class="col-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
						<div class="header-section">
							<div class="container">
								<form method="POST" action="{{ route('login') }}">
                        		@csrf
                        		 <span class="wrong-details">
                        		 	@if (session('_previous') != '/register')
                        		 	
			                        	@if ($errors->has('name'))
                                    		<strong>{{ $errors->first('name') }} </strong><br>
                                    	@endif
                                    	@if ($errors->has('email'))
                                    	    <strong>{{ $errors->first('email') }}</strong><br>
                                    	@endif
                                    	@if ($errors->has('password'))
                                            <strong>{{ $errors->first('password') }}</strong><br>
                                		@endif
			                        @endif
			                        </span>
									<div class="form-group row">
										<label for="login-email" class="col-sm-4 col-form-label"><img class="icon" src="<?php echo config('app.url'); ?>/images/login_icons/username.png" height="25px"><span>@lang("$lang.Email")</span></label>
										<div class="col-sm-8">
											<input type="email" class="form-control" id="login-email" name="email">
										</div>
									</div>

									<div class="form-group row">
										<label for="login-password" class="col-sm-4 col-form-label"><img class="icon" src="<?php echo config('app.url'); ?>/images/login_icons/password.png" height="19px"><span>@lang("$lang.Password")</span></label>
										<div class="col-sm-8">
											<input type="password" class="form-control" id="login-password" name="password">
										</div>
									</div>
    		                        <div class="col-md-6 offset-md-4">
										<label>
	                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> @lang("$lang.Remember Me")
	                                    </label>
									</div>
									<div class="row">
										<div class="col-12 col-sm-6 offset-sm-3">
											<div class="text-center">
												<button type="submit" class="btn main1-white-btn submit" >@lang("$lang.LOGIN")</button>
											</div>
										</div>
									</div>
									

								</form>

								<div class="footer">
									<div class="row">
										<div class="col">
											<div class="link"><a href="{{route('try-lesson')}}">@lang("$lang.Try a free lesson")</a></div>
										</div>
										<div class="col">
											<div class="link hover"><a onclick="toRegister()">@lang("$lang.Create an account")</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--  register  ->
				<div class="row register-section header-section-wrapper d-none" id="register">
					<div class="col-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
						<div class="header-section">
							<div class="container">
								{{Form::open(['route' => 'register'])}}
			                        @csrf
			                        <span class="wrong-details">
                        		    	@if(session('_previous') == '/register')
				                        	@if ($errors->has('name'))
	                                    		<strong>{{ $errors->first('name') }}</strong><br>
	                                    	@endif

	                                    	@if ($errors->has('email'))
	                                    	    <strong>{{ $errors->first('email') }}</strong><br>
	                                    	@endif
	                                    	@if ($errors->has('password'))
	                                            <strong>{{ $errors->first('password') }}</strong><br>
	                                		@endif
                                		@endif
                                   </span>
									<div class="form-group row">
										<label for="name" class="col-sm-4 col-form-label">@lang("$lang.Username")</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" id="name" name="name" required>
										</div>
									</div>

									<div class="form-group row">
										<label for="password" class="col-sm-4 col-form-label">@lang("$lang.Password")</label>
										<div class="col-sm-8">
											<input type="password" class="form-control" id="password" name="password" required>
										</div>
									</div>

									<div class="form-group row">
										<label for="register-repsw" class="col-sm-4 col-form-label">@lang("$lang.Confirm Password")</label>
										<div class="col-sm-8">
											<input type="password" class="form-control" id="password-confirm" name="password_confirmation" required="required">
										</div>
									</div>

									<div class="form-group row">
										<label for="email" class="col-sm-4 col-form-label">@lang("$lang.Email")</label>
										<div class="col-sm-8">
											<input type="email" class="form-control" id="email" placeholder="Ex.janedoe@gmail.com" name="email" required>
										</div>
									</div>

									<div class="form-group row">
										<label for="phone" class="col-sm-4 col-form-label">@lang("$lang.Phone Number")</label>
										<div class="col-sm-8">
											<input type="number" class="form-control" id="phone" placeholder="+962" name="phone">
										</div>
									</div>

									<div class="form-group row">
										<label for="age" class="col-sm-4 col-form-label">@lang("$lang.Age")</label>
										<div class="col-sm-8">
											<input type="number" class="form-control" id="age" name="age">
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-6 offset-sm-3">
											<div class="text-center">
												{{ Form::submit(__("$lang.Sign Up") , ['class'=>'btn main1-white-btn submit']) }}
											</div>
										</div>
									</div>
									
								</form>
								<div class="footer">
									<div class="row">
										<div class="col">
											<div class="link">
												<a href="{{route('try-lesson')}}">@lang("$lang.Try a free lesson")</a>
											</div>
										</div>
										<div class="col">
											<div class="link hover">
												<a onclick="toLogin()">@lang("$lang.Already have an account")</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endif -->

				<!--  subscibe  -->
				<div class="row subscibe-section header-section-wrapper d-none" id="subscribe">
					@foreach($models as $model) 
					
					<div class="col-12 col-md-8 offset-md-2 col-lg-4 offset-lg-0 subscribtion">
						<div class="header-section">
							<div class="container subscription-model-modal"  data-id="{{$model->id}}" data-name="{{$model->name_en}}" data-price="{{$model->price}}" data-display-price="{{$model->display_price}}" data-full-access="{{$model->full_access == 1 ? __($lang.'.Unlimited') : __($lang.'.One Course')}}" data-period="{{ sprintf('%d' ,$model->period_in_days / 30) == 0 ? __($lang.'.Payment Before The Course') : __($lang.'.Payment every').sprintf('%d' , $model->period_in_days / 30) .' '.__($lang.'.months')}}" >
								<div class="title">{{$model->name_en}}</div>
								@if ($model->display_price != '')
								<div class="price"><span class="price-val" >{{$model->display_price}}</span></div>
								@else 
								<div class="price"><span class="" ></span></div>
								@endif
								<div class="details">
									<div class="row no-gutters">
										<div class="col-6">@lang("$lang.Number of Courses")</div>
										<div class="col-6 right" >{{$model->full_access == 1 ? __($lang.'.Unlimited') : __($lang.'.One Course')}}</div>
									</div>
									<div class="row no-gutters ">
										{{ sprintf('%d' ,$model->period_in_days / 30) == 0 ? __("$lang.Payment Before The Course") : __("$lang.Payment every")." ".sprintf('%d' , $model->period_in_days / 30) . ' '. __("$lang.months")}} 
									</div>
								</div>

								<div class="row">
									<div class="col-12 col-sm-8 offset-sm-2 text-center">
										@if($model->full_access == 1)
										<button type="submit" class="btn main2-border-btn submit w-100" onclick="gotoPayment(this);">
											@lang("$lang.Subscribe")
										</button>
										@else
										<a class="btn main2-border-btn submit w-100" href="#courses" >
											@lang("$lang.Browse Courses")
										</a>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
					@endforeach


				</div>
			</div>
		</div>
	</section>

@endsection

@section('scripts')
	<script src="{{URL::asset('js/client/index.js')}}"></script>	

	<script type="text/javascript">		
	toSubscribe();
	@if(session('_previous') == '/register')
		toRegister();
	@endif

	link = window.location.href ;
	if (link.search('#register') != -1 ){
		toRegister();
	}

	@if(session('payment_repsonse') == 'Success')
		swal('Success', 'Payment accepted Successfully' , 'success');
		@php
			session(['payment_repsonse'=>'']);
		@endphp
	@endif

	@if(session('payment_repsonse') == 'Success-COUPON MIRACLE')
		swal('Success', 'Subscription Created Successfully, Your coupon amount is greater than paymnet amount!' , 'success');
		@php
			session(['payment_repsonse'=>'']);
		@endphp
	@endif

	@if(session('payment_repsonse') == 'Coupon Error')
		swal('Success', 'Incorrect Coupon Code' , 'success');
		@php
			session(['payment_repsonse'=>'']);
		@endphp
	@endif


	@if(session('payment_repsonse') == 'Fail')
		swal({
		  type: 'error',
		  title: 'Oops...',
		  text: 'Payment not accepted, try again later'
		});
		@php
			session(['payment_repsonse'=>'']);
		@endphp
	@endif

	function submitContactUsForm(){
		name = $('#contact-name').val();
		email = $('#contact-email').val();
		message = $('#contact-message').val();
		subject =$('#contact-subject').val();
		flag1 =0 ;
		flag2 = 0;
		for(i=0 ;i < email.length ; i++){
			if (email[i] == '@'){ 

				flag1 =1;
			}

			if (email[i] == '.' && flag1 ==1 ){
				flag2 =1;
			}
		}
		if (flag1 && flag2){
	       	$.ajax({
	            type: "POST",
	            url: '/contact-us', 
	            dataType: "json",
	            data: {'name': name ,'email': email, 'message' : message , 'subject' : subject},
	            headers: {
	                "x-csrf-token": $("[name=_token]").val()
	            },
	        }).always(function(response){
	        	console.log(response.responseText);
	        	if(response.responseText == 'ok'){
		        	swal('Success', 'Email Sent Successfully, We will reply as soon as possible!' , 'success');
	        	}
	        	else {
	        		swal({
						type: 'error',
						title: 'Oops...',
						text: 'Something went wrong! , Try again later',
					});
	        	}
	        });
       }
       else {
       	swal({
		  type: 'error',
		  title: 'Oops...',
		  text: 'Enter a valid E-mail!'
		})
       }
	} 

	</script>
@endsection