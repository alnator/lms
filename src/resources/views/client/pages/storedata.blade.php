<?php $lang = App::getLocale() ?>
<div class="row">
        @foreach($items as $item)
        <div class="col-xl-4 col-lg-4 col-md-6">
            <div class="style-full style-half">
              <div class="main-pic"> 
                @if(isset($item->images))
                    <?php $p=1;?>
                    @foreach($item->images as $image)
                        <?php if(count($item->images)== $p){ ?>
                            @if(file_exists($image->img_src))
                            <img src="{{ $image->img_src }}" alt="">
                            @else
                                <img src="<?php echo config('app.url'); ?>/css/client/images/lessons-slider-pic2.png" alt="">
                            @endif
                        <?php }
                        $p++; ?>
                    @endforeach
                @else
                    <img src="<?php echo config('app.url'); ?>/css/client/images/lessons-slider-pic2.png" alt="">
                @endif
              </div>
              <div class="product-title">
                @if(session('lang') == 'ar')
                    <h4>{{ $item->items->name_en }}</h4>
                @else
                    <h4>{{ $item->items->name_en }}</h4>
                @endif
                <p>&nbsp;</p>
                @if($item->qty > 0)
                    <span>
                        <img src="<?php echo config('app.url'); ?>/css/client/images/right-icon.png" alt="">@lang("$lang.Available in store")
                    </span>
                @else
                    <span>
                        <img src="<?php echo config('app.url'); ?>/css/client/images/cross-icon.png" alt="">@lang("$lang.Not Available in store")
                    </span>
                @endif
                <div class="sub-unsub">
                  <p><span>{{ Helper::showCurrency($item->maxPrice) }}</span></p>
                  
                </div>
                @if($item->qty > 0)
                    <form method="POST" action="{{ route('addtocart') }}">
                    @csrf
                    <input type="hidden" name="itemid" value="{{ $item->id }}">
                    <input type="hidden" name="type" value="Item">
                    <input type="hidden" name="price" value="{{$item->maxPrice}}">
                    <input type="hidden" name="supplierId" value="{{$item->supplierId}}">
                    <input type="hidden" name="qty" value="1">
                    
                        <button type="submit" >@lang("$lang.Add to Cart")</button>
                    </form>
              @else
                <button disabled="disabled">@lang("$lang.Add to Cart")</button>
              @endif
              </div>        
            </div>
        </div>
        @endforeach
    </div>
    <div class="row">
      <div class="col-xl-12">
        <nav aria-label="Page navigation example justify-content-center">
          {!! $items->appends(['search' => request()->query('searchdata')])->links() !!}
          
        </nav>
      </div>
    </div>