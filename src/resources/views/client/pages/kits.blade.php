@extends('client.master')

@section('title', 'Eureka')

@section('body-tag', 'level-page')

@section('body')
@include('client.partials.linked-nav')
<?php $lang = App::getLocale() ?>

</div>
<section class="new-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">@lang("$lang.Store")</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $kitItem->name_en }}</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</section>
<section class="Robotics_kit">
  <div class="container">
    <div class="row">
      <div class="col-xl-5 col-lg-5 col-md-5">
        <div class="kitimg">

            @if(count($kitItem->images) > 0 )
                    <?php $p2=1;?>
                    @foreach($kitItem->images as $image)
                       <?php if(count($kitItem->images) == $p2){ ?>
                            @if(file_exists($image->img_src))
                                <img src="{{ $image->img_src }}" alt="">
                            @else
                                <img src="<?php echo config('app.url'); ?>/css/client/images/kit-1.jpg" alt="ter">
                            @endif
                        <?php }
                        $p2++; ?>
                    @endforeach
            @else
                <img src="<?php echo config('app.url'); ?>/css/client/images/kit-1.jpg" alt="">
            @endif
        </div>
      </div>
      <div class="col-xl-7 col-lg-7 col-md-7">
        <div class="kittext">
          <h2 class="comntitle">{{ $kitItem->name_en }}</h2>
            <span>
                @if($kitItem->active == '1' && $kitItem->available == '1')
                    <img src="<?php echo config('app.url'); ?>/css/client/images/right-icon.png" alt="">@lang("$lang.Available in store")
                @else
                    <img src="<?php echo config('app.url'); ?>/css/client/images/cross-icon.png" alt="">@lang("$lang.Not Available in store")
                @endif
                
            </span>
            <p> {{ $kitItem->description_en }}</p>
            <div class="sub-unsub">
            <p><span>{{ Helper::showCurrency($kitItem->maxPrice) }}</span></p>
            @if($kitItem->active == '1' && $kitItem->available == '1')
            <form method="POST" action="{{ route('addtocart') }}">
                    @csrf
                    <input type="hidden" name="itemid" value="{{ $kitItem->id }}">
                    <input type="hidden" name="type" value="Kit">
                    <input type="hidden" name="price" value="{{$kitItem->maxPrice}}">
                    <input type="hidden" name="supplierId" value="{{$kitItem->supplierId}}">
                    <input type="hidden" name="qty" value="1">
                    
                        <button type="submit" >@lang("$lang.Order now")</button>
                    </form>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>  
</section>
<section class="lessons-need-sliders Included_Items">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h5 class="comntitle">@lang("$lang.Included Items")</h5>
      </div>
    </div>
  </div>
  <div class="lessons-slider-area" style="padding: 0px !important;">
    <div class="col-lg-12">
      <div class="owl-carousel owl-theme" id="lessons-two">
        @foreach($items as $item)
        <div class="item">
            <div class="style-full style-half">
            <div class="main-pic">
                @if(isset($item->images))
                    <?php $p=1;?>
                    @foreach($item->images as $image)
                        <?php if(count($item->images)== $p){ ?>
                            @if(file_exists($image->img_src))
                            <img src="{{ $image->img_src }}" alt="">
                            @else
                                <img src="<?php echo config('app.url'); ?>/css/client/images/lessons-slider-pic2.png" alt="">
                            @endif
                        <?php }
                        $p++; ?>
                    @endforeach
                @else
                    <img src="<?php echo config('app.url'); ?>/css/client/images/lessons-slider-pic2.png" alt="">
                @endif 
                 
            </div>
            <div class="product-title">
                @if(session('lang') == 'ar')
                    <h4>{{ $item->items->name_en }}</h4>
                @else
                    <h4>{{ $item->items->name_en }}</h4>
                @endif
                <p>&nbsp;</p>
                @if($item->qty > 0)
                    <span>
                        <img src="<?php echo config('app.url'); ?>/css/client/images/right-icon.png" alt="">@lang("$lang.Available in store")
                    </span>
                @else
                    <span>
                        <img src="<?php echo config('app.url'); ?>/css/client/images/cross-icon.png" alt="">@lang("$lang.Not Available in store")
                    </span>
                @endif
                
              
                <div class="sub-unsub">
                    <p><span>{{ Helper::showCurrency($item->maxPrice) }}</span></p>
                </div>
            </div>
            @if($item->qty > 0)
                    <form method="POST" action="{{ route('addtocart') }}">
                    @csrf
                    <input type="hidden" name="itemid" value="{{ $item->id }}">
                    <input type="hidden" name="type" value="Item">
                    <input type="hidden" name="price" value="{{$item->maxPrice}}">
                    <input type="hidden" name="supplierId" value="{{$item->supplierId}}">
                    <input type="hidden" name="qty" value="1">
                    
                        <button type="submit" >@lang("$lang.Order now")</button>
                    </form>
              @else
                <button disabled="disabled">@lang("$lang.Order now")</button>
              @endif
            </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</section>
<section class="Related_items">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <h2 class="comntitle">@lang("$lang.Related Items")</h2>
      </div>
    </div>
  </div>
  <div class="col-lg-12">
  <div class="owl-carousel" id="Related_items" style="padding: 0px !important;">
     @foreach($kits as $kit)
    <div class="item">
      <div class="style-full style-half">
        <div class="main-pic"> 
            @if(isset($kit->images))
                    <?php $p1=1;?>
                    @foreach($kit->images as $image)
                        <?php if(count($kit->images)== $p1){ ?>
                            @if(file_exists($image->img_src))
                                <img src="{{ $image->img_src }}" alt="">
                            @else
                                <img src="<?php echo config('app.url'); ?>/css/client/images/kit-1.jpg" alt="">
                            @endif
                        <?php }
                        $p1++; ?>
                    @endforeach
            @else
                <img src="<?php echo config('app.url'); ?>/css/client/images/kit-1.jpg" alt="">
            @endif
           
        </div>
        <div class="product-title">
          <h4>{{ $kit->name_en }} </h4>
          <p>{{ $kit->itemsCount }} @lang("$lang.Number of Items")</p>
            <span>
                @if($kit->active == '1' && $kit->available == '1')
                    <img src="<?php echo config('app.url'); ?>/css/client/images/right-icon.png" alt="">@lang("$lang.Available in store")
                @else
                    <img src="<?php echo config('app.url'); ?>/css/client/images/cross-icon.png" alt="">@lang("$lang.Not Available in store")
                @endif
            </span>
          <div class="sub-unsub">
            <p><span>{{ Helper::showCurrency($kit->maxPrice) }}</span></p>
          </div>
            <button onclick="location.href='{{ route('kit-page' , $kit->id) }}'"> @lang("$lang.Continue")</button>
            
        </div>
        
      </div>
    </div>
    @endforeach
   
  </div>
</div>
</section>
<?php /*<section class="about_eureka inner-development">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">


      

<div id="courses" class="courses" style="min-height: 120vh">
        <!-- <div class="home-content"> -->
            <div class="kits-container">

                <!-- Customize Kits -->
                <div class="row" id="items-card" style="display: none" >
                    <div class="col-12" role="document">
                        <div class="card-content">
                            <div class="card-header" >
                                <div class="col-12 card-header-col" >
                                    <h3 class="card-heading">@lang("$lang.Customize Kit")</h3>
                                    <label for="items-filter" style="color: #fff">@lang("$lang.filter")</label>
                                    <input type="text" class="form-control" id="items-filter"placeholder='@lang("$lang.type here and hit Enter")!'>
                                </div>
                            </div>
                            <div class="card-body items-card-body">
                                <div class="row  d-flex justify-content-center" id="items-div-filter">
                                    @foreach($items as $item)
                                        <div class="col row item">
                                            <div class="col-10 offset-1 class custom-item-section" >
                                                <div class="col-12 items-subsection ">
                                                    <h4>{{$item->name_en}}</h4>
                                                </div>
                                                <div class="col-12 img-col">
                                                    <center>
                                                        <img src="{{isset($item->images[0]->img_src) ?  $item->images[0]->img_src : ''}}" width="200" height="230">
                                                    </center>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-6 item-details" style=" text-align: left;">
                                                        <span>
                                                        @lang("$lang.Item Price")
                                                        </span>
                                                        <br>
                                                        <span>
                                                        @lang("$lang.Add to kit")
                                                        </span>
                                                        <br><br>
                                                        <span>
                                                        @lang("$lang.Quantity")
                                                        </span>
                                                    </div>
                                                    <div class="col-6 item-details" style="text-align: right;">
                                                        @if ($item->maxPrice['MaxAmount'] !=  -1)
                                                        <span>
                                                            {{$item->maxPrice['MaxAmount'] }} $ 
                                                        </span>
                                                        <br>
                                                        <span class="add-kit-span">
                                                          </label>
                                                          <input type="checkbox" data-id="{{$item->id}}" data-price="{{$item->maxPrice['MaxAmount']}}" class="option-input checkbox" />
                                                          </label>
                                                        </span>
                                                        <span>
                                                            <input type="number" value="0" data-price="{{$item->maxPrice['MaxAmount']}}" data-id="{{$item->id}}" class="qty-input" style="width: 63px;margin-top: 20px;" >
                                                        </span>
                                                        @else 
                                                        <span>
                                                            @lang("$lang.Unavailable") 
                                                        </span>
                                                        <br>
                                                        <span class="add-kit-span">
                                                          </label>
                                                            @lang("$lang.Unavailable")
                                                          </label>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach 
                                </div>
                                <div class="col-12 modal-footer-details" >
                                    <hr style="margin-top: 40px">
                                    
                                    <div class="col-12" style="color: #fff">
                                        <div class="row">
                                            <div class="col-12">
                                                <center>
                                                    <h2>@lang("$lang.Shipping Information")</h2>
                                                </center>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="city-input">@lang("$lang.City") *</label>
                                                <input class="form-control" type="text" name="city" id="city-input"><br>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="name-input">@lang("$lang.Name") *</label>
                                                <input class="form-control" type="text" name="name" id="name-input"><br>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="phone-input">@lang("$lang.Phone Number") *</label>
                                                <input class="form-control" type="number" name="phone" id="phone-input"><br>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="cell-phone-input">@lang("$lang.Cell Phone Number") *</label>
                                                <input class="form-control" type="text" name="cell_phone" id="cell-phone-input"><br>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="email-input">@lang("$lang.Email") *</label>
                                                <input class="form-control" type="text" name="email" id="email-input"><br>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="province-input">@lang("$lang.Province")</label>
                                                <input class="form-control" type="text" name="province" id="province-input"><br>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="postal-input">@lang("$lang.Postal Code")</label>
                                                <input class="form-control" type="text" name="postal" id="postal-input"><br>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="country-input">@lang("$lang.Country") *</label>
                                                <select class="form-control" type="text" name="country_code" id="country-input" style="width:100%;"></select><br>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="line1-input">@lang("$lang.Address Line") 1 *</label>
                                                <input class="form-control" type="text" name="line1" id="line1-input"><br>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="line2-input">@lang("$lang.Address Line") 2</label>
                                                <input class="form-control" type="text" name="line2" id="line2-input"><br>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="line3-input">@lang("$lang.Address Line") 3</label>
                                                <input class="form-control" type="text" name="line3" id="line3-input"><br>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-4 offset-4">
                                        <div class="row">
                                            <div class="col-9 checked-items-details" >
                                                <span>
                                                    @lang("$lang.Number of items")
                                                </span>
                                                <br>
                                                <span>
                                                    @lang("$lang.Total Price") 
                                                </span>
                                            </div>
                                            <div class="col-3 checked-items-details">
                                                <span id="items-count">
                                                    0
                                                </span>
                                                <br>
                                                <span>
                                                    <span id="items-price">0</span> $
                                                </span>
                                            </div>

                                            <div class="col-12 note-div">
                                                @lang("$lang.Payment on Delivery")
                                            </div>
                                        </div>                      
                                    </div>
                                    <div class="col-md-4 offset-4">                 
                                        <div class="col-12 purchase-button-div">
                                            <button class="btn purchase-btn main2-border-btn-dark w-100" id="confirm-custom-kit">@lang("$lang.Confirm Order")</button>
                                            <button class="btn purchase-btn main2-border-btn-dark w-100 toggle" style="margin-top: 1em">@lang("$lang.Cancel")</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <!-- Kits -->
            <div class="row " id="kits-modal" >
                <div class="col-12" role="document">
                    <div class="row">

                    <div class="col-12 col-sm-12  col-md-4 row kit-container ">
                        <div class="col-12 modal-content kits-modal">
                            <div class="modal-header">
                                    <h4 class="modal-heading"> @lang("$lang.Customize Kit")</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row no-gutters kit-details">
                                    <div class="col-6">@lang("$lang.Number of Items")</div>
                                    <div class="col-6 right">@lang("$lang.Unlimited")</div>
                                </div>
                                <div class="row no-gutters kit-details">
                                    <div class="col-6">@lang("$lang.Price") </div>
                                    <div class="col-6 right">@lang("$lang.Depends on Items")</div>
                                </div>
                            </div>
                            <div class="modal-footer row">
                                <div class="col-12 purchase-button-div">
                                    <button class="btn purchase-btn main2-border-btn-dark w-100 toggle" >@lang("$lang.Customize Kit")</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @foreach ($kits as $kit)
                    <div class="col-12 col-sm-12 col-md-4  row kit-container ">
                        <div class="col-12 modal-content kits-modal">
                            <div class="modal-header">
                                    <h4 class="modal-heading">{{$kit->name_en}}</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row no-gutters kit-details">
                                    <div class="col-6">@lang("$lang.Number of Items")</div>
                                    <div class="col-6 right">{{$kit->itemsCount}}</div>
                                </div>
                                <div class="row no-gutters kit-details">
                                    <div class="col-6">@lang("$lang.Price") </div>
                                    <div class="col-6 right">{{$kit->maxPrice >= 0 ?$kit->maxPrice : 'Unavailable'}}$</div>
                                </div>
                            </div>
                            <div class="modal-footer row">
                                <div class="col-12 col-lg-12 purchase-button-div">
                                    <button class="btn purchase-btn main2-border-btn-dark w-100" onclick="loadKitItems({{$kit->id}})">@lang("$lang.View Kit")</button>
                                </div>
                                <!-- <div class="col-12 col-lg-6  purchase-button-div">
                                    <button class="btn purchase-btn main2-border-btn-dark w-100" style="border-color: #fff;">@lang("$lang.Purhcase")</button>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                    </div>

                </div>
            </div>

            <div class="modal fade row" id="kit-details-modal" tabindex="-1" role="dialog" aria-labelledby="items" style="display: none" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-xl col-12" role="document" >
                    <div class="modal-content" id="items-modal-body">
                        
                    </div>
                </div>
            </div>


            <!-- /Contatiner --> 
            <!-- </div> -->
        </div>
    </div>
    </div>
    </div>
</div>
</section>
*/ ?>
@endsection


@section('scripts')

    <script type="text/javascript">
            var kitsFlag = 0;

            function toggleView(){
                if (kitsFlag == 0){
                    $('#items-card').fadeOut();
                    $('#kits-modal').fadeIn();
                    kitsFlag =1 ;
                }
                else {
                    $('#kits-modal').fadeOut();
                    $('#items-card').fadeIn();    
                    kitsFlag =0 ;
                }
            }
            $('.toggle').click(function(){
                toggleView();
            });

            @if(isset($kitItem))
                loadKitItems({{$kitItem->id}})
            @endif
    </script>
@if($lang=='ar')
<script>
  $('#lessons-two').owlCarousel({
  loop:false,
  center: false,
  margin: 30,
  nav: true,
  rtl:true,
  dots:false,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
    768: {
      items:2
    },
    1000: {
      items:3
    },
    1100: {
      items: 3
    }
  }
})
</script>
@else    
<script>
  $('#lessons-two').owlCarousel({
  loop:false,
  center: false,
  margin: 30,
  nav: true,
  dots:false,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
    768: {
      items:2
    },
    1000: {
      items:3
    },
    1100: {
      items: 3
    }
  }
})
</script>
@endif
@endsection