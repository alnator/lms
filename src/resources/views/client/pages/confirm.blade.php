@extends('client.master')

@section('title', 'Eureka')

@section('body-tag', 'shoppingcart-page')

@section('body')
@include('client.partials.linked-nav')
<?php $lang = App::getLocale() ?>


<section class="new-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">@lang("$lang.Home")</a></li>
            <li class="breadcrumb-item active" aria-current="page">@lang("$lang.Checkout")</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</section>
<section class="lessons-need-sliders Shopping_Cart">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <h1 class="comntitle">@lang("$lang.Checkout")</h1>
      </div>
    </div>

    <div class="row">
      <div class="col-xl-5 col-lg-5 col-md-6">
        <?php  //dd($orderitem); ?>


        @foreach($orderitem as $items)
  

          <div class="style-full style-half">
            <div class="main-pic"> 
              @if(isset($items->imagepath))
                <img src="{{url($items->imagepath)}}" alt=""> 
              @endif  
            </div>
            <div class="product-title">
              <img src="images/cross-icon.png" class="cross" alt=""/>
              <h4>{{$items->itemname}}</h4>
              <p>{{$items->qty}} items</p>
             
              <div class="sub-unsub">               
                <p>&nbsp;<span>{{ Helper::showCurrency($items->price) }}</span></p>



              </div>
             
            </div>
          </div>
    @endforeach

      </div>
      <div class="col-xl-7 col-lg-7 col-md-6">
        <div class="address_details">
          <aside><strong>@lang("$lang.Total")  : </strong> <span>{{ Helper::showCurrency($order->total_price) }}</span></aside>
          <hr>
         
          <h4>@lang("$lang.Contact information"):</h4>
          <div class="row">
            <div class="col-xl-12">
              <p>{{$order->user->name}} ( {{$order->user->email}} )</p>
              
              <p>{{$order->phone}}</p>
              <p>{{$order->phone_number}}</p>
            
              <hr>
            </div>            
          </div>

           <h4>@lang("$lang.Delivery address"):</h4>
          <div class="row">
            <div class="col-xl-12">
              <p>{{$order->line1}} , {{$order->line2}} {{$order->line3}}</p>
              <p>{{$order->city}}</p>
              <p>{{$order->province}}</p>
              <p>{{$order->postal_code}}</p>
              <p>{{$order->country_code}}</p>
              <hr>
            </div>            
          </div>


         
         
          <h4>@lang("$lang.Payment Method") :</h4>
          <div class="row">
            <div class="col-xl-12">
              <p>{{ $order->payment_method }}</p>
            </div>
          </div>
            @if(isset($order->coupon_amount) && $order->coupon_amount > 0)
            <hr>
            <h4>@lang("$lang.Discount"):</h4>
            <div class="row">
            <div class="col-xl-12">
              <p>{{ Helper::showCurrency($order->coupon_amount) }}</p>
             
            </div>
          </div>
            @endif

            <div class="row">
            <div class="col-xl-12 text-center">

              @if($order->payment_method == 'cod')
               {!! Form::open(['route'=>'sts-payment' , 'id'=>'sts']) !!}
                      <input type="hidden" name="coupon" id="coupon-field">
                      <input type="hidden" name="pmode" id="pmode" value="cod">
                      <input type="hidden" name="model_id" id="model_id_form_fort" value="5">
                      <input type="hidden" name="course_id" id="course_id_form_fort" value="{{ $order->id }}">
                      <button onclick="$('#sts').submit()">@lang("$lang.Confirm")</button> 
                      {!! Form::close() !!}
              @else

                      {!! Form::open(['route'=>'sts-payment' , 'id'=>'sts']) !!}
                      <input type="hidden" name="coupon" id="coupon-field">
                      <input type="hidden" name="pmode" id="pmode" value="card">
                      <input type="hidden" name="model_id" id="model_id_form_fort" value="5">
                      <input type="hidden" name="course_id" id="course_id_form_fort" value="{{ $order->id }}">
                      <button onclick="$('#sts').submit()">@lang("$lang.Pay Now")</button> 
                      {!! Form::close() !!}


              @endif        
          </div>
        </div>

      </div>
    </div>
  </div>
    

      
   
  </div>
    
</section>

<style type="text/css">
  .address_details form#sts button{bottom: -47px;}
</style>
@endsection
@section('scripts')

@endsection