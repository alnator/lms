@extends('client.master')

@section('title', 'Eureka')

@section('body-tag', 'level-page')

@section('body')
@include('client.partials.linked-nav')
<?php $lang = App::getLocale() ?>


<section class="new-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">@lang("$lang.Courses")</a></li>
            <li class="breadcrumb-item active" aria-current="page">@lang("$lang.Free Classes")</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</section>
	<section class="inner-banner"> <img src="<?php echo config('app.url'); ?>/public{{$bannerimages->image_path}}" alt=""> </section>



<section class="about_eureka inner-development">
  <div class="container">
    
  

   <?php // dd($freeLessons); ?>


    <div class="topic-chapter">
      <div class="row">
        <div class="col-md-12">
          <h2>@lang("$lang.Free Classes")</h2>
          <div class="sessions-lessons">
			
						<ul class="list-unstyled course-list">
            				
								
								@foreach($course as $video)
									<li>
						              <div class="chapter-img">
						              	@if(isset($video->image_path) && $video->image_path!="")
						               <img src=" <?php echo config('app.url'); ?>/public{{$video->image_path}}" alt="">
						               @else
						               
						               <img src="{{ Helper::showvimeo($video->url_identifier) }}" alt="">
						               @endif



						                </div>
						              <div class="chapter-detail">
						                <div class="capter-title">
						                  <h5><a class="lesson-name" href="{{$video->free ? route('free-lesson',$video->id) : route('lesson',$video->id)  }}">
															{{$video->name_en}} - {{$video->course->title_en}} 
														</a></h5>
						                  
										
																


								


						               

						                  

						                  
						                  <span>{{intval($video->estimated_time / 60)}}:{{$video->estimated_time % 60 < 10 ? '0'.$video->estimated_time % 60  : $video->estimated_time % 60 }}</span>

						                  		 


						                   </div>
						                <p>{{ $video->description_en }} </p>

						              


						              </div>
						            </li>
								@endforeach
						</ul>
						
						
						</div>
					</div>
          
          
        </div>
      </div>
    </div> 
</div>
</section>




@endsection
