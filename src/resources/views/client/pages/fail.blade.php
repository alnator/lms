@extends('client.master')

@section('title', 'Eureka')

@section('body-tag', 'shoppingcart-page')

@section('body')
@include('client.partials.linked-nav')
<?php $lang = App::getLocale() ?>


<section class="new-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="#">@lang("$lang.Courses")</a></li>
            <li class="breadcrumb-item active" aria-current="page">@lang("$lang.Shopping Cart")</li> -->
          </ol>
        </nav>
      </div>
    </div>
  </div>
</section>
<section class="lessons-need-sliders Shopping_Cart">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <h1 class="comntitle">@lang("$lang.Order")</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-12">
       
        <h4 class="comntitle">@lang("$lang.Payment Canceled. Order Id") :#{{ $order->id }}</h4>
      </div>
    </div>
    
</section>


@endsection
@section('scripts')
 







@endsection