@extends('client.master')


@section('title', 'Eureka')


@section('body-tag' , '404')
@section('body')

@include('client.partials.linked-nav')


<?php $lang= App::getLocale() ?>

<section class="new-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="#">Courses</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">404</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</section>




                    
                




<section class="user_profile"> 
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <h1>Kit not found</h1>
            </div>
        </div>
        <div class="row">
        
            <div class="col-xl-12 col-lg-12 col-md-12 ">
                  <a href="{{ url('/stores') }}">Back to Store.</a> 
            </div>
        </div>
    </div>
</section>










                    

@endsection
@section ('scripts')

<script type="text/javascript">



</script>


@endsection

