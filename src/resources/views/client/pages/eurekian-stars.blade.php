@extends('client.master')
@section ('title', 'Eurekian Stars')
@section('body')
<header class="all-nav">
		@include('client.partials.linked-nav')
	</header>
	<div class="top-logo">
		<a href="{{route('home')}}">
			<img src="<?php echo config('app.url'); ?>/images/logo.png" alt="" width="100px">
		</a>
	</div>

	<section id="stars" class="stars">
		<div class="container">
			<div class="title"><span class="line">Eurek</span>ian Stars</div>
			<div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat commodi, qui maxime in tempora sit! Unde cupiditate ad, iusto laborum et, odio minima repellat nobis quibusdam voluptate laudantium officiis. Facere.</div>
			<div class="lg-sc-imgs d-none d-xl-block">
			<div class="star-img-wrapper" style="top: 70px; left: 50%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[0]) ? $stars[0]: '/images/logo.png')}}">
			</div>
			<div class="star-img-wrapper"  style="top: 50px; left: 78%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[1]) ? $stars[1]: '/images/logo.png')}}">
			</div>
			<div class="star-img-wrapper"  style="top: 165px; left: 65%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[2]) ? $stars[2]: '/images/logo.png')}}">
			</div>
			<div class="star-img-wrapper"  style="top: 200px; left: 33%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[3]) ? $stars[3]: '/images/logo.png')}}">
			</div>
			<div class="star-img-wrapper"  style="top: 240px; left: 48%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[4]) ? $stars[4]: '/images/logo.png')}}">
			</div>
			<div class="star-img-wrapper"  style="top: 350px; left: 64%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[5]) ? $stars[5]: '/images/logo.png')}}">

			</div>
			<div class="star-img-wrapper"  style="top: 260px; left: 77%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[6]) ? $stars[6]: '/images/logo.png')}}">
			</div>
			<div class="star-img-wrapper"  style="top: 300px; left: 20%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[7]) ? $stars[7]: '/images/logo.png')}}">
			</div>
			<div class="star-img-wrapper"  style="top: 390px; left: 40%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[8]) ? $stars[8]: '/images/logo.png')}}">
			</div>
			</div>
			<div class="sm-sc-imgs d-xl-none">
			<div class="owl-carousel owl-theme">
				<div class="item">
					<div class="star-img-wrapper">
						<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[0]) ? $stars[0]: '/images/logo.png')}}">
					</div>
				</div>
				<div class="item">
					<div class="star-img-wrapper">
						<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[1]) ? $stars[1]: '/images/logo.png')}}">
					</div>
				</div>
				<div class="item">
					<div class="star-img-wrapper">
						<img class="star-img img-fluid" src="{{(isset($stars[2]) ? $stars[2]: '/images/logo.png')}}">
					</div>
				</div>
				<div class="item">
					<div class="star-img-wrapper">
						<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[3]) ? $stars[3]: '/images/logo.png')}}">
					</div>
				</div>
				<div class="item">
					<div class="star-img-wrapper">
						<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[4]) ? $stars[4]: '/images/logo.png')}}">
					</div>
				</div>
			</div>

			</div>
		</div>
	</section>


	
	@include ('client.partials.footerbar')
@endsection
