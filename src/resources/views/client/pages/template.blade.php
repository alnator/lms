@extends('client.master')

@section('title', 'Eureka')

@section('body-tag', 'home-page')

@section('body')

	@include('client.partials.linked-nav')

	<?php $lang = App::getLocale() ?>

	<section class="new-breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="{{route('home')}}">@lang("$lang.Home")</a></li>
							<li class="breadcrumb-item active" aria-current="page">{{$title_en}}</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</section>


	<section class="inner-banner"> <img src="<?php  echo isset($bannerimages->image_path) ? config('app.url').'/public'.$bannerimages->image_path : ''; ?>" alt=""> </section>

	<section class="about_eureka inner-development eureka-static-page">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					{!! $body !!}
				</div>
			</div>
		</div>
	</section>

@endsection
@section ('scripts')
	<script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>

	<script type="text/javascript">
		function submitContactUsForm(){
			name = $('#contact-name').val();
			email = $('#contact-email').val();
			message = $('#contact-message').val();
			subject =$('#contact-subject').val();
			flag1 =0 ;
			flag2 = 0;
			for(i=0 ;i < email.length ; i++){
				if (email[i] == '@'){

					flag1 =1;
				}

				if (email[i] == '.' && flag1 ==1 ){
					flag2 =1;
				}
			}
			if (flag1 && flag2){
				$.ajax({
					type: "POST",
					url: '/contact-us',
					dataType: "json",
					data: {'name': name ,'email': email, 'message' : message , 'subject' : subject},
					headers: {
						"x-csrf-token": $("[name=_token]").val()
					},
				}).always(function(response){
					console.log(response.responseText);
					if(response.responseText == 'ok'){
						swal('Success', 'Email Sent Successfully, We will reply as soon as possible!' , 'success');
					}
					else {
						swal({
							type: 'error',
							title: 'Oops...',
							text: 'Something went wrong! , Try again later',
						});
					}
				});
			}
			else {
				swal({
					type: 'error',
					title: 'Oops...',
					text: 'Enter A valid Email!'
				})
			}
		}
		document.body.style.zoom = 0.8
	</script>

@endsection