@extends('client.master')

@section('title', 'Eureka')

@section('body-tag', 'shoppingcart-page')

@section('body')
@include('client.partials.linked-nav')
<?php $lang = App::getLocale() ?>


<section class="notification">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="comntitle">@lang("$lang.Contact Us")</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-4 col-lg-4 col-md-4">
        <div class="contactbox">
          <div class="imgdiv">
            <img src="<?php echo config('app.url'); ?>/css/client/images/call.png" alt="">
          </div>
          <span>@lang("$lang.Phone")</span>
          <a href="tel:{{$footer->number1}}">{{$footer->number1}}</a>
          <a href="tel:{{$footer->number2}}">{{$footer->number2}}</a>
        </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-4">
        <div class="contactbox">
          <div class="imgdiv">
            <img src="<?php echo config('app.url'); ?>/css/client/images/mail.png" alt="">
          </div>
          <span>@lang("$lang.Email")</span>
          <a href="mailto:{{$footer->email}}">{{$footer->email}}</a>
        </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-4">
        <div class="contactbox">
          <div class="imgdiv">
            <img src="<?php echo config('app.url'); ?>/css/client/images/call.png" alt="">
          </div>
          <span>@lang("$lang.Social Media")</span>
          <div class="contactsocial">
            <a href="{{$footer->facebook}}" class="fa fa-facebook"></a>
            <a href="{{$footer->instagram}}" class="fa fa-instagram"></a>
            <a href="{{$footer->twitter}}" class="fa fa-twitter"></a>
            <!-- <a href="javascript:void" class="fa fa-linkedin"></a> -->
            <a href="{{$footer->youtube}}" class="fa fa-youtube-play"></a>
          </div>
        </div>
      </div>
    </div>
    <div class="row align-items-center justify-content-between get_touch">
      <div class="col-xl-12"><h1 class="comntitle">@lang("Get in touch")</h1></div>
      <div class="col-xl-5 col-lg-6 col-md-6">
        <div class="gettouchform">            
			@if(session()->has('message'))
			    <div class="alert alert-success" id="successMessage">
			        {{ session()->get('message') }}
			    </div>
			@endif		 
            <form id="form1" method="POST" action="{{ route('contactUs') }}">
            	@csrf
            	<input type="hidden" name="subject" value="Contact">
              <div class="form-group">
                <label for="exampleInputEmail1">@lang("$lang.Full name")</label>
                <input type="text" class="form-control" name="name" id="contactname" aria-describedby="emailHelp" >
                
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">@lang("$lang.Email Address")</label>
                <input type="email" class="form-control" name="email" id="contactemail" aria-describedby="emailHelp" >
                
              </div>
              <!-- <div class="form-group form-row">
                <div class="col-lg-12">
                  <label for="exampleInputEmail1">Country</label>
                  <select>
                    <option>Select City</option>
                    <option>City 1</option>
                    <option>City 2</option>
                    <option>City 3</option>
                    <option>City 4</option>
                  </select>
                </div>
              </div> -->
              <!-- <div class="form-group form-row">
                <div class="col-lg-12">
                  <label for="exampleInputEmail1">Message Type</label>
                  <select>
                    <option>Select City</option>
                    <option>City 1</option>
                    <option>City 2</option>
                    <option>City 3</option>
                    <option>City 4</option>
                  </select>
                </div>
              </div> -->
              <div class="form-group">
                <label for="exampleInputEmail1">@lang("$lang.Message Type")</label>
                <textarea type="text" name="message" id="contactmessage"></textarea>
                
              </div>
              <button  id="btn" class="btn btn-primary">@lang("$lang.Send")</button>
            </form>
        </div>
      </div>
      <div class="col-lg-6 col-md-6"> <img src="<?php echo config('app.url'); ?>/css/client/images/login-pic.jpg" class="d-none d-md-block" alt=""> </div>
    </div>
  </div>
</section>


@endsection
@section('scripts')
<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
 <script type="text/javascript">
$("#form1").validate({
    rules: {
        name: "required",
        email: {
		      required: true,
		      email: true
		},
		message:"required"
    },
    messages: {
        name: "Please Enter Full Name",
        email: "Please Enter Valid Email",
	    message: "Please Enter Message",

    }
});

  /*
onclick="submitContactUsForm1();"
  function submitContactUsForm1(){
    var appurl = "<?php echo config('app.url'); ?>";
    name = $('#contactname').val();
    email = $('#contactemail').val();
    message = $('#contactmessage').val();
    subject ="Contact";
    flag1 =0 ;
    flag2 = 0;

    
		    for(i=0 ;i < email.length ; i++){
		      if (email[i] == '@'){ 

		        flag1 =1;
		      }

		      if (email[i] == '.' && flag1 ==1 ){
		        flag2 =1;
		      }
		    }
    if (flag1 && flag2){
				$.ajax({
					type: "POST",
					url: appurl+'/contact-us', 
					dataType: "json",
					data: {'name': name ,'email': email, 'message' : message , 'subject' : subject},
					headers: {
					"x-csrf-token": $("[name=_token]").val()
					},
				}).always(function(response){
					console.log(response.responseText);
					if(response.responseText == 'ok'){
					swal('Success', 'Email Sent Successfully, We will reply as soon as possible!' , 'success');
				}
				else {
					swal({
					type: 'error',
					title: 'Oops...',
					text: 'Something went wrong! , Try again later',
					});
				}
				});
       }
       else {
		    swal({
		      type: 'error',
		      title: 'Oops...',
		      text: 'Enter a valid E-mail!'
		    })
       }

      
  }*/
  setTimeout(function() {
  $('#successMessage').fadeOut('slow');
}, 30000);
  </script>







@endsection