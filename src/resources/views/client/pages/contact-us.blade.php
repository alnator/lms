@extends('client.master')
@section('title', 'Contact Us')
@section ('styles')
 <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection
@section('body')
<header class="all-nav">
		@include('client.partials.linked-nav')
	</header>
	<div class="top-logo">
		<a href="{{route('home')}}">
			<img src="<?php echo config('app.url'); ?>/images/logo.png" alt="" width="100px">
		</a>
	</div>
<section id="contact" class="contact">
	<div class="container-fluid p-0">
		<div class="row no-gutters">
			<div class="col-lg-8 col-xl-7">
				<div class="images-wrapper">
					<div class="row no-gutters">
						<div class="col-sm-6">
							<div class="row no-gutters">
								<div class="col-6 col-sm-6"><img class="img-fluid" src="<?php echo config('app.url'); ?>/images/last_section_images/1st_image.png" style="padding-right: 3px;padding-bottom: 3px;"></div>
								<div class="col-6 col-sm-6"><img class="img-fluid" src="<?php echo config('app.url'); ?>/images/last_section_images/2nd_image.png" style="padding-right: 3px;padding-bottom: 3px;"></div>
								<div class="col-sm-12"><img class="img-fluid" src="<?php echo config('app.url'); ?>/images/last_section_images/4th_image.png" style="padding-right: 3px;padding-top: 3px;"></div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="row no-gutters">
								<div class="col-sm-12"><img class="img-fluid" src="<?php echo config('app.url'); ?>/images/last_section_images/3rd_image.png" style="padding-left: 3px;padding-bottom: 3px;"></div>
								<div class="col-6 col-sm-6"><img class="img-fluid" src="<?php echo config('app.url'); ?>/images/last_section_images/5th_image.png" style="padding-left: 3px;padding-top: 3px;"></div>
								<div class="col-6 col-sm-6"><img class="img-fluid" src="<?php echo config('app.url'); ?>/images/last_section_images/6th_image.png" style="padding-left: 3px;padding-top: 3px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-xl-5">
				<div class="contact-wrapper">
					<div class="title"><span class="line">Cont</span>act Us</div>
					<form id="contact-form" action="#">
						<div class="form-group row">
							<label for="contact-name" class="col-md-2 col-lg-3 col-form-label">Name</label>
							<div class="col-md-10 col-lg-9">
								<input type="text" class="form-control"  id="contact-name" name="name">
							</div>
						</div>
						<div class="form-group row">
							<label for="contact-phone" class="col-md-2 col-lg-3 col-form-label" >E-Mail</label>
							<div class="col-md-10 col-lg-9">
								<input type="email" class="form-control" id="contact-email" name="email">
							</div>
						</div>
						<div class="form-group row">
							<label for="contact-subject" class="col-md-2 col-lg-3 col-form-label" >Subject</label>
							<div class="col-md-10 col-lg-9">
								<input type="text" class="form-control"  id="contact-subject" name="subject">
							</div>
						</div>
						<div class="form-group row">
<!--
							<label for="contact-msg" class="col-md-3 col-lg-4 col-form-label">Message</label>
							<div class="col-md-9 col-lg-8">
								<input type="text" class="form-control" id="contact-msg" name="message">
							</div>
-->
				<div class="col-12"><textarea class="form-control mt-3" name="message" rows="2" id="contact-message" placeholder="Your message..."></textarea></div>
					
						</div>
					</form>
					<div class="row">
						<div class="col-md-4 offset-md-8">
							<button type="submit" onclick="submitContactUsForm();" class="btn white-border-btn submit w-100 mt-2">Send</button>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
@include ('client.partials.footerbar')
@endsection
@section ('scripts')
    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>

	<script type="text/javascript">
		function submitContactUsForm(){
		name = $('#contact-name').val();
		email = $('#contact-email').val();
		message = $('#contact-message').val();
		subject =$('#contact-subject').val();
		flag1 =0 ;
		flag2 = 0;
		for(i=0 ;i < email.length ; i++){
			if (email[i] == '@'){ 

				flag1 =1;
			}

			if (email[i] == '.' && flag1 ==1 ){
				flag2 =1;
			}
		}
		if (flag1 && flag2){
	       	$.ajax({
	            type: "POST",
	            url: '/contact-us', 
	            dataType: "json",
	            data: {'name': name ,'email': email, 'message' : message , 'subject' : subject},
	            headers: {
	                "x-csrf-token": $("[name=_token]").val()
	            },
	        }).always(function(response){
	        	console.log(response.responseText);
	        	if(response.responseText == 'ok'){
		        	swal('Success', 'Email Sent Successfully, We will reply as soon as possible!' , 'success');
	        	}
	        	else {
	        		swal({
						type: 'error',
						title: 'Oops...',
						text: 'Something went wrong! , Try again later',
					});
	        	}
	        });
       }
       else {
       	swal({
		  type: 'error',
		  title: 'Oops...',
		  text: 'Enter A valid Email!'
		})
       }
	} 
	</script>
@endsection