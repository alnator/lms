@extends('client.master')

@section('title', 'Eureka')

@section('body-tag', 'level-page')

@section('body')
@include('client.partials.linked-nav')
<?php $lang = App::getLocale() ?>


<section class="new-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">@lang("$lang.Courses")</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$course->title_en}}</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</section>
	
<section class="inner-banner"> <img src="<?php echo config('app.url'); ?>/public{{$course->wide_image_path}}" alt=""> </section>

<?php // dd($course); ?>
<section class="about_eureka inner-development">
	 @if (session('message'))
    <div class="container">
    <div class="row">
      <div class="col-xl-8 col-lg-7">
                        <div class="alert alert-success">
                            {{ session('message') }}  <a href="{{ url('/getcart') }} ">Go to cart</a>
                        </div>
                      </div>
                    </div>
                  </div>
                        @endif

  <div class="container">
    <div class="row">
      <div class="col-xl-8 col-lg-7">
        <h1 class="comntitle">{{$course->title_en}}</h1>
        <p>@lang("$lang.About") {{$course->title_en}}:<br> 
										{{$course->description_en}}</p>
		
<?php $download='file_path_'.$lang; ?>										
		@if (isset($course->downloadable->$download))

				@if($course->downloadable->$download != "")

				<a href="{{$course->downloadable->{'file_path_'.$lang} }}" class="course-btn-download">
					<img src="<?php echo config('app.url'); ?>/css/client/images/course-download-icon.png" alt=""> @lang("$lang.Dwonload Attachements")
				</a>

				@else 

				@if($course->downloadable->file_path_en!="")
				<a href="{{$course->downloadable->file_path_en }}" class="course-btn-download">
					<img src="<?php echo config('app.url'); ?>/css/client/images/course-download-icon.png" alt=""> @lang("$lang.Dwonload Attachements")
				</a>
				@endif


				@endif
			@endif


			@if(isset($course->kit_id) && $course->kit_id != ''  )
				<a href="{{ route('kit-page' , $course->kit_id) }}" class="course-btn-download">
					<img src="<?php echo config('app.url'); ?>/css/client/images/get-kit-icon.png" alt="">
					@lang("$lang.Get Course's Kit")
				</a>
			@endif


      </div>
      <div class="col-xl-4 col-lg-5">
        <div class="chaper-lesson"> 
        	@if($course->is_youtube == '1')

        	
        	  <iframe width="100%" height="100%" id="youtube_player" class="yt_player_iframe" src="https://www.youtube.com/embed/{{$course->url_identifier}}?enablejsapi=1" allowfullscreen></iframe>

        	 
        	@else
        	<img src="<?php echo config('app.url'); ?>/public{{$course->image_path}}" alt="">
         	@endif
          <h3>{{ count($course->session) }} @lang("$lang.Session"), {{$lessoncount}} @lang("$lang.Lessons")</h3>
        
         
        @if($purchased == 0 || Helper::showpurchase($course->id) == 0)


        <form method="POST" action="{{ route('addtocart') }}">
                    @csrf
                    <input type="hidden" name="itemid" value="{{ $course->id }}">
                    <input type="hidden" name="type" value="Course">
                    <input type="hidden" name="price" value="{{ $course->price }}">
                    <input type="hidden" name="supplierId" value="">
                    <input type="hidden" name="qty" value="1">
                    
                        <button type="submit" >@lang("$lang.Purchase course")</button>
                  </form>
                 
		<!-- <button  type="button" data-toggle="modal" data-target="#myModal">@lang("$lang.Purchase course")</button> -->
		@elseif(isset($resume) && $resume>0)
		<a href="{{route('session' , $resume)}}" class="resume">@lang("$lang.Continue")</a>
		@else

		@endif
         
        </div>
      </div>
    </div>
    @if($lang == 'ar')
    	{!! $course->text_ar !!}
    @else
    	{!! $course->text_en !!}
    @endif
   <!--  <div class="you-will-learn">
      <div class="row">
        <div class="col-md-12">
          <h4>you will learn:</h4>
          <ul class="list-unstyled">
            <li> With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science. </li>
            <li> With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science. </li>
            <li> With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science. </li>
            <li> With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science. </li>
          </ul>
        </div>
      </div>
    </div> -->
    @if(count($testimonial)>0)
    <div class="inner-testimonials">
      <div class="row">
        <div class="col-lg-2 col-md-2 col-4"> <img src="<?php echo config('app.url'); ?>/css/client/images/quote-icon.png" alt=""> </div>
        <div class="col-lg-10">
          <div class="owl-carousel owl-theme" id="testi-slider">
             @foreach($testimonial as $item )
	            <div class="item">
	              <div class="testi-slider">
	                <p>{{ $item->description_en }}</p>
	                <h5>{{ $item->name_en }} - {{ $item->title_en }} </h5>
	              </div>
	            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
    @endif


    <div class="topic-chapter">
      <div class="row">
        <div class="col-md-12">
          <h4>@lang("$lang.Topics covered in this course")</h4>
          <div class="sessions-lessons">
				@foreach($course->session as $session)
					@if($session->video->count() != 0 )
						<ul class="list-unstyled course-list">
            				<h4><a href="{{route('session',$session->id)}}">@lang("$lang.Session") {{$session->session_number}}</a></h4>
								
								@foreach($session->video as $video)
									<li>
						              <div class="chapter-img">
						              	@if(isset($video->image_path) && $video->image_path!="")
						               <img src=" <?php echo config('app.url'); ?>/public{{$video->image_path}}" alt="">
						               @else
						               <img src="{{ Helper::showvimeo($video->url_identifier) }}" alt="">
						               @endif



						                </div>
						              <div class="chapter-detail">
						                <div class="capter-title">
						                  <h5><a class="lesson-name" href="{{$video->free ? route('free-lesson',$video->id) : route('lesson',$video->id)  }}">
															{{$video->name_en}}
														</a></h5>
						                  
										
																@if($video->done ==1 )
																	<i><img src="<?php echo config('app.url'); ?>/css/client/images/right-icon.png" alt=""></i>
																@else
																	<i><img src="<?php echo config('app.url'); ?>/images/icons/Group%20224.png" alt=""></i>

																

																@endif	


									@if(isset($video->free) && $video->free == 1)
						               <i><img src="<?php echo config('app.url'); ?>/css/client/images/star-icon.png" alt=""></i>
						               @endif


						               

						                  

						                  
						                  <span>{{intval($video->estimated_time / 60)}}:{{$video->estimated_time % 60 < 10 ? '0'.$video->estimated_time % 60  : $video->estimated_time % 60 }}</span>

						                  		 


						                   </div>
						                <p>{{ $video->description_en }} </p>

						              


						              </div>
						            </li>
								@endforeach
						</ul>
						
						@endif
						@endforeach
						</div>
					</div>
          
          
        </div>
      </div>
    </div> 
</div>
</section>


<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">@lang("$lang.Single Course Subscription")</h4>
      </div>
      <div class="modal-body">
       <div class="container" id="payment-data" data-id="{{$course->id}}" data-price="{{$course->price}}">
				
				<div class="row subscription">
					<div class="col-md-5 pr-lg-5">
						
						<div class="price"><span class="price-val" id="price">{{ Helper::showCurrency($course->price) }}</span></div>
						<div class="details">
							<div class="row no-gutters">
								<div class="col-sm-6">@lang("$lang.Number of courses")</div>
								<div class="col-sm-6 right" id="number-of-courses">@lang("$lang.This Courses")</div>
							</div>
							<div class="row no-gutters" id="period">
								@lang("$lang.Payment every: This Payment Only") !
							</div>
						</div>

					</div>
					<div class="col-md-7">
						<div class="payment-wrapper">
							<div class="mb-3 font-weight-bold">@lang("$lang.Enter Your Coupon Here") <i><small>( @lang("$lang.optional"))</small></i></div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-check">
										<div class="row">
											<div class="col-sm-12 col-md-8">
												<label>@lang("$lang.Voucher Code"): </label>
												<input type="text" class="form-control" id="voucher-code">
											</div>
											<div class="col-sm-12 col-md-4" style="display: flex;">
												<button class="btn main2-border-btn w-100" style="align-self: flex-end;" id="check-coupon-btn">
													@lang("$lang.Check")
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="mb-3 font-weight-bold">@lang("$lang.Choose your payment method:")</div>

							<div class="row">
								<div class="col-sm-9">
									<div class="form-check text-center">
										<!-- <input class="form-check-input" style="margin-top: -23px;" type="radio" value="aa" id="pay-visa" name="pay_method" form="payment-form"> -->
										<label class="form-check-label" for="pay-visa">
											{!! Form::open(['route'=>'sts-payment' , 'id'=>'sts']) !!}
											<input type="hidden" name="coupon" id="coupon-field">
											<input type="hidden" name="model_id" id="model_id_form_fort" value="5">
											<input type="hidden" name="course_id" id="course_id_form_fort" value="{{$course->id}}">
											<img class="visa-img" src="<?php echo config('app.url'); ?>/images/STS.png" alt="visa"onclick="$('#sts').submit()"> 
											{!! Form::close() !!}
								  		</label>
									</div>
								</div>
								
								<!-- <div class="col-sm-6">
									<div class="form-check text-center">
										<input class="form-check-input" type="radio" value="paypal" id="pay-paypal" name="pay_method" form="payment-form">
										<label class="form-check-label" for="pay-paypal">
											{!! Form::open(['route'=>'paypal-test' , 'id' => 'pay'])   !!}
												<input type="hidden" name="model_id" id="model-id-form">
												<input type="hidden" name="course_id" id="course-id-form">
												<img class="paypal-img" src="<?php echo config('app.url'); ?>/images/payment/paypal.png" alt="paypal" onclick="submitPayment()">
											{!! Form::close() !!}
								  		</label>
									</div>
								</div> -->
								
							</div>

							
							<div class="row mb-2">
									
									<div class="col-lg-6 text-center offset-lg-3"><button class="btn main2-border-btn w-100" onclick="removeModal();">@lang("$lang.Cancel")</button></div>
							</div>
						</div>
					</div>
				</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>




@endsection
@section('scripts')
<script type="text/javascript">
	@if(!empty(session('empty_session')) &&session('empty_session') == 1)
		swal({
			type: 'error',
			title: 'Whoops... Empty Session',
			text: 'this session does not contain any video',
		});
	@endif 

	$('.modal').modal("hide");
	function paymentModal(){
		$('#payment').removeClass('d-none');	
		$('#page').addClass('background');
	}
	function removeModal(){
		$('#payment').addClass('d-none');	
		$('#page').removeClass('background');
	}
	function submitPayment(){
		model_id =$('#payment-data').data('id');
		$('#model-id-form').val(5);
		course_id = $('#payment-data').data('id');
		$('#course-id-form').val(course_id);
		$('#pay').submit();
	}
	function submitPayfort(){
		model_id =$('#payment-data').data('id');
		$('#model_id_form_fort').val(5);
		course_id = $('#payment-data').data('id');
		$('#course_id_form_fort').val(course_id);
		$('#payfort').submit();
	}
</script>
@if($lang=='ar')
<script>

  $('#testi-slider').owlCarousel({
    loop:true,
  margin: 25,
  nav: true,
  rtl:true,
  dots:false,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 1
    },
    1000: {
      items:1
    },
    1100: {
      items: 1
    }
  }
})
</script>
@else
<script>

  $('#testi-slider').owlCarousel({
    loop:true,
  margin: 25,
  nav: true,
  dots:false,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 1
    },
    1000: {
      items:1
    },
    1100: {
      items: 1
    }
  }
})
</script>
@endif
@endsection