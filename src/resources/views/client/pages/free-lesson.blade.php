@extends('client.master')


@section('title', 'Eureka')

@section('styles')
	

@endsection

@section('body-tag','free-lesson-page')

@section('body')
<?php $lang = App::getLocale(); ?>

@include('client.partials.linked-nav')


<section class="new-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">@lang("$lang.Courses")</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$course->title_en}}</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</section>



<section class="about_eureka inner-development">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <h4 class="comntitle">@lang("$lang.Number of sessions") {{$course->session->count()}}</h4>
        <div class="row">
          <div class="col-lg-7">
            <h1 class="comntitle">{{$course->title_en}}</h1>
          </div>
          <div class="col-lg-5">
            <div class="inner-quick-links"> 
            	<?php $download='file_path_'.$lang; ?>                   
    @if (isset($course->downloadable->$download))

        @if($course->downloadable->$download != "")

        <a href="{{$course->downloadable->{'file_path_'.$lang} }}">
          <img src="<?php echo config('app.url'); ?>/css/client/images/download-arrow.png" alt="">@lang("$lang.Dwonload Attachements")
        </a>

        @else 

        @if($course->downloadable->file_path_en!="")
        <a href="{{$course->downloadable->file_path_en }}">
          <img src="<?php echo config('app.url'); ?>/css/client/images/download-arrow.png" alt="">@lang("$lang.Dwonload Attachements")
        </a>
        @endif


        @endif
      @endif
			@if ($video->quiz != 0)
				<a href="javascript:void" onclick="Q1();" style="float: right;"><img src="<?php echo config('app.url'); ?>/css/client/images/take-quiz-icon.png" alt="">@lang("$lang.Solve Quiz")</a>
			@endif
			
      @if($video->free == 0)
			 @if($purchased == 0)
						<a href="" data-toggle="modal" data-target="#myModal">
					<img src="<?php echo config('app.url'); ?>/css/client/images/take-quiz-icon.png" alt="">@lang("$lang.purchase course")
				</a>
			@endif
      @endif
					
				

             </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            @if($video->is_youtube == 1)
             

              <iframe src="https://player.vimeo.com/video/{{$video->url_identifier}}?color=ffffff&title=0&byline=0&portrait=0&autoplay=true&speed=true&transparent=false" width="100%" height="409" frameborder="0" allow="autoplay; fullscreen" allowfullscreen ></iframe>


            @else
            
             <div class="use-play-1 flowplayer" data-player-id="56058953-2cbd-4858-a915-1253bf7ef7b2">
                    <script src="//cdn.flowplayer.com/players/8dfd6c14-ba3a-445e-8ef5-191d9358ed0a/native/flowplayer.async.js">
                      { "src": "{{$video->path}}" }
                    </script>
                  </div>

                    
            @endif
            



            <p>{{$video->name_en}} : {{intval($video->estimated_time / 60)}}:{{$video->estimated_time % 60 < 10 ? '0'.$video->estimated_time % 60  : $video->estimated_time % 60}} @lang("$lang.min")</p>
            <p>{{$course->description_en}}</p>
           	<p>{{$video->description_en}}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="lessons-need-sliders">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h5 class="comntitle">@lang("$lang.You will need")</h5>
      </div>
    </div>
  </div>
  <div class="lessons-slider-area">
    <div class="col-xl-4 col-lg-4 col-md-4">
   
      <div class="style-full">
        <div class="main-pic">
          @if(isset($kits->images))
            <?php $kk=1;?>
            @foreach($kits->images as $image)
              <?php if(count($kits->images)== $kk){ ?>
                <img src="{{ $image->img_src }}" alt="">
              <?php }
              $kk++; ?>
            @endforeach
          @else
            <img src="<?php echo config('app.url'); ?>/css/client/images/lessons-slider-pic1.png" alt="">
          @endif 
           
        </div>
        <hr class="img-btm-part">
        <div class="product-title">
          <h4>{{ $kits->name_en }}</h4>
           <p>{{ $kits->itemsCount }} @lang("$lang.Number of Items")</p>
            @if($kits->active == '1'  && $kits->available != 0)
            <span>
              <img src="<?php echo config('app.url'); ?>/css/client/images/right-icon.png" alt="">@lang("$lang.Available in store")
            </span>
            @else
            <span>
              <img src="<?php echo config('app.url'); ?>/css/client/images/cross-icon.png" alt="">@lang("$lang.Not Available in store")
            </span>
            @endif
          <div class="sub-unsub">
            <p><span>{{ $kits->maxPrice }}$</span></p>
          </div>
        </div>
            <button onclick="location.href='{{ route('kit-page' , $kits->id) }}'"> @lang("$lang.Continue")</button>
            
      </div>
      
    </div>
    <div class="col-xl-8 col-lg-8 col-md-8">
      <div class="owl-carousel owl-theme" id="lessons-two">
        <?php $k=1; ?>
        @foreach($items as $kititem)
        
         <?php if($k==1){ ?>
          <div class="item">
         <?php } ?>
        
          <div class="style-full style-half">
            <div class="main-pic"> 
              @if(isset($kititem->images))
              <?php $p=1;?>
              @foreach($kititem->images as $image)
                <?php if(count($kititem->images)== $p){ ?>
                  @if(file_exists($image->img_src))
                  <img src="{{ $image->img_src }}" alt="">
                  @else
                    <img src="<?php echo config('app.url'); ?>/css/client/images/lessons-slider-pic2.png" alt="">
                  @endif
                <?php }
                $p++; ?>
              @endforeach
            @else
              <img src="<?php echo config('app.url'); ?>/css/client/images/lessons-slider-pic2.png" alt="">
            @endif
               
            </div>
            <div class="product-title">
              @if(session('lang') == 'ar')
                <h4>{{ $kititem->items->name_ar }}</h4>
              @else
                <h4>{{ $kititem->items->name_en }}</h4>
              @endif
            
            <p>&nbsp;</p>
              @if($kititem->qty > 0)
              <span>
                <img src="<?php echo config('app.url'); ?>/css/client/images/right-icon.png" alt="">@lang("$lang.Available in store")
              </span>
              @else
              <span>
                <img src="<?php echo config('app.url'); ?>/css/client/images/cross-icon.png" alt="">@lang("$lang.Not Available in store")
              </span>
              @endif
              
            
              <div class="sub-unsub">
                <p><span>{{ Helper::showCurrency($kititem->maxPrice) }}</span></p>
                
              </div>
            </div>
              @if($kititem->qty > 0)
                  <form method="POST" action="{{ route('addtocart') }}">
                    @csrf
                    <input type="hidden" name="itemid" value="{{ $kititem->id }}">
                    <input type="hidden" name="type" value="Item">
                    <input type="hidden" name="price" value="{{$kititem->maxPrice}}">
                    <input type="hidden" name="supplierId" value="{{$kititem->supplierId}}">
                    <input type="hidden" name="qty" value="1">
                    
                        <button type="submit" >@lang("$lang.Order now")</button>
                  </form>
              @else
                <button onclick="location.href='{{ route('kit-page' , $kititem->kit_id) }}'" disabled="disabled">@lang("$lang.Order now")</button>
              @endif
            
          </div>
          <?php if($k % 2 == 0 && $k != count($items)){ ?>
          </div>
          <div class="item">
          <?php } 
          if($k == count($items)){ ?>
          </div>
          <?php }
           ?>
        
        <?php 
          $k++; ?>
         @endforeach
      </div>
    </div>
  </div>
</section>


<section class="lessons-next-pre">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div id="myCarousel" class="carousel slide next-pre" data-ride="carousel">
          <div class="carousel-inner">
          	<div class="carousel-item  active">
              <div class="row">
                <div class="col-sm-6">
                	@if(session('lang') == 'ar')
                		<div class="nxt-pre-slider right">
                	@else
                		<div class="nxt-pre-slider left">
                	@endif
                  
                  	@if(isset($previous))
                    <p>@lang("$lang.Session") {{ $previous->sessionbyid->session_number }}</p>
                    <h4><a href="{{$previous->free ? route('free-lesson',$previous->id) : route('lesson',$previous->id)  }}">{{ $previous->name_en }}</a></h4>
                    @else
                    <p>@lang("$lang.Get Started")</p>
                    <h4><a href="{{route('course-page', $course->slug)}}">@lang("$lang.Get Started")</a></h4>
                    @endif
                  </div>
                </div>
                <div class="col-sm-6">
                	@if(session('lang') == 'ar')
                		<div class="nxt-pre-slider left">
                	@else
                		<div class="nxt-pre-slider right">
                	@endif
                    @if(isset($next))
                    <p>@lang("$lang.Session") {{ $next->sessionbyid->session_number }}</p>
                    <h4><a href="{{$next->free ? route('free-lesson',$next->id) : route('lesson',$next->id)  }}">{{ $next->name_en }}</a></h4>
                    @else
                    	@if ($video->quiz != 0)
                    		<p>@lang("$lang.Solve Quiz")</p>
							<h4><a href="javascript:void" onclick="Q1();" >@lang("$lang.Solve Quiz")</a></h4>
						@endif
                    @endif
                  </div>
                </div>
              </div>
            </div>
            
          </div>
          <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"><img src="<?php echo config('app.url'); ?>/css/client/images/right-arrow.png" alt=""></span> </a> 
          <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"><img src="<?php echo config('app.url'); ?>/css/client/images/left-arrow.png" alt=""></span> </a> 
        </div>
        
        
      </div>
    </div>
  </div>
</section>


<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">@lang("$lang.Single Course Subscription")</h4>
      </div>
      <div class="modal-body">
       <div class="container" id="payment-data" data-id="{{$course->id}}" data-price="{{$course->price}}">
				
				<div class="row subscription">
					<div class="col-md-5 pr-lg-5">
						
						<div class="price"><span class="price-val" id="price">{{$course->price}} JOD</span></div>
						<div class="details">
							<div class="row no-gutters">
								<div class="col-sm-6">@lang("$lang.Number of courses")</div>
								<div class="col-sm-6 right" id="number-of-courses">@lang("$lang.This Courses")</div>
							</div>
							<div class="row no-gutters" id="period">
								@lang("$lang.Payment every: This Payment Only") !
							</div>
						</div>

					</div>
					<div class="col-md-7">
						<div class="payment-wrapper">
							<div class="mb-3 font-weight-bold">@lang("$lang.Enter Your Coupon Here") <i><small>( @lang("$lang.optional"))</small></i></div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-check">
										<div class="row">
											<div class="col-sm-12 col-md-8">
												<label>@lang("$lang.Voucher Code"): </label>
												<input type="text" class="form-control" id="voucher-code">
											</div>
											<div class="col-sm-12 col-md-4" style="display: flex;">
												<button class="btn main2-border-btn w-100" style="align-self: flex-end;" id="check-coupon-btn">
													@lang("$lang.Check")
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="mb-3 font-weight-bold">@lang("$lang.Choose your payment method:")</div>

							<div class="row">
								<div class="col-sm-9">
									<div class="form-check text-center">
										<!-- <input class="form-check-input" style="margin-top: -23px;" type="radio" value="aa" id="pay-visa" name="pay_method" form="payment-form"> -->
										<label class="form-check-label" for="pay-visa">
											{!! Form::open(['route'=>'sts-payment' , 'id'=>'sts']) !!}
											<input type="hidden" name="coupon" id="coupon-field">
											<input type="hidden" name="model_id" id="model_id_form_fort" value="5">
											<input type="hidden" name="course_id" id="course_id_form_fort" value="{{$course->id}}">
											<img class="visa-img" src="<?php echo config('app.url'); ?>/images/STS.png" alt="visa"onclick="$('#sts').submit()"> 
											{!! Form::close() !!}
								  		</label>
									</div>
								</div>
								
								<!-- <div class="col-sm-6">
									<div class="form-check text-center">
										<input class="form-check-input" type="radio" value="paypal" id="pay-paypal" name="pay_method" form="payment-form">
										<label class="form-check-label" for="pay-paypal">
											{!! Form::open(['route'=>'paypal-test' , 'id' => 'pay'])   !!}
												<input type="hidden" name="model_id" id="model-id-form">
												<input type="hidden" name="course_id" id="course-id-form">
												<img class="paypal-img" src="<?php echo config('app.url'); ?>/images/payment/paypal.png" alt="paypal" onclick="submitPayment()">
											{!! Form::close() !!}
								  		</label>
									</div>
								</div> -->
								
							</div>

							
							<div class="row mb-2">
									
									<div class="col-lg-6 text-center offset-lg-3"><button class="btn main2-border-btn w-100" onclick="removeModal();">@lang("$lang.Cancel")</button></div>
							</div>
						</div>
					</div>
				</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>




<!--quiz Modal -->
<div class="modal fade" id="quiz-modal" tabindex="-1" role="dialog" aria-labelledby="quiz" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">

			<div class="modal-header">
				<h5 class="modal-title sr-only" id="quiz">Quiz</h5>

			</div>

			<div class="modal-body">
				<div class="quiz-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit aliquid necessitatibus deleniti quibusdam asperiores, ullam quo vel itaque obcaecati non, harum cupiditate accusamus amet maiores ipsum consequatur fuga! Expedita, natus.</div>
				<form id="quiz-form">
					<div class="form-check">
						<input class="form-check-input" type="radio" name="quiz_radio" id="exampleRadios1" value="option1">
						<label class="form-check-label" for="exampleRadios1">
							Lorem ipsum dolor sit amet, aque, sit dolorum quo animi d
						  </label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="quiz_radio" id="exampleRadios2" value="option1">
						<label class="form-check-label" for="exampleRadios2">
							consectetur adipisicing elit. Adipisci, consequuntur, mollitia magnam asperiores e
						  </label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="quiz_radio" id="exampleRadios3" value="option1">
						<label class="form-check-label" for="exampleRadios3">
							olorem alias cum placeat ea dignissimos vel magni ut quae ducimus. Libero.
						  </label>
					</div>
				</form>
			</div>
			<div class="modal-footer">
			<div class="container">
			<div class="row">
			<div class="col-md-6 col-lg-2 offset-lg-8 mb-2">
				<button type="submit" class="btn main2-border-btn w-100" form="quiz-form">Submit</button>
			</div>
			<div class="col-md-6 col-lg-2">
			<button class="btn main2-border-btn w-100" data-dismiss="modal">Skip</button>
				</div>
			</div>
			</div>
				
				
			</div>
		</div>
	</div>
</div>

@endsection

@section ('scripts')
<script type="text/javascript">
	@if (Auth::check())
	$.ajax({
            type: "POST",
            url: '/video/finished', 
            dataType: "json",
            data: {'video_id':  {{$video->id}} },
            headers: {
                "x-csrf-token": $("[name=_token]").val()
            }
        });
	@endif


</script>
@if($lang=='ar')
<script>
  $('#lessons-two').owlCarousel({
    loop:false,
	center: false,
  margin: 30,
  rtl:true,
  nav: true,
  dots:false,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 1
    },
    900: {
      items:2
    },
    1000: {
      items:2
    },
    1100: {
      items: 2
    }
  }
})
</script> 
@else
<script>
  $('#lessons-two').owlCarousel({
    loop:false,
	center: false,
  margin: 30,
  nav: true,
  dots:false,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 1
    },
    900: {
      items:2
    },
    1000: {
      items:2
    },
    1100: {
      items: 2
    }
  }
})
</script> 
@endif

@endsection
