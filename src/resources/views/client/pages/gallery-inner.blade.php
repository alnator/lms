@extends('client.master')

@section('title', 'Eureka Gallery')

@section('body')
@include('client.partials.linked-nav')
<?php $lang = App::getLocale() ?>
</div>	

<section class="inner-banner"> <img src="<?php echo config('app.url'); ?>/css/client/images/course-banner.jpg" alt=""> </section>



<section class="gallery">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <h2 class="comntitle">{{ $albums->name_en }}</h2> 
        <div class="portfolio-slides">

        	@if(isset($galleryItems) && count($galleryItems)>0)
            
          <ul class="list-unstyled">
          	<?php $j=0; ?>
          	@foreach($galleryItems as $item)
			  <li>
				  <a href="#myGallery" data-slide-to="{{ $j }}">
            @if($item->type == 1)
				  <img src="{{$item->img_src}}" data-toggle="modal" data-target="#myModal" alt="">
          @else
          <img src="https://img.youtube.com/vi/{{$item->youtube_embed}}/hqdefault.jpg" data-toggle="modal" data-target="#myModal" alt="">
          
          @endif
				  </a>
			</li>
			<?php $j++; ?>
			@endforeach
 
</ul>
        <!--modal body-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog">
<div class="modal-content">

<div class="modal-body">

 <!-- carousel body-->
<div id="myGallery" class="carousel slide carousel-fade" data-interval="false">
  <div class="carousel-inner">

  	<?php $i=1; ?>
          	@foreach($galleryItems as $item)

          	 <div class="carousel-item @if($i==1) active @endif">
                @if($item->type == 1)
  <img src="{{$item->img_src}}" alt="{{$item->title_en}}">
  @else
  <iframe width="100%" height="100%" id="youtube_player" class="yt_player_iframe1" src="https://www.youtube.com/embed/{{$item->youtube_embed}}?enablejsapi=1"></iframe>
@endif

  <div class="carousel-caption">
 {{$item->title_en}}
  </div>
  </div>

			<?php $i++; ?>
			@endforeach



 


</div>

<!-- Previous and Next buttons-->
	<div class="pull-right">
<a class="left carousel-control" href="#myGallery" role="button" data-slide="prev">
<span class="fa fa-angle-left"></span></a> 
	
<a class="right carousel-control" href="#myGallery" role="button" data-slide="next">
<span class="fa fa-angle-right"></span></a>
		</div>
	
	
</div>

</div>

	
    </div>
    </div></div>    

    @else
    No Record Found.!
    @endif
        

   
						</div>              
      </div>
    </div>  
  </div>
</section>









@endsection
