@extends('client.master')

@section('title', 'Eureka')

@section('body-tag', 'Eureka Achievements')

@section('body')
@include('client.partials.linked-nav')
<?php $lang = App::getLocale() ?>

<section class="timeline">
  	<div class="container">
  		@foreach($achievements as $key => $achievement)
			@if($achievement->img_src == null)
			    <div class="timeline-item">
			      <div class="timeline-img"></div>
			      <div class="timeline-content {{$key % 2 ? 'js--fadeInRight' : 'js--fadeInLeft'}}">
			        <h2>{{ $achievement->{'title_'. $lang} }}</h2>
					<div class="date">{{ $achievement->event_date }}</div>
					<p>{{ $achievement->{'description_'.$lang} }}</p>
			      </div>
			    </div>
			@else
				<div class="timeline-item">
					<div class="timeline-img"></div>
					<div class="timeline-content timeline-card {{$key % 2 ? 'js--fadeInRight' : 'js--fadeInLeft'}}">
						<div class="timeline-img-header" style="background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.4)), url({{$achievement->img_src}}) center center no-repeat;">
							<h2>{{ $achievement->{'title_'. $lang} }}</h2>
						</div>
						<div class="date">{{ $achievement->event_date }}</div>
						<p>{{ $achievement->{'description_'.$lang} }}</p>
					</div>
				</div>
			@endif
		@endforeach
	</div>
</section>
@endsection


@section ('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 1
<script src="https://cdn.jsdelivr.net/scrollreveal.js/3.3.1/scrollreveal.min.js"></script> 
<script>
 $(function(){

  window.sr = ScrollReveal();

  if ($(window).width() < 768) {

  	if ($('.timeline-content').hasClass('js--fadeInLeft')) {
  		$('.timeline-content').removeClass('js--fadeInLeft').addClass('js--fadeInRight');
  	}

  	sr.reveal('.js--fadeInRight', {
	    origin: 'right',
	    distance: '300px',
	    easing: 'ease-in-out',
	    duration: 800,
	  });

  } else {
  	
  	sr.reveal('.js--fadeInLeft', {
	    origin: 'left',
	    distance: '300px',
		  easing: 'ease-in-out',
	    duration: 800,
	  });

	  sr.reveal('.js--fadeInRight', {
	    origin: 'right',
	    distance: '300px',
	    easing: 'ease-in-out',
	    duration: 800,
	  });

  }
  
  sr.reveal('.js--fadeInLeft', {
	    origin: 'left',
	    distance: '300px',
		  easing: 'ease-in-out',
	    duration: 800,
	  });

	  sr.reveal('.js--fadeInRight', {
	    origin: 'right',
	    distance: '300px',
	    easing: 'ease-in-out',
	    duration: 800,
	  });


});

</script> 
@endsection