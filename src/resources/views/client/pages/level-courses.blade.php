@extends('client.master')

@section('title', 'Eureka')

@section('body-tag', 'level-page')

@section('body')
    @include('client.partials.linked-nav')
    <?php $lang = App::getLocale() ?>

    <section class="new-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">@lang("$lang.Courses")</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{$levels->name_en}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- <section class="inner-banner"> <img src="<?php echo config('app.url'); ?>/css/client/images/course-banner.jpg" alt=""> </section> -->

    <section class="about_eureka">




        <div class="aboutslider">
            <div class="owl-carousel owl-theme" id="aboutslider">
                <?php $k=1; ?>
                @for($i=0 ; $i < $levelall->count(); $i++ )
                    <?php $levelq =$levelall[$i];  ?>

                    <div class="aboutbox @if($i%4==0) blue_bg @elseif($i%3==0) yellow_bg @elseif($i%2==0) green_bg @else red_bg @endif">
                        <a href="{{route('level', $levelq->slug)}}">
                            <h4>{{$levelq->name_en}}</h4>

                            <img src="<?php echo config('app.url'); ?>/public{{ $levelq->icon_path }}" alt="">
                        </a>
                    </div>

                    <?php $k++; ?>

                    @if($i%4==0)
                        <?php $k=1; ?>
                    @endif

                @endfor

                <?php if($levelall->count() > 4){ $arrowactive=""; }else{ $arrowactive=""; } ?>



            </div>
        </div>




        <div class="slider_data">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8 col-md-12">
                        <h2 class="comntitle">{{$levels->name_en}}</h2>
                        <p>{{$levels->description_en}}</p>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12" style="margin-top: 7%">
                        <div class="buycourses">
                            <img src="<?php echo config('app.url'); ?>/public{{ $levels->icon_path }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if(Auth::id() != null)
        @if(isset($courses) && count($courses)>0)
            <section class="profile_courses">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <h2 class="comntitle text-center">@lang("$lang.my courses")</h2>
                            <div class="owl-carousel owl-theme" id="profile_courses">

                                @foreach($courses as $coursesobj)
                                    <div class="item">
                                        <div class="coursesbox">
                                            <h4>{{ $coursesobj->title_en }}</h4>
                                            <img src="<?php echo config('app.url'); ?>{{(isset($coursesobj->image_path) ? '/public'.$coursesobj->image_path: '/images/course-1.jpg')}}" alt="">
                                            <p>{{ $coursesobj->description_en }}</p>
                                            <aside>
                                                <small>{{ count($coursesobj->lession) }} @lang("$lang.Lessons") ({{ $coursesobj->courseTime }} @lang("$lang.Hours"))</small>
                                                <span>{{ Helper::showCurrency($coursesobj->price) }}</span>
                                            </aside>
                                            <a href="{{route('course-page', $coursesobj->slug)}}" @if(Helper::showPlanStatus($coursesobj->id) !='')  style="background: #333d77;" @endif >@if(Helper::showPlanStatus($coursesobj->id) !='') {{ Helper::showPlanStatus($coursesobj->id) }} @else @lang("$lang.Get Started") @endif</a>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endif



    @if(isset($othercourses) && count($othercourses)>0)
        <section class="Renewable_Energey_Courses">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <h2 class="comntitle">Other {{$levels->name_en}} Courses</h2>
                        <ul class="list-unstyled">

                            @foreach($othercourses as $coursesobj)
                                <li>
                                    <div class="Energeyimg">
                                        <img src="<?php echo config('app.url'); ?>{{(isset($coursesobj->image_path) ? '/public'.$coursesobj->image_path: '/images/course-1.jpg')}}" alt="">
                                    </div>
                                    <div class="Energeytext">
                                        <h2>{{ $coursesobj->title_en }}</h2>
                                        <small>{{ count($coursesobj->lession)}} @lang("$lang.Lessons") ({{ $coursesobj->courseTime }} @lang("$lang.Hours"))</small>
                                        <p>{{ $coursesobj->description_en }}</p>
                                        <strong>{{ Helper::showCurrency($coursesobj->price) }}</strong>
                                        <button onclick="window.location.href='{{route('course-page', $coursesobj->slug)}}'">@lang("$lang.Get Started")</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            </div>
        </section>
    @endif




@endsection

@section ('scripts')
    <script src="{{URL::asset('js/client/index.js')}}"></script>
    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>



    @if($lang=='ar')
        <script>
            $('#aboutslider').owlCarousel({
                loop: false,
                margin: 30,
                rtl:true,
                // center: true,
                nav: true,
                navText: ["<i class='lni lni-arrow-right {{ $arrowactive }}'></i>", "<i class='lni lni-arrow-left'></i>"],
                dots: false,
                responsive: {
                    0: {
                        items: 1
                    },
                    479: {
                        items: 1
                    },
                    768: {
                        items: 2
                    },
                    979: {
                        items: 3
                    },
                    1000: {
                        items: 3
                    }
                    ,
                    1300: {
                        items: 4
                    }
                }
            })

            // $('.owl-carousel').find('.owl-nav').removeClass('disabled');
            // $('.owl-carousel').on('changed.owl.carousel', function (event) {
            //     $(this).find('.owl-nav').removeClass('disabled');
            // });
        </script>


        <script>
            $('#profile_courses').owlCarousel({
                margin: 25,
                nav: true,
                rtl:true,
                dots:false,
                navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    1000: {
                        items:3
                    },
                    1100: {
                        items: 4
                    }
                }
            })
        </script>

    @else

        <script>
            $('#aboutslider').owlCarousel({
                loop: false,
                margin: 30,
// center: true,
                nav: true,
                navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right {{ $arrowactive }}'></i>"],
                dots: false,
                responsive: {
                    0: {
                        items: 1
                    },
                    479: {
                        items: 1
                    },
                    768: {
                        items: 2
                    },
                    979: {
                        items: 3
                    },
                    1000: {
                        items: 3
                    }
                    ,
                    1300: {
                        items: 4
                    }
                }
            })

            // $('.owl-carousel').find('.owl-nav').removeClass('disabled');
            // $('.owl-carousel').on('changed.owl.carousel', function (event) {
            //     $(this).find('.owl-nav').removeClass('disabled');
            // });


        </script>

        <script>
            $('#profile_courses').owlCarousel({
                margin: 25,
                nav: true,
                dots:false,
                navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    1000: {
                        items:3
                    },
                    1100: {
                        items: 4
                    }
                }
            })
        </script>

    @endif
@endsection