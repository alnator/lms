@extends('client.master')

@section('title', 'Eureka Gallery')

@section('body')
@include('client.partials.linked-nav')
<?php $lang = App::getLocale() ?>
</div>	

<section class="inner-banner"> <img src="<?php echo config('app.url'); ?>/css/client/images/course-banner.jpg" alt=""> </section>

<section class="gallery">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
       <!--  <div class="filters filter-button-group">
          <ul>
            <li class="active" data-filter="*">All</li>
            <li data-filter=".webdesign">Web Design</li>
            <li data-filter=".webdev">Web Development</li>
            <li data-filter=".brand">Brand Identity</li>
          </ul>
		    </div> -->

		<div class="content grid">

		
			@foreach($albums as $album )

		  <div class="single-content webdesign webdev grid-item">
        <img class="p2" src="{{ $album->img_src }}" alt=""> 
          <div class="overlay">
            <a href="{{ url('/item/'.$album->id) }}">
              <i class="fa fa-picture-o"></i>
              <p>{{ $album->name_en??'' }}</p>
              <span>{{ $album->images_count}} Item</span>
            </a> 
          </div>     
		  </div>

		  @endforeach

		 
              
        </div>
        
              
      </div>
    </div>
  </div>
</section>












@endsection
