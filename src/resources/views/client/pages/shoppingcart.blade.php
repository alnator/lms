@extends('client.master')

@section('title', 'Eureka')

@section('body-tag', 'shoppingcart-page')

@section('body')
@include('client.partials.linked-nav')
<?php $lang = App::getLocale() ?>


<section class="new-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">@lang("$lang.Home")</a></li>
            <li class="breadcrumb-item active" aria-current="page">@lang("$lang.Shopping Cart")</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</section>
<section class="lessons-need-sliders Shopping_Cart">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <h1 class="comntitle">@lang("$lang.Shopping Cart")</h1>
      </div>
    </div>
    <div>

    <div class="row">
      <div class="col-xl-5 col-lg-5 col-md-6"  id="listcart">
         
          
          
      </div>
      <div class="col-xl-7 col-lg-7 col-md-6" id="sideadd" style="display: none;">
        <form method="POST" action="{{ route('checkout') }}">
          @csrf
          @if(isset($cart))
          <input type="hidden" name="cartid" value="{{$cart->id}}">
          @endif
        <div class="address_details">
          <aside><strong>@lang("$lang.Total Price")</strong> 
          @if(isset($cart))
          <span class="price-val"></span>
          @else
          <span>0 JOD</span>
          @endif</aside>
          <hr>
          <h4>@lang("$lang.Delivery address"):</h4>
          <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6">
              <label>@lang("$lang.Country")</label>
              <select class="form-control" type="text" name="country_code" id="country-input1" required  style="width: 100%">
                
              </select>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6">
              <label>@lang("$lang.City")</label>
              <input class="form-control" type="text" name="city" id="city-input" required>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6">
              <label>@lang("$lang.Province")</label>
              <input class="form-control" type="text" name="province" id="province-input" required>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6">
              <label>@lang("$lang.Postal Code")</label>
              <input class="form-control" type="text" name="postal" id="postal-input" required>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-12">
              <label>@lang("$lang.Address Line") 1</label>
              <input class="form-control" type="text" name="line1" id="line1-input" required>
              
            </div> 
            <div class="col-xl-12">
              <label>@lang("$lang.Address Line") 2</label>
              <input class="form-control" type="text" name="line2" id="line2-input" required>
             
            </div>
            <div class="col-xl-12">
              <label>@lang("$lang.Address Line") 3</label>
              <input class="form-control" type="text" name="line3" id="line3-input">
              <hr>
            </div>            
          </div>
          <h4>@lang("$lang.Contact information"):</h4>
          <div class="row">
            <div class="col-xl-12">
              <label>@lang("$lang.Name")</label>
              <input class="form-control" type="text" name="name" id="name-input" required>
              <hr>
            </div>            
          </div>
          <div class="row">
            
            <div class="col-xl-6 col-lg-6 col-md-6">
              <label>@lang("$lang.Phone Number")</label>
              <input class="form-control" type="number" name="phone" id="phone-input" required>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6">
              <label>@lang("$lang.Cell Phone Number")</label>
              <input class="form-control" type="text" name="cell_phone" id="cell-phone-input">
            </div>
          </div>
          <div class="row">
            <div class="col-xl-12">
              <label>@lang("$lang.Email")</label>
              <input class="form-control" type="text" name="email" id="email-input" required>
              <hr>
            </div>            
          </div>
          <h4>@lang("$lang.Payment Method"):</h4>
            <div class="radio">
              <input id="radio-1" name="payment_method" type="radio" value="cod" checked>
              <label for="radio-1" class="radio-label">@lang("$lang.Cash on  delivery")</label>
            </div>
            <div class="radio">
              <input id="radio-2" name="payment_method" type="radio" value="card">
              <label  for="radio-2" class="radio-label">@lang("$lang.Credit card")</label>
            </div>
            <hr>
            <h4>@lang("$lang.Discount"):</h4>
            <div class="col-xl-12">
              <label>@lang("$lang.Promo Code")</label>
              <input type="text" placeholder='@lang("$lang.enter the promo code if you have one")' name="voucher-code" id="voucher-code">
               <a class="btn btn-info btn-sm" id="check-coupon-btn">@lang("$lang.Apply")</a>
               <a class="btn btn-danger btn-sm" id="remove-coupon-btn" style="display: none;">@lang("$lang.Remove")</a>
              <hr>

            </div>
            <div class="checkbox">
                <div class="form-group">
                  <input type="checkbox" id="agree" name="agree" value="1" required>
                  <label for="agree">@lang("$lang.i agree on") <mark>@lang("$lang.service rules")</mark> @lang("$lang.and on") <mark>@lang("$lang.sales policy")</mark></label>
                </div>
            </div>

            <button type="submit" id="proceed">@lang("$lang.Proceed to checkout")</button>

      </div>
</form>
    </div>
    </div>
    
  </div>
</section>



@endsection
@section('scripts')
<script>

  $(document).ready(function(){
   filter_data();
});


$(document).on('click', '.quantity-left-minus', function(e){
    var cartdetailid=$(this).data('id');
    // Stop acting like a button
    e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity_'+cartdetailid).val());
        // If is not undefined
            // Increment
            if(quantity>1){
            $('#quantity_'+cartdetailid).val(quantity - 1);
            qty(cartdetailid,quantity - 1);
            }
            if(quantity <= 2){
               $(".minus_"+cartdetailid).addClass('none');
            }
    });

$(document).on('click', '.quantity-right-plus', function(e){
    var cartdetailid=$(this).data('id');
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity_'+cartdetailid).val());
        // If is not undefined
            $(".minus_"+cartdetailid).removeClass('none');
            $('#quantity_'+cartdetailid).val(quantity + 1);
            qty(cartdetailid,quantity + 1);
            // Increment
});
function qty(id,qty){
   $.ajax({
                url:"{{ url('updatecart') }}",
                method:"POST",
                data:{ "_token": "{{ csrf_token() }}",id:id,qty:qty },
                success:function(data) {
                     //window.location.href = "{{route('getcart')}}"
                     filter_data();
                }
            }); 
}
function filter_data() 
{
    $.ajax({
        url:"{{ url('ajaxcart') }}",
        method:"POST",
        data:{ "_token": "{{ csrf_token() }}" },
        success:function(data) {
            $("#listcart").html(data.html);
            $(".price-val").html(data.price);

            if(data.price<='0 JOD' || data.price<='0.00 JOD'){
              $('#sideadd').hide();
               swal('Error' , 'Cart Empty.',  'error');
               $("#proceed").prop('disabled', true);
            }else{
              $('#sideadd').show();
            }
                       
        }
    }); 
}
$(document).on('click', '.removecart', function(){
  var deleteid=$(this).data('id');
  $.ajax({
      url:"{{ url('deletecart') }}",
      method:"POST",
      data:{ "_token": "{{ csrf_token() }}",deleteid:deleteid },
      success:function(data) {
           filter_data();
      }
  }); 
      
});
var opt='<option value="">--Select Country--</option>';
  $.ajax({url: "https://restcountries.eu/rest/v2/all", success: function(data){
    for (var i = data.length - 1; i >= 0; i--) {
            opt += "<option value='"+data[i].alpha2Code+"'>";
            opt += data[i].name;
            opt += "</option>";
          }

          $('#country-input1').html(opt);


  }});

$('#country-input11').select2({
   width: 'resolve',
   minimumResultsForSearch: -1,
      ajax: {
        url: 'https://restcountries.eu/rest/v2/all',
        header: {
          'Access-Control' : '*'
        },
        processResults: function (data) {
          for (var i = data.length - 1; i >= 0; i--) {
            data[i].id =data[i].alpha2Code;
            data[i].text = data[i].name;
          }
          return {
            results: data
          };
        }
      }
    });
  

  $('#check-coupon-btn').click(function(){
      code = $('#voucher-code').val();
      validateCoupon(code);
    });


  function validateCoupon(couponCode){
      $.ajax({
        url: "{{ url('validate-coupon') }}",
        method:'POST',
        data: {
          coupon: couponCode,
        },
        headers: {
          'x-csrf-token': $("[name=_token]").val(),
        },
      }).done(response => {
        response = JSON.parse(response);
        if (response.valid )
        {
        
          updatePrices(response.amount, couponCode,'U');
          swal('Success', 'Your coupon code is submitted successfully.' , 'success');
        }
        else {
          swal('Error' , 'Invalid coupon code.',  'error');
        }
      });
    }

function updatePrices(amount, couponCode,type)
    {
      $.ajax({
                url:"{{ url('updatecoupon') }}",
                method:"POST",
                data:{ "_token": "{{ csrf_token() }}",amount:amount,couponCode:couponCode,type:type },
                success:function(data) {
                     //window.location.href = "{{route('getcart')}}"
                     filter_data();

                     if(type=="U"){
                     $("#check-coupon-btn").hide();
                     $("#remove-coupon-btn").show();
                      }else{
                      $("#check-coupon-btn").show();
                     $("#remove-coupon-btn").hide();
                     $('#voucher-code').val('');
                      swal('Success', 'Your coupon code is Removed successfully.' , 'success');
                      }
                }
            }); 
    }


      $('#remove-coupon-btn').click(function(){
      
      updatePrices('','','R');
    });

</script>

@endsection