@extends('client.master')


@section('title', 'Eureka')


@section('body-tag' , 'change-pass')
@section('body')

@include('client.partials.linked-nav')


<?php $lang= App::getLocale() ?>

<section class="new-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="#">Courses</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">@lang("$lang.Change Password")</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</section>




					
				




<section class="user_profile"> 
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<h1>@lang("$lang.Change Password")</h1>
			</div>
		</div>
		<div class="row">
		
			<div class="col-xl-6 col-lg-6 col-md-6 offset-md-3">
				 @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif
				 <form method="POST" action="{{ route('pass-change-front') }}">

				 	 @csrf
                        

                        <div class="form-group">
                            <label for="currentpass">@lang("$lang.Current Password")</label>

                            <input id="currentpass" type="password" class="form-control" name="currentpass" value="" required >

                              
                            
                        </div>

                        <div class="form-group">
                            <label for="newpassword">@lang("$lang.Password")</label>

                            <input id="newpassword" type="password" class="form-control" name="password" required>

                               
                        </div>

                        <div class="form-group">
                            <label for="password-confirm">@lang("$lang.Confirm Password")</label>

                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            
                        </div>

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary">
                                @lang("$lang.Reset Password")
                            </button>
                        </div>
                    </form> 
			</div>
		</div>
	</div>
</section>










					

@endsection
@section ('scripts')

<script type="text/javascript">



</script>


@endsection
