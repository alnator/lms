@extends('client.master')


@section('title', 'Eureka')


@section('body-tag' , 'profile-page')
@section('body')

@include('client.partials.linked-nav')


<?php $lang= App::getLocale() ?>

<section class="new-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="#">Courses</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">@lang("$lang.Order")</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</section>

<section class="about_eureka inner-development">
  <div class="container">
    
  

   

    <div class="topic-chapter">
      <div class="row">
        <div class="col-md-12">

          <div class="sessions-lessons">
			
						
						  <div class="col-12 card">
    
    <div class="row">
      <div class="col-12"><h4 class="h4cls">@lang("$lang.Order") #{{ $order->id }}</h4><span class="spancls"><strong>@if($order->status=='paid') @lang("$lang.Paid") @else @lang("$lang.Unpaid") @endif</strong></span><hr></div>
      <div class="col-12"><h4>@lang("$lang.Shipping Information")</h4><hr></div>

      <div class="col-6"><label>@lang("$lang.Name") : </label>&nbsp;&nbsp;<strong>{{$order->user->name}}</strong></div>
      <div class="col-6"><label>@lang("$lang.Email") : </label>&nbsp;&nbsp;<strong>{{$order->user->email}}</strong></div>
      <div class="col-6"><label>@lang("$lang.Phone") : </label>&nbsp;&nbsp;<strong>{{$order->phone}}</strong></div>
      <div class="col-6"><label>@lang("$lang.Address") : </label>&nbsp;&nbsp;<strong>{{$order->line1}} , {{$order->line2}} {{$order->line3}}</strong></div>
      <div class="col-6"><label>@lang("$lang.Country") : </label>&nbsp;&nbsp;<strong>{{$order->user->country}}</strong></div>
      <div class="col-6"><label>@lang("$lang.City") : </label>&nbsp;&nbsp;<strong>{{$order->city}}</strong></div>
       <div class="col-6"><label>@lang("$lang.Payment Mode") : </label>&nbsp;&nbsp;<strong>{{$order->payment_method}}</strong></div>

       <div class="col-6"><label>@lang("$lang.Order Date") : </label>&nbsp;&nbsp;<strong>{{$order->created_at->format('Y-m-d')}}</strong></div>


      <div class="col-12"><hr></div>
    </div>
    <div class="row">
      <div class="col-12"><h4>@lang("$lang.Order Items")</h4><hr></div>
      <div class="col-12">
      @if(count($orderItems)>0)
      <div class="table-responsive">
        <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">@lang("$lang.Item Name")</th>
      <th scope="col">@lang("$lang.Type")</th>
      <th scope="col">@lang("$lang.Price")</th>
      <th scope="col">@lang("$lang.Quantity")</th>
      <th scope="col">@lang("$lang.Total Price")</th>
      
    </tr>
  </thead>
  <tbody>
    
    @foreach($orderItems as $items)
    <tr>
      <th scope="row">{{$items->id}}</th>
      <td>{{$items->itemname}}</td>
      <td>{{$items->type}}</td>
      <td>{{ Helper::showCurrency($items->price) }}</td>
      <td>{{$items->qty}}</td>
      <td>{{ Helper::showCurrency($items->final_total) }}</td>
     
    </tr>
    @endforeach

    @if(isset($order->coupon_code))
    <tr>
      <th scope="row" colspan="5">@lang("$lang.Coupon")</th>
      <td >{{$order->coupon_code}}</td>
    </tr>

    <tr>
      <th colspan="5" scope="row">@lang("$lang.Discount")</th>
      <td >{{ Helper::showCurrency($order->coupon_amount) }}</td>
    </tr>

    @endif

    <tr>
      <th scope="row" colspan="5">@lang("$lang.Total Price")</th>
      <td>{{ Helper::showCurrency($order->total_price) }}</td>
    </tr>

      </tbody>
  </table>
</div>
 
  @else
    <p href="template" class="text-center">@lang("$lang.Record Not Found")</p>
    @endif
      </div>
    </div>

  </div>
						
						</div>
					</div>
          
          
        </div>
      </div>
    </div> 

</section>
					
<style type="text/css">
  .h4cls {
    margin-top: 20px;
    width: 80%;
    float: left;
  }

  .spancls{
    width: 20%;
    margin-top: 20px;
  }
</style>
@endsection
