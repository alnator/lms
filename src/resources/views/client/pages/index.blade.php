@extends('client.master')

@section('title', 'Eureka')

@section('body-tag', 'home-page')

@section('body')

@include('client.partials.nav')

<?php $lang = App::getLocale() ?>

 {!! $homebannerText !!}

</div>
		



<section class="about_eureka">
  <div class="container">
    <div class="row">
      <div class="col-xl-10">
        {!! $aboutText !!}
      </div>
    </div>
  </div>



  <div class="aboutslider">
    <div class="owl-carousel owl-theme" id="aboutslider">
  	<?php $k=1; ?>    
    	@for($i=0 ; $i < $levels->count(); $i++ )
				<?php $level =$levels[$i];  ?> 

					<div class="aboutbox @if($i%4==0) blue_bg @elseif($i%3==0) yellow_bg @elseif($i%2==0) green_bg @else red_bg @endif">
			        	<a href="{{route('level', $level->slug)}}">
			          	<h4>{{$level->name_en}}</h4>
			          	
			          	 <img src="<?php echo config('app.url'); ?>/public{{ $level->icon_path }}" alt="">
			        	</a>
			      	</div>

			      	<?php $k++; ?>

			      		@if($i%4==0)
			      		<?php $k=1; ?>
			      		@endif

				@endfor

			<?php if($levels->count() > 4){ $arrowactive="arrowactive"; }else{ $arrowactive=""; } ?>
      
     
      
    </div>
  </div>
</section>
<?php // dd($courses); ?>
<section class="courses">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <h2 class="comntitle text-center">@lang("$lang.Courses")</h2>
        <div class="owl-carousel owl-theme" id="courses">
          
          
       	@foreach($courses as $coursesobj)	
          <div class="item">
            <div class="coursesbox">
              <h4>{{ $coursesobj->title_en }}</h4>
              <img src="<?php echo config('app.url'); ?>{{(isset($coursesobj->image_path) ? '/public'.$coursesobj->image_path: '/images/course-1.jpg')}}" alt="">
              <p>{{ $coursesobj->description_en }}</p>
              <aside>
                <small>{{ count($coursesobj->lession)}} @lang("$lang.Lessons") ({{ $coursesobj->courseTime }} @lang("$lang.Hours"))</small>
                <span>{{ Helper::showCurrency($coursesobj->price) }}</span>
              </aside>
              <a href="{{route('course-page', $coursesobj->slug)}}">@lang("$lang.Get Started")</a>
            </div>
          </div>
           @endforeach
          
          
        </div>
        <div class="clearfix"></div>
        <a href="{{route('courses')}}" class="viewall">@lang("$lang.View all courses")</a>
      </div>
    </div>
  </div>
</section>
 


<section class="Eureka_Stars">
 <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <h2 class="comntitle text-center">@lang("$lang.Eurek")@lang("$lang.ian Stars")</h2>
        <div class="owl-carousel owl-theme" id="Eureka_Stars">
          
        	@foreach($stars as $starsobj)
           @if(isset($starsobj->user->id))
            
          <div class="item">
            <div class="starbox">
              <img src="<?php echo config('app.url'); ?>/public{{(isset($starsobj->image_path) ? $starsobj->image_path: '/images/logo.png')}}" alt="" class="img-circle">
             <span>{{ $starsobj->user->name }}</span>
              <small>{{ $starsobj->user->country }}</small>
              <!-- <h4>1st Level: Robotics WEDO 1</h4> -->
            </div>
          </div>


        
             @endif
          @endforeach
          
        </div>
      </div>
    </div>
  </div>
</section>




<section class="Testimonials">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <h1>@lang("$lang.Testimonials")</h1>
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
          	<?php 
          	$i=1; 
          	$ctrl = 1;
            $k1 = 0; 
            ?>
            
          	@foreach($testimonial as $item )
              <?php
              //echo $k1."====";
               if($ctrl == 1) { ?>
               <div class="carousel-item active">
              <?php } ?>
               <?php if($i % 5 == 0 ) { $k1=0;
                 ?>
                </div>
                <div class="carousel-item">
               <?php  } ?>	
                <?php 
                //echo $k1."=>";
                $classbg = '';
                if($k1==0){ $classbg = 'yellow_bg'; }
                if($k1==1){ $classbg = 'green_bg'; } 
                if($k1==2){ $classbg = 'blue_bg'; }
                if($k1==3){ $classbg = 'red_bg'; } 
                ?>
                <?php if($k1==0){ ?>
                  <div class="row justify-content-end">
                <?php } ?>

                <?php if($k1==2){ ?>
                  <div class="row">
                <?php } ?>
                
                
	                <div class="col-xl-5 col-lg-5 col-md-5">
	                  <div class="testbox <?php echo $classbg; ?>">
	                    <strong>{{ $item->name_en }} - {{ $item->title_en }}</strong>
	                    <p>{{ $item->description_en }}</p>
	                  </div>
	                </div>
                  <?php if($k1==1){ ?>
                   </div>
                <?php } ?>

                <?php if($k1==3){ ?>
                   </div>
                <?php } ?>
               
                 
                  
                

	             <?php if($ctrl == count($testimonial)){
                echo "</div>";
               } ?>
          	<?php $i++; 
          	$ctrl++;
            $k1++;
          	?>
            @endforeach
            


          </div>
          @if($lang=='ar')
           <div class="test_control">
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="lni lni-arrow-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="lni lni-arrow-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            
          </div>

          @else


          <div class="test_control">
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="lni lni-arrow-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="lni lni-arrow-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
          
          @endif
        
      </div>
    </div>
  </div>
</section>

<section class="mob_Testimonials">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <h1>@lang("$lang.Testimonials")</h1>
        <div class="owl-carousel owl-theme" id="mob_Testimonials">
          <?php $ii=1; 
            $kk=0; 
            ?>
          @foreach($testimonial as $item )
          <?php if($ii % 5 == 0 && $ii != 1) { $kk=0;  } 
          $classbg = '';
          if($kk==0){ $classbg1 = 'yellow_bg'; }
          if($kk==1){ $classbg1 = 'green_bg'; } 
          if($kk==2){ $classbg1 = 'blue_bg'; }
          if($kk==3){ $classbg1 = 'red_bg'; } ?>
          <div class="item">
            <div class="testbox <?php echo $classbg1; ?> ">
              <strong>{{ $item->name_en }} - {{ $item->title_en }}</strong>
                      <p>{{ $item->description_en }}</p>
            </div>
          </div>
          <?php $ii++; 
            $kk++;
            ?>
            @endforeach
        </div>
      </div>
    </div>
  </div>
</section>


<section class="vision_partner">
 <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <h2 class="comntitle text-center">@lang("$lang.Vision & Partners")</h2>
        <div class="owl-carousel owl-theme" id="vision_partner">
          
        	@foreach($vision as $visionobj)
         
            
          <div class="item">
            <div class="visionbox">
              <img src="<?php echo config('app.url'); ?>/public{{(isset($visionobj->image_path_en) ? $visionobj->image_path_en: '/images/logo.png')}}" alt="" class="img-responsive">
            
            </div>
          </div>


        
       
          @endforeach
          
        </div>
      </div>
    </div>
  </div>
</section>



	<!-- <section id="home" class="home" style="min-height: 120vh">
		<div class="home-content">
			<div class="container">
	 -->			<!--  login  -->
			<!-- 

				@if(!Auth::check())
				<div class="row login-section header-section-wrapper" id="login">
					<div class="login-title col-12">
						@lang("$lang.join the extreme adventure")
					</div>
					<div class="col-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
						<div class="header-section">
							<div class="container">
								<form method="POST" action="{{ route('login') }}">
                        		@csrf
                        		 <span class="wrong-details">
                        		 	@if (session('_previous') != '/register')
                        		 	
			                        	@if ($errors->has('name'))
                                    		<strong>{{ $errors->first('name') }} </strong><br>
                                    	@endif
                                    	@if ($errors->has('email'))
                                    	    <strong>{{ $errors->first('email') }}</strong><br>
                                    	@endif
                                    	@if ($errors->has('password'))
                                            <strong>{{ $errors->first('password') }}</strong><br>
                                		@endif
			                        @endif
			                        </span>
									<div class="form-group row">
										<label for="login-email" class="col-sm-4 col-form-label"><img class="icon" src="<?php echo config('app.url'); ?>/images/login_icons/username.png" height="25px"><span>@lang("$lang.Email")</span></label>
										<div class="col-sm-8">
											<input type="email" class="form-control" id="login-email" name="email">
										</div>
									</div>

									<div class="form-group row">
										<label for="login-password" class="col-sm-4 col-form-label"><img class="icon" src="<?php echo config('app.url'); ?>/images/login_icons/password.png" height="19px"><span>@lang("$lang.Password")</span></label>
										<div class="col-sm-8">
											<input type="password" class="form-control" id="login-password" name="password">
										</div>
									</div>
    		                        <div class="col-md-6 offset-md-4">
										<label>
	                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> @lang("$lang.Remember Me")
	                                    </label>
									</div>
									<div class="row">
										<div class="col-12 col-sm-6 offset-sm-3">
											<div class="text-center">
												<button type="submit" class="btn main1-white-btn submit" >@lang("$lang.LOGIN")</button>
											</div>
										</div>
									</div>
									

								</form>

								<div class="footer">
									<div class="row">
										<div class="col">
											<div class="link"><a href="{{route('try-lesson')}}">@lang("$lang.Try a free lesson")</a></div>
										</div>
										<div class="col">
											<div class="link hover"><a onclick="toRegister()">@lang("$lang.Create an account")</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--  register  ->
				<div class="row register-section header-section-wrapper d-none" id="register">
					<div class="col-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
						<div class="header-section">
							<div class="container">
								{{Form::open(['route' => 'register'])}}
			                        @csrf
			                        <span class="wrong-details">
                        		    	@if(session('_previous') == '/register')
				                        	@if ($errors->has('name'))
	                                    		<strong>{{ $errors->first('name') }}</strong><br>
	                                    	@endif

	                                    	@if ($errors->has('email'))
	                                    	    <strong>{{ $errors->first('email') }}</strong><br>
	                                    	@endif
	                                    	@if ($errors->has('password'))
	                                            <strong>{{ $errors->first('password') }}</strong><br>
	                                		@endif
                                		@endif
                                   </span>
									<div class="form-group row">
										<label for="name" class="col-sm-4 col-form-label">@lang("$lang.Username")</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" id="name" name="name" required>
										</div>
									</div>

									<div class="form-group row">
										<label for="password" class="col-sm-4 col-form-label">@lang("$lang.Password")</label>
										<div class="col-sm-8">
											<input type="password" class="form-control" id="password" name="password" required>
										</div>
									</div>

									<div class="form-group row">
										<label for="register-repsw" class="col-sm-4 col-form-label">@lang("$lang.Confirm Password")</label>
										<div class="col-sm-8">
											<input type="password" class="form-control" id="password-confirm" name="password_confirmation" required="required">
										</div>
									</div>

									<div class="form-group row">
										<label for="email" class="col-sm-4 col-form-label">@lang("$lang.Email")</label>
										<div class="col-sm-8">
											<input type="email" class="form-control" id="email" placeholder="Ex.janedoe@gmail.com" name="email" required>
										</div>
									</div>

									<div class="form-group row">
										<label for="phone" class="col-sm-4 col-form-label">@lang("$lang.Phone Number")</label>
										<div class="col-sm-8">
											<input type="number" class="form-control" id="phone" placeholder="+962" name="phone">
										</div>
									</div>

									<div class="form-group row">
										<label for="age" class="col-sm-4 col-form-label">@lang("$lang.Age")</label>
										<div class="col-sm-8">
											<input type="number" class="form-control" id="age" name="age">
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-6 offset-sm-3">
											<div class="text-center">
												{{ Form::submit(__("$lang.Sign Up") , ['class'=>'btn main1-white-btn submit']) }}
											</div>
										</div>
									</div>
									
								</form>
								<div class="footer">
									<div class="row">
										<div class="col">
											<div class="link">
												<a href="{{route('try-lesson')}}">@lang("$lang.Try a free lesson")</a>
											</div>
										</div>
										<div class="col">
											<div class="link hover">
												<a onclick="toLogin()">@lang("$lang.Already have an account")</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endif -->

				<!--  subscibe  -->
				<!-- <div class="row subscibe-section header-section-wrapper d-none" id="subscribe">
					@foreach($models as $model) 
					
					<div class="col-12 col-md-8 offset-md-2 col-lg-4 offset-lg-0 subscribtion">
						<div class="header-section">
							<div class="container subscription-model-modal"  data-id="{{$model->id}}" data-name="{{$model->name_en}}" data-price="{{$model->price}}" data-display-price="{{$model->display_price}}" data-full-access="{{$model->full_access == 1 ? __($lang.'.Unlimited') : __($lang.'.One Course')}}" data-period="{{ sprintf('%d' ,$model->period_in_days / 30) == 0 ? __($lang.'.Payment Before The Course') : __($lang.'.Payment every').sprintf('%d' , $model->period_in_days / 30) .' '.__($lang.'.months')}}" >
								<div class="title">{{$model->name_en}}</div>
								@if ($model->display_price != '')
								<div class="price"><span class="price-val" >{{$model->display_price}}</span></div>
								@else 
								<div class="price"><span class="" ></span></div>
								@endif
								<div class="details">
									<div class="row no-gutters">
										<div class="col-6">@lang("$lang.Number of Courses")</div>
										<div class="col-6 right" >{{$model->full_access == 1 ? __($lang.'.Unlimited') : __($lang.'.One Course')}}</div>
									</div>
									<div class="row no-gutters ">
										{{ sprintf('%d' ,$model->period_in_days / 30) == 0 ? __("$lang.Payment Before The Course") : __("$lang.Payment every")." ".sprintf('%d' , $model->period_in_days / 30) . ' '. __("$lang.months")}} 
									</div>
								</div>

								<div class="row">
									<div class="col-12 col-sm-8 offset-sm-2 text-center">
										@if($model->full_access == 1)
										<button type="submit" class="btn main2-border-btn submit w-100" onclick="gotoPayment(this);">
											@lang("$lang.Subscribe")
										</button>
										@else
										<a class="btn main2-border-btn submit w-100" href="#courses" >
											@lang("$lang.Browse Courses")
										</a>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
					@endforeach


				</div>
 -->
				<!--  payment  -->
				<div class="row payment-section header-section-wrapper d-none" id="payment" style="display: none;">
					<div class="col-12 col-md-10 offset-md-1">
						<div class="header-section">
							<div class="container" id="payment-data" data-id="" data-price="">
								
								<div class="row subscription">
									<div class="col-md-5 pr-lg-5">
										<div class="title" id="subscription-name">@lang("$lang.annual subscribtion")</div>
										<div class="price"><span class="price-val" id="price">200</span></div>
										<div class="details">
											<div class="row no-gutters">
												<div class="col-sm-6">@lang("$lang.Number of courses")</div>
												<div class="col-sm-6 right" id="number-of-courses">6</div>
											</div>
											<div class="row no-gutters" id="period">
												@lang("$lang.Payment every") 12 @lang("$lang.months")
											</div>
										</div>

									</div>
									<div class="col-md-7">
										<div class="payment-wrapper">
											<div class="mb-3 font-weight-bold">@lang("$lang.Enter Your Coupon Here") <i><small>(@lang("$lang.optional"))</small></i></div>
											<div class="row">
												<div class="col-sm-12">
													<div class="form-check">
														<div class="row">
															<div class="col-sm-12 col-md-8">
																<label>@lang("$lang.Voucher Code"): </label>
																<input type="text" class="form-control" id="voucher-code">
															</div>
															<div class="col-sm-12 col-md-4" style="display: flex;">
																<button class="btn main2-border-btn w-100" style="align-self: flex-end;" id="check-coupon-btn">
																	@lang("$lang.Check")
																</button>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="mb-3 font-weight-bold">@lang("$lang.Choose your payment method:")</div>

											<div class="row">
												<div class="col-sm-12">
													<div class="form-check text-center">
														<!-- <input class="form-check-input" style="margin-top: -23px;" type="radio" value="aa" id="pay-visa" name="pay_method" form="payment-form"> -->
														<label class="form-check-label" for="pay-visa">

															{!! Form::open(['route'=>'sts-payment' , 'id'=>'sts']) !!}
															<input type="hidden" name="coupon" id="coupon-field">
															<input type="hidden" name="model_id" id="model_id_form_fort">
															<div class="clickable visa-img-div "> 
																@if (!Auth::check())
																	<a href="{{route('login')}}" >
																		<img class="visa-img" src="<?php echo config('app.url'); ?>/images/STS.png" alt="Sts PayOne">
																	</a>
																@else
																<img class="visa-img" src="<?php echo config('app.url'); ?>/images/STS.png" alt="Sts PayOne" onclick="submitPayfort()">
																@endif
															</div> 
															{!! Form::close() !!}
												  		</label>
													</div>
												</div><!-- 
												<div class="col-sm-6">
													<div class="form-check text-center">
														<- <input class="form-check-input" type="radio" value="paypal" id="pay-paypal" name="pay_method" form="payment-form"> ->
														<label class="form-check-label" for="pay-paypal">
															{{-- Form::open(['route'=>'paypal-test' , 'id' => 'pay'])   }
																<input type="hidden" name="model_id" id="model-id-form">
																<div class="clickable paypal-img-div" >
																	<img class="paypal-img"  src="<?php echo config('app.url'); ?>/images/payment/paypal.png" alt="paypal" onclick="submitPayment()">
																</div>
															{ Form::close() --}}
												  		</label>
													</div>
												</div> -->
											</div>
											<!--
											<div class="mt-2 mb-1 font-weight-bold">Credit Card Details:</div>

											<form id="payment-form" class="pl-lg-5 mb-4">
												<div class="form-group">
													<input type="text" class="form-control" id="pay-cardnum" name="card_number" placeholder="Card number...">
												</div>
												<div class="form-group">
													<input type="text" class="form-control" id="pay-cardname" name="holder_name" placeholder="Card holder name...">
												</div>
												<div class="form-group">
													<label class="mb-0" for="pay-expire"><span>Expiration Date</span></label>
													<input type="text" class="form-control" id="pay-expire" name="expiration_date" placeholder="DD/MM/YY">
												</div>
												<div class="form-group">
													<input type="text" class="form-control" id="pay-ccv" name="ccv" placeholder="CVV">
												</div>

											</form>
										-->
											<div class="row mb-2">
													<!-- <div class="col-lg-6 text-center">
														<button type="submit" class="btn main2-border-btn submit w-100" form="payment-form">
															@lang("$lang.Make Payment")
														</button>
													</div> -->
													<div class="col-lg-12 text-center"><button class="btn main2-border-btn w-100" onclick="toSubscribe();">@lang("$lang.Cancel")</button></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				
<!-- 			</div>
		</div>
	</section>

 -->	<!-- <section id="courses" class="courses">
		<div class="container">
			<div class="row">
				@for($i=0 ; $i < $levels->count() - $levels->count() %3 ; $i++ )
				<?php $level =$levels[$i];  ?> 
					<div class="col-sm-6 col-md-4">
						<div class="course">
							<a href="{{route('level', $level->slug)}}">
								<div class="overlay">
									<div class="name">{{$level->name_en}}</div>
								</div>
								<img src="<?php echo config('app.url'); ?>{{$level->image_path_en}}" alt="">
							</a>
						</div>
					</div>
				@endfor

				@if($levels->count() % 3 == 1)
					<div class="col-sm-6 col-md-4 offset-4">
						<div class="course">
							<a href="{{route('level',$levels[$levels->count() -1 ]->slug )}}">
								<div class="overlay">
									<div class="name">{{$levels[$levels->count() -1 ]->name_en}}</div>
								</div>
								<img src="<?php echo config('app.url'); ?>{{$levels[$levels->count() -1 ]->image_path_en}}" alt="">
							</a>
						</div>
					</div>

				@endif 


				@if($levels->count() % 3 == 2)
					
					<div class="col-sm-6 col-md-4 offset-xl-2 ">
						<div class="course">
							<a href="{{route('level',$levels[$levels->count() -2 ]->slug )}}">
								<div class="overlay">
									<div class="name">{{$levels[$levels->count() -2 ]->name_en}}</div>
								</div>
							</a>
							<img src="<?php echo config('app.url'); ?>{{$levels[$levels->count() -2 ]->image_path_en}}" alt="">
						</div>
					</div>

					<div class="col-sm-6 col-md-4 ">
						<div class="course">
							<a href="{{route('level',$levels[$levels->count() -1 ]->slug )}}">
								<div class="overlay">
									<div class="name">{{$levels[$levels->count() -1 ]->name_en}}</div>
								</div>
							</a>
							<img src="<?php echo config('app.url'); ?>{{$levels[$levels->count() -1 ]->image_path_en}}" alt="">
						</div>
					</div>

				@endif

			</div>
		</div>
	</section> -->

	<!-- <section id="stars" class="stars">
		<div class="container">
			<div class="title"><span class="line">@lang("$lang.Eurek")</span>@lang("$lang.ian Stars")</div>
			<div class="text">
				{!! $starsText->text !!} 

			</div>
			<div class="lg-sc-imgs d-none d-xl-block">
			<div class="star-img-wrapper" style="top: 70px; left: 50%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[0]) ? $stars[0]: '/images/logo.png')}}">
			</div>
			<div class="star-img-wrapper"  style="top: 50px; left: 78%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[1]) ? $stars[1]: '/images/logo.png')}}">
			</div>
			<div class="star-img-wrapper"  style="top: 165px; left: 65%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[2]) ? $stars[2]: '/images/logo.png')}}">
			</div>
			<div class="star-img-wrapper"  style="top: 200px; left: 33%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[3]) ? $stars[3]: '/images/logo.png')}}">
			</div>
			<div class="star-img-wrapper"  style="top: 240px; left: 48%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[4]) ? $stars[4]: '/images/logo.png')}}">
			</div>
			<div class="star-img-wrapper"  style="top: 350px; left: 64%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[5]) ? $stars[5]: '/images/logo.png')}}">

			</div>
			<div class="star-img-wrapper"  style="top: 260px; left: 77%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[6]) ? $stars[6]: '/images/logo.png')}}">
			</div>
			<div class="star-img-wrapper"  style="top: 300px; left: 20%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[7]) ? $stars[7]: '/images/logo.png')}}">
			</div>
			<div class="star-img-wrapper"  style="top: 390px; left: 40%;">
				<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[8]) ? $stars[8]: '/images/logo.png')}}">
			</div>
			</div>
			<div class="sm-sc-imgs d-xl-none">
			<div class="owl-carousel owl-theme">
				<div class="item">
					<div class="star-img-wrapper">
						<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[0]) ? $stars[0]: '/images/logo.png')}}">
					</div>
				</div>
				<div class="item">
					<div class="star-img-wrapper">
						<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[1]) ? $stars[1]: '/images/logo.png')}}">
					</div>
				</div>
				<div class="item">
					<div class="star-img-wrapper">
						<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[2]) ? $stars[2]: '/images/logo.png')}}">
					</div>
				</div>
				<div class="item">
					<div class="star-img-wrapper">
						<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[3]) ? $stars[3]: '/images/logo.png')}}">
					</div>
				</div>
				<div class="item">
					<div class="star-img-wrapper">
						<img class="star-img img-fluid" src="<?php echo config('app.url'); ?>{{(isset($stars[4]) ? $stars[4]: '/images/logo.png')}}">
					</div>
				</div>
			</div>

			</div>
		</div>
	</section> -->

	



@endsection

@section('scripts')
	<script src="{{URL::asset('js/client/index.js')}}"></script>	

	<script type="text/javascript">		
	toSubscribe();
	@if(session('_previous') == '/register')
		toRegister();
	@endif

	link = window.location.href ;
	if (link.search('#register') != -1 ){
		toRegister();
	}

	@if(session('payment_repsonse') == 'Success')
		swal('Success', 'Payment accepted Successfully' , 'success');
		@php
			session(['payment_repsonse'=>'']);
		@endphp
	@endif

	@if(session('payment_repsonse') == 'Success-COUPON MIRACLE')
		swal('Success', 'Subscription Created Successfully, Your coupon amount is greater than paymnet amount!' , 'success');
		@php
			session(['payment_repsonse'=>'']);
		@endphp
	@endif

	@if(session('payment_repsonse') == 'Coupon Error')
		swal('Success', 'Incorrect Coupon Code' , 'success');
		@php
			session(['payment_repsonse'=>'']);
		@endphp
	@endif


	@if(session('payment_repsonse') == 'Fail')
		swal({
		  type: 'error',
		  title: 'Oops...',
		  text: 'Payment not accepted, try again later'
		});
		@php
			session(['payment_repsonse'=>'']);
		@endphp
	@endif

	 





	</script>

@if($lang=='ar')
	 <script>
  $('#aboutslider').owlCarousel({
  loop: false,
  margin: 30,
  rtl:true,
  // center: true,
  nav: true,
      navText: ["<i class='lni lni-arrow-right {{ $arrowactive }}'></i>", "<i class='lni lni-arrow-left'></i>"],
      dots: false,
  responsive: {
      0: {
          items: 1
      },
      479: {
          items: 1
      },
      768: {
          items: 2
      },
      979: {
          items: 3
      },
      1000: {
          items: 3
      }
      ,
      1300: {
          items: 4
      }
  }
  })

  // $('.owl-carousel').find('.owl-nav').removeClass('disabled');
  // $('.owl-carousel').on('changed.owl.carousel', function (event) {
  //     $(this).find('.owl-nav').removeClass('disabled');
  // });
</script>

@else

	<script>
$('#aboutslider').owlCarousel({
loop: false,
margin: 30,
// center: true,
nav: true,
    navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right {{ $arrowactive }}'></i>"],
    dots: false,
responsive: {
    0: {
        items: 1
    },
    479: {
        items: 1
    },
    768: {
        items: 2
    },
    979: {
        items: 3
    },
    1000: {
        items: 3
    }
    ,
    1300: {
        items: 4
    }
}
})

// $('.owl-carousel').find('.owl-nav').removeClass('disabled');
// $('.owl-carousel').on('changed.owl.carousel', function (event) {
//     $(this).find('.owl-nav').removeClass('disabled');
// });


</script>

@endif




@endsection