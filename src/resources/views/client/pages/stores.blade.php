@extends('client.master')

@section('title', 'Eureka')

@section('body-tag', 'level-page')

@section('body')
@include('client.partials.linked-nav')
<?php $lang = App::getLocale() ?>

</div>
<section class="lessons-need-sliders Kits_slider">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h5 class="comntitle">@lang("$lang.Kits")</h5>
      </div>
    </div>
  </div>

   @if (session('message'))
    <div class="container">
    <div class="row">
      <div class="col-lg-12">
                        <div class="alert alert-success">
                            {{ session('message') }}  <a href="{{ url('/getcart') }} ">Go to cart</a>
                        </div>
                      </div>
                    </div>
                  </div>
                        @endif


  <div class="lessons-slider-area Kits_slider_area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="owl-carousel owl-theme" id="Kits">
            @foreach($kits as $kit)
            <div class="item">
              <div class="style-full style-half">
                <div class="main-pic cursor" onclick="location.href='{{ route('kit-page' , $kit->id) }}'"> 
                    @if(count($kit->images) > 0 )
                    <?php $p2=1;?>
                        @foreach($kit->images as $image)
                           <?php if(count($kit->images) == $p2){ ?>
                                @if(file_exists($image->img_src))
                                    <img src="{{ $image->img_src }}" alt="">
                                @else
                                    <img src="<?php echo config('app.url'); ?>/css/client/images/kit-1.jpg" alt="ter">
                                @endif
                            <?php }
                            $p2++; ?>
                        @endforeach
                    @else
                        <img src="<?php echo config('app.url'); ?>/css/client/images/kit-1.jpg" alt="">
                    @endif
                     
                </div>
                <div class="product-title">
                  <h4 class="cursor" onclick="location.href='{{ route('kit-page' , $kit->id) }}'">{{ $kit->name_en }}</h4>
                  <p>{{ $kit->itemsCount }} @lang("$lang.Number of Items")</p>
                  <span>
                      @if($kit->active == '1' && $kit->available == '1')
                        <img src="<?php echo config('app.url'); ?>/css/client/images/right-icon.png" alt="">@lang("$lang.Available in store")
                    @else
                        <img src="<?php echo config('app.url'); ?>/css/client/images/cross-icon.png" alt="">@lang("$lang.Not Available in store")
                    @endif
                  </span>
                  <div class="sub-unsub">
                    <p><span>{{ Helper::showCurrency($kit->maxPrice) }}</span></p>
                    
                  </div>
                </div>
                @if($kit->active == '1' && $kit->available == '1')
                    <form method="POST" action="{{ route('addtocart') }}">
                        @csrf
                        <input type="hidden" name="itemid" value="{{ $kit->id }}">
                        <input type="hidden" name="type" value="Kit">
                        <input type="hidden" name="price" value="{{$kit->maxPrice}}">
                        <input type="hidden" name="supplierId" value="{{$kit->supplierId}}">
                        <input type="hidden" name="qty" value="1">
                        <button type="submit" >@lang("$lang.Add to Cart")</button>
                    </form>
                @else
                    <button disabled="disabled">@lang("$lang.Add to Cart")</button>
                @endif
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
   </div>
</section>

<section class="productlist">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-lg-6 col-md-6">
        <h2 class="comntitle">@lang("$lang.Items")</h2>
      </div>
      
      <div class="col-xl-6 col-lg-6 col-md-6">
        <div class="productsearch">
          <input type="text" placeholder="Search ny name" id="search" name="search">
          <button class="fa fa-search" id="searchitem"></button>
        </div>
      </div>
    </div>
    <div id="mydatareplace">
    <div class="row">
        @foreach($items as $item)
        <div class="col-xl-4 col-lg-4 col-md-6">
            <div class="style-full style-half">
              <div class="main-pic"> 
                @if(isset($item->images))
                    <?php $p=1;?>
                    @foreach($item->images as $image)
                        <?php if(count($item->images)== $p){ ?>
                            @if(file_exists($image->img_src))
                            <img src="{{ $image->img_src }}" alt="">
                            @else
                                <img src="<?php echo config('app.url'); ?>/css/client/images/lessons-slider-pic2.png" alt="">
                            @endif
                        <?php }
                        $p++; ?>
                    @endforeach
                @else
                    <img src="<?php echo config('app.url'); ?>/css/client/images/lessons-slider-pic2.png" alt="">
                @endif
              </div>
              <div class="product-title">
                @if(session('lang') == 'ar')
                    <h4>{{ $item->items->name_en }}</h4>
                @else
                    <h4>{{ $item->items->name_en }}</h4>
                @endif
                <p>&nbsp;</p>
                @if($item->qty > 0)
                    <span>
                        <img src="<?php echo config('app.url'); ?>/css/client/images/right-icon.png" alt="">@lang("$lang.Available in store")
                    </span>
                @else
                    <span>
                        <img src="<?php echo config('app.url'); ?>/css/client/images/cross-icon.png" alt="">@lang("$lang.Not Available in store")
                    </span>
                @endif
                <div class="sub-unsub">
                  <p><span>{{ Helper::showCurrency($item->maxPrice) }}</span></p>
                  
                </div>
                @if($item->qty > 0)
                    <form method="POST" action="{{ route('addtocart') }}">
                    @csrf
                    <input type="hidden" name="itemid" value="{{ $item->id }}">
                    <input type="hidden" name="type" value="Item">
                    <input type="hidden" name="price" value="{{$item->maxPrice}}">
                    <input type="hidden" name="supplierId" value="{{$item->supplierId}}">
                    <input type="hidden" name="qty" value="1">
                    
                        <button type="submit" >@lang("$lang.Add to Cart")</button>
                    </form>
              @else
                <button disabled="disabled">@lang("$lang.Add to Cart")</button>
              @endif
              </div>        
            </div>
        </div>
        @endforeach
    </div>

    <div class="row">
      <div class="col-xl-12">
        <nav aria-label="Page navigation example justify-content-center">
          {!! $items->render() !!}
          
        </nav>
      </div>
    </div>
    </div>
  </div>  
</section>
@endsection


@section('scripts')
<script type="text/javascript">

$("#searchitem").click(function(){
    var page = '';
    var search = $('#search').val();
    if(search != ''){
        getData(page);
    }
    

});

$(window).on('hashchange', function() {
    if (window.location.hash) {
        var page = window.location.hash.replace('#', '');
        if (page == Number.NaN || page <= 0) {
            return false;
        }else{
            getData(page);
        }
    }
});
$(document).ready(function()
{
    $(document).on('click', '.pagination a',function(event)
    {
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        event.preventDefault();
        var myurl = $(this).attr('href');
        var page=$(this).attr('href').split('page=')[1];
        getData(page);
    });
});
function getData(page){
    var search = $('#search').val();
    var siteurl= "<?php echo config('app.url'); ?>";
        $.ajax(
        {
            url: '?page=' + page,
            type: "get",
            datatype: "html",
            data:{'search':search}
        })
        .done(function(data)
        {
            $("#mydatareplace").empty().html(data);
            location.hash = page;
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
              alert('No response from server');
        });
}
</script>
@if($lang == 'ar')
<script>
  $('#Kits').owlCarousel({
  loop:true,
  center: false,
  margin: 30,
  nav: true,
  dots:false,
  rtl:true,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 1
    },
    768: {
      items:2
    },
    1000: {
      items:2
    },
    1100: {
      items: 3
    }
  }
})
</script>
@else
<script>
  $('#Kits').owlCarousel({
  loop:true,
    center: false,
  margin: 30,
  nav: true,
  dots:false,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 1
    },
    768: {
      items:2
    },
    1000: {
      items:2
    },
    1100: {
      items: 3
    }
  }
})
</script>
@endif 
@endsection