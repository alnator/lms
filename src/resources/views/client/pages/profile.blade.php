@extends('client.master')


@section('title', 'Eureka')


@section('body-tag' , 'profile-page')
@section('body')

@include('client.partials.linked-nav')


<?php $lang= App::getLocale() ?>

<section class="new-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="#">Courses</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">@lang("$lang.Profile")</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</section>




					
				




<section class="user_profile"> 
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<h1>@lang("$lang.Profile")</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-2 col-lg-3 col-md-3">
				<div class="user_img">
					<img src="<?php echo config('app.url'); ?>/public{{$user->image_path}}" alt="">
				</div>
				<div class="upload text-center">
							{{ Form::open(['route'=>'upload-pic' , 'files'=>true , 'id'=>'upload-pic'])}}
							{{ Form::file('image' , [ 'hidden' =>'hidden' , 'id' =>'pic'] )}}
							<a class="session-resume btn main2-border-btn-dark" style="font-size: 12px;" onclick="upload()">   <i class="fas fa-cloud-upload-alt"></i> @lang("$lang.Change picture")</a>
							{{Form::close()}}
						</div>
			</div>
			<div class="col-xl-10 col-lg-9 col-md-9">
				<div class="user_detail">
					<a href="{{ url('change-pass') }}" class="setting">
						<img src="<?php echo config('app.url'); ?>/css/client/images/setting.png" alt="">
					</a>
					<h4>{{$user->name}}</h4>
					<h5>{{$user->email}}</h5>
					<p>{{$user->pnone}}</p>
					
					
				</div>
			</div>
		</div>
	</div>
</section>




<section class="profile_courses">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <h2 class="comntitle text-center">@lang("$lang.my courses")</h2>
        <div class="owl-carousel owl-theme" id="profile_courses">
          
          	@foreach($courses as $course)

          	
          		@if($course->progress<100)
          <div class="item">
            <div class="coursesbox">
              <h4>{{ $course->title_en }}@if(isset($course->progress)) ({{$course->progress}}%) @endif </h4>
              <img src="<?php echo config('app.url'); ?>{{isset($course->image_path) ? $course->image_path : '/images/courses_page/cover_game_development.png'}} " alt="" class="courcesstatus" data-id="course_{{$course->id}}">
              <p>{{ $course->overview_en }}</p>
             

              <aside>
               <small>{{ count($course->lession)}} @lang("$lang.Lessons") ({{ $course->courseTime }} @lang("$lang.Hours"))</small>
                <span>{{ Helper::showCurrency($course->price) }}</span>

              </aside>
              <a href="{{route('course-page', $course->slug)}}" @if(Helper::showPlanStatus($course->id) !='')  style="background: #333d77;" @endif >@if(Helper::showPlanStatus($course->id) !='') {{ Helper::showPlanStatus($course->id) }} @else @lang("$lang.Get Started") @endif</a>

            </div>
          </div>
         @endif
          	@endforeach
          
         
          
          
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</section>



<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="
    background: #fff;
">

    <!-- Modal content-->
    <div class="modal-content">


<section class="Accomplishments myli">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				
				
	
	
					
							
@foreach($courses as $course)
<ul class="course_{{$course->id}} ulcls" style="display: none;">
	<li>
		<div class="acomptext">
			<h3>{{$course->title_en}}</h3>
			@if(isset($course->progress))<small class="date">{{$course->progress}}%</small>@endif
		</div>
	</li>
	<li>
		<div class="col-md-6">
	@for($i = 0 ; $i < $course->session->count() ; $i++)
		<?php $session =  $course->session[$i] ;?>
				
				@if($session->done == 1) @else <i class="fa fa-times" aria-hidden="true"></i> @endif
				@if($session->done == 1) <i class="fa fa-check"></i> @endif
				@lang("$lang.Session") {{$session->session_number}}<br>
			
	@endfor
	<br>
	<div class="btn btn-success btn-xs">@lang("$lang.Finish")</div>
							</div>
							<div class="col-md-6 text-right"> 				
											
									@foreach($course->session as $session)
									@if($session->done == null)
									
									<br>	
									@lang("$lang.Session") {{$session->session_number}}
												
									<br>
										{{$course->title_en}}
									<br>

									{{intval($session->time / 60)}}:{{intval($session->time % 60)}} @lang("$lang.min")
									<br><br>			
									<a class="btn btn-info" href="{{route('session' , $session->id)}}">@lang("$lang.RESUME")</a>
							</div>		
									</li>
											
								<?php break; ?>
								@endif
								@endforeach
								
							</ul>
						
						@endforeach
						

				<!-- <button>Load more</button> -->
			</div>
		</div>
	</div>
</section>

<div class="modal-footer">
        <button type="button" class="nextbtn" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>




@if(isset($accomplishments) && count($accomplishments)>0)
<section class="Accomplishments">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<h3>@lang("$lang.accomplishments")</h3>
				<ul>

						@foreach($accomplishments as $accom)


					<li>
						<div class="iconimg">
							<img src="<?php echo config('app.url'); ?>/css/client/images/check.png" alt="">
						</div>
						<div class="acomptext">
							<h3>{{$accom->title_en}}</h3>
							<small class="date">{{$accom->progress}}%</small>
						</div>
					</li>
					@endforeach
				</ul>
				<!-- <button>Load more</button> -->
			</div>
		</div>
	</div>
</section>



@endif

					

@endsection
@section ('scripts')

<script type="text/javascript">
$(".courcesstatus").click(function(){

	//$('#myModal').modal('toggle');
$('#myModal').modal('show');
//$('#myModal').modal('hide');


 var data=$(this).data('id');
$(".ulcls").hide();
 $("."+data).toggle();


});
</script>



	<script type="text/javascript">
		function toAccomp(){
			$('#courses').addClass('d-none');
			$('#accom').removeClass('d-none');
			$('#courses-label').removeClass('active')
			$('#accom-label').addClass('active');
		}
		function toMyCourses(){
			$('#accom').addClass('d-none');
			$('#courses').removeClass('d-none');
			$('#accom-label').removeClass('active');
			$('#courses-label').addClass('active')
		}
		if (window.location.href.indexOf("accomplishments") != -1){
			toAccomp();
		}
	</script>
	<script type="text/javascript">
		function upload(){
			$('#pic').click();
			$('#pic').change(function (){
				// alert("hi");
				$('#upload-pic').submit();
			});
		}
	</script>

<style type="text/css">
	.myli li{
		    padding: 15px 25px;
	}

	.myli{
		margin: 0px 0;
	}

	.myli ul{
		padding: 20px 0px 20px 25px;
    margin-top: 10px;
	}

 .modal-footer .nextbtn {
    margin-right: 0;
    background: #ff7360;
    border-radius: 30px;
    padding: 5px 30px 02px;
    color: #fff;
    font-size: 18px;
    font-weight: 600;
    border: none;
}
</style>
@endsection
