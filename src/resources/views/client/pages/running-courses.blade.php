@extends('client.master')


@section('title', 'Eureka')

@section('styles')
<style type="text/css">
	.row-active::before{
		border-bottom: 2px solid #ED2790 !important;
	}
	.circle-active{
	    background-color: #ED2790 !important;		
	}
	.img-fluid {
		width: 100%;
	}
</style>

@endsection
@section('body-tag' , 'running-courses-page')
@section('body')
	<?php $lang = App::getLocale() ?>
	<header class="all-nav">
		@include('client.partials.linked-nav')
	</header>
	<div class="top-logo">
		<a href="{{route('home')}}">
			<img src="<?php echo config('app.url'); ?>/images/logo.png" alt="" width="100px">
		</a>
	</div>

<div class="page-wrapper">
	<div class="container-fluid">
		<div class="row no-gutters">
			<div class="col-lg-3">
				<div class="profile-details text-center">
					<div class="prof-img-wrapper">
						<img class="prof-img img-fluid" src="{{$user->image_path}}" alt="">
					</div>
					<div class="prof-links">
						<ul class="list">
							<li class="link active">
								<div class="row-style"><span class="line"></span><span class="circle"></span></div><a href="{{route('profile')}}">@lang("$lang.my courses")</a>
							</li>
							<li class="link">
								<div class="row-style"><span class="line"></span><span class="circle"></span></div><a href="{{route('profile')}}#accomplishments">@lang("$lang.accomplishments")</a>
							</li>
							<!-- <li class="link">
								<div class="row-style"><span class="line"></span><span class="circle"></span></div><a href="">levels</a>
							</li> -->
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="container" style="overflow: hidden;">
					<div class="my-courses">
						<div class="course">
							<div class="course-img-wrapper">
								<img class="course-img img-fluid" src="{{$course->image_path}}" alt="">

								<div class="course-info-wrapper">
									<div class="container">
										<div class="row">
											<div class="col-9">
												<div class="course-info">
													<!-- <div class="course-level">Level Three: Electronics</div> -->
													<div class="course-name">{{$course->title_en}}</div>
													<div class="course-desc">{{$course->overview_en}}</div>
												</div>
											</div>
											<div class="col-3">
												<div class="cource-percentage">
													<div class="flex-wrapper">
														<div class="single-chart m-auto">
															<svg viewbox="0 0 36 36" class="circular-chart orange">
															  <path class="circle-bg"
																d="M18 2.0845
																  a 15.9155 15.9155 0 0 1 0 31.831
																  a 15.9155 15.9155 0 0 1 0 -31.831"
															  />
															  <path class="circle"
																stroke-dasharray="{{$course->progress}}, 100"
																d="M18 2.0845
																  a 15.9155 15.9155 0 0 1 0 31.831
																  a 15.9155 15.9155 0 0 1 0 -31.831"
															  />
															  <text x="18" y="20.35" class="percentage">{{$course->progress}}%</text>
															</svg>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="course-content">
									<div class="course-progress text-center">
										<hr class="progress-line" style="width: calc(100% - 20px)" >
										<hr class="progress-line done" style="width: calc({{$course->progress}}% - 20px)">
										<div class="row">
											@for($i = 0 ; $i < $course->session->count() ; $i++)
											<?php $session =  $course->session[$i] ;?>
											<div class="col">
												<div class="session-circle-wrapper">
													<div class="session-number {{$session->done == 1 ? 'd-none' : '' }} ">{{$i + 1}}</div>
													<div class="session-done  {{$session->done == 1 ? '' : 'd-none' }} "><img src="<?php echo config('app.url'); ?>/images/icons/check.png" alt="session done" height="30px"></div>
													<div class="session-name ">@lang("$lang.Session") {{$session->session_number}}</div>
												</div>
											</div>
											@endfor
										</div>
										
									</div>

								@foreach($course->session as $session)
									@if($session->done == null)
									<div class="session-info">
										<div class="container">
											<div class="row">
												<div class="col-md-3">
													<div class="session-name">@lang("$lang.Session") {{$session->session_number}}</div>
												</div>
												<div class="col-md-6">
													<div class="session-desc"></div>
													<div>
														{{$course->description_en}}
													</div>
													<div class="session-time">{{intval($session->time/ 60 )}}:{{intval($session->time % 60 ) < 10 ? '0'.intval($session->time% 60 ):intval($session->time% 60 ) }} @lang("$lang.min")</div>
												</div>
												<div class="col-md-3">
													<a class="session-resume btn main2-border-btn-dark w-100" href="{{route('session' , $session->id)}}">@lang("$lang.RESUME")</a>
												</div>
											</div>
										</div>
									</div>
								
								<?php break; ?>
								@endif
								@endforeach

								<div class="container">
									<div class="sessions-wrapper">
										
										@foreach($course->session as $session)
										@if($session->video->count() != 0 )
										<div class="session">
											<div class="session-name">@lang("$lang.Session") {{$session->session_number}}</div>
											<ul class="lessons">
												
												@foreach($session->video as $video)
												<li class="lesson">
													<div class="row-style"><span class="circle"></span></div>
													<div class="row">
														<div class="col-4 col-sm-5 col-md-4 col-lg-5"><a class="lesson-name" href="{{route('lesson',$video->id)}}">
															{{$video->name_en}}
														</a></div>
														<div class="col-5 col-sm-4 col-md-6 col-lg-4 p-0">
															<hr>
														</div>
														<div class="col-3 col-sm-3 col-md-2 col-lg-3">
															<div class="period"><span class="period-val">{{intval($video->estimated_time /60) }}:{{$video->estimated_time %60 < 10 ? '0'.$video->estimated_time %60 : $video->estimated_time %60}}</span><span class="period-icon">
															@if ($video->free != 1)

																@if($video->done == 1 )
																	<i class="far fa-check-circle" style="color: green;margin-right: 3px"></i>
																@else
																	<img src="<?php echo config('app.url'); ?>/images/icons/Group%20224.png" width="18px" alt="" style="height: 17px;margin-right: 3px">
																@endif
																@if ($video->solved == 1 )
																	<i class="far fa-check-circle" style="color: green"></i>
																@elseif ($video->solved == 0)
																	<img src="<?php echo config('app.url'); ?>/images/icons/Group%20224.png" width="18px" style="height: 17px;" alt=""> 
																@endif
															@endif 
															</span></div>
														</div>
													</div>
												</li>
												@endforeach
											</ul>
										</div>
										@endif
										@endforeach
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection