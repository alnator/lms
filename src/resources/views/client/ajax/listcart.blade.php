<?php $lang = App::getLocale() ?>
@if(isset($cartitem) &&  count($cartitem) > 0)
   
         
          @foreach($cartitem as $item)
          @if($item->type == 'Course')
          <div class="style-full style-half">
            <div class="main-pic"> <img src="<?php echo config('app.url'); ?>/public{{ $item->getCourse->image_path }}" alt=""> </div>
            <div class="product-title">
              <img src="images/cross-icon.png" class="cross" alt=""/>
              @if($lang == 'ar')
              <h4>{{ $item->getCourse->title_ar }}</h4>
              @else
              <h4>{{ $item->getCourse->title_en }}</h4>
              @endif
              <p>{{ $item->qty }} @lang("$lang.Number of Items")</p>
              <span>
                @if($item->getCourse->active == '1')
                <img src="<?php echo config('app.url'); ?>/css/client/images/right-icon.png" alt="">@lang("$lang.Available in store") @else
                <img src="<?php echo config('app.url'); ?>/css/client/images/cross-icon.png" alt="">@lang("$lang.Not Available in store")
                @endif
              </span>
              <div class="sub-unsub">               
                <p>{{ Helper::showCurrency($item->price) }}<span>{{ Helper::showCurrency($item->final_total) }}</span></p>
              </div>
              <!-- <div class="number">
                <span class="minus">-</span>
                <input type="text" value="1"/>
                <span class="plus">+</span>
              </div> -->
              <button class="btn btn-danger btn-sm removecart" data-id="{{$item->id}}"><i class="fa fa-trash-o"></i></button>
            </div>
          </div>
          @elseif($item->type == 'Kit')
          <div class="style-full style-half">
            <div class="main-pic"> 
                    <?php $p1=1;?>
                    @foreach($item->getkit->images as $image)
                        <?php if(count($item->getkit->images)== $p1){ ?>
                            @if(file_exists($image->img_src))
                                <img src="{{ $image->img_src }}" alt="">
                            @else
                                <img src="<?php echo config('app.url'); ?>/css/client/images/kit-1.jpg" alt="">
                            @endif
                        <?php }
                        $p1++; ?>
                    @endforeach
            </div>
            <div class="product-title">
              <img src="images/cross-icon.png" class="cross" alt=""/>
              @if($lang == 'ar')
              <h4>{{ $item->getkit->name_ar }}</h4>
              @else
              <h4>{{ $item->getkit->name_en }}</h4>
              @endif
              <p>{{ count($item->getkit->items) }} @lang("$lang.Number of Items")</p>
              <span>
                @if($item->getKitAvailable($item->product_id) == '1')
                <img src="<?php echo config('app.url'); ?>/css/client/images/right-icon.png" alt="">@lang("$lang.Available in store") @else
                <img src="<?php echo config('app.url'); ?>/css/client/images/cross-icon.png" alt="">@lang("$lang.Not Available in store")
                @endif
              </span>
              <div class="sub-unsub">               
                 <p>{{ Helper::showCurrency($item->price) }}<span>{{ Helper::showCurrency($item->final_total) }}</span></p>
              </div>
              <div class="number">
                <span class="minus quantity-left-minus minus_{{$item->id}}" data-type="minus" data-id="{{$item->id}}" >-</span>
                <input type="text" id="quantity_{{$item->id}}" name="quantity" value="{{ $item->qty }}" min="1" max="100"/>
                <span class="plus quantity-right-plus" data-type="plus" data-id="{{$item->id}}" >+</span>
              </div>
              <button class="btn btn-danger btn-sm removecart" data-id="{{$item->id}}"><i class="fa fa-trash-o"></i></button>
            </div>
          </div>
          @elseif($item->type == 'Item')
          <div class="style-full style-half">
            <div class="main-pic"> 
              <?php $p=1;?>
                    @foreach($item->getKitItem->items->images as $image)
                        <?php if(count($item->getKitItem->items->images)== $p){ ?>
                            @if(file_exists($image->img_src))
                            <img src="{{ $image->img_src }}" alt="">
                            @else
                                <img src="<?php echo config('app.url'); ?>/css/client/images/lessons-slider-pic2.png" alt="">
                            @endif
                        <?php }
                        $p++; ?>
                    @endforeach 
            </div>
            <div class="product-title">
              <h4>{{ $item->getKitItem->items->name_en }}</h4>
              
              <p>1 @lang("$lang.Number of Items")</p>
              <span>
                @if($item->getItemAvailable($item->getKitItem->items->id,$item->supplierid) > 0)
                <img src="<?php echo config('app.url'); ?>/css/client/images/right-icon.png" alt="">@lang("$lang.Available in store") @else
                <img src="<?php echo config('app.url'); ?>/css/client/images/cross-icon.png" alt="">@lang("$lang.Not Available in store")
                @endif
              </span>
              <div class="sub-unsub">               
                 <p>{{ Helper::showCurrency($item->price) }}<span>{{ Helper::showCurrency($item->final_total) }}</span></p>
              </div>
              <div class="number">
                <span class="minus quantity-left-minus minus_{{$item->id}}" data-type="minus" data-id="{{$item->id}}" >-</span>
                <input type="text" id="quantity_{{$item->id}}" name="quantity" value="{{ $item->qty }}" min="1" max="100"/>
                <span class="plus quantity-right-plus" data-type="plus" data-id="{{$item->id}}" >+</span>
              </div>
              <button class="btn btn-danger btn-sm removecart" data-id="{{$item->id}}"><i class="fa fa-trash-o"></i></button>
            </div>
          </div>
          @endif
          @endforeach
          
      
    @else
      <div class="row">
        <div class="col-xl-12">
          <h4 class="text-center">@lang("$lang.Record Not Found")</h4>
        </div>
      </div>
    @endif