

<footer>
  <div class="container">
    <div class="row">
      <div class="col-xl-7 col-lg-7 col-md-12">
        <div class="row">
          <div class="col-xl-4 col-lg-4 col-md-4">
            <h6>@lang("$lang.Company")</h6>
            <a href="{{ url('page/about')}}">@lang("$lang.About")</a>
            <a href="{{ url('page/careers')}}">@lang("$lang.Careers")</a>
            <a href="{{ url('page/press')}}">@lang("$lang.Press")</a>
            <a href="{{ url('page/become-a-teacher')}}">@lang("$lang.Become a Teacher")</a>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4">
            <h6>@lang("$lang.Community")</h6>
            <a href="{{route('courses')}}">@lang("$lang.Courses")</a>
            <a href="{{ url('page/become-a-member')}}">@lang("$lang.Become a Member")</a>
            <a href="{{ url('/free-classes')}}">@lang("$lang.Free Classes")</a>
            <a href="{{ url('page/refer-a-friend')}}">@lang("$lang.Refer a Friend")</a>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4">
            <h6>@lang("$lang.Eureka Academy")</h6>
             
          
           <a href="{{ url('page/about')}}">@lang("$lang.About")</a>
            <a href="{{ url('page/highlights')}}">@lang("$lang.Highlights")</a>
            <a href="{{ url('/achievements')}}">@lang("$lang.Achievments")</a>
            <a href="{{ url('/gallery')}}">@lang("$lang.Gallery")</a>
          
    
          </div>          
        </div>
      </div>
      <div class="col-xl-5 col-lg-5 col-md-12">
        <h6>@lang("$lang.Got a question?")</h6>
        <form id="testc">
          <textarea placeholder='@lang("$lang.What do you need help with?")' id="contact-message" required="required"></textarea>
          <div class="inputdiv">
            <input type="text" id="contact-name" placeholder='@lang("$lang.Full name")' required="required">
            <input type="email" id="contact-email" placeholder='@lang("$lang.Email Address")' required="required">
          </div>
          <button id="submitContactUsForm">@lang("$lang.Submit")</button>
          <span class="spanc"></span>
        </form>
      </div>
    </div>
  </div>
</footer>


  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <div class="bottom_footer">
        <div class="otherlink">



          <a href="{{ url('page/terms-of-user')}}">@lang("$lang.Terms of user")</a>
          <a href="{{ url('page/help')}}">@lang("$lang.Help")</a>
          <a href="{{ url('contact-us')}}">@lang("$lang.Contact Us")</a>
        </div>
        <div class="sociallink">
          <a href="{{$footer->twitter}}">
          <img src="{{URL::asset('css/client/images/social-1.png')}}" 
          onmouseover="this.src='{{URL::asset('css/client/images/social-1-1.png')}}'" 
          onmouseout="this.src='{{URL::asset('css/client/images/social-1.png')}}'" />
          </a>
          <a href="{{$footer->facebook}}">
          <img src="{{URL::asset('css/client/images/social-2.png')}}" 
          onmouseover="this.src='{{URL::asset('css/client/images/social-2-2.png')}}'" 
          onmouseout="this.src='{{URL::asset('css/client/images/social-2.png')}}'" />
          </a>
          <!-- <a href="{{$footer->facebook}}">
          <img src="{{URL::asset('css/client/images/social-3.png')}}" 
          onmouseover="this.src='{{URL::asset('css/client/images/social-3-3.png')}}'" 
          onmouseout="this.src='{{URL::asset('css/client/images/social-3.png')}}'" />
          </a> -->
          <a href="{{$footer->instagram}}">
          <img src="{{URL::asset('css/client/images/social-4.png')}}" 
          onmouseover="this.src='{{URL::asset('css/client/images/social-4-4.png')}}'" 
          onmouseout="this.src='{{URL::asset('css/client/images/social-4.png')}}'" />
          </a>
          <!-- <a href="{{$footer->youtube}}">
          <img src="{{URL::asset('css/client/images/social-5.png')}}" 
          onmouseover="this.src='{{URL::asset('css/client/images/social-5-5.png')}}'" 
          onmouseout="this.src='{{URL::asset('css/client/images/social-5.png')}}'" />
          </a> -->
        </div>
      </div>
        <div class="row">
          <div class="col-sm-1">
            <img src="<?php echo config('app.url'); ?>/public/images/visa.png" class="image-object">
          </div>
          <div class="col-sm-1">
            <img src="<?php echo config('app.url'); ?>/public/images/master_card.png" class="image-object">
          </div>
        </div>
    </div>
  </div>
</div>


