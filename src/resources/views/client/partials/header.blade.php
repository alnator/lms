<?php $lang = App::getLocale() ?>
	

@if(session('lang') == 'ar')

    <link href="{{URL::asset('css/client/css-rtl/bootstrap-rtl.min.css')}}" rel="stylesheet">

@else
	
	<link href="{{URL::asset('css/client/css/bootstrap.css')}}" rel="stylesheet">

@endif

    <link href="{{URL::asset('css/client/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/client/css/LineIcons.css')}}" rel="stylesheet">
	  <link href="{{URL::asset('css/client/css/owl.carousel.min.css')}}" rel="stylesheet">
@if(session('lang') == 'ar')

	 <link href="{{URL::asset('css/client/css-rtl/style.css')}}" rel="stylesheet">
	 <link href="{{URL::asset('css/client/custom.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/client/css-rtl/responsive.css')}}" rel="stylesheet">

@else

    <link href="{{URL::asset('css/client/css/style.css')}}" rel="stylesheet">
        <link href="{{URL::asset('css/client/custom.css')}}" rel="stylesheet">

    <link href="{{URL::asset('css/client/css/responsive.css')}}" rel="stylesheet">

@endif



 <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

	<style>
	.cookie-banner {
	  position: fixed;
	  bottom: 40px;
	  left: 10%;
	  right: 10%;
	  width: 80%;
	  padding: 5px 14px;
	  display: flex;
	  align-items: center;
	  justify-content: space-between;
	  background-color: #eee;
	  border-radius: 5px;
	  box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);
	}
	.close {
	  font-size: 23px;
	  height: 20px;
	  background-color: #777;
	  border: none;
	  color: white;
	  border-radius: 2px;
	  cursor: pointer;
	  padding: 0px 3px;
	}
		

		
		
	</style>

	@yield('styles')