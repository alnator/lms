<?php $lang = App::getLocale() ?>


<header class="inner-header">
  <div class="container">
    <div class="row">
      <div class="col-xl-2 col-lg-2 col-md-12">
        <div class="mobbardiv">
          <div class="logo"> <a href="{{route('home')}}"><img src="{{URL::asset('css/client/images/logo.png')}}" alt=""></a> </div>
          <div class="navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
          </div>
        </div>
      </div>
      <div class="col-xl-10 col-lg-10 col-md-12">
        <nav class="navbar navbar-expand-lg navbar-light">
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              
             @foreach($slugs as $slug)

             @if(strtolower(request()->segment(count(request()->segments())))==strtolower($slug->slug))
					<li class="nav-item active">
			@else			
				<li class="nav-item {{ (request()->is(strtolower($slug->slug))) ? 'active' : '' }}">
			@endif		
						<a class="nav-link" href="{{route('slug', $slug->slug)}}">{{$slug->title_en}}</a>
					</li>
					@endforeach
            </ul>
            <div class="rightmenu">
              <select onchange="javascript:location.href = this.value;">
                <option value="{{route('change-language', 'ar')}}" @if(session('lang') == 'ar') selected @endif>عربي</option>
                <option value="{{route('change-language', 'en')}}" @if(empty(session('lang') ) || session('lang') == 'en') selected @endif>Eng</option>
              </select>

               <!-- <a href="{{route('kits')}}" onclick="showKit()">
              	<i class="fa fa-star"></i>
              </a> -->


              <a href="javascript:void" class="searchlink"> <i class="fa fa-search"></i> </a>
              <a href="{{route('getcart')}}"> <i class="fa fa-shopping-cart"></i> </a>

              @if(Auth::check())
               <a href="{{route('transaction')}}" title="Transaction">
                <i class="fa fa-first-order"></i>
              </a>
              @endif


              <ul class="list-unstyled signup-login">

              	@if(!Auth::check())
								
								 <li class="login-btn"><a href="javascript:void"  onclick="{{!Auth::check() ? 'toLogin();' : ''}}" title="@lang("$lang.Login")">@lang("$lang.Login")
								</a></li>

								 <li><a href="javascript:void" onclick="toRegister();" title="@lang("$lang.Create an account")">@lang("$lang.Sign Up")							
								</a></li>
							@else

							<li class="login-btn"><a href="{{route('profile')}}" title="@lang("$lang.Profile")">@lang("$lang.Profile")</a></li>

							<li>{{ Form::open(['route'=>'logout' , 'id'=>'logout']) }}
								<a href="javascript:void" onclick="$('#logout').submit();" title="@lang("$lang.Logout")">@lang("$lang.Logout")</a>
							
							{{ Form::close()}}
							</li>
							
							@endif



               
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
  </div>
  <div class="searchdiv">
    <div class="container">
      <div class="row">
        <div class="col-xl-12">
          <div class="serchbox"> <i class="fa fa-search"></i>
            <input type="text" placeholder="Search here">
          </div>
        </div>
      </div>
    </div>
  </div>
</header>



