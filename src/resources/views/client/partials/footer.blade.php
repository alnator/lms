<?php $lang = App::getLocale() ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{URL::asset('css/client/js/bootstrap.min.js')}}"></script>
    

    <script src="{{URL::asset('css/client/js/owl.carousel.min.js')}}"></script>
   <!--  <script src="js/owl.carousel-right.js"></script> -->

        <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
	<!--  mustache  -->
	<script src="{{URL::asset('js/client/mustache.min.js')}}"></script>
	<!--  checkbox  -->
	<script src="{{URL::asset('js/client/core.js')}}"></script>
	<!-- <script src="{{URL::asset('js/client/checkbox.js')}}"></script> -->
	<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>

<script>
  $(".searchlink").click(function(){
    $(".searchdiv").slideToggle();
  });
</script>

@if($lang=='ar')
<script>
$('#Related_items').owlCarousel({
  loop:true,
  center: false,
  margin: 40,
  nav: true,
  rtl:true,
  dots:false,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
   768: {
      items:2
    },
    1000: {
      items:3
    },
    1100: {
      items: 3
    }
   }
  })
</script> 


<script>
  $('#courses').owlCarousel({
  margin: 25,
  nav: false,
  dots:true,
  rtl:true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
    1000: {
      items:3
    },
    1100: {
      items: 4
    }
  }
})
</script>
<script>
  $('#Eureka_Stars').owlCarousel({
    loop:true,
  margin: 25,
  rtl:true,
  nav: true,
  dots:false,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
    900: {
      items:3
    },
    1000: {
      items:4
    },
    1100: {
      items: 5
    }
  }
})


    $('#vision_partner').owlCarousel({
    loop:true,
  margin: 35,
  rtl:true,
  nav: true,
  dots:false,
  autoplay:true,
  autoplayTimeout:1000,
  autoplayHoverPause:true,
  
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
    900: {
      items:3
    },
    1000: {
      items:4
    },
    1100: {
      items: 5
    }
  }
})

  
</script>
<script>
  $('#mob_Testimonials').owlCarousel({
    loop:true,
  margin: 25,
  nav: true,
  dots:false,
  rtl:true,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 1
    },
    1000: {
      items:1
    },
    1100: {
      items: 1
    }
  }
})
</script>
<script>
  $('#profile_courses').owlCarousel({
  margin: 25,
  nav: true,
  rtl:true,
  dots:false,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
    1000: {
      items:3
    },
    1100: {
      items: 4
    }
  }
})
</script>
@else


<script>
  $('#profile_courses').owlCarousel({
  margin: 25,
  nav: true,
  dots:false,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
    1000: {
      items:3
    },
    1100: {
      items: 4
    }
  }
})
</script>
<script>
  $('#courses').owlCarousel({
  margin: 25,
  nav: false,
  dots:true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
    1000: {
      items:3
    },
    1100: {
      items: 4
    }
  }
})
</script>
<script>
  $('#Eureka_Stars').owlCarousel({
    loop:true,
  margin: 25,
  nav: true,
  dots:false,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
    900: {
      items:3
    },
    1000: {
      items:4
    },
    1100: {
      items: 5
    }
  }
})



  $('#vision_partner').owlCarousel({
    loop:true,
  margin: 35,
  nav: true,
  autoplay:true,
  autoplayTimeout:1000,
  autoplayHoverPause:true,
  dots:false,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
    900: {
      items:3
    },
    1000: {
      items:4
    },
    1100: {
      items: 5
    }
  }
})


  
</script>
<script>
  $('#mob_Testimonials').owlCarousel({
    loop:true,
  margin: 25,
  nav: true,
  dots:false,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 1
    },
    1000: {
      items:1
    },
    1100: {
      items: 1
    }
  }
})
</script>


<script>
$('#Related_items').owlCarousel({
  loop:true,
  center: false,
  margin: 40,
  nav: true,
  dots:false,
  navText: ["<i class='lni lni-arrow-left'></i>", "<i class='lni lni-arrow-right'></i>"],
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
   768: {
      items:2
    },
    1000: {
      items:3
    },
    1100: {
      items: 3
    }
   }
  })
</script> 


@endif

	
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> -->

	<!--Owl Carousel-->
	
	<!--  main js file  -->
	<script src="{{URL::asset('js/client/script.js')}}"></script>



	<script type="text/javascript">
	function toRegister(){
		@if (Route::currentRouteName() == 'login' || Route::currentRouteName() == 'register')
			$('.home-content .header-section-wrapper').addClass("d-none");
			$('#register').removeClass('d-none');
		@else 
			window.location = '{{route('register')}}';
		@endif
		
	}


	function toLogin(){
		@if (Route::currentRouteName() == "login" || Route::currentRouteName() == 'register')
			$('.home-content .header-section-wrapper').addClass("d-none");
			$('#login').removeClass('d-none');
		@else 
			window.location = '{{route('login')}}';
		@endif
		
	}

	var customKitItemsCount = 0 ;
	var customKitPrice = 0 ;
	var customKitItems = [] ; 
	var lang = '{{$lang}}';

	function showCustomizeKit(){
		$(".modal").modal("hide");
		$("#items-modal").modal("show");
	}

	function showKit(){
		$(".modal").modal("hide");
		$("#kits-modal").modal("show");
	}	
	function loadKitItems(id){
			$(".modal").modal("hide");
			$('#items-modal-body').empty();
			$.ajax({
	            type: "GET",
	            url: `/get-kit-items/${id}`, 
	            dataType: "json",
	            data: {'lang' : lang},
	            headers: {
	                "x-csrf-token": $("[name=_token]").val()
	            },
	        }).always(function(response){
	       		$('#items-modal-body').append(response.responseText);
	        	updateIt();

	        });
	        $('#kit-details-modal').modal('show');
	        updateIt();

	}
	function confirmKit(id){
		if ($('#city-input1').val() === "" ||
			$('#name-input1').val() == "" ||
			$('#email-input1').val() == "" ||
			$('#phone-input1').val() == "" ||
			$('#line1-input1').val() == "" ||
			$('#country-input1').val() == "" || $('#country-input1').val() == null ||
			$('#cell-phone-input1').val() == ""||
			$('#aramex-input1').val() == ""
			){
			swal('Error' , 'Fill Required Fields First' , 'error');
		}
		else {
			$.ajax({
	            type: "POST",
	            url: `/confirm-kit/${id}`, 
	            dataType: "json",
	            data: {
            		'city' : $('#city-input1').val(),
	            	'name' : $('#name-input1').val(),
	            	'phone' : $('#phone-input1').val(),
	            	'cell_phone' : $('#cell-phone-input1').val(),
	            	'email' : $('#email-input1').val(),
	            	'province' : $('#province-input1').val(),
	            	'postal' : $('#postal-input1').val(),
	            	'country_code' : $('#country-input1').val(),
	            	'line1' : $('#line1-input1').val(),
	            	'line2' : $('#line2-input1').val(),
	            	'line3' : $('#line3-input1').val(),
	            },
	            headers: {
	                "x-csrf-token": $("[name=_token]").val()
	            },
	        }).always(function(response){
	        	if (response > 0 ){
	        		swal('Success', 'Your Kit has confirmed successfully!' , 'success');
	        		$('.modal').modal('hide');
	        	}
	        	else 
	    			swal({
						type: 'error',
						title: 'Oops...',
						text: 'Your kit is not available right now try again later!',
					});

	        });
	    }
	};

	function calcTotal(price , qty , id){
		for (var i = customKitItems.length - 1; i >= 0; i--){
			if (customKitItems[i] == id ){
				customKitItems.splice(i,1);
				customKitPrice -= price;
				customKitItemsCount--;
			}
		}
		for (var i = 0 ; i < qty; i++){
			customKitItems.push(id);
			customKitPrice += price;
			customKitItemsCount++;
		}
		$('#items-count').html(customKitItemsCount);
		$('#items-price').html(customKitPrice);
	}
	
	function removeFromKit(price , id){
		for (var i = customKitItems.length - 1; i >= 0; i--) {
			if (customKitItems[i] == id ){
				customKitItems.splice(i,1);
				customKitPrice -= price;
				customKitItemsCount--;	
			}
		}
		$('#items-count').html(customKitItemsCount);
		$('#items-price').html(customKitPrice);		
	}

	$(".qty-input").on("keypress", function (evt) {
	    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
	    {
	        evt.preventDefault();
	    	return ;
	    }
	    val =  $(this).val() * 10 ;
	    val += Number(evt.key);

	    id = $(this).data('id');
	    price = $(this).data('price');
	    qty = val;
	    flag = $($(this).parent().parent().find('.option-input.checkbox')[0]);
	    if($(flag).is(':checked'))
		    calcTotal(price , qty , id);
	});
	
	$('#items-filter').on("keypress", function(evt){
		if (evt.which == 13 && $(this).val() != '' ){
			val = $(this).val();
			$.ajax({
	            type: "POST",
	            url: `/items-filter/${val}`, 
	            dataType: "json",
	            data: {},
	            headers: {
	                "x-csrf-token": $("[name=_token]").val()
	            }
	        }).always(function(response){
	        	$('#items-div-filter').html(response.responseText);
	        });
		}
	});

	$('#confirm-custom-kit').click(function(){
		if ($('#city-input').val() === "" ||
			$('#name-input').val() == "" ||
			$('#email-input').val() == "" ||
			$('#phone-input').val() == "" ||
			$('#line1-input').val() == "" ||
			$('#country-input').val() == "" ||$('#country-input').val() == null ||
			$('#cell-phone-input').val() == ""||
			$('#aramex-input').val() == ""
			){
			swal('Error' , 'Fill Required Fields First' , 'error');
		}
		else 
			$.ajax({
		            type: "POST",
		            url: '/confirm-custom-kit/', 
		            dataType: "json",
		            data: {
		            	'items' : customKitItems,
		            	'city' : $('#city-input').val(),
		            	'name' : $('#name-input').val(),
		            	'phone' : $('#phone-input').val(),
		            	'cell_phone' : $('#cell-phone-input').val(),
		            	'email' : $('#email-input').val(),
		            	'province' : $('#province-input').val(),
		            	'postal' : $('#postal-input').val(),
		            	'country_code' : $('#country-input').val(),
		            	'line1' : $('#line1-input').val(),
		            	'line2' : $('#line2-input').val(),
		            	'line3' : $('#line3-input').val()
		             },
		            headers: {
		                "x-csrf-token": $("[name=_token]").val()
		            },
		        }).always(function(response){
		        	if (response > 0 ){
		        		swal('Success', 'Your Kit has confirmed successfully!' , 'success');
		        		$('.modal').modal('hide');
		        	}
		        	else{ 
	        			swal({
							type: 'error',
							title: 'Oops...',
							text: 'Your kit is not available right now try again later!',
						});
	        		}
		        });
		});
		$('#country-input').select2({
		  ajax: {
		    url: 'https://restcountries.eu/rest/v2/all',
		    header: {
		    	'Access-Control' : '*'
		    },
		    processResults: function (data) {
		      for (var i = data.length - 1; i >= 0; i--) {
		      	data[i].id =data[i].alpha2Code;
		      	data[i].text = data[i].name;
		      }
		      return {
		        results: data
		      };
		    }
		  }
		});
		function updateIt(){
			$('#country-input1').select2({
			  ajax: {
			    url: 'https://restcountries.eu/rest/v2/all',
			    header: {
			    	'Access-Control' : '*'
			    },
			    processResults: function (data) {
			      for (var i = data.length - 1; i >= 0; i--) {
			      	data[i].id =data[i].alpha2Code;
			      	data[i].text = data[i].name;
			      }
			      return {
			        results: data
			      };
			    }
			  }
			});
			$('#confirm-custom-kit').click(function(){
			if ($('#city-input').val() === "" ||
				$('#name-input').val() == "" ||
				$('#email-input').val() == "" ||
				$('#phone-input').val() == "" ||
				$('#line1-input').val() == "" ||
				$('#country-input').val() == "" ||
				$('#cell-phone-input').val() == ""||
				$('#aramex-input').val() == ""
				){
				swal('Error' , 'Fill Required Fields First' , 'error');
			}
			else 
				$.ajax({
			            type: "POST",
			            url: '/confirm-custom-kit/', 
			            dataType: "json",
			            data: {
			            	'items' : customKitItems,
			            	'city' : $('#city-input').val(),
			            	'name' : $('#name-input').val(),
			            	'phone' : $('#phone-input').val(),
			            	'cell_phone' : $('#cell-phone-input').val(),
			            	'email' : $('#email-input').val(),
			            	'province' : $('#province-input').val(),
			            	'postal' : $('#postal-input').val(),
			            	'country_code' : $('#country-input').val(),
			            	'line1' : $('#line1-input').val(),
			            	'line2' : $('#line2-input').val(),
			            	'line3' : $('#line3-input').val(),
			             },
			            headers: {
			                "x-csrf-token": $("[name=_token]").val()
			            },
			        }).always(function(response){
			        	if (response > 0 ){
			        		swal('Success', 'Your Kit has confirmed successfully!' , 'success');
			        		$('.modal').modal('hide');
			        	}
			        	else{ 
		        			swal({
								type: 'error',
								title: 'Oops...',
								text: 'Your kit is not available right now try again later!',
							});
		        		}
			        });
			});
		
		}

		

		
		
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			if (localStorage.getItem('cookieSeen') != 'shown') {
				$('.cookie-banner').delay(2000).fadeIn();
			  	localStorage.setItem('cookieSeen','shown')
			}
			$('.close').click(function() {
				$('.cookie-banner').fadeOut();
			});
		});


 	$( "#testc" ).submit(function( event ) {  
  

		
		var appurl ="<?php echo config('app.url'); ?>";
		name = $('#contact-name').val();
		email = $('#contact-email').val();
		message = $('#contact-message').val();
		subject ="Contact";
		flag1 =0 ;
		flag2 = 0;
		for(i=0 ;i < email.length ; i++){
			if (email[i] == '@'){ 

				flag1 =1;
			}

			if (email[i] == '.' && flag1 ==1 ){
				flag2 =1;
			}
		}
		if (flag1 && flag2){


			$.ajax({
                 type: 'POST',
                 url: appurl+'/contact-us-front', 
                 data: {'name': name ,'email': email, 'message' : message , 'subject' : subject},
                 dataType: 'json',
                 headers: {"x-csrf-token": $("[name=_token]").val()},
                 success: function(response) {

                 	

                 	if(response.response == 'ok'){
                 		 $( ".spanc" ).text( "Email Sent Successfully, We will reply as soon as possible!" ).show();
		        	swal('Success', 'Email Sent Successfully, We will reply as soon as possible!' , 'success');
	        	}
	        	else {
	        		swal({
						type: 'error',
						title: 'Oops...',
						text: 'Something went wrong! , Try again later',
					});
	        	}
                 	  
                    //  window.location.href = "http://www.page-2.com";
                 }
             });
            // 


	      
       }
       else {
       	swal({
		  type: 'error',
		  title: 'Oops...',
		  text: 'Enter a valid E-mail!'
		})
       }


       return false;
	});
	</script>



  <script type="text/javascript">   
 

  @if(session('payment_repsonse') == 'Success')
    swal('Success', 'Payment accepted Successfully' , 'success');
    @php
      session(['payment_repsonse'=>'']);
    @endphp
  @endif

  @if(session('payment_repsonse') == 'Success-COUPON MIRACLE')
    swal('Success', 'Subscription Created Successfully, Your coupon amount is greater than paymnet amount!' , 'success');
    @php
      session(['payment_repsonse'=>'']);
    @endphp
  @endif

  @if(session('payment_repsonse') == 'Coupon Error')
    swal('Success', 'Incorrect Coupon Code' , 'success');
    @php
      session(['payment_repsonse'=>'']);
    @endphp
  @endif


  @if(session('payment_repsonse') == 'Fail')
    swal({
      type: 'error',
      title: 'Oops...',
      text: 'Payment not accepted, try again later'
    });
    @php
      session(['payment_repsonse'=>'']);
    @endphp
  @endif

   





  </script>

  
<script>
 $(document).ready( function() {   

$('.grid').isotope({
  itemSelector: '.grid-item',
});

// filter items on button click
$('.filter-button-group').on( 'click', 'li', function() {
  var filterValue = $(this).attr('data-filter');
  $('.grid').isotope({ filter: filterValue });
  $('.filter-button-group li').removeClass('active');
  $(this).addClass('active');
});






    })

</script> 
<script>
  $(document).on("click", '[data-toggle="lightbox"]', function(event) {
  event.preventDefault();
  $(this).ekkoLightbox();
});
</script>



	@yield('scripts')