<table width="100%" style="border-collapse: collapse;border: 1px solid black;">
        			<tr>
                        <td colspan="4" style="text-align: left;width: 100%;border: 1px solid black;">
                			<strong>OrderID : {{ $order->id }}</strong>
                		</td>
                		
					</tr>
					<tr>
                        <td colspan="4" style="text-align: center;width: 100%;border: 1px solid black;">
                			<strong>OrderDetail</strong>
                		</td>
                		
					</tr>
        			<tr>
                        <td style="text-align: center;width: 25%;border: 1px solid black;">
                			<strong>Product Name</strong>
                		</td>
                		<td style="text-align: center;width: 25%;border: 1px solid black;">
                			<strong>Quantity</strong>
                		</td>
                		<td style="text-align: center;width: 25%;border: 1px solid black;">
                			<strong>Tax</strong>
                		</td>
                		<td style="text-align: center;width: 25%;border: 1px solid black;">
                			<strong>Item Price</strong>
                		</td>
					</tr>
                    @foreach($orderItems as $items)
                    	<tr>
						      <td style="text-align: center;width: 25%;border: 1px solid black;">{{$items->itemname}}</td>
						      
						      <td style="text-align: center;width: 25%;border: 1px solid black;">{{ $items->qty}}</td>
						      <td style="text-align: center;width: 25%;border: 1px solid black;">0%</td>
						      <td style="text-align: right;width: 25%;border: 1px solid black;">{{ $items->final_total}}</td>
						     
						    </tr>
					@endforeach
					<tr>
						      <td colspan="3" style="text-align: right;width: 25%;border: 1px solid black;">Total Price</td>
						      
						      <td style="text-align: right;width: 25%;border: 1px solid black;">{{ number_format((float)$order->total_price, 2, '.', '') }}</td>
						     
						    </tr>
                    </table>