<!DOCTYPE html>
<html>
<head>
	<title></title>

<link rel="stylesheet" href="{{asset('/css/admin/bootstrap/bootstrap.min.css')}}">
</head>
<body>
	<div class="row">
		<div class="col-12">
			<div class="col-10 offset-1" id="table">
				<table class="table">
					<thead>
					    <tr>
						@foreach($headers as $header)
					      <th scope="col">{{$header}}</th>
					    @endforeach
					    </tr>
					</thead>
					<tbody>
					  	@foreach($data as $data1)
					    <tr>

					    	@foreach($data1 as $col)
					      		<td>{{$col}}</td>
					      	@endforeach
					    </tr>
					    @endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>


</body>
</html>