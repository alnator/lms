<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<!-- Bootstrap Core CSS -->
<!-- <link href="<?php echo config('app.url'); ?>/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Custom CSS -->
    <!-- <link href="css/helper.css" rel="stylesheet"> -->

<link href="<?php echo config('app.url'); ?>/css/jsgrid-theme.min.css" rel="stylesheet">
<link href="<?php echo config('app.url'); ?>/css/jsgrid.min.css" rel="stylesheet">
<link href="<?php echo config('app.url'); ?>/css/style.css" rel="stylesheet">
<style >
	.btn-default{
		border: 1px solid #26dad2;
	}
	.jsgrid-filter-row input{
		    padding: 1px 0px;
		    padding-top: 1px;
		    padding-right: 0px;
		    padding-bottom: 1px;
		    padding-left: 0px;
	}
	.jsgrid-grid-header 
	{
		overflow-y: hidden !important;
	}
	.jsgrid-grid-body {		
		overflow-y: hidden !important;
	}
</style>
    @yield('styles')