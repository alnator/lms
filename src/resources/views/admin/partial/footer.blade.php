<!-- End Wrapper -->
<!-- All Jquery -->
<script src="<?php echo config('app.url'); ?>/js/lib/jquery/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="<?php echo config('app.url'); ?>/js/lib/bootstrap/js/popper.min.js"></script>
<script src="<?php echo config('app.url'); ?>/js/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->
<script src="<?php echo config('app.url'); ?>/js/jquery.slimscroll.js"></script>
<!--Menu sidebar -->
<script src="<?php echo config('app.url'); ?>/js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="<?php echo config('app.url'); ?>/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
<!--Custom JavaScript -->
<script src="<?php echo config('app.url'); ?>/js/custom.min.js"></script>

@yield('scripts')