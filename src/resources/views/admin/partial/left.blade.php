<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="nav-label">Home</li>
                <li style="cursor: pointer;" > 
                    <a class="has-arrow" aria-expanded="false">
                        <i class="fa fa-tachometer"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                    <ul aria-expanded="false" class="collapse ">
                        <!-- <li><a href="{{route('categories-admin')}}">Categories </a></li> -->
                        <li><a href="{{route('levels-admin')}}">Theme </a></li>
                        <li><a href="{{route('courses-admin')}}">Courses </a></li>
                        <li><a href="{{route('sessions-admin')}}">Sessions</a></li>
                        <li><a href="{{route('videos-admin')}}">Lesson</a></li>
                        <li><a href="{{route('groups-admin')}}">Groups</a></li>
                        <li><a href="{{route('users-admin')}}">Users</a></li>
                        <!-- <li><a href="{{route('files-admin')}}">Downloadables</a></li> -->
                        <li><a href="{{route('subscription-admin')}}">Subscription Models</a></li>
                        <li><a href="{{route('testimonials-admin')}}">Testimonials</a></li>
                        <li><a href="{{route('banner-images')}}">Banner Images</a></li>
                        <li><a href="{{route('vision-admin')}}">Vision Partners</a></li>
                    </ul>

                </li>
                <li style="cursor: pointer;">
                    <a class="has-arrow" aria-expanded="false"><i class="fa fa-usd f-s-40 color-primary" style="margin-bottom: 3px;margin-left: 4px;"></i><span class="hide-menu"">Payments</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('payments-admin')}}">Payments</a></li>
                        <li><a href="{{route('payment-methods-admin')}}">Payment Methods</a></li>
                        <li><a href="{{route('coupon-admin')}}">Coupons</a></li>
                        <li><a href="{{route('teacher-admin')}}">Teacher Fees</a></li>
                        <!-- <li><a href="{{route('abandoned-carts-admin')}}">Abandoned Carts</a></li> -->
                        <li><a href="{{route('teacher-billing')}}">Teachers Billing</a></li>
                    </ul>
                </li>
                <li style="cursor: pointer;">
                    <a class="has-arrow" aria-expanded="false"><i class="fas fa-toolbox f-s-40 color-primary" style="margin-bottom: 3px;margin-left: 4px;"></i><span class="hide-menu"">Lab</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('kits-admin')}}">Kits</a></li>
                        <li><a href="{{route('items-admin')}}">Items</a></li>
                        <li><a href="{{route('suppliers-admin')}}">Suppliers</a></li>
                        <li><a href="{{route('orders-admin')}}">Created Orders</a></li>
                        <li><a href="{{route('all-orders-admin')}}">All Orders</a></li>
                    </ul>
                </li>
              <!--   <li style="cursor: pointer;">
                    <a class="has-arrow" aria-expanded="false"><i class="fa fa-usd f-s-40 color-primary" style="margin-bottom: 3px;margin-left: 4px;"></i><span class="hide-menu">Stripes</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('stripe-admin')}}">Stripe Customers</a></li>
                        <li><a href="{{route('stripe-plan-admin')}}">Stripe Plans</a></li>
                    </ul>
                </li> -->
                 <li style="cursor: pointer;">
                    <a class="has-arrow" aria-expanded="false"><i class="fas fa-file-image f-s-40 color-primary" style="margin-bottom: 3px;margin-left: 4px;"></i><span class="hide-menu"">Gallery</span></a>
                    <ul aria-expanded="false" class="collapse">
                        
                    <li style="cursor: pointer;"><a href="{{route('admin-albums')}}">Albums</a></li>
                    <li style="cursor: pointer;"><a href="{{route('admin-gallery')}}">Items</a></li>
                    </ul>
                </li>

                <li style="cursor: pointer;">
                    <a href="{{route('admin-achievements')}}">
                        <i class="fas fa-trophy" style="margin-bottom:8px;margin-left: 4px"></i>
                        <span class="hide-menu">Achievements</span>
                    </a>
                </li>

                <li style="cursor: pointer;">
                    <a href="{{route('slugs-admin')}}">
                        <i class="fas fa-file-image" style="margin-bottom:8px;margin-left: 4px"></i>
                        <span class="hide-menu">Slugs</span>
                    </a>
                </li>
                <li style="cursor: pointer;">
                    <a href="{{route('stars-text-admin')}}">
                        <i class="far fa-file-alt" style="margin-bottom:8px;margin-left: 4px"></i>
                        <span class="hide-menu">Eurekian Stars Text</span>
                    </a>
                </li>

                <li style="cursor: pointer;">
                    <a href="{{route('quiz-reports-admin')}}">
                        <i class="far fa-check-square" style="margin-bottom:8px;margin-left: 4px"></i>
                        <span class="hide-menu">Quiz Reports</span>
                    </a>
                </li>
                <li style="cursor: pointer;">
                    <a href="{{route('footer-admin')}}">
                        <i class="fas fa-mobile" style="margin-bottom:8px;margin-left: 4px"></i>
                        <span class="hide-menu">Numbers And Links</span>
                    </a>
                </li>
                <li style="cursor: pointer;">
                    <a href="{{route('emails-admin')}}">
                        <i class="far fa-envelope-open" style="margin-bottom:8px;margin-left: 4px"></i>
                        <span class="hide-menu">Contact Us Report</span>
                    </a>
                </li>
                <li style="cursor: pointer;">
                    <a href="{{route('subscription-report')}}">
                        <i class="fas fa-chart-line" style="margin-bottom:8px;margin-left: 4px"></i>
                        <span class="hide-menu">Subscriptions Report</span>
                    </a>
                </li>
                <li style="cursor: pointer;">
                    <a href="{{route('admin-email-subscription')}}">
                        <i class="far fa-envelope-open" style="margin-bottom:8px;margin-left: 4px"></i>
                        <span class="hide-menu">Emails Subscription</span>
                    </a>
                </li>
                <li style="cursor: pointer;">
                    <a href="{{route('email-template')}}">
                        <i class="far fa-envelope-open" style="margin-bottom:8px;margin-left: 4px"></i>
                        <span class="hide-menu">Email Template</span>
                    </a>
                </li>
                <!--
                <li> 
                    <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-bar-chart"></i><span class="hide-menu">Charts</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a >Flot</a></li>
                        <li><a >ChartJs</a></li>
                        <li><a >Chartist </a></li>
                        <li><a >AmChart</a></li>
                        <li><a >EChart</a></li>
                        <li><a >Sparkline</a></li>
                        <li><a >Peity</a></li>
                    </ul>
                </li>
                <li class="nav-label">Features</li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu">Bootstrap UI <span class="label label-rouded label-warning pull-right">6</span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a >Alert</a></li>
                        <li><a >Button</a></li>
                        <li><a >Dropdown</a></li>
                        <li><a >Progressbar</a></li>
                        <li><a >Tab</a></li>
                        <li><a >Typography</a></li>
                    </ul>
                </li>
    <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu">Components <span class="label label-rouded label-danger pull-right">6</span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a >Calender</a></li>
                        <li><a >Datamap</a></li>
                        <li><a >Nestedable</a></li>
                        <li><a >Sweetalert</a></li>
                        <li><a >Toastr</a></li>
                        <li><a >Weather</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-wpforms"></i><span class="hide-menu">Forms</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a >Basic Forms</a></li>
                        <li><a >Form Layout</a></li>
                        <li><a >Form Validation</a></li>
                        <li><a >Editor</a></li>
                        <li><a >Dropzone</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-table"></i><span class="hide-menu">Tables</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a >Basic Tables</a></li>
                        <li><a >Data Tables</a></li>
                    </ul>
                </li>
                <li class="nav-label">Layout</li>
                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-columns"></i><span class="hide-menu">Layout</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a >Blank</a></li>
                        <li><a >Boxed</a></li>
                        <li><a >Fix Header</a></li>
                        <li><a >Fix Sidebar</a></li>
                    </ul>
                </li>
                
            </ul>
        </nav>
        <!- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</div>
