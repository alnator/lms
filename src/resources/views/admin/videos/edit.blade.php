@extends('admin.adminmaster')

@section ('title','Lesson')

@section ('content')
			

	<div class="col-12 card" >
		{!! Form::model($video,['route' => ['video-edit-api',$video->id] , 'files' => 'true']) !!}	
			<div class="row">
				<div class="col-12">
					<h3> Main Details </h3>
				</div>
				<div class="col-6"> 
					{{ Form::label('name_en', 'Name (EN):')}}
					{{ Form::text('name_en',null,['class'=>'form-control input-rounded']) }}
					<br>
					{{ Form::label('description_en' , 'Description (EN):')}}
					{{ Form::textarea('description_en' , null , ['class'=>'form-control' , 'style'=>'height: 10em;'])}}

				</div>
				<div class="col-6">
					{{ Form::label('name_ar', 'Name (AR):')}}
					{{ Form::text('name_ar',null,['class'=>'form-control input-rounded']) }}
					<br>
					{{ Form::label('description_ar' , 'Description (AR):')}}
					{{ Form::textarea('description_ar' , null , ['class'=>'form-control' , 'style'=>'height: 10em;'])}}
				</div>
			 </div>
		
	</div>
	<div class="col-12 card" >
		<div class="row">
			<div class="col-12">
				<h3>More Details</h3>
			</div>
			<div class="col-6"> 
				{{ Form::label('course_id','Course:')}}
				<select class="input-rounded custom-select form-control" required="required" name="course_id" id="select">
				@foreach($courses as $course)
					<option value="{{$course->id}}" {{$course->id == $video->course_id ? 'selected="selected"' : ''}}  >{{$course->title_en}}</option>		
				@endforeach
				</select>
				<br><br>
				
				{{ Form::label('session_id','Session:')}}
				<select class="input-rounded custom-select form-control" required="required" name="session_id" id="fill-it">
					@if($sessions->count() != 0)
						@foreach($sessions as $session)
							<option value="{{$session->id}}" {{ $video->session_id == $session->id ? 'selected="selected"' : '' }}> {{$session->session_number}}</option>
						@endforeach
					@endif
				</select>
				
				
				
				

			</div>
			<div class="col-6">
				{{ Form::label('user_id','Teacher:')}}
				<select class="input-rounded custom-select form-control" required="required" name="user_id">
				@foreach($users as $user)
					<option value="{{$user->id}}">{{$user->email}}</option>
				@endforeach
				</select>
				<br>
				<br>
				{{ Form::label('order' , 'Order in The Session:')}}
				{{ Form::number('order' , null , ['class'=>'form-control input-rounded'])}}

					<br>
			</div>

			<div class="col-6">
				{{ Form::label('estimated_time','Estimated Time:')}}
				{{ Form::text('estimated_time' , null , [ 'class' =>'form-control input-rounded','id'=>'estimated_time'])}}
				<br>
				
			</div>

		</div>

	</div>
	<div class="col-12 card" >
		<div class="row" style="padding-left: 1em;padding-right: 1em">
			<div class="col-4" style="margin-top: 3em; display: none;"> 
				{{ Form::label('is_youtube', 'Youtube:') }}
				{{ Form::checkbox('is_youtube' ,'1' ) }}
			</div>
			<div class="col-8 d-none" id="link">
				{{ Form::label('youtube_link' , 'Embed Code:')}}
				{{ Form::text('url_identifier' , null ,['class' =>'form-control','id' => 'url_identifier'])}}
			</div>
		</div>
		<div class="row">
			<div class="col-4" style="margin-top: 3em;"> 
				{{ Form::label('active', 'Active:')}}
				{{ Form::checkbox('active' ,'1')}}
			</div>
			<div class="col-4">
				<br>
				{{ Form::label('image_path' , 'Image:')}}
				{{ Form::file('image_path' , ['class' => 'form-control ' ])}}

				@if(isset($video->image_path)) <img src="<?php echo config('app.url').'/public'.$video->image_path; ?>" height="100px"> @endif
				
			</div>
			<div class="col-4" id="upload-video1" style="display: none;">
				{{ Form::label('video' , 'Video:') }}
				{{ Form::file('video' , ['class' => 'form-control' ])}}
			</div>
		</div>

		<div class="row">
			<div class="col-4" style="margin-top: 3em"> 
				{{ Form::label('free', 'Free Lesson ?:')}}
				{{ Form::checkbox('free' ,'1')}}
			</div>
			
		</div>

		
	</div>

	<div class="col-12 card">
		{{ Form::submit('Submit Lesson' , ['class'=>'btn btn-success '])}}
		<a href="{{route('videos-admin')}}" class="btn btn-default " style="margin-top: 10px;border: 1px solid #26dad2;"> Cancel</a>
	</div>

	{!! Form::close() !!}






@endsection


@section ('scripts')

<script type="text/javascript">
counter = 0;
</script>
	@if($video->is_youtube == 1)
	
	<script type="text/javascript">
		
	$('#link').removeClass('d-none');
			$('#upload-video').addClass('d-none'); 
	</script>

	@endif


<script >
	$('#select').change(function (){
		$.ajax({
            type: "POST",
            url: `<?php echo config('app.url'); ?>/api/admin/get-sessions/`,
            dataType: "json",
            data: {'course_id':$('#select').val() },
            headers: {
                "x-csrf-token": $("[name=_token]").val()
            },
        }).done((response) =>{
        	$('#fill-it option').remove();
        	for (var i = response.length - 1; i >= 0; i--) {
            	$('#fill-it').append($('<option>', {
				    value: response[i]['id'],
				    text: response[i]['session_number']
				}));
        	}
  		});
	});

	

	$('#is_youtube').click(function (){


		  if($(this).prop("checked") == true){
              $('#link').removeClass('d-none');
			$('#upload-video').addClass('d-none');
			$('#video').prop('required',false);
            }
            else if($(this).prop("checked") == false){
               $('#link').addClass('d-none');
			$('#upload-video').removeClass('d-none');
			$('#video').prop('required',true);
            }


		
	});

	

	$("#url_identifier1").blur(function(){
		var videoid=$(this).val();
      var youtubeUrl = "https://www.googleapis.com/youtube/v3/videos?id="+videoid+"&key=AIzaSyC2o8JmCHISlfD6SpT7aE7A22jliodkl_4&part=snippet,contentDetails";
        $.ajax({
            async: false,
            type: 'GET',
            url: youtubeUrl,
            success: function(data) {
              var youtube_time = data.items[0].contentDetails.duration;
              var duration = covtime(youtube_time); 
              $("#estimated_time").val(duration);
            }
        });
});

function covtime(youtube_time){
  array = youtube_time.match(/(\d+)(?=[MHS])/ig)||[]; 
    var formatted = array.map(function(item){
        if(item.length<2) return '0'+item;
        return item;
    }).join(':');
    return hmsToSecondsOnly(formatted);
}

function hmsToSecondsOnly(str) {
    var p = str.split(':'),
        s = 0, m = 1;

    while (p.length > 0) {
        s += m * parseInt(p.pop(), 10);
        m *= 60;
    }

    return s;
}
</script>
@endsection 