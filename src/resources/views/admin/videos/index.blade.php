@extends('admin.adminmaster')

@section ('title','Lesson')
@section ('styles')



    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
	<div class="col-12 card" id="videos-grid">
		
	</div>
	<div class="col-12 card">
		<a href="{{route('video-create')}}" class="btn btn-success btn-block">Upload Lesson</a>
	</div>
@endsection


@section ('scripts')
<script type="text/javascript">var siteurl= " <?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/videos-grid.js"></script>
    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript">
        @if( session('error') == '1')
	
    		swal({
			  type: 'error',
			  title: 'Oops...',
			  text: 'Error while video upload, Please try again later.'
			 });	
		
		  <?php 
		  session(['error'=>'0']);
		  ?>
	@endif
</script>
@endsection 