@extends('admin.adminmaster')

@section ('title','Lesson')

@section ('content')
			
<style>
        .progress { position:relative; width:100%; border: 1px solid #7F98B2; padding: 1px; border-radius: 3px; }
        .bar { background-color: #B4F5B4; width:0%; height:25px; border-radius: 3px; }
        .percent { position:absolute; display:inline-block; top:3px; left:48%; color: #7F98B2;}
    </style>
	{!! Form::model($video,['route' => ['video-upload-api',$video->id] , 'files' => 'true']) !!}	
	<div class="col-12 card" >
		<div class="row" style="padding-left: 1em;padding-right: 1em">
			
			<div class="col-12" >
				{{ Form::label('video' , 'Video:') }}
				{{ Form::file('video' , ['class' => 'form-control ' ])}}
			</div>
		</div>
	</div>
	<div class="col-12 card" >
		<div class="progress">
            <div class="bar"></div >
            <div class="percent">0%</div >
        </div>
    </div>
	<div class="col-12 card">
		{{ Form::submit('Upload Lesson' , ['class'=>'btn btn-success '])}}
		<a href="{{route('videos-admin')}}" class="btn btn-default " style="margin-top: 10px;border: 1px solid #26dad2;"> Cancel</a>
	</div>

	{!! Form::close() !!}
@endsection


@section ('scripts')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>
<script type="text/javascript">
 
    
 
    (function() {
 
    var bar = $('.bar');
    var percent = $('.percent');
    var status = $('#status');
 
    $('form').ajaxForm({
        beforeSend: function() {
            status.empty();
            var percentVal = '0%';
            var posterValue = $('input[name=file]').fieldValue();
            bar.width(percentVal)
            percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        success: function() {
            var percentVal = 'Wait, Saving';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        complete: function(xhr) {
            status.html(xhr.responseText);
            alert('Uploaded Successfully');
            //window.location.href = "/file-upload";
        }
    });
     
    })();
</script>

@endsection 