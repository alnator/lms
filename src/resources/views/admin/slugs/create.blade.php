@extends('admin.adminmaster')

@section ('title','Sessions')

@section ('styles')

   <link rel="stylesheet" href="{{URL::asset('css/client/font-awesome.min.css')}}" />
	<!--  checkbox  -->
	<link rel="stylesheet" href="{{URL::asset('css/client/checkbox.css')}}" />
	<!--Owl Carousel-->
	<link rel="stylesheet" href="{{URL::asset('css/client/owl.carousel.min.css')}}" />
	<link rel="stylesheet" href="{{URL::asset('css/client/owl.theme.default.min.css')}}" />
	<!--  main css style file  -->
	<link rel="stylesheet" href="{{URL::asset('css/client/style.css')}}" />
	<style >
		header.all-nav .nav2 .follow img 
		{
			margin-right: 2px;
		}
		@media (min-width: 768px){
			.navbar {
				border-radius: 0px;
			}
		}
		.navactive 
		{
			display: block !important; 
		}
		.hover {
			cursor: pointer;
		}

		.icon{
			padding-right: 1em;
		}
		.wrong-details{
			margin-left: 6em;
			color: red;
		}
		a {
	    	color: inherit;
		}
		a:hover {
	    	text-decoration: none;
		}
		input.form-control {
			color: black;
		}
	</style>
    <script src="<?php echo config('app.url'); ?>/js/ckeditor/ckeditor.js"></script>
@endsection

@section ('content')
	<div class="col-12 card" >
	{!! Form::open(['route'=>'slugs-create-api' , 'files'=>true]) !!}

		<div class="row">
			<div class="col-6">
				{{Form::label('title_en' , 'Title (EN):')}}
				{{Form::text('title_en' , null, ['class'=>'form-control input-rounded' , 'required' => 'required']) }}
			</div>
			<div class="col-6">
				{{ Form::label('title_ar' , 'Title (AR):')}}
				{{ Form::text('title_ar' , null, ['class' => 'form-control input-rounded','required' => 'required']) }}
			</div>
			<div class="col-6">
				{{ Form::label('order', 'Order: ')}}
				{{ Form::text('order', null,['class'=>'form-control input-rounded','required' => 'required']) }}
			</div>
			<div class="col-6">
				{{ Form::label('slug', 'Slug Name:')}}
				{{ Form::text('slug' , null , ['class' => 'form-control input-rounded','required' => 'required'])}}
			</div>
		</div>
		<div class="col-12" style="margin-top: 20px">
			<h3>External Site</h3><small>(optional)</small>
			<div class="row">
				<div class="col-12">
					{{ Form::label('href', 'URI:')}}
					{{ Form::text('href' , null , ['class' => 'form-control input-rounded'])}}
				</div>
				<div class="col-12">
					<br><br>
					<center>
					{{ Form::label('active','Active:')}}
					{{ Form::checkbox('active' , null)}}
					</center>
				</div>
			</div>
		</div>
	</div>
		<input id = "fill-it" type="text" hidden="hidden" name="text">
	<div class="col-12 card">
		<h4>Slug English Text</h4>
		<textarea name="editor" id= "editor">
			<h1>This is a sample</h1>
			<p>testing  in-line Styles</p>
		</textarea>
	</div>
	<div class="col-12 card">
		<h4>Slug Arabic Text</h4>
		<textarea name="editor_ar" id= "editorar">
			<h1>مثال</h1>
			<p>مثال </p>
		</textarea>
	</div>

	<div class="col-12 card">
		{{ Form::submit('Create Slug', ['class' => 'btn btn-success btn-block'])}}

		<a href="{{route('slugs-admin')}}" class="btn btn-default btn-block" style="margin-top:10px;">Cancel</a>
	</div>		
	{!! Form::close() !!}
@endsection

@section ('scripts')
	
		<script>
			CKEDITOR.replace('editor' , {
				filebrowserBrowseUrl: '/images/',
				filebrowserUploadUrl: '/upload?command=QuickUpload&type=Files',
				allowedContent : true	
			});
			CKEDITOR.replace('editorar' , {
				filebrowserBrowseUrl: '/images/',
				filebrowserUploadUrl: '/upload?command=QuickUpload&type=Files',
				allowedContent : true	
			});
		</script>
		<script >
			// var editor = CKEDITOR.instances.editor;
			// bool = 0;
			// editor.on('key', function( evt ) { 
			// 	if (bool == 0 && evt.keyCode != 27){
			// 		editor.execCommand('maximize');
			// 		bool = 1
			// 	}
			// 	if (evt.keyCode == 27){
			// 		editor.execCommand('maximize');
			// 		bool = 0
			// 	}
			// });
			// var editor2 = CKEDITOR.instances.editorar;
			// bool2 = 0;
			// editor2.on('key', function( evt ) { 
			// 	if (bool2 == 0 && evt.keyCode != 27){
			// 		editor2.execCommand('maximize');
			// 		bool2 = 1
			// 	}
			// 	if (evt.keyCode == 27){
			// 		editor2.execCommand('maximize');
			// 		bool2 = 0
			// 	}
			// });

			$('footer').css('display', 'none');		
		</script>
@endsection 