@extends('admin.adminmaster')

@section ('title','About Us Text')
@section ('styles')

    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
  	<script>tinymce.init({ selector:'textarea',menubar: false , height: 400});</script>
 	<style type="text/css">
 		#mceu_11 {
 			margin-top: 3em;
 			margin-bottom: 3em;
 		}
 	</style>
@endsection

@section ('content')
	<div class="col-12 card" id="teachers-grid">
		<h1>About Us</h1>
		<div class="row">
			<div class="col-12">
				<textarea name="about">
						{{$aboutUs->text}}

				</textarea>
			</div>

			<div class="col-12">
				<button class="btn btn-block btn-success" onclick="update();">Update</button>
			</div>
		</div>
	</div>
@endsection


@section ('scripts')
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript">
    	setInterval(function(){ 
    		hide();    
		}, 200);
    	function hide(){
    		$('.mce-notification-inner').css('display','none');
    		$('#mceu_26').css('display','none');
    	}
    	function update(){
    		str = tinymce.get('about').save();
    		type = 'about';
		    $.ajax({
		        type: "POST",
		        url: `/admin/about/update`,
		        data : {'text' : str , 'type' : type },
		        headers: {
		            "x-csrf-token": $("[name=_token]").val()
		        },
		    }).done(response => {
		      if(response  == 'ok'){
		        swal("Updated !", "About Us text has updated successfully.", "success");
		       }
		    });
    	}
    </script>
@endsection 