@extends('admin.adminmaster')

@section ('title','Subscription')

@section ('content')
			

	<div class="col-12 card" >
		{!! Form::model( $subscription,['route' => ['subscription-edit-api' , $subscription->id] ]) !!}	
			<div class="row">
				<div class="col-12">
					<h3> Main Details </h3>
				</div>
				<div class="col-6"> 
					{{ Form::label('name_ar', 'Name (AR):')}}
					{{ Form::text('name_ar',null,['class'=>'form-control input-rounded']) }}
					<br>
					{{ Form::label('display_price' , 'Display Price:')}}
					{{ Form::number('display_price' ,null , ['class'=>'form-control input-rounded'])}}
					<br>
					{{ Form::label('period_in_days' , 'Period In Days:')}}
					{{ Form::number('period_in_days' ,null , ['class'=>'form-control input-rounded' ])}}
					
				</div>
				<div class="col-6">
					{{ Form::label('name_en', 'Name (EN):')}}
					{{ Form::text('name_en',null,['class'=>'form-control input-rounded']) }}
					<br>
					{{ Form::label('price' , ' Price:')}}
					{{ Form::number('price' ,null , ['class'=>'form-control input-rounded' ])}}
					<br>
					{{ Form::label('full_access' , 'Full Access:')}}
					{{ Form::checkbox('full_access' ,null , ['class'=>'form-control input-rounded' ])}}
				</div>
			 </div>
	</div>

	<div class="col-12 card">
		{{ Form::submit('Submit Subscription Update' , ['class'=>'btn btn-success '])}}
		<a href="{{route('subscription-admin')}}" class="btn btn-default " style="margin-top: 10px;border: 1px solid #26dad2;"> Cancel</a>
	</div>

	{!! Form::close() !!}
@endsection


@section ('scripts')

@endsection 