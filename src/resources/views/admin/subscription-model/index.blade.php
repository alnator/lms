@extends('admin.adminmaster')

@section ('title','Subscription')
@section ('styles')



    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
	<div class="col-12 card" id="subscription-grid">
		
	</div>
	<div class="col-12 card">
		<a href="{{route('subscription-create')}}" class="btn btn-success btn-block">Create Subscription</a>
	</div>
@endsection


@section ('scripts')
<script type="text/javascript">var siteurl= " <?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/subscription-grid.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>

@endsection 