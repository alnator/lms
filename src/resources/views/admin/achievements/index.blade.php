@extends('admin.adminmaster')

@section ('title','Achievements')
@section ('styles')
    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">
@endsection

@section ('content')
	<div class="col-12 card" id="achievements-grid">
		
	</div>
	<div class="col-12 card">
		<a href="{{route('admin-create-achievement')}}" class="btn btn-success btn-block">Add Achievements</a>
	</div>
@endsection


@section ('scripts')
<script type="text/javascript">var siteurl="<?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/achievements-grid.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>

@endsection 