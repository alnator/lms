@extends('admin.adminmaster')

@section ('title','Achievements')

@section('styles')

	<link rel="stylesheet" href="<?php echo config('app.url'); ?>/css/bootstrap-datepicker3.min.css">
	
@endsection

@section ('content')
	{!! Form::model($achievement , ['route' => ['admin-update-achievement' , $achievement->id], 'files' => true  ]) !!}		

	<div class="col-12 card" >
		<div class="row">
			<div class="col-12">
				<h3> Main Details </h3>
			</div>
			
			<div class="col-6">
				{{ Form::label('title_en', 'Engilsh Title') }}
				{{ Form::text('title_en', null, ['class'=>'form-control input-rounded' , 'required' => 'required'] ) }}
			</div>
			<div class="col-6"> 
				{{ Form::label('title_ar', 'Arabic Title')}}
				{{ Form::text('title_ar' , null , ['class'=>'form-control input-rounded' , 'required'=>'required'] ) }}
			</div>

			<div class="col-6">
				{{ Form::label('description_en', 'Engilsh Description') }}
				{{ Form::textarea('description_en', null, ['class'=>'form-control' , 'required' => 'required' , 'style' => 'height:10em'] ) }}
			</div>
			<div class="col-6"> 
				{{ Form::label('description_ar', 'Arabic Description')}}
				{{ Form::textarea('description_ar' , null , ['class'=>'form-control' , 'required'=>'required', 'style' => 'height:10em'] ) }}
			</div>

			<div class="col-6"> 
				{{ Form::label('event_date', 'Event Date:')}}
				{{ Form::text('event_date' , null , ['class'=>'form-control input-rounded' , 'required'=>'required' , 'id' => 'datepicker'] ) }}
			</div>

			<div class="col-6">
				{{ Form::label('img_src', 'Image:')}}<br>
				{{ Form::file('img_src', null ,['class' => 'form-control input-rounded']) }}
			</div>

		 </div>
	</div>
	
	<div class="col-12 card">
		{{ Form::submit('Update Achievemets' , ['class'=>'btn btn-success'])}}
		<a href="{{route('admin-achievements')}}" class="btn btn-default " style="margin-top: 10px;border: 1px solid #26dad2;"> Cancel</a>
	</div>

	{!! Form::close() !!}
@endsection


@section ('scripts')
 	<script src="<?php echo config('app.url'); ?>/js/bootstrap-datepicker.min.js"></script>
 	<script type="text/javascript">
 		  $.fn.datepicker.defaults.format = "yyyy-mm-dd";
    $( function() {
	    $("#datepicker").datepicker({
	       changeMonth: true,
	       changeYear: true
	    });
	  });
 	</script>
@endsection 