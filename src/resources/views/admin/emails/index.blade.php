@extends('admin.adminmaster')

@section ('title','Contact Us')
@section ('styles')

    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">
    <style>
    .display 
    {
   		display: flex !important;
    }
    .popup 
    {
		position: absolute;
		left: 20%;
		top: 15%;
		z-index: 20;
	}
	.btn-success
	{
		margin-top: 20px;
	}
	footer {
		display: none;
	}
	.date {
		    margin-bottom: -19px;
    		text-align: right;
	}
    </style>
@endsection

@section ('content')
	<div class="col-12 card" id="emails-grid">
		
	</div>


	<div class="col-8 card popup" id="display" style="display: none">
		<div class="col-12">
			<p>
				<label>Email: </label>
				<input type="text" disabled id="email" class="form-control">
			</p>
			<p>
				<label>Name: </label>
				<input type="text" disabled id="name" class="form-control">
			</p>
			<p>
				<label>Subject: </label>
				<input type="text" disabled id="subject" class="form-control">
			</p>
			<hr>
			<div class="col-12 card">
				<p id="text">
					
				</p>
				<i class="date">
					<small>
						Sent at: 
					</small>
					<small id="date">
					</small>
				</i>
			</div>
		</div>
		<button class="btn btn-block btn-success" onclick="$('#display').removeClass('display');">OK</button>
	</div>
@endsection


@section ('scripts')
<script type="text/javascript">var siteurl = "<?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/emails-grid.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>

@endsection 