@extends('admin.adminmaster')

@section ('title','Subscriptions Report')
@section ('styles')

    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
	<style type="text/css">
		.top-margin {
			margin-top: 10px;
		}
		.width-64 {
			width: 34%;
		}
		.width-20{
			width: 20%;
		}
		.left-margins {
			margin-left:20px;
		}
		.width-25{
			width :25%;
		}
		.annual-btn {
			float: right;
    		width: 140px;
		}
		.label-active {
			background-color: #d6746d;
		}
		.label-users{
			background-color: #50429c;
		}
		.amcharts-chart-div a {
			display: none !important;
		}
		.excel-btn {
			margin-right: 10px;
		}

	</style>
@endsection

@section ('content')
	<div class="col-12 card" >
		<div class="row">
			<h3 style="margin-left:20px">Overall Subscriptions This Year</h3>
			<div class="col-12 row">
				<select class="form-control input-rounded custom-select width-25 left-margins" id="subscription-model-filter-all">
					<option value="0">All Subscriptions</option>
					<option value="3">Annual Subscription</option>
					<option value="4">Monthly Subscription</option>
					<option value="5">Course Subscription</option>
				</select>
				<input type="text" name="year" id = "overall-year" style="margin-left: 5px;" value="2018" class="form-control input-rounded width-25">
				<button class="btn btn-default annual-btn" style="margin-left: 300px" id="annual-btn">Filter</button>
			</div>
			<div class="col-12 row top-margin">

				<div class="col">
					<center>
						<span class="label label-active" style="align-content: center;">Active Accounts(has an active subscription)</span>
					</center>	
				</div>
				<div class="col">
					<center>
						<span class="label label-default">New Subscriptions</span>
					</center>
				</div>
				<div class="col">
					<center>
						<span class="label label-users">New Users</span>
					</center>
				</div>
			</div>
			<div class="col-12 top-margin" id="chart" style="height: 25em">
				
			</div>
			<p class="left-margins ">
			<label >Overall Subscriptions:</label> <b><i id="all-total"></i></b>
			</p>
			<input type="hidden" id="annual-subscription-filter">
			<div class="col-12 top-margin" id="annual-subscription-grid"></div>

			<div class="col-12">
				<input type="hidden" id="subscription-pdf-link" value="{{route('subscription-to-pdf')}}">
				<input type="hidden" id="subscription-excel-link" value="{{route('subscription-to-excel')}}">
				<button class="btn btn-default" onclick="getPDF();" style="float: right">Export to PDF</button>
				<button class="btn btn-default excel-btn" onclick="getExcel('xlsx');" style="float: right">Export to Excel</button>
				<button class="btn btn-default excel-btn" onclick="getExcel('csv');" style="float: right">Export to CSV</button>
			</div>
		</div>
	</div>
	<div class="col-12 card" >
		<div class="row">
			<h3 class="left-margins">Months Subscription</h3>
			<div class="col-12 row">

				<select class="form-control input-rounded custom-select width-64 left-margins" name="month" id="month-filter">
					<option value="1" >Jan</option>
					<option value="2" >Feb</option>
					<option value="3" >Mar</option>
					<option value="4" >Apr</option>
					<option value="5" >May</option>
					<option value="6" >Jun</option>
					<option value="7" >Jul</option>
					<option value="8" >Aug</option>
					<option value="9" >Sep</option>
					<option value="10" >Oct</option>
					<option value="11" >Nov</option>
					<option value="12" >Dec</option>
				</select>

				<input type="text" name="year" id="year" class="form-control input-rounded width-20 left-margins" placeholder="2018" value="2018">
				<select class="form-control input-rounded custom-select width-25 left-margins" id="subscription-model-filter">
					<option value="0">All Subscriptions</option>
					<option value="3">Annual Subscription</option>
					<option value="4">Monthly Subscription</option>
					<option value="5">Course Subscription</option>
				</select>
				<button class="btn btn-default" id="month-btn"  style="margin-left: 20px;"> Show Results</button>
			</div>
			<div class="col-12 top-margin" id="monthChart" >
				
			</div>
			<p id="month-total" class="d-none left-margins ">
			<label >Overall Subscriptions <small><i>(in selected month)</i></small>:</label> <b><i id="total"></i></b>
			</p>
			<input type="hidden" id="subscription-filter">
			<div class="col-12 top-margin" id="subscription-grid"></div>

			<div class="col-12">
				<button class="btn btn-default" onclick="getMonthPDF();" style="float: right">Export to PDF</button>
				<button class="btn btn-default excel-btn" onclick="getMonthExcel('xlsx');" style="float: right">Export to Excel</button>
				<button class="btn btn-default excel-btn" onclick="getMonthExcel('csv');" style="float: right">Export to CSV</button>
			</div>
			
		</div>
	</div>
	<div class="col-12 card">
		<div class="col-12 ">
			<div class="row">
				<h3 class="left-margins">Monthly Income</h3>
				<div class="col-12 row top-margin">
					<select class="form-control js-example-basic-multiple width-64 left-margins" multiple="1" name="income-month" id="income-month-filter">
						<option value="0" selected>All </option>
						<option value="1" >Jan</option>
						<option value="2" >Feb</option>
						<option value="3" >Mar</option>
						<option value="4" >Apr</option>
						<option value="5" >May</option>
						<option value="6" >Jun</option>
						<option value="7" >Jul</option>
						<option value="8" >Aug</option>
						<option value="9" >Sep</option>
						<option value="10" >Oct</option>
						<option value="11" >Nov</option>
						<option value="12" >Dec</option>
					</select>

					<input type="text" name="year" id="income-year" class="form-control input-rounded width-20 left-margins" placeholder="2018" value="2018">
					<select class="form-control input-rounded custom-select width-25 left-margins" id="income-subscription-model-filter">
						<option value="0">All Subscriptions</option>
						<option value="3">Annual Subscription</option>
						<option value="4">Monthly Subscription</option>
						<option value="5">Course Subscription</option>
					</select>
					<button class="btn btn-default" id="income-btn"  style="margin-left: 20px;width: 12.7%;"> Show Results</button>
				</div>
				<div class="col-12 row" id="months-income">

				</div>
				<p id="income-total" class="d-none left-margins ">
				<label>Overall Income <small><i>(in selected months)</i></small>:</label> <b><i id="income-total-val"></i></b>
				</p>
				<input type="hidden" id="income-filter">
				<div class="col-12 row" id="income-grid">

				</div>
				<div class="col-12">
					<input type="hidden" id="income-pdf-link" value="{{route('income-to-pdf')}}">
					<input type="hidden" id="income-excel-link" value="{{route('income-to-excel')}}">
					<button class="btn btn-default" onclick="getIncomePDF();" style="float: right">Export to PDF</button>
					<button class="btn btn-default excel-btn" onclick="getIncomeExcel('xlsx');" style="float: right">Export to Excel</button>
					<button class="btn btn-default excel-btn" onclick="getIncomeExcel('csv');" style="float: right">Export to CSV</button>
				</div>
			</div>
		</div>
	</div>
@endsection


@section ('scripts')
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
	<script type="text/javascript" src="<?php echo config('app.url'); ?>/js/subscription-report.js"></script>	

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>



@endsection 