@extends('admin.adminmaster')

@section ('title','Testimonials')

@section ('content')
{!! Form::model($testimonial,['route'=>['testimonial-edit-api', $testimonial->id] , 'files'=>true ]) !!}
	<div class="col-12 card" >
		<div class="row">
			<div class="col-6">
				{{ Form::label('name_ar' , 'Name (AR):')}}
				{{ Form::text('name_ar' , null , ['class' => 'form-control input-rounded'])}}
				<br>
				{{ Form::label('title_ar' , 'Title (AR):')}}
				{{ Form::text('title_ar' , null , ['class' => 'form-control input-rounded'])}}
				<br>
				{{ Form::label('description_ar' , 'Description (AR):')}}
				{{ Form::textarea('description_ar' , null , ['class' => 'form-control','style'=> 'height: 25%;'])}}
				
				<br>
				{{ Form::label('type' , 'Type: ')}}

				<div class="form-check-inline">
				  <label class="form-check-label">
				    <input type="radio" id="testimonial-type" class="form-check-input" name="type" value="Site" <?php if($testimonial->type=='Site'){ echo "checked"; }?>>Site Wise
				  </label>
				  <label class="form-check-label">
				    <input type="radio" id="testimonial-type" class="form-check-input" name="type" value="Course" <?php if($testimonial->type=='Course'){ echo "checked"; }?>>Course Wise
				  </label>
				</div>
				<div class="testimonial-type <?php if($testimonial->type=='Site'){ ?>d-none<?php } ?>">
				<br>
				{{ Form::label('course_id' , 'Course ')}}
				<select name="course_id" class="form-control">
					@foreach($course as $courseitem)
					<option value="{{$courseitem->id}}">{{$courseitem->title_en}}</option>
					@endforeach
				</select>
				</div>
			</div>
			<div class="col-6">
				{{ Form::label('name_en' , 'Name (EN):')}}
				{{ Form::text('name_en' , null , ['class' => 'form-control input-rounded'])}}
				<br>
				{{ Form::label('title_en' , 'Title (EN):')}}
				{{ Form::text('title_en' , null , ['class' => 'form-control input-rounded'])}}
				<br>
				{{ Form::label('description_en' , 'Description (EN):')}}
				{{ Form::textarea('description_en' , null , ['class' => 'form-control','style'=> 'height: 25%;'])}}
				<br>
				{{ Form::label('active' , 'Active: ')}}
				{{ Form::checkbox('active' , null)}}
			</div>
		</div>
		<div class="col-12" style="margin-top:10px">
			<div class="row">
				<div class="col-12">
					<div class="row">
						<!-- <div class="col-6">
							{{ Form::label('image_path_ar', 'Image (AR):')}}
							{{ Form::file('image_path_ar' , ['class'=>'form-control'])}}
						</div> -->
						<div class="col-6">
							{{ Form::label('image_path_en', 'Image:')}}
							{{ Form::file('image_path_en' , ['class'=>'form-control'])}}	
						</div>
						<div class="col-6">
							<img src="<?php echo config('app.url').'/public'.$testimonial->image_path_en; ?>" height="100px">
						</div>
					</div>
				</div>
			</div>
		</div>
	
	</div>
	<div class="col-12 card">
		{{ Form::submit('Update Level', ['class' => 'btn btn-success btn-block'])}}

		<a href="{{route('testimonials-admin')}}" class="btn btn-default btn-block" style="margin-top:10px;border: 1px solid #26dad2;">Cancel</a>
	</div>		
{!! Form::close() !!}
@endsection

@section ('scripts')
<script type="text/javascript">
	$('input[type=radio][name=type]').change(function() {
    if (this.value == 'Site') {
        $('.testimonial-type').addClass('d-none')
    }
    else if (this.value == 'Course') {
        $('.testimonial-type').removeClass('d-none');
    }
});

</script>
@endsection 