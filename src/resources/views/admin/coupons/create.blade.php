@extends('admin.adminmaster')

@section ('title','Coupon')

@section ('content')
			

	<div class="col-12 card" >
		{!! Form::open(['route' => 'coupon-create-api' , 'files' => 'true']) !!}	
			<div class="row">
				<div class="col-12">
					<h3> Main Details </h3>
				</div>
				<div class="col-6"> 
					{{ Form::label('qty' , 'Quantity :')}}
					{{ Form::number('qty' ,null , ['class'=>'form-control input-rounded', 'required' => 'required' ])}}

				</div>
				<div class="col-6">
					<label>For User</label>
					<input type="radio" name="type" id="user-rad" >
					<br>
					<label>For Course</label>
					<input type="radio" name="type" id="course-rad" >
					<br>
					<label>For Group</label>
					<input type="radio" name="type" id="group-rad" >

				</div>
				<div class="col-12 row">
					<div class="col-6">
						{{ Form::label('amount', 'Amount:')}}
						{{ Form::number('amount',null,['class'=>'form-control input-rounded']) }}
					</div>
					<div class="col-6">
						<div class="d-none" id="course-section">
							{{ Form::label('course_id' , 'Course:')}}
							<select class="input-rounded custom-select form-control"  name="course_id">
								<option value="0"></option>
								@foreach($courses as $course)
									<option value="{{$course->id}}" >{{$course->title_en}} </option>
								@endforeach
							</select> 
						</div>
						<div class="d-none" id="user-section">
							{{ Form::label('user_id' , 'User:')}}
							<select class="input-rounded custom-select form-control"  name="user_id">
									<option value="0"></option>
								@foreach($users as $user)
									<option value="{{$user->id}}" >{{$user->email}} </option>
								@endforeach
							</select>
						</div>
						<div class="d-none" id="group-section">
							{{ Form::label('group_id' , 'Group:')}}
							<select class="input-rounded custom-select form-control"  name="group_id">
									<option value="0"></option>
								@foreach($groups as $group)
									<option value="{{$group->id}}" >{{$group->name}} </option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="col-12">
					<hr>
					{{ Form::label('code', 'Code :')}}
					{{ Form::label('prefix' , 'As a Prefix')}}
					{{ Form::checkbox('prefix')}}
					{{ Form::text('code',null,['class'=>'form-control input-rounded' , 'maxlength'=>'15']) }}
					
				</div>

			</div>
	</div>

	<div class="col-12 card">
		{{ Form::submit('Submit Coupon' , ['class'=>'btn btn-success '])}}
		<a href="{{route('coupon-admin')}}" class="btn btn-default " style="margin-top: 10px;border: 1px solid #26dad2;"> Cancel</a>
	</div>

	{!! Form::close() !!}
@endsection


@section ('scripts')

	<script type="text/javascript">
		$('#user-rad').click(function (){
			if(this.checked){
				$('#user-section').removeClass('d-none');
				$('#group-section').addClass('d-none');
				$('#course-section').addClass('d-none');
			}
		});
		$('#course-rad').click(function(){
			if (this.checked){
				$('#user-section').addClass('d-none');
				$('#group-section').addClass('d-none');
				$('#course-section').removeClass('d-none');
			}
		});
		$('#group-rad').click(function(){
			if(this.checked){
				$('#user-section').addClass('d-none');
				$('#course-section').addClass('d-none');
				$('#group-section').removeClass('d-none');
			}
		})
	</script>
@endsection 