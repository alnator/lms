@extends('admin.adminmaster')

@section ('title','Stripe Plans')

@section ('content')
			

	<div class="col-12 card" >
		{!! Form::open(['route' => 'stripe-plan-create-api' ]) !!}	
			<div class="row">
				<div class="col-12">
					<h3> Main Details </h3>
				</div>
				<div class="col-12"> 
					{{ Form::label('course_id' , 'Course:') }}
					<select class="input-rounded custom-select form-control"  name="course_id">
						@foreach($courses as $course)
							<option value="{{$course->id}}" >{{$course->title_en}} </option>
						@endforeach
					</select>
					<br>
					
					{{ Form::label('subscription_model_id' , 'Subscription Model:') }}
					<select class="input-rounded custom-select form-control"  name="subscription_model_id">
						@foreach($subscriptions as $subscription)
							<option value="{{$subscription->id}}" >{{$subscription->name_en}} </option>
						@endforeach
					</select>
					<br>
					{{ Form::label('stripe_plan_id' , 'Stripe Plan ID')}}
					{{ Form::text('stripe_plan_id', null , ['class'=>'form-control input-rounded']) }}
					
				</div>
			 </div>
	</div>

	<div class="col-12 card">
		{{ Form::submit('Submit Stripe Plan' , ['class'=>'btn btn-success '])}}
		<a href="{{route('stripe-plan-admin')}}" class="btn btn-default " style="margin-top: 10px;border: 1px solid #26dad2;"> Cancel</a>
	</div>

	{!! Form::close() !!}
@endsection


@section ('scripts')

@endsection c