@extends('admin.adminmaster')

@section ('title','Stripe Plans')
@section ('styles')



    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
	<div class="col-12 card" id="stripe-plan-grid">
		
	</div>
	<div class="col-12 card">
		<a href="{{route('stripe-plan-create')}}" class="btn btn-success btn-block">Add Stripe Plan</a>
	</div>
@endsection


@section ('scripts')
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/stripe-plan-grid.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>

@endsection 