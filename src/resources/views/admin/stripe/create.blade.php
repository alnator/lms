@extends('admin.adminmaster')

@section ('title','Stripe')

@section ('content')
			

	<div class="col-12 card" >
		{!! Form::open(['route' => 'stripe-create-api' ]) !!}	
			<div class="row">
				<div class="col-12">
					<h3> Main Details </h3>
				</div>
				<div class="col-6"> 
					{{ Form::label('user_id' , 'User:') }}
					<select class="input-rounded custom-select form-control"  name="user_id">
						@foreach($users as $user)
							<option value="{{$user->id}}" >{{$user->name}} </option>
						@endforeach
					</select>

					<br><br>
					{{ Form::label('email' , 'Email:')}}
					{{ Form::email('email' ,null , ['class'=>'form-control input-rounded', 'required' => 'required' ])}}
				
				</div>
				<div class="col-6">
					{{ Form::label('stripe_id', 'Stripe ID:')}}
					{{ Form::text('stripe_id',null,['class'=>'form-control input-rounded', 'required' => 'required']) }}
					<br>
					{{ Form::label('card_id' , 'Card ID:')}}
					{{ Form::number('card_id' ,null , ['class'=>'form-control input-rounded', 'required' => 'required' ])}}
				</div>
			 </div>
	</div>

	<div class="col-12 card">
		{{ Form::submit('Submit Stripe Customer' , ['class'=>'btn btn-success '])}}
		<a href="{{route('stripe-admin')}}" class="btn btn-default " style="margin-top: 10px;border: 1px solid #26dad2;"> Cancel</a>
	</div>

	{!! Form::close() !!}
@endsection


@section ('scripts')

@endsection c