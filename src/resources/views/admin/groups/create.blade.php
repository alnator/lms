@extends('admin.adminmaster')

@section ('title','Group')

@section ('content')
{!! Form::open(['route'=>'groups-create-api' , 'files'=>true]) !!}
	<div class="col-12 card" >
		<div class="row">
			<div class="col-12">
				@if(!empty(session('error') ) )
					<div class="alert alert-danger" role="alert">
					  {{ session('error') }}
					</div>
				@endif
			</div>
			<div class="col-6">
				{{ Form::label('name' , 'Name (AR):')}}
				{{ Form::text('name' , null , ['class' => 'form-control input-rounded'])}}
			</div>
			<div class="col-6">
				{{ Form::label('file' , 'Excel Sheet:')}}<br>
				{{ Form::file('file', null , ['class'=>'form-control'])}}
			</div>
		</div>
	
	</div>
	<div class="col-12 card">
		{{ Form::submit('Submit Group', ['class' => 'btn btn-success btn-block'])}}

		<a href="{{route('groups-admin')}}" class="btn btn-default btn-block" style="margin-top:10px;">Cancel</a>
	</div>		
{!! Form::close() !!}
@endsection

@section ('scripts')

@endsection 