@extends('admin.adminmaster')

@section ('title','Group Users')
@section ('styles')

    <!-- <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet"> -->

@endsection

@section ('content')
	<div class="col-12 card">
		<div class="col-12" style="text-align: center;"> 
			<h2>Group: <i>{{$group->name}}</i></h2>
		</div>
		<div class="col-12" >
			<br>
			<h4>Add Using Excel</h4>
			<p>This feature will let you upload the users using excel sheet.<br>
			<i>Required Fields: 'name', 'email', 'phone', 'country', 'amount' and 'subscription' as "annual or monthly". </i></p>
			<label>Upload Excel Sheet:</label>
			<input type="file" name="file" id="file-input" class="form-control" accept=".xls, .xlsx">
		</div>
		<div class="col-12 d-none" id="added-members">
			<div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>User Name</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Country</th>
                            <th>Subscription Model</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody id="users-table-body">
                    </tbody>
                </table>
            </div>
		</div>
	</div>
{!! Form::open(['route'=>['menual-add-group-members-api' ,$group->id] , 'files'=>true]) !!}
 <div class="col-12 card">
        <div class="col-12" >
            <br>
            <h4>Menual Add Member</h4>
        </div>
        <div class="row">
            <div class="col-6">
                {{ Form::label('name' , 'Name')}}
                {{ Form::text('name' , null , ['class' => 'form-control input-rounded', 'required'=>'required'])}}
            </div>
            <div class="col-6">
                {{ Form::label('email' , 'Email')}}
                {{ Form::email('email' , null , ['class' => 'form-control input-rounded', 'required'=>'required'])}}
            </div>
            <div class="col-6">
                {{ Form::label('phone' , 'Phone')}}
                {{ Form::number('phone' , null , ['class' => 'form-control input-rounded', 'required'=>'required'])}}
            </div>
            <div class="col-6">
                {{ Form::label('amount' , 'Amount')}}
                {{ Form::number('amount' , null , ['class' => 'form-control input-rounded', 'required'=>'required'])}}
            </div>
            <div class="col-6">
                {{ Form::label('country' , 'Country')}}
                {{ Form::text('country' , null , ['class' => 'form-control input-rounded', 'required'=>'required'])}}
            </div>
            
            <div class="col-6">
                {{ Form::label('subscription', 'Subscription:')}}
                <select class="input-rounded custom-select form-control"  name="subscription" required="required">
                    <option value="" >Select Plan</option>
                    <option value="annual" >Annual</option>
                    <option value="monthly" >Monthly</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-12 card">
        {{ Form::submit('Add Member', ['class' => 'btn btn-success btn-block'])}}

        <a href="{{route('groups-admin')}}" class="btn btn-default btn-block" style="margin-top:10px;">Cancel</a>
    </div>      
{!! Form::close() !!}
@endsection


@section ('scripts')
    <script type="text/javascript">
    var link = "{{route('add-group-members-api' ,$group->id)}}";
    </script>
    <script type="text/javascript">
    	$('#file-input').change(function (event){
    		$('#added-members').removeClass('d-none');
    		file = event.target.files[0];
	    	var formData = new FormData();
	    	formData.append( 'file' , file);

	    	ajaxFunc(link, formData);
    	});
    	function ajaxFunc(link , formData){

    		$.ajax({
    		    type: "POST",
		        url: link,
		        cache: false,
        		dataType: 'json',
        		processData: false,
		        data: formData,
		        contentType: false,
		        headers: {
		            "x-csrf-token": $("[name=_token]").val()
		        }
		    }).always(response=> {
				$('#users-table-body').html(response.responseText);
			});


    	}
    </script>
@endsection 