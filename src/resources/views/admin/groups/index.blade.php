@extends('admin.adminmaster')

@section ('title','Groups')
@section ('styles')

    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
	<div class="col-12 card" id="groups-grid">
		
	</div>
	<div class="col-12 card">
		<a href="{{route('groups-create')}}" class="btn btn-success btn-block">Create Group</a>
	</div>
@endsection


@section ('scripts')
	<script type="text/javascript">var siteurl= " <?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/groups-grid.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
    
@endsection 