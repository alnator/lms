@if(count($users) > 0)
	@foreach($users as $user)
	    <tr>
	        <th scope="row">{{$user['name']}}</th>
	        <td>{{$user['phone']}}</td>
	        <td>{{$user['email']}}</td>
	        <td>{{$user['country']}}</td>
	        <td>{{$user['subscription']}}</td>
	        <td>{{$user['amount']}}</td>
	    </tr>
	@endforeach
@else
	<tr>
        <th scope="row" colspan="6">Something Went Wrong. Please Upload Unique name,email</th>
    </tr>
@endif