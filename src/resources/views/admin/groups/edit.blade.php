@extends('admin.adminmaster')

@section ('title','Group')

@section ('content')
{!! Form::model($group,['route'=>['groups-edit-api' , $group->id] , 'files'=>true]) !!}
	<input type="hidden" name="id" value="{{$group->id}}">
	<div class="col-12 card" >
		<div class="row">
			<div class="col-6">
				{{ Form::label('name' , 'Name (AR):')}}
				{{ Form::text('name' , null , ['class' => 'form-control input-rounded'])}}
			</div>
			<div class="col-6">
				{{ Form::label('file', 'Excel File:')}}<br>
				{{ Form::file('file', null, ['class'=>'form-control'])}}
			</div>
		</div>
	
	</div>
	<div class="col-12 card">
		{{ Form::submit('Update Group', ['class' => 'btn btn-success btn-block'])}}

		<a href="{{route('groups-admin')}}" class="btn btn-default btn-block" style="margin-top:10px;">Cancel</a>
	</div>		
{!! Form::close() !!}
@endsection

@section ('scripts')

@endsection 