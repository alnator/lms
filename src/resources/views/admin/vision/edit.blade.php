@extends('admin.adminmaster')

@section ('title','Vision')

@section ('content')
{!! Form::model($vision,['route'=>['vision-edit-api', $vision->id] , 'files'=>true ]) !!}
	<div class="col-12 card" >
		<div class="row">
			<div class="col-6">
				{{ Form::label('name_en' , 'Name (EN):')}}
				{{ Form::text('name_en' , null , ['class' => 'form-control input-rounded'])}}
				<br>
				
				{{ Form::label('image_path_en', 'Image:')}}
				{{ Form::file('image_path_en' , ['class'=>'form-control'])}}
				
				<br>

				<img src="<?php echo config('app.url').'/public'.$vision->image_path_en; ?>" height="100px">

				
			</div>
			<div class="col-6">
				
				{{ Form::label('name_ar' , 'Name (AR):')}}
				{{ Form::text('name_ar' , null , ['class' => 'form-control input-rounded'])}}
				
				<br>
				
				{{ Form::label('active' , 'Active: ')}}
				{{ Form::checkbox('active' , null)}}
			</div>
		</div>
		
	
	</div>
	<div class="col-12 card">
		{{ Form::submit('Update', ['class' => 'btn btn-success btn-block'])}}

		<a href="{{route('vision-admin')}}" class="btn btn-default btn-block" style="margin-top:10px;border: 1px solid #26dad2;">Cancel</a>
	</div>		
{!! Form::close() !!}
@endsection

@section ('scripts')
<script type="text/javascript">
	$('input[type=radio][name=type]').change(function() {
    if (this.value == 'Site') {
        $('.vision-type').addClass('d-none')
    }
    else if (this.value == 'Course') {
        $('.vision-type').removeClass('d-none');
    }
});

</script>
@endsection 