@extends('admin.adminmaster')

@section ('title','Vision')

@section ('content')
{!! Form::open(['route'=>'vision-create-api' , 'files'=>true]) !!}
	<div class="col-12 card" >
		<div class="row">
			<div class="col-6">

				{{ Form::label('name_en' , 'Name (EN):')}}
				{{ Form::text('name_en' , null , ['class' => 'form-control input-rounded','required'=>'required'])}}


				<br>

				{{ Form::label('image_path_en', 'Image:')}}
					{{ Form::file('image_path_en' , ['class'=>'form-control','required'=>'required'])}}


			</div>
			<div class="col-6">
				
				
				{{ Form::label('name_ar' , 'Name (AR):')}}
				{{ Form::text('name_ar' , null , ['class' => 'form-control input-rounded','required'=>'required'])}}
				
				<br>
				{{ Form::label('active' , 'Active: ')}}
				{{ Form::checkbox('active' , null,['checked'=>'checked'])}}
				
			</div>
			
			
		</div>
		
	
	</div>
	<div class="col-12 card">
		{{ Form::submit('Submit', ['class' => 'btn btn-success btn-block'])}}

		<a href="{{route('vision-admin')}}" class="btn btn-default btn-block" style="margin-top:10px;">Cancel</a>
	</div>		
{!! Form::close() !!}
@endsection
