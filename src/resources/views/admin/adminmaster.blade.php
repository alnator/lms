<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo config('app.url'); ?>/images/favicon.png">
    <title>Eureka Admin Panel</title>
    @include('admin.partial.header');
    
</head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    
    <!-- Main wrapper  -->
    <div id="main-wrapper" style="margin-top: -1.5em">
        @csrf
        <!-- header header  -->
        @include('admin.partial.nav')
        <!-- End header header -->
        <!-- Left Sidebar  -->
        @include('admin.partial.left')
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">@yield('title') Dashboard</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Home</li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row" style="display: block">
                  @yield('content')
                </div>
            </div>



            <footer class="footer"> <center>© 2018 All rights reserved.  <a href="http://eurekatechacademy.com">Eureka</a></center></footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>


    @include('admin.partial.footer');
    

</body>

</html>
