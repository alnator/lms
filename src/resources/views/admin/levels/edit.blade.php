@extends('admin.adminmaster')

@section ('title','Theme')

@section ('content')
{!! Form::model($level,['route'=>['level-edit-api', $level->id] , 'files'=>true ]) !!}
	<div class="col-12 card" >
		<div class="row">
			<div class="col-6">
				{{ Form::label('name_ar' , 'Name (AR):')}}
				{{ Form::text('name_ar' , null , ['class' => 'form-control input-rounded'])}}
				<br>
				{{ Form::label('description_ar' , 'Description (AR):')}}
				{{ Form::text('description_ar' , null , ['class' => 'form-control', 'style'=> '    height: 35%;'])}}
				<br>
				{{ Form::label('slug', 'Slug: ')}} <small> (No Spaces,Small Letters Only)</small>
				{{ Form::text('slug' , null , ['class'=>'form-control input-rounded' , 'required'=>'required' ,'pattern'=>'^\S+$']) }}
			</div>
			<div class="col-6">
				{{ Form::label('name_en' , 'Name (EN):')}}
				{{ Form::text('name_en' , null , ['class' => 'form-control input-rounded'])}}
				<br>
				{{ Form::label('description_en' , 'Description (EN):')}}
				{{ Form::textarea('description_en' , null , ['class' => 'form-control', 'style'=> '    height: 35%;'])}}
				<br>
				{{ Form::label('active' , 'Active: ')}}
				{{ Form::checkbox('active' , null)}}
			</div>
		</div>
		<div class="col-12" style="margin-top:20px">
			<div class="row">
				<div class="col-12">
					<div class="row">
						<!-- <div class="col-6">
							{{ Form::label('image_path_ar', 'Image (AR):')}}
							{{ Form::file('image_path_ar' , ['class'=>'form-control'])}}
						</div> -->
						<div class="col-6">
							{{ Form::label('image_path_en', 'Image:')}}
							{{ Form::file('image_path_en' , ['class'=>'form-control'])}}
							<img src="<?php echo config('app.url'); ?>/public{{$level->image_path_en}}" height="100px">	
						</div>
						<div class="col-6">
							{{ Form::label('icon_path', 'Icon Image:')}}
							{{ Form::file('icon_path' , ['class'=>'form-control'])}}
							<img src="<?php echo config('app.url'); ?>/public{{$level->icon_path}}" height="100px">	
						</div>
					</div>
				</div>
			</div>
		</div>
	
	</div>
	<div class="col-12 card">
		{{ Form::submit('Update Theme', ['class' => 'btn btn-success btn-block'])}}

		<a href="{{route('levels-admin')}}" class="btn btn-default btn-block" style="margin-top:10px;border: 1px solid #26dad2;">Cancel</a>
	</div>		
{!! Form::close() !!}
@endsection

@section ('scripts')

@endsection 