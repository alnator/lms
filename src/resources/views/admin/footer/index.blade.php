@extends('admin.adminmaster')

@section ('title','Numbers and Links')

@section ('content')
{!! Form::model($footer,['route'=>['footer-admin-api', $footer->id]]) !!}
	<div class="col-12 card" >
		<div class="row">
			<div class="col-6">
				{{ Form::label('number1' , 'First Number:')}}
				{{ Form::text('number1' , null , ['class' => 'form-control input-rounded'])}}
				<br>
			</div>
			<div class="col-6">
				{{ Form::label('number2', 'Second Number: ')}}
				{{ Form::text('number2' , null , ['class'=>'form-control input-rounded']) }}
				<br>
			</div>
			<div class="col-6">
				{{ Form::label('email' , 'Email: ')}}
				{{ Form::text('email' , null , ['class' => 'form-control input-rounded'])}}
				<br>
			</div>
			<div class="col-6">
				{{ Form::label('margin' , 'Margin Percentage: ')}}
				{{ Form::number('margin' , null , ['class' => 'form-control input-rounded', 'required' => 'required'])}}
				<br>
			</div>
		</div>
		<div class="col-12" style="margin-top:20px">
			<div class="row">
				<br><br>
				<div class="col-6">
					{{ Form::label('facebook', 'Facebook Link:')}}
					{{ Form::text('facebook' , null, ['class'=>'form-control input-rounded'])}}
					<br>
				</div>
				<div class="col-6">
					{{ Form::label('twitter', 'Twitter Link:')}}
					{{ Form::text('twitter' , null , ['class'=>'form-control input-rounded'])}}
					<br>
				</div>
				<div class="col-6">
					{{ Form::label('instagram', 'Instagram Link:')}}
					{{ Form::text('instagram' , null , ['class'=>'form-control input-rounded'])}}	
					<br>
				</div>
				<div class="col-6">
					{{ Form::label('youtube', 'Youtube Link:')}}
					{{ Form::text('youtube' , null , ['class'=>'form-control input-rounded'])}}	
					<br>
				</div>


				<div class="col-3">
					{{ Form::label('currency_symbol', 'Currency Symbol:')}}
					{{ Form::text('currency_symbol' , null , ['class'=>'form-control input-rounded'])}}	
					<br>
				</div>
				<div class="col-3">
					{{ Form::label('currency_conversation', 'Currency Conversation:')}}
					{{ Form::text('currency_conversation' , null , ['class'=>'form-control input-rounded'])}}	
					<br>
				</div>


				<div class="col-6">
					{{ Form::label('currency_position', 'Currency Position:')}}
					<select name="currency_position" class="form-control input-rounded">
						<option value="after" @if($footer->currency_position == 'after') selected @endif>After</option>
						<option value="before" @if($footer->currency_position == 'before') selected @endif>Before</option>
					</select>
					<br>
					
				</div>


			</div>
		</div>
	
	</div>
	<div class="col-12 card">
		{{ Form::submit('Update', ['class' => 'btn btn-success btn-block'])}}
	</div>		
{!! Form::close() !!}
@endsection

@section ('scripts')

@endsection 