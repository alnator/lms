@extends('admin.adminmaster')

@section ('title',"$user->name Courses")
@section ('styles')



    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
	<div class="col-12 card" id="user-courses-grid">
		
	</div>
@endsection


@section ('scripts')
	<script type="text/javascript">
		var userId = {{$user->id}};
		var siteurl = "<?php echo config('app.url'); ?>";
	</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/user-courses-grid.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
    
@endsection 