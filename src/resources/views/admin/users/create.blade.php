@extends('admin.adminmaster')

@section ('title','Create New User')

@section ('styles')

<style type="text/css">
	.col-6{
		margin-top: 30px;
	}
</style>

@endsection

@section ('content')
	<div class="col-12">
		{{ Form::open(['route'=>'user-create-api', 'files'=>'true'])}}
			
		<div class="row">
			<div class="col-12">
				User Details
			</div>
			<div class="col-6">
				{{ Form::label('name', 'Username: ') }}
				{{ Form::text('name', null , ['class'=>'form-control input-rounded' , 'required' => 'required']) }}
			</div>
			<div class="col-6">
				{{ Form::label('age', 'Age: ') }}
				{{ Form::number('age', null , ['class'=>'form-control input-rounded', 'required' => 'required']) }}
			</div>
			<div class="col-6">
				{{ Form::label('phone', 'Phone Number: ') }}
				{{ Form::text('phone', null , ['class'=>'form-control input-rounded', 'required' => 'required']) }}
			</div>
			<div class="col-6">
				{{ Form::label('email', 'Email: ') }}
				{{ Form::text('email', null , ['class'=>'form-control input-rounded', 'required' => 'required']) }}
			</div>
			<div class="col-6">
				{{ Form::label('country', 'Country: ') }}
				{{ Form::text('country', null , ['class'=>'form-control input-rounded', 'required' => 'required']) }}
			</div>
			<div class="col-6">
				{{ Form::label('role', 'Role: ') }}
				<select class="form-control input-rounded" name="role">
					<option value="1" selected>User</option>
					<option value="2">Teacher</option>
					<option value="3">Supplier</option>
				</select>
			</div>
			<div class="col-6">
				{{ Form::label('password', 'Password: ') }}
				{{ Form::text('password', null , ['class'=>'form-control input-rounded', 'required' => 'required']) }}
			</div>
			<div class="col-6">
				{{ Form::label('image_path', 'Image: ') }}<small><small>(optional)</small></small><br>
				{{ Form::file('image_path') }}
			</div>

			<div class="col-6">
				{{ Form::label('active', 'Active: ') }}
				{{ Form::checkbox('active') }}	
			</div>

			<div class="col-6">
				{{ Form::label('admin', 'Admin: ') }}
				{{ Form::checkbox('admin') }}	
			</div>
		</div>
	</div>		
	<div class="col-12" style="margin-top: 30px">
		{{ Form::submit('Create User' , ['class'=>'btn btn-block btn-success']) }}
		<a href="{{route('users-admin')}}" class="btn btn-block btn-default">Cancel</a>
		{{ Form::close() }}
	</div>

@endsection

@section ('scripts')

@endsection 