@extends('admin.adminmaster')

@section ('title','Update User Details')

@section ('styles')
<style type="text/css">
	.col-6{
		margin-top: 30px;
	}
</style>
@endsection

@section ('content')
	<div class="col-12">
			{{ Form::model( $user,['route'=>['user-edit-api',$user->id], 'files'=>'true'])}}

		<div class="row">
			<div class="col-12">
				<h2>User Details</h2>
			</div>
			<div class="col-6">
				{{ Form::label('name', 'Username: ') }}
				<br>
				{{ Form::text('name', null , ['class'=>'form-control input-rounded', 'required' => 'required']) }}
			</div>
			<br>
			<div class="col-6">
				{{ Form::label('age', 'Age: ') }}
				<br>
				{{ Form::number('age', null , ['class'=>'form-control input-rounded', 'required' => 'required']) }}
			</div>
			<br>
			<div class="col-6">
				{{ Form::label('phone', 'Phone Number: ') }}
				<br>
				{{ Form::text('phone', null , ['class'=>'form-control input-rounded', 'required' => 'required']) }}
			</div>
			<br>
			<div class="col-6">
				{{ Form::label('email', 'Email: ') }}
				<br>
				{{ Form::text('email', null , ['class'=>'form-control input-rounded', 'required' => 'required']) }}
			</div>
			<br><div class="col-6">
				{{ Form::label('country', 'Country: ') }}
				<br>
				{{ Form::text('country', null , ['class'=>'form-control input-rounded', 'required' => 'required']) }}
			</div>
			<br>
			<div class="col-6">
				{{ Form::label('role', 'Role: ') }}
				<select class="form-control custom-select input-rounded" name="role">
					<option value="1" {{$user->supplier == 0 && $user->admin == 0 ? 'selected=selected' :''}}>User</option>
					<option value="2" {{$user->teacher == 1 ? 'selected=selected' : '' }}>Teacher</option>
					<option value="3" {{ $user->supplier ==1 ? 'selected=selected' : '' }}>Supplier</option>
				</select>
			</div>
			<br>
			<div class="col-6">
				{{ Form::label('image_path', 'Image: ') }}<small><small>(optional)</small></small>
				<br>
				{{ Form::file('image_path') }}
				<img src="<?php echo config('app.url'); ?>/public{{$user->image_path}}" height="100px">
			</div>
			<div class="col-6">
				{{ Form::label('active', 'Active: ') }}
				{{ Form::checkbox('active' , null) }}	
			</div>

			<div class="col-6">
				{{ Form::label('admin', 'Admin: ') }}
				{{ Form::checkbox('admin', null) }}	
			</div>


		</div>
	</div>		
	<div class="col-12" style="margin-top: 20px"> 
		{{ Form::submit('Update User' , ['class'=>'btn btn-block btn-success']) }}
		<a href="{{route('users-admin')}}" class="btn btn-block btn-default">Cancel</a>
		{{ Form::close() }}
	</div>

@endsection

@section ('scripts')

@endsection 