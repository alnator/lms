@extends('admin.adminmaster')

@section ('title','Users')
@section ('styles')



    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
	<div class="col-12 card" id="users-grid">
		
	</div>
	<div class="col-12 card">
		<a href="{{route('user-create')}}" class="btn btn-block btn-default"> Add New User </a>
	</div>
@endsection


@section ('scripts')
<script type="text/javascript">var siteurl= " <?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/users-grid.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
    
@endsection 