@extends('admin.adminmaster')

@section ('title','Courses')

@section ('content')




	<div class="col-12 card" >
		<div class="row">
			<div class="col-12">
				<h3> Course Details </h3>
			</div>
			<div class="col-6"> 
				<label>Title EN: <b> &nbsp;&nbsp;&nbsp; {{$course->title_en }} </b></label>
				<br>
				<label>Theme ID:  &nbsp;&nbsp;&nbsp; <b>{{$course->level_id }}</b></label>
				<br>
				<label>Price:     &nbsp;&nbsp;&nbsp; <b>{{$course->price}}</b></label>
				<br>
				<label>Overview EN: &nbsp;&nbsp;&nbsp; <b>{{$course->overview_en}}</b></label>
				
			</div>
			<div class="col-6">
				<label>Title AR:  &nbsp;&nbsp;&nbsp; <b>{{$course->title_ar }}</b></label>
				<br>
				<label>language:  &nbsp;&nbsp;&nbsp; <b>{{ isset($course->language) ?$course->language : 'Arabic - English' }}</b></label>
				<br>
				<label>Course Estimated Time :  &nbsp;&nbsp;&nbsp; <b>{{$course->course_estimated_time }}</b></label>
				<br>
				<label>OverView AR:  &nbsp;&nbsp;&nbsp; <b>{{$course->overview_ar}}</b></label>
				<br>

			</div>
		 </div>
		
	</div>

	<div class="col-12 card" >
		<div class="row">
			<div class="col-6">
				<label>Description EN: &nbsp;&nbsp;&nbsp; <p><b>{{$course->description_en}} </b></p></label>
				
			</div>
			<div class="col-6">
				<label>Description AR: &nbsp;&nbsp;&nbsp; <p><b>{{$course->description_ar}} </b></p></label>
			</div>
			<div class="col-4 offset-5">
				<label>Active: &nbsp;&nbsp;&nbsp; <b> {{$course->active == 1 ? 'Yes' : 'No' }}</b></label>
			</div>
		</div>
	</div>
	
	<div class="col-12 card">
		<a href="{{route('courses-admin' , $id)}}" class="btn btn-default " style="margin-top: 10px;border: 1px solid #26dad2;"> Cancel</a>
	</div>

	{!! Form::close() !!}
@endsection


@section ('scripts')

@endsection 