@extends('admin.adminmaster')

@section ('title','Courses')

@section ('content')




	<div class="col-12 card" >
		<div class="row">
			<div class="col-12">
				<h3> Course Details </h3>
			</div>
			<div class="col-6"> 
				<label>Title EN: <b> &nbsp;&nbsp;&nbsp; {{$course->title_en }} </b></label>
				<br>
				<label>Level ID:  &nbsp;&nbsp;&nbsp; <b>{{$course->level_id }}</b></label>
				<br>
				<label>Price:     &nbsp;&nbsp;&nbsp; <b>{{$course->price}}</b></label>
				<br>
				<label>Overview EN: &nbsp;&nbsp;&nbsp; <b>{{$course->overview_en}}</b></label>
			</div>
			<div class="col-6">
				<label>Title AR:  &nbsp;&nbsp;&nbsp; <b>{{$course->title_ar }}</b></label>
				<br>
				<label>language:  &nbsp;&nbsp;&nbsp; <b>{{ isset($course->language) ?$course->language : 'Arabic - English' }}</b></label>
				<br>
				<label>Course Estimated Time :  &nbsp;&nbsp;&nbsp; <b>{{$course->course_estimated_time }}</b></label>
				<br>
				<label>OverView AR:  &nbsp;&nbsp;&nbsp; <b>{{$course->overview_ar}}</b></label>
			</div>
		 </div>
		
	</div>



	<div class="col-12 card" >
		{!! Form::open(['route' => ['add-user-api' , $id]]) !!}		

		<div class="row">

			<div class="col-12">
				<h3> Payment </h3>
			</div>
			<div class="col-6"> 
				{{ Form::text('id' , $id , ['hidden' => 'hidden'])}}


				{{ Form::label('user_id', 'User:')}}
				<select class="input-rounded custom-select form-control" required="required" name="user_id">
				@foreach($users as $user)
					<option value="{{$user->id}}">{{$user->name}}</option>
				@endforeach
				</select>
				
			</div>
			<div class="col-6">
				{{ Form::label('amount' , 'Amount :')}}
				{{ Form::number('amount', null , ['class'=>'form-control input-rounded'])}}
			</div>
			<div class="col-12"> 
				{{ Form::label('payment_method_id', 'PaymentMethod:')}}
				<select class="input-rounded custom-select form-control" required="required" name="payment_method_id">
				@foreach($paymentmethods as $paymentmethod)
					<option value="{{$paymentmethod->id}}">{{$paymentmethod->name}}</option>
				@endforeach
				</select>
			</div>
		 </div>
		
	</div>
	<div class="col-12 card" >
		<div class="row">
			<div class="col-12">
				<h3>Subscription</h3>
			</div>
			<div class="col-12"> 
				{{ Form::label('subscription_model_id', 'Subscription:')}}
				<select class="input-rounded custom-select form-control" required="required" name="subscription_model_id">
				@foreach($subscriptions as $subscription)
					<option value="{{$subscription->id}}">{{$subscription->name_en}} - Price:{{$subscription->display_price}} -
						Days:{{ $subscription->period_in_days}}</option>
				@endforeach
				</select>
			</div>
		</div>

	</div>

	<div class="col-12 card">
		{{ Form::submit('Submit User' , ['class'=>'btn btn-success '])}}
		<a href="{{route('course-users-admin' , $id)}}" class="btn btn-default " style="margin-top: 10px;border: 1px solid #26dad2;"> Cancel</a>
	</div>

	{!! Form::close() !!}
@endsection


@section ('scripts')

@endsection 