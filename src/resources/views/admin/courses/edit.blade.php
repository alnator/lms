@extends('admin.adminmaster')

@section ('title','Courses')
@section ('styles')
<script src="<?php echo config('app.url'); ?>/js/ckeditor/ckeditor.js"></script>
@endsection
@section ('content')
		{!! Form::model($course,['route' => ['courses-edit-api',$course->id] , 'files' => 'true']) !!}		

	<div class="col-12 card" >
			<div class="row">
				<div class="col-12">
					<h3> Main Details </h3>
				</div>
				<div class="col-6"> 
					{{ Form::label('level', 'Level:')}}
					<select class="input-rounded custom-select form-control" required="required" name="level_id">
					@foreach($levels as $level)
						<option value="{{$level->id}}" {{$course->level_id == $level->id ? 'selected="selected"' : ''}} >{{$level->name_en}} </option>
					@endforeach
					</select>
					<input hidden="hidden" value="{{$course->wide_image_path}}" name="orginal_wide_image_path">
					<input hidden="hidden" value="{{$course->image_path}}" name="orginal_image_path">
					<br>
					{{ Form::label('title_ar', 'Arabic Title:')}}
					{{ Form::text('title_ar' , null , ['class'=>'form-control input-rounded' , 'required'=>'required'])}}
					<br>
					{{ Form::label('overview_ar', 'Arabic Overview') }}
					{{ Form::textarea('overview_ar', null, ['class'=>'form-control' , 'style'=> '    height: 25%;'] ) }}
					<br>
					{{ Form::label('description_ar', 'Arabic Descrtiption') }}
					{{ Form::textarea('description_ar', null, ['class'=>'form-control' , 'style'=> '    height: 35%;'] ) }}

				</div>
				<div class="col-6">
					{{ Form::label('lang', 'Language:')}}
					<select class="input-rounded custom-select form-control" required="required" name="lang">
						<option value="1">Arabic  </option>
						<option value="2">English </option>
					</select>
					<br>
					{{ Form::label('title_en', 'English Title:')}}
					{{ Form::text('title_en' , null , ['class'=>'form-control input-rounded' , 'required'=>'required'])}}
					<br>			
					{{ Form::label('overview_en', 'English Overview') }}
					{{ Form::textarea('overview_en', null, ['class'=>'form-control' , 'style'=> '    height: 25%;'] ) }}
					<br>
					{{ Form::label('description_en', 'English Descrtiption') }}
					{{ Form::textarea('description_en', null, ['class'=>'form-control' , 'style'=> '    height: 35%;'] ) }}

				</div>
			 </div>
		
	</div>
	<div class="col-12 card" >
		<div class="row">
			<div class="col-12">
				<h3>More Details</h3>
			</div>
			<div class="col-6"> 
				{{ Form::label('price' , 'Price:') }}
				{{ Form::number('price' , null , ['class'=>'form-control input-rounded' ,'required'=>'required']) }}
				<br>
				{{ Form::label('emonth' , 'Month:') }}
				
				<select name="emonth" class="form-control input-rounded" required="required">
					<option value="">--Select Month--</option>
					<?php for($i=1;$i<=12;$i++){ ?>
					<option value="{{$i}}" @if($course->emonth == $i) selected @endif>{{$i}}</option>
				<?php } ?>
				</select>
				<br>

				{{ Form::label('slug', 'Slug: ')}} <small> (No Spaces,Small Letters Only)</small>
				{{ Form::text('slug' , null , ['class'=>'form-control input-rounded' , 'required'=>'required' ,'pattern'=>'^\S+$']) }}
				<br>
				{{ Form::label('is_youtube', 'Youtube:') }}
				{{ Form::checkbox('is_youtube' ,'1' ) }}
				<div id="link" class="d-none">
				{{ Form::label('video' , 'Youtube Link:')}}
				{{ Form::text('url_identifier' , null , ['class'=>'form-control' , 'placeholder' => 'for eg . ADCF7894'])}}
				
				</div>
				
				
				<br>
				{{ Form::label('wide_image_path' , ' Wide Image:') }}
				{{ Form::file('wide_image_path',['class' => 'form-control'])}}
				<img src="<?php echo config('app.url'); ?>/public{{$course->wide_image_path}}" height="100px">
				
			</div>
			<div class="col-6">
				{{ Form::label('course_estimated_time' , 'Estimated Time:') }}
				{{ Form::number('course_estimated_time' , null , ['class'=>'form-control input-rounded','required'=>'required']) }}
				<br>
				{{ Form::label('video' , 'Youtube Embed Code:') }}
				{{ Form::text('video',$course->intro_video_path , ['class' => 'form-control '])}}
				<br>
				{{ Form::label('active' , 'Active: ')}}
				{{ Form::checkbox('active' , null)}}
				<br>
				{{ Form::label('kit_id' , 'Kit: ')}}
				<select class="input-rounded custom-select form-control" name="kit_id">
					<option></option>
					@foreach($kits as $kit)
						<option value="{{$kit->id}}" {{$course->kit_id == $kit->id ? 'selected="selected"' : ''}} >{{$kit->name_en}} </option>
					@endforeach
				</select>
				<br>
				<br>
				{{ Form::label('user_id','Teacher:')}}
				<select class="input-rounded custom-select form-control" required="required" name="user_id">
				@foreach($users as $user)
					<option value="{{$user->id}}">{{$user->email}}</option>
				@endforeach
				</select>
<br>
<br>
				<div id="upload-video">
				{{ Form::label('video' , 'Image:') }}
				{{ Form::file('image' , ['class' => 'form-control'])}}
				<img src="<?php echo config('app.url'); ?>/public{{$course->image_path}}" height="100px">
				</div>


			</div>
		</div>

	</div>

	<div class="col-12 card" >
		<div class="row">
			<div class="col-12">
				<h3>Attachements Details <i><small>(optional)</small></i></h3>
			</div>
			<input type="hidden" name="attachement_id" value="{{$course->downloadable->id ?? null}}">

			<div class="col-6"> 
				{{ Form::label('attachement_name_en' , 'Attachement Name EN:') }}
				{{ Form::text('attachement_name_en' , $course->downloadable->name_en ?? '', ['class'=>'form-control input-rounded' ]) }}
			</div>
			<div class="col-6"> 
				{{ Form::label('attachement_name_ar' , 'Attachement Name AR:')}} 
				{{ Form::text('attachement_name_ar' , $course->downloadable->name_ar ?? '', ['class'=>'form-control input-rounded']) }}
			</div>
			<div class="col-6"> 
				{{ Form::label('attachement_file_en' , 'File EN:') }}
				{{ Form::file('attachement_file_en',['class' => 'form-control', 'accept'=>'.zip'])}}
			</div>
			<div class="col-6"> 
				{{ Form::label('attachement_file_ar' , 'File AR:') }}
				{{ Form::file('attachement_file_ar',['class' => 'form-control', 'accept'=>'.zip'])}}
			</div>
		</div>
	</div>
	<div class="col-12 card">
		<h3>Slug English Text</h3>
		<textarea class="editor" name="editor" id="editor">
			{{ $course->text_en }}
		</textarea>
		<br><br>
	</div>
	<div class="col-12 card">
		<h3>Slug Arabic Text</h3>
		<textarea class="editor" name="editor_ar" id="editorar" >
			{{ $course->text_ar }} 
		</textarea>
	</div>
	<input id = "fill-it" type="text" hidden="hidden" name="text">
	<div class="col-12 card">
		{{ Form::submit('Update Changes' , ['class'=>'btn btn-success '])}}
		<a href="{{route('courses-admin')}}" class="btn btn-default " style="margin-top: 10px;border: 1px solid #26dad2;"> Cancel</a>
	</div>

	{!! Form::close() !!}
@endsection


@section ('scripts')
<script>
			CKEDITOR.replace('editor' , {
				filebrowserBrowseUrl: '/images/',
				filebrowserUploadUrl: '/upload?command=QuickUpload&type=Files',
				allowedContent : true	
			});
			CKEDITOR.replace('editorar' , {
				filebrowserBrowseUrl: '/images/',
				filebrowserUploadUrl: '/upload?command=QuickUpload&type=Files',
				allowedContent : true	
			});
		</script>
		<script>
		counter = 0;
	{!!$course->is_youtube == 1 ? "counter = 1;toggle1();" : ''!!}

	$('#is_youtube').click(function (){
		if (counter  % 2 == 0){
			toggle1();
			counter ++;
		}
		else {
			toggle2();
			counter ++;
		}
	});

	function toggle1(){
			$('#link').removeClass('d-none');
			//$('#upload-video').addClass('d-none');
	}

	function toggle2(){
		$('#link').addClass('d-none');
		//$('#upload-video').removeClass('d-none');	
	}
</script>
@endsection 