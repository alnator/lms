@extends('admin.adminmaster')

@section ('title','Create Quiz')

@section ('styles')


    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
			

	<div class="col-12 card" >
			<input  hidden type="text" name="id" value="{{$quiz->id}}" id="quiz_id">

			<input  hidden type="text" name="type" id="type" value="{{$quiz->type}}">


			<div class="row">
				<div class="col-12">
					<h3> Create a quiz </h3>
				</div>
				<div class="col-6">
					<br>
					<label>Course :</label><br>
					<input type="text" class="form-control input-rounded" name="course_id" placeholder="{{$course->title_en}}" id = "course_id" disabled>
					<br>
					<label>Video :</label><br>
					<input type="text" class="form-control input-rounded" name="video_id" placeholder="{{$video->name_en}}" data-value="{{$video->id}}" id="video_id" disabled>
					
				</div>
				<div class="col-6">
					<br>
					<label for="quiz_statement">Quiz Statement (EN): </label><br>
					<textarea  name="quiz_statement" class="form-control" id="quiz_statement" style="width: 95%;height: 65%">{!! $quiz->quiz_statement !!}
					 </textarea>
					<br>
					<label for="quiz_statement">Quiz Statement (AR): </label><br>
					<textarea  name="quiz_statement_ar" class="form-control" id="quiz_statement_ar" style="width: 95%;height: 65%">{!! $quiz->quiz_statement_ar !!}
					 </textarea>
						
					
				</div>
			
			<div class="col-12" style="margin-top: 210px;">
				<button class="btn btn-success btn-block" onclick="saveQuiz();">Save Quiz</button>
			</div>
		</div>
	</div>

	<div class="col-12 card">
				
			<div class="row" id="choices">
				<div class="col-12">
					<h3> Quiz choices </h3>
				</div>
				@foreach ($choices as $choice)
				<div class="col-5 card" style="background-color: lavenderblush;margin-left: 5.5%" >
					<label>Choice Statement: </label>
					<input type="text"  class="choice_id" name="choice_id"  value="{{$choice->id}}" hidden>
					<input class="statement form-control input-rounded" value="{{$choice->choice_statement}}" type="text" name="choice_statement">
					<br>
					<label>Choice Statement: </label>
					<input class="statement_ar form-control input-rounded" value="{{$choice->choice_statement_ar}}" type="text" name="choice_statement_ar">
					
					<br>
					<label>Correct: </label>
					<input class="correct" class="form-control" type="checkbox" name="correct" {{$choice->correct == 1 ? 'checked' : ''}}>
					<br>
					<button class="btn btn-default btn-block edit" onclick="saveMe(this);">Edit</button>
					<button class="btn btn-danger btn-block done" onclick="deleteChoice(this)">Detete </button>
				</div>
				@endforeach
				<div class="col-5 card" style="background-color: lavenderblush;margin-left: 5.5%" >
					<label>Choice Statement (EN): </label>
					<input type="text"  name="choice_id" class="choice_id" value="0" hidden>
					<input class="statement form-control input-rounded" type="text" name="choice_statement">
					<br>
					<label>Choice Statement (AR): </label>
					<input class="statement_ar form-control input-rounded" type="text" name="choice_statement_ar">
					<br>
					<label>Correct: </label>
					<input class="correct" class="form-control" type="checkbox" name="correct">
					<br>
					<button class="btn btn-default btn-block edit" onclick="saveMe(this);">Add</button>

				</div>
			</div>
	</div>


@endsection


@section ('scripts')
    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>

<script type="text/javascript">
	function saveMe(obj){
		var siteurl = "<?php echo config('app.url'); ?>";
		statement = $(obj).parent().find('.statement').val();
		statement_ar = $(obj).parent().find('.statement_ar').val();
		correct = $(obj).parent().find('.correct')[0].checked;

		quiz = $('#quiz_id').val();
		choiceId = $(obj).parent().find('.choice_id').val();
		if (quiz == null  || quiz === undefined || quiz == ""){
			swal({
				type: 'error',
				title: `Can't Save Choice`,
				text: 'You Should Save Quiz First',
			});
			return ; 
		}
		else {
			// return;
			$.ajax({
			    type: "POST",
		        url: siteurl+`/api/admin/quiz/choice/create`,
		        data: {'statement' : statement , 'statement_ar' : statement_ar , 'correct' : correct , 'quiz_id' : quiz , 'choice_id':choiceId},
		        headers: {
		            "x-csrf-token": $("[name=_token]").val()
		        },
			}).done(response=> {
				$(obj).parent().find('.choice_id').attr('value' , response);
				$(obj).parent().find('.choice_id').val(response);

				$(obj).parent().find('.edit').text('Edit');
				if ($(obj).parent().find('.done').length == 0 ){
						$(obj).parent().append(`<button class="btn btn-danger btn-block done" onclick="deleteChoice(this)">Detete </button>`);
					$('#choices').append(`<div class="col-5 card" style="background-color: lavenderblush;margin-left: 5.5%" >
					<label>Choice Statement (EN): </label>
					<input type="text" class="choice_id" name="choice_id"  value="0" hidden>
					<input class="statement form-control input-rounded" type="text" name="choice_statement">
					<br>
					<label>Choice Statement (EN): </label>
					<input class="statement_ar form-control input-rounded" type="text" name="choice_statement_ar">
					
					<br>
					<label>Correct: </label>
					<input class="correct" class="form-control" type="checkbox" name="correct">
					<br>
					<button class="btn btn-default btn-block edit" onclick="saveMe(this);">Add</button>
				</div>`);
				}
				swal('Choice Saved Successfully!','This Quiz Choice has been saved successfully','success');
				
			});	
		}
	}
	function saveQuiz(){
		var siteurl = "<?php echo config('app.url'); ?>";
		statement = $('#quiz_statement').val();
		statement_ar = $('#quiz_statement_ar').val();
		videoId = $('#video_id').data('value');  
		quizId = $('#quiz_id').val();
		type = $('#type').val();
		$.ajax({
		    type: "POST",
	        url: siteurl+`/api/admin/quiz/create`,
	        data: {'statement' : statement ,'statement_ar' : statement_ar, 'video_id' : videoId , 'quiz_id' : quizId, 'type' : type },
	        headers: {
	            "x-csrf-token": $("[name=_token]").val()
	        },
		}).done(response=> {
			$('#quiz_id').val(response);
			swal('Choice Saved Successfully!','This Quiz Choice has been saved successfully','success');
		});
	}
	function deleteChoice(obj){
		var siteurl = "<?php echo config('app.url'); ?>";
		id = $(obj).parent().find('.choice_id').val();

		if (id == null || id == undefined || id == "" ){
			swal({
				type: 'error',
				title: `Oops...`,
				text: 'Something went wrong, Refresh the page and try again',
			});
		}
		else {
			$.ajax({
			    type: "POST",
		        url: siteurl+`/api/admin/choice/destroy`,
		        data: {'id' : id },
		        headers: {
		            "x-csrf-token": $("[name=_token]").val()
		        },
			}).done(response=> {
				swal('Choice Deleted Successfully!','This Quiz Choice has been deleted successfully','success');
				$(obj).parent().remove();
			});
		}
	}
</script>
@endsection 