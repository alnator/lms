@extends('admin.adminmaster')

@section ('title','Sessions')

@section ('content')
	<div class="col-12 card" >
{!! Form::open(['route'=>'sessions-create-api']) !!}

		<div class="row">
			<div class="col-6">
				{{ Form::label('session_number' , 'Session Name (EN):')}}
				{{ Form::text('session_number' , null , ['class' => 'form-control input-rounded'])}}
			</div>
			<div class="col-6">
				{{ Form::label('session_number_ar' , 'Session Name (AR):')}}
				{{ Form::text('session_number_ar' , null , ['class' => 'form-control input-rounded'])}}
			</div>
			<div class="col-6">
				{{ Form::label('order' , 'Order:')}}
				{{ Form::text('order' , null , ['class' => 'form-control input-rounded'])}}
			</div>
		</div>
		<div class="col-12" style="margin-top: 20px">
			{{ Form::label('course_id', 'Course:')}}
			<select class="input-rounded custom-select form-control"  name="course_id">
			@foreach($courses as $course)
				<option value="{{$course->id}}" >{{$course->title_en}} </option>
			@endforeach
			</select>
		</div>
	
	</div>
	<div class="col-12 card">
		{{ Form::submit('Submit Session', ['class' => 'btn btn-success btn-block'])}}

		<a href="{{route('sessions-admin')}}" class="btn btn-default btn-block" style="margin-top:10px;">Cancel</a>
	</div>		
{!! Form::close() !!}
@endsection

@section ('scripts')

@endsection 