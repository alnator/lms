@extends('admin.adminmaster')

@section ('title','Sessions')
@section ('styles')
    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    	.popup{
    		position: absolute;
    		left: 25%;
    		top: 25%;
    	}
    	.table{
    		height: 3em;
    	}
    	thead tr td{
    		text-align: center;
    		border-bottom :1.5px solid #A9A9A9;
    	}
    	tbody tr td {
			text-align: center;
    		border-top :1.5px solid #A9A9A9;
    		
    	}
    	tbody tr td:last-child {
    		text-align: center;
    	}
    	.display {
    		display: flex !important;
    	}
    	.margins {
    		margin-bottom: 4em ;
    	}
    	</style>
@endsection

@section ('content')
	<div class="col-12 card" id="session-grid">
		
	</div>
	<div class="col-12 card">
		<a href="{{route('session-create')}}" class="btn btn-success btn-block">Add Session</a>
	</div>

	<div class="col-8 card popup" id="display" style="display: none">
		<table class="margins" id="dataTable">
			<thead>
			<tr class="table">
				<td>ID</td>
				<td>Video Name</td>
				<td>Estimated Time</td>
				<td>Order</td>
			</tr>
			</thead>
		</table>
		<button class="btn btn-block btn-success" onclick="$('#display').removeClass('display');">OK</button>
	</div>
@endsection


@section ('scripts')
	<script type="text/javascript">var siteurl= " <?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/session-grid.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
    
@endsection 