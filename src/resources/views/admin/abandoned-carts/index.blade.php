@extends('admin.adminmaster')

@section ('title','Abandoned Carts')
@section ('styles')



    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
	<div class="col-12 card" id="abandoned-carts-grid">
		
	</div>
	<div class="col-12 card">
		<a href="{{route('abandoned-carts-create')}}" class="btn btn-success btn-block">Add Abandoned Cart</a>
	</div>
@endsection


@section ('scripts')
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/abandoned-carts-grid.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>

@endsection 