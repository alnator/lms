@extends('admin.adminmaster')

@section ('title','Abandoned Carts')

@section ('content')
	<div class="col-12 card" >
		{!! Form::open(['route'=>'abandoned-carts-create-api']) !!}

		<div class="row">

			<div class="col-6">
					{{ Form::label('user_id', 'User:')}}
					<select class="input-rounded custom-select form-control"  name="user_id">
					@foreach($users as $user)
						<option value="{{$user->id}}" >{{$user->name}} </option>
					@endforeach
					</select>
					<br><br>
					{{ Form::label('subscription_model_id', 'Subscription Model:')}}
					<select class="input-rounded custom-select form-control"  name="subscription_model_id">
					@foreach($subscriptions as $subscription)
						<option value="{{$subscription->id}}" >{{$subscription->name_en}} </option>
					@endforeach
					</select>
					
			</div>
			<div class="col-6">

					{{ Form::label('course_id', 'Level:')}}
					<select class="input-rounded custom-select form-control"  name="course_id">
					@foreach($courses as $course)
						<option value="{{$course->id}}" >{{$course->title_en}} </option>
					@endforeach
					</select>
					<br><br>
					{{ Form::label('payment_method_id', 'Payment Methods:')}}
					<select class="input-rounded custom-select form-control"  name="payment_method_id">
					@foreach($methods as $method)
						<option value="{{$method->id}}" >{{$method->name}} </option>
					@endforeach
					</select>
			</div>
			<div class="col-12">
				<br>
					{{ Form::label('coupon', 'Coupon:')}}
					{{ Form::text('coupon' ,null , ['class'=>'form-control input-rounded'])}}
			</div>
		</div>
</div>
	<div class="col-12 card">
		{{ Form::submit('Submit Abandoned Cart', ['class' => 'btn btn-success btn-block'])}}

		<a href="{{route('abandoned-carts-admin')}}" class="btn btn-default btn-block" style="margin-top:10px;">Cancel</a>
	</div>		
{!! Form::close() !!}
@endsection

@section ('scripts')

@endsection 