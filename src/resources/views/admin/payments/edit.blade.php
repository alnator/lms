@extends('admin.adminmaster')

@section ('title','Payments')

@section ('content')
			

	<div class="col-12 card" >
		{!! Form::model($payment,['route' => ['payment-edit-api',$payment->id] ]) !!}	
			<div class="row">
				<div class="col-12">
					<h3> Main Details </h3>
				</div>
				<div class="col-6"> 
					{{ Form::label('user_id', 'User:')}}
					<select class="input-rounded custom-select form-control"  name="user_id">
					@foreach($users as $user)
						<option value="{{$user->id}}" >{{$user->name}} </option>
					@endforeach
					</select>
					<br><br>
					{{ Form::label('amount', 'Amount:')}}
					{{ Form::number('amount' , null , ['class'=>'form-control input-rounded']) }}

				</div>
				<div class="col-6">
					{{ Form::label('invoice_id', 'Invoiced ID:')}}
					{{ Form::number('invoice_id' , null , ['class'=>'form-control input-rounded']) }}				
					<br><br>
					{{ Form::label('coupon', 'Coupon:')}}
					{{ Form::text('coupon' , null , ['class'=>'form-control input-rounded']) }}

				</div>
			 </div>
	</div>

	<div class="col-12 card">
		{{ Form::submit('Update Payment' , ['class'=>'btn btn-success '])}}
		<a href="{{route('subscription-admin')}}" class="btn btn-default " style="margin-top: 10px;border: 1px solid #26dad2;"> Cancel</a>
	</div>

	{!! Form::close() !!}
@endsection


@section ('scripts')

@endsection 