@extends('admin.adminmaster')

@section ('title','Payments')
@section ('styles')
    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">
@endsection

@section ('content')
<input type="hidden" id="filter-data">
	<div class="row">
		<div class="col-12">
			<button class="btn btn-default" id="pdf-export-btn" style="float: right; margin-left: 10px">Export To PDF</button>		
			<button class="btn btn-default" id="excel-export-btn" style="float: right; margin-left: 10px">Export To Excel</button>	
			<button class="btn btn-default" id="csv-export-btn" style="float: right">Export To CSV</button>	
		</div>
		<div class="col-12 card" id="payments-grid">
		</div>
	</div>
	<div class="col-12">
		<a href="{{ route('payments-create') }}" class="btn btn-success btn-block">Create Payment</a>
	</div>

	@endsection


@section ('scripts')
<script type="text/javascript">var siteurl= " <?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/payments-grid.js?v=1"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
@endsection 