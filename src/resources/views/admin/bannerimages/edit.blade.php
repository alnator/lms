@extends('admin.adminmaster')

@section ('title','Banner images')

@section ('content')
{!! Form::model($bannerimages,['route'=>['bannerimages-edit-api', $bannerimages->id] , 'files'=>true ]) !!}
	<div class="col-12 card" >
		
		
			<div class="row">
				<div class="col-12">
					<div class="row">
						<!-- <div class="col-6">
							{{ Form::label('image_path_ar', 'Image (AR):')}}
							{{ Form::file('image_path_ar' , ['class'=>'form-control'])}}
						</div> -->
						<div class="col-6">
							{{ Form::label('image_path', 'Image:')}}
							{{ Form::file('image_path' , ['class'=>'form-control'])}}	
						</div>
						<div class="col-6">
							<img src="<?php echo config('app.url').'/public'.$bannerimages->image_path; ?>" height="100px">
						</div>
					</div>
				</div>
			</div>

			<div class="row">
			<div class="col-6">
				{{ Form::label('display_area' , 'Display Area')}}
				{{ Form::text('display_area' , null , ['class' => 'form-control input-rounded','readonly'=>'readonly'])}}
				
				
			</div>
			<div class="col-6">
				{{ Form::label('display_size' , 'Display Size')}}
				{{ Form::text('display_size' , null , ['class' => 'form-control input-rounded','readonly'=>'readonly'])}}
				
			</div>
		</div>
		
	
	</div>
	<div class="col-12 card">
		{{ Form::submit('Update Level', ['class' => 'btn btn-success btn-block'])}}

		<a href="{{route('banner-images')}}" class="btn btn-default btn-block" style="margin-top:10px;border: 1px solid #26dad2;">Cancel</a>
	</div>		
{!! Form::close() !!}
@endsection

@section ('scripts')

@endsection 