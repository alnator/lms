@extends('admin.adminmaster')

@section ('title','Email Subscription')
@section ('styles')

    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">
    <style>
    .display 
    {
   		display: flex !important;
    }
    .popup 
    {
		position: absolute;
		left: 20%;
		top: 15%;
		z-index: 20;
	}
	.btn-success
	{
		margin-top: 20px;
	}
	footer {
		display: none;
	}
	.date {
		    margin-bottom: -19px;
    		text-align: right;
	}
    </style>
@endsection

@section ('content')
	<div class="col-12 card" id="emails-grid">
		
	</div>
	<div class="col-12">
		<button style="float: right" class="btn btn-primary" id="create-btn">Create Email</button>
	</div>



	<div class="modal fade" id="edit-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Email Subscription</h5>
	        <button type="button" class="close" onclick="closeModal()" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
			<form action="" id="submit-link" method="POST">
				<input type="hidden" name="id" id="email-id">
	        	<div class="row">
	        		<div class="col-md-6 form-group">
	        			<label>From: </label>
	        			<input class="form-control" type="email" id="email-from" name="from">
	        		</div>
	        		<div class="col-md-6 form-group">
	        			<label>Subject: </label>
	        			<input class="form-control" type="text" id="email-subject" name="subject">
	        		</div>
	        	</div>
	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeModal()">Close</button>
	        <button type="submit" form="submit-link" id="submit-btn" class="btn btn-primary"></button>
	      </div>
	    </div>
	  </div>
	</div>
@endsection


@section ('scripts')
<script type="text/javascript"> var siteurl = "<?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/email-subscription-grid.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript">
    	// $('#edit-create-modal').modal({
		    // backdrop: 'static',
		    // keyboard: false
		// });
    	$('#create-btn').click(function(){
		  	$('#email-id').val('');
		  	$('#email-from').val('');
		  	$('#email-subject').val('');
	  		$('#submit-btn').html("Create");
		  	$('#submit-link').attr('action', siteurl+'/admin/emails/create');
			$('#edit-create-modal').modal('show');
    		$('.modal-backdrop').css('z-index' , '1000');
    		$('.modal-backdrop').addClass('show');
    		$('body').addClass('modal-open');
    		$('#edit-create-modal').addClass('show');
    		$('#edit-create-modal').css('display' , 'block');
    	});
    	function closeModal(){
    		$('body').removeClass('modal-open');
    		$('.modal-backdrop').removeClass('show');
    		$('.modal-backdrop').css('z-index' , '-1000');
    		$('#edit-create-modal').removeClass('show');
    		$('#edit-create-modal').css('display' , 'none');
			$('#edit-create-modal').modal('hide');
		}
    </script>
@endsection 



	        