@extends('admin.adminmaster')

@section ('title','Email Subscription')
@section ('styles')
    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" />
    <style>
    .display 
    {
   		display: flex !important;
    }
    .popup 
    {
		position: absolute;
		left: 20%;
		top: 15%;
		z-index: 20;
	}
	.btn {
		margin: 0px 10px;
	}
	footer {
		display: none;
	}
	.date {
		    margin-bottom: -19px;
    		text-align: right;
	}
    </style>
@endsection

@section ('content')

	<div class="col-12 card" id="emails-grid">
		<div class="card-body">
			{!! Form::open(['route' => 'admin-send-subs']) !!}
			<input type="hidden" name="subs_id" value="{{$emailSubscription->id}}">
			<div class="row">
				<div class="col-md-6 form-group">
					<label>
						Send to:
					</label>
					<br>
					<select class="js-example-basic-multiple form-control" name="users[]" multiple="multiple">
						@foreach($users as $user)
							<option value="{{ $user->email }}">
								{{ $user->email }}
							</option>
						@endforeach 
					</select>
				</div>
				<div class="col-md-6 form-group">
					<label>
						Content:
					</label>
					<br>
					<textarea class="form-control" name='content' style="height: 100px"></textarea>
				</div>
				<div class="col-md-12 justify-content-center">
					<button type="submit" class="btn btn-success" style="float: right;width: 6rem"> Send </button>
					<a href="{{route('admin-email-subscription')}}">
						<button type="button" class="btn btn-primary" style="float: right;width: 6rem"> Cancel </button>
					</a>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>

@endsection

@section ('scripts')
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript">
    	$(document).ready(function() {
		    $('.js-example-basic-multiple').select2();
		});
    </script>
@endsection 