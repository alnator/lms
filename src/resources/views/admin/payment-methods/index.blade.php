@extends('admin.adminmaster')

@section ('title','Payment Methods')
@section ('styles')



    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
	<div class="col-12 card" id="payments-grid">
		
	</div>

<!-- 	<div class="col-12 card">
		<a href="{{route('payment-methods-create')}}" class="btn btn-success btn-block">Create Payment Method</a>
	</div>
 -->

@endsection


@section ('scripts')
<script type="text/javascript">var siteurl= " <?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/payment-methods-grid.js?v=1"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
    
@endsection 