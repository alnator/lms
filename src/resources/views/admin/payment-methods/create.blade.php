@extends('admin.adminmaster')

@section ('title','Payment Methods')

@section ('content')


	<div class="col-12 card" >
{!! Form::open(['route'=>'payment-method-create-api']) !!}
		<div class="row">
			
			<div class="col-4">
				{{ Form::label('name', 'Name:')}}
				{{ Form::text('name' , null , ['class'=>'form-control input-rounded'])}}
			</div>
			

			<div class="col-4">
				{{ Form::label('percent_fees', 'Percentage Fees:')}}
				{{ Form::number('percent_fees' , null , ['class'=>'form-control input-rounded'])}}
			</div>
			

			<div class="col-4">
				{{ Form::label('fixed_fees', 'Fixed Fees:')}}
				{{ Form::number('fixed_fees' , null , ['class'=>'form-control input-rounded'])}}	
			</div>

		</div>
	
	</div>
	<div class="col-12 card">
		{{ Form::submit('Submit Method', ['class' => 'btn btn-success btn-block'])}}

		<a href="{{route('payment-methods-admin')}}" class="btn btn-default btn-block" style="margin-top:10px;">Cancel</a>
	</div>		
{!! Form::close() !!}
@endsection

@section ('scripts')

@endsection 