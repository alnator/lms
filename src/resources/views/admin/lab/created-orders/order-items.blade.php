@extends('admin.adminmaster')

@section ('title','Order Items')
@section ('styles')

    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    	.popup{
    		position: absolute;
    		left: 25%;
    		top: 25%;
    	}
    	.display {
    		display: flex !important;
    	}
    </style>
@endsection

@section ('content')
	<div class="col-12 card" id="order-items-grid">
		
	</div>
	<div class="col-12 card">
		{!! Form::open(['route' => ['confirm-order', $orderItems[0]->order_id] ] ) !!}
		<button class="btn btn-success btn-block">Confirm Order</button>
		{!! Form::close() !!}
	</div>
	<div class="col-8 card popup" id="display" style="display: none">
			@foreach($orderItems as $item)
			<input type="text" name="ids[]" hidden value="{{$item->id}}">
			<input type="text" name="item_{{$item->id}}" hidden>
			<div class="row">
				<div class="col-4">
					<label>{{$item->item->name_en}}</label>
				</div>
				<div class="col-8">
					<select class="input-rounded custom-select form-control"  name="suplier_item_{{$item->id}}">
					@foreach($item->supplierItem as $supItem)
						<option value="{{$supItem->id}}" >{{$supItem->user->email}} => {{$supItem->price}} $</option>
					@endforeach
					</select>
				</div>
				<div class="col-12"><br></div>
			</div>
			@endforeach
			<hr>
		<button class="btn btn-block btn-success" >Submit</button>
	</div>
@endsection


@section ('scripts')
<script type="text/javascript">var siteurl = "<?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/order-items-grid.js"></script>
    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript">
    	$('#confirm').click(function (){
    		$('#display').addClass('display');
    	});
    </script>
@endsection 