@extends('admin.adminmaster')

@section ('title','Kits')

@section ('content')
{!! Form::open(['route'=>'items-create-api' , 'files'=>true]) !!}
	<div class="col-12 card" >
		<div class="row">
			<div class="col-6">
				{{ Form::label('name_ar' , 'Name (AR): ')}}
				{{ Form::text('name_ar', null , ['class'=> 'form-control input-rounded'])}}
			</div>
			<div class="col-6">
				{{ Form::label('name_en' , 'Name (EN): ')}}
				{{ Form::text('name_en' , null , ['class'=> 'form-control input-rounded'])}}
			</div>
			<div class="col-12">
				{{ Form::label('description' , 'Description: ')}}
				{{ Form::textarea('description' , null , ['class' => 'form-control input-rounded'])}}
			</div>
			<div class="col-12 row" id="images">
				<div class="col-3">
					{{ Form::label('images[]' , 'Image')}}<span style="float: right; color: red;cursor: pointer;" onclick="deleteIt(this);">x</span>
					{{ Form::file('images[]')}}
					
				</div>
				
			</div>
			<div class="col-4 offset-5">
				<br>
				<br>

				<span class="btn btn-default" onclick="addImage()">
					Add Image
				</span>
			</div>
		</div>	

	</div>
	<div class="col-12 card">
		{{ Form::submit('Submit Item', ['class' => 'btn btn-success btn-block'])}}

		<a href="{{route('items-admin')}}" class="btn btn-default btn-block" style="margin-top:10px;">Cancel</a>
	</div>		
{!! Form::close() !!}
@endsection

@section ('scripts')
	<script type="text/javascript">
		function addImage(){
			$('#images').append(`<div class="col-3">
					<label for="images[]">Image</label><span style="float: right; color: red;cursor: pointer;" onclick="deleteIt(this);">x</span>
					<input name="images[]" type="file" id="images[]">
					
				</div>`);	
		}

		function deleteIt(e){
			$(e).parent().remove();
		}
	</script>
@endsection 