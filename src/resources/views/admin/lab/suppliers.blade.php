@extends('admin.adminmaster')

@section ('title','Suppliers')
@section ('styles')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    	.popup{
    		position: absolute;
    		left: 25%;
    		top: 25%;
    	}
    	.display {
    		display: flex !important;
    	}
    	.modal-btn {
    		margin-top: 20px;
    	}
    	.select2-container{
    		width: 100% !important;
    	}
    </style>
@endsection

@section ('content')
	<div class="col-12 card" id="suppliers-grid">
		
	</div>
	<div class="col-12 card">
		<button class="btn btn-success btn-block" id="add-supp">Add Suppliers</button>
	</div>

	<div class="col-8 card popup row" id="display" style="display: none">
		{!! Form::open(['route'=>'add-supplier-api']) !!}
		<div class="col-12">
			<h3>Add Suppliers: </h3>
			<br>
			<select class="form-control js-example-basic-multiple" multiple name="users[]" id="income-month-filter">		
			@foreach($users as $user)
				<option value="{{$user->id}}">
					{{$user->email}} 
				</option>
			@endforeach
		</select>
		</div>
		<button class="btn btn-success btn-block modal-btn"> Submit Suppliers</button>
		<a class="btn btn-default btn-block" id="cancel">Cancel</a>
		{!! Form::close() !!}
	</div>
@endsection 


@section ('scripts')
<script type="text/javascript">var siteurl = "<?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/suppliers-grid.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript">
    	$('.js-example-basic-multiple').select2();
		$('#add-supp').click(function(){
    		$('#display').addClass('display');
		});
		$('#cancel').click(function(){
    		$('#display').removeClass('display');
		});
    </script>
@endsection 