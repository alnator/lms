@extends('admin.adminmaster')

@section ('title','Orders')
@section ('styles')

    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    	.btn {
    		margin-left: 10px;
    		margin-bottom: 10px;
    	}
    	.jsgrid-control-field {
    		padding: 4px 4px;
    	}
    </style>
@endsection

@section ('content')
	<input type="hidden" id="pdf-link" value="{{route('all-orders-pdf')}}">
	<input type="hidden" id="filter" >
	<input type="hidden" id="excel-link" value="{{route('all-orders-excel')}}">
	
	<div class="col-12" >
		<button class="btn btn-default"  id="pdf-btn" style="float: right;">Export To PDF</button>
		<button class="btn btn-default"  id="excel-btn" style="float: right;">Export To Excel</button>
		<button class="btn btn-default"  id="csv-btn" style="float: right;">Export To CSV</button>
	</div>
	<div class="col-12 card" id="orders-grid">
		
	</div>
@endsection


@section ('scripts')
<script type="text/javascript">var siteurl = "<?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/all-orders-grid.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
    
    <script type="text/javascript">
    	$('#pdf-btn').click(function (){
    		filter = $('#filter').val();
    		link = $('#pdf-link').val();
    		link += `?filter=${filter}`;
    		window.location.href = link;
    	});
    	$('#excel-btn').click(function (){
    		filter = $('#filter').val();
    		link = $('#excel-link').val();
    		link += `?filter=${filter}&type=xlsx`;
    		window.location.href = link;
    	});
    	$('#csv-btn').click(function (){
    		filter = $('#filter').val();
    		link = $('#excel-link').val();
    		link += `?filter=${filter}&type=csv`;
    		window.location.href = link;
    	});
    </script>
@endsection 