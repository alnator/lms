@extends('admin.adminmaster')

@section ('title','Orders')
@section ('styles')

    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
    <div class="col-12 card">
        
        <div class="row" id="DivIdToPrint">
            <table width="100%" >
                <tr>
                    <td style="text-align: center;" >
                        <img src="<?php echo config('app.url'); ?>/css/client/images/logo.png" style="height: 60px;" alt="">
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;">
                        <strong>Order Invoice</strong>
                    </td>
                </tr>
                
                <tr>
                    <td >
                        <table width="100%" style="border-collapse: collapse;border: 1px solid black;">
                        	<tr>
                        		<td style="text-align: left;padding-left: 16px;width: 70%;border: 1px solid black;"><strong>Name : </strong>{{$order->user->name}}</td>
                        		<td style="text-align: left;border: 1px solid black;">Invoice ID : </td>
                        	</tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <table width="100%" style="border-collapse: collapse;border: 1px solid black;">
                        	<tr>
                        		<td style="text-align: left;padding-left: 16px;width: 25%;border-right: 1px solid black;"><strong>Address : </strong></td>
                        		<td style="text-align: left;"> 
                        		{{$order->line1}}, 
                        		@if($order->line2 != "")
                        			{{$order->line2}},
                        		@endif
                        		@if($order->line3 != "")
                        			{{$order->line3}},
                        		@endif
                        		{{$order->city}},
                        		{{$order->user->country}}
                        	</td>
                        	</tr>
                        	<tr>
                        		<td style="text-align: left;padding-left: 16px;width: 25%;border-right: 1px solid black;"><strong>Mobile Number : </strong></td>
                        		<td style="text-align: left;"> 
                        			{{$order->phone}} 
                        			@if($order->phone_number != "")
                        		 	/ {{$order->phone_number}}
                        		 	@endif
                        		</td>
                        	</tr>
                        	<tr>
                        		<td style="text-align: left;padding-left: 16px;width: 25%;border-right: 1px solid black;"><strong>Email : </strong></td>
                        		<td style="text-align: left;">{{$order->user->email}}</td>
                        	</tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td >
                        <table width="100%" style="border-collapse: collapse;border: 1px solid black;">
                        	<tr>
                        		<td style="text-align: center;width: 25%;border: 1px solid black;">
                        			<strong>Product Name</strong>
                        		</td>
                        		<td style="text-align: center;width: 25%;border: 1px solid black;">
                        			<strong>Quantity</strong>
                        		</td>
                        		<td style="text-align: center;width: 25%;border: 1px solid black;">
                        			<strong>Tax</strong>
                        		</td>
                        		<td style="text-align: center;width: 25%;border: 1px solid black;">
                        			<strong>Item Price</strong>
                        		</td>

                        	</tr>
                        	@foreach($orderItems as $items)
						    <tr>
						      <td style="text-align: center;width: 25%;border: 1px solid black;">{{$items->itemname}}</td>
						      
						      <td style="text-align: center;width: 25%;border: 1px solid black;">{{$items->qty}}</td>
						      <td style="text-align: center;width: 25%;border: 1px solid black;">0%</td>
						      <td style="text-align: center;width: 25%;border: 1px solid black;">{{$items->final_total}}</td>
						     
						    </tr>
						    @endforeach
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;">
                        <strong>Billing Details</strong>
                    </td>
                </tr>
                <tr>
                    <td >
                        <table width="100%" style="border-collapse: collapse;border: 1px solid black;">
                        	<tr>
                        		<td style="text-align: left;padding-left: 16px;width: 50%;border-right: 1px solid black;"><strong>Order ID : </strong></td>
                        		<td style="text-align: left;border-bottom: 1px solid black;"> 
                        		{{$order->id}}
                        		</td>
                        	</tr>
                        	<tr>
                        		<td style="text-align: left;padding-left: 16px;width: 50%;border-right: 1px solid black;"><strong>Payment Method : </strong></td>
                        		<td style="text-align: left;border-bottom: 1px solid black;"> 
                        		{{$order->payment_method}}
                        		</td>
                        	</tr>
                        	<tr>
                        		<td style="text-align: left;padding-left: 16px;width: 50%;border-right: 1px solid black;"><strong>Payment Status : </strong></td>
                        		<td style="text-align: left;border-bottom: 1px solid black;"></td>
                        	</tr>
                        	<tr>
                        		<td style="text-align: left;padding-left: 16px;width: 50%;border-right: 1px solid black;"><strong>Total Items Price : </strong></td>
                        		<td style="text-align: left;border-bottom: 1px solid black;">{{$order->total_price}}</td>
                        	</tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;">
                        <strong>Order Invoice</strong>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;">
                        <strong>(Tax is included in product price)</strong>
                    </td>
                </tr>
                <tr>
                    <td >
                        <table width="100%" style="border-collapse: collapse;border: 1px solid black;">
                        	
                        	<tr>
                        		<td style="text-align: left;padding-left: 16px;width: 50%;border-right: 1px solid black;"><strong>Total Items Price : </strong></td>
                        		<td style="text-align: left;border-bottom: 1px solid black;">{{$order->total_price}}</td>
                        	</tr>
                        	<tr>
                        		<td style="text-align: left;padding-left: 16px;width: 50%;border-right: 1px solid black;"><strong>Shipping & Handling : </strong></td>
                        		<td style="text-align: left;border-bottom: 1px solid black;"> 
                        		0
                        		</td>
                        	</tr>
                        	<tr>
                        		<td style="text-align: left;padding-left: 16px;width: 50%;border: 1px solid black;"><strong>Grand Total (Including Tax) : </strong></td>
                        		<td style="text-align: left;border-bottom: 1px solid black;"> 
                        		{{$order->total_price}}
                        		</td>
                        	</tr>
                        	

                        </table>
                    </td>
                </tr>
            </table>
            
        </div>
        <div class="row">
        	<div class="col-12 text-center">
                <a href="<?php echo config('app.url'); ?>/admin/all-orders" class="btn btn-default" >Cancel</a>
           	<input type='button' class="btn btn-primary" id='btn' value='Print' onclick='printDiv();'>
           </div>
        </div>
     </div>
@endsection


@section ('scripts')
 <script>
 	function printDiv() 
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
 </script>   
    
@endsection 