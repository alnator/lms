@extends('admin.adminmaster')

@section ('title','Kits')

@section ('styles')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section ('content')
	{!! Form::open(['route'=> [ 'kits-add-api', $id]  , 'files'=>true]) !!}
		<div class="col-12 card" >
			<div class="row">
				<div class="col-12">
					{{ Form::label('items' , 'Add Items: ')}}
					<select class="form-control js-example-basic-multiple" multiple name="items[]" id="income-month-filter">
						@foreach($items as $item)
							<option value="{{$item->id}}">
								{{$item->name_en}} 
							</option>
						@endforeach
					</select>		
				</div>
			</div>	
		</div>
		<div class="col-12 card">
			{{ Form::submit('Submit Items', ['class' => 'btn btn-success btn-block'])}}
			<a href="{{ route('kit-items' , $id) }}" class="btn btn-default btn-block" style="margin-top:10px;">Cancel</a>
		</div>		
	{!! Form::close() !!}
@endsection

@section ('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

	<script type="text/javascript">
		function addImage(){
			$('#images').append(`<div class="col-3">
					<label for="images[]">Image</label><span style="float: right; color: red;cursor: pointer;" onclick="deleteIt(this);">x</span>
					<input name="images[]" type="file" id="images[]">
				</div>`);	
		}

		function deleteIt(e){
			$(e).parent().remove();
		}
    $('.js-example-basic-multiple').select2();

	</script>
@endsection 