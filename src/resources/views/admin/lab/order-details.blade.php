@extends('admin.adminmaster')

@section ('title','Orders')
@section ('styles')

    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
	<div class="col-12 card">
		
		<div class="row">
			<div class="col-12"><h4>Order Detail</h4><hr></div>
			<div class="col-12"><h4>Shipping Information</h4><hr></div>

			<div class="col-6"><label>Name : </label>&nbsp;&nbsp;<strong>{{$order->user->name}}</strong></div>
			<div class="col-6"><label>Email : </label>&nbsp;&nbsp;<strong>{{$order->user->email}}</strong></div>
			<div class="col-6"><label>Phone : </label>&nbsp;&nbsp;<strong>{{$order->phone}} / {{$order->phone_number}}</strong></div>
			<div class="col-6"><label>Address : </label>&nbsp;&nbsp;<strong>{{$order->line1}}<br>{{$order->line2}}<br>{{$order->line3}}</strong></div>
			<div class="col-6"><label>Country : </label>&nbsp;&nbsp;<strong>{{$order->user->country}}</strong></div>
			<div class="col-6"><label>city : </label>&nbsp;&nbsp;<strong>{{$order->city}}</strong></div>
			<div class="col-12"><hr></div>
		</div>
		<div class="row">
			<div class="col-12"><h4>Order Items</h4><hr></div>
			<div class="col-12">
			@if(count($orderItems)>0)
				<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Item Name</th>
      <th scope="col">Type</th>
      <th scope="col">Supplier Email</th>
      <th scope="col">Price</th>
      <th scope="col">Qty</th>
      <th scope="col">Total Price</th>
      
    </tr>
  </thead>
  <tbody>
  	<?php $course = 0; ?>
  	@foreach($orderItems as $items)
      <?php 
      if($items->type == 'Kit' || $items->type == 'Item'){
        $course = 1;
      }
      ?>

    <tr>
      <th scope="row">{{$items->id}}</th>
      <td>{{$items->itemname}}</td>
      <td>{{$items->type}}</td>
      <td>@if(isset($items->supplier)) {{$items->supplier->email}} @else - @endif</td>
      <td>{{$items->price}}</td>
      <td>{{$items->qty}}</td>
      <td>{{$items->final_total}}</td>
     
    </tr>
    @endforeach
    <tr>
      <th scope="row">Total Price</th>
      <td colspan="6">{{$order->total_price}}</td>
    </tr>
  		</tbody>
	</table>
  <div class="col-12">
    @if($order->status == 'unpaid' || $order->status == 'pending')
    <form method="POST" action="{{ route('updateorder') }}">
          @csrf
          <input type="hidden" name="orderid" value="{{$order->id}}">
          <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6">
              <label>Status</label>
              <select name="status" class="form-control">
                <option value="paid" <?php if($order->status == 'paid' ){ echo "selected"; } ?>>Paid </option>
                <option value="unpaid" <?php if($order->status == 'unpaid' ){ echo "selected"; } ?>>Unpaid </option>
                <option value="pending" <?php if($order->status == 'pending' ){ echo "selected"; } ?>>Pending </option>
                <option value="complete" <?php if($order->status == 'complete' ){ echo "selected"; } ?>>Complete</option>
              </select>
            </div>
          </div>
          @if($course == 0 && $order->shipment_id != '' || $course == 1)
          <div class="row" style="margin-top: 10px;">
            <div class="col-xl-12 col-lg-12 col-md-12 text-center">
              <a href="<?php echo config('app.url'); ?>/admin/all-orders" class="btn btn-default" >Cancel</a>
              <button type="submit" name="update" class="btn btn-info">Update</button>
            </div>
          </div>
          
          @endif


    </form>
          @if($course == 0 && $order->shipment_id == '')

          <div class="row" style="margin-top: 10px;">
            <div class="col-xl-12 col-lg-12 col-md-12 text-center">
              <a href="<?php echo config('app.url'); ?>/admin/all-orders" class="btn btn-default" >Cancel</a>
              <button onclick="moneycollection('{{$order->id}}')" class="btn btn-info">Money Collection</button>
            </div>
          </div>
          @endif
    @endif
  </div>
  @if($order->status == 'paid')
	<div class="col-12 text-center" style="margin-top: 20px;">
		<a href="<?php echo config('app.url'); ?>/admin/all-orders" class="btn btn-default" >Cancel</a>
    
		<a href="<?php echo config('app.url'); ?>/admin/order-invoice/{{$order->id}}" class="btn btn-info">Print Invoice</a>
    
	</div>
  @endif
	@else
    <p href="template" class="text-center">Rocord Not Found</p>
    @endif
			</div>
		</div>

	</div>
@endsection


@section ('scripts')
	
  
<script type="text/javascript">
  function moneycollection(orderId){
    swal({
      title: "Money Collection to Aramex",
      text: "",
      icon: "warning",
      buttons: ["No", "Yes"],
      dangerMode: true,
    })
    .then((yes) => {
      if (yes) {
          $.ajax({
          url:"{{ url('moneycollection') }}",
          method:"POST",
          data:{ "_token": "{{ csrf_token() }}","orderId" : orderId },
          success:function(data) {
              swal(data);
                                window.location.reload();
               
              
          }
          });
      } else {
              swal({
              title: "Payment Received",
              text: "",
              icon: "warning",
              buttons: ["No", "Yes"],
              dangerMode: true,
              })
              .then((yes) => {
                if (yes) {
                    $.ajax({
                    url:"{{ url('paymentreceive') }}",
                    method:"POST",
                    data:{ "_token": "{{ csrf_token() }}","orderId" : orderId },
                    success:function(data) {
                        //alert(data)
                        window.location.reload();
                    }
                    });
                } else {
                  
                }
              });
      }
    });
  }
</script>  
@endsection 