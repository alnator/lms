@extends('admin.adminmaster')

@section ('title','Files')
@section ('styles')



    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
	<div class="col-12 card" id="downloadable-grid">
		
	</div>
	<div class="col-12 card">
		<a href="{{route('files-create')}}" class="btn btn-success btn-block">Upload File</a>
	</div>
@endsection


@section ('scripts')
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/downloadable-grid.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>

@endsection 