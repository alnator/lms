@extends('admin.adminmaster')

@section ('title','Files')

@section ('content')
			

	<div class="col-12 card" >
		{!! Form::open(['route' => 'files-create-api' , 'files' => 'true']) !!}	
			<div class="row">
				<div class="col-12">
					<h3> Main Details </h3>
				</div>
				<div class="col-6"> 
					{{ Form::label('name_en', 'Name (EN):')}}
					{{ Form::text('name_en',null,['class'=>'form-control input-rounded']) }}
					<br>
					{{ Form::label('file_path_ar' , 'File (AR):')}}
					{{ Form::file('file_path_ar'  , ['class'=>'form-control', 'required' => 'required' ])}}

				</div>
				<div class="col-6">
					{{ Form::label('name_ar', 'Name (AR):')}}
					{{ Form::text('name_ar',null,['class'=>'form-control input-rounded']) }}
					<br>
					{{ Form::label('file_path_en' , 'File (EN):')}}
					{{ Form::file('file_path_en' , ['class' => 'form-control ' , 'required' => 'required']) }}
				</div>
			 </div>
	</div>

	<div class="col-12 card">
		<div class="row">
			<div class="col-6">
				{{ Form::label('course_id' , 'Course:')}}
				<select class="input-rounded custom-select form-control" required="required" name="course_id">
				@foreach($courses as $course)
					<option value="{{$course->id}}">{{$course->title_en}}</option>
				@endforeach
				</select>
			</div>
			<div class="col-6">
				{{ Form::label('video_id' , 'Video:')}}
				<select class="input-rounded custom-select form-control" required="required" name="video_id">
				@foreach($videos as $video)
					<option value="{{$video->id}}">{{$video->name_en}}</option>
				@endforeach
				</select>
			</div> 
		</div>
	</div>



	<div class="col-12 card">
		{{ Form::submit('Submit File' , ['class'=>'btn btn-success '])}}
		<a href="{{route('files-admin')}}" class="btn btn-default " style="margin-top: 10px;border: 1px solid #26dad2;"> Cancel</a>
	</div>

	{!! Form::close() !!}
@endsection


@section ('scripts')

@endsection 