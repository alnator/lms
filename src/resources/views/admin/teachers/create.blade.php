@extends('admin.adminmaster')

@section ('title','Teachers')

@section ('content')
	<div class="col-12 card" >
		{!! Form::open(['route'=>'teacher-create-api']) !!}

		<div class="row">

			<div class="col-4">

				{{ Form::label('user_id' , 'User:')}}
				<select class="input-rounded custom-select form-control"  name="user_id">
					@foreach($users as $user)
						<option value="{{$user->id}}" >{{$user->name}} </option>
					@endforeach
				</select>
			</div>
			<div class="col-4">
				{{ Form::label('fixed_fees' , 'Fixed Fees:')}}
				{{ Form::number('fixed_fees' , null , ['class' => 'form-control input-rounded'])}}
			</div>
			<div class="col-4">
				{{ Form::label('percent_fees' , 'Percent Fees:')}}
				{{ Form::number('percent_fees' , null , ['class' => 'form-control input-rounded'])}}
			</div>
		</div>
</div>
	<div class="col-12 card">
		{{ Form::submit('Submit Teacher', ['class' => 'btn btn-success btn-block'])}}

		<a href="{{route('teacher-admin')}}" class="btn btn-default btn-block" style="margin-top:10px;">Cancel</a>
	</div>		
{!! Form::close() !!}
@endsection

@section ('scripts')

@endsection 