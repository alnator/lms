@extends('admin.adminmaster')

@section ('title','Teacher Billing')
@section ('styles')


	<link rel="stylesheet" href="<?php echo config('app.url'); ?>/css/bootstrap-datepicker3.min.css">
    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
<input type="hidden" id="teacher-views">
<input type="hidden" id="teacher-percentage">
	<div class="col-12 card">
		<div class="row">
			<div class="col">
			<label>Choose a teacher</label>
			<select class="custom-select form-control input-rounded" id="teacher-select">
				@foreach($users as $user)
				<option value="{{$user->id}}" class="form-control"> {{$user->name}}</option>
				@endforeach
			</select>	
			</div>
			<div class="col">
				<label>Select from date</label>
				<input type="text" class="form-control input-rounded " name="from" id="from-date">
			</div>
			<div class="col">
				<label>select to date</label>
				<input type="text" class="form-control input-rounded " name="to" id="to-date">
			</div>
			<div class="col">
				<label>AWS Streaming Bill</label>
				<input type="number" class="form-control input-rounded" name="aws" id="aws-bill">
			</div>
			<div class="col">
				<br>
				<button class="btn btn-default btn-block" style="margin-top: 10px" id="calc-billing"> Calculate</button>
			</div>
		</div>

	</div>
	<div id = "single-course-div" class="col-12 card">
		<p> Single Course Billing : <b><i id="single-course"></i></b> </p>
		<div class="col-12" id="course-grid"> </div>
		<div class="col-12">
			<button class="btn btn-default" id="export1" style="float: right">Export to CSV</button>
		</div>
	</div>
	<div id = "subscription-div" class="col-12 card">
		<p> Subscriptions Billing :  <b> <i id="subscriptions"> </i></b></p>

		<div class="col-12" id="subscriptions-grid"> </div>		
		<div class="col-12">
			<button class="btn btn-default" id="export" style="float: right">Export to CSV</button>
		</div>
	</div>
@endsection


@section ('scripts')
<script type="text/javascript">var siteurl= " <?php echo config('app.url'); ?>";</script>
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
 <!-- <script src="<?php echo config('app.url'); ?>/js/teachers-grid.js"></script> -->
 	<script src="<?php echo config('app.url'); ?>/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo config('app.url'); ?>/js/teacher-billing.js"></script>
    <script type="text/javascript">
      $.fn.datepicker.defaults.format = "yyyy/mm/dd";
    $( function() {
    $("#to-date").datepicker({
       yearRange: "-3:0",
       changeMonth: true,
       changeYear: true
    });
  });
    $( function() {
    $("#from-date").datepicker({
       yearRange: "-3:0",
       changeMonth: true,
       changeYear: true
    });
  }); 

    $('#calc-billing').on('click' ,function(e){
    	showBillingSingleCourse();
    });
    </script>
@endsection 