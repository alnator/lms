@extends('admin.adminmaster')

@section ('title','Gallery')

@section ('content')
			

	<div class="col-12 card" >
		{!! Form::open(['route' => 'admin-store-gallery' , 'files' => true ]) !!}	
			<div class="row">
				<div class="col-12">
					<h3> Gallery Item Details </h3>
				</div>
				<div class="col-6"> 
					{{ Form::label('title_en', 'Title EN:')}}
					{{ Form::text('title_en', null ,['class' => 'form-control input-rounded']) }}
					<br><br>
					{{ Form::label('overview_en', 'Overview EN:')}}
					{{ Form::text('overview_en', null ,['class' => 'form-control input-rounded']) }}
					<br><br>
				</div>
				
				<div class="col-6">
					{{ Form::label('title_ar', 'Title AR:')}}
					{{ Form::text('title_ar', null ,['class' => 'form-control input-rounded']) }}
					<br><br>
					{{ Form::label('overview_ar', 'Overview AR:')}}
					{{ Form::text('overview_ar', null ,['class' => 'form-control input-rounded']) }}
					<br><br>
				</div>

				<div class="col-6">
					{{ Form::label('album_id', 'Album:')}}
					<select class="form-control input-rounded" name="album_id" id="album_id">
						<option value="">None</option>
						@foreach($albums as $album)
							<option value="{{$album->id}}" >{{$album->name_en}}</option>
						@endforeach
					</select>
				</div>

				<div class="col-6">
					{{ Form::label('img_src', 'Image:')}}<br>
					{{ Form::file('img_src', null ,['class' => 'form-control input-rounded']) }}
					<br><br>
					{{ Form::label('type', 'Item Type:')}}
					<select name="type" id="type" class="form-control input-rounded">
						<option value="1"  selected>Image</option>
						<option value="2" >Video</option>
					</select>
					<br><br>
				</div>

				<div class="col-6 d-none" id="hide-me" >
					{{ Form::label('youtube_embed', 'Youtube Embed Code:')}}
					{{ Form::text('youtube_embed', null ,['class' => 'form-control input-rounded']) }}
					<br><br>
				</div>
			 </div>
	</div>

	<div class="col-12 card">
		{{ Form::submit('Create Item' , ['class'=>'btn btn-success '])}}
		<a href="{{route('admin-gallery')}}" class="btn btn-default " style="margin-top: 10px;border: 1px solid #26dad2;"> Cancel</a>
	</div>

	{!! Form::close() !!}
@endsection


@section ('scripts')
	<script type="text/javascript">
		$('#type').change(function(){
			if ($(this).val() == 1){
				$('#hide-me').addClass('d-none');
			}
			else {
				$('#hide-me').removeClass('d-none');
			}
		});
	</script>
@endsection 