@extends('admin.adminmaster')

@section ('title','Course Categories')
@section ('styles')



    <link href="<?php echo config('app.url'); ?>/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

@endsection

@section ('content')
	<div class="col-12 card" id="course-categories-grid">
		
	</div>
	<div class="col-12 card">
		<a href="{{route('categories-create')}}" class="btn btn-success btn-block">Create Category</a>
	</div>
@endsection


@section ('scripts')
	<script src="<?php echo config('app.url'); ?>/js/jsgrid.min.js"></script>
	<script src="<?php echo config('app.url'); ?>/js/categories-grid.js"></script>

    <script src="<?php echo config('app.url'); ?>/js/lib/sweetalert/sweetalert.min.js"></script>

@endsection 