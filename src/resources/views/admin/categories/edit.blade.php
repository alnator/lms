@extends('admin.adminmaster')

@section ('title','Categories')

@section ('content')
	<div class="col-12 card" >
		{!! Form::model($category,['route'=>['categories-edit-api',$category->id]]) !!}

		<div class="row">

			<div class="col-6">

				{{ Form::label('name_ar' , 'Name (AR):')}}
				{{ Form::text('name_ar' , null , ['class' => 'form-control input-rounded'])}}
			</div>
			<div class="col-6">
				{{ Form::label('name_en' , 'Name (EN):')}}
				{{ Form::text('name_en' , null , ['class' => 'form-control input-rounded'])}}
			</div>
		</div>
</div>
	<div class="col-12 card">
		{{ Form::submit('Submit Category', ['class' => 'btn btn-success btn-block'])}}

		<a href="{{route('categories-admin')}}" class="btn btn-default btn-block" style="margin-top:10px;">Cancel</a>
	</div>		
{!! Form::close() !!}
@endsection

@section ('scripts')

@endsection 