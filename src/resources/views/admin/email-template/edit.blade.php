@extends('admin.adminmaster')

@section ('title','Email Template')
@section('styles')

	<link rel="stylesheet" href="<?php echo config('app.url'); ?>/css/bootstrap-datepicker3.min.css">
	
@endsection
@section ('content') 
	{!! Form::model( $emailtemplate, ['route' => ['email-template-edit-api' , $emailtemplate->email_temp_id] ]) !!}		

	<div class="col-12 card" >
		<div class="row">
			<div class="col-12">
				{{ Form::label('email_temp_name', 'Email Template Name:') }}
				{{ Form::text('email_temp_name', null, ['class'=>'form-control input-rounded' , 'required' => 'required'] ) }}
			</div>
			<div class="col-12"> 
				{{ Form::label('email_temp_desc', 'Email Template Description:')}}
				{{ Form::text('email_temp_desc' , null , ['class'=>'form-control input-rounded' , 'required'=>'required'] ) }}
			</div>
			<div class="col-12"> 
				{{ Form::label('email_temp_subject', 'Email Template Subject:')}}
				{{ Form::text('email_temp_subject' , null , ['class'=>'form-control input-rounded' , 'required'=>'required'] ) }}
			</div>
			<div class="col-12"> 
				{{ Form::label('email_temp_body', 'Email Template Body:')}}
				{{ Form::textarea('email_temp_body' , null , ['class'=>'form-control' , 'required'=>'required','style'=> '    height: 80%;'] ) }}
			</div>



			
		 </div>
	</div>
	
	<div class="col-12 card">
		{{ Form::submit('Submit Email Template' , ['class'=>'btn btn-success'])}}
		<a href="{{route('email-template')}}" class="btn btn-default " style="margin-top: 10px;border: 1px solid #26dad2;"> Cancel</a>
	</div>

	{!! Form::close() !!}
@endsection


@section ('scripts')
<script src="<?php echo config('app.url'); ?>/js/bootstrap-datepicker.min.js"></script>
 	<script type="text/javascript">
 		  $.fn.datepicker.defaults.format = "yyyy-mm-dd";
    $( function() {
	    $("#datepicker").datepicker({
	       changeMonth: true,
	       changeYear: true
	    });
	  });
 	</script>
@endsection 