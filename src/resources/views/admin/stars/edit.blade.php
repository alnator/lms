@extends('admin.adminmaster')

@section ('title','Eurekian-Stars Edit Paragraph')

@section ('styles')

<script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>
@endsection

@section ('content')
			

	<div class="col-12 card" >
		{!! Form::model($para, ['route' => 'stars-text-api' ]) !!}	
			<div class="row">
				<div class="col-12"> 
					{{ Form::label('Eurekian Stars' , 'Eurekian Starts Text (English):') }}
					<textarea name="paragraph" id="editor">
						{!! $para->text !!}
					</textarea>
				</div>
				<div class="col-12"> 
					{{ Form::label('Eurekian Stars' , 'Eurekian Starts Text (Arabic):') }}
					<textarea name="paragraph_ar" id="ar-editor">
						{!! $para->text_ar !!}
					</textarea>
				</div>
			 
			 </div>
	</div>

	<div class="col-12 card">
		{{ Form::submit('Update Text' , ['class'=>'btn btn-success '])}}
		<a href="{{route('admin-panel')}}" class="btn btn-default " style="margin-top: 10px;border: 1px solid #26dad2;"> Cancel</a>
	</div>

	{!! Form::close() !!}
@endsection


@section ('scripts')
	<script type="text/javascript">
	ClassicEditor
		.create( document.querySelector( '#editor' ) )
		.then( editor => {
			console.log( editor );
		} )
		.catch( error => {
			console.error( error );
		} );

	ClassicEditor
		.create( document.querySelector( '#ar-editor' ) )
		.then( editor => {
			console.log( editor );
		} )
		.catch( error => {
			console.error( error );
		} );
	</script>


@endsection 