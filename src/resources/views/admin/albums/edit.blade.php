@extends('admin.adminmaster')

@section ('title','Albums')
@section('styles')

	<link rel="stylesheet" href="<?php echo config('app.url'); ?>/css/bootstrap-datepicker3.min.css">
	
@endsection
@section ('content')
	{!! Form::model( $album, ['route' => ['admin-update-album' , $album->id] , 'files' => 'true' ]) !!}		

	<div class="col-12 card" >
		<div class="row">
			<div class="col-12">
				<h3> Main Details </h3>
			</div>
			
			<div class="col-6">
				{{ Form::label('name_en', 'Engilsh Title') }}
				{{ Form::text('name_en', null, ['class'=>'form-control input-rounded' , 'required' => 'required'] ) }}
			</div>
			<div class="col-6"> 
				{{ Form::label('name_ar', 'Arabic Title:')}}
				{{ Form::text('name_ar' , null , ['class'=>'form-control input-rounded' , 'required'=>'required'] ) }}
			</div>
			<div class="col-6"> 
				{{ Form::label('album_date', 'Album Date:')}}
				{{ Form::text('album_date' , null , ['class'=>'form-control input-rounded' , 'required'=>'required' , 'id' => 'datepicker'] ) }}
			</div>
			<div class="col-6">
					{{ Form::label('img_src', 'Image:')}}<br>
					{{ Form::file('img_src', null ,['class' => 'form-control input-rounded']) }}
			<img src="{{$album->img_src}}" height="100" width="100">
			</div>

		 </div>
	</div>
	
	<div class="col-12 card">
		{{ Form::submit('Submit Album' , ['class'=>'btn btn-success'])}}
		<a href="{{route('admin-albums')}}" class="btn btn-default " style="margin-top: 10px;border: 1px solid #26dad2;"> Cancel</a>
	</div>

	{!! Form::close() !!}
@endsection


@section ('scripts')
<script src="<?php echo config('app.url'); ?>/js/bootstrap-datepicker.min.js"></script>
 	<script type="text/javascript">
 		  $.fn.datepicker.defaults.format = "yyyy-mm-dd";
    $( function() {
	    $("#datepicker").datepicker({
	       changeMonth: true,
	       changeYear: true
	    });
	  });
 	</script>
@endsection 