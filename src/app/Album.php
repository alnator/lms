<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\GalleryItem;

class Album extends Model
{
    protected $table= 'albums';
    protected $fillable = ['name_en', 'name_ar' ,'filter','album_date','img_src'];

    public function images()
    {
    	return $this->hasMany(GalleryItem::class ,'album_id','id');
    }

}
