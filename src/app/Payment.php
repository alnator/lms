<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Subscription;
use App\PaymentMethod;
use App\User;
class Payment extends Model
{
  protected $table = "payment";

  protected $fillable = [
      'user_id',
      'amount',
      'invoice_id',
      'coupon',
      'payment_method_id'
  ];

  public function subscription(){
  	return $this->belongsTo(Subscription::class , 'id', 'payment_id');
  }
  public function paymentMethod(){
  	return $this->belongsTo(PaymentMethod::class , 'payment_method_id' , 'id');
  }
  public function user(){
    return $this->belongsTo(User::class , 'user_id' , 'id');
  }
}
