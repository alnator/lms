<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
  protected $table = "coupon";

  protected $fillable = [
      'code',
      'course_id',
      'group_id',
      'user_id',
      'payment_method_ids',
      'subscription_model_ids',
      'discount_type_id',
      'amount',
      'image_path_ar',
      'image_path_en',
      'popup',
      'qty',
      'start_at',
      'end_at'
  ];
}
