<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model // Contact Us Main Page
{
    protected $table = 'email';
    protected $fillable = ['name','subject', 'email', 'text'];
}
