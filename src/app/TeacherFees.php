<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherFees extends Model
{
    protected $table = 'teacher_fees';

    protected $fillable =[
      'user_id',
      'percent_fees',
      'fixed_fees'
    ];
}
