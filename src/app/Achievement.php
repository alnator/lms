<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{
    protected $table ='achievements';
    protected $fillable =['id' , 'title_en' , 'title_ar' , 'description_ar' , 'description_en' ,'event_date'];
    
}
