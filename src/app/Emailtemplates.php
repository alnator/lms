<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Emailtemplates extends Authenticatable
{
    use Notifiable;
    protected $table = 'email_templates';
    protected $primaryKey = 'email_temp_id';

 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email_temp_receiver',
        'email_temp_name',
        'email_temp_desc',
        'email_temp_subject',
        'email_temp_body',
        'email_temp_status',
        'email_temp_type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];




}