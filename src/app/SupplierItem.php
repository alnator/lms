<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Item;

class SupplierItem extends Model
{
    protected $table= 'supplier_item';
    protected $fillable= ['id','item_id','supplier_id','price','active','qty'];

	public function user(){
		return $this->belongsTo(User::class , 'supplier_id' , 'id');
	}
	public function item(){
		return $this->belongsTo(Item::class , 'item_id', 'id');
	}
}
