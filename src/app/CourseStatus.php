<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseStatus extends Model
{
  protected $table = "course_status";

  protected $fillable = [
      'course_id',
      'status',
      'user_id'
  ];
}
