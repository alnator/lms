<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionModel extends Model
{
    protected $table = 'subcription_model';

    protected $fillable = [
      'name_ar',
      'name_en',
      'name',
      'display_price',
      'price',
      'period_in_days',
      'full_access'
    ];
}
