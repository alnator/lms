<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AbandonedCart extends Model
{
  protected $table = 'abandoned_carts';
  protected $fillable = ['user_id','subscription_model_id','course_id','payment_method_id','coupon'];

  public function user()
  {
      return $this->belongsTo(User::class, 'user_id', 'id');
  }

  public function subscriptionsModel()
  {
      return $this->belongsTo(SubscriptionModel::class, 'subscription_model_id', 'id');
  }

  public function course()
  {
      return $this->belongsTo(Course::class, 'course_id', 'id');
  }
}
