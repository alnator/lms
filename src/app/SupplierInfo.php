<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class SupplierInfo extends Model
{
    protected $table ='supplier_info';
    protected $fillable = [
    	'user_id',
    	'city',
    	'name', 
    	'phone_number',
    	'cell_phone_number',
    	'email',
    	'province_code',
    	'post_code',
    	'country_code',
    	'address_line1',
    	'address_line2',
    	'address_line3'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class , 'user_id' , 'id');
    }
}
