<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Course;

class Downloadable extends Model
{
  protected $table = "downloadable"; 

  protected $fillable = [
      'name_ar',
      'name_en',
      'file_path_ar',
      'file_path_en',
      'video_id',
      'course_id'
  ];

  public function course(){
  	return $this->belongsTo(Course::class,'course_id' , 'id');
  }
}
