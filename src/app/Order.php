<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\OrderItem;
use App\Kit;
class Order extends Model
{
    protected $table= 'order';
    protected $fillable = [ 
        'id',
        'user_id',
        'kit_id',
        'total_price',
        'selling_price',
        'state', 
        'updated_by',
        'line1',
        'line2',
        'line3',
        'account_number',
        'phone_number',
        'phone',
        'city',
        'supplier_id',
        'aramex_person_name',
        'country_code',
        'shipment_label_url',
        'shipment_id',
        'payment_method',
        'status',
        'coupon_code',
        'coupon_amount',
        'province',
        'postal_code',
        'contact_name',
        'contact_email',
        'post_data',
        'response_data'

    ];

    public function user(){
    	return $this->belongsTo(User::class, 'user_id', 'id');
    } 

    public function items(){
    	return $this->hasMany(OrderItem::class, 'order_id', 'id');
    }
    public function kit(){
    	return $this->belongsTo(Kit::class , 'kit_id', 'id');
    }
    public function updatedBy(){
        return $this->belongsTo(User::class , 'updated_by', 'id');
    }
}
