<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class OrderTracking extends Model
{
    protected $table= 'order_tracking';
    protected $fillable = [ 
        'id',
        'order_id', 
        'waybillnumber', 
        'updatecode', 
        'updatedescription', 
        'updatedatetime', 
        'updatelocation', 
        'comments', 
        'problemcode'
        
    ];

    
}
