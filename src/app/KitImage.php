<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Kit;

class KitImage extends Model
{
    protected $table = 'kit_img';
    protected $fillable = ['kit_id' , 'img_src'];

    public function kit(){
    	return $this->belongsTo(Kit::class , 'id', 'kit_id');
    }
}
