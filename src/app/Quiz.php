<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\QuizChoice;

class Quiz extends Model
{
    protected $table = "quiz";
    protected $fillable = ['id' ,'video_id','quiz_statement', 'quiz_statement_ar'];


    public function choice(){
    	return $this->hasMany(QuizChoice::class , 'quiz_id', 'id');
    }
}
