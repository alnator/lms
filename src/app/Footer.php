<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{
    protected $table= 'footer';
    protected $fillable = ['number1' , 'number2','email','facebook' ,'twitter', 'instagram' , 'youtube','created_at','updated_at','margin','currency_symbol','currency_position','currency_conversation'];

   
}
