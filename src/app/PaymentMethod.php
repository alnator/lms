<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
  protected $table = "payment_method";

  protected $fillable = [
      'name',
      'logo_image_path',
      'percent_fees',
      'fixed_fees'
  ];
  
  const PAYPAL = 1; 

  const PAYFORT = 2;
  
  const MANUALLY = 3; // stripe before changes

  const STS = 4;

  const ARAMEX = 5;
}
