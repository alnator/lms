<?php

namespace App\Http\Controllers;
use LaravelPayfort\Traits\PayfortResponse as PayfortResponse;
use Illuminate\Http\Request;
use Auth;
use App\SubscriptionModel;
use App\Course;

class PayfortController extends Controller
{
	use PayfortResponse;
    public function index(Request $request){
    	// dd($request);
    	// $payfort = new Payfort();
    	if (!Auth::check()){
    		return redirect(route('login'));
    	}
    	$user = Auth::user();

    	$modelId = $request->model_id; // Subscription Model ID
        // dd($totalAmountWithFee);

    	$model = SubscriptionModel::find($modelId);
    	$requestParams = array(
		'command' => 'AUTHORIZATION',
		'access_code' => 'zx0IPmPy5jp1vAz8Kpg7', // from sandbox
		'merchant_identifier' => 'CycHZxVj', // from sandbox
		'merchant_reference' => 'XYZ9239-yu898', // from sandbox
 		'currency' => 'USD',
		'language' => 'en',
		'customer_email' => $user->email,
		'signature' => '7cad05f0212ed933c9a5d5dffa31661acf2c827a',
		'order_description' => $model->name_en,
		'return_url ' => '/payfort-redirect'
		);

    	if ($modelId == 5){
            $course = Course::find($request->course_id);
        	$requestParams =array_merge($requestParams, ['amount'=>$course->price]);
        }
        else {
        	$requestParams =array_merge($requestParams, ['amount' => $model->price]);	
        }

		$redirectUrl = 'https://sbcheckout.payfort.com/FortAPI/paymentPage';
		echo "<html xmlns='http://www.w3.org/1999/xhtml'>\n<head></head>\n<body>\n";
		echo "<form action='$redirectUrl' method='post' name='frm'>\n";
		foreach ($requestParams as $a => $b) {
			echo "\t<input type='hidden' name='".htmlentities($a)."' value='".htmlentities($b)."'>\n";
		}
		echo "\t<script type='text/javascript'>\n";
		echo "\t\tdocument.frm.submit();\n";
		echo "\t</script>\n";
		echo "</form>\n</body>\n</html>";
    }


    public function processReturn(Request $request){
        $payfort_return = $this->handlePayfortCallback($request);
        dd($payfort_return);
        # Here you can process the response and make your decision.
        # The response structure is as described in payfort documentation
    }
}
