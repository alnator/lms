<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\AdminService;
use App\Services\OrderService;
use App\Services\SupplierService;
use App\Services\AramexService;
use Auth;
use App\Item;
use App\OrderItem;
use App\SupplierItem;
use App\SupplierInfo;
use App\OrderStates;
use App\Order;

class SupplierController extends Controller
{
    public function __construct()
    {
    	$this->middleware('issupplier');
    }

    public function getSupplierIndex()
    {
        return redirect(route('waiting-items-supplier'));
    }

    public function getKitsPage(){
    	return view('supplier.pages.kits');
    }
    public function getKitsData(Request $request){
    	$filter = $request->filter;
    	return AdminService::getKitData($filter);
    }

    public function getKitItemsPage($id){
    	return view('supplier.pages.kits-items');
    } 

    public function getKitItemsData($id, Request $request){
    	$filter = $request->filter;
    	return AdminService::getKitItemsData($filter , $id);
    }

    public function getItemsPage(){
    	return view('supplier.pages.items');
    }

    public function getItemsData(Request $request){
    	$filter = $request->filter;
    	return AdminService::getItemsData($filter);
    } 

    public function getSupplyingItemsPage(){ 
    	return view('supplier.pages.supplying-items')->withSupplierId(Auth::id());
    }	

    public function getSupplyingItemsData(Request $request){
    	$filter = $request->filter;
        $supplier_id = $request->id;
    	return SupplierService::getSupplyingItemsData($filter , $supplier_id);
    }

    public function updateSupplyingItem($id , Request $request){
        $supplier_id = $request->id;
        SupplierService::CreateUpdateSupplyingItem($request , $supplier_id);
        return redirect(route('items-supplying'));
    }
    public function updateSupplyingItemPage($id){
        $updItem = SupplierItem::find($id);
        $item = Item::find($updItem->item_id);
        return view('supplier.pages.update-item')->withSuppliedItems($item)->withItem($updItem);
    }
    public function addSupplyingItem(Request $request){
        $id = $request->id;
        SupplierService::CreateUpdateSupplyingItem($request , $id);
        return redirect(route('items-supplying'));
    }
    public function addSupplyingItemPage(){
        $id =  Auth::id();
        $item = SupplierItem::select('item_id')->where('supplier_id', '=' ,$id)->get()->toArray();
        $itemSup = Item::whereNotIn('id' , $item)->get();
        return view('supplier.pages.add-item')->withSuppliedItems($itemSup);
    }

    public function getWaitingPage(){
        return view('supplier.pages.waiting');
    }
    
    public function getWaitingData(Request $request){
        $filter = $request->filter;
        return SupplierService::getWaitingData($filter);
    }

    public static function packItem (Request $request , $id){
        $orderItem  = OrderItem::find($id);
        $orderItem->packed = 1; 
        $orderItem->update();
        $orderComplete = OrderService::checkOrderState($orderItem->order_id);
        return $orderComplete;
    }


    public function getShipmentInfoPage(Request $request)
    {
        $supplierInfo = SupplierInfo::where('user_id', Auth::id())->first();
        return view('supplier.pages.shipment-info')->withSupplierInfo($supplierInfo);
    }

    public function updateShipmentInfo(Request $request)
    {
        SupplierInfo::updateOrCreate(['user_id'=>Auth::id()],[
            'city' => $request->city,
            'name' => $request->name,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'cell_phone_number' => $request->cell_phone_number,
            'province_code' => $request->province_code,
            'post_code' => $request->post_code,
            'country_code' => $request->country_code,
            'address_line1' => $request->address_line1,
            'address_line2' => $request->address_line2,
            'address_line3' => $request->address_line3,
        ]);
        return redirect(route('shipment-info'));
    }

    public function getShippingListPage()
    {
        return view('supplier.pages.shipping-orders');
    }

    public function getShippingListData(Request $request)
    {
        $filter = $request->filter;
        $data = SupplierService::getShippingListData($filter);
        return $data;
    }

    public function shipOrder($orderId,Request $request)
    {
        if (!isset($request->date)){
            return 'not ok';
        }
        $time = strtotime($request->date)*1000;
        $validationRes = AramexService::createShipment($orderId , $time);
        if (is_string($validationRes)){
            return $validationRes;
        }
        else {
            Order::where('id' , $orderId)->update(['state' => OrderStates::ONDELIVERY]);
            return 'ok';
        }
    }
    public function shipOrderdetails($orderId)
    {
        
        $orders = OrderItem::where('order_id','=',$orderId)->where(function ($query) {
            $query->where('type', '=', 'Kit')
                  ->orWhere('type', '=', 'Item');
        })->with('kitItem')->with('kit')->get();
        $str = '';
        $i=1;
        foreach ($orders as $value) {
            $str.= '<tr>';
            if($value->type == 'Kit'){

                $str.= '<td>'.$value->kit->name_en.'</td><td>'.$i.'</td>';
            }elseif($value->type == 'Item'){

                $str.= '<td>'.$value->kitItem->items->name_en.'</td><td>'.$i.'</td>';
            }
            $i++;
            $str.='</tr>';
        
        }
        echo $str;
        exit;
    }
    
}
