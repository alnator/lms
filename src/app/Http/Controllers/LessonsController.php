<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\Course;
use App\UserProgress;
use App\Session;
use App\Subscription;
use Auth;
use App\Quiz;
use App\QuizChoice;
use Carbon\Carbon;
use App\QuizReport;
use App\Item;
use App\Kit;
use App\Services\SlugService;
use App\Services\OrderService;
use App;
use App\Footer;
use App\KitItem;
use App\Bannerimages;


class LessonsController extends Controller
{

    public function tryFreeLesson(){
        $videos = Video::where('free', '=', 1)->get();
        if ($videos->count() != 0 )
            $id = rand() % $videos->count();
        else 
            return redirect(route('home'));
        return LessonsController::getFreeLesson($videos[$id]->id);
    }

    public function Freeclasses(){


         self::langService();
      $freeLessons = Video::with(['course'=>function($query){
    $query->select('id','title_en','title_ar');
}])->where('free' , '=', 1)->where('active', 1)->get();

    /*  $freeLessons = Course::with(['session' =>function ($query){
            $query->with(['video' => function ($query){
                $query->where('active' , 1);
                $query->where('free' , '=', 1);
            }]);
        }])->get();*/


//dd($freeLessons);
      if (App::getLocale() == 'ar'){
            
            foreach ($freeLessons as $video) {
                $video->name_en = $video->name_ar;
                $video->description_en = $video->description_ar;
                $video->course->title_en = $video->course->title_ar;
            
            }
        }

        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
        $bannerimages = Bannerimages::where('display_area', '=', 'free-classes')->first();

          return view('client.pages.free-classes')->withCourse($freeLessons)->withSlugs($slugs)->withFooter($footer)->withBannerimages($bannerimages);



    }



    

    public function getFreeLesson($id){
        self::langService();
        $video = Video::find($id);
        if($video->free != 1)
         return redirect(session('_previous')['url']);
        $video = Video::find($id);
        $course = Course::with(['session'=> function($query){
            $query->with(['video' => function ($query){
                $query->where('active' , 1);
            }]);
        }])->find($video->course_id);
        $session= Session::find($video->session_id);
        $purchased =0;
        if (Auth::check()){
            $subscriptions = Subscription::where('user_id',  '=' , Auth::id())->whereIn('course_id' ,[ $course->id, 0]);
            if ($subscriptions->count() != 0){
                $purchased =1;
            }    
        }
        $slugs = SlugService::getSlugs();
    	$footer = SlugService::getFooter();
        if (App::getLocale() == 'ar'){
            $course->title_en = $course->title_ar;
            $course->overview_en = $course->overview_ar;
            $course->description_en = $course->description_ar;
            foreach ($course->session as $session) {
                $session->session_number = $session->session_number_ar;
                foreach ($session->video as $video2) {
                    $video2->name_en = $video2->name_ar;
                    $video2->description_en = $video2->description_ar;
                }
            }
            $video->name_en = $video->name_ar;
            $video->description_en = $video->description_ar;
                
        }
        $footerdata = Footer::find(1);
        $margin = $footerdata->margin;
        if(isset($course->kit_id) && $course->kit_id!="" && $course->kit_id!=0)
       {
        $kits = Kit::find($course->kit_id);
        $kitprice =OrderService::getKitMinimumPrice($kits->id);
        $kits->maxPrice = $kitprice['minimumPrice'] + ($kitprice['minimumPrice'] * ($margin/100));
        $kits->supplierId = $kitprice['supplierId'];
        $kits->available = $kitprice['available'];
        $kits->itemsCount = $kits->items->count();
        if (App::getLocale() == 'ar'){
            $kits->name_en = $kits->name_ar;
            $kits->description_en = $kits->description_ar;
        }
//dd($kits);
       $items = KitItem::where('kit_id','=',$course->kit_id)->take(9)->get();
        foreach ($items as $item) {
            $price = OrderService::getMaximumPrice($item->item_id);
        	$item->maxPrice =$price['MaxAmount'] + ($price['MaxAmount'] * ($margin/100));
        	$item->supplierId = $price['SupplierId'];
            $item->qty = $price['qty'];

        }
        }


         $previous = Video::where('course_id','=',$session->course_id)->where('id', '<', $id)->where('active' , 1)->orderBy('id','desc')->first();
         if(isset($previous)){
            if (App::getLocale() == 'ar'){
                $previous->name_en=$previous->name_ar;
                $previous->description_en=$previous->description_ar;
            }else{
                $previous->name_en=$previous->name_en;
                $previous->description_en=$previous->description_en;
            }
            
             }
            // get next user id
            $next = Video::where('course_id','=',$session->course_id)->where('id', '>', $id)->where('active' , 1)->orderBy('id')->first();

            if(isset($next)){


            if (App::getLocale() == 'ar'){
                $next->name_en=$next->name_ar;
                $next->description_en=$next->description_ar;
            }else{
                $next->name_en=$next->name_en;
                $next->description_en=$next->description_en;
            }

                }

        return view('client.pages.free-lesson')->withCourse($course)->withSession($session)->withVideo($video)->withPurchased($purchased)->withSlugs($slugs)->withFooter($footer)->withItems($items)->withKits($kits)->withPrevious($previous)->withNext($next);
    }

    public function getLessonPage($id){
        self::langService();
       // dd(Auth::check());



            
        if(!Auth::check()){
            return back();
        }
        $video = Video::find($id);
        $course = Course::with(['session'=> function($query){
            $query->with(['video' => function ($query){
                $query->where('active' , 1);
            }]);
        }])->find($video->course_id);

        $qCount = Quiz::where('video_id' , $video->id)->where('status' , 'Active')->where('type' , 'video')->count();
        $video->quiz = 1;
        if ($qCount == 0){
            $video->quiz = 0;
        }

        $user_id = Auth::id();
        $subsCheck = Subscription::where('course_id' , '=', $video->course_id)->orWhere('course_id' , '=', 0)->where('user_id' , '=', $user_id)->get();




        //dd($subsCheck);
        //Check Subscription
        $bln = 0 ;
        foreach ($subsCheck as $subs) {
            if (Carbon::parse($subs->end_date)->gte(Carbon::today())){
                $bln =1;
            }
        }
        if (!Auth::user()->admin){
            if($subsCheck->count() == 0 )
                return back();
            if ($bln == 0)
                return redirect(route('home'));
        }


        $session= Session::find($video->session_id);
        // get previous Video
	    $previous = Video::where('course_id','=',$session->course_id)->where('id', '<', $id)->where('active' , 1)->orderBy('id','desc')->first();

	    // get next Video
	    $next = Video::where('course_id','=',$session->course_id)->where('id', '>', $id)->where('active' , 1)->orderBy('id')->first();

	    //echo "<pre>";
	    //print_r($previous);
	    //dd($next);
	    //exit;
        foreach ($course->session as $csession) {
            foreach ($csession->video as $svideo) {
                $prog = UserProgress::where('video_id', '=' , $svideo->id)->where('user_id', '=' , $user_id)->get()->first();
                if (isset($prog)){
                    if($prog->video_seen == 1 ){
                        $svideo->done = 1;
                    }
                    $svideo->solved = 0;
                    if ($prog->quiz_passed){
                        $svideo->solved = 1;
                    }

                    $quizCount  = Quiz::where('video_id', $svideo->id)->where('status' , 'Active')->where('type' , 'video')->count();
                    if ($quizCount == 0 ){
                        $svideo->solved = -1;
                    }
                }
            }
        }
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
        //Quiz
        $quizes = Quiz::with('choice')->where('video_id','=',$id)->where('status' , 'Active')->where('type' , 'video')->get()->toArray();

        if (count($quizes) > 5){
            shuffle($quizes);
            //$quizes = array_slice($quizes, 0, 5);
        }
        $footer = Footer::find(1);
        $margin = $footer->margin;

        $kits = Kit::with('images')->find($course->kit_id);
        $kitprice =OrderService::getKitMinimumPrice($kits->id);
        $kits->maxPrice = $kitprice['minimumPrice'] + ($kitprice['minimumPrice'] * ($margin/100));
        $kits->supplierId = $kitprice['supplierId'];
        $kits->available = $kitprice['available'];
        $kits->itemsCount = $kits->items->count();
        if (App::getLocale() == 'ar'){
            $kits->name_en = $kits->name_ar;
            $kits->description_en = $kits->description_ar;
        }

       $items = KitItem::where('kit_id','=',$course->kit_id)->take(9)->get();
        foreach ($items as $item) {
            $price = OrderService::getMaximumPrice($item->item_id);
            $item->maxPrice =$price['MaxAmount'] + ($price['MaxAmount'] * ($margin/100));
            $item->supplierId = $price['SupplierId'];
            $item->qty = $price['qty'];

        }

        
        if (App::getLocale() == 'ar'){
            $course->title_en = $course->title_ar;
            $course->overview_en = $course->overview_ar;
            $course->description_en = $course->description_ar;
            foreach ($course->session as $session) {
                $session->session_number = $session->session_number_ar;
                foreach ($session->video as $video2) {
                    $video2->name_en = $video2->name_ar;
                    $video2->description_en = $video2->description_ar;
                }
            }
            $video->name_en = $video->name_ar;
            $video->description_en = $video->description_ar;
           

            foreach ($quizes as $quiz) { 
            
                $quiz['quiz_statement'] = $quiz['quiz_statement_ar'];
                foreach ($quiz['choice'] as $choice) {
                    $choice['choice_statement']  = $choice['choice_statement_ar'];
                }
            }



            $previous = Video::where('course_id','=',$session->course_id)->where('id', '<', $id)->where('active' , 1)->orderBy('id','desc')->first();
			 if(isset($previous)){
            $previous->name_en=$previous->name_ar;
			$previous->description_en=$previous->description_ar;
             }
		    // get next user id
		    $next = Video::where('course_id','=',$session->course_id)->where('id', '>', $id)->where('active' , 1)->orderBy('id')->first();

            if(isset($next)){
		    $next->name_en=$next->name_ar;
			$next->description_en=$next->description_ar;
            }
        }
        return view('client.pages.lesson')->withCourse($course)->withSession($session)->withVideo($video)->withSlugs($slugs)->withQuizes($quizes)->withFooter($footer)->withItems($items)->withKits($kits)->withPrevious($previous)->withNext($next);
    }
    
    public function getSessionPage($id){
            self::langService(); 
        $session = Session::find($id);
        // dd($session->id);
        if(!Auth::check()){
            return back();
        }
        $course = Course::with(['session'=> function($query){
            $query->with(['video' => function ($query){
                $query->where('active' , 1);
            }]);
        }])->find($session->course_id);
        $video = Video::where('session_id', '=' , $id)->first();
        $sessionvideo = Video::where('session_id', '=' , $id)->get();
       
        $previous = Video::where('course_id','=',$session->course_id)->where('id', '<', $video->id)->where('active' , 1)->orderBy('id','desc')->first();

        // get next Video
        $next = Video::where('course_id','=',$session->course_id)->where('id', '>', $video->id)->where('active' , 1)->orderBy('id')->first();
       

        $user_id = Auth::id();
        if (!isset($video)){
            session(['empty_session' => 1]);
            return redirect()->back();
        }
        $qCount = Quiz::where('video_id' , $id)->where('status' , 'Active')->where('type' , 'session')->count();
        $video->quiz = 1;
        if ($qCount == 0){
            $video->quiz = 0;
        }
        //Check Subscription
        $subsCheck = Subscription::where('user_id' , '=', $user_id)->where('course_id' , '=', $video->course_id)->orWhere('course_id' , '=', 0)->get();
        $bln = 0 ;
        foreach ($subsCheck as $subs) {
            if (Carbon::parse($subs->end_date)->gte(Carbon::today())){
                $bln = 1;
            }
        }
        if (!Auth::user()->admin){
            if($subsCheck->count() == 0 ) 
                return redirect(route('home'));
            if ($bln == 0 ){
                return redirect(route('home'));
            }
        }
        foreach ($course->session as $csession) {
            foreach ($csession->video as $svideo) {
                $prog = UserProgress::where('video_id', '=' , $svideo->id)->where('user_id', '=' , $user_id)->get()->first();
                if (isset($prog)){
                    if($prog->video_seen == 1 ){
                        $svideo->done = 1;
                    }
                    $svideo->solved = 0;
                    if ($prog->quiz_passed){
                        $svideo->solved = 1;
                    }

                    $quizCount  = Quiz::where('video_id', $id)->where('status' , 'Active')->where('type' , 'session')->count();
                    if ($quizCount == 0 ){
                        $svideo->solved = -1;
                    }
                }
            }
        }
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
        $items = Item::with('images')->take(9)->get();
        foreach ($items as $item) {
            $item->maxPrice = OrderService::getMaximumPrice($item->id);

            if (App::getLocale() == 'ar')
                $item->name_en = $item->name_ar;
        }

        $kits = Kit::with('items')->with('images')->get();
        // foreach ($kits as $kit) {
        //     $kit->maxPrice = OrderService::getKitSellingPrice($kit->id);
        //     $kit->itemsCount = $kit->items->count();
        //     if (App::getLocale() == 'ar')
        //         $kit->name_en = $kit->name_ar;
        // }
        $quizes = Quiz::with('choice')->where('video_id','=',$id)->where('status' , 'Active')->where('type' , 'session')->get()->toArray();
        
        if (count($quizes) > 5){
            shuffle($quizes);
            $quizes = array_slice($quizes, 0, 5);
        }
        if (App::getLocale() == 'ar'){
            $course->title_en = $course->title_ar;
            $course->overview_en = $course->overview_ar;
            $course->description_en = $course->description_ar;
            foreach ($course->session as $session) {
                $session->session_number = $session->session_number_ar;
                foreach ($session->video as $video2) {
                    $video2->name_en = $video2->name_ar;
                    $video2->description_en = $video2->description_ar;
                }
            }
            $video->name_en = $video->name_ar;
           $video->description_en = $video->description_ar;
            foreach ($quizes as $quiz) {   

                $quiz['quiz_statement'] = $quiz['quiz_statement_ar'];
                foreach ($quiz['choice'] as $choice) {
                    $choice['choice_statement']  = $choice['choice_statement_ar'];
                }           
            }
           

            $previous = Video::where('course_id','=',$session->course_id)->where('id', '<', $course->id)->where('active' , 1)->orderBy('id','desc')->first();
            if(isset($previous)){
            $previous->name_en=$previous->name_ar;
            $previous->description_en=$previous->description_ar;
             }
            // get next user id

            $next = Video::where('course_id','=',$session->course_id)->where('id', '>',  $course->id)->where('active' , 1)->orderBy('id')->first();
        if(isset($next)){
            $next->name_en=$next->name_ar;
            $next->description_en=$next->description_ar;
        }
            foreach ($sessionvideo as  $svideo) {
                $svideo->name_en=$svideo->name_ar;
                $svideo->description_en=$svideo->description_ar;
            }
                    
        }
        return view('client.pages.session')->withCourse($course)->withSession($session)->withVideo($video)->withSlugs($slugs)->withQuizes($quizes)->withFooter($footer)->withItems($items)->withKits($kits)->withPrevious($previous)->withNext($next)->withSessionvideo($sessionvideo);
    }
    public function videoFinished(Request $request){
        $video_id = $request->video_id;
        $video = Video::find($video_id);
        $cid = $video->course_id;
        $pid = UserProgress::UpdateOrCreate(['user_id' => Auth::id() , 'video_id' => $video_id] ,[
            'quiz_passed' => 1,
            'course_id'=>$cid
        ]);


        return 1;
    }   
    public function quizCheck(Request $request){

        $quizId = $request->quiz_id;
        $choiceId = $request->choice_id;
        $quiz = Quiz::find($quizId);
        if($quiz->type == 'video'){
        $video = Video::find($quiz->video_id);
        }else{
          $video = Session::find($quiz->video_id);   
        }
        if ($choiceId == -1 )
            return 'wrong2';

        $report = QuizReport::UpdateOrCreate(['id'=>0],[
            'user_id' => Auth::id(),
            'quiz_id'=> $quizId,
            'choice_id' => $choiceId,
            'video_id' => $video->id,
            'course_id' => $video->course_id,
            'correct' => 0
        ]);


        $choice = QuizChoice::find($choiceId);
        if ($choice->quiz_id == $quizId && $choice->correct == 1){
            $report->correct = 1;
            $report->update(); 
            return 'ok';
        }
        return 'wrong';
    }
    public  static function langService(){
        if (!empty(session('lang'))){
            App::setlocale(session('lang'));
        }
        else 
            App::setlocale('en');
    }
    public function videoSeen(Request $request){
        $videoId = Video::find($request->video_id);
        $prog = UserProgress::where('user_id' , '=',Auth::id() )->where('video_id' , '=',$videoId->id )->get()->first();
        UserProgress::UpdateOrCreate([ 'user_id' => Auth::id(), 'video_id' => $videoId->id], [
            'course_id' => $videoId->course_id,
            'quiz_passed' => isset($prog->quiz_passed) ? $prog->quiz_passed : 0,
            'video_seen' => 1
        ]);
        return 'ok';
    }
}
