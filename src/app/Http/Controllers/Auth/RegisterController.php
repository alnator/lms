<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Services\SlugService;
use App;
use App\Http\Controllers\ClientController;
use App\Bannerimages;
use Mail;
use App\Services\MailService;
use App\Footer;
use App\Emailtemplates;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }



    public function showRegistrationForm()
    {


           $bannerimages = Bannerimages::where('display_area', '=', 'login')->first();

           $bannerimagesre = Bannerimages::where('display_area', '=', 'register')->first();


        $slugs = SlugService::getSlugs();
        
        $footer = SlugService::getFooter();

        self::langService();
        session(['_previous'=>'/register']) ;

        return view('auth.login')->withSlugs($slugs)->withFooter($footer)->withRegister(1)->withBannerimages($bannerimages)->withBannerimagesre($bannerimagesre);

        return ClientController::index();
    }

    public  static function langService(){
        if (!empty(session('lang'))){
            App::setlocale(session('lang'));
        }
        else 
            App::setlocale('en');
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if(Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ])->errors() != null){
            session(['_previous'=>'/register']) ;
        }    
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $userdata)
    {
       // dd($data);
        $footer = Footer::find(1);
        $admin_email = $footer->email;

        $EmailTemplatesObj = Emailtemplates::find(6);
        $msg=nl2br($EmailTemplatesObj->email_temp_body);
        $msg=str_replace("<USER>",$userdata['name'],$msg);

        MailService::send('email.template',$data=['msg'=>$msg], $admin_email, $userdata['email'], $EmailTemplatesObj->email_temp_subject);
        
        $EmailTemplatesObjadmin = Emailtemplates::find(7);
        $msgadmin=nl2br($EmailTemplatesObjadmin->email_temp_body);
        $msgadmin=str_replace("<FULLNAME>",$userdata['name'],$msgadmin);
        $msgadmin=str_replace("<EMAIL>",$userdata['email'],$msgadmin);
        $msgadmin=str_replace("<AGE>",$userdata['phone'],$msgadmin);
        $msgadmin=str_replace("<PHONE>",$userdata['age'],$msgadmin);

        MailService::send('email.template',$data=['msg'=>$msgadmin], $admin_email, $admin_email, $EmailTemplatesObjadmin->email_temp_subject);
        
        
        //dd($userdata);
        return User::create([
            'name' => $userdata['name'],
            'email' => $userdata['email'],
            'password' => Hash::make($userdata['password']),
            'phone' => $userdata['phone'],
            'age' => $userdata['age'],
        ]);
    }
}
