<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use View;
use App\User;
use Password;
use Mail;
use DB;
use App\Services\SlugService;
use App\Services\MailService;
use App\Bannerimages;
use App\Emailtemplates;
use App\Footer;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function showLinkRequestForm(){
        $bannerimages = Bannerimages::where('display_area', '=', 'login')->first();
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
         return view('auth.passwords.email')->withSlugs($slugs)->withFooter($footer)->withBannerimages($bannerimages);
    }
    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $userobj = User::where('email',$request->email)->get()->first();
        if(count($userobj)>0)
        {
            if($userobj->active == '1')
            {
                    // We will send the password reset link to this user. Once we have attempted
                    // to send the link, we will examine the response then see the message we
                    // need to show to the user. Finally, we'll send out a proper response.
                $field = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'email';
                $request->merge([$field => $request->email]);

                //$response =  Password::sendResetLink($request->only($field));
           
                DB::table('password_resets')->insert([
                    'email' => $request->email,
                    'token' => str_random(60)
                ]);



                    /*$response = $this->broker()->sendResetLink(
                        $request->only('email')
                    );*/
                    $string="<br><a href=".url('password/reset')."/".$request->_token."?email=".urlencode($request->email).">Reset Password</a><br>";

                    $footer = Footer::find(1);
                    $admin_email = $footer->email;
        
                    $EmailTemplatesObj = Emailtemplates::find(4);
                    $msg=nl2br($EmailTemplatesObj->email_temp_body);
                    $msg=str_replace("<RESETPASSWORDLINK>",$string,$msg);

                    MailService::send('email.template',$data=['msg'=>$msg], $admin_email, $request->email, $EmailTemplatesObj->email_temp_subject);
                    return back()->with('status', 'E-Mail sent successfully.');

                    
                    /*return $response == Password::RESET_LINK_SENT
                                ? $this->sendResetLinkResponse($response)
                                : $this->sendResetLinkFailedResponse($request, $response);*/
                
            }else{
                
                    return redirect('/password/reset')->with('inactive','test');
                
            }
        
        }else{
            
                    return redirect('/password/reset')->with('exits','test');
            
        }
    
    }
}
