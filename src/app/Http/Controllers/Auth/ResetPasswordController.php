<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\SlugService;
use App\Bannerimages;
use DB;
use App\User;
class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    protected function resetPassword($user, $password)
    {
        $user->forceFill([
            'password' => bcrypt($password),
            'remember_token' => Str::random(60),
        ])->save();

        //$this->guard()->login($user);
    }
    public function showResetForm(Request $request, $token = null)
    {
        $bannerimages = Bannerimages::where('display_area', '=', 'login')->first();
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
        return view('auth.passwords.reset')->withSlugs($slugs)->withFooter($footer)->withBannerimages($bannerimages)->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
    public function changePassword(Request $request)
    {

            $email=DB::table('password_resets')->where('email', $request->email)->value('email');
            if(count($email)>0){
            $userobjs = User::where('email',$request->email)->get();
            $user_id=$userobjs[0]->id;
                
            if($request->password == $request->password_confirmation){

            $this->validate($request,[
            'password' => 'required|min:6',
            'password_confirmation' => 'required|string|same:password',
            ]);
            //Change Password
            $usersave = User::find($user_id);
            $usersave->password = bcrypt($request->password);
            $usersave->save();

            DB::table('password_resets')->where('email', $request->email)->delete();

            return redirect()->back()->with("status","Password changed successfully !");

            }else{
                //Current password and new password are same
               return redirect()->back()->with("status"," Password cannot be same as your Conform password."); 
            }
            }else{
                return redirect('password/reset')->with("status","Invalid Token. Your Token Is Expired. Please Try Again !");
            }
    }


    public function reset(Request $request)
    {

        dd($request);
        $this->validate($request, $this->rules(), $this->validationErrorMessages());

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
                    ? $this->sendResetResponse($response)
                    : $this->sendResetFailedResponse($request, $response);
    }


         public function sendResetResponse($response)
    {
        return redirect('control_panel/home')
                            ->with('msg_status', trans($response));
    }


}
