<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SubscriptionService;
use App\Services\PaymentService;
use App\Services\CouponService;
use App\Services\SlugService;
use App\PaymentMethod;
use App\Course;
use App\SubscriptionModel;
use App\Order;
use App\OrderItem;
use Auth;
use App\Services\OrderService;
use App;
use App\Subscription;

use Mail;
use App\Emailtemplates;
use App\Services\MailService;
use App\Footer;

class StsPaymentController extends Controller
{
	private $secrectKey='OGUzNmFkN2RmZGI0OTk4NTdkMGJmMzRk';
	private $merchantId='2000000033';
	private $itemId = '334';

	public function __construct(){
		// $secrectKey = env('PAYONE_SECRET');
		$secrectKey = 'OGUzNmFkN2RmZGI0OTk4NTdkMGJmMzRk';
		// $merchantId = env('PAYONE_MERCHANT');
		$merchantId = '2000000033';
		$itemId = "334";
	}

	private function generateHashCode(){
		$transactionId = (int)(microtime(true)*1000);
    	if ($transactionId < 0 ){
    		$transactionId *= -1;
    	}
       	session(['transaction_id' => $transactionId]);

    	$parameters = array();
	
		$parameters['TransactionID'] = $transactionId; //TMP:
	    $parameters['MerchantID'] = $this->merchantId;
	    $parameters['Amount'] = session('amount') * 1000;
	    $parameters['CurrencyISOCode'] = "400"; // Dinar Code
	    $parameters['MessageID'] = "1";
	    $parameters['Quantity'] = "1";
	    // $parameters['Channel'] = "0";
	    $parameters['Language'] = "en";
	    $parameters['ItemID']  = $this->itemId;
	    // $parameters['ThemeID'] = "1000000001"; 
	
	    $parameters['ResponseBackURL'] = route("sts-response");
	
	    $parameters['Version'] = "1.0";

	    ksort($parameters);

	    $orderedString = $this->secrectKey;
	    foreach($parameters as $k=>$param){
	   		$orderedString .= $param;
	    }
	
	    $secureHash = hash('sha256', $orderedString, false);
	    session(['secure_hash' => $secureHash]);
	}
  public  static function langService(){
        if (!empty(session('lang'))){
            App::setlocale(session('lang'));
        }
        else 
            App::setlocale('en');
    }

	public function redirectPostRequest(Request $request){

		 self::langService();


		session(['subscription_model'=>$request->model_id]);
		if (isset($request->course_id) ){
			session(['course_id' => $request->course_id ?: 0]);
		}
    	
		if (isset($request->coupon)){
			$course = $request->course_id ?: 0;
			$valid = CouponService::validateCoupon($request->coupon , Auth::id(), $course);
			if ($valid){
				session(['coupon' => $request->coupon]);
			}
			else {
				session()->forget('course_id');
				session(['payment_repsonse' => 'Coupon Error']);
				return redirect('/');
			}
		}

    	$modelId = $request->model_id; // Subscription Model ID
    	
    	if ($modelId == 5){
            $course = Order::find($request->course_id);
        	$price = $course->total_price;
        	$oldPrice = $price; // just in case of coupon miracle
        	if (isset($request->coupon))
        	{
				$amount = CouponService::getCouponAmount($request->coupon); // array
        		$price = $this->calculateNewPrice($amount, $price); // depending on coupon type "percentage or fixed"
        	}
        } else {
        	$model = SubscriptionModel::find($modelId);	
        	$price = $model->price;
        	$oldPrice = $price; // just in case of coupon miracle
        	if (isset($request->coupon))
        	{
        		$amount = CouponService::getCouponAmount($request->coupon); // array
        		$price = $this->calculateNewPrice($amount, $price); // depending on coupon type "percentage or fixed"
        	}
        } 
		session(['amount' => $price]);	
		if ($price <= 0 )
		{
			return $this->checkoutOrder($oldPrice , $request->coupon);
		}
		$this->generateHashCode();
		$attributesData = array();

		$attributesData["TransactionID"] = session('transaction_id');
		$attributesData["MerchantID"] = $this->merchantId;
		$attributesData["Amount"] = session('amount') * 1000;
		$attributesData["CurrencyISOCode"] = "400";
		$attributesData["MessageID"] = "1";
		$attributesData["Quantity"] = "1";
		$attributesData["Channel"] = "0";
		$attributesData["Language"] = "en";
		$attributesData["ThemeID"] = "1000000001";
	    $attributesData['ItemID']  = $this->itemId;

		$attributesData["ResponseBackURL"] = route('sts-response');
		$attributesData["RedirectURL"] = 'https://srstaging.stspayone.com/SmartRoutePaymentWeb/SRPayMsgHandler';
		$attributesData["Version"] = "1.0";
		
		$attributesData["SecureHash"] = session('secure_hash');
		session(['SmartRouteParams' => $attributesData]);

		if($request->pmode=='cod'){

			 $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
        //getOrdermail
        OrderService::getOrdermail($course->id,'cod');

        $order=$course;

				return view('client.pages.success')
            ->withFooter($footer)
            ->withSlugs($slugs)
            ->withOrder($order); 
			
		}else{
		return view('client.pages.payment-redirect')->with('data',$attributesData);
		}
	}

	public function stsResponse(Request $request){
			 self::langService();
			  $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
				

				
		$responseParameters = $request->toArray();
		// $parameterNames = isset($_REQUEST)?array_keys($_REQUEST):[]; 
		
		// $responseParameters = []; 
		// foreach($parameterNames as $paramName){
		// 	$responseParameters[$paramName]  = filter_input(INPUT_POST,$paramName) ;
		// }
		
		// ksort($responseParameters);
		// $orderedString = $this->secrectKey;;
		// foreach($responseParameters as $k=>$param){
		// 	$orderedString .= $param;
		// }

		ksort($responseParameters);
		//echo $request->Response_GatewayStatusCode;
		//dd($responseParameters);


		$orderedString = $this->secrectKey;
		foreach($responseParameters as $k=>$param){
			$orderedString .= $param;
		}

		$secureHash = hash('sha256', $orderedString, false);

		$receivedSecureHash = $request->Response_SecureHash;
		$receivedSecureHash = $secureHash;

		// WHAT THE FFFFUUUCCCCKKKK ?? O.O
		// session(['subscription_model'=>'3']);
		// session(['course_id'=>'0']);
		

		if($receivedSecureHash !== $secureHash){
			$orderid=session('course_id');
			$order = Order::find($orderid);

				 $order->status ="unpaid";
				 $order->save();

			session(['payment_repsonse' => 'Fail']);
			return redirect('/');
		}
		else{
			$transactionId = session('transaction_id');
			$gatewayStatusCode = $request->Response_GatewayStatusCode;
			if ($gatewayStatusCode ==  '0000'){ // Accepted transaction
				$cardHolderName = $request->Response_CardHolderName;
				$receiptReferenceNumber = $request->Response_RRN;
				$orderid=session('course_id');
				 $order = Order::find($orderid);

				$coupon = empty($order->coupon_code) ? '' : $order->coupon_code ;
				
				$payment = PaymentService::makePayment($request->Response_Amount,$receiptReferenceNumber ,$coupon,  PaymentMethod::STS, Auth::id());
				
				$subscriptionModel = SubscriptionModel::find(session('subscription_model'));
				
				$startDate = date('Y-m-d H:i:s');
				


				
				// $order = Order::find($orderid);
				 $order->status ="paid";
				 $order->save();
				 
				  $orderobj = OrderItem::where('order_id','=',$orderid)->where('type','=','Course')->get();		

				foreach ($orderobj as $orderlist) {

					if($orderlist->type=='Course'){
					
					$course_ids=$orderlist->item_id;
					$courseobj = Course::find($course_ids);


					$endDate = date('Y-m-d H:i:s', strtotime($startDate. ' + ' . $courseobj->emonth .' months'));

	 $subscriptionsobj = Subscription::where('user_id',  '=' , Auth::user()->id)->where('course_id' ,$orderlist->item_id)->update([
           'status' => 'inactive'
        ]);
     


				SubscriptionService::subscribe(Auth::user()->id , session('subscription_model') , $payment ,PaymentMethod::STS , $orderlist->item_id ,$startDate ,$endDate );


				//***** mail code ******//

				$footer = Footer::find(1);
                $admin_email = $footer->email;

				$EmailTemplatesObj = Emailtemplates::find(9);
                $msg=nl2br($EmailTemplatesObj->email_temp_body);
               
               $coursename = $courseobj->title_en;
               
                $msg=str_replace("<DATE>",$startDate,$msg);
                $msg=str_replace("<NAME>",$coursename,$msg);
              
                MailService::send('email.template',$data=['msg'=>$msg], $admin_email, Auth::user()->email, $EmailTemplatesObj->email_temp_subject);

                MailService::send('email.template',$data=['msg'=>$msg], $admin_email, 'siddhraj@webmynesystems.com', $EmailTemplatesObj->email_temp_subject);


				//***** mail code ******//


				}
			}

				OrderService::getOrdermail($orderid,'card');


				//***** mail code ******//

				$footer = Footer::find(1);
                $admin_email = $footer->email;

				$EmailTemplatesObj = Emailtemplates::find(8);
                $msg=nl2br($EmailTemplatesObj->email_temp_body);
                $string="<br><a href=".url('order-detail')."/".$orderid.">".$orderid."</a><br>";
                


                $date=date('Y-m-d');
                $msg=str_replace("<DATE>",$date,$msg);
                $msg=str_replace("<ORDER>",$string,$msg);
                $msg=str_replace("<TRANSACTION>",$transactionId,$msg);

                MailService::send('email.template',$data=['msg'=>$msg], $admin_email, Auth::user()->email, $EmailTemplatesObj->email_temp_subject);

                MailService::send('email.template',$data=['msg'=>$msg], $admin_email, 'siddhraj@webmynesystems.com', $EmailTemplatesObj->email_temp_subject);


				//***** mail code ******//


				session(['payment_repsonse' => 'Success']);
			return view('client.pages.success')
            ->withFooter($footer)
            ->withSlugs($slugs)
            ->withOrder($order); 
			}else if($request->Response_StatusCode=="10008"){
				$orderid=session('course_id');
				 $order = Order::find($orderid);
				 $order->status = "canceled";
				 $order->save();
				 session(['payment_repsonse' => 'Fail']);
				 return view('client.pages.fail')
            ->withFooter($footer)
            ->withSlugs($slugs)
            ->withOrder($order); 
				
			}else{
				
				session(['payment_repsonse' => 'Fail']);
				$orderid=session('course_id');
			$order = Order::find($orderid);

				 $order->status ="unpaid";
				 $order->save();
				return redirect('/');
			}
		} 
	} 

	private function calculateNewPrice($couponAmount , $price)
	{
		if ($couponAmount['type'] == 'f')
		{
			$price -= $couponAmount['amount'];
		}
		else {
			$price -= ($price * $couponAmount['amount']);
		}
		return $price;
	}

	private function checkoutOrder($price,$coupon)
	{
		$payment = PaymentService::makePayment($price,"COUPON $coupon AMOUNT GREATER THAN OR EQUAL PAYMENT AMOUNT",$coupon,  PaymentMethod::MANUALLY, Auth::id());

		$subscriptionModel = SubscriptionModel::find(session('subscription_model'));

		$startDate = date('Y-m-d H:i:s');
		$endDate = date('Y-m-d H:i:s', strtotime($startDate. ' + ' . $subscriptionModel->period_in_days .' days'));

		SubscriptionService::subscribe(Auth::user()->id , session('subscription_model') , $payment ,PaymentMethod::MANUALLY , session('course_id') ,$startDate ,$endDate );

		session(['payment_repsonse' => 'Success-COUPON MIRACLE']);
		return redirect('/');
	}
}
