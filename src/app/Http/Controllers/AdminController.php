<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\AdminService;
use App\Services\MediaService;
use App\Services\S3Service;
use App\Services\TeacherBillingService;
use App\Services\SubscriptionService;
use App\Services\PaymentService;
use App\Services\PDFService;
use App\Services\MailService;
use App\Services\ExcelService;
use App\Services\AramexService;
use App\Level;
use Auth;
use App\Course;
use App\User;
use App\Footer;
use Illuminate\Http\Response;
use App\OrderStates;
use App\Video;
use App\EmailBroadcast;
use App\Album;
use App\CourseCategory;
use App\Downloadable;
use App\OrderItems;
use App\SupplierItem;
use App\Subscription;
use App\SubscriptionModel;
use App\Payment;
use App\TeacherFees;
use App\Coupon;
use App\GroupUser;
use Excel;
use App\StripeCustomer;
use App\KitItem;
use App\StripePlan;
use App\PaymentMethod;
use App\Group;
use App\AbandonedCart;
use App\Session;
use App\Kit;
use App\Achievement;
use App\OrderItem;
use App\Slug;
use App\Order;
use App\Email;
use App\MenuItem;
use App\Quiz;
use App\StarsText;
use App\QuizChoice;
use Session as Sess;
use App\Item;
use App\GalleryItem;
use App\Testimonial;
use App\Vision;
use App\Bannerimages;
use App\Emailtemplates;
use App\Services\OrderService;
class AdminController extends Controller
{
    public function __construct()
    {
      $this->middleware(['isadmin'] , ['except' => [
          'getCoursesdata',
          'destroyCourse',
          'getCourseUsersData',
          'deleteSubscription',
          'getCourseSessions',
          'setAddCourseUser',           
          'destroyLevel',
          'getLevelsData',
          'getPaymentMethodsData',
          'setPaymentMethodCreate',
          'setPaymentMethodEdit',
          'getSessionData',
          'destroySession',
          'getVideosdata',
          'destroyVideo',
          'getQuizesData',
          'createEditQuiz',
          'createEditChoice',
          'destroyQuiz',
          'destroyChoice',
          'getQuizReportData',
          'getEmailsData',
          'destroyEmail',
          'getTestimonialsData',
          'destroyTestimonial',
          'getVisionData',
          'destroyVision',
          'getBannerimagesData',
          'getEmailTemplateData'
        ]
      ]);
    }

    public function index(){
        return redirect(route('subscription-report'));
        return view('admin.adminmaster');
    }

    // Categories
    public function getCategoriesPage(){
      return view('admin.categories.index');
    }

    public function getCategoriesData(Request $request){
      $filter = $request->filter;
      return AdminService::getCategoriesData($filter);
    }

    public function getCategoriesCreate(){
      return view('admin.categories.create');
    }
    
    public function setCategoriesCreate(Request $request){
      AdminService::CreateUpdateCategory($request);
      return redirect(route('categories-admin'));
    }

    public function getCategoryEdit($id){
      $category = CourseCategory::find($id);
      return view('admin.categories.edit')->withCategory($category);
    }
    public function setCategoriesEdit(Request $request , $id){
      AdminService::CreateUpdateCategory($request);
      return redirect(route('categories-admin'));
    }
    public function destroyCategory($id){
      CourseCategory::find($id)->delete();
      return 1;
    }


    // Courses 
    public function getCoursesPage(){
    	return view('admin.courses.index');
    }

    public function getCoursesData(Request $request){
    	$filter = $request->filter;
    	return AdminService::getCoursesData($filter);
    }

    public function getCourseEdit($id){
    	$levels = Level::get();
    	$course = Course::with('downloadable')->find($id);
      $kits = Kit::get();
      $users = User::select('id','name','email')->get();
    	return view('admin.courses.edit')->withLevels($levels)->withCourse($course)->withKits($kits)->withUsers($users);
    }

    public function setCourseEditCreate(Request $request){
     	AdminService::updateCreateCourse($request);
    	return redirect(route('courses-admin'));
    }

    public function destroyCourse($id){
    	$course = Course::find($id);	
      $courseStudents = Subscription::where('course_id', '=' , $id)->delete();
    	Session::where('course_id' , $id)->delete();
      Video::where('course_id', $id)->delete();
      $course->delete();
    	return 1;
    }

    public function getCourseCreate(){
    	$levels = Level::get();
      $kits = Kit::get();
      $users = User::select('id','name','email')->get();
    	return view('admin.courses.create')->withLevels($levels)->withKits($kits)->withUsers($users);
    }

    public function getCourseUsersPage($id){
      $course = Course::select('title_en')->find($id);
      $courseusers = Subscription::where('course_id', '=' , $id)->get();
      return view('admin.courses.users')->withId($id)->withCourse($course)->withCourseusers($courseusers);
    }
    public function getCourseUsersData(Request $request ,$id){
      $filter = $request->filter;
      return AdminService::getCourseUsersData($filter,$id);
    }

    public function deleteSubscription($id, $subid){
      Subscription::find($subid)->delete();
      return 1;
    }

    public function getAddCourseUserPage($id){
      $course = Course::find($id);
      $users = User::select('name','email','id')->get();
      $subscriptions = SubscriptionModel::get();
      $paymentmethods = PaymentMethod::get();

      return view('admin.courses.adduser')->withUsers($users)->withSubscriptions($subscriptions)->withId($id)->withCourse($course)->withPaymentmethods($paymentmethods);
    }
    public function setAddCourseUser(Request $request){
      AdminService::AddCourseUser($request);
      return redirect(route('course-users-admin', $request->id));
    }

    public function getCourseDetailsPage($id){
      $course = Course::find($id);
      return view('admin.courses.details')->withCourse($course)->withId($id);
    }

    public function getCourseSessions(Request $request){
      $course_id = $request->course_id;
      return Session::select('id','session_number')->where('course_id' , '=', $course_id)->get();
    }
    // Levels

    public function getLevelsPage(){
    	return view('admin.levels.index');
    }

    public function getLevelsData(Request $request){
    	$filter = $request->filter;
    	return AdminService::getLevelsData($filter);
   	}

   	public function getLevelCreate(){
   		return view('admin.levels.create');
   	}

   	public function setLevelCreateUpdate(Request $request){
   		AdminService::createUpdateLevel($request);
   		return redirect(route('levels-admin'));
   	} 

   	public function getLevelEdit($id){
   		$level = Level::find($id);
   		return view('admin.levels.edit')->withLevel($level);
   	}
   	public function destroyLevel($id){
   		$level = Level::find($id);
   		$level->delete();
   		return 1;
   	}
    // Sessions

    public function getSessionPage(){
      return view('admin.sessions.index');
    }
    public function getSessionData(Request $request){
      $filter = $request->filter;
      return AdminService::getSessionData($filter);
    }
    public function getSessionEdit($id){
      $session = Session::find($id);
      $course = Course::select('id', 'title_en')->get();
      return view('admin.sessions.edit')->withSession($session)->withCourses($course);
    }
    public function setSessionEdit(Request $request){
      AdminService::createUpdateSession($request);
      return redirect(route('sessions-admin'));
    }
    public function getSessionCreate(){
      $courses = Course::select('id','title_en')->get();      
      return view('admin.sessions.create')->withCourses($courses);
    }
    public function setSessionCreate(Request $request){
      AdminService::createUpdateSession($request);
      return redirect(route('sessions-admin'));
    }
    public function destroySession($id){
      Session::find($id)->delete();
      return 1;
    }


    // Videos

    public function getVideosPage(){
      return view('admin.videos.index');
    }
    public static function VideosPageHasError(){
        session(['error' => '1']);
        return view('admin.videos.index');
    }
    public function getVideosData(Request $request){
      $filter = $request->filter;
      return AdminService::getVideosData($filter);
    }

    public function getVideoCreate(){
      $courses = Course::get();
      $users = User::select('id','name','email')->get();
      return view('admin.videos.create')->withUsers($users)->withCourses($courses);
    }
    
    public function setVideoCreateEdit(Request $request){
   
      return AdminService::craeteUpdateVideo($request);
      
    }

    public function destroyVideo($id){
      $video = Video::find($id);
      $video->delete();
      return 1;
    }

    public function getVideoEdit($id){
      $video = Video::find($id);
      $courses = Course::get();
      $sessions = Session::where('course_id' , '=', $video->course_id )->get();

      $users = User::select('id','name','email')->get();
      return view('admin.videos.edit')->withVideo($video)->withUsers($users)->withCourses($courses)->withSessions($sessions);
    }
    public function getUploadVideoPage($videoId)
    {
    	$video = Video::find($videoId);
        return view('admin.videos.upload')->withVideo($video);
    }
    public function setVideoUpload(Request $request){
      return AdminService::craeteUpdateVideoUpload($request);
      
    }
      //Quizes

    public function getQuizesPage($type,$videoId)
    {


        return view('admin.quizes.index')->withVideoId($videoId)->withType($type);
    }
    public function getQuizesData($type,$videoId , Request $request){
      $filter = $request->filter;
        


      return AdminService::getQuizesData($videoId , $filter, $type);
    }
    public function getQuizCreate($type,$videoId){
      if($type=="video"){
      $video = Video::find($videoId);
      $course= Course::find($video->course_id);
      }else{
        
         $video = Session::find($videoId);
        $course= Course::find($video->course_id);
      }
      return view('admin.quizes.create')->withVideo($video)->withCourse($course)->withType($type);
    }
    
    public function getQuizEdit($type,$videoId , $quizId){
      $quiz = Quiz::find($quizId);
      if($type=="video"){
      $video = Video::find($videoId);
      $course= Course::find($video->course_id);
      }else{
        $video = Session::find($videoId);
        $course= Course::find($video->course_id);
      }
      $choices = QuizChoice::where('quiz_id' , '=', $quizId)->get();
      return view('admin.quizes.edit')->withQuiz($quiz)->withVideo($video)->withQuiz($quiz)->withChoices($choices)->withCourse($course)->withType($type);
    }
    
    public function createEditQuiz(Request $request)
    {
     
      return AdminService::createEditQuiz($request);
    }

    public function createEditChoice(Request $request)
    {
      return AdminService::createEditChoice($request);
    } 

    public function destroyQuiz($quizId){
      QuizChoice::where('quiz_id', '=' ,$quizId)->delete();
      Quiz::find($quizId)->delete();
      return 'ok';  
    }
    public function destroyChoice(Request $request){
      $id = $request->id;
      QuizChoice::find($id)->delete();
      return 'ok';
    }
    //Quiz Reports

    public function getQuizReportPage(){
      return view('admin.quiz-report.index');
    }
    public function getQuizReportData(Request $request){
      $filter = $request->filter;
      return AdminService::getQuizReportData($filter);
    }

    // Users 

    public function getUsersPage(){
      return view('admin.users.index');
    }

    public function getUsersData(Request $request){
      $filter = $request->filter;
      return AdminService::getUsersData($filter);
    }

    public function getUserCreate(){
      return view('admin.users.create');
    }
    public function setUserCreate(Request$request){
      AdminService::createUpdateUser($request);
      return redirect(route('users-admin'));
    }

    public function getUserEdit($id){
      $user = User::find($id);
      return view('admin.users.edit')->withUser($user);
    }
    public function setUserEdit($id,Request$request){
      AdminService::createUpdateUser($request);
      return redirect(route('users-admin'));
    }
    public function getUserCoursesPage($id){
      $user = User::find($id);
      return view('admin.users.courses')->withUser($user);
    }

    public function getUserCoursesData($id){
      return AdminService::getUserCourses($id);
    }

    public function getUserCourseProgressPage($userId , $courseId){
      return view('admin.users.progress');
    }

    public function getUserCourseProgressData(Request $request ,$userId , $courseId){
      $filter = $request->filter;
      return AdminService::getUserCourseProgressData($filter,$userId ,$courseId);
    }

    public function destroyUser($id){
      $user = User::find($id); 
      $user->delete();
      return 1;
    }
    public function starUser($id){
      return AdminService::starUser($id);
    }

    //Downloadables 


    public function getFilesPage()
    {
      return view('admin.files.index');
    }

    public function getFilesData(Request $request)
    {
      $filter = $request->filter;
      return AdminService::getFilesData($filter);
    }

    public function getFilesCreate(){
      $videos = Video::get();
      $courses = Course::get();
      return view('admin.files.create')->withCourses($courses)->withVideos($videos);
    }

    public function getFilesEdit($id){
        $videos = Video::get();
        $courses = Course::get();
        $file = Downloadable::find($id);
        return view('admin.files.edit')->withCourses($courses)->withVideos($videos)->withFile($file);
    }

    public function setFilesCreateEdit(Request $request){
      AdminService::CreateUpdateFiles($request);
      return redirect(route('files-admin'));
    }

    public function destroyFile($id){
      Downloadable::find($id)->delete();
      return 1; 
    }

    //Payments 

    public function getPaymentsPage(){
      return view('admin.payments.index');
    }

    public function getPaymentsData(Request $request){
      $filter = $request->filter;
      return AdminService::getPaymentsData($filter);
    }
    public function getPaymentDataPDF (Request $request){
      $d = json_decode($request->filter);
      $filter['id']= $d->id == '' ? null : $d->id  ;
      $filter['user_id'] = $d->user->email == '' ? null : $d->user->email;
      $filter['amount'] = $d->amount == '' ? null :$d->amount;
      $filter['invoice_id'] = $d->invoice_id == '' ? null : $d->invoice_id;
      $filter['coupon'] = $d->coupon == '' ? null :$d->coupon;
      $filter['pageIndex'] = 0;
      $data = AdminService::getPaymentsData( $filter ,1);
      $results['headers'] = $data['headers'];
      $arr = [];
      foreach ($data['data'] as $var) {
          $temp = [$var->id ,
            $var->user_id , 
            isset($var->amount) ?$var->amount: '' ,
            isset($var->invoice_id) ?$var->invoice_id : '' ,
            isset($var->coupon) ? $var->coupon : '',
            $var->created_at
          ];
          $arr[] = $temp;
      }
      $results['data'] = $arr;
      return PDFService::exportFunction($results);
    }

    public function getPaymentDataExcel ($type,Request $request){
      $d = json_decode($request->filter);
      $filter['id']= $d->id == '' ? null : $d->id  ;
      $filter['user_id'] = $d->user->email == '' ? null : $d->user->email;
      $filter['amount'] = $d->amount == '' ? null :$d->amount;
      $filter['invoice_id'] = $d->invoice_id == '' ? null : $d->invoice_id;
      $filter['coupon'] = $d->coupon == '' ? null :$d->coupon;
      $filter['pageIndex'] = 0;
      $data = AdminService::getPaymentsData( $filter ,1);
      $results['headers'] = $data['headers'];
      $arr = [];
      foreach ($data['data'] as $var) {
          $temp = [$var->id ,
            $var->user_id , 
            isset($var->amount) ?$var->amount: '' ,
            isset($var->invoice_id) ?$var->invoice_id : '' ,
            isset($var->coupon) ? $var->coupon : '',
            $var->created_at
          ];
          $arr[] = $temp;
      }
      $results['data'] = $arr;
      if ($type == 'xlsx')
        return ExcelService::exportExcel($results);
      else 
        return ExcelService::exportCSV($results);
    }

    public function getPaymentsCreate(){
      $users = User::select('id','name','email')->get();
      return view('admin.payments.create')->withUsers($users);
    }

    public function setPaymentCreate(Request $request){
      AdminService::createUpdatePayments($request);
      return redirect(route('payments-admin'));
    }

    public function getPaymentsEdit($id){
      $payment = Payment::find($id);
      $users = User::select('id','name','email')->get();
      return view('admin.payments.edit')->withUsers($users)->withPayment($payment);
    }

    public function setPaymentEdit(Request $request){
      AdminService::createUpdatePayments($request);
      return redirect(route('payments-admin'));
    }

    public function deletePayment($id){
      Payment::find($id)->delete();
      return 1;
    }


    //Teacher Fees

    public function getTeacherFeesPage(){
      return view('admin.teachers.index');
    }

    public function getTeacherFeesData(Request $request){
      $filter = $request->filter;
      return AdminService::getTeacherFeesData($filter);
    }

    public function destroyTeacher($id){
      TeacherFees::find($id)->delete();
      return 1;
    }

    public function getTeacherCreate(){
      $users = User::select('id','name','email')->get();
      return view('admin.teachers.create')->withUsers($users);
    }

    public function setTeacherCreate(Request $request){
      AdminService::createUpdateTeacher($request);
      return redirect(route('teacher-admin'));
    }

    public function getTeacherEdit($id){
      $teacher = TeacherFees::find($id);
        $users = User::select('id','name','email')->get();
      return view('admin.teachers.edit')->withTeacher($teacher)->withUsers($users);
    }

    public function setTeacherEdit(Request $request){
      AdminService::createUpdateTeacher($request);
      return redirect(route('teacher-admin'));
    }

    public function getTeacherBillingPage(){
      $users = User::where('teacher' ,'=', 1)->get();
      return view('admin.teachers.teacher-billing')->withUsers($users); 
    }

    public function getTeacherBillingData(Request $request){
      return TeacherBillingService::calculateTeacherBilling($request->teacher_id , $request->from_date , $request->to_date , $request->aws, $request->single_course);
    }


    //Coupons

    public function getCouponPage(){
      return view('admin.coupons.index');
    }

    public function getCouponData(Request $request){
      $filter = $request->filter;
      return AdminService::getCouponData($filter);
    }

    public function getCouponCreate(){
      $users = User::select('id','name','email')->get();
      $courses = Course::select('id' , 'title_en')->get();
      $groups = Group::all();
      return view('admin.coupons.create')->withUsers($users)->withCourses($courses)->withGroups($groups);
    }
    public function setCouponCreate(Request $request){
      AdminService::createUpdateCoupon($request);
      return redirect(route('coupon-admin'));
    }

    public function getCouponEdit($id){
      $coupon = Coupon::find($id);
      $groups = Group::all();
      $users = User::select('id', 'name', 'email')->get();
      $courses = Course::select('id', 'title_en')->get();
      return view ('admin.coupons.edit')->withUsers($users)->withCoupon($coupon)->withCourses($courses)->withGroups($groups); 
    }

    public function setCouponEdit(Request $request){
      AdminService::createUpdateCoupon($request);
      return redirect(route('coupon-admin'));
    }

    public function destroyCoupon($id){
      Coupon::find($id)->delete();
      return 1;
    }


    //  Subscription Model

    public function getSubscriptionPage(){
      return view('admin.subscription-model.index');
    }
    
    public function getSubscriptionData(Request $request){
      $filter = $request->filter;
      return AdminService::getSubscriptionData($filter);
    }

    public function getSubscriptionCreate(){
      return view('admin.subscription-model.create');
    }

    public function setSubscriptionCreate(Request $request){
      AdminService::createUpdateSubscription($request);
      return redirect(route('subscription-admin'));
    }
    
    public function getSubscriptionEdit($id){

      $subscription = SubscriptionModel::find($id);
      return view('admin.subscription-model.edit')->withSubscription($subscription);
    }
    
    public function setSubscriptionEdit(Request $request){
      AdminService::createUpdateSubscription($request);
      return redirect(route('subscription-admin'));
    }
    
    public function destroySubscription($id){
      SubscriptionModel::find($id)->delete();
      return 1;
    }


    //Stripe Customes 

    public function getStripePage(){
      return view('admin.stripe.index');
    }
    
    public function getStripeData(Request $request){
      $filter = $request->filter;
      return AdminService::getStripeData($filter);
    }

    public function getStripeCreate(){
      $users= User::select('id','name','email')->get();
      return view('admin.stripe.create')->withUsers($users);
    }

    public function setStripeCreate(Request $request){
      AdminService::createUpdateStripe($request);
      return redirect(route('stripe-admin'));
    }

    public function getStripeEdit($id){
      $users = User::select('id','name','email')->get();
      $stripe = StripeCustomer::find($id);
      return view('admin.stripe.edit')->withUsers($users)->withStripe($stripe);
    }
    
    public function setStripeEdit(Request $request){
      AdminService::createUpdateStripe($request);
      return redirect(route('stripe-admin'));
    }

    public function destroyStripe($id){
      StripeCustomer::find($id)->delete();
      return 1;
    }

  
    //Stripe Plans

    public function getStripePlanPage(){
      return view('admin.stripe-plans.index');
    }
    
    public function getStripePlanData(Request $request){
      $filter = $request->filter;
      return AdminService::getStripePlanData($filter);
    }

    public function getStripePlanCreate(){
      $courses = Course::get();
      $subscriptions = SubscriptionModel::get();
      return view('admin.stripe-plans.create')->withCourses($courses)->withSubscriptions($subscriptions);
    }

    public function setStripePlanCreate(Request $request){
      AdminService::createUpdateStripePlan($request);
      return redirect(route('stripe-plan-admin'));
    }

    public function getStripePlanEdit($id){
      $plan = StripePlan::find($id);
      $courses = Course::get();
      $subscriptions = SubscriptionModel::get();
      return view('admin.stripe-plans.edit')->withPlan($plan)->withCourses($courses)->withSubscriptions($subscriptions);
    }

    public function setStripePlanEdit(Request $request){
      AdminService::createUpdateStripePlan($request);
      
      return redirect(route('stripe-plan-admin'));
    }

    public function destroyStripePlan($id){
      StripePlan::find($id)->delete();
      return 1;
    }


    //Payment Methods

    public function getPaymentMethodPage(){
      return view('admin.payment-methods.index');
    }

    public function getPaymentMethodsData(Request $request){
      $filter = $request->filter;
      return AdminService::getPaymentMethodsData($filter);
    }

    public function getPaymentMethodCreate(){
      return view('admin.payment-methods.create');
    }
    
    public function setPaymentMethodCreate(Request $request){
      AdminService::createUpdateMethods($request);
      return redirect(route('payment-methods-admin'));
    }
    
    public function getPaymentMethodEdit($id){
      $method = PaymentMethod::find($id);
      return view('admin.payment-methods.edit')->withMethod($method);
    }

    public function setPaymentMethodEdit(Request $request){
      AdminService::createUpdateMethods($request);
      return redirect(route('payment-methods-admin'));
    }

    public function destroyPaymentMethod($id){
      PaymentMethod::find($id)->delete();
      return 1;
    }

    // Abandoned Carts


    public function getAbandondeCartPage(){
      return view('admin.abandoned-carts.index');
    }

    public function  getAbandonedCartsData(Request $request){
      $filter = $request->filter;
      return AdminService::getAbandondeCartData($filter);
    }

    public function getAbandondeCartCreate(){
      $courses = Course::get();
      $subscriptions = SubscriptionModel::get(); 
      $methods = PaymentMethod::get();
      $users = User::select('id','name','email')->get();
      return view('admin.abandoned-carts.create')->withCourses($courses)->withSubscriptions($subscriptions)->withMethods($methods)->withUsers($users); 
    }
    
    public function setAbandonedCartCreate(Request $request){
      AdminService::createUpdateCarts($request);
      return redirect(route('abandoned-carts-admin'));
    }

    public function getAbandondeCartEdit($id){
      $courses = Course::get();
      $subscriptions = SubscriptionModel::get(); 
      $methods = PaymentMethod::get();
      $users = User::select('id','name','email')->get();
      $cart = AbandonedCart::find($id);
      return view('admin.abandoned-carts.edit')->withCourses($courses)->withSubscriptions($subscriptions)->withMethods($methods)->withUsers($users)->withCart($cart);
    }

    public function setAbandonedCartEdit(Request $request){
      AdminService::createUpdateCarts($request);
      return redirect(route('abandoned-carts-admin'));
    }

    public function destroyCart($id){
      AbandonedCart::find($id)->delete();
      return 1;
    }
      //Slugs 

    public function getSlugsIndex(){  
        return view('admin.slugs.index');
    }
    public function getSlugsData(Request $request){
      $filter = $request->filter;
      return AdminService::getSlugsData($filter);
    }
    public function getSlugsCreate(){
      return view('admin.slugs.create');
    }
    public function setSlugsCreate(Request $request){
      AdminService::createUpdateSlugs($request);
      return redirect(route('slugs-admin'));
    }
    public function getSlugsEdit($id){
      $slug = Slug::find( $id);
      return view('admin.slugs.edit')->withSlug($slug);
    }
    public function setSlugsEdit(Request $request){
      AdminService::createUpdateSlugs($request);
      return redirect(route('slugs-admin'));
    }
    public function destroySlug($id){
      Slug::find($id)->delete();
      return 1;
    }
    public function uploadImage(Request $request){
      return response()->json([
          "uploaded" => 1,
          "fileName"=> $request->file('upload')->getClientOriginalName(),
          "url"=> S3Service::uploadImageToS3($request->file('upload'))
          ]);
    }

    public function getStarsTextEdit(){
      $para = StarsText::find(1);
      return view('admin.stars.edit')->withPara($para);
    } 
    public function setStarsTextEdit(Request $request){
      AdminService::editStarsText($request);
      return redirect(route('stars-text-admin'));
    }
    public function getFooterContentpage(){
      $footer = Footer::find(1);
      return view('admin.footer.index')->withFooter($footer);
    }
    public function setFooterContent(Request $request){
      AdminService::createUpdateFooterContent($request);
      return redirect(route('footer-admin'));
    }



    //Emails

  public function getEmailsPage(){
    return view('admin.emails.index');
  }
  public function getEmailsData(Request $request){
    $filter = $request->filter;
    return AdminService::getEmailsData($filter);
  }
  public function destroyEmail($id){
    Email::find($id)->delete();
    return 1;
  }


  //Groups 
  public function getGroupPage(){
    return view('admin.groups.index');  
  }
  
  public function getGroupsData(Request $request){
    $filter = $request->filter;
    return AdminService::getGroupsData($filter);
  }
  
  public function getGroupCreatePage(){
    return view('admin.groups.create');
  }
  
  public function setGroupCreate(Request $request){
    $res = AdminService::createUpdateGroup($request);
    if ($res == 'error')
    {
      session()->flash('error' , 'There is an error in the file, Please re-check the structure');
      return redirect(route('groups-create'));
    }
    return redirect(route('groups-admin'));
  }

  public function getGropuDetailsPage($id){
    // $group= Group::find($id);
    return view('admin.groups.details');
  }



  public function getGroupDetailsData($id , Request $request){
    $filter = $request->filter;
    return AdminService::getGroupData($id ,$filter );
  }

  public function removeMemberFromGroup($Gid, $Uid){
    GroupUser::where('group_id', $Gid)->where('user_id' , $Uid)->get()->first()->delete();
    return 1;
  }

  public function getGroupEditPage($id){
    $group = Group::find($id);
    return view('admin.groups.edit')->withGroup($group);
  }

  public function setGroupEdit($id , Request $request){
    AdminService::createUpdateGroup($request);
    return redirect(route('groups-admin'));
  }
 
  public function destroyGroup($id){
    Group::find($id)->delete();
    return 1;
  } 

  public function getGroupAddMembersPage($id){
    $group = Group::find($id);
    return view('admin.groups.add-users')->withGroup($group);
  }

  public function addGroupMembers($id ,Request $request){
    $data =AdminService::importGroupUsers($id ,$request);
    //dd($data);
    return view('admin.groups.table-template')->withUsers($data);
  }
  public function addMenualGroupMembers($id ,Request $request){
    AdminService::addMenualGroupUsers($id ,$request);
    return redirect(route('groups-admin'));
  }

 // Reports 
  public function getSubscriptionReportPage(){
    return view('admin.reports.subscription');
  }
  public function getSubscriptionReportPDF(Request $request){
    $model = $request->model;
    $year = $request->year;
    $month = 0;
    $filter = $request->filter;

    $filter = (array)json_decode($request->filter);
    $filter['user'] = (array) $filter['user'];
    $filter['payment'] = (array) $filter['payment'];
    $filter['course'] = (array) $filter['course'];
    if (isset($request->month)){
      $month = $request->month;
    }

    $data = SubscriptionService::getSubscriptionMonthsReportGrid($model , $month,$year , $filter,1);
    $results['headers'] = $data['headers'];
    $arr = [];
    foreach ($data['data'] as $var) {
        $temp = [$var->id ,
          $var->user->email ,
          isset($var->payment->amount) ?$var->payment->amount: ''  ,
          isset($var->course->title_en) ?$var->course->title_en : ''  ,
          $var->subscriptionModel->name_en,
          $var->created_at
        ];
        $arr[] = $temp;
    }
    $results['data'] = $arr;
    return PDFService::exportFunction($results);
  }
  public function getSubscriptionReportExcel(Request $request){
    $model = $request->model;
    $year = $request->year;
    $month = 0;
    $filter = (array)json_decode($request->filter);
    $filter['user'] = (array) $filter['user'];
    $filter['payment'] = (array) $filter['payment'];
    $filter['course'] = (array) $filter['course'];
    $type = $request->type;
    if (isset($request->month)){
      $month = $request->month;
    }

    $data = SubscriptionService::getSubscriptionMonthsReportGrid($model , $month,$year , $filter ,1);
    $results['headers'] = $data['headers'];
    $arr = [];
    foreach ($data['data'] as $var) {
        $temp = [$var->id ,
          $var->user->email ,
          isset($var->payment->amount) ?$var->payment->amount: ''  ,
          isset($var->course->title_en) ?$var->course->title_en : ''  ,
          $var->subscriptionModel->name_en,
          $var->created_at
        ];
        $arr[] = $temp;
    }
    $results['data'] = $arr;
    if ($type == 'xlsx')
      return ExcelService::exportExcel($results);
    else 
      return ExcelService::exportCSV($results);
  }

  public function getSubscriptionReport(){
    return SubscriptionService::getSubscriptionReports();
  }  
  
  public function getSubscriptionReportsForChart(Request $request){
    return SubscriptionService::getSubscriptionReportsForChart($request);
    
  }
  
  public function getSubscriptionMonthFilterReport(Request $request){
    return SubscriptionService::getSubscriptionMonthFilterReport($request);
  }
  
  public function getIncomeReport(Request $request){
    return PaymentService::getIncomeReport($request);
  }
  
  public function getSubscriptionMonthsGrid($Su , $Mo,$Ye , Request $request){
    $filter = $request->filter;
    return SubscriptionService::getSubscriptionMonthsReportGrid($Su , $Mo , $Ye , $filter);
  }
  public function getIncomeData(Request $request){
    $filter = $request->filter;
    $month = $filter['month'];
    $year = $filter['year'];
    $subscription = $filter['subscription'];
    return PaymentService::getIncomeReportGrid($month , $year , $subscription,$filter);
  }

  public function getIncomeDataPDF(Request $request){
    $months = explode(',', $request->month);
    $year = $request->year;
    $filter = (array)json_decode($request->filter);
    $filter['user'] = (array) $filter['user'];
    $data = PaymentService::getIncomeReportGrid($months , $year , $request->subscription , $filter , 1);
    $results['headers'] = $data['header'];

    $arr = [];
    foreach ($data['data'] as $var) {
        $temp = [$var->id ,
          $var->user->email ,
          isset($var->amount) ?$var->amount: ''  ,
          isset($var->subscriptionModel->name_en) ? $var->subscriptionModel->name_en : '',
          $var->created_at
        ];
        $arr[] = $temp;
    }
    $results['data'] = $arr;
    return PDFService::exportFunction($results);
  }
  public function getIncomeDataExcel (Request $request){
    $months = explode(',', $request->month);
    $year = $request->year;
    $type = $request->type;
    $filter = (array)json_decode($request->filter);
    $filter['user'] = (array) $filter['user'];
    $data = PaymentService::getIncomeReportGrid($months , $year , $request->subscription ,$filter, 1);
    $results['headers'] = $data['header'];
    $arr = [];
    foreach ($data['data'] as $var) {
        $temp = [$var->id ,
          $var->user->email ,
          isset($var->amount) ?$var->amount: ''  ,
          isset($var->subscriptionModel->name_en) ? $var->subscriptionModel->name_en : '',
          $var->created_at
        ];
        $arr[] = $temp;
    }
    $results['data'] = $arr;
    if ($type == 'xlsx')
      return ExcelService::exportExcel($results);
    else 
      return ExcelService::exportCSV($results);
    
  }

  //Laboratory 
  public function getKitsPage(){
    return view('admin.lab.kits.kits');
  }

  public function getKitData(Request $request){
    $filter = $request->filter;
    return AdminService::getKitData($filter);
  }
  
  public function getKitItemsPage($id){
    return view('admin.lab.kits.kit-items')->withId($id);
  }

  public function getKitItemsData($id , Request $request){
    $filter = $request->filter;
    return AdminService::getKitItemsData($filter, $id);
  }

  public function getAddItemPage($id){
    $kititems = KitItem::select('item_id')->where('kit_id' , $id)->get()->toArray();
    $items = Item::whereNotIn('id' , $kititems)->get();
    return view('admin.lab.kits.add')->withItems($items)->withId($id);
  }

  public function getCreateKitPage(){
    return view('admin.lab.kits.create');
  }
  
  public function setCreateKit(Request $request){
    AdminService::createUpdateKit($request);
    return redirect(route('kits-admin'));
  }

  public function getEditKitPage($id){
    $kit = Kit::find($id);
    return view('admin.lab.kits.edit')->withKit($kit);
  } 

  public function setKitEdit($id , Request $request){
    AdminService::createUpdateKit($request);
    return redirect(route('kits-admin'));
  }
  public function setAddKitItems($id,Request $request){
    AdminService::addItems($id , $request);
    return redirect(route('kit-items' , $id));
  }
  public function destroyKit($id){
    KitItem::where('kit_id' ,$id )->delete();
    Kit::find($id)->delete();
    return 1;
  } 
  public function removeItemFormKit($Kid , $Iid){
    KitItem::where('kit_id' , $Kid)->where('item_id' , $Iid)->delete();
    return 1; 
  }

  public function getSuppliersPage(){
    $users = User::select('id' , 'email' , 'name')->where('supplier' ,'!=' , 1)->get();
    return view('admin.lab.suppliers')->withUsers($users);
  } 

  public function getItemsPage (){
    return view('admin.lab.items.index');
  }
  
  public function getCreateItemPage(){
    return view('admin.lab.items.create');
  }
  
  public function setCreateItem(Request $request){
    AdminService::createUpdateItem($request);
    return redirect(route('items-admin'));
  }

  public function getEditItemPage($id){
    $item = Item::find($id);
    return view('admin.lab.items.edit')->withItem($item);
  }

  public function setEditItem($id , Request $request){
    AdminService::createUpdateItem($request);
    return redirect(route('items-admin'));
  }

  public function destroyItem($id){
    Item::find($id)->delete();
    $kititem = KitItem::where('item_id',$id)->get();
    if(count($kititem)>0 ){
        $kititem->delete();
    }
    return 1;
  } 

  public function getItemsData(Request $request){
    $filter = $request->filter;
    return AdminService::getItemsData($filter);
  }

  public function getSuppliersData(Request $request){
    $filter = $request->filter;
    return AdminService::getSuppliersData($filter);
  }
  
  public function  addSupplier(Request $request){
    AdminService::addSupplier($request);
    return redirect(route('suppliers-admin'));
  }

  public function getSupplierItemsPage(){
    return view('admin.lab.supplier-items');
  }

  public function getSupplierItemData($id , Request $request){
    $filter = $request->filter;
    return AdminService::getSuppliersDetails($id, $filter);
  }

  public function getOrdersPage(){
    return view('admin.lab.created-orders.orders');
  }
  public function getOrdersData(Request $request){
    $filter = $request->filter;
    return AdminService::getOrdersData($filter);
  }

  public function getOrderItemsPage ($id){
    $orderItems = OrderItem::where('order_id' , '=' , $id)
    ->with(['supplierItem'=> function($query){
      $query->with('user');
    }])
    ->with('item');

    $orderItems =$orderItems->get();
    return view('admin.lab.created-orders.order-items')->withOrderItems($orderItems);
  }

  public function getOrderItemsData($id , Request $request){
    $filter = $request->filter;
    return AdminService::getOrderItemsData($id , $filter);
  }

  public function ConfirmOrder($id , Request $request){
    $price = 0 ;
    // foreach ($request->ids as $key => $value) {
    //   $orderItem = OrderItem::find($value);
    //   $suplierItem = SupplierItem::find($request["suplier_item_$value"]);
    //   $orderItem->price = $suplierItem->price;
    //   $price += $suplierItem->price;
    //   $suplierItem->qty = $suplierItem->qty - 1;
    //   $suplierItem->save();
    //   $orderItem->save();
    // }
    $order = Order::find($id);
    $order->state = OrderStates::CONFIRMED;
    $order->updated_by = Auth::id();
    $order->save();
    return redirect(route('orders-admin'));
  }

  public function getAllOrdersPage(){
    return view('admin.lab.orders'); 
  } 

  public function getAllOrdersData(Request $request){
    $filter = $request->filter;
    return AdminService::getAllOrdersData($filter);
  }

  public function getAllOrdersPDF(Request $request){
    $filter = json_decode($request->filter);
    $filter = (array) $filter;
    $filter['user'] = (array)$filter['user'] ;
    $filter['kit']= (array) $filter['kit'];
    $filter['updated_by'] = (array) $filter['updated_by'];
    $data = AdminService::getAllOrdersData($filter , 1);
    $results['headers'] = $data['header'];
    $arr = [] ;
    foreach ($data['data'] as $data) {
      $temp =[];
      $temp[] = $data['id'];
      $temp[] = $data['user']['email'];
      $temp[] = $data['kit']['name_en'];
      $temp[] = $data['total_price'];
      $temp[] = $data['selling_price'];
      $temp[] = $data['state'];
      $temp[] = $data['updatedBy']['email'];
      $arr[] = $temp;
    }

    $results['data']= $arr;
    return PDFService::exportFunction($results);  
  }
  public function getAllOrdersExcel(Request $request ){
    $filter = json_decode($request->filter);
    $filter = (array) $filter;
    $filter['user'] = (array)$filter['user'] ;
    $filter['kit']= (array) $filter['kit'];
    $filter['updated_by'] = (array) $filter['updated_by'];
    $data = AdminService::getAllOrdersData($filter , 1);
    $results['headers'] = $data['header'];
    $arr = [] ;
    foreach ($data['data'] as $data) {
      $temp =[];
      $temp[] = $data['id'];
      $temp[] = $data['user']['email'];
      $temp[] = $data['kit']['name_en'];
      $temp[] = $data['total_price'];
      $temp[] = $data['selling_price'];
      $temp[] = $data['state'];
      $temp[] = $data['updatedBy']['email'];
      $arr[] = $temp;
    }
    $results['data']= $arr;
    $type = $request->type;
    if ($type == 'xlsx')
      return ExcelService::exportExcel($results);  
    else
      return ExcelService::exportCSV($results);  

  }

  public function getOrderDetails($id){
    $order = Order::find($id);
    $orderItems = OrderItem::select('id', 'item_id', 'price','qty','final_total' ,'type', 'selling_price' ,'supplier_id')->where('order_id' , $id)->with('kitItem')->with('kit')->with('course')->with('supplier')->orderBy('id', 'desc')->get();
    
    foreach ($orderItems as $value) {
      if($value->type == 'Course'){
        $value->itemname = $value->course->title_en;
      }elseif($value->type == 'Kit'){
        $value->itemname = $value->kit->name_en;
      }elseif($value->type == 'Item'){
        $value->itemname = $value->kitItem->items->name_en;
      }
    }
    //dd($orderItems);
    return view('admin.lab.order-details')->withOrder($order)->withOrderItems($orderItems);;
  }
    public function getOrderinvoicePage($id){
    $order = Order::find($id);
    $orderItems = OrderItem::select('id', 'item_id', 'price','qty','final_total' ,'type', 'selling_price' ,'supplier_id')->where('order_id' , $id)->with('kitItem')->with('kit')->with('course')->with('supplier')->orderBy('id', 'desc')->get();
    
    foreach ($orderItems as $value) {
      if($value->type == 'Course'){
        $value->itemname = $value->course->title_en;
      }elseif($value->type == 'Kit'){
        $value->itemname = $value->kit->name_en;
      }elseif($value->type == 'Item'){
        $value->itemname = $value->kitItem->items->name_en;
      }
    }
    //dd($orderItems);
    return view('admin.lab.order-invoice')->withOrder($order)->withOrderItems($orderItems);;
  }
  public function getOrderDetailsData($id , Request $request){
    $filter = $request->filter;
    return AdminService::getOrderDetailsData($id,$filter);
  }

  //Emails Subscription

  public function getEmailSubscriptionsPage()
  {
    return view('admin.email-subscriptions.index');
  }

  public function getEmailSubscriptionsData(Request $request){
    $filter = $request->filter;
    return MailService::getEmailsData($filter);
  }

  public function createEmailSubscription(Request $request){
    EmailBroadcast::create([
      'from' => $request->from,
      'subject' => $request->subject,
      'template' => 'some-template'
    ]);
    return redirect(route('admin-email-subscription'));
  }

  public function updateEmailSubscription(Request $request){
    EmailBroadcast::where('id' , $request->id)->update([
      'from' => $request->from,
      'subject' => $request->subject, 
      'template' => 'some-template'
    ]);
    return redirect(route('admin-email-subscription'));
  }

  public function deleteEmailSubscription($id){
    $email = EmailBroadcast::where('id',$id)->delete();
    return 1;
  }

  public function getSendEmailSubscriptionPage($id){
    $emailSubscription = EmailBroadcast::find($id);
    $users = User::select('email','id')->get();
    return view('admin.email-subscriptions.send')->withEmailSubscription($emailSubscription)->withUsers($users);
  }

  public function sendEmailSubscription(Request $request)
  {
    $emailSubscription = EmailBroadcast::find($request->subs_id);
    $content = $request->content;
    $users = $request->users;
    foreach ($users as $user) {
      // MailService::send($emailSubscription->template , $content , $emailSubscription->from , $user ,$emailSubscription->subject );
    }
    return redirect(route('admin-email-subscription'));
  }


  
  public function getGalleryPage()
  {
    return view('admin.gallery.index');
  }

  public function getGalleryData(Request $request)
  {
    $filter=  $request->filter;
    return AdminService::getGalleryData($filter);
  }

  public function getGalleryCreatePage()
  {
    $albums = Album::get();
    return view('admin.gallery.create')->withAlbums($albums);
  }

  public function createGallery(Request $request)
  {
    AdminService::createUpdateGallery($request);
    return redirect(route('admin-gallery'));
  }

  public function getGalleryEditPage(Request $request , $id)
  {
    $albums = Album::get();
    $item = GalleryItem::find($id);
    return view('admin.gallery.edit')->withItem($item)->withAlbums($albums);;
  }

  public function updateGallery(Request $request , $id)
  {
    AdminService::createUpdateGallery($request);
    return redirect(route('admin-gallery'));
  }

  public function deleteGallery($id)
  {
    $gallery = GalleryItem::find($id);
    $gallery->delete();
    return 1;
  }


  public function getAlbumsPage()
  {
    return view('admin.albums.index');
  }

  public function getAlbumsData(Request $request)
  {
    $filter=  $request->filter;
    return AdminService::getAlbumsData($filter);
  }

  public function getAlbumsCreatePage()
  {
    return view('admin.albums.create');
  }

  public function createAlbums(Request $request)
  {
    AdminService::createUpdateAlbum($request);
    return redirect(route('admin-albums'));
  }

  public function getAlbumsEditPage(Request $request , $id)
  {
    $album = Album::find($id);
    return view('admin.albums.edit')->withAlbum($album);;
  }

  public function updateAlbums(Request $request , $id)
  {
    AdminService::createUpdateAlbum($request);
    return redirect(route('admin-albums'));
  }

  public function deleteAlbums($id)
  {
    $images =GalleryItem::where('album_id' , $id)->get();
    if(count($images) > 0){
        $images->delete();
    } 
    $album = Album::find($id);
    $album->delete();
    return 1;
  }




  public function getAchievementsPage()
  {
    return view('admin.achievements.index');
  }

  public function getAchievementsData(Request $request)
  {
    $filter=  $request->filter;
    return AdminService::getAchievementsData($filter);
  }

  public function getAchievementsCreatePage()
  {
    return view('admin.achievements.create');
  }

  public function createAchievements(Request $request)
  {
    AdminService::createUpdateAchievements($request);
    return redirect(route('admin-achievements'));
  }

  public function getAchievementsEditPage(Request $request , $id)
  {
    $achievement = Achievement::find($id);
    return view('admin.achievements.edit')->withAchievement($achievement);;
  }

  public function updateAchievements(Request $request , $id)
  {
    AdminService::createUpdateAchievements($request);
    return redirect(route('admin-achievements'));
  }

  public function deleteAchievements($id)
  {
    $achievement = Achievement::find($id);
    $achievement->delete();
    return 1;
  }

  // Testimonials

    public function getTestimonialsPage(){
      return view('admin.testimonials.index');
    }

    public function getTestimonialsData(Request $request){
      $filter = $request->filter;
      return AdminService::getTestimonialsData($filter);
    }

    public function getTestimonialCreate(){
      $course = Course::get();
      return view('admin.testimonials.create')->withCourse($course);
    }

    public function setTestimonialCreateUpdate(Request $request){
      AdminService::createUpdateTestimonial($request);
      return redirect(route('testimonials-admin'));
    } 

    public function getTestimonialEdit($id){
      $testimonial = Testimonial::find($id);
      $course = Course::get();
      return view('admin.testimonials.edit')->withTestimonial($testimonial)->withCourse($course);
    }
    public function destroyTestimonial($id){
      $level = Testimonial::find($id);
      $level->delete();
      return 1;
    }

    // Vision

    public function getVisionPage(){
      return view('admin.vision.index');
    }

    public function getVisionData(Request $request){
      $filter = $request->filter;
      return AdminService::getVisionData($filter);
    }

    public function getVisionCreate(){
      
      return view('admin.vision.create');
    }

    public function setVisionCreateUpdate(Request $request){
      AdminService::createUpdateVision($request);
      return redirect(route('vision-admin'));
    } 

    public function getVisionEdit($id){
      $vision = Vision::find($id);
     
      return view('admin.vision.edit')->withVision($vision);
    }
    public function destroyVision($id){
      $level = Vision::find($id);
      $level->delete();
      return 1;
    }  


// banner images

    public function getBannerimagesPage(){
      return view('admin.bannerimages.index');
    }

    

     public function getBannerimagesData(Request $request){

   
      $filter = $request->filter;

      return AdminService::getBannerimagesData($filter);
    }

      public function getBannerimagesEdit($id){
      $bannerimages = Bannerimages::find($id);
      return view('admin.bannerimages.edit')->withBannerimages($bannerimages);
    }

    public function setBannerimagesCreateUpdate(Request $request){
      AdminService::createUpdateBannerimages($request);
      return redirect(route('banner-images'));
    }
    public function getUpdateorder(Request $request){
      $orderobj = Order::find($request->orderid);
      
      if($request->status == 'paid'){

      $subscriptionModel = SubscriptionModel::find(5);
        
      $coupon = empty(session('coupon')) ? '' : session('coupon') ;
        
      $payment = PaymentService::makePayment($orderobj->total_price,$orderobj->id ,$coupon,  PaymentMethod::MANUALLY, $orderobj->user_id);

      $startDate = date('Y-m-d H:i:s');
     // $endDate = date('Y-m-d H:i:s', strtotime($startDate. ' + ' . $subscriptionModel->period_in_days .' days'));

      $orderobj1 = OrderItem::where('order_id','=',$request->orderid)
      ->where('type','=','Course')->get();   

        foreach ($orderobj1 as $orderlist) {

          if($orderlist->type=='Course'){
          
          $course_ids=$orderlist->item_id;
          $courseobj = Course::find($course_ids);


          $endDate = date('Y-m-d H:i:s', strtotime($startDate. ' + ' . $courseobj->emonth .' months'));

           $subscriptionsobj = Subscription::where('user_id',  '=' , $orderobj->user_id)->where('course_id' ,$orderlist->item_id)->update([
           'status' => 'inactive'
        ]);

        SubscriptionService::subscribe($orderobj->user_id ,$subscriptionModel->id, $payment ,PaymentMethod::MANUALLY , $orderlist->item_id ,$startDate ,$endDate );
        }




       
        }
        OrderService::getsupplierOrdermail($request->orderid);
       }
      $order = Order::find($request->orderid);
      $order->status = $request->status;
      $order->save();
      

      
      return redirect('/admin/order/'.$request->orderid.'/details');
    }
    
     //Email Template
    
    public function getEmailTemplateIndex(){  
        return view('admin.email-template.index');
    }
    public function getEmailTemplateData(Request $request){
      $filter = $request->filter;
      return AdminService::getEmailTemplateData($filter);
    }
    public function getEmailTemplateCreate(){
      return view('admin.email-template.create');
    }
    public function setEmailTemplateCreate(Request $request){
      AdminService::createUpdateEmailTemplate($request);
      return redirect(route('email-template'));
    }
    public function getEmailTemplateEdit($id){
      $emailtemplate = Emailtemplates::find( $id);
      return view('admin.email-template.edit')->withEmailtemplate($emailtemplate);
    }
    public function setEmailTemplateEdit(Request $request){
      AdminService::createUpdateEmailTemplate($request);
      return redirect(route('email-template'));
    }
    public function destroyEmailTemplate($id){
      Emailtemplates::find($id)->delete();
      return 1;
    }
    public function moneycollection(Request $request){
        $time = strtotime("+1 day") *1000;
        $validationRes = AramexService::createMoneyCollectionShipment($request->orderId , $time);
        //dd( $validationRes);
        if (is_string($validationRes)){
            return $validationRes;
        }
        else {
            Order::where('id' , $request->orderId)->update(['state' => OrderStates::ONDELIVERY]);
            return 'ok';
        }

      
    }
    public function paymentreceive(Request $request){
        /*Order::where('id' , $request->orderId)->update(['state' => OrderStates::DELIVERED],'status' => 'paid']);*/
        $orderobj = Order::find($request->orderId);
      
        $subscriptionModel = SubscriptionModel::find(5);
        
        $coupon = empty(session('coupon')) ? '' : session('coupon') ;
        
        $payment = PaymentService::makePayment($orderobj->total_price,$orderobj->id ,$coupon,  PaymentMethod::MANUALLY, $orderobj->user_id);

        $startDate = date('Y-m-d H:i:s');
       // $endDate = date('Y-m-d H:i:s', strtotime($startDate. ' + ' . $subscriptionModel->period_in_days .' days'));

        $orderobj1 = OrderItem::where('order_id','=',$request->orderId)
          ->where('type','=','Course')->get();   

        foreach ($orderobj1 as $orderlist) {

          if($orderlist->type=='Course'){
          
          $course_ids=$orderlist->item_id;
          $courseobj = Course::find($course_ids);


          $endDate = date('Y-m-d H:i:s', strtotime($startDate. ' + ' . $courseobj->emonth .' months'));

           $subscriptionsobj = Subscription::where('user_id',  '=' , $orderobj->user_id)->where('course_id' ,$orderlist->item_id)->update([
           'status' => 'inactive'
        ]);

        SubscriptionService::subscribe($orderobj->user_id ,$subscriptionModel->id, $payment ,PaymentMethod::MANUALLY , $orderlist->item_id ,$startDate ,$endDate );
        }




       
        }
        
        $order = Order::find($request->orderId);
        $order->status = 'paid';
        $order->state = OrderStates::DELIVERED;
        $order->save();
      

      
      
    }
    
    
}

