<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\SubscriptionModel;
use App\Subscription;
use App\lesson;
use App\Session;
use App\Video;
use App\Level;
use App\UserProgress;
use Auth;
use App\Star;
use App\Services\MailService;
use App\Services\CouponService;
use App\StarsText;
use App\Services\SlugService;
use App\Email;
use App\Item;
use App\Slug;
use App\Kit;
use App\Album;
use App\Achievement;
use DB;
use App;
use Session as Sess;
use App\Services\OrderService;
use App\GalleryItem;
use App\Testimonial;
use App\Bannerimages;
use App\Footer;
use App\KitItem;
use App\SupplierItem;
use App\Services\CartService;
use App\Cart;
use App\Cartdetail;
use App\Services\CourseService;
use App\OrderItem;
use App\Helper\Helper;
use App\Services\AramexService;
use App\Order;
use App\Emailtemplates;
use App\Vision;

class ClientController extends Controller
{
    public static function index()
    {
        self::langService();
        $lang = App::getLocale();

        $levels = Level::where('active' ,'=' ,1)->get();

        $models = SubscriptionModel::get();
        $stars = Star::get();
    
        $vision = Vision::where('active' ,'=' ,1)->get();        

        $aboutText = Slug::where('slug', 'like', 'About-Eureka')->first()->text;
        $aboutText = SlugService::cleanIt($aboutText);

        $courses = Course::where('active','!=', 0)->orderBy('id','desc')->take(4)->get();
        foreach ($courses as $value) {
            $value->courseTime = CourseService::getCourseTime($value->id);
        }
        

        $contactText = Slug::where('slug', 'like', 'contact-us')->first()->text;
        $contactText = SlugService::cleanIt($contactText);
        $homebannerText = Slug::where('slug', 'like', 'home-banner')->first()->text;
        $homebannerText = SlugService::cleanIt($homebannerText);
        
      
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
        $starsText = StarsText::find(1);

        $items = Item::with('images')->take(9)->get();
        foreach ($items as $item) {
            $item->maxPrice = OrderService::getMaximumPrice($item->id);
            if (App::getLocale() == 'ar')
                $item->name_en = $item->name_ar;
        
        }

        $kits = Kit::with('items')->with('images')->get();
        // foreach ($kits as $kit) {
        //     $kit->maxPrice = OrderService::getKitSellingPrice($kit->id);
        //     $kit->itemsCount = $kit->items->count();
        //     if (App::getLocale() == 'ar')
        //         $kit->name_en = $kit->name_ar;
        // }
        if ($lang == 'ar'){    
            $starsText->text= $starsText ->text_ar;
            $aboutText = Slug::where('slug', 'like', 'About-Eureka')->first()->text_ar;
            $aboutText = SlugService::cleanIt($aboutText);
            $contactText = Slug::where('slug', 'like', 'contact-us')->first()->text_ar;
            $contactText = SlugService::cleanIt($contactText);
            $homebannerText = Slug::where('slug', 'like', 'home-banner')->first()->text_ar;
            $homebannerText = SlugService::cleanIt($homebannerText);
        
            foreach ($models as $model) {
                $model->name_en = $model->name_ar;
            }
            foreach ($levels as $level) {
                $level->name_en = $level->name_ar;
                 $level->image_path_en = $level->image_path_ar;
            }

             foreach ($courses as $course) {
                $course->title_en = $course->title_ar;
                $course->description_en = $course->description_ar;
                
            }


        }

        
       

         $testimonial = Testimonial::where('active' ,'=' ,1)->where('type' ,'=' ,'Site')->orderBy('id', 'desc')->get();
      //  dd($stars);
        return view('client.pages.index')->withLevels($levels)->withModels($models)->withStars($stars)->withAboutText($aboutText)->withContactText($contactText)->withSlugs($slugs)->withStarsText($starsText)->withFooter($footer)->withItems($items)->withHomebannerText($homebannerText)->withTestimonial($testimonial)->withCourses($courses)->withVision($vision);
    }

    public function subscription(){
        self::langService();
        $lang = App::getLocale();

        $levels = Level::where('active' ,'=' ,1)->get();

        $models = SubscriptionModel::get();
        $stars = Star::get();
        $aboutText = Slug::where('slug', 'like', 'about')->first()->text;
        $aboutText = SlugService::cleanIt($aboutText);
        $contactText = Slug::where('slug', 'like', 'contact-us')->first()->text;
        $contactText = SlugService::cleanIt($contactText);
        $starsArr  = [];
        for($i = 0 ; $i < $stars->count() ; $i++){
            array_push($starsArr, $stars[$i]->image_path);
        }
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
        $starsText = StarsText::find(1);

        $items = Item::with('images')->take(9)->get();
        foreach ($items as $item) {
            $item->maxPrice = OrderService::getMaximumPrice($item->id);
            if (App::getLocale() == 'ar')
                $item->name_en = $item->name_ar;

        }

        $kits = Kit::with('items')->with('images')->get();
        // foreach ($kits as $kit) {
        //     $kit->maxPrice = OrderService::getKitSellingPrice($kit->id);
        //     $kit->itemsCount = $kit->items->count();
        //     if (App::getLocale() == 'ar')
        //         $kit->name_en = $kit->name_ar;
        // }
        if ($lang == 'ar'){
            $starsText->text= $starsText ->text_ar;
            $aboutText = Slug::where('slug', 'like', 'about')->first()->text_ar;
            $aboutText = SlugService::cleanIt($aboutText);
            $contactText = Slug::where('slug', 'like', 'contact-us')->first()->text_ar;
            $contactText = SlugService::cleanIt($contactText);
            foreach ($models as $model) {
                $model->name_en = $model->name_ar;
            }
            foreach ($levels as $level) {
                $level->name_en = $level->name_ar;
            }
        }
        return view('client.pages.subscription')->withLevels($levels)->withModels($models)->withStars($starsArr)->withAboutText($aboutText)->withContactText($contactText)->withSlugs($slugs)->withStarsText($starsText)->withFooter($footer)->withItems($items);
    }

    public function getCoursePage($name){
        self::langService();
        
        $data = Course::with(['session' =>function ($query){
            $query->with(['video' => function ($query){
                $query->where('active' , 1);
            }]);
        }])->where('slug' , '=' , $name)->get()->first();
        if ($data == null )
            return abort(404);
        $id = $data->id;
        if( $data == null||$data->count() == 0 ){
            return abort(404);
        }
        $purchased = 0;
        $freeLessons = Video::where('free' , '=', 1)->where('course_id','=' , $id )->where('active', 1)->limit(2)->get();
        if (Auth::id() != null){
              $user_id = Auth::id();
            foreach ($data->session as $csession) {
                foreach ($csession->video as $video) {
                    if(UserProgress::where('video_id', '=' , $video->id)->where('user_id', '=' , $user_id)->get()->count() != 0 ){
                        $video->done = 1;
                    }

                }
            }
            $subscriptions = Subscription::where('user_id',  '=' , Auth::id())->whereIn('course_id' ,[ $id, 0]);
            if ($subscriptions->count() != 0){
                $purchased =1;
            }
        }
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
        if (App::getLocale() == 'ar'){
            $data->title_en = $data->title_ar;    
            $data->description_en = $data->description_ar;
            $data->overview_en = $data->overview_ar;
            $less_a=0;
            foreach ($data->session as $sess) {
                $sess->session_number = $sess->session_number_ar;
                foreach ($sess->video as $video) {
                    $video->name_en = $video->name_ar;
                    $video->description_en = $video->description_ar;
                    $less_a++;
                }
            }
            foreach ($freeLessons as $video) {
                $video->name_en = $video->name_ar;
                $video->description_en = $video->description_ar;
            }
        }

         $less_a=0;
            foreach ($data->session as $sess) {
              
                foreach ($sess->video as $video) {
                   
                    $less_a++;
                }
            }
            $resume="";
            foreach ($data->session as $sess) {
            if($sess->done == null)
            {
                 $resume=$sess->id;
                 break;
            }   
            }


          
          
                $items = Item::with('images')->take(9)->get();
        foreach ($items as $item) {
            $item->maxPrice = OrderService::getMaximumPrice($item->id);
            if (App::getLocale() == 'ar')
                $item->name_en = $item->name_ar;
        
        }

        $kits = Kit::with('items')->with('images')->get();
       //  foreach ($kits as $kit) {
       //      $kit->maxPrice = OrderService::getKitSellingPrice($kit->id);
       //      $kit->itemsCount = $kit->items->count();
       //      if (App::getLocale() == 'ar')
       //          $kit->name_en = $kit->name_ar;
       // }
         $testimonial = Testimonial::where('active' ,'=' ,1)->where('type' ,'=' ,'Course')->where('course_id' ,'=' ,$data->id)->orderBy('id', 'desc')->get();
        return view('client.pages.level')->withCourse($data)->withFreeLessons($freeLessons)->withPurchased($purchased)->withSlugs($slugs)->withFooter($footer)->withItems($items)->withKits($kits)->withTestimonial($testimonial)->withLessoncount($less_a)->withResume($resume);
    }
 
    public function frontcontactUs(Request $request){



        $footer = Footer::find(1);
        $admin_email = $footer->email;

        $message = $request->message;
        $from = $request->email;
        $subject = $request->subject;
        $name = $request->name;







        $email = Email::create(['name'=> $name , 'subject'=> $subject , 'email' => $from  , 'text' => $message]);

        $EmailTemplatesObj = Emailtemplates::find(5);
        $msg=nl2br($EmailTemplatesObj->email_temp_body);
        $msg=str_replace("<FULLNAME>",$name,$msg);
        $msg=str_replace("<EMAIL>",$from,$msg);
        $msg=str_replace("<MESSAGE>",$message,$msg);

        
        /* Start admin mail */
        MailService::send('email.template',$data=['msg'=>$msg], $admin_email, $admin_email, $EmailTemplatesObj->email_temp_subject);
        MailService::send('email.template',$data=['msg'=>$msg], $admin_email, $admin_email='siddhraj@webmynesystems.com', $EmailTemplatesObj->email_temp_subject);


        // MailService::send('template' , [$msg] ,$from , 'info@eureka.com' , $subject);
        return json_encode(['response'=>'ok']);
    }   

    public function levelPage($levelName){
        self::langService();

        $levelall = Level::where('active' ,'=' ,1)->get();



        $level = Level::where('slug', 'like', $levelName)->first();

        if (Auth::id() != null){
        $user = Auth::user();
        $subscriptoins = Subscription::where('user_id', '=', $user->id)->get();
        
        $courses_ids = [];

        foreach ($subscriptoins as $subs) {
            array_push($courses_ids,$subs->course_id );
        }

        $courses = Course::where('level_id', '=', $level->id)->where('active','!=', 0)->whereIn('id',$courses_ids)->get();
            foreach ($courses as $value) {
                $value->courseTime = CourseService::getCourseTime($value->id);
            }
        $othercourses = Course::where('level_id', '=', $level->id)->where('active','!=', 0)->whereNotIn      ('id',$courses_ids)->get();  
            foreach ($othercourses as $value) {
                $value->courseTime = CourseService::getCourseTime($value->id);
            }
        }else{
            $courses = Course::where('level_id', '=', $level->id)->where('active','!=', 0)->get();
            foreach ($courses as $value) {
                $value->courseTime = CourseService::getCourseTime($value->id);
            }
            $othercourses = Course::where('level_id', '=', $level->id)->where('active','!=', 0)->get();
            foreach ($othercourses as $value) {
                $value->courseTime = CourseService::getCourseTime($value->id);
            }  
        }



       
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
        $items = Item::with('images')->take(9)->get();
        foreach ($items as $item) {
            $item->maxPrice = OrderService::getMaximumPrice($item->id);
            if (App::getLocale() == 'ar')
                $item->name_en = $item->name_ar;
        
        }

        $kits = Kit::with('items')->with('images')->get();
        // foreach ($kits as $kit) {
        //     $kit->maxPrice = OrderService::getKitSellingPrice($kit->id);
        //     $kit->itemsCount = $kit->items->count();
        //     if (App::getLocale() == 'ar')
        //         $kit->name_en = $kit->name_ar;

        // }        
        if (App::getLocale() == 'ar'){
            foreach ($courses as $course) {
                $course->title_en = $course->title_ar;
            }

            foreach ($othercourses as $course) {
                $course->title_en = $course->title_ar;
            }

            $level->name_en=$level->name_ar;
            $level->description_en=$level->description_ar;




            foreach ($levelall as $level) {
                $level->name_en = $level->name_ar;
                // $level->image_path_en = $level->image_path_ar;
            }
        }
        return view('client.pages.level-courses')->withSlugs($slugs)->withCourses($courses)->withFooter($footer)->withItems($items)->withKits($kits)->withLevels($level)->withLevelall($levelall)->withOthercourses($othercourses);
    }

    

        public function coursesPage(){
        self::langService();

        $levelall = Level::where('active' ,'=' ,1)->get();



       

        if (Auth::id() != null){
        $user = Auth::user();
        $subscriptoins = Subscription::where('user_id', '=', $user->id)->get();
        
        $courses_ids = [];

        foreach ($subscriptoins as $subs) {
            array_push($courses_ids,$subs->course_id );
        }

        $courses = Course::where('active','!=', 0)->whereIn('id',$courses_ids)->get();

            foreach ($courses as $value) {
                $value->courseTime = CourseService::getCourseTime($value->id);
            }

        $othercourses = Course::where('active','!=', 0)->whereNotIn      ('id',$courses_ids)->get();  
            foreach ($othercourses as $value) {
                $value->courseTime = CourseService::getCourseTime($value->id);
            }

        }else{
            $courses = Course::where('active','!=', 0)->get();
            foreach ($courses as $value) {
                $value->courseTime = CourseService::getCourseTime($value->id);
            }
            $othercourses = Course::where('active','!=', 0)->get();
            foreach ($othercourses as $value) {
                $value->courseTime = CourseService::getCourseTime($value->id);
            }  
        }



       
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
        $items = Item::with('images')->take(9)->get();
        foreach ($items as $item) {
            $item->maxPrice = OrderService::getMaximumPrice($item->id);
            if (App::getLocale() == 'ar')
                $item->name_en = $item->name_ar;
        
        }

        $kits = Kit::with('items')->with('images')->get();
        // foreach ($kits as $kit) {
        //     $kit->maxPrice = OrderService::getKitSellingPrice($kit->id);
        //     $kit->itemsCount = $kit->items->count();
        //     if (App::getLocale() == 'ar')
        //         $kit->name_en = $kit->name_ar;

        // }        
        if (App::getLocale() == 'ar'){
            foreach ($courses as $course) {
                $course->title_en = $course->title_ar;
            }

            foreach ($othercourses as $course) {
                $course->title_en = $course->title_ar;
            }

           



            foreach ($levelall as $level) {
                $level->name_en = $level->name_ar;
               //  $level->image_path_en = $level->image_path_ar;
            }
        }
    return view('client.pages.courses')->withSlugs($slugs)->withCourses($courses)->withFooter($footer)->withItems($items)->withKits($kits)->withLevelall($levelall)->withOthercourses($othercourses);
    }

    public function slugRedirector($slug){

        self::langService();
        $template= Slug::where('slug','=',$slug)->first();
        if ($template->href != null ){
            return redirect($template->href);
        }
        $items = Item::with('images')->take(9)->get();
        foreach ($items as $item) {
            $item->maxPrice = OrderService::getMaximumPrice($item->id);
            if (App::getLocale() == 'ar')
                $item->name_en = $item->name_ar;
        }

        $kits = Kit::with('items')->with('images')->get();
        // foreach ($kits as $kit) {
        //     $kit->maxPrice = OrderService::getKitSellingPrice($kit->id);
        //     $kit->itemsCount = $kit->items->count();
        //     if (App::getLocale() == 'ar')
        //         $kit->name_en = $kit->name_ar;
        // }        

        if (App::getLocale() == 'en'){
            $body = $template->text;
            $title_en = $template->title_en;
        }
        else{
            $body = $template->text_ar;
        
        $title_en = $template->title_ar;
    }
        $slugs =SlugService::getSlugs();
        $footer = SlugService::getFooter();
       
         $bannerimages = Bannerimages::where('display_area', '=', $slug)->first();



        return view('client.pages.template',compact('body','title_en'))->withSlugs($slugs)->withFooter($footer)->withItems($items)->withKits($kits)->withBannerimages($bannerimages);
        
    } 
    public function setLang($lang){
        session(['lang'=> $lang]);
        return back();
    }
    public  static function langService(){
        if (!empty(session('lang'))){
            App::setlocale(session('lang'));
        }
        else 
            App::setlocale('en');
    }

    public function getKitItems($kitId , Request $request){
        $kit = Kit::with(['items'=> function ($query){
            $query->with('images');
            $query->with('items');
        }])->find($kitId);
        $kit->maxPrice = OrderService::getKitSellingPrice($kit->id);
        $kit->itemsCount = $kit->items->count();
        // foreach ($kit->items as $item) {
        //     $item->maxPrice = OrderService::getMaximumPrice($item->item_id);
        //     if ($request->lang == 'ar'){
        //         $item->name_en = $item->name_ar;
        //     }
        // }
        if(isset($kit->id)){
            return view('client.pages.kit-items-template')->withKit($kit)->withLang($request->lang);
        }else{
            return view ('client.pages.404');
        }
    }

    public function FilterItems($string){
        $items = Item::where(DB::raw("concat_ws('-',name_en,name_ar)") , 'like' , '%'.$string.'%')->with('images')->get();
        foreach ($items as $item) {
            $item->maxPrice = OrderService::getMaximumPrice($item->id);
            if (App::getLocale() == 'ar')
                $item->name_en = $item->name_ar;
        
        }
        return view('client.pages.items-filter-template')->withItems($items);
    }
    public function confirmKit($id , Request $request){
        if (OrderService::orderKit($id , $request)){
            return 1; 
        }
        else {
            return 0;
        }
    } 

    public function confirmCustomKit(Request $request){
        $items = $request->items;
        if (OrderService::orderCustomKit($items , $request)){
            return 1; 
        }
        else {
            return 0;
        }  
    }

    public function validateCoupon(Request $request)
    {
        $code = $request->coupon;
        $courseId = 0;
        if (isset($request->course_id));
            $courseId = $request->course_id;
        $valid = CouponService::validateCoupon($code, Auth::id() , $courseId);
        $response = new \stdClass;
        if ($valid)
        {
            $amount = CouponService::getCouponAmount($code);
            $response->amount = $amount['amount'];
            $response->type = $amount['type'];
            $response->success = true;
            $response->valid = true;
        }
        else
        {
            $response->success = false;
            $response->valid = false;
        }
        return json_encode($response);
    }

    
    public function getGalleryPage(Request $request)
    {
        self::langService();
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
        $items = Item::with('images')->take(9)->get();
        foreach ($items as $item) {
            $item->maxPrice = OrderService::getMaximumPrice($item->id);
            if (App::getLocale() == 'ar')
                $item->name_en = $item->name_ar;
        
        }

        $kits = Kit::with('items')->with('images')->get();
        // foreach ($kits as $kit) {
        //     $kit->maxPrice = OrderService::getKitSellingPrice($kit->id);
        //     $kit->itemsCount = $kit->items->count();
        //     if (App::getLocale() == 'ar')
        //         $kit->name_en = $kit->name_ar;
        // }

        $galleryItems = GalleryItem::with('album')->get();
        $albums = Album::withCount(['images'])->get();

       

        return view('client.pages.gallery')
            ->withSlugs($slugs)
            ->withFooter($footer)
            ->withAlbums($albums)
            ->withItems($items)
            ->withKits($kits)
            ->withGalleryItems($galleryItems);
    }



    public function getGallery($id)
    {
        self::langService();
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
        

       

        $galleryItems = GalleryItem::where('album_id','=',$id)->get();
        $albums = Album::find($id);

      // dd($galleryItems);

        return view('client.pages.gallery-inner')
            ->withSlugs($slugs)
            ->withFooter($footer)
            ->withAlbums($albums)
            ->withGalleryItems($galleryItems);
    }


    

    public function getKitsPage(Request $request ){
        self::langService();

        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();

        $items = Item::with('images')->take(9)->get();
        foreach ($items as $item) {
            $item->maxPrice = OrderService::getMaximumPrice($item->id);
            if (App::getLocale() == 'ar')
                $item->name_en = $item->name_ar;
        
        }

        $kits = Kit::with('items')->with('images')->get();
        foreach ($kits as $kit) {
            if (OrderService::checkKitAvailablity($kit->id))
                $kit->maxPrice = OrderService::getKitSellingPrice($kit->id);
            else 
                $kit->maxPrice = -1;
            $kit->itemsCount = $kit->items->count();
            if (App::getLocale() == 'ar')
                $kit->name_en = $kit->name_ar;
        }
        
            if(isset($kits->id)){
        return view('client.pages.kits')
            ->withSlugs($slugs)
            ->withFooter($footer)
            ->withItems($items)
            ->withKits($kits);

            }else{

                return view ('client.pages.404')->withSlugs($slugs)
            ->withFooter($footer);
            }
    }

    public function getKitsPageWithKit(Request $request , $kitId)
    {
         self::langService();

        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
        $footerdata = Footer::find(1);
        $margin = $footerdata->margin;
        $items = KitItem::where('kit_id','=',$kitId)->take(9)->get();
        foreach ($items as $item) {
            $price = OrderService::getMaximumPrice($item->item_id);
            $item->maxPrice =$price['MaxAmount'] + ($price['MaxAmount'] * ($margin/100));
            $item->supplierId = $price['SupplierId'];
            $item->qty = $price['qty'];

        }

        $kits = Kit::where('id','!=',$kitId)->where('active','=','1')->with('items')->with('images')->get();
        foreach ($kits as $kit) {
            

            $kitprice =OrderService::getKitMinimumPrice($kit->id);
            $kit->maxPrice = $kitprice['minimumPrice'] + ($kitprice['minimumPrice'] * ($margin/100));
            $kit->supplierId = $kitprice['supplierId'];
            $kit->available = $kitprice['available'];
            
            $kit->itemsCount = $kit->items->count();
            if (App::getLocale() == 'ar')
                $kit->name_en = $kit->name_ar;
        }
        $kit = Kit::find($kitId);
        if(isset($kit->id)){


        $kitprice =OrderService::getKitMinimumPrice($kit->id);
        
        $kit->maxPrice = $kitprice['minimumPrice'] + ($kitprice['minimumPrice'] * ($margin/100));
        $kit->supplierId = $kitprice['supplierId'];
        $kit->available = $kitprice['available'];
        $kit->itemsCount = $kit->items->count();

        if (App::getLocale() == 'ar'){
            $kit->name_en = $kit->name_ar;
            $kit->description_en = $kit->description_ar;
        }
       //dd($kit);
    }

        if(isset($kit->id)){
        return view('client.pages.kits')
            ->withSlugs($slugs)
            ->withKitItem($kit)
            ->withFooter($footer)
            ->withItems($items)
            ->withKits($kits);

            }else{

                return view ('client.pages.404')->withSlugs($slugs)
            ->withKitItem($kit)
            ->withFooter($footer);
            }

       
    }

    public function getAchievementsPage()
    {
        self::langService();
        $footer = SlugService::getFooter();
        $slugs = SlugService::getSlugs();

        $achievements = Achievement::orderBy('id', 'desc')->get();

        return view('client.pages.achievements')
            ->withFooter($footer)
            ->withSlugs($slugs)
            ->withAchievements($achievements);
    }
    public function setAddtocart(Request $request)
    {
        self::langService();
        if(!Auth::check()){
            return redirect('login');
        }
        CartService::CreateUpdateCart($request);

         return redirect()->back()->with('message', 'Item Added in Cart.');

        //return redirect('getcart');
    }
    public function getcart()
    {
        self::langService();
        if(!Auth::check()){
            return redirect('login');
        }
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
        $user = Auth::user();
        $cart1 = Cart::where('uid','=',$user->id)->get()->first();
        if(isset($cart1)){
        $cartdetail_sum = Cartdetail::where('cart_id','=',$cart1->id)->get()->sum("final_total");
        $cart_sum = Cart::find($cart1->id);
        $cart_sum->total = $cartdetail_sum;
        $cart_sum->save();
        }
        $cart = Cart::where('uid','=',$user->id)->get()->first();
        //dd($cart);
        if(isset($cart)){
        $cartitem = Cartdetail::where('cart_id','=',$cart->id)->orderBy('type', 'ASC')->get();
        }else{
           $cartitem=array(); 
        }
        return view('client.pages.shoppingcart')
            ->withFooter($footer)
            ->withSlugs($slugs)
            ->withCart($cart)
            ->withCartitem($cartitem);
        
    }
    public function updatecart(Request $request)
    {
        $success = 0;
        $cartdetailobj = Cartdetail::find($request->id);
        $cartdetailobj->qty = $request->qty;
        $cartdetailobj->final_total = $cartdetailobj->price * $request->qty;
        $cartdetailobj->save();
        if(isset($cartdetailobj))
        {
            $success = 1;
        }    
        return response()->json( array('success' => $success));
    }


    public function updatecoupon(Request $request)
    {
        $success = 0;
         $user = Auth::user();
        
        if($request->type == 'U'){
        $cartobj1 = Cart::where('uid','=',$user->id)->get()->first();

         $cartobj1->coupon=$request->amount;
         $cartobj1->coupon_code=$request->couponCode;
        $cartobj1->save();

        }else{
             $cartobj1 = Cart::where('uid','=',$user->id)->get()->first();
             $amount = $cartobj1->coupon;
             $total = $cartobj1->total;

        $cartobj1->total=$amount+$total;     
        $cartobj1->coupon="0.00";
         $cartobj1->coupon_code="";
        $cartobj1->save();





        }


        if(isset($cartobj1))
        {
            $success = 1;
        }    
        return response()->json( array('success' => $success));
    }

    


    public function ajaxcart(Request $request)
    {
            $success = 0;
            $price=0;
             $user = Auth::user();
            $cartobj1 = Cart::where('uid','=',$user->id)->get()->first();
        
            if(isset($cartobj1)){
            $cart_id = $cartobj1->id;

            $cartdetail_sum = Cartdetail::where('cart_id','=',$cart_id)->get()->sum("final_total");;

            
          

            $cart = Cart::find($cart_id);

            $price1 = $cartdetail_sum-$cart->coupon;
            $price = number_format($cartdetail_sum-$cart->coupon, 2, '.', '');


            $cart_sum = Cart::find($cart_id);
            $cart_sum->total = $price1;
            $cart_sum->save(); 

            $cartitem = Cartdetail::where('cart_id','=',$cart_id)->orderBy('type', 'ASC')->get();
            if(isset($cartdetailobj))
            {
                $success = 1;
            } 
            $view = view('client.ajax.listcart',compact('cart','cartitem'))->render();   
        }else{
            $view = view('client.ajax.listcartempty')->render();
        }

         $price=Helper::showCurrency($price);
        return response()->json( array('success' => $success,'html' => $view, 'price'=>$price));
    }
    public function deletecart(Request $request)
    {
            $success = 0;
            $cartdetailobj = Cartdetail::find($request->deleteid);
            $cartdetailobj->delete();
            if(isset($cartdetailobj))
            {
                $success = 1;
            }    
            return response()->json( array('success' => $success));
    }
    public function checkout(Request $request)
    {

     


         self::langService();
        $order = OrderService::checkout($request);
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
       
       

             if(isset($order->id)){
        $orderitem = OrderItem::select('id', 'item_id', 'price','qty','final_total' ,'type', 'selling_price' ,'supplier_id')->where('order_id' , $order->id)->with('kitItem')->with('kitImage')->with('kit')->with('course')->with('supplier')->orderBy('id', 'desc')->get();

            foreach ($orderitem as $value) {
      if($value->type == 'Course'){
        $value->itemname = $value->course->title_en;
        $value->imagepath = '/public'.$value->course->image_path;



      }elseif($value->type == 'Kit'){

     
        $value->itemname = $value->kit->name_en;
        
        $p2=1;
        foreach($value->kit->images as $image){
            if(count($value->kit->images) == $p2){
                if(file_exists($image->img_src)){
                    $value->imagepath = $image->img_src;
                }else{
                    $value->imagepath = '/css/client/images/kit-1.jpg';
                }
            }
            $p2++;
        }


      }elseif($value->type == 'Item'){
        $value->itemname = $value->kitItem->items->name_en;
        //$value->imagepath = $value->kitItem->items->image_path;
        $p3=1;
        foreach($value->kitItem->items->images as $image){
            if(count($value->kitItem->items->images) == $p3){
                if(file_exists($image->img_src)){
                    $value->imagepath = $image->img_src;
                }else{
                    $value->imagepath = '/css/client/images/lessons-slider-pic2.png';
                }
            }
            $p3++;
        }
      }
    }


        }else{
           $orderitem=array(); 
           return redirect('getcart');
        }
    
           




         return view('client.pages.confirm')
            ->withFooter($footer)
            ->withSlugs($slugs)
            ->withOrder($order)
            ->withOrderitem($orderitem);    
        
    }
    public function getOrdermail()
    {
        $order = OrderService::getOrdermail(67,'cod');
    }
    public function getStores(Request $request)
    {
        self::langService();
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
        $footerdata = Footer::find(1);
        $margin = $footerdata->margin;
        $kits = Kit::where('active','=','1')->with('items')->with('images')->get();
        foreach ($kits as $kit) {
            $kitprice =OrderService::getKitMinimumPrice($kit->id);
            $kit->maxPrice = $kitprice['minimumPrice'] + ($kitprice['minimumPrice'] * ($margin/100));
            $kit->supplierId = $kitprice['supplierId'];
            $kit->available = $kitprice['available'];
            
            $kit->itemsCount = $kit->items->count();
            if (App::getLocale() == 'ar')
                $kit->name_en = $kit->name_ar;
        }
        $searchdata = $request->search;
        if($request->search!=""){
            $items = KitItem::join('item', 'item.id', '=', 'kit_item.item_id')
            ->where('item.name_en', 'LIKE', "%$request->search%")
            ->groupBy('item_id')->paginate(6);
        }else{
            $items = KitItem::groupBy('item_id')->paginate(6);
        }
        foreach ($items as $item) {
            $price = OrderService::getMaximumPrice($item->item_id);
            $item->maxPrice =$price['MaxAmount'] + ($price['MaxAmount'] * ($margin/100));
            $item->supplierId = $price['SupplierId'];
            $item->qty = $price['qty'];

        }
        if ($request->ajax()) 
        {
            return view('client.pages.storedata')
            ->withKits($kits)
            ->withItems($items)
            ->withFooter($footer)
            ->withSlugs($slugs)->with('searchdata')->with('i', ($request->input('page', 1) - 1) * 6);
        }else{

            return view('client.pages.stores')
            ->withKits($kits)
            ->withItems($items)
            ->withFooter($footer)
            ->withSlugs($slugs)->with('searchdata')->with('i', ($request->input('page', 1) - 1) * 6);
        }
    }

    public function getOrdertracking(){
        $orderobj = Order::where('shipment_id' ,'!=','')->where('state' ,'!=','5')->get();
        if(count($orderobj)>0)
        foreach ($orderobj as $order) {
            AramexService::trackingShipment($order->id,$order->shipment_id);
        }
        
    }
    public function getcontactUs(){
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
         return view('client.pages.contact')
            ->withFooter($footer)
            ->withSlugs($slugs);
    }
    public function setcontactUs(Request $request){
        $footer = Footer::find(1);
        $admin_email = $footer->email;

        $message = $request->message;
        $from = $request->email;
        $subject = $request->subject;
        $name = $request->name;

        $email = Email::create(['name'=> $name , 'subject'=> $subject , 'email' => $from  , 'text' => $message]);

        $EmailTemplatesObj = Emailtemplates::find(5);
        $msg=nl2br($EmailTemplatesObj->email_temp_body);
        $msg=str_replace("<FULLNAME>",$name,$msg);
        $msg=str_replace("<EMAIL>",$from,$msg);
        $msg=str_replace("<MESSAGE>",$message,$msg);

        
        /* Start admin mail */
        MailService::send('email.template',$data=['msg'=>$msg], $admin_email, $admin_email, $EmailTemplatesObj->email_temp_subject);
        /*MailService::send('email.template',$data=['msg'=>$msg], $admin_email, $admin_email='ketulpatel@webmynesystems.com', $EmailTemplatesObj->email_temp_subject);*/

        /* end admin mail */

        //MailService::send('template' , [$msg] ,$from , 'info@eureka.com' , $subject);
        
        return redirect()->back()->with('message', 'Email Sent Successfully, We will reply as soon as possible!');
       // return 'ok' ;
    }   
    

}
    