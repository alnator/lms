<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaypalIPN;
use App\SubscriptionModel;
use App\Services\SubscriptionService;
use Auth;
use App\Services\PaymentService;
use Carbon\Carbon;
use App\Course;
use App\PaymentMethod;
use Exception;


class PaypalController extends Controller
{
    public function index(Request $request){
    	// dd($request->model_id);
        if (!Auth::check()){
            return redirect(route('login'));
        }
            // dd($request->course_id);

        $modelId = $request->model_number; // Subscription Model ID
        if ($modelId == 5 ){
            $course = Course::find($request->course_id);
            $totalAmountWithFee = $course->price;
        }
        else {
            $totalAmountWithFee = SubscriptionService::getAmount($modelId);
        }
        // dd($totalAmountWithFee);
    	$paypalURL = '';//PayPal Link
		$paypalID = '';// Paypal Account
		$form = "<form id='paypal' action='". $paypalURL."' method='post'>";
		$form .='<input type="hidden" name="business" value="'. $paypalID.'">';
		$form .='<input type="hidden" name="cmd" value="_xclick">';
		$itemDetail = "Detail goes here"; // Details 
		
		$form .='<input type="hidden" name="item_name" value=" '.$itemDetail.'">
		    <input type="hidden" name="model_number" value="'.$modelId.'">
		    <input type="hidden" name="amount" value="'.$totalAmountWithFee.'">
		    <input type="hidden" name="currency_code" value="USD">';

		$form .="<input type='hidden' name='cancel_return' value='http://localhost/home'> <input type='hidden' name='return' value='http://localhost/paypal'>";
		$form.="<script type=\"text/javascript\"> document.forms['paypal'].submit();</script>";
		// dd($form);
        echo $form; // This will Submit to Paypal
    }

    public function ipn()
    {

        // Reply with an empty 200 response to indicate to paypal the IPN was received correctly.
        header("HTTP/1.1 200 OK");
        $data = $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2) {
                $myPost[$keyval[0]] = urldecode($keyval[1]);
            }
        }

        $ipn = new PaypalIPN();
        // Use the sandbox endpoint during testing.
        $ipn->useSandbox();
        $verified=false;
        try {
            $verified = $ipn->verifyIPN($raw_post_array);
        } catch (\Exception $e) {
        }

        if ($verified) {
            /*
             * Process IPN
             * A list of variables is available here:
             * https://developer.paypal.com/webapps/developer/docs/classic/ipn/integration-guide/IPNandPDTVariables/
             */
            if (isset($myPost['payment_status'])) {
                if ($myPost['payment_status'] == "Completed") {
                    // Provider Parameters
                    
                    $subscriptionModel = SubscriptionModel::find($myPost['model_number']);
                    $amount = $myPost['mc_gross'];
                    if($amount != $subscriptionModel->price){
			            throw new Exception("Mismatch Amount");
					}
                    $course = 0;
                    if (isset($myPost['course'])){
                    	$course = $myPost['course'];
                    }
                    $userId = Auth::id();
                    $invoiceId = $myPost['txn_id']; 
                    $paymentId = PaymentService::makePayment( $amount , $invoiceId , 1);
                    $paymentMethod = PaymentMethod::PAYPAL;
                    $startDate = Carbon::today();
                    $endDate = Carbon::today()->addDays($subscriptionModel->period_in_days);
                    $useEmail = $myPost['payer_email'] ;	
                    SubscriptionService::subscribe($userId , $subscriptionModel->id , $paymentId , $paymentMethod , $course , $startDate , $endDate);
                }
            }
        }
    }
    public function test(Request $request){

        $userId = Auth::id();
        $model = SubscriptionModel::find($request->model_id);
        
        $amount = $model->price;
        $paymentId = PaymentService::makePayment( $amount, -1,'', 1);
        $paymentMethod = PaymentMethod::PAYPAL;
        $startDate = Carbon::today();
        $course = 0;
        if (isset($request->course_id))
            $course= $request->course_id;
        $endDate = Carbon::today()->addDays($model->period_in_days);
        $userEmail = Auth::user()->email;
        SubscriptionService::subscribe($userId , $model->id , $paymentId , $paymentMethod ,$course,$startDate, $endDate);
        return back();
    }
}

