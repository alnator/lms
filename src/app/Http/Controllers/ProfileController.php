<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Subscription;
use App\Course;
use App\User;
use App\UserProgress;
use App\Services\MediaService;
use App\Star;
use App\Services\SlugService;
use App;
use App\Item;
use App\Kit;
use App\Services\OrderService;
use App\Quiz;
use App\Order;
use App\OrderItem;
use App\Services\CourseService;

use Illuminate\Support\Facades\Hash;


class ProfileController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function getProfile(){
        self::langService();
        $user = Auth::user();
        $subscriptoins = Subscription::where('user_id', '=', $user->id)->get();
        
        $courses_ids = [];

        foreach ($subscriptoins as $subs) {
            array_push($courses_ids,$subs->course_id );
        }
        $courses = Course::with(['session'=>function ($query){
            $query->with(['video' => function ($query){
                $query->where('active' , 1);
            }]);
        }])
        ->whereIn('id',$courses_ids)->get();
        $accomplishments = [];

        foreach ($courses as $value) {
            $value->courseTime = CourseService::getCourseTime($value->id);
        }

        for($i=0 ; $i <$courses->count() ;$i++) {
           //echo $i;
            $course = $courses[$i];


            $counter = 0;
            $course->quiz = 0;
            $progressCounter = 0;
            foreach ($course->session as $session) {

                

                $session_videos = 0;
                $session_time = 0;

               


                foreach ($session->video as $video) {


                     
                    $counter++;
                    $session_time += $video->estimated_time;
                    $progress = UserProgress::where('user_id' , '=' , $user->id)->where('video_id','=',$video->id)->get()->first();

                 

                    if(isset($progress )){

                        if ($progress->quiz_passed != 1){
                            $quizes = Quiz::where('video_id' , $video->id)->get();
                            if ($quizes->count() > 0 )
                                $courses[$i]->quiz = 1;
                        }
                        $progressCounter++;
                        $session_videos++;
                    }
                  
                }
                $session->time = $session_time;
              //  echo $progressCounter."<br>";
            //    echo $session->video->count()." == ".$session_videos;


                if($session->video->count() == $session_videos)
                    $session->done = 1;
            }


            if ($counter != 0 )
                $course->progress =  intval(($progressCounter/ $counter)*100) ;

          //  echo $course->progress;

           if ($course->progress == 100){
                array_push($accomplishments,$course);
               // $courses->forget($i);
            }



        }
        $slugs = SlugService::getSlugs();
    	$footer = SlugService::getFooter();
        $items = Item::with('images')->take(9)->get();
        foreach ($items as $item) {
            $item->maxPrice = OrderService::getMaximumPrice($item->id);

            if (App::getLocale() == 'ar')
                $item->name_en = $item->name_ar;
        }

        $kits = Kit::with('items')->with('images')->get();
        // foreach ($kits as $kit) {
        //     $kit->maxPrice = OrderService::getKitSellingPrice($kit->id);
        //     $kit->itemsCount = $kit->items->count();
        //     if (App::getLocale() == 'ar')
        //         $kit->name_en = $kit->name_ar;
        // }
        if (App::getLocale() == 'ar'){
            foreach ($courses as $course) {
                $course->title_en = $course->title_ar;
                $course->overview_en = $course->overview_ar;
                $course->description_en = $course->description_ar;
                foreach ($course->session as $session) {
                    $session->session_number = $session->session_number_ar;
                    foreach ($session->video as $video2) {
                        $video2->name_en = $video2->name_ar;
                        $video2->description_en = $video2->description_ar;
                    }
                }
            }
            foreach ($accomplishments as $course) {
                $course->title_en = $course->title_ar;
                $course->overview_en = $course->overview_ar;
                $course->description_en = $course->description_ar;
                foreach ($course->session as $session) {
                    $session->session_number = $session->session_number_ar;
                    foreach ($session->video as $video2) {
                        $video2->name_en = $video2->name_ar;
                        $video2->description_en = $video2->description_ar;
                    }   
                }
            }
        }
        return view('client.pages.profile')->withCourses($courses)->withAccomplishments($accomplishments)->withUser($user)->withSlugs($slugs)->withFooter($footer)->withItems($items)->withKits($kits);
    }


    public function changepass(){
        self::langService();
        $user = Auth::user();
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
        
        return view('client.pages.changepass')->withUser($user)->withSlugs($slugs)->withFooter($footer);
    }

     public function setchangepass(Request $request)
    {
        $userjob = Auth::user();

           $user = User::findOrFail($userjob->id);

           if (Hash::check($request->currentpass, $user->password)) { 

                 if($request->password == $request->password_confirmation){

            $this->validate($request,[
            'password' => 'required|min:6',
            'password_confirmation' => 'required|string|same:password',
            ]);
            //Change Password
           $user->fill([
    'password' => Hash::make($request->password)
    ])->save();

         

            return redirect()->back()->with("status","Password changed successfully !");

            }else{
                //Current password and new password are same
               return redirect()->back()->with("status"," Password cannot be same as your Conform password."); 
            }



   

 

} else {
    
    return redirect()->back()->with("status","Password does not match");
}






                
           
           
            
    }



    

    public function getRunningCourse($id){
                self::langService();
        $user = Auth::user();
       
        $course = Course::with(['session'=>function ($query){
            $query->with(['video' => function ($query){
                $query->where('active' , 1);
            }]);
        }])->find($id);
        $counter = 0;
        $progressCounter = 0;
        foreach ($course->session as $session) {
            $session_videos = 0;
            $session_time = 0;
            foreach ($session->video as $video) {
                $counter++;
                $session_time += $video->estimated_time;
                $progress = UserProgress::where('user_id' , '=' , $user->id)->where('video_id','=',$video->id)->get();
                if($progress->count() != 0){
                    $progressCounter++;
                    $session_videos++;
                }

            }
            $session->time = $session_time;
            if($session->video->count() == $session_videos)
                $session->done = 1;
        }
        if($counter != 0 )
            $course->progress =  intval(($progressCounter/ $counter)*100) ;
        foreach ($course->session as $csession) {
            foreach ($csession->video as $video) {
                $prog = UserProgress::where('video_id', '=' , $video->id)->where('user_id', '=' , $user->id)->get()->first();
                if (isset($prog)){
                    if($prog->video_seen == 1 ){
                        $video->done = 1;
                    }
                    $video->solved = 0;
                    if ($prog->quiz_passed){
                        $video->solved = 1;
                    }

                    $quizCount  = Quiz::where('video_id', $video->id)->count();
                    if ($quizCount == 0 ){
                        $video->solved = -1;
                    }
                }
            }
        }
        $items = Item::with('images')->take(9)->get();
        foreach ($items as $item) {
            $item->maxPrice = OrderService::getMaximumPrice($item->id);

            if (App::getLocale() == 'ar')
                $item->name_en = $item->name_ar;
        }

        $kits = Kit::with('items')->with('images')->get();
        // foreach ($kits as $kit) {
        //     $kit->maxPrice = OrderService::getKitSellingPrice($kit->id);
        //     $kit->itemsCount = $kit->items->count();
        //     if (App::getLocale() == 'ar')
        //         $kit->name_en = $kit->name_ar;
        // }
        $slugs = SlugService::getSlugs();
    	$footer = SlugService::getFooter();
        if(App::getLocale() == 'ar'){
            $course->title_en = $course->title_ar;
            $course->overview_en = $course->overview_ar;
            $course->description_en = $course->description_ar;
            foreach ($course->session as $session) {
                $session->session_number = $session->session_number_ar;
                foreach ($session->video as $video2) {
                    $video2->name_en = $video2->name_ar;
                    $video2->description_en = $video2->description_ar;
                }
            }
        }

        return view('client.pages.running-courses')->withCourse($course)->withUser($user)->withSlugs($slugs)->withFooter($footer)->withItems($items)->withKits($kits);
    }

    public function changePic(Request $request){
        $user = Auth::user();
        $user->image_path =MediaService::saveImage($request->image);
        $user->update();
        $star = Star::where('user_id' , '=' ,$user->id )->get()->first();
        if (isset($star) && $star->count() != 0){
            $star->image_path = $user->image_path;
            $star->update();
        }
        return redirect(route('profile'));
    }
    public  static function langService(){
        if (!empty(session('lang'))){
            App::setlocale(session('lang'));
        }
        else 
            App::setlocale('en');
    }

     
       public function getTransaction(){
        self::langService();
        $user = Auth::user();
        $order = Order::where('user_id','=',$user->id)->orderBy('id','desc')->get();
              
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
      

        
        return view('client.pages.transaction')->withOrder($order)->withUser($user)->withSlugs($slugs)->withFooter($footer);
    }


    public function getOrder($id){
        self::langService();
        $user = Auth::user();
                    
        $slugs = SlugService::getSlugs();
        $footer = SlugService::getFooter();
      
       $order = Order::find($id);
    $orderItems = OrderItem::select('id', 'item_id', 'price','qty','final_total' ,'type', 'selling_price' ,'supplier_id')->where('order_id' , $id)->with('kitItem')->with('kit')->with('course')->with('supplier')->orderBy('id', 'desc')->get();

      foreach ($orderItems as $value) {
      if($value->type == 'Course'){
        $value->itemname = $value->course->title_en;
      }elseif($value->type == 'Kit'){
        $value->itemname = $value->kit->name_en;
      }elseif($value->type == 'Item'){
        $value->itemname = $value->kitItem->items->name_en;
      }
    }

        
        return view('client.pages.order-details')->withOrder($order)->withOrderItems($orderItems)->withUser($user)->withSlugs($slugs)->withFooter($footer);
    }







}
