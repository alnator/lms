<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class isSupplier
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() == null ){
            return redirect('login');
        }

        if (Auth::guest() || Auth::user()->supplier != 1){
            return redirect(route('home'));
        }

        return $next($request);
    }
}
