<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Video;
use App\Course;

class UserProgress extends Model
{
    protected $table= 'user_progress';
    protected $fillable = ['user_id' , 'video_id', 'quiz_passed' ,'video_seen','course_id'];
    public function course()
    {
    	return $this->belongsTo(Course::class, 'id' , 'course_id'); 
    }

    public function user()
    {
    	return $this->belongsTo(User::class , 'id' , 'user_id');
    }

    public function video()
    {
    	return $this->belongsTo(Video::class , 'id' , 'video_id');
    }
}
