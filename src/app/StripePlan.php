<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StripePlan extends Model
{
  protected $table = 'stripe_plan';

  protected $fillable = [
      'subscription_model_id',
      'course_id',
      'stripe_plan_id'
  ];
}
