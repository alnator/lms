<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Star;
use App\GroupUser;
use App\SupplierItem;
use App\User;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','age','active', 'supplier','teacher' , 'fees','admin','country'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function star(){
        return $this->has(Star::class, 'id', 'user_id'); 
    }

    public function groupUser(){
        return $this->hasMany(GroupUser::class);
    }
    public function item(){
        return $this->hasMany(SupplierItem::class , 'supplier_id', 'id');
    }
    public function order(){
        return $this->hasMany(Order::class ,'id' , 'user_id');
    }
}
