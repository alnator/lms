<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slug extends Model
{
    protected $table = 'slugs';
    protected $fillable = ['text' ,'text_ar','title_en','active','title_ar', 'slug','premission', 'href' ,'updated_at','created_at'];
}
