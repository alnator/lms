<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStates extends Model
{
    const CREATED =  1; 
    const CONFIRMED = 2;
    const PACKED = 3 ;
    const ONDELIVERY = 4;
    const DELIVERED = 5;
}
