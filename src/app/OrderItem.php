<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\SupplierItem;
use App\Item;
use App\Order;
use App\Kit;
use App\Course;
use App\KitItem;
use App\KitImage;
class OrderItem extends Model
{
    protected $table = 'order_item';
    protected $fillable = ['id','order_id',	'item_id',	'supplier_id',	'price','selling_price'	,'packed','type', 'mainid','qty','final_total'];

    public function supplier(){
    	return $this->belongsTo(User::class , 'supplier_id' ,'id');
    }
    public function supplierItem(){
    	return $this->hasMany(SupplierItem::class , 'item_id', 'item_id');
    }
    public function item(){
    	return $this->belongsTo(Item::class , 'item_id' , 'id');
    }
    public function order(){
        return $this->belongsTo(Order::class , 'order_id', 'id');
    }
    public function kit(){
        return $this->belongsTo(Kit::class , 'item_id' , 'id');
    }
    public function course(){
        return $this->belongsTo(Course::class , 'item_id' , 'id');
    }
    public function kitItem(){
        return $this->belongsTo(KitItem::class , 'item_id' , 'id');
    }
    
      public function kitImage(){
        return $this->belongsTo(KitImage::class ,'item_id', 'kit_id');
    }

}
