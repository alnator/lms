<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StripeCustomer extends Model
{
  protected $table = 'stripe_customer';

  protected $fillable = [
      'user_id',
      'email',
      'stripe_id',
      'card_id'
  ];
}
