<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Vision extends Model
{
  protected $table = "vision";

  protected $fillable = [
      'name_ar',
      'name_en',
      'image_path_ar',
      'image_path_en',
      'active',
      'created_by',
      'updated_by'
  ];
  
  public function updatedBy()
  {
    return $this->belongsTo(User::class , 'updated_by' , 'id');
  }
  public function createdBy()
  {
    return $this->belongsTo(User::class , 'created_by' , 'id');
  }
}
