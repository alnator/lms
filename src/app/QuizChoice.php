<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Quiz;

class QuizChoice extends Model
{
    protected $table = "choices";
    protected $fillable = ['choice_statement', 'choice_statement_ar', 'id' , 'correct', 'quiz_id'];

    public function quiz(){
    	return $this->belongsTo(Quiz::class , 'id', 'quiz_id');
    }

}
