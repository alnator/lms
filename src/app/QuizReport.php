<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class QuizReport extends Model
{
    protected $table = "quiz_report";
    protected $fillable = [ 'course_id', 'user_id', 'quiz_id' ,'video_id' ,'choice_id', 'correct'];


    public function user(){
    	return $this->belongsTo(User::class ,'user_id' , 'id');
    }
}
