<?php
namespace App\Services;

use App\Cart;
use App\Cartdetail;
use App;
use Auth;

class CartService
{
	
    public static function CreateUpdateCart($request){
		
		
		$user = Auth::user();
		//dd($user);
		$cart = Cart::where('uid','=',$user->id)->get()->first();
		
		if(isset($cart)){
			//dd($cart);
		}else{
			$cart = Cart::updateOrCreate(['id' => 0 ] ,[
				'uid' => $user->id,
				'total' => $request->price
			]);
		}
		
		$cartdetail = Cartdetail::where('product_id','=',$request->itemid)->where('type','=',$request->type)->where('cart_id','=',$cart->id)->get()->first();
		if(isset($cartdetail)){
			//$cartdetail->qty = $cartdetail->qty + $request->qty;
			//$cartdetail->final_total = $cartdetail->price * ($cartdetail->qty + $request->qty);
            //$cartdetail->save();
		}else{
			$cartdetail = Cartdetail::updateOrCreate(['id' => 0 ] ,[
				'cart_id' => $cart->id,
				'product_id' => $request->itemid,
				'type' => $request->type,
				'price' => $request->price,
				'qty' => $request->qty,
				'final_total' => $request->price,
				'supplierid' => $request->supplierId
			]);
			
		}
		$total=0;
		$cartdetail = Cartdetail::where('cart_id','=',$cart->id)->get();
		foreach ($cartdetail as $cartvalue) {
			$total+=$cartvalue->final_total;
		}
		$cart->total = $total;
		$cart->save();
		/*$category = CourseCategory::updateOrCreate(['id' => isset($request->id) ? $request->id : 0 ] , [
			'name_ar' => $request->name_ar,
			'name_en' => $request->name_en
		]);*/	
	}
}
