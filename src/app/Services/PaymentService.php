<?php

namespace App\Services; 
use App\Services\CouponService;
use App\Payment;
use Auth;

class PaymentService {

 	public static function makePayment($amount ,$invoiceId , $coupon = '' ,$paymentMethod , $userId = null){
 		if ($userId == null){
 			$userId = Auth::id();
 		}
 		return PaymentService::createPayment($userId , $amount , $invoiceId ,$coupon , $paymentMethod)->id;
    }	
	
	private static function createPayment($user_id , $amount , $invoice_id , $coupon, $paymentMethod){
		$payment = Payment::updateOrCreate(['id' => 0] , [
			'user_id' => $user_id,
			'amount' => $amount,
			'invoice_id' => $invoice_id,
			'coupon' => $coupon, 
			'payment_method_id'=>$paymentMethod
		]);
		if ($coupon != '')
		{
			CouponService::useCoupon($coupon);
		}
		return $payment;
	}


	public static function getIncomeReport($request){
		$months = $request->month ;
		$year = $request->year;
		$model = $request->subscription;
		$data= [];
		$response = [];
		if ($months[0] == 0){
			for ($i=1 ; $i< 13 ;$i ++){
				$query = Payment::whereMonth('created_at',$i)->whereYear('created_at', $year);
				if ($model != 0){
					$query->whereHas('subscription',function($query) use ($model){
						$query->where('subscription_model_id', '=', $model);
					});
				}
				$query = $query->sum('amount');
				$data[$i]= $query;
			}
		}
		else {
			foreach ($months as $month) {
				$query = Payment::whereMonth('created_at',$month)->whereYear('created_at', $year);
				if ($model != 0){
					$query->whereHas('subscription' ,function($query) use ($model){
						$query->where('subscription_model_id', '=', $model);
					});
				}
				$query = $query->sum('amount');
				$data[$month] = $query;
			}
		}
		$sum =0 ;
		foreach ($data as $key => $value) {
			$response[$key] = new \stdClass();
			if ($key < 10)
				$response[$key]->month = "0".$key."-$year";
			else 
				$response[$key]->month = $key."-$year";			
			$response[$key]->month_income = $value;
			$sum += $value;
		}
		return [json_encode($response), $sum];
	}

	public static function getIncomeReportGrid($months , $year, $subscription , $filter, $pdf = 0){
		$index = $filter ? $filter['pageIndex'] : 0 ; 

		$payments = Payment::select(['id','user_id','amount','payment_method_id' , 'created_at'])->with('user')->with('subscription');
		
		$all= 0;
		foreach ($months as $value) {
			if ($value == 0)
				$all =1;
		}

		if (!$all){
			$str = '';
			for ($i=0; $i <count($months) ; $i++) { 
				if ($i == 0)
					$str .= 'MONTH(created_at) =' .$months[$i];
				else 
					$str .= ' or MONTH(created_at) ='.$months[$i];	
			}
			$payments->whereRaw($str);
		}
		if ($subscription != 0){
			$payments->whereHas('subscription' , function ($query) use ($subscription){
				$query->where('subscription_model_id', '=', $subscription);
			});
		}
		$payments->whereYear('created_at' ,$year);

		if (!empty($filter['user']['email'])){
			$payments->whereHas('user' , function($query) use ($filter){
				$query->where('email' , 'like','%'.$filter['user']['email'].'%' );
			});
		}
		if (!empty($filter['amount'])){
			$payments->where('amount' , $filter['amount']);
		}
		if (!empty($filter['id'])){
			$payments->where('id' , $filter['id']);
		}



		$payments->orderBy('id','desc');
		$result['total'] = $payments->count();
		if ($pdf == 0){
			$skip = ($index == 1) ? 0 : ($index-1)*10 ;
			$result['data']=$payments->take(10)->skip($skip)->get();
			return $result;
		}
		else {
			$result['data']=$payments->get();
			$result['header']= ['ID' , 'User Email' , 'Amount' , 'Subscription Model' , 'Created at'];
			return $result;
		}
	}
}
