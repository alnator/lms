<?php

namespace App\Services; 
use App\SubscriptionModel;
use App\Payment;
use App\Subscription;
use Carbon\Carbon;
use App\User;
class SubscriptionService {

   
   public static function getAmount($id){
        return SubscriptionModel::find($id)->price;
   }
   public static function subscribe($userId , $subscriptionModel , $paymentId , $paymentMethod , $course , $startDate , $endDate ,$stripe_subscription_id = 0 ){

   

      Subscription::updateOrCreate(['id'=> 0],[
        'user_id' => $userId,
        'subscription_model_id' => $subscriptionModel,
        'payment_id' => $paymentId,
        'payment_method_id' => $paymentMethod,
        'course_id' => $course,
        'start_date' => $startDate,
        'end_date' => $endDate,
        'status' => 'active',
        'stripe_subscription_id' => $stripe_subscription_id,
        'percentage_progress'=> 100
    ]);
   }
   public static function getSubscriptionReports(){
      $reports = [];
      $monthCounter =1 ;
      for ($monthCounter ; $monthCounter <13 ; $monthCounter++){
        $reports[$monthCounter ] = Subscription::whereMonth('created_at', '=', $monthCounter)->with('payment')->get(); 
      }
      return $reports;
   }
   public static function getSubscriptionReportsForChart($request){
      $reports = [];
      $activeReports = [];
      $registeredUsers = [];
      $year = $request->year;
      $monthCounter =1 ;
      $model = $request->model;
      for ($monthCounter ; $monthCounter <13 ; $monthCounter++){
          //All Subscriptions
        $query = Subscription::whereMonth('created_at', '=', $monthCounter)->whereYear('created_at', $year);
        if ($model != 0 ){
          $query->where('subscription_model_id', '=', $model);
        }
        $reports[$monthCounter ] = $query->count(); 
          //Active Subscriptions
        $dateStr = Carbon::create($year , $monthCounter , 1 , 0 );

        $query2 = Subscription::where('end_date', '>=',$dateStr )
        ->where('start_date' ,'<=' ,$dateStr->addDays(29));
        if ($model != 0 ){
          $query2->where('subscription_model_id', '=', $model);
        }
        $activeReports[$monthCounter] = $query2->count();
          // Registered Users
        $query2 = User::whereMonth('created_at',$monthCounter)->whereYear('created_at', $year);
        $registeredUsers[$monthCounter] = $query2->count(); 
      }

      $data = [];
      $monthCounter =1 ;
      $overall = 0;
      foreach ($reports as $key => $report) {
        $data[$monthCounter] = new \stdClass();
        if ($key < 10)
            $data[$monthCounter]->month = "0$key" . "-$year";
        else
            $data[$monthCounter]->month = "$key" . "-$year";
         
        $data[$monthCounter]->value = $report;
        $data[$monthCounter]->value2= $activeReports[$monthCounter];  
        $data[$monthCounter]->value3= $registeredUsers[$monthCounter];
        $overall += $report;
        $monthCounter +=1 ;
      
      }

      $res[0] =json_encode($data) ;
      $res[1] = $overall; 
      return $res;
   }

  public static function getSubscriptionMonthFilterReport($request){
    $days = [] ;
    $year = $request->year;
    $month = $request->month;
    $model= $request->model;

    $query = Subscription::select('*')->whereMonth('created_at', $month)->whereYear('created_at' , $year);
    if ($model != 0 ){
      $query->where('subscription_model_id' , '=', $model);
    }
    $query =  $query->get();

    $date = Carbon::create($year , $month , 1 , 0 );;
    $daysInMonth = $date->daysInMonth;
    $overall = $query->count();
    $chartData= [];
    foreach ($query as $row) {
      $date = new Carbon($row->created_at);
      if (isset($days[$date->day]))
        $days[$date->day] ++;
      else 
        $days[$date->day]= 1;

    } 

    for ($i=1; $i <= $daysInMonth ; $i++) 
    { 
      $chartData[$i] = new \stdClass();
      if ($i < 10)
      {
        $chartData[$i]->day = "0$i";
      }
      else 
      {         
        $chartData[$i]->day = "$i;";
      }
      $chartData[$i]->value = isset($days[$i]) ?$days[$i] : 0 ;  
    }
    $res [0]= json_encode($chartData);
    $res [1] = $overall;
    return $res;
  } 

  public static function getSubscriptionMonthsReportGrid($model , $month , $year , $filter , $pdf = 0){
    $index = $filter ? $filter['pageIndex'] : 0 ; 
    // dd($model);
    $subscriptions = Subscription::select('*')->with('user')->with('course')->with('subscriptionModel')->with('payment');

    if ($month != 0  && $year != 0)
      $subscriptions->whereMonth('created_at',$month)->whereYear('created_at' , $year);
    else if ($year != 0)
      $subscriptions->whereYear('created_at' , $year);
    
    if ($model != 0)
      $subscriptions->where('subscription_model_id' , $model);
 
    if (!empty ($filter['id'])){
      $subscriptions->where('id' ,$filter['id'] );
    }
    if(!empty($filter['user']['email'] )){
      $subscriptions->whereHas('user' ,function ($query) use ($filter){
        $query->where('email','like' , '%'.$filter['user']['email'] .'%');
      });
    }
    if (!empty($filter['payment']['amount'] )) {
      $subscriptions->whereHas('payment' ,function ($query) use ($filter){
        $query->where('amount', $filter['payment']['amount'] );
      });
    }
    if( !empty($filter['course']['title_en'])){
      $subscriptions->whereHas('course' ,function ($query) use ($filter){
        $query->where('title_en', 'like','%'.$filter['course']['title_en'].'%' );
      });
    }



    $subscriptions->orderBy('id','desc');
    $result['total'] = $subscriptions->count();

    if ($pdf == 0){
      $skip = ($index == 1) ? 0 : ($index-1)*10 ;
      $result['data']=$subscriptions->take(10)->skip($skip)->get();
      
      return $result;
    }
    else {
      $result['data'] =  $subscriptions->get();
      $result['headers'] = ['ID' , 'User Email' , 'Amount' , 'Course' , 'Subscription Model' , 'created_at'];
      return $result;
    }
  }
}
