<?php
namespace App\Services;

use App\Video;
use App\Course;
use App;
class CourseService
{
	
    public static function getCourseTime($courseID){
    	
    	$seconds = Video::where('course_id','=',$courseID)->get()->sum("estimated_time");
    	$hours = floor($seconds / 3600);
  		$minutes = floor(($seconds / 60) % 60);
  		$seconds = $seconds % 60;

    	return sprintf("%02d", $hours).':'.sprintf("%02d", $minutes).':'.sprintf("%02d", $seconds);
    }
}
   