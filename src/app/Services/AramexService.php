<?php 

namespace App\Services;

use App\Services\ShipmentService;
use App\Order;
use SoapClient;
use SoapFault;
use App\OrderTracking;
use App\OrderStates;
use App\OrderItem;
use App\Course;
use App\Subscription;
use App\SubscriptionModel;
use App\PaymentMethod;
use App\Services\SubscriptionService;
use App\Emailtemplates;
use App\Services\MailService;
use App\Footer;
use App\User;
class AramexService 
{
	public static function createShipment($orderId , $time){

		$soapClient = new SoapClient('http://ws-srv-php/projects/eureka/app/Http/Controllers/shipping.wsdl');
		
		$shipmentObj = new ShipmentService();

    	$shipmentObj->fillAddress($orderId);
    	$shipmentObj->fillShipmentObject($orderId, $time);
    	$shipmentObj->addPickupsParams($orderId , $time);

    	$shipmentRequestParam = $shipmentObj->getPickupArray();
    	
    	//$call = $soapClient->CreatePickup($shipmentRequestParam);
    	//echo "<pre>";
    	//echo json_encode($shipmentRequestParam);
    	$call =$shipmentObj->curlRequestdata('https://ws.aramex.net/ShippingAPI.V2/Shipping/Service_1_0.svc/json/CreatePickup',$shipmentRequestParam);
    	$json = json_encode($call);
        $calldata = json_decode($json,TRUE);
        //dd($calldata);
    	$order = Order::find($orderId);
    	$order->post_data = json_encode($shipmentRequestParam);
    	$order->response_data = json_encode($call);
    	$order->save();

    	if ($calldata['HasErrors'] == 'true'){
            return $calldata['Notifications']['Notification']['Message'];
    		//return $call->Notifications->Notification->Message; 
    	}
    	else {
            $call =$shipmentObj->curlRequestdata('https://ws.aramex.net/ShippingAPI.V2/Shipping/Service_1_0.svc/json/CreateShipments',$shipmentRequestParam);
            $json = json_encode($call);
            $calldata = json_decode($json,TRUE);
            //dd($calldata);
    		
            $order = Order::find($orderId);
    		$order->post_data = json_encode($shipmentRequestParam);
    		//$call = $soapClient->CreateShipments($shipmentRequestParam);
    		$order->response_data = json_encode($call);
    		$order->save();
        	if ($calldata['HasErrors'] == 'true'){
                if (is_array($calldata['Shipments']['ProcessedShipment']['Notifications']['Notification']))
                    return $calldata['Shipments']['ProcessedShipment']['Notifications']['Notification'][0]['Message']; 
                else 
                    return $calldata['Shipments']['ProcessedShipment']['Notifications']['Notification']['Message'];

                /*if (is_array($call->Shipments->ProcessedShipment->Notifications->Notification))
                    return $call->Shipments->ProcessedShipment->Notifications->Notification[0]->Message; 
                else 
                    return $call->Shipments->ProcessedShipment->Notifications->Notification->Message;*/ 
    		}else {
                $label = $calldata['Shipments']['ProcessedShipment']['ShipmentLabel']['LabelURL'];
                $shipmentId = $calldata['Shipments']['ProcessedShipment']['ID'];

	    		/*$label = $call->Shipments->ProcessedShipment->ShipmentLabel->LabelURL;
	    		$shipmentId = $call->Shipments->ProcessedShipment->ID;*/
	    		$shipmentObj->updateOrderReceipts($orderId,$label, $shipmentId);
	    		return true;
    		}
    	}
    }
    public static function trackingShipment($orderId,$shipmentId){

        $soapClient = new SoapClient('http://ws-srv-php/projects/eureka/app/Http/Controllers/Service_1_0.wsdl');
        
        $shipmentObj = new ShipmentService();
        $shipmentRequestParam = $shipmentObj->trackingShipment($shipmentId);
        try {
            $auth_call = $soapClient->TrackShipments($shipmentRequestParam);
            //echo "<pre>";
            //print_r($auth_call);
            if(isset($auth_call->TrackingResults->KeyValueOfstringArrayOfTrackingResultmFAkxlpY)){
	            $shippingId = $auth_call->TrackingResults->KeyValueOfstringArrayOfTrackingResultmFAkxlpY->Key;
	            $trackingresult = $auth_call->TrackingResults->KeyValueOfstringArrayOfTrackingResultmFAkxlpY->Value->TrackingResult;
	            foreach ($trackingresult as $value) {
	            	
	            	$WaybillNumber = $value->WaybillNumber;
				    $UpdateCode = $value->UpdateCode;
				    $UpdateDescription = $value->UpdateDescription;
				    $UpdateDateTime = $value->UpdateDateTime;
				    $UpdateLocation = $value->UpdateLocation;
				    $Comments = $value->Comments; 
				    $ProblemCode = $value->ProblemCode;
				    echo "OrderID ==> ".$orderId."<br>
				    WaybillNumber ==> ".$WaybillNumber."<br>
				    UpdateCode ==> ".$UpdateCode."<br>
				    UpdateDescription ==> ".$UpdateDescription."<br>
				    UpdateDateTime ==> ".$UpdateDateTime."<br>
				    UpdateLocation ==> ".$UpdateLocation."<br> 
				    Comments ==> ".$Comments."<br> 
				    ProblemCode ==> ".$ProblemCode."<br>";

				    echo "---------------------------------------------------------------------------------------------<br>";

				    $ordertrackingobj = OrderTracking::where('order_id','=',$orderId)->where('waybillnumber','=',$WaybillNumber)->where('updatecode','=',$UpdateCode)->where('updatedescription','=',$UpdateDescription)->get()->first();
				    if(isset($ordertrackingobj)){
				    	//echo "Record All ready Exist";
				    }else{

				    	OrderTracking::updateOrCreate(['id' => 0 ] ,[
							'order_id' => $orderId,
							'waybillnumber' => $WaybillNumber,
							'updatecode' => $UpdateCode,
							'updatedescription' => $UpdateDescription,
							'updatedatetime' => $UpdateDateTime, 
					        'updatelocation' => $UpdateLocation, 
					        'comments' => $Comments, 
					        'problemcode' => $ProblemCode
						]);
				    }
                    if($UpdateCode == 'SH005' || $UpdateDescription == 'Delivered'){
                        
                        $order = Order::find($orderId);
                        $order->state = OrderStates::DELIVERED;
                        $order->save();
                        $orderitem = OrderItem::where('order_id' , $orderId)->where('type' ,'=' ,'Course')->get();
                        $order = Order::find($orderId);
                        if($order->payment_method == 'cod'){

                            if(isset($orderitem)){
                                foreach ($orderitem as $item) {
                                    $user_id = $order->user_id;
                                    if($item->type=='Course'){
                        
                                        $course_ids=$item->item_id;
                                        $courseobj = Course::find($course_ids);
                                        $startDate = date('Y-m-d H:i:s');
                                        $endDate = date('Y-m-d H:i:s', strtotime($startDate. ' + ' . $courseobj->emonth .' months'));
                                        $subscriptionsobj = Subscription::where('user_id',  '=' , $user_id)->where('course_id' ,$item->item_id)->update(['status' => 'inactive']);
                                        $subscriptionModel = SubscriptionModel::find(5);

                                        SubscriptionService::subscribe($order->user_id , $subscriptionModel->id , $orderId ,PaymentMethod::ARAMEX , $item->item_id ,$startDate ,$endDate );

                                            $users = User::find($user_id);
                                            //***** mail code ******//

                                            $footer = Footer::find(1);
                                            $admin_email = $footer->email;

                                            $EmailTemplatesObj = Emailtemplates::find(9);
                                            $msg=nl2br($EmailTemplatesObj->email_temp_body);
                                           
                                           $coursename = $courseobj->title_en;
                                           
                                            $msg=str_replace("<DATE>",$startDate,$msg);
                                            $msg=str_replace("<NAME>",$coursename,$msg);
                                          
                                            MailService::send('email.template',$data=['msg'=>$msg], $admin_email, $users->email, $EmailTemplatesObj->email_temp_subject);

                                            MailService::send('email.template',$data=['msg'=>$msg], $admin_email, 'siddhraj@webmynesystems.com', $EmailTemplatesObj->email_temp_subject);
                                            
                                            //***** mail code ******//
                                    }
                                }
                            }
                        }


                    }
	            }
        	}

            
        } catch (SoapFault $fault) {
            die('Error : ' . $fault->faultstring);
        }
        
    }
    public static function createMoneyCollectionShipment($orderId , $time){

		$shipmentObj = new ShipmentService();
        $shipmentObj->fillAddressForMoney($orderId);
    	$shipmentObj->fillShipmentObjectForMoney($orderId, $time);
    	$shipmentRequestParam = $shipmentObj->getPickupArrayForMoney();
    	
    		$order = Order::find($orderId);
            //echo json_encode($shipmentRequestParam);
    		$order->post_data = json_encode($shipmentRequestParam);
    		//$call = $soapClient->CreateShipments($shipmentRequestParam);
            $call =$shipmentObj->curlRequestdata('https://ws.aramex.net/ShippingAPI.V2/Shipping/Service_1_0.svc/json/CreateShipments',$shipmentRequestParam);
    		$order->response_data = json_encode($call);

    		$order->save();
            $json = json_encode($call);
            $calldata = json_decode($json,TRUE);
    		
            //echo "<pre>";
            //echo $calldata['HasErrors'];
        	if ($calldata['HasErrors'] == 'true'){
                //echo "1230";
                if (is_array($calldata['Shipments']['ProcessedShipment']['Notifications']['Notification']))
                    return $calldata['Shipments']['ProcessedShipment']['Notifications']['Notification'][0]['Message']; 
                else 
                    return $calldata['Shipments']['ProcessedShipment']['Notifications']['Notification']['Message'];
    		}else {
                
                $order = Order::find($orderId);
                $order->status = 'codrequest';
                $order->save();
	    		$label = $calldata['Shipments']['ProcessedShipment']['ShipmentLabel']['LabelURL'];
	    		$shipmentId = $calldata['Shipments']['ProcessedShipment']['ID'];
	    		$data = $shipmentObj->updateOrderReceipts($orderId,$label, $shipmentId);
                
	    		return true;
    		}
    	//}
    }
}