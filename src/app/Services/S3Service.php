<?php

namespace App\Services; 


use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;

class S3Service {
	
	public static function uploadFileToS3(Request $request)
	{
	  	$video = $request->file('video');
		
	  	$name = time() . str_replace(' ', '', $request->video->getClientOriginalName());

	  	$path = '/input/';

		$s3 = \Storage::disk('s3')->putFileAs($path , $video , $name);
		if ($s3 == true){
			$res = '/output/hls-input/'. str_replace('.mp4', '.ts.m3u8', $name) ;
			$res = str_replace('.MP4', '.ts.m3u8', $res);
			$res = str_replace('.avi', '.ts.m3u8', $res);
			$res = str_replace('.AVI', '.ts.m3u8', $res);
			$res = str_replace('.MOV', '.ts.m3u8', $res);
			$res = str_replace('.mov', '.ts.m3u8', $res);
			$res = str_replace('.mkv', '.ts.m3u8', $res);
			$res = str_replace('.MKV', '.ts.m3u8', $res);
			$res = str_replace('.FLV', '.ts.m3u8', $res);
			$res = str_replace('.flv', '.ts.m3u8', $res);
			$res = str_replace('.avchd', '.ts.m3u8', $res);
			$res = str_replace('.AVCHD', '.ts.m3u8', $res);
			return  $res;
		}
		else {
			return '504';
		}

	}
 	// here
	public static function uploadImageToS3($image)
	{
	  	$path = '/images/'.time() .$image->getClientOriginalName() ;
		$s3 = \Storage::disk('s3')->put($path , file_get_contents($image));
		return 'http://dn734b9758kwp.cloudfront.net'.$path; // CloudFront For Input Bucket
	}

	public static function updateFileToS3($file)
	{
  		$path = '/files/'.time() .$file->getClientOriginalName() ;
		$s3 = \Storage::disk('s3')->put($path , file_get_contents($file));
		return 'http://dn734b9758kwp.cloudfront.net'.$path; // CloudFront For Input Bucket
	}

}
