<?php
namespace App\Services;

use Mail;
use App\EmailBroadcast;
class MailService
{
    public static function send($template,$data, $from, $to, $subject)
    {
        try{
            return  Mail::send($template, $data , function ($msg) use ($from,$to,$subject) {
                $msg->from($from, 'eurekatech.org')->to($to)->subject($subject);
            });
        }
        catch(Exception $ex){}
    }

    public static function getEmailsData($filter){

    	$index = $filter ? $filter['pageIndex'] : 0 ;


    	$emails = EmailBroadcast::select('id','from' , 'subject' , 'template');

    	if (!empty($filter['from'])){
    		$emails->where('from', 'like', '%'.$filter['from'].'%');
    	}

    	if (!empty($filter['subject'])){
    		$emails->where('subject' , 'like' , '%'.$filter['subject'].'%');
    	}


		$emails->orderBy('id','desc');
		$result['total'] = $emails->count();

		$skip = ($index == 1) ? 0 : ($index-1)*10 ;
		$result['data']=$emails->take(10)->skip($skip)->get();
		return $result;
    }
}
