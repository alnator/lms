<?php 
namespace App\Services;

use App\User;
use App\SupplierInfo;
use App\Order;
use App\OrderItem;
use App\Footer;
class ShipmentService
{
	const ACCNUM = '20016';
	const ACCENT = 'AMM';
	const ACCCNTRYCD = 'JO';
	const ACCPIN = '543543';
	// const USRNM = 'moustafa.allahham@tradinos.com';
	const USRNM = 'Nabily@aramex.com';
	// const PSRD = 'P@ssw0rd';
	const PSRD = 'Aramex123$';
	const VER = 'v1';

	protected $param;
	protected $parammoneycollect;
	

	public function __construct(){
		/*$this->param = [
			'ClientInfo'  			=> [
				'AccountCountryCode'	=> '',
				'AccountEntity'		 	=> '',
				'AccountNumber'		 	=> '',
				'AccountPin'		 	=> '',
				'UserName'			 	=> '',
				'Password'			 	=> '',
				'Version'			 	=> ''
			],

			'Transaction' 			=> [
				'Reference1'			=> '', // extra param for response Order Id
				'Reference2'			=> '', // extra param for response
				'Reference3'			=> '', // extra param for response
				'Reference4'			=> '', // extra param for response
			],
			'LabelInfo'				=> [
				'ReportID' 				=> 9201,
				'ReportType'			=> 'URL',
			],
			'Shipments' => [
				'Shipment' => [
					'Shipper'	=> [
						'Reference1' => 'some ref',
						'AccountNumber' => '',
						'Contact'		=> [
							'PersonName'			=> 'Eureka Tech Academy',
							'CompanyName'			=> 'Eureka Tech Academy',
							'PhoneNumber1'			=> '5555555',
							'CellPhone'				=> '07777777',
							'EmailAddress'			=> 'michael@aramex.com',
						],
						"PartyAddress"=> [
			               "Line1"=> "Test",
			               "Line2"=> null,
			               "Line3"=> null,
			               "City"=> "Amman",
			               "StateOrProvinceCode"=> null,
			               "PostCode"=> null,
			               "CountryCode"=> "JO",
			               "Longitude"=> "0",
			               "Latitude"=> "0"
			            ],
					],

					'Consignee'	=> [
						'Reference1'	=> 'Ref 333333', // for response
						'Reference2'	=> 'Ref 444444', // for response
						'AccountNumber' => '20016', //Account Number 
						'Contact'		=> [
							'PersonName'			=> 'Mazen',//Person Name 
							'CompanyName'			=> 'Aramex',
							'PhoneNumber1'			=> '6666666', //Phone Number
							'CellPhone'				=> '0994728051', //Cell Phone 
							'EmailAddress'			=> 'mazen@aramex.com', // Email
						],
						'PartyAddress'	=> [
							'Line1'					=> 'Mecca St',//Line1 
							'Line2'					=> 'Mecca St',//Line2 
							'Line3'					=> 'Mecca St',//Line3
							'CountryCode'			=> 'JO', // Country Code
               				'City'=> 'Amman',
               				'PostCode' => '11110',
						],
								
					],
					
					'ShippingDateTime' => time(), // Should be Filled
					"DueDate"=> null, // Should be Filled
        			"Comments"=> null, //Should Be Filled
        			"PickupLocation"=> null, // Should be Filled
			        "Attachments"=> null,
			        "ForeignHAWB"=> null,
			        // "CustomsAmount" => '0',
			        "TransportType"=> "0",
			        "PickupGUID"=> null,
			        "Number"=> null,
					'Details' => [						
						'ActualWeight' => [
							'Value'					=> 1,
							'Unit'					=> 'Kg'
						],
						
						'ProductGroup' 			=> 'DOM',
						'ProductType'			=> 'ONP',
						'PaymentType'			=> 'P',
						'PaymentOptions' 		=> '',
						'NumberOfPieces'		=> 1,
						'DescriptionOfGoods' 	=> 'Box of Electronic tools and stuffs',
						'GoodsOriginCountry' 	=> 'Jo',
										
						'CollectAmount'			=> [
							'Value'					=> '',
							'CurrencyCode'			=> 'JOD'
						],
									
						'Items' 				=> [
							'PackageType' 	=> 'Box',
							'Quantity'		=> 1,
							'Weight'		=> [
								'Value'		=> '1',
								'Unit'		=> 'Kg',		
							],
							'Comments'		=> 'Electronic tools and stuffs',
							'Reference'		=> ''
					],
				]
			]
		]
	];*/
	$this->param = array(
			'Shipments' => array(
				0 => array(
						'Shipper'	=> array(
										'Reference1' 	=> 'Ref 111111',
										'Reference2' 	=> 'Ref 222222',
										'AccountNumber' => '20016',
										'PartyAddress'	=> array(
											'Line1'					=> 'Mecca St',
											'Line2' 				=> '',
											'Line3' 				=> '',
											'City'					=> 'Amman',
											'StateOrProvinceCode'	=> '',
											'PostCode'				=> '',
											'CountryCode'			=> 'Jo'
										),
										'Contact'		=> array(
											'Department'			=> '',
											'PersonName'			=> 'Michael',
											'Title'					=> '',
											'CompanyName'			=> 'Aramex',
											'PhoneNumber1'			=> '5555555',
											'PhoneNumber1Ext'		=> '125',
											'PhoneNumber2'			=> '7777777',
											'PhoneNumber2Ext'		=> '123',
											'FaxNumber'				=> '',
											'CellPhone'				=> '07777777',
											'EmailAddress'			=> 'michael@aramex.com',
											'Type'					=> ''
										),
						),
												
						'Consignee'	=> array(
										'Reference1'	=> 'Ref 333333',
										'Reference2'	=> 'Ref 444444',
										'AccountNumber' => '',
										'PartyAddress'	=> array(
											'Line1'					=> '15 ABC St',
											'Line2'					=> '',
											'Line3'					=> '',
											'City'					=> 'Dubai',
											'StateOrProvinceCode'	=> '',
											'PostCode'				=> '',
											'CountryCode'			=> 'AE'
										),
										
										'Contact'		=> array(
											'Department'			=> '',
											'PersonName'			=> 'Mazen',
											'Title'					=> '',
											'CompanyName'			=> 'Aramex',
											'PhoneNumber1'			=> '6666666',
											'PhoneNumber1Ext'		=> '155',
											'PhoneNumber2'			=> '123456',
											'PhoneNumber2Ext'		=> '123',
											'FaxNumber'				=> '',
											'CellPhone'				=> '',
											'EmailAddress'			=> 'mazen@aramex.com',
											'Type'					=> ''
										),
						),
						
						'ThirdParty' => array(
										'Reference1' 	=> '',
										'Reference2' 	=> '',
										'AccountNumber' => '',
										'PartyAddress'	=> array(
											'Line1'					=> '',
											'Line2'					=> '',
											'Line3'					=> '',
											'City'					=> '',
											'StateOrProvinceCode'	=> '',
											'PostCode'				=> '',
											'CountryCode'			=> ''
										),
										'Contact'		=> array(
											'Department'			=> '',
											'PersonName'			=> '',
											'Title'					=> '',
											'CompanyName'			=> '',
											'PhoneNumber1'			=> '',
											'PhoneNumber1Ext'		=> '',
											'PhoneNumber2'			=> '',
											'PhoneNumber2Ext'		=> '',
											'FaxNumber'				=> '',
											'CellPhone'				=> '',
											'EmailAddress'			=> '',
											'Type'					=> ''							
										),
						),
						
						'Reference1' 				=> 'Shpt 0001',
						'Reference2' 				=> '',
						'Reference3' 				=> '',
						'ForeignHAWB'				=> '',
						'TransportType'				=> 0,
						'ShippingDateTime' 			=> time(),
						'DueDate'					=> time(),
						'PickupLocation'			=> '',
						'PickupGUID'				=> '',
						'Comments'					=> 'Shpt 0001',
						'AccountingInstrcutions' 	=> '',
						'OperationsInstructions'	=> '',
						
						'Details' => array(
										'Dimensions' => array(
											'Length'				=> 10,
											'Width'					=> 10,
											'Height'				=> 10,
											'Unit'					=> 'cm',
											
										),
										
										'ActualWeight' => array(
											'Value'					=> 1,
											'Unit'					=> 'Kg'
										),
										
										'ProductGroup' 			=> 'DOM',
										'ProductType'			=> 'ONP',
										'PaymentType'			=> 'P',
										'PaymentOptions' 		=> '',
										'Services'				=> '',
										'NumberOfPieces'		=> 1,
										'DescriptionOfGoods' 	=> 'Box of Electronic tools and stuffs',
										'GoodsOriginCountry' 	=> 'Jo',
										"ChargeableWeight"      => null,
										'CashOnDeliveryAmount' 	=> array(
											'Value'					=> 0,
											'CurrencyCode'			=> ''
										),
										
										'InsuranceAmount'		=> array(
											'Value'					=> 0,
											'CurrencyCode'			=> ''
										),
										
										'CollectAmount'			=> array(
											'Value'					=> 0,
											'CurrencyCode'			=> 'JOD'
										),
										
										'CashAdditionalAmount'	=> array(
											'Value'					=> 0,
											'CurrencyCode'			=> ''							
										),
										
										'CashAdditionalAmountDescription' => '',
										
										'CustomsValueAmount' => array(
											'Value'					=> 0,
											'CurrencyCode'			=> ''								
										),
										
										'Items' 				=> array(
											
										)
						),
				),
		),
		
			'ClientInfo'  			=> array(
										'AccountCountryCode'	=> 'JO',
										'AccountEntity'		 	=> 'AMM',
										'AccountNumber'		 	=> '20016',
										'AccountPin'		 	=> '221321',
										'UserName'			 	=> 'reem@reem.com',
										'Password'			 	=> '123456789',
										'Version'			 	=> '1.0'
									),

			'Transaction' 			=> array(
										'Reference1'			=> '001',
										'Reference2'			=> '', 
										'Reference3'			=> '', 
										'Reference4'			=> '', 
										'Reference5'			=> '',									
									),
			'LabelInfo'				=> array(
										'ReportID' 				=> 9729,
										'ReportType'			=> 'URL',
			),
	);

	$this->parammoneycollect = array(
			'Shipments' => array(
				0 => array(
						'Shipper'	=> array(
										'Reference1' 	=> 'Ref 111111',
										'Reference2' 	=> 'Ref 222222',
										'AccountNumber' => '20016',
										'PartyAddress'	=> array(
											'Line1'					=> 'Mecca St',
											'Line2' 				=> '',
											'Line3' 				=> '',
											'City'					=> 'Amman',
											'StateOrProvinceCode'	=> '',
											'PostCode'				=> '',
											'CountryCode'			=> 'Jo'
										),
										'Contact'		=> array(
											'Department'			=> '',
											'PersonName'			=> 'Michael',
											'Title'					=> '',
											'CompanyName'			=> 'Aramex',
											'PhoneNumber1'			=> '5555555',
											'PhoneNumber1Ext'		=> '125',
											'PhoneNumber2'			=> '4444444',
											'PhoneNumber2Ext'		=> '456',
											'FaxNumber'				=> '',
											'CellPhone'				=> '07777777',
											'EmailAddress'			=> 'michael@aramex.com',
											'Type'					=> ''
										),
						),
												
						'Consignee'	=> array(
										'Reference1'	=> 'Ref 333333',
										'Reference2'	=> 'Ref 444444',
										'AccountNumber' => '',
										'PartyAddress'	=> array(
											'Line1'					=> '15 ABC St',
											'Line2'					=> '',
											'Line3'					=> '',
											'City'					=> 'Dubai',
											'StateOrProvinceCode'	=> '',
											'PostCode'				=> '',
											'CountryCode'			=> 'AE'
										),
										
										'Contact'		=> array(
											'Department'			=> '',
											'PersonName'			=> 'Mazen',
											'Title'					=> '',
											'CompanyName'			=> 'Aramex',
											'PhoneNumber1'			=> '6666666',
											'PhoneNumber1Ext'		=> '155',
											'PhoneNumber2'			=> '789456',
											'PhoneNumber2Ext'		=> '456',
											'FaxNumber'				=> '',
											'CellPhone'				=> '',
											'EmailAddress'			=> 'mazen@aramex.com',
											'Type'					=> ''
										),
						),
						
						'ThirdParty' => array(
										'Reference1' 	=> '',
										'Reference2' 	=> '',
										'AccountNumber' => '',
										'PartyAddress'	=> array(
											'Line1'					=> '',
											'Line2'					=> '',
											'Line3'					=> '',
											'City'					=> '',
											'StateOrProvinceCode'	=> '',
											'PostCode'				=> '',
											'CountryCode'			=> ''
										),
										'Contact'		=> array(
											'Department'			=> '',
											'PersonName'			=> '',
											'Title'					=> '',
											'CompanyName'			=> '',
											'PhoneNumber1'			=> '',
											'PhoneNumber1Ext'		=> '',
											'PhoneNumber2'			=> '',
											'PhoneNumber2Ext'		=> '',
											'FaxNumber'				=> '',
											'CellPhone'				=> '',
											'EmailAddress'			=> '',
											'Type'					=> ''							
										),
						),
						
						'Reference1' 				=> 'Shpt 0001',
						'Reference2' 				=> '',
						'Reference3' 				=> '',
						'ForeignHAWB'				=> '',
						'TransportType'				=> 0,
						'ShippingDateTime' 			=> time(),
						'DueDate'					=> time(),
						'PickupLocation'			=> '',
						'PickupGUID'				=> '',
						'Comments'					=> 'Shpt 0001',
						'AccountingInstrcutions' 	=> '',
						'OperationsInstructions'	=> '',
						
						'Details' => array(
										'Dimensions' => array(
											'Length'				=> 10,
											'Width'					=> 10,
											'Height'				=> 10,
											'Unit'					=> 'cm',
											
										),
										
										'ActualWeight' => array(
											'Value'					=> 0.5,
											'Unit'					=> 'Kg'
										),
										
										'ProductGroup' 			=> 'DOM',
										'ProductType'			=> 'ONP',
										'PaymentType'			=> 'P',
										'PaymentOptions' 		=> '',
										'Services'				=> '',
										'NumberOfPieces'		=> 1,
										'DescriptionOfGoods' 	=> 'Docs',
										'GoodsOriginCountry' 	=> 'Jo',
										"ChargeableWeight"      => null,
										'CashOnDeliveryAmount' 	=> array(
											'Value'					=> 0,
											'CurrencyCode'			=> ''
										),
										
										'InsuranceAmount'		=> array(
											'Value'					=> 0,
											'CurrencyCode'			=> ''
										),
										
										'CollectAmount'			=> array(
											'Value'					=> 0,
											'CurrencyCode'			=> ''
										),
										
										'CashAdditionalAmount'	=> array(
											'Value'					=> 0,
											'CurrencyCode'			=> ''							
										),
										
										'CashAdditionalAmountDescription' => '',
										
										'CustomsValueAmount' => array(
											'Value'					=> 0,
											'CurrencyCode'			=> ''								
										),
										
										'Items' 				=> array(
											
										)
						),
				),
		),
		
			'ClientInfo'  			=> array(
										'AccountCountryCode'	=> 'JO',
										'AccountEntity'		 	=> 'AMM',
										'AccountNumber'		 	=> '20016',
										'AccountPin'		 	=> '221321',
										'UserName'			 	=> 'reem@reem.com',
										'Password'			 	=> '123456789',
										'Version'			 	=> '1.0',
										"Source"				=> '24'
									),

			'Transaction' 			=> array(
										'Reference1'			=> '001',
										'Reference2'			=> '', 
										'Reference3'			=> '', 
										'Reference4'			=> '', 
										'Reference5'			=> '',									
									),
			'LabelInfo'				=> array(
										'ReportID' 				=> 9729,
										'ReportType'			=> 'URL',
			),
	);
	/*$this->parammoneycollect = [
			'ClientInfo'  			=> [
				'AccountCountryCode'	=> '',
				'AccountEntity'		 	=> '',
				'AccountNumber'		 	=> '',
				'AccountPin'		 	=> '',
				'UserName'			 	=> '',
				'Password'			 	=> '',
				'Version'			 	=> ''
			],

			'Transaction' 			=> [
				'Reference1'			=> '', // extra param for response Order Id
				'Reference2'			=> '', // extra param for response
				'Reference3'			=> '', // extra param for response
				'Reference4'			=> '', // extra param for response
			],
			'LabelInfo'				=> [
				'ReportID' 				=> 9201,
				'ReportType'			=> 'URL',
			],
			'Shipments' => [
				0 => [
					'Shipper'	=> [
						'Reference1' => 'some ref',
						'AccountNumber' => '',
						'Contact'		=> [
							'PersonName'			=> 'Eureka Tech Academy',
							'CompanyName'			=> 'Eureka Tech Academy',
							'PhoneNumber1'			=> '5555555',
							'CellPhone'				=> '07777777',
							'EmailAddress'			=> 'michael@aramex.com',
						],
						"PartyAddress"=> [
			               "Line1"=> "Test",
			               "Line2"=> null,
			               "Line3"=> null,
			               "City"=> "Amman",
			               "StateOrProvinceCode"=> null,
			               "PostCode"=> null,
			               "CountryCode"=> "JO",
			               "Longitude"=> "0",
			               "Latitude"=> "0"
			            ],
					],

					'Consignee'	=> [
						'Reference1'	=> 'Ref 333333', // for response
						'Reference2'	=> 'Ref 444444', // for response
						'AccountNumber' => '20016', //Account Number 
						'Contact'		=> [
							'PersonName'			=> 'Mazen',//Person Name 
							'CompanyName'			=> 'Aramex',
							'PhoneNumber1'			=> '6666666', //Phone Number
							'CellPhone'				=> '0994728051', //Cell Phone 
							'EmailAddress'			=> 'mazen@aramex.com', // Email
						],
						'PartyAddress'	=> [
							'Line1'					=> 'Mecca St',//Line1 
							'Line2'					=> 'Mecca St',//Line2 
							'Line3'					=> 'Mecca St',//Line3
							'CountryCode'			=> 'JO', // Country Code
               				'City'=> 'Amman',
               				'PostCode' => '11110',
						],
								
					],
					"ThirdParty" => [
				        "Reference1" => "",
				        "Reference2" => "",
				        "AccountNumber" => "",
				        "PartyAddress" => [
				          "Line1" => "Test",
				          "Line2" => "",
				          "Line3" => "",
				          "City" => "Irbid",
				          "StateOrProvinceCode" => "",
				          "PostCode" => "",
				          "CountryCode" => "JO",
				          "Longitude" => 0,
				          "Latitude" => 0,
				          "BuildingNumber" => null,
				          "BuildingName" => null,
				          "Floor" => null,
				          "Apartment" => null,
				          "POBox" => null,
				          "Description" => ""
				        ],
				        "Contact"=> [
				          "Department" => "",
				          "PersonName" => "aramex",
				          "Title" => "",
				          "CompanyName" => "aramex",
				          "PhoneNumber1" => "009625515111",
				          "PhoneNumber1Ext" => "",
				          "PhoneNumber2" => "",
				          "PhoneNumber2Ext" => "",
				          "FaxNumber" => "",
				          "CellPhone" => "9627956000200",
				          "EmailAddress" => "test@test.com",
				          "Type" => ""
				        ]
				      ],
					'ShippingDateTime' => time(), // Should be Filled
					"DueDate"=> time(), // Should be Filled
        			"Comments"=> null, //Should Be Filled
        			"PickupLocation"=> null, // Should be Filled
			        "Attachments"=> null,
			        "ForeignHAWB"=> null,
			        // "CustomsAmount" => '0',
			        "TransportType"=> "0",
			        "PickupGUID"=> null,
			        "Number"=> null,
					'Details' => [						
						'ActualWeight' => [
							'Value'					=> 1,
							'Unit'					=> 'Kg'
						],
						
						'ProductGroup' 			=> 'DOM',
						'ProductType'			=> 'ONP',
						'PaymentType'			=> 'P',
						'PaymentOptions' 		=> 'Consignee Account Number',
						'NumberOfPieces'		=> 1,
						'DescriptionOfGoods' 	=> 'Money Collection',
						'GoodsOriginCountry' 	=> 'Jo',
										
						'CollectAmount'			=> [
							'Value'					=> '',
							'CurrencyCode'			=> 'JOD'
						],
									
						'Items' 				=> [],
				]
			]
		]
	];*/

	

		$this->param['ClientInfo']['AccountCountryCode']= $this::ACCCNTRYCD;
		$this->param['ClientInfo']['AccountEntity'] = $this::ACCENT;
		$this->param['ClientInfo']['AccountNumber'] = $this::ACCNUM;
		$this->param['ClientInfo']['AccountPin'] = $this::ACCPIN;
		$this->param['ClientInfo']['UserName'] = $this::USRNM;
		$this->param['ClientInfo']['Password'] = $this::PSRD;
		$this->param['ClientInfo']['Version'] = $this::VER;
		$this->param['Shipments'][0]['Shipper']['AccountNumber'] = $this::ACCNUM;

		$this->parammoneycollect['ClientInfo']['AccountCountryCode']= $this::ACCCNTRYCD;
		$this->parammoneycollect['ClientInfo']['AccountEntity'] = $this::ACCENT;
		$this->parammoneycollect['ClientInfo']['AccountNumber'] = $this::ACCNUM;
		$this->parammoneycollect['ClientInfo']['AccountPin'] = $this::ACCPIN;
		$this->parammoneycollect['ClientInfo']['UserName'] = $this::USRNM;
		$this->parammoneycollect['ClientInfo']['Password'] = $this::PSRD;
		$this->parammoneycollect['ClientInfo']['Version'] = $this::VER;
		$this->parammoneycollect['Shipments'][0]['Shipper']['AccountNumber'] = $this::ACCNUM;

		
	}


	public function fillShipmentObject($orderId , $time){
		$order = Order::find($orderId);

		$this->param['Shipments'][0]['Details']['PaymentOptions'] = "ACCT";
		$this->param['Shipments'][0]['Details']['CollectAmount']['Value'] = $order->selling_price;
		$this->param['Shipments'][0]['Details']['ActualWeight']= [
			'Value' => isset($order->weight) ?$order->weight : 2,
			'Unit' => 'Kg'
		];
		$this->param['Shipments'][0]['Details']['Items']['Weight']['Value'] = isset($order->weight) ?$order->weight : 2;
		if($order->payment_method=='cod'){

			$footer = Footer::find(1);
        	$currency_conversation = $footer->currency_conversation;
        	$usdprice = $order->total_price * $currency_conversation;
        	
        	$this->param['Shipments'][0]['Details']['Services'] = 'CODS';
			$this->param['Shipments'][0]['Details']['CashOnDeliveryAmount'] = [
			"CurrencyCode" => "JOD", "Value" => round($order->total_price, 2)
			];
		}
		

		$this->param['Transaction']['Reference1'] = $orderId;

		// $time = strtotime('today midnight +36 hours');
		// $this->param['Shipments'][0]['ShippingDateTime'] = date(DATE_ISO8601,$time + 96709);
		$strdate = $time;
		//"/Date(".$strdate.")/"
		$this->param['Shipments'][0]['ShippingDateTime'] = "/Date(".$strdate.")/" ;

		$duedate = $time + 96709 + 96709 + 96709;
		//"/Date(".$duedate.")/"
		$this->param['Shipments'][0]['DueDate'] = "/Date(".$duedate.")/";

		//dd($this->param);
	}

	protected function fillShipperInfo($orderId){
		//$supplierId = Order::find($orderId)->supplier_id;
		$orders = OrderItem::where('order_id','=',$orderId)->where(function ($query) {
            $query->where('type', '=', 'Kit')
                  ->orWhere('type', '=', 'Item');
        })->get()->first();
        $supplierId =$orders->supplier_id;
		$supplier = SupplierInfo::where('user_id' , $supplierId)->first();

		$this->param['Shipments'][0]['Shipper']['Contact']['PersonName'] = $supplier->name;
		$this->param['Shipments'][0]['Shipper']['Contact']['PhoneNumber1'] = $supplier->phone_number;
		$this->param['Shipments'][0]['Shipper']['Contact']['CellPhone'] = $supplier->cell_phone_number;
		$this->param['Shipments'][0]['Shipper']['Contact']['EmailAddress'] = $supplier->email;

		$this->param['Shipments'][0]['Shipper']['PartyAddress']['City'] = $supplier->city;
		$this->param['Shipments'][0]['Shipper']['PartyAddress']['Line1'] = $supplier->address_line1;
		$this->param['Shipments'][0]['Shipper']['PartyAddress']['Line2'] = $supplier->address_line2;
		$this->param['Shipments'][0]['Shipper']['PartyAddress']['Line3'] = $supplier->address_line3;
		$this->param['Shipments'][0]['Shipper']['PartyAddress']['StateOrProvinceCode'] = $supplier->province_code;
		$this->param['Shipments'][0]['Shipper']['PartyAddress']['PostCode'] = $supplier->post_code;
		$this->param['Shipments'][0]['Shipper']['PartyAddress']['CountryCode'] = $supplier->country_code;
	}

	public function fillConsigneeInfo($orderId)
	{
		$order = Order::find($orderId);
		$user = User::find($order->user_id);
		$this->param['Shipments'][0]['Consignee']['AccountNumber'] = $this::ACCNUM;

		$this->param['Shipments'][0]['Consignee']['Contact']['PersonName'] = $user->name;
		$this->param['Shipments'][0]['Consignee']['Contact']['CompanyName'] = $user->name;
		if($order->phone_number == ""){
			$this->param['Shipments'][0]['Consignee']['Contact']['PhoneNumber1'] = $user->phone;
		}else{
			$this->param['Shipments'][0]['Consignee']['Contact']['PhoneNumber1'] = $order->phone_number;
		}
		
		$this->param['Shipments'][0]['Consignee']['Contact']['CellPhone'] = $user->phone;
		$this->param['Shipments'][0]['Consignee']['Contact']['EmailAddress'] = $user->email;

		$this->param['Shipments'][0]['Consignee']['PartyAddress']['Line1'] = $order->line1;
		$this->param['Shipments'][0]['Consignee']['PartyAddress']['Line2'] = $order->line2;
		$this->param['Shipments'][0]['Consignee']['PartyAddress']['Line3'] = $order->line3;
		$this->param['Shipments'][0]['Consignee']['PartyAddress']['City'] = $order->city;
		$this->param['Shipments'][0]['Consignee']['PartyAddress']['CountryCode'] = $order->country_code;
		$this->param['Shipments'][0]['Consignee']['PartyAddress']['PostCode'] = $order->postal_code;
		
		//dd($this->param);
	}



	public function fillAddress($orderId){
		// Shipper
		$this->fillShipperInfo($orderId);
		// Consignee
		$this->fillConsigneeInfo($orderId);
	}

	public function getShipmentArray(){
		return $this->param;
	}

	public function addPickupsParams($orderId , $time){

		//$supplierId = Order::find($orderId)->supplier_id;
		$orders = OrderItem::where('order_id','=',$orderId)->where(function ($query) {
            $query->where('type', '=', 'Kit')
                  ->orWhere('type', '=', 'Item');
        })->get()->first();
        $supplierId =$orders->supplier_id;
		$supplier = SupplierInfo::where('user_id' , $supplierId)->first();

		// $time = strtotime('today midnight +36 hours');
		// dd(abs($time - strtotime("+3 days 1 hour")) , $time);
		$this->param['Pickup'] = [	
			'Reference1'=> '123123',
			'PickupLocation' =>"here",
			'Status' => 'Pending',
			'Comments' => '',
			'Vehicle' => '',
			'PickupDate' => '/Date('.(strtotime("+1 day") *1000).')/',
			//'/Date('.$time.')/'
			'ReadyTime' => '/Date('.($time + 96709).')/',
			'LastPickupTime' => '/Date('.($time + 96709).')/', // +26 hours
			'ClosingTime' => '/Date('.($time +103124).')/',//+28 hours
			'PickupContact' => [
				'PersonName'	=> $supplier->name,
				'CompanyName'	=> 'Eureka Tech Academy',
				'PhoneNumber1'	=> $supplier->phone_number,
				'PhoneNumber2'	=> '',
				'CellPhone'		=> $supplier->cell_phone_number,
				'EmailAddress'	=> $supplier->email,
				'Type'			=> ''
			],
			'PickupAddress' => [
				'Line1' => $supplier->address_line1,
				'Line2' => $supplier->address_line2,
				'Line3' => $supplier->address_line3,
				'CountryCode' => $supplier->country_code,
				'City' => $supplier->city,
				'PostCode' => $supplier->post_code 
			],
			'PickupItems' => [  
				0 => [
					'Reference1'=> '123123',
					'ProductGroup' => 'EXP',
					'Payment' => 'C',
					'ProductType' => 'EPX',
					'NumberOfPieces' => '1',
					"PackageType" => "",
					'ShipmentWeight' => [
						'Value' => isset($order->weight) ? $order->weight : '2' ,
						'Unit' => 'Kg'
					],
					'NumberOfShipments' => 1,
					'ShipmentVolume'=> [
						'Value'=> isset($order->volume) ? $order->volume : '50',
						'Unit'=>'Cm3'
					],
					"CashAmount" =>  null,
					"ExtraCharges" =>  null,
					"ShipmentDimensions" => [
						"Length" => 0.0,
						"Width" =>  0.0,
						"Height" => 0.0,
						"Unit" =>  "cm"
					],
					"Comments" =>  ""
				]
			
			]
		];
		
	}

	public function getPickupArray(){
		return $this->param;
	}

	public function updateOrderReceipts($orderId , $labelUrl , $shipmentId)
	{
		//echo "==".$orderId."==".$labelUrl."==".$shipmentId;
		$orderdata=Order::where('id' , $orderId)->update(['shipment_id' => $shipmentId , 'shipment_label_url' => $labelUrl]);
		return $orderdata;
	}
	public function trackingShipment($shipmentId){
		
		$params = array(
        'ClientInfo' =>array( 
                         'AccountCountryCode' => $this::ACCCNTRYCD,
                         'AccountEntity' => $this::ACCENT,
                         'AccountNumber' => $this::ACCNUM, // replace with our
                         'AccountPin' => $this::ACCPIN,
                         'UserName' => $this::USRNM,
                         'Password' => $this::PSRD,
                         'Version' => $this::VER,
                         'Source' => '24'
                        ),
        'Transaction' => array(
                            'Reference1' => '',
                            'Reference2' => '',
                            'Reference3' => '',
                            'Reference4' => '',
                            'Reference5' => '', 
                        ),
        'Shipments' => array(
                        $shipmentId
                    ),
        "GetLastTrackingUpdateOnly" => false
        );

	return $params;

	}
	protected function fillShipperInfoForMoney($orderId){
		//$supplierId = Order::find($orderId)->supplier_id;
		
		$supplier = SupplierInfo::where('user_id' , 1)->first();

		$this->parammoneycollect['Shipments'][0]['Shipper']['Contact']['PersonName'] = $supplier->name;
		$this->parammoneycollect['Shipments'][0]['Shipper']['Contact']['PhoneNumber1'] = $supplier->phone_number;
		$this->parammoneycollect['Shipments'][0]['Shipper']['Contact']['CellPhone'] = $supplier->cell_phone_number;
		$this->parammoneycollect['Shipments'][0]['Shipper']['Contact']['EmailAddress'] = $supplier->email;

		$this->parammoneycollect['Shipments'][0]['Shipper']['PartyAddress']['City'] = $supplier->city;
		$this->parammoneycollect['Shipments'][0]['Shipper']['PartyAddress']['Line1'] = $supplier->address_line1;
		$this->parammoneycollect['Shipments'][0]['Shipper']['PartyAddress']['Line2'] = $supplier->address_line2;
		$this->parammoneycollect['Shipments'][0]['Shipper']['PartyAddress']['Line3'] = $supplier->address_line3;
		$this->parammoneycollect['Shipments'][0]['Shipper']['PartyAddress']['StateOrProvinceCode'] = $supplier->province_code;
		$this->parammoneycollect['Shipments'][0]['Shipper']['PartyAddress']['PostCode'] = $supplier->post_code;
		$this->parammoneycollect['Shipments'][0]['Shipper']['PartyAddress']['CountryCode'] = $supplier->country_code;
	}

	public function fillConsigneeInfoForMoney($orderId)
	{
		$order = Order::find($orderId);
		$user = User::find($order->user_id);
		$this->parammoneycollect['Shipments'][0]['Consignee']['AccountNumber'] = $this::ACCNUM;

		$this->parammoneycollect['Shipments'][0]['Consignee']['Contact']['PersonName'] = $user->name;
		$this->parammoneycollect['Shipments'][0]['Consignee']['Contact']['CompanyName'] = $user->name;
		if($order->phone_number == ""){
			$this->parammoneycollect['Shipments'][0]['Consignee']['Contact']['PhoneNumber1'] = $user->phone;
		}else{
			$this->parammoneycollect['Shipments'][0]['Consignee']['Contact']['PhoneNumber1'] = $order->phone_number;
		}
		
		$this->parammoneycollect['Shipments'][0]['Consignee']['Contact']['CellPhone'] = $user->phone;
		$this->parammoneycollect['Shipments'][0]['Consignee']['Contact']['EmailAddress'] = $user->email;

		$this->parammoneycollect['Shipments'][0]['Consignee']['PartyAddress']['Line1'] = $order->line1;
		$this->parammoneycollect['Shipments'][0]['Consignee']['PartyAddress']['Line2'] = $order->line2;
		$this->parammoneycollect['Shipments'][0]['Consignee']['PartyAddress']['Line3'] = $order->line3;
		$this->parammoneycollect['Shipments'][0]['Consignee']['PartyAddress']['City'] = $order->city;
		$this->parammoneycollect['Shipments'][0]['Consignee']['PartyAddress']['CountryCode'] = $order->country_code;
		$this->parammoneycollect['Shipments'][0]['Consignee']['PartyAddress']['PostCode'] = $order->postal_code;

		/*$this->parammoneycollect['Shipments'][0]['Consignee']['PartyAddress']['Line1'] = 'Irbid';
		$this->parammoneycollect['Shipments'][0]['Consignee']['PartyAddress']['Line2'] = 'Irbid';
		$this->parammoneycollect['Shipments'][0]['Consignee']['PartyAddress']['Line3'] = 'Irbid';
		$this->parammoneycollect['Shipments'][0]['Consignee']['PartyAddress']['City'] = 'Irbid';
		$this->parammoneycollect['Shipments'][0]['Consignee']['PartyAddress']['CountryCode'] = 'JO';
		$this->parammoneycollect['Shipments'][0]['Consignee']['PartyAddress']['PostCode'] = '';*/

		
		
		//dd($this->param);
	}


	public function fillAddressForMoney($orderId){
		// Shipper
		$this->fillShipperInfoForMoney($orderId);
		// Consignee
		$this->fillConsigneeInfoForMoney($orderId);
	}
	public function fillShipmentObjectForMoney($orderId , $time){
		$order = Order::find($orderId);
		$this->parammoneycollect['Shipments'][0]['Details']['DescriptionOfGoods'] = "Money Collection";
		$this->parammoneycollect['Shipments'][0]['Details']['PaymentOptions'] = "";
		//ARCC
		$this->parammoneycollect['Shipments'][0]['Details']['CollectAmount']['Value'] = $order->selling_price;
		$this->parammoneycollect['Shipments'][0]['Details']['ActualWeight']= [
			'Value' => isset($order->weight) ?$order->weight : 2,
			'Unit' => 'Kg'
		];
		$this->parammoneycollect['Shipments'][0]['Details']['Items'] = [];
		if($order->payment_method=='cod'){

			$footer = Footer::find(1);
        	$currency_conversation = $footer->currency_conversation;
        	$usdprice = $order->total_price * $currency_conversation;
        	
        	$this->parammoneycollect['Shipments'][0]['Details']['Services'] = 'CODS';
			$this->parammoneycollect['Shipments'][0]['Details']['CashOnDeliveryAmount'] = [
			"CurrencyCode" => "JOD", "Value" => round($order->total_price,2)
			];
			// round($usdprice, 3)
		}
		

		$this->parammoneycollect['Transaction']['Reference1'] = $orderId;
		$strdate = $time + 96709 + 96709;
		$this->parammoneycollect['Shipments'][0]['ShippingDateTime'] = "/Date(".$strdate.")/";
		$duedate = $time + 96709 + 96709 + 96709;
		$this->parammoneycollect['Shipments'][0]['DueDate'] = "/Date(".$duedate.")/";
		//dd($this->parammoneycollect)
		
	}
	public function addPickupsParamsForMoney($orderId , $time){

		
        $supplierId = 1;
        $supplier = SupplierInfo::where('user_id' , $supplierId)->first();
		$this->parammoneycollect['Pickup'] = [	
			'Reference1'=> '123123',
			'PickupLocation' =>"here",
			'Status' => 'Pending',
			'PickupDate' => $time,
			'ReadyTime' => $time,
			'LastPickupTime' => $time + 96709, // +26 hours
			'ClosingTime' => $time +103124,//+28 hours
			'PickupContact' => [
				'PersonName'	=> $supplier->name,
				'CompanyName'	=> 'Eureka Tech Academy',
				'PhoneNumber1'	=> $supplier->phone_number,
				'CellPhone'		=> $supplier->cell_phone_number,
				'EmailAddress'	=> $supplier->email
			],
			'PickupAddress' => [
				'Line1' => $supplier->address_line1,
				'Line2' => $supplier->address_line2,
				'Line3' => $supplier->address_line3,
				'CountryCode' => $supplier->country_code,
				'City' => $supplier->city 
			],
			'PickupItems' => [
				'PickupItemDetail' => [
					'Reference1'=> '123123',
					'ProductGroup' => 'EXP',
					'Payment' => 'C',
					'ProductType' => 'EPX',
					'NumberOfPieces' => '1',
					'ShipmentWeight' => [
						'Value' => isset($order->weight) ? $order->weight : '2' ,
						'Unit' => 'Kg'
					],
					'NumberOfShipments' => 1,
					'ShipmentVolume'=> [
						'Value'=> isset($order->volume) ? $order->volume : '50',
						'Unit'=>'Cm3'
					]
				]
			]
		];
		
	}

	public function getPickupArrayForMoney(){
		return $this->parammoneycollect;
	}
	public function curlRequestdata($url,$data){
		

		$curl = curl_init();

		curl_setopt_array($curl, array(
		    CURLOPT_URL => $url,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_ENCODING => "",
		    CURLOPT_MAXREDIRS => 10,
		    CURLOPT_TIMEOUT => 30000,
		    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		    CURLOPT_CUSTOMREQUEST => "POST",
		    CURLOPT_POSTFIELDS => json_encode($data),
		    CURLOPT_HTTPHEADER => array(
		    	// Set here requred headers
		        "accept: */*",
		        "accept-language: en-US,en;q=0.8",
		        "content-type: application/json",
		    ),
		));

		$response = curl_exec($curl);
		$responsedata = simplexml_load_string($response);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		    return "cURL Error #:" . $err;
		} else {
		    return $responsedata;
		}
	}
}
