<?php

namespace App\Services; 
use Excel;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\GroupUser;
use App\SubscriptionModel;
use Carbon\Carbon;

class ExcelService {

    public static function import($file, $id){      

        $responseArray = []; 
        $group_id = $id;
        try {
            Excel::load( $file->getRealPath(), function ($reader) use (&$responseArray, $group_id) {
                foreach ($reader->takeRows(1000)->toArray() as $row) {

                    if ($row['email']==null && $row['phone'] == null &&  $row['name'] == null)
                        return ;
                    $email = $row['email'];
                    $password = ExcelService::random_str(8);
                    $hashed = Hash::make($password);

                    $user = User::where('email' , 'like', $email )->first();
                    if (!isset($user) || count($user) == 0){
                        $user = User::updateOrCreate(['id' => 0],[
                            'email' => $row['email'],
                            'name'=>$row['name'],
                            'country'=>$row['country'],
                            'password'=> $hashed,
                            'phone' => isset($row['phone']) ? $row['phone'] : 000  
                        ]);
                    }
                    else {
                        $user = User::updateOrCreate(['id' => $user->id], [
                            'email' => $row['email'],
                            'name'=>$row['name'],
                            'country'=>$row['country'],
                            'phone' => isset($row['phone']) ? $row['phone'] : 000  
                        ]);
                    
                    }
                    $responseArray[] = ['name' => $row['name'],
                     'phone' => $row['phone'],
                     'email' => $row['email'],
                     'country' => $row['country'],
                     'subscription' => $row['subscription'],
                     'amount'=> $row['amount']
                    ];
                    $subscriptionModel = $row['subscription'];

                    if (isset($subscriptionModel) &&  $subscriptionModel != ''){
                        
                        $subscriptionModel = SubscriptionModel::where('name_en' , 'like', $subscriptionModel)->get()->first();
                        if (isset($subscriptionModel)){
                            $amount =0 ;
                            if (isset($row['amount']) && $row['amount'] != ""){
                                $amount = $row['amount'];
                            }
                            
                            $payment = PaymentService::makePayment($amount , 0 , '',$subscriptionModel->id , $user->id);


                            $startDate = Carbon::today();
                            $endDate = Carbon::today()->addDays($subscriptionModel->period_in_days);

                            $subscription = SubscriptionService::subscribe($user->id, $subscriptionModel->id, $payment, -1, 0 , $startDate , $endDate);
                        }
                    }
                    $groupUser = GroupUser::updateOrCreate(['id' => 0],[
                        'group_id' =>$group_id,
                        'user_id' => $user->id 
                    ]);
                }
                
            });
        }
        catch(\Exception $e){
            $responseArray['error'] ='error';
            return $responseArray;
        }
        return $responseArray;
        
    }
    private static function random_str($length, $keyspace = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces []= $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }


    public static function exportExcel($data){
        $arr = array_merge([$data['headers']],$data['data'] );
        return Excel::create('excel', function($excel) use ($arr) {
            $excel->sheet('mySheet', function($sheet) use ($arr) {
                $sheet->fromArray($arr);
            });
        })->download('xlsx');
    }

    public static function exportCSV($data){
        $arr = array_merge([$data['headers']],$data['data'] );
        return Excel::create('excel', function($excel) use ($arr) {
            $excel->sheet('mySheet', function($sheet) use ($arr) {
                $sheet->fromArray($arr);
            });
        })->download('csv');
    }
    

}