<?php

namespace App\Services; 
use PDF;
use Excel;

class PDFService {   

    public static function exportFunction($data){
        $pdf = PDF::loadView('pdf.invoice' ,  $data);
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 5000);
        $pdf->setOption('enable-smart-shrinking', true);
        $pdf->setOption('no-stop-slow-scripts', true);
        return $pdf->download('invoice.pdf');
    }

}

