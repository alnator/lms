<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use App\Services\MediaService;
use App\Services\S3Service;
use App\Course;
use App\Kit;
use App\KitImage;
use App\OrderItem;
use App\Order;
use App\OrderStates;
use App\Level;
use App\Video;
use App\KitItem;
use App\User;
use App\Album;
use App\Subscription;
use App\SubscriptionModel;
use App\Downloadable;
use App\CourseCategory;
use App\Payment;
use App\TeacherFees;
use App\Coupon;
use App\StripeCustomer;
use App\StripePlan;
use App\Footer;
use App\PaymentMethod;
use App\Achievement;
use App\AbandonedCart;
use Carbon\Carbon;
use App\UserProgress;
use App\Star;
use App\GroupUser;
use App\Email;
use App\Quiz;
use App\QuizChoice;
use App\QuizReport;
use App\Group;
use App\Item;
use App\Session;
use App\Slug;
use Illuminate\Support\Facades\Hash;
use App\StarsText;
use App\MenuItem;
use App\ItemImage;
use App\GalleryItem;
use Auth;
use Session as Sess;
use App\Testimonial;
use App\Vision;
use App\Bannerimages;
use Storage;
use App\Emailtemplates;
class AdminService {

    CONST vimeoToken = 'f79c4a0561ff57eb45da08a9c1ae0903';
    //Categories

    public static function getCategoriesData($filter){

        $index = $filter ? $filter['pageIndex'] : 0 ;

        $categories = CourseCategory::select(['id','name_en','name_ar']);
        if (!empty($filter['id']))
        {
            $categories->where('id','=',$filter['id']);
        }

        if (!empty($filter['name_en']))
        {
            $categories->where('name_en','like','%'.$filter['name_en'].'%');
        }

        if (!empty($filter['name_ar']))
        {
            $categories->where('name_ar', 'like', '%'.$filter['name_ar'].'%');
        }


        $categories->orderBy('id','desc');
        $result['total'] = $categories->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$categories->take(10)->skip($skip)->get();

        return $result;
    }

    public static function CreateUpdateCategory(Request $request){
        $category = CourseCategory::updateOrCreate(['id' => isset($request->id) ? $request->id : 0 ] , [
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en
        ]);
    }

    //Courses

    public static function getCoursesData($filter){

        $index = $filter ? $filter['pageIndex'] : 0 ;

        $courses = Course::select(['id','lang_id','title_ar','title_en' , 'created_by' , 'updated_by'])->with('updatedBy')->with('createdBy');
        if (!empty($filter['id']))
        {
            $courses->where('id','=',$filter['id']);
        }

        if (isset($filter['lang_id'])){
            $courses->where('lang_id' , '=' , $filter['lang_id']);
        }

        if (isset($filter['title_ar'])){
            $courses->where('title_ar' , 'like' , '%'.$filter['title_ar'].'%');
        }

        if (isset($filter['title_en'])){
            $courses->where('title_en' , 'like' , '%'.$filter['title_en'].'%');
        }

        if (isset($filter['updated_by']['email'])){
            $courses->whereHas('updatedBy' , function($query) use ($filter){
                $query->where('email' , 'like' , '%'.$filter['updated_by']['email'].'%');
            });
        }
        if (isset($filter['created_by']['email'])){
            $courses->whereHas('createdBy' , function($query) use ($filter){
                $query->where('email' , 'like' , '%'.$filter['created_by']['email'].'%');
            });
        }
        $courses->orderBy('id','desc');
        $result['total'] = $courses->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$courses->take(10)->skip($skip)->get();
        return $result;
    }

    public static function updateCreateCourse(Request $request){

        $course = Course::updateOrCreate(['id'=> isset($request->id)? $request->id : 0] , [
            'title_en'=>$request->title_en,
            'title_ar'=>$request->title_ar,
            'level_id'=>$request->level_id,
            'lang_id'=>$request->lang,
            'overview_ar'=>$request->overview_ar,
            'overview_en'=> $request->overview_en,
            'description_ar'=>$request->description_ar,
            'description_en'=>$request->description_en,
            'text_ar'=>$request->editor_ar,
            'text_en'=>$request->editor,
            'course_estimated_time'=>$request->course_estimated_time,
            'price'=>$request->price,
            'intro_video_path' =>$request->video,
            'active' => $request->active == 'on'  || $request->active == '1'? 1 : 0 ,
            'slug' => strtolower($request->slug),
            'kit_id' => (isset($request->kit_id)) ? $request->kit_id : '',
            'is_youtube' =>(isset($request->is_youtube)) ? $request->is_youtube : 0,
            'free' =>(isset($request->free)) ? $request->free : 0,
            'user_id'=>$request->user_id,
            'emonth'=>$request->emonth,
        ]);

        if ($request->is_youtube == 1){
            $course->url_identifier = $request->url_identifier;
            $course->update();
        }


        self::handleAttachements($course->id, $request);
        if (isset($request->id)){
            $course->updated_by = Auth::user()->id;
            if (isset($course)){

                $course->image_path = !$request->hasFile('image') ?$request->orginal_image_path  : MediaService::saveImage($request->file('image'));

                $course->wide_image_path = !$request->hasFile('wide_image_path') ? $request->orginal_wide_image_path  : MediaService::saveImage($request->file('wide_image_path'));
            }
            $course->update();

        }
        else {
            $course->created_by = Auth::user()->id;
            $course->image_path = MediaService::saveImage($request->file('image'));
            $course->wide_image_path =  MediaService::saveImage($request->file('wide_image_path'));
            $course->update();
        }
    }

    public static function handleAttachements($courseId , $data){
        $id = 0 ;
        if (isset($data['attachement_name_en']) || isset($data['attachement_name_ar']))
        {
            if (isset($data['attachement_id']))
            {
                $id = $data['attachement_id'];
            }
            $update = [
                'name_en' => $data['attachement_name_en'],
                'name_ar' => $data['attachement_name_ar'],
                'course_id' => $courseId
            ];

            if($data->hasFile('attachement_file_ar'))
            {
                $filename = S3Service::updateFileToS3($data->file('attachement_file_ar'));
                $update['file_path_ar'] = $filename;
            }
            if($data->hasFile('attachement_file_en'))
            {
                $filename = S3Service::updateFileToS3($data->file('attachement_file_en'));
                $update['file_path_en'] = $filename;
            }

            Downloadable::updateOrCreate(['id' => $id], $update);
        }

    }


    public static function getCourseUsersData($filter,$id){
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $subscription = Subscription::select('id', 'user_id', 'payment_id','start_date' , 'end_date' , 'status','course_id')->where('course_id' , '=', $id);

        if(isset($filter['start_date'])){
            $subscription->where('start_date', 'like' ,  '%'.$filter['start_date'].'%');
        }

        if(isset($filter['end_date'])){
            $subscription->where('end_date', 'like' ,  '%'.$filter['end_date'].'%');
        }

        if(isset($filter['status'])){
            $subscription->where('status', 'like' ,  '%'.$filter['status'].'%');
        }

        if(isset($filter['user_id'])){
            $subscription->where('user_id', '=' , $filter['user_id']);
        }

        if(isset($filter['payment_id'])){
            $subscription->where('payment_id', '=' , $filter['payment_id']);
        }

        if(isset($filter['id'])){
            $subscription->where('id', '=' , $filter['id']);
        }

        $subscription->orderBy('id','desc');
        $result['total'] = $subscription->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$subscription->take(10)->skip($skip)->get();

        return $result;
    }


    public static function AddCourseUser($request){
        $payment = Payment::updateOrCreate(['id' => 0] , [
            'user_id' => $request->user_id,
            'amount' => $request->amount,
            'payment_method_id' => $request->payment_method_id
        ]);
        $subscription = SubscriptionModel::find($request->subscription_model_id);
        return Subscription::updateOrCreate(['course_id' => $request->id ,'user_id'=> $request->user_id ],[
            'start_date' => Carbon::now(),
            'end_date' => Carbon::now()->addDays($subscription->period_in_days),
            'subscription_model_id' => $subscription->id,
            'user_id' => $request->user_id,
            'status' => 'active',
            'payment_id' => $payment->id,
            'payment_method_id' => $request->payment_method_id
        ]);
    }
    // Levels

    public static function getLevelsData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;
        $levels = Level::select('name_ar', 'name_en' , 'id' , 'updated_by' , 'created_by')->with('updatedBy')->with('createdBy');

        if (isset($filter['name_en'])){
            $levels->where('name_en' ,'like',  '%'.$filter['name_en'].'%');
        }

        if (isset($filter['name_ar'])){
            $levels->where('name_ar' ,'like',  '%'.$filter['name_ar'].'%');
        }

        if (isset($filter['id'])){
            $levels->where('id' ,'=', $filter['id']);
        }

        if (isset($filter['updated_by']['email'])){
            $levels->whereHas('updatedBy' , function($query) use ($filter){
                $query->where('email' , 'like' , '%'.$filter['updated_by']['email'].'%');
            });
        }
        if (isset($filter['created_by']['email'])){
            $levels->whereHas('createdBy' , function($query) use ($filter){
                $query->where('email' , 'like' , '%'.$filter['created_by']['email'].'%');
            });
        }

        $levels->orderBy('id','desc');
        $result['total'] = $levels->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$levels->take(10)->skip($skip)->get();
        return $result;
    }

    public static function createUpdateLevel(Request $request)
    {
        $level=  Level::updateOrCreate(['id' => isset($request->id)? $request->id : 0 ] , [
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'slug' => strtolower($request->slug),
            'active'=> $request->active == 'on' ? 1 : 0,
            'description_ar' => $request->description_ar,
            'description_en' => $request->description_en,
        ]);
        if ($request->hasFile('image_path_en')){
            $level->image_path_en = MediaService::saveImage($request->image_path_en);
        }
        if ($request->hasFile('image_path_ar')){
            $level->image_path_ar = MediaService::saveImage($request->image_path_ar);
        }
        if ($request->hasFile('icon_path')){
            $level->icon_path = MediaService::saveImage($request->icon_path);
        }
        if(isset($request->id)){
            $level->updated_by = Auth::user()->id;
        }
        else {
            $level->created_by = Auth::user()->id;
        }
        $level->update();
    }

    //Sessions

    public static function getSessionData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;
        $session = Session::select('id', 'session_number', 'session_number_ar', 'order', 'course_id', 'created_by', 'updated_by')->with('video')->with('course')->with('updatedBy')->with('createdBy');
        if(isset($filter['id'])){
            $session->where('id', '=', $filter['id']);
        }
        if(isset($filter['session_number'])){
            $session->where('session_number' , 'like', '%'.$filter['session_number'].'%');
        }
        if(isset($filter['order'])){
            $session->where('order' , '=', $filter['order']);
        }
        if(isset($filter['course_id'])){
            $session->whereHas('course' , function ($query) use ($filter){
                $query->where('title_en' , 'like' ,'%'.$filter['course_id'].'%');
            });
        }

        if (isset($filter['updated_by']['email'])){
            $session->whereHas('updatedBy' , function($query) use ($filter){
                $query->where('email' , 'like' , '%'.$filter['updated_by']['email'].'%');
            });
        }
        if (isset($filter['created_by']['email'])){
            $session->whereHas('createdBy' , function($query) use ($filter){
                $query->where('email' , 'like' , '%'.$filter['created_by']['email'].'%');
            });
        }

        $session->groupBy('course_id' , 'order', 'session_number' , 'session_number_ar' , 'course_id' , 'created_by' , 'updated_by', 'id' );
        $result['total'] = $session->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$session->take(10)->skip($skip)->get();
        // foreach ($result['data'] as $row) {
        // 	$row->course_id = Course::find($row->course_id)->title_en;
        // }
        return $result;
    }

    public static function createUpdateSession($request){
        $session = Session::updateOrCreate(['id'=>$request->id] ,[
            'session_number'=> $request->session_number,
            'session_number_ar'=>$request->session_number_ar,
            'course_id' => $request->course_id,
            'order' => $request->order
        ]);
        if (!isset($request->id)){
            $session->created_by = Auth::user()->id;
        }else {
            $session->updated_by = Auth::user()->id;
        }
        $session->update();
    }

    //Videos

    public static function getVideosData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;
        $videos = Video::select('id','name_ar','name_en','course_id','user_id','order','path','estimated_time','active','is_youtube' , 'created_by' , 'updated_by')->with('updatedBy')->with('createdBy');

        if (isset($filter['name_en'])){
            $videos->where('name_en' ,'like', '%'.$filter['name_en']. '%');
        }

        if (isset($filter['name_ar'])){
            $videos->where('name_ar' ,'like',  '%'.$filter['name_ar'].'%');
        }

        if (isset($filter['id'])){
            $videos->where('id' ,'=', $filter['id']);
        }

        if (isset($filter['course_id'])){
            $courses = Course::select('id')->where('title_en' , 'like', '%'.$filter['course_id'].'%' )->get()->toArray();
            $videos->whereIn('course_id',$courses);
        }

        if (isset($filter['order'])){
            $videos->where('order' ,'=', $filter['order']);
        }

        if (isset($filter['path'])){
            $videos->where('path' ,'like', $filter['path']);
        }

        if (isset($filter['estimated_time'])){
            $videos->where('estimated_time' ,'=', $filter['estimated_time']);
        }

        if (isset($filter['active'])){
            $videos->where('active' ,'=', strtolower($filter['active']) == "yes"? 1 : 0 );
        }

        if (isset($filter['is_youtube'])){
            $videos->where('is_youtube' ,'=', strtolower($filter['is_youtube']) == "yes"? 1 : 0 );
        }
        if (isset($filter['updated_by']['email'])){
            $videos->whereHas('updatedBy' , function($query) use ($filter){
                $query->where('email' , 'like' , '%'.$filter['updated_by']['email'].'%');
            });
        }
        if (isset($filter['created_by']['email'])){
            $videos->whereHas('createdBy' , function($query) use ($filter){
                $query->where('email' , 'like' , '%'.$filter['created_by']['email'].'%');
            });
        }


        $videos->orderBy('id','desc');
        $result['total'] = $videos->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$videos->take(10)->skip($skip)->get();

        foreach ($result['data'] as $row) {
            $row->active  = ($row->active == 1)? 'Yes' : 'No';
            $row->is_youtube = ($row->is_youtube == 1) ? 'Yes' : 'No';
            $user = User::find($row->user_id);
            if (isset($user->name))
                $row->user_id = $user->name;
            else
                $row->user_id = "<i>Unknown</i>";
            $course = Course::find($row->course_id);
            $row->course_id = isset($course)? $course->title_en: '';
        }
        return $result;
    }

    public static function craeteUpdateVideo(Request $request){

        if(!$request->estimated_time) {
            $request->estimated_time = self::vimeoVideoDuration($request->url_identifier);
        }

        $video = Video::updateOrCreate(['id' => isset($request->id) ?$request->id : 0 ], [
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'description_en' => $request->description_en ,
            'description_ar' =>  $request->description_ar,
            'session_id' => $request->session_id,
            'course_id' => $request->course_id,
            'user_id' =>$request->user_id,
            'order' =>$request->order,
            'estimated_time' => $request->estimated_time,
            'url_identifier' => $request->url_identifier,
            'active' =>(isset($request->active)) ?$request->active : 0 ,
            'is_youtube' =>(isset($request->is_youtube)) ? $request->is_youtube : 0,
            'free' =>(isset($request->free)) ? $request->free : 0
        ]);

        if ($request->hasFile('video')){
            $s3path = S3Service::uploadFileToS3($request);
            if ($s3path[0] == '/'){
                $video->path = Video::CLOUDFRONTDOMAIN .  $s3path;
                $video->update();

                $video_path = MediaService::saveVideo($request->file('video'));
                $getID3 = new \getID3;
                $file = $getID3->analyze($video_path);
                //print_r($file);
                $video->estimated_time = $file['playtime_seconds'];
                $video->update();
                $video_path1=public_path(str_replace('public/','',$video_path));
                unlink($video_path1);
            }
            else {
                return redirect(route('videos-error-admin'));
            }
        }


        if ($request->hasFile('image_path')){
            $video->image_path = MediaService::saveImage($request->image_path);
        }


        if (isset($request->id)){
            $video->updated_by = Auth::user()->id;
        }
        else {
            $video->created_by = Auth::user()->id;
        }
        $video->update();
        return redirect(route('videos-admin'));
    }
    public static function craeteUpdateVideoUpload(Request $request){
        $video = Video::updateOrCreate(['id' => isset($request->id) ?$request->id : 0 ], [
            'is_youtube' => 0
        ]);
        /*$dir = public_path('videos');
       if ($dh = opendir($dir)) {
         while ($file = readdir($dh)) {
           $filename = realpath($dir.'/'.$file);
           //echo '('.(is_file($filename) ? '' : 'NOT ').'file) ('.(is_readable($filename) ? '' : 'NOT ').'readable) ('.(file_exists($filename) ? '' : 'NOT ').'exists) = '.$filename."\n";

         }
       }*/

        if ($request->hasFile('video')){
            $s3path = S3Service::uploadFileToS3($request);
            if ($s3path[0] == '/'){
                $video->path = Video::CLOUDFRONTDOMAIN .  $s3path;
                $video->update();

                $video_path = MediaService::saveVideo($request->file('video'));
                $getID3 = new \getID3;
                $file = $getID3->analyze($video_path);
                //print_r($file);
                $video->estimated_time = $file['playtime_seconds'];
                $video->update();
                $video_path1=public_path(str_replace('public/','',$video_path));
                unlink($video_path1);

            }
            else {
                //return redirect(route('videos-error-admin'));
                return response()->json(['error'=>'You have not upload file.']);
            }

        }

        if (isset($request->id)){
            $video->updated_by = Auth::user()->id;
        }
        else {
            $video->created_by = Auth::user()->id;
        }
        $video->update();
        return response()->json(['success'=>'You have successfully upload file.']);
    }
    //Quizes

    public static function getQuizesData($videoId , $filter, $type){

        $index = $filter ? $filter['pageIndex'] : 0 ;
        $quizes = Quiz::select('id', 'video_id', 'quiz_statement','status')->where('video_id', '=', $videoId)->where('type', '=', $type);

        if (isset($filter['id'])){
            $quizes->where('id', '=', $filter['id']);
        }
        if (isset($filter['quiz_statement'])){
            $quizes->where('quiz_statement','like', '%'.$filter['quiz_statement'].'%');
        }

        $quizes->orderBy('id','desc');
        $result['total'] = $quizes->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$quizes->take(10)->skip($skip)->get();
        foreach ($result['data'] as $row) {
            if($type=="video"){
                $vid = Video::find($row->video_id);
                $row->video_name = $vid->name_en;
                $row->course_id = Course::find($vid->course_id)->title_en;
            }else{

                $vid = Session::find($row->video_id);
                $row->video_name = $vid->session_number;
                $row->course_id = Course::find($vid->course_id)->title_en;
            }
        }

        return $result;
    }
    // public static function createUpdateQuiz($videoId , $request){
    // 	Quiz::updateOrCreate(['id' => isset($request->id) ? $request->id : 0 ],[
    // 		'videoId'=> $videoId,
    // 		'quiz_statement' => $request->quiz_statement
    // 	]);
    // 	foreach ($request->choices as $choice) {
    // 		QuizChoice::updateOrCreate(['id'=> isset($choice->id) ? $choice->id : 0 ],[
    // 			'quiz_id'=> $request->id,
    // 			'choice_statement' => $choice->choice_statement,
    // 			'correct' => $choice->correct
    // 		]);
    // 	}
    // }
    public static function createEditQuiz($request){

        $quizes_count = Quiz::where('video_id',$request->video_id)->count();


        $quiz = Quiz::updateOrCreate(['id' => $request->quiz_id] , [
            'quiz_statement'=>$request->statement,
            'video_id' => $request->video_id,
            'quiz_statement_ar' => $request->statement_ar
        ]);

        if($quizes_count >= 4 ){
            $quiz_up = Quiz::where('video_id', $request->video_id)
                ->update(['status' => "Active"]);

        }


        $quiz_up = Quiz::where('id', $quiz->id)
            ->update(['type' => $request->type]);

        return $quiz->id;
    }

    public static function createEditChoice($request){
        $correct = $request->correct == "true" ? 1 : 0;
        // dd($correct);
        $choice = QuizChoice::updateOrCreate(['id' => isset($request->choice_id) ?$request->choice_id : 0 ],[
            'choice_statement' => $request->statement ,
            'choice_statement_ar' => $request->statement_ar ,
            'correct' => $correct,
            'quiz_id' => $request->quiz_id
        ]);
        return $choice->id;
    }


    //Quiz Reports

    public static function getQuizReportData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;
        $reports = QuizReport::select('user_id' , 'course_id', 'video_id' ,'choice_id' ,'quiz_id', 'correct', 'created_at','id');

        if (isset($filter['user']['group_user'][0]['group'][0]['name'])){
            $reports->with(['user'=> function ($query) use ($filter){
                $query->With(['groupUser' => function ($query) use ($filter){
                    $query->with(['group' => function ($query)use ($filter){
                        $query->where('name' , 'like', '%'.$filter['user']['group_user'][0]['group'][0]['name'].'%');
                    }]);
                }]);
            }]);
        }else {
            $reports->with(['user'=> function ($query){
                $query->With(['groupUser' => function ($query){
                    $query->with('group');
                }]);
            }]);
        }


        if(isset($filter['user_id'])){
            $user = User::select('id')->where('email' , 'like', '%'.$filter['user_id'].'%')->get()->toArray();
            $reports->whereIn('user_id' , $user);
        }
        if(isset($filter['course_id'])){
            $course = Course::select('id')->where('title_en' , 'like', '%'.$filter['course_id'].'%')->get()->toArray();
            $reports->whereIn('course_id' , $course);
        }
        if(isset($filter['video_id'])){
            $videos = video::select('id')->where('name_en' , 'like', '%'.$filter['video_id'].'%')->get()->toArray();
            $reports->whereIn('video_id' , $videos);
        }
        if(isset($filter['quiz_id'])){
            $quizes = Quiz::select('id')->where('quiz_statement' , 'like', '%'.$filter['quiz_id'].'%')->get()->toArray();
            $reports->whereIn('quiz_id' , $quizes);
        }
        if (isset($filter['choice_id'])){
            $quizchoices = QuizChoice::select('id')->where('choice_statement' , 'like', '%'.$filter['choice_id'].'%')->get()->toArray();
            $reports->whereIn('choice_id' , $quizchoices);
        }
        if (isset($filter['correct'])){
            if (strtolower($filter['correct']) == 'yes')
                $reports->where('correct', '=', 1);
            else
                $reports->where('correct', '=', 0);
        }

        $reports->orderBy('id','desc');

        $result['total'] = $reports->count();
        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$reports->take(10)->skip($skip)->get();

        foreach ($result['data'] as $row) {
            $row->user_id = User::find($row->user_id)->email;
            $row->course_id = Course::find($row->course_id)->title_en;
            if(Quiz::find($row->quiz_id)->type == 'video'){
                $row->video_id =Video::find($row->video_id)->name_en;
            }else{

                $row->video_id =Session::find($row->video_id)->session_number;
            }
            $row->quiz_id = Quiz::find($row->quiz_id)->quiz_statement;
            $row->choice_id = QuizChoice::find($row->choice_id)->choice_statement;
            $row->correct = $row->correct ==1 ? 'Yes' : 'No';
        }
        return $result;
    }
    //Users

    public static function getUsersData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;
        $users = User::select('id','name','email','active' , 'supplier' ,'teacher');

        if (isset($filter['name'])){
            $users->where('name' ,'like',  '%'.$filter['name'].'%');
        }

        if (isset($filter['email'])){
            $users->where('email' ,'like',  '%'.$filter['email'].'%');
        }

        if (isset($filter['id'])){
            $users->where('id' ,'=', $filter['id']);
        }

        $users->orderBy('id','desc');

        $result['total'] = $users->count();
        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$users->take(10)->skip($skip)->get();

        foreach ($result['data'] as $row) {
            $star = Star::where('user_id', '=', $row->id )->get();
            if($star->count() != 0 )
                $row->star = 1;
            else
                $row->star = 0;
        }

        return $result;
    }
    public static function createUpdateUser($request){
        $hashed = $request->password;
        if (Hash::needsRehash($hashed)) {
            $hashed = Hash::make($hashed);
        }
        $token = bin2hex(random_bytes(20));
        if (isset($request->id)){
            $userBeforeModification = User::find($request->id);
            $lastPassword = $userBeforeModification->password;
        }

        $user = User::updateOrCreate(['id' => isset($request->id)? $request->id : 0], [
            'name'=>$request->name,
            'age'=>$request->age,
            'phone'=>$request->phone,
            'email'=>$request->email,
            'remember_token'=>$token,
            'active' => isset($request->active) ? 1 : 0,
            'supplier' => $request->role ==3 ? 1 : 0,
            'teacher' => $request->role == 2 ? 1 : 0,
            'password' => '' ,
            'admin' => isset($request->admin) ||  $request->admin == 'on' ? 1 : 0,
            'country'=>$request->country
        ]);
        if (isset($request->password)){
            $user->password = $hashed;
        }
        else {
            $user->password = $lastPassword;
        }
        if ($request->hasFile('image_path')){
            $user->image_path = MediaService::saveImage($request->file('image_path'));
        }
        $user->save();

    }



    public static function getUserCourses($id){
        $userCourses = Subscription::where('user_id', '=', $id)->get();
        $flag = 0;
        $courses = [];
        foreach ($userCourses as $course) {
            if ($course->course_id == 0){
                $flag =1 ;
                break;
            }
            $cour = Course::find($course->course_id);
            array_push($courses , $cour);
        }
        if ($flag == 1){
            $courses = Course::get();
        }
        $result['data'] = $courses ;
        return $result;
    }
    public static function getUserCourseProgressData($filter, $userId ,$courseId )
    {
        // 	$course = Course::find($courseId);
        $course = Course::with(['session' => function ($query){
            $query->with('video');
        }])->find($courseId);
        $sessionCounter = 0;
        $videoCounter = 0;
        foreach ($course->session as $session) {
            $counter = 0;
            foreach ($session->video as $vid) {
                $userProgress = UserProgress::where('user_id', '=',$userId)->where('video_id','=',$vid->id)->get();
                if($userProgress->count() != 0 ){
                    $vid->done = 1 ;
                    $counter++;
                    $videoCounter++;
                }
            }

            $sessionVideoCount = $session->video->count();
            if($counter == $sessionVideoCount){
                $session->done = 1;
                $sessionCounter++;
            }
            else {
                $session->done = 0;
            }
            if( $sessionVideoCount != 0 )
                $session->progress = intval($counter / $sessionVideoCount * 100) .' %';


        }
        $video = Video::where('course_id' , '=' , $courseId)->get();
        if($sessionCounter = $course->session->count()){
            $course->done = 1;
        }
        $count = $video->count();
        $course->progress = 0 ;
        if ($count != 0 )
            $course->progress = $videoCounter / $count *100;

        $result['total'] = $count;
        $result['data'] = $course->session;
        return $result;
    }
    public static function starUser($id){
        $star = Star::where('user_id', '=' , $id)->get();
        if ($star->count() != 0 ){
            Star::where('user_id' , '=', $id)->delete();
            return 'removed';
        }
        else {
            $user = User::find($id);
            Star::updateOrCreate(['id'=> 0] ,[
                'user_id' => $id ,
                'image_path' => $user->image_path
            ]);
            return 'ok';
        }
    }


    // Downloadables


    public static function getFilesData($filter){

        $index = $filter ? $filter['pageIndex'] : 0 ;
        $files = Downloadable::select('id','name_ar','name_en', 'file_path_ar' , 'file_path_en', 'video_id','course_id');

        if (isset($filter['name'])){
            $files->where('name_en' ,'like',  '%'.$filter['name'].'%');
        }

        if (isset($filter['email'])){
            $files->where('email' ,'like',  '%'.$filter['email'].'%');
        }

        if (isset($filter['file_path_ar'])){
            $files->where('file_path_ar' ,'like',  '%'.$filter['file_path_ar'].'%');
        }

        if (isset($filter['file_path_en'])){
            $files->where('file_path_en' ,'like',  '%'.$filter['file_path_en'].'%');
        }

        if (isset($filter['id'])){
            $files->where('id' ,'=', $filter['id']);
        }

        if (isset($filter['video_id'])){
            $files->where('video_id' ,'=', $filter['video_id']);
        }

        if (isset($filter['course_id'])){
            $files->where('course_id' ,'=', $filter['course_id']);
        }


        $files->orderBy('id','desc');
        $result['total'] = $files->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$files->take(10)->skip($skip)->get();

        return $result;
    }

    public static function CreateUpdateFiles(Request $request){
        $file = Downloadable::updateOrCreate(['id' => isset($request->id) ?$request->id : 0 ] , [
            'name_en' =>$request->name_en,
            'name_ar' =>$request->name_ar,
            // 'file_path_ar' => $request->hasFile('file_path_ar') ?  MediaService::saveImage($request->file_path_ar) : '' ,
            // 'file_path_ed' => $request->hasFile('file_path_en') ?  MediaService::saveImage($request->file_path_en) : '',
            'video_id' => $request->video_id,
            'course_id' => $request->course_id
        ]);
    }

    // Payments

    public static function getPaymentsData($filter , $pdf = 0){

        $index = $filter ? $filter['pageIndex'] : 0 ;

        $paymentsRecords  = Payment::select('id','user_id','amount','invoice_id','payment_method_id' ,'coupon' ,'created_at')->with(['user','paymentMethod']);

        if(isset($filter['id'])){
            $paymentsRecords->where('id','=', $filter['id']);
        }

        if(isset($filter['user_id'])){
            $paymentsRecords->where('user_id','=', $filter['user_id']);
        }

        if(isset($filter['amount'])){
            $paymentsRecords->where('amount','=', $filter['amount']);
        }

        if(isset($filter['invoice_id'])){
            $paymentsRecords->where('invoice_id','=' , $filter['invoice_id']);
        }

        if(isset($filter['coupon'])){
            $paymentsRecords->where('coupon', '=' , $filter['coupon']);
        }



        $paymentsRecords->orderBy('id','desc');
        $result['total'] = $paymentsRecords->count();

        if ($pdf == 0 ){
            $skip = ($index == 1) ? 0 : ($index-1)*10 ;
            $result['data']=$paymentsRecords->take(10)->skip($skip)->get();

            return $result;
        }
        else {
            $result['data']=$paymentsRecords->get();
            $result['headers'] = ['ID', 'User', 'Amount' , 'Invoice ID' ,'coupon' , 'Created at'];

            return $result;
        }
    }

    public static function createUpdatePayments($request){
        return Payment::updateOrCreate(['id' => isset($request->id) ?$request->id : 0 ] , [
            'user_id' => $request->user_id,
            'amount' => $request->amount,
            'invoice_id' => $request->invoice_id,
            'coupon' => $request->coupon,
            'payment_method_id' => 3
        ]);
    }


    //Teacher Fees

    public static function getTeacherFeesData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $teacher = TeacherFees::select('id','user_id','percent_fees' ,'fixed_fees');

        if (isset($filter['id'])){
            $teacher->where('id','=',$filter['id']);
        }

        if (isset($filter['user_id'])){
            $teacher->where('user_id','=',$filter['user_id']);
        }

        if (isset($filter['percent_fees'])){
            $teacher->where('percent_fees','=',$filter['percent_fees']);
        }

        if (isset($filter['fixed_fees'])){
            $teacher->where('fixed_fees','=',$filter['fixed_fees']);
        }

        $teacher->orderBy('id','desc');
        $result['total'] = $teacher->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$teacher->take(10)->skip($skip)->get();
        foreach ($result['data'] as $row) {
            if ($row->user_id == 0 ){
                $row->user_name = "";
            }
            else{
                $row->user_name = User::find($row->user_id)->email;
            }
        }
        return $result;

        return $result;

    }

    public static function createUpdateTeacher($request){
        return TeacherFees::updateOrCreate(['id'=> isset($request->id)? $request->id : 0],[
            'user_id' => $request->user_id,
            'percent_fees' => $request->percent_fees,
            'fixed_fees' => $request->fixed_fees
        ]);
    }

    // Coupons
    public static function getCouponData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $coupon = Coupon::select('id', 'code' ,'amount' , 'user_id' , 'course_id' ,'qty');

        if(isset($filter['id'])){
            $coupon->where('id','=',$filter['id']);
        }
        if(isset($filter['code'])){
            $coupon->where('code','like', '%'.$filter['code'].'%');
        }
        if(isset($filter['qty'])){
            $coupon->where('qty','=',$filter['qty']);
        }

        if(isset($filter['amount'])){
            $coupon->where('amount','=',$filter['amount']);
        }
        if (isset($filter['user_id'])){
            $coupon->where('user_id', '=' , $filter['user_id']);
        }


        $coupon->orderBy('id','desc');
        $result['total'] = $coupon->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$coupon->take(10)->skip($skip)->get();
        foreach ($result['data'] as $row) {
            if ($row->user_id == 0 ){
                $row->user_id = "";
                $row->course_id = Course::find($row->course_id)->title_en;
            }
            else{
                $row->user_id = User::find($row->user_id)->email;
                $row->course_id = "";
            }
        }
        return $result;
    }


    public static function createUpdateCoupon($request){
        $code = "";
        if ($request->prefix == 1){
            $length = strlen($request->code);
            $suffix = AdminService::random_str(15);
            $code = $request->code .'-'.$suffix;
        }
        else {
            $code = $request->code;
        }
        if ($request->group_id == null ||$request->group_id == 0 ){
            return Coupon::updateOrCreate(['id' => isset($request->id) ? $request->id : 0 ], [
                'code' => $code,
                'amount' => $request->amount,
                'user_id' => $request->user_id,
                'course_id' => $request->course_id,
                'coupon_id' => $request->coupon_id,
                'qty' => $request->qty
            ]);
        }
        else {
            $group = Group::with('groupUsers')->find($request->group_id);

            foreach ($group->groupUsers as $row) {
                Coupon::updateOrCreate(['id' => isset($request->id) ? $request->id : 0 ], [
                    'code' => $code,
                    'amount' => $request->amount,
                    'course_id' => $request->course_id,
                    'user_id' => $row->user_id,
                    'qty' => $request->qty
                ]);
            }
        }
    }


    // Subscription Models

    public static function getSubscriptionData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $subscription = SubscriptionModel::select('id','name_en','name_ar','display_price' , 'price' ,'period_in_days' ,'full_access');

        if (isset($filter['id'])){
            $subscription->where('id', '=', $filter['id']);
        }

        if (isset($filter['name_en'])){
            $subscription->where('name_en', 'like',  '%'.$filter['name_en'].'%');
        }

        if (isset($filter['name_ar'])){
            $subscription->where('name_ar', 'like',  '%'.$filter['name_ar'].'%');
        }

        if (isset($filter['display_price'])){
            $subscription->where('display_price', '=', $filter['display_price']);
        }

        if (isset($filter['price'])){
            $subscription->where('price', '=', $filter['price']);
        }

        if (isset($filter['period_in_days'])){
            $subscription->where('period_in_days', '=', $filter['period_in_days']);
        }

        if (isset($filter['full_access'])){
            $subscription->where('full_access', '=', $filter['full_access']);
        }

        $subscription->orderBy('id','desc');
        $result['total'] = $subscription->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$subscription->take(10)->skip($skip)->get();

        return $result;
    }

    public static function createUpdateSubscription($request){
        return SubscriptionModel::updateOrCreate(['id' => isset($request->id)? $request->id :  0] , [
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'display_price' => $request->display_price,
            'price' => $request->price,
            'period_in_days'=> $request->period_in_days,
            'full_access' => isset($request->full_access) ? $request->full_access : 0
        ]);
    }

    //Stripe Customers

    public static function getStripeData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;


        $customers = StripeCustomer::select('id' ,'user_id' , 'email' ,'stripe_id' ,'card_id');

        if(isset($filter['id'] )){
            $customers->where('id', '=', $filter['id']);
        }

        if(isset($filter['user_id'] )){
            $customers->where('user_id', '=', $filter['user_id']);
        }

        if(isset($filter['email'] )){
            $customers->where('email', '=', $filter['email']);
        }

        if(isset($filter['stripe_id'] )){
            $customers->where('stripe_id', '=', $filter['stripe_id']);
        }

        if(isset($filter['card_id'] )){
            $customers->where('card_id', '=', $filter['card_id']);
        }


        $customers->orderBy('id','desc');
        $result['total'] = $customers->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$customers->take(10)->skip($skip)->get();

        return $result;
    }

    public static function createUpdateStripe($request){
        return StripeCustomer::updateOrCreate(['id' => isset($request->id) ?$request->id : 0 ] , [
            'user_id' => $request->user_id,
            'email' => $request->email,
            'stripe_id' => $request->stripe_id,
            'card_id' => $request->card_id
        ]);
    }


    //Stripe Plans

    public static function getStripePlanData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $plans = StripePlan::select('id','subscription_model_id', 'course_id' ,'stripe_plan_id');

        if (isset($filter['id'])){
            $plans->where('id', '=', $filter['id']);
        }

        if (isset($filter['subscription_model_id'])){
            $plans->where('subscription_model_id', '=', $filter['subscription_model_id']);
        }

        if (isset($filter['course_id'])){
            $plans->where('course_id', '=', $filter['course_id']);
        }

        if (isset($filter['stripe_plan_id'])){
            $plans->where('stripe_plan_id', 'like',  '%'.$filter['stripe_plan_id'].'%');
        }


        $plans->orderBy('id','desc');
        $result['total'] = $plans->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$plans->take(10)->skip($skip)->get();

        return $result;
    }

    public static function createUpdateStripePlan($request){
        return StripePlan::updateOrCreate(['id' => isset($request->id) ?$request->id : 0] , [
            'subscription_model_id' => $request->subscription_model_id ,
            'course_id' =>$request->course_id,
            'stripe_plan_id' => $request->stripe_plan_id
        ]);
    }



    //Payment Methods

    public static function getPaymentMethodsData($filter){

        $index = $filter ? $filter['pageIndex'] : 0 ;

        $methods  = PaymentMethod::select('name' ,'percent_fees' , 'fixed_fees' , 'id');

        if (isset($filter->name)){
            $methods->where('name', 'like' ,  '%'.$filter->name.'%');
        }

        if (isset($filter->percent_fees)){
            $methods->where('percent_fees', '=' , $filter->percent_fees);
        }

        if (isset($filter->fixed_fees)){
            $methods->where('fixed_fees', '=' , $filter->fixed_fees);
        }

        if (isset($filter->id)){
            $methods->where('id', '=' , $filter->id);
        }


        $methods->orderBy('id','desc');
        $result['total'] = $methods->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$methods->take(10)->skip($skip)->get();

        return $result;
    }

    public static function createUpdateMethods($request){
        return PaymentMethod::updateOrCreate(['id' => isset($request->id) ? $request->id : 0] , [
            'name' => $request->name,
            'percent_fees' => $request->percent_fees,
            'fixed_fees' => $request->fixed_fees
        ]);
    }

    // Abandoned Carts

    public static function getAbandondeCartData($filter){

        $index = $filter ? $filter['pageIndex'] : 0 ;

        $cart = AbandonedCart::select('id','user_id','subscription_model_id','course_id' ,'payment_method_id' ,'coupon');

        if(isset($filter['id'])){
            $cart->where('id', '=', $filter['id']);
        }


        if(isset($filter['user_id'])){
            $cart->where('user_id', '=', $filter['user_id']);
        }

        if(isset($filter['subscription_model_id'])){
            $cart->where('subscription_model_id', '=', $filter['subscription_model_id']);
        }

        if(isset($filter['course_id'])){
            $cart->where('course_id', '=', $filter['course_id']);
        }

        if(isset($filter['payment_method_id'])){
            $cart->where('payment_method_id', '=', $filter['payment_method_id']);
        }

        if(isset($filter['coupon'])){
            $cart->where('coupon', '=', $filter['coupon']);
        }

        $cart->orderBy('id','desc');
        $result['total'] = $cart->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$cart->take(10)->skip($skip)->get();

        return $result;
    }

    public static function createUpdateCarts(Request $request){
        return AbandonedCart::updateOrCreate(['id' => isset($request->id)? $request->id : 0] , [
            'user_id'=> $request->user_id,
            'subscription_model_id' => $request->subscription_model_id,
            'course_id' => $request->course_id,
            'payment_method_id' => $request->payment_method_id,
            'coupon' => $request->coupon
        ]);
    }


    // Slugs
    public static function getSlugsData($filter){

        $index = $filter ? $filter['pageIndex'] : 0 ;

        $slug = Slug::select('id','parent','title_en', 'title_ar', 'slug','active');


        if (isset($filter['id'])){
            $slug->where('id', '=', $filter['id']);
        }
        if (isset($filter['parent'])){
            $slug->where('parent', '=', $filter['parent']);
        }
        if (isset($filter['title_en'])){
            $slug->where('title_en', 'like', '%'.$filter['title_en'].'%');
        }
        if (isset($filter['title_ar'])){
            $slug->where('title_ar', 'like', '%'.$filter['title_ar'].'%');
        }
        if (isset($filter['slug'])){
            $slug->where('slug', 'like','%'.$filter['slug'].'%');
        }

        $slug->orderBy('id','desc');
        $result['total'] = $slug->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$slug->take(10)->skip($skip)->get();

        return $result;
    }

    public static function createUpdateSlugs($request)
    {
        // dd($request);
        $slug = Slug::updateOrCreate(['id'=> isset($request->id) ? $request->id : 0] ,[
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'slug' => $request->slug,
            'text' => $request->editor,
            'text_ar' => $request->editor_ar,
            'order' => $request->order,
            'active'=> ($request->active == 'on') ? 1 : 0,
            'href' => $request->href
        ]);
        if ($slug->slug == 'contact-us'){
            $slug->text = str_replace('</form>', '</form><div class="row"><div class="col-md-4 offset-md-8"><button type="submit" onclick="submitContactUsForm();" class="btn white-border-btn submit w-100 mt-2">Send</button></div></div>', $slug->text);
            $slug->update();
        }
    }

    public static function updateAbout($request){
        $text = Slug::updateOrCreate(['type' => 'about'] , ['text'=> $request->text]);
        return 'ok';
    }
    public static function editStarsText($request){
        StarsText::updateOrCreate(['id' => 1] , [
            'text'=>$request->paragraph,
            'text_ar' => $request->paragraph_ar
        ]);
    }

    public static function createUpdateFooterContent($request){
        Footer::updateOrCreate(['id'=>1],[
            'number1' => $request->number1,
            'number2' => $request->number2,
            'email' => $request->email,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'youtube' => $request->youtube,
            'instagram' => $request->instagram,
            'margin' => $request->margin,
            'currency_symbol' => $request->currency_symbol,
            'currency_conversation' => $request->currency_conversation,
            'currency_position' => $request->currency_position
        ]);
    }

    //Email

    public static function getEmailsData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $email = Email::select('id','email','name', 'subject', 'text' , 'created_at');

        if (!empty($filter['id'])){
            $email ->where('id', '=', $filter['id']);
        }
        if (!empty($filter['name'])){
            $email->where('name', 'like', '%'.$filter['name'].'%');
        }
        if (!empty($filter['email'])){
            $email->where('email', 'like', '%'.$filter['email'].'%');
        }
        if (!empty($filter['subject'])){
            $email->where('subject', 'like', '%'.$filter['subject'].'%');
        }

        $email->orderBy('id','desc');
        $result['total'] = $email->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$email->take(10)->skip($skip)->get();

        return $result;
    }



    public static function getGroupsData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $group = Group::select('id','name','created_at');

        if (!empty($filter['id'])){
            $group ->where('id', '=', $filter['id']);
        }
        if (!empty($filter['name'])){
            $group->where('name', 'like', '%'.$filter['name'].'%');
        }

        $group->orderBy('id','desc');
        $result['total'] = $group->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$group->take(10)->skip($skip)->get();

        return $result;
    }

    public static function createUpdateGroup($request){
        $group = Group::updateOrCreate(['id' => isset($request->id) ?$request->id : 0 ]  , [
            'name' => $request->name
        ]);
        if (isset($request->id) ){
            $group->updated_by = Auth::user()->id;
        }
        else {
            $group->created_by = Auth::user()->id;
        }
        if ($request->hasFile('file')){
            $res = AdminService::importGroupUsers($group->id , $request);
        }
        $group->save();
        if (!empty($res['error']))
        {
            GroupUser::where('group_id' , $group->id)->delete();
            $group->delete();
            return 'error';
        }
        return $group;
    }

    public static function getGroupData($id , $filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $group = GroupUser::select('id','user_id' , 'group_id','created_at')->where('group_id' , $id)->with('user');

        if (!empty($filter['id'])){
            $group ->where('id', '=', $filter['id']);
        }
        if (!empty($filter['name'])){
            $group->where('name', 'like', '%'.$filter['name'].'%');
        }

        $group->orderBy('id','desc');
        $result['total'] = $group->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$group->take(10)->skip($skip)->get();

        return $result;

    }
    public static function importGroupUsers($id ,$request)
    {
        $responseArray = [];
        if ($request->hasFile('file')){
            $responseArray = ExcelService::import($request->file('file'), $id);
        }
        return $responseArray;
    }
    public static function addMenualGroupUsers($id ,$request)
    {
        $group_id = $id;
        if ($request->email==null && $request->phone == null &&  $request->name == null)
            return ;

        $email = $request->email;
        $password = AdminService::random_str(8);
        $hashed = Hash::make($password);
        $user = User::where('email' , 'like', $email )->first();
        if (!isset($user) || count($user) == 0){
            $user = User::updateOrCreate(['email' => $request->email], [
                'name'=>$request->name,
                'password'=> $hashed,
                'phone' => isset($request->phone) ? $request->phone : 000,
                'country'=>$request->country
            ]);
        }
        else {
            $user = User::updateOrCreate(['email' => $request->email], [
                'name'=>$request->name,
                'phone' => isset($request->phone) ? $request->phone : 000,
                'country'=>$request->country
            ]);

        }
        $subscriptionModel = $request->subscription;
        if (isset($subscriptionModel) &&  $subscriptionModel != ''){

            $subscriptionModel = SubscriptionModel::where('name_en' , 'like', $subscriptionModel)->get()->first();
            if (isset($subscriptionModel)){
                $amount =0 ;
                if (isset($request->amount) && $request->amount != ""){
                    $amount = $request->amount;
                }

                $payment = PaymentService::makePayment($amount , 0 , '',$subscriptionModel->id , $user->id);


                $startDate = Carbon::today();
                $endDate = Carbon::today()->addDays($subscriptionModel->period_in_days);

                $subscription = SubscriptionService::subscribe($user->id, $subscriptionModel->id, $payment, -1, 0 , $startDate , $endDate);
            }
        }
        $groupUser = GroupUser::updateOrCreate(['id' => 0],[
            'group_id' =>$group_id,
            'user_id' => $user->id
        ]);
    }

    public static function getKitData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $kits = Kit::select('id','name_en', 'name_ar' , 'active','created_at');

        if (!empty($filter['id'])){
            $kits ->where('id', '=', $filter['id']);
        }
        if (!empty($filter['name_en'])){
            $kits->where('name_en', 'like', '%'.$filter['name_en'].'%');
        }
        if (!empty($filter['name_ar'])){
            $kits->where('name_ar', 'like', '%'.$filter['name_ar'].'%');
        }
        if (!empty($filter['active'])){
            $kits->where('active', '=', $filter['active']);
        }
        if (!empty($filter['price'])){
            $kits->where('price', '=', $filter['price']);
        }

        $kits->orderBy('id','desc');
        $result['total'] = $kits->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$kits->take(10)->skip($skip)->get();
        foreach ($result['data'] as $res) {
            if (OrderService::checkKitAvailablity($res->id))
            {
                $res->selling_price = OrderService::getKitSellingPrice($res->id);
                $res->min_price = OrderService::getKitMinimumPrice($res->id);
            }
            else {
                $res->selling_price = 'Unavailable';
                $res->min_price = 'Unavailable';
            }

        }
        return $result;
    }


    public static function createUpdateKit($request){
        $kit = Kit::updateOrCreate(['id' => isset($request->id) ?$request->id : 0  ] ,[
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'description_ar' => $request->description_ar,
            'description_en' => $request->description_en,
            'active' => $request->active == 'on' || $request->active == '1'? 1 : 0
        ]);
        if (isset($request->images)){
            foreach ($request->images as $img) {
                KitImage::create(['img_src' => S3Service::uploadImageToS3($img),
                    'kit_id' => $kit->id
                ]);
            }
        }
        if (isset($request->id)){
            $kit->updated_by = Auth::user()->id;
        }
        else {
            $kit->created_by = Auth::user()->id;
        }
    }

    public static function addItems($id , $request){
        foreach ($request->items as $item) {
            KitItem::create(['item_id' => $item , 'kit_id' => $id] );
        }
    }

    public static function getKitItemsData($filter , $id){
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $items = Item::whereHas('kit', function($query) use ($id){
            $query->where('kit_id' , $id);
        })->select('name_ar', 'name_en' , 'id');

        if (!empty($filter['id'])){
            $items->where('id' , $filter['id']);
        }
        if (!empty($filter['name_en'])){
            $items->where('name_en' , 'like' , '%'.$filter['name_en'].'%');
        }
        if (!empty($filter['name_ar'] )){
            $items->where('name_ar', 'like' , '%'. $filter['name_ar'].'%');
        }

        $items->orderBy('id','desc');
        $result['total'] = $items->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$items->take(10)->skip($skip)->get();
        foreach ($result['data'] as $res) {
            $res->kit_id = $id;
        }

        return $result;
    }

    public static function getSuppliersData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $supplier = User::where('supplier' , 1)->select('id','name', 'email' , 'phone');

        if (!empty($filter['id'])){
            $supplier->where('id' , $filter['id']);
        }
        if (!empty($filter['name'])){
            $supplier->where('name' ,'like', '%'.$filter['name'].'%');
        }
        if (!empty($filter['email'])){
            $supplier->where('email' , 'like' , '%'.$filter['email'].'%');
        }
        if (!empty($filter['phone'])){
            $supplier->where('phone' , 'like' , '%'.$filter['phone'].'%');
        }

        $supplier->orderBy('id','desc');
        $result['total'] = $supplier->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$supplier->take(10)->skip($skip)->get();

        return $result;
    }
    public static function addSupplier($request){
        foreach ($request->users as $val) {
            User::updateOrCreate(['id' => $val],[ 'supplier' => 1]);
        }
    }


    public static function getSuppliersDetails($id , $filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $supplier = Item::whereHas('supplier' , function ($query) use ($id){
            $query->where('supplier_id', '=' , $id);
        })->select('id','name_en', 'name_ar' ,'id');
        $supplier->with(['supplier'=>function($query)use ($id){
            $query->where('supplier_id' , $id);
        }]);

        //Filtrations

        $supplier->orderBy('id','desc');
        $result['total'] = $supplier->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$supplier->take(10)->skip($skip)->get();
        return $result;

    }

    public static function getItemsData ($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $item = Item::select('id' , 'name_en' , 'name_ar');

        if (!empty($filter['id'])){
            $item->where('id', $filter['id']);
        }
        if (!empty($filter['name_en'])){
            $item->where('name_en' , 'like' , '%'.$filter['name_en'].'%');
        }
        if (!empty($filter['name_ar'])){
            $item->where('name_ar', 'like' , '%'.$filter['name_ar'].'%');
        }

        $item->orderBy('id','desc');
        $result['total'] = $item->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$item->take(10)->skip($skip)->get();

        return $result;
    }
    public static function createUpdateItem($request){
        $item = Item::updateOrCreate(['id' => isset($request->id) ?$request->id : 0  ] ,[
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'description' => $request->description
        ]);
        if (isset($request->images)){
            foreach ($request->images as $img) {
                ItemImage::create(['img_src' => S3Service::uploadImageToS3($img),
                    'item_id' => $item->id
                ]);
            }
        }
        if (isset($request->id)) {
            $item->updated_by = Auth::user()->id;
        }
        else {
            $item->created_by = Auth::user()->id;
        }
        $item->save();
    }



    public static function getOrdersData($filter ){
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $order = Order::select('id' , 'user_id' , 'kit_id', 'total_price' , 'selling_price' ,'state')->where('state' , OrderStates::CREATED)->with('user');

        if (!empty($filter['user']['email'])){
            $order->whereHas('user' , function($query) use ($filter){
                $query->where('email', 'like' , '%'.$filter['user']['email'].'%');
            });
        }
        if (!empty($filter['id'])){
            $order->where('id' , $filter['id']);
        }
        if (!empty($filter['selling_price'])){
            $order->where('selling_price' , $filter['selling_price']);
        }
        if (!empty($filter['total_price'])){
            $order->where('total_price' , $filter['total_price']);
        }

        $order->orderBy('id' , 'desc');
        $result['total'] = $order->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10;
        $result['data'] = $order->take(10)->skip($skip)->get();

        return $result;
    }

    public static function getOrderItemsData ($id ,$filter){
        $index = $filter ? $filter['pageIndex'] : 0;

        $orderItem = OrderItem::select('id','price' , 'supplier_id' ,'item_id', 'order_id')->where('order_id', $id)->with('supplier')->with('item');


        $orderItem->orderBy('id', 'desc');
        $result['total'] = $orderItem->count();

        $skip = ($index ==1 ) ? 0 : ($index-1)*10;
        $result['data'] = $orderItem->take(10)->skip($skip)->get();

        return $result;
    }


    public static function getAllOrdersData($filter , $pdf = 0 ){
        $index = $filter ? $filter['pageIndex'] : 0;

        $orders = Order::select('id','user_id', 'kit_id' , 'total_price' ,'selling_price' , 'state' , 'updated_by')->with('kit')->with('user')->with('updatedBy');

        if (!empty($filter['id'])){
            $orders->where('id' , $filter['id']);
        }

        if (!empty($filter['user']['email'])){
            $orders->whereHas('user' , function ($query) use ($filter){
                $query->where('email' ,'like', '%'. $filter['user']['email'].'%');
            });
        }

        if (!empty($filter['kit']['name_en'])){
            $orders->whereHas('kit' ,function ($query) use ($filter) {
                $query->where('name_en' , 'like' ,'%'. $filter['kit']['name_en'].'%');
            });
        }

        if (!empty($filter['total_price'])){
            $orders->where('total_price' , $filter['total_price']);
        }

        if (!empty($filter['selling_price'])){
            $orders->where('selling_price' , $filter['selling_price']);
        }

        if (!empty($filter['state'])){
            $str = strtolower($filter['state']);
            switch ($str) {
                case 'created':
                    $orders->where('state' , OrderStates::CREATED);
                    break;

                case 'confirmed':
                    $orders->where('state' , OrderStates::CONFIRMED);
                    break;

                case 'packed' :
                    $orders->where('state' , OrderStates::PACKED);
                    break;

                case 'ondelivery' || 'on delivery':
                    $orders->where('state' , OrderStates::ONDELIVERY);
                    break;

                case 'delivered':
                    $orders->where('state' , OrderStates::DELIVERED);
                    break;

                default:
                    break;
            }
        }

        $orders->orderBy('id', 'desc');
        $result['total'] = $orders->count();

        if ($pdf == 0 ){
            $skip = ($index ==1 ) ? 0 : ($index-1)*10;
            $result['data'] = $orders->take(10)->skip($skip)->get();

            foreach ($result['data'] as $res) {
                switch ($res->state) {
                    case 1:
                        $res->state = 'CREATED';
                        break;
                    case 2:
                        $res->state = 'CONFIRMED';
                        break;
                    case 3:
                        $res->state = 'PACKED';
                        break;
                    case 4:
                        $res->state = 'ONDELIVERY';
                        break;
                    case 5:
                        $res->state = 'DELIVERED';
                        break;

                    default:
                        break;
                }
            }
            return $result;
        }
        else {
            $result['data'] = $orders->get();
            $result['header'] = ['ID' , 'User Email' , 'Kit Name' , 'Total Price' , 'Selling Price' , 'status' , 'last Updated By'];
            foreach ($result['data'] as $res) {
                switch ($res->state) {
                    case 1:
                        $res->state = 'CREATED';
                        break;
                    case 2:
                        $res->state = 'CONFIRMED';
                        break;
                    case 3:
                        $res->state = 'PACKED';
                        break;
                    case 4:
                        $res->state = 'ONDELIVERY';
                        break;
                    case 5:
                        $res->state = 'DELIVERED';
                        break;

                    default:
                        break;
                }
            }
            return $result;
        }

    }

    public static function getOrderDetailsData($id , $filter){
        $index = $filter ? $filter['pageIndex'] : 0;

        $orderItems = OrderItem::select('id', 'item_id', 'price','qty','final_total' ,'type', 'selling_price' ,'supplier_id')->where('order_id' , $id)->with('kitItem')->with('kit')->with('course')->with('supplier');



        $orderItems->orderBy('id', 'desc');
        $result['total'] = $orderItems->count();


        $skip = ($index ==1 ) ? 0 : ($index-1)*10;
        $orderItemsdata = $orderItems->take(10)->skip($skip)->get();

        foreach ($orderItemsdata as $value) {
            if($value->type == 'Course'){
                $value->itemname = $value->course->title_en;
            }elseif($value->type == 'Kit'){
                $value->itemname = $value->kit->name_en;
            }elseif($value->type == 'Item'){
                $value->itemname = $value->kitItem->items->name_en;
            }
        }
        $result['data'] = $orderItemsdata;

        return $result;
    }

    private static function random_str($length, $keyspace = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces []= $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }


    public static function getGalleryData($filter)
    {
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $items = GalleryItem::select(['id','title_en','title_ar']);

        if (!empty($filter['id']))
        {
            $items->where('id','=',$filter['id']);
        }

        if (!empty($filter['title_en']))
        {
            $items->where('title_en','like','%'.$filter['title_en'].'%');
        }

        if (!empty($filter['title_ar']))
        {
            $items->where('title_ar', 'like', '%'.$filter['title_ar'].'%');
        }

        $items->orderBy('id','desc');
        $result['total'] = $items->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$items->take(10)->skip($skip)->get();

        return $result;
    }

    public static function createUpdateGallery($request)
    {
        $id = 0;
        if (isset($request->id))
        {
            $id = $request->id;
        }

        $item = GalleryItem::updateOrCreate(['id' => $id] , [
            'title_ar' => $request->title_ar,
            'title_en' => $request->title_en,
            'overview_en' => $request->overview_en,
            'overview_ar' => $request->overview_ar,
            'img_src' => $request->img_src,
            'youtube_embed' => $request->youtube_embed,
            'type' => $request->type,
            'album_id' => $request->album_id
        ]);

        if ($request->hasFile('img_src'))
        {
            $imgSrc= S3Service::uploadImageToS3($request->file('img_src'));
            $item->img_src = $imgSrc;
            $item->save();
        }

        return 1;
    }

    public static function getAlbumsData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $albums = Album::select(['id','name_ar','name_en','album_date']);

        if (!empty($filter['name_en']))
        {
            $albums->where('name_en','=',$filter['name_en']);
        }

        if (!empty($filter['name_ar']))
        {
            $albums->where('name_ar','like','%'.$filter['name_ar'].'%');
        }
        if (!empty($filter['album_date']))
        {
            $albums->where('album_date','like','%'.$filter['album_date'].'%');
        }

        $albums->orderBy('id','desc');
        $result['total'] = $albums->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$albums->take(10)->skip($skip)->get();

        return $result;
    }

    public static function createUpdateAlbum($request)
    {
        $id = 0;
        if (isset($request->id))
        {
            $id = $request->id;
        }
        // dd($request->file('img_src'));
        $item=Album::updateOrCreate(['id' => $id] , [
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'filter' => preg_replace("/[^A-Za-z0-9]/", "", strtolower($request->name_en)),
            'album_date' => $request->album_date
        ]);


        if ($request->hasFile('img_src'))
        {
            $imgSrc= S3Service::uploadImageToS3($request->file('img_src'));
            $item->img_src = $imgSrc;
            $item->save();
        }


        return 1;
    }


    public static function getAchievementsData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;

        $achievements = Achievement::select(['id','title_en','title_ar' , 'event_date']);

        if (!empty($filter['title_en']))
        {
            $achievements->where('title_en','like',$filter['title_en']);
        }

        if (!empty($filter['title_en']))
        {
            $achievements->where('title_en','like','%'.$filter['title_en'].'%');
        }

        $achievements->orderBy('id','desc');
        $result['total'] = $achievements->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$achievements->take(10)->skip($skip)->get();

        return $result;
    }

    public static function createUpdateAchievements($request)
    {
        $id = 0;
        if (isset($request->id))
        {
            $id = $request->id;
        }

        $achievement = Achievement::updateOrCreate(['id' => $id] , [
            'title_ar' => $request->title_ar,
            'title_en' => $request->title_en,
            'description_ar' => $request->description_ar,
            'description_en' => $request->description_en,
            'event_date' => $request->event_date,
            'img_src' => $request->img_src
        ]);

        if ($request->hasFile('img_src'))
        {
            $imgSrc= S3Service::uploadImageToS3($request->file('img_src'));
            $achievement->img_src = $imgSrc;
            $achievement->save();
        }

        return 1;
    }

    // Testimonials

    public static function getTestimonialsData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;
        $testimonials = Testimonial::select('name_ar', 'name_en' ,'title_ar',
            'title_en', 'id' , 'updated_by' , 'created_by')->with('updatedBy')->with('createdBy');

        if (isset($filter['name_en'])){
            $testimonials->where('name_en' ,'like',  '%'.$filter['name_en'].'%');
        }

        if (isset($filter['name_ar'])){
            $testimonials->where('name_ar' ,'like',  '%'.$filter['name_ar'].'%');
        }
        if (isset($filter['title_ar'])){
            $testimonials->where('title_ar' ,'like',  '%'.$filter['title_ar'].'%');
        }

        if (isset($filter['title_en'])){
            $testimonials->where('title_en' ,'like',  '%'.$filter['title_en'].'%');
        }
        if (isset($filter['description_ar'])){
            $testimonials->where('description_ar' ,'like',  '%'.$filter['description_ar'].'%');
        }

        if (isset($filter['description_en'])){
            $testimonials->where('description_en' ,'like',  '%'.$filter['description_en'].'%');
        }

        if (isset($filter['id'])){
            $testimonials->where('id' ,'=', $filter['id']);
        }

        if (isset($filter['updated_by']['email'])){
            $testimonials->whereHas('updatedBy' , function($query) use ($filter){
                $query->where('email' , 'like' , '%'.$filter['updated_by']['email'].'%');
            });
        }
        if (isset($filter['created_by']['email'])){
            $testimonials->whereHas('createdBy' , function($query) use ($filter){
                $query->where('email' , 'like' , '%'.$filter['created_by']['email'].'%');
            });
        }

        $testimonials->orderBy('id','desc');
        $result['total'] = $testimonials->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$testimonials->take(10)->skip($skip)->get();
        return $result;
    }

    public static function createUpdateTestimonial(Request $request)
    {
        $testimonial=  Testimonial::updateOrCreate(['id' => isset($request->id)? $request->id : 0 ] , [
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'active'=> $request->active == 'on' ? 1 : 0,
            'type' => $request->type,
        ]);
        if($request->type == 'Site'){
            $testimonial->course_id = 0;
        }else{
            if(isset($request->course_id)){
                $testimonial->course_id = $request->course_id;
            }
        }

        if ($request->hasFile('image_path_en')){
            $testimonial->image_path_en = MediaService::saveImage($request->image_path_en);
        }
        if ($request->hasFile('image_path_ar')){
            $testimonial->image_path_ar = MediaService::saveImage($request->image_path_ar);
        }
        if ($request->hasFile('icon_path')){
            $testimonial->icon_path = MediaService::saveImage($request->icon_path);
        }
        if(isset($request->id)){
            $testimonial->updated_by = Auth::user()->id;
        }
        else {
            $testimonial->created_by = Auth::user()->id;
        }
        $testimonial->update();
    }




    // Vision

    public static function getVisionData($filter){
        $index = $filter ? $filter['pageIndex'] : 0 ;
        $vision = Vision::select('name_ar', 'name_en', 'id', 'image_path_en' , 'updated_by' , 'created_by')->with('updatedBy')->with('createdBy');

        if (isset($filter['name_en'])){
            $vision->where('name_en' ,'like',  '%'.$filter['name_en'].'%');
        }

        if (isset($filter['name_ar'])){
            $vision->where('name_ar' ,'like',  '%'.$filter['name_ar'].'%');
        }


        if (isset($filter['id'])){
            $vision->where('id' ,'=', $filter['id']);
        }

        if (isset($filter['updated_by']['email'])){
            $vision->whereHas('updatedBy' , function($query) use ($filter){
                $query->where('email' , 'like' , '%'.$filter['updated_by']['email'].'%');
            });
        }
        if (isset($filter['created_by']['email'])){
            $vision->whereHas('createdBy' , function($query) use ($filter){
                $query->where('email' , 'like' , '%'.$filter['created_by']['email'].'%');
            });
        }

        $vision->orderBy('id','desc');
        $result['total'] = $vision->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$vision->take(10)->skip($skip)->get();
        return $result;
    }

    public static function createUpdateVision(Request $request)
    {
        $vision=  Vision::updateOrCreate(['id' => isset($request->id)? $request->id : 0 ] , [
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'active'=> $request->active == 'on' ? 1 : 0,

        ]);


        if ($request->hasFile('image_path_en')){
            $vision->image_path_en = MediaService::saveImage($request->image_path_en);
        }
        if ($request->hasFile('image_path_ar')){
            $vision->image_path_ar = MediaService::saveImage($request->image_path_ar);
        }

        if(isset($request->id)){
            $vision->updated_by = Auth::user()->id;
        }
        else {
            $vision->created_by = Auth::user()->id;
        }
        $vision->update();
    }


    public static function getBannerimagesData($filter){





        $index = $filter ? $filter['pageIndex'] : 0 ;
        $bannerimages = Bannerimages::select('image_path', 'display_area' ,'display_size',
            'id' , 'updated_by' , 'created_by')->with('updatedBy')->with('createdBy');





        if (isset($filter['display_area'])){
            $bannerimages->where('display_area' ,'like',  '%'.$filter['display_area'].'%');
        }



        if (isset($filter['id'])){
            $bannerimages->where('id' ,'=', $filter['id']);
        }

        if (isset($filter['updated_by']['email'])){
            $bannerimages->whereHas('updatedBy' , function($query) use ($filter){
                $query->where('email' , 'like' , '%'.$filter['updated_by']['email'].'%');
            });
        }
        if (isset($filter['created_by']['email'])){
            $bannerimages->whereHas('createdBy' , function($query) use ($filter){
                $query->where('email' , 'like' , '%'.$filter['created_by']['email'].'%');
            });
        }

        $bannerimages->orderBy('id','desc');
        $result['total'] = $bannerimages->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$bannerimages->take(10)->skip($skip)->get();



        return $result;
    }


    public static function createUpdateBannerimages(Request $request)
    {
        $bannerimages=  Bannerimages::updateOrCreate(['id' => isset($request->id)? $request->id : 0 ] , [
            'display_area' => $request->display_area
        ]);
        if ($request->hasFile('image_path')){
            $bannerimages->image_path = MediaService::saveImage($request->image_path);
        }


        if(isset($request->id)){
            $bannerimages->updated_by = Auth::user()->id;
        }
        else {
            $bannerimages->created_by = Auth::user()->id;
        }
        $bannerimages->update();
    }

    // Slugs
    public static function getEmailTemplateData($filter){

        $index = $filter ? $filter['pageIndex'] : 0 ;

        $emailtemplate = Emailtemplates::select('email_temp_id','email_temp_receiver',
            'email_temp_name',
            'email_temp_desc',
            'email_temp_subject',
            'email_temp_body','email_temp_status');


        if (isset($filter['email_temp_id'])){
            $emailtemplate->where('email_temp_id', '=', $filter['email_temp_id']);
        }
        if (isset($filter['email_temp_receiver'])){
            $emailtemplate->where('email_temp_receiver', '=', $filter['email_temp_receiver']);
        }
        if (isset($filter['email_temp_name'])){
            $emailtemplate->where('email_temp_name', 'like', '%'.$filter['email_temp_name'].'%');
        }
        if (isset($filter['email_temp_desc'])){
            $emailtemplate->where('email_temp_desc', 'like', '%'.$filter['email_temp_desc'].'%');
        }
        if (isset($filter['email_temp_subject'])){
            $emailtemplate->where('email_temp_subject', 'like','%'.$filter['email_temp_subject'].'%');
        }

        $emailtemplate->orderBy('email_temp_id','desc');
        $result['total'] = $emailtemplate->count();

        $skip = ($index == 1) ? 0 : ($index-1)*10 ;
        $result['data']=$emailtemplate->take(10)->skip($skip)->get();

        return $result;
    }

    public static function createUpdateEmailTemplate($request)
    {
        // dd($request);
        $emailtemplate = Emailtemplates::updateOrCreate(['email_temp_id'=> isset($request->id) ? $request->id : 0] ,[
            'email_temp_name' => $request->email_temp_name,
            'email_temp_desc' => $request->email_temp_desc,
            'email_temp_subject' => $request->email_temp_subject,
            'email_temp_body' => $request->email_temp_body,

        ]);
        $emailtemplate->update();

    }

    public static function vimeoVideoDuration($video_url) {

        $authorization = self::vimeoToken;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.vimeo.com/videos/{$video_url}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer {$authorization}",
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        if (empty($err)) {
            $info = json_decode($response);

            if(isset($info->duration)){
                return (int)$info->duration;
            }
        }
        return false;
    }

}

