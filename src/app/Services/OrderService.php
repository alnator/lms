<?php

namespace App\Services; 
use App\SubscriptionModel;
use App\Payment;
use App\Subscription;
use Carbon\Carbon;
use App\User;
use App\Order;
use App\Item;
use App\OrderItem;
use App\SupplierItem;
use App\KitItem;
use App\OrderStates;
use Auth;
use App\Kit;
use App\Cart;
use App\Cartdetail;
use App\Services\CouponService;
use App\Emailtemplates;
use App\Services\MailService;
use Mail;
use App\Footer;
class OrderService {

	public static function orderCustomKit($itemArray ,$params){
		if (! self::checkOrderAvailablity($itemArray))
			return false;

		$user = Auth::user();
		$min = self::getCustomOrderMinimumPrice($itemArray);
		
		$total_price = $min['minimumPrice'];
		$selling_price = 0;
		$order = Order::create([
			'user_id' => $user->id,
			'kit_id' => 0,
			'total_price' => $total_price,
			'state' => OrderStates::CREATED,
			'line1'=>'',
			'phone' =>'',
			'country_code' => '',
			'city' => ''
		]);
		
		$supplier = $min['supplierId'];
		$supplier = User::with('item')->find($supplier);
		foreach ($supplier->item as $item) {
			$max = self::getMaximumPrice($item->item_id);
			foreach ($itemArray as $i) {
				if ($i == $item->item_id){
					$selling_price += $max['MaxAmount']; // It's inside the loop for "Repitition" '[1,2,2]'
					$itemPrice = $item->price;
					$item->qty --;
					$orderItem = OrderItem::create([
						'order_id'=> $order->id,
						'item_id' => $i,
						'supplier_id' => $supplier->id,
						'price' => $itemPrice,
						'selling_price' => $max['MaxAmount']
					]);
					$item->update();
				}
			}	
		}


		$order->city = $params->city;
		// $order->aramex_person_name = $params->name;
		$order->phone_number = $params->phone;
		$order->phone = $params->cell_phone;
		// $order->city = $params->province;
		// $order->city = $params->postal;
		$order->country_code = $params->country_code;
		$order->line1 = $params->line1;
		$order->line2 = $params->line2;
		$order->line3 = $params->line3;
		$order->supplier_id = $supplier->id;
		$order->selling_price = $selling_price;
		$order->update();
		return true;
	}

	public static function orderKit($kitId , $params){
		if(! self::checkKitAvailablity($kitId)){
			return false;
		}
		$user = Auth::user();
		$kit = Kit::with(['items' => function($query){
			$query->with('items');
		}])->find($kitId);
		$order = Order::create(['user_id' => $user->id , 'kit_id' => $kitId , 'total_price' => 0 , 'state' => OrderStates::CREATED,'line1'=>'' , 'phone' =>'' ,'country_code' => '' , 'city' => '']);
		$total_price = 0;
		$selling_price = 0;
		
		$min = self::getKitMinimumPrice($kitId);
		$suppItems = User::with('item')->find($min['supplierId']);
		foreach ($kit->items as $item) {
			$max = self::getMaximumPrice($item->item_id);
			$selling_price += $max['MaxAmount'];
			$itemPrice;
			foreach ($suppItems->item as $supItem) {
				if ($supItem->item_id == $item->item_id){
					$itemPrice = $supItem->price;
					$total_price += $itemPrice;
					$supItem->qty --;
					$orderItem = OrderItem::create(['order_id' => $order->id,
						'item_id' => $item->item_id,
					 	'supplier_id' => $supItem->supplier_id,
						'price' => $itemPrice,
						'selling_price' => $max['MaxAmount']
					]);
					$supItem->update();
				}
			}
		}	
		$order->city = $params->city;
		// $order->aramex_person_name = $params->name;
		$order->phone_number = $params->phone;
		$order->phone = $params->cell_phone;
		// $order->city = $params->province;
		// $order->city = $params->postal;
		$order->country_code = $params->country_code;
		$order->line1 = $params->line1;
		$order->line2 = $params->line2;
		$order->line3 = $params->line3;

		$order->supplier_id = $suppItems->id;
		$order->total_price = $total_price;
		$order->selling_price = $selling_price;
		$order->update();
		return true;
	}

	public static function checkKitAvailablity($id){
		$kit = Kit::with('items')->find($id);
		$arr = [];
		$items = [] ;
		foreach ($kit->items as  $value) {
			if (!isset($arr[$value->item_id]))
				$arr[$value->item_id] = 0 ;
			$arr[$value->item_id] ++ ;
			$items[] = $value->item_id;
		}
		$items = array_unique($items);
		$suppItems = User::with('item');
		foreach ($items as  $item) {
			$suppItems->whereHas('item' , function ($query) use ($item , $arr){
					$query->where('item_id' , $item)->where('qty' , '>=' , $arr[$item]);
			});
		}
		$suppItems = $suppItems->get();
		if (isset($suppItems) && $suppItems->count() != 0 ){
			return true;
		}
		return false;
	}

	public static function checkOrderAvailablity($itemArray){
		$amounts = [];
		foreach ($itemArray as $value) {
			if (!isset($amounts[$value]) )
				$amounts[$value] = 0;
			$amounts[$value] ++ ;
		}
		$itemArray = array_unique($itemArray);
		$suppItems = User::with('item');
		foreach ($itemArray as  $item) {
			$suppItems->whereHas('item' , function ($query) use ($item , $amounts){
					$query->where('item_id' , $item)->where('qty' , '>=' , $amounts[$item]);
			});
		}
		$suppItems = $suppItems->get();
		if (isset($suppItems) && $suppItems->count() != 0 ){
			return true;
		}
		return false;
	}

	public static function getMaximumPrice($itemId){
		$items = SupplierItem::where('item_id' , $itemId)->get();
	
		$max = -1 ;
		$sup_id = 0 ;
		$qty=0;
		if (isset($items) && $items->count() != 0){
			foreach ($items as $value) {
				if ($max < $value->price ){
					$max = $value->price;
					$sup_id = $value->supplier_id;
					$qty = $value->qty;
				}
			}
		}
		return ['MaxAmount' => $max , 'SupplierId' => $sup_id, 'qty' => $qty]; 
	}

	public static function checkOrderState($orderId){
		$order = Order::with('items')->find($orderId);
		$packed =1;
		foreach ($order->items as $orderItem) {
			if($orderItem->type == 'Kit' || $orderItem->type == 'Item'){
			if ($orderItem->packed != 1 ){
				$packed = 0 ;
				break;
			}
			}
		}
		if ($packed == 1){
			$order->state = OrderStates::PACKED;
			$order->update();
			return 2;
		}
		return 1;
	}

	public static function getKitSellingPrice($kitId){
		$items = KitItem::where('kit_id' , $kitId)->select('item_id')->get()->toArray();
		$supplierItems = SupplierItem::whereIn('item_id' , $items)->get();
		$arr =[];
		foreach ($supplierItems as  $item) {
			if (!isset($arr[$item->item_id])){
				$arr[$item->item_id] = 0;
			}
			if ($arr[$item->item_id] < $item->price){
				$arr[$item->item_id] = $item->price;
			}
		}
		
		$sellingPrice = 0 ;
		foreach ($items as  $item) {
			if (!isset($item['item_id'])){
			$sellingPrice += $arr[$item['item_id']];
			}
		}
		

		return $sellingPrice;
	}

	public static function getKitMinimumPrice($id){
		$kit = Kit::with('items')->find($id);
		$arr = [];
		$items = [] ;
		foreach ($kit->items as  $value) {
			if (!isset($arr[$value->item_id]))
				$arr[$value->item_id] = 0 ;
			$arr[$value->item_id] ++ ;
			$items[] = $value->item_id;
		}
		$items = array_unique($items);
		$suppItems = User::with('item')->where('supplier', 1);
		foreach ($items as  $item) {
			$suppItems->whereHas('item' , function ($query) use ($item , $arr){
					$query->where('item_id' , $item);
					//->where('qty' , '>=' , $arr[$item])
			});
		}
		$suppItems = $suppItems->get();
		$minPrice = 9999999;
		$supp_id = -1;
		$available = 1;
		foreach ($suppItems as  $value) {
			$tempMinPrice = 0;
			foreach ($value->item as  $sup_item) {
				foreach ($items as $item) {
					if ($item == $sup_item->item_id){
						$tempMinPrice += ($sup_item->price * $arr[$item]);
						if($sup_item->qty == 0){
							$available = 0;
						}
					}
				}
			}
			if ($minPrice > $tempMinPrice){
				$minPrice = $tempMinPrice;
				$supp_id = $value->id;
			}
		}
		return ['minimumPrice' => $minPrice , 'supplierId' => $supp_id, 'available' => $available];
	}

	public static function getCustomOrderMinimumPrice($itemsArray){
		$minimumPrice = 99999999;
		$supplierId = 0;
		$amounts = [];
		foreach ($itemsArray as $item){
			if (!isset($amounts[$item]))
				$amounts[$item] = 0;
			$amounts[$item]++;
		}
		$itemsArray = array_unique($itemsArray);
		$suppliers = User::where('supplier' , 1)->with('item');
		foreach ($itemsArray as $item) {
			$suppliers->whereHas('item', function($query)use ($item , $amounts){
				$query->where('item_id' ,$item)->where('qty', '>=' , $amounts[$item]);
			});
		}
		$suppliers = $suppliers->get();
		foreach ($suppliers as $supplier) {
			$tempMinPrice=0;
			foreach ($supplier->item as $item) 
				foreach ($itemsArray as $i) 
					if ($i == $item->item_id)
						$tempMinPrice+= ($item->price * $amounts[$i]);
			if ($minimumPrice > $tempMinPrice){
				$minimumPrice = $tempMinPrice;
				$supplierId = $supplier->id;
			}
		}
		return ['minimumPrice' => $minimumPrice , 'supplierId' => $supplierId];
	}
	public static function checkout($request){
		
		$user = Auth::user();
		
		$cartobj = Cart::find($request->cartid);



		if(isset($cartobj))
		{
			$order = Order::create([
				'user_id' => $user->id,
				'kit_id' => 0,
				'total_price' => $cartobj->total,
				'state' => OrderStates::CREATED,
				'line1'=> $request->line1,
				'line2'=> $request->line2,
				'line3'=> $request->line3,
				'phone' =>$request->phone,
				'phone_number' =>$request->cell_phone,
				'contact_name' =>$request->name,
				'contact_email' =>$request->email,

				'country_code' => $request->country_code,
				'city' => $request->city,
				'province' => $request->province,
				'postal_code' => $request->postal,
				'payment_method' => $request->payment_method,
				'coupon_code' => $cartobj->coupon_code,
				'coupon_amount' => $cartobj->coupon
			]);
		if($cartobj->coupon_code != ''){
		 $valid = CouponService::useCoupon($cartobj->coupon_code);
		}

		$cartdetail = Cartdetail::where('cart_id','=',$cartobj->id)->get();
		foreach ($cartdetail as $cartvalue) {
			if($cartvalue->type == 'Kit'){
				$items = KitItem::where('kit_id' , $cartvalue->product_id)->select('item_id')->get()->toArray();
				$main =json_encode($items);
				foreach ($items as $value) {
					$supplieritems = SupplierItem::where('item_id' , $value['item_id'])->where('supplier_id' , $cartvalue->supplierid)->get()->first();
					if(isset($supplieritems)){
						$supplieritems->qty --;
						$supplieritems->update();
					}
				}
				$orderItem = OrderItem::create([
					'order_id'=> $order->id,
					'item_id' => $cartvalue->product_id,
					'supplier_id' => $cartvalue->supplierid,
					'price' => $cartvalue->price,
					'selling_price' => $cartvalue->price,
					'qty' => $cartvalue->qty,
					'final_total' => $cartvalue->final_total,
					'type' => $cartvalue->type,
					'mainid' => $main
				]);
					
				
			}elseif($cartvalue->type == 'Item'){
			
				$items = KitItem::find($cartvalue->product_id);

				$supplieritems = SupplierItem::where('item_id' , $items->item_id)->where('supplier_id' , $cartvalue->supplierid)->get()->first();
				if(isset($supplieritems)){
				$itemPrice = $supplieritems->price;
				$supplieritems->qty --;
				$orderItem = OrderItem::create([
					'order_id'=> $order->id,
					'item_id' => $cartvalue->product_id,
					'supplier_id' => $cartvalue->supplierid,
					'price' => $itemPrice,
					'selling_price' => $cartvalue->price,
					'qty' => $cartvalue->qty,
					'final_total' => $cartvalue->final_total,
					'type' => $cartvalue->type
				]);
				$supplieritems->update();
				}
				
			}else{
				$orderItem = OrderItem::create([
					'order_id'=> $order->id,
					'item_id' => $cartvalue->product_id,
					'supplier_id' => $cartvalue->supplierid,
					'price' => $cartvalue->price,
					'selling_price' => $cartvalue->price,
					'qty' => $cartvalue->qty,
					'final_total' => $cartvalue->final_total,
					'type' => $cartvalue->type
				]);	
			}
		}

		
		$cartdelete = Cart::find($cartobj->id);	
      	$cartdetaildelete = Cartdetail::where('cart_id','=',$cartobj->id)->delete();
    	$cartdelete->delete();
		//exit;
		if($request->payment_method == 'cod'){
			//self::getOrdermail($order->id,'cod');
		}
    	
		return $order;
		}else{
			$order = Order::where('user_id','=', $user->id)->where('status', 'pending')->orderBy('id', 'DESC')->get()->first();
			return $order;
		}
	}
	public static function getOrdermail($id,$Payment_type)
    {
        $order = Order::find($id);
        $user_email=$order->user->email;

        $footer = Footer::find(1);
        $admin_email = $footer->email;
        $supplier_email='';
        $orderItems = OrderItem::select('id', 'item_id', 'price','qty','final_total' ,'type', 'selling_price' ,'supplier_id')->where('order_id' , $order->id)->with('kitItem')->with('kit')->with('course')->with('supplier')->orderBy('id', 'desc')->get();
    
	    foreach ($orderItems as $value) {
	      if($value->type == 'Course'){
	        $value->itemname = $value->course->title_en;
	      }elseif($value->type == 'Kit'){
	        $value->itemname = $value->kit->name_en;
	      }elseif($value->type == 'Item'){
	        $value->itemname = $value->kitItem->items->name_en;
	      }
	      if(isset($value->supplier->email) && $value->supplier->email == ''){
	      		$supplier_email=$value->supplier->email;
	      }
	    }
	    $string ='';
        $string .='<table width="100%" style="border-collapse: collapse;border: 1px solid black;">
        			<tr>
                        <td colspan="4" style="text-align: left;width: 100%;border: 1px solid black;">
                			<strong>OrderID : '.$order->id.'</strong>
                		</td>
                		
					</tr>
					<tr>
                        <td colspan="4" style="text-align: center;width: 100%;border: 1px solid black;">
                			<strong>OrderDetail</strong>
                		</td>
                		
					</tr>
        			<tr>
                        <td style="text-align: center;width: 25%;border: 1px solid black;">
                			<strong>Product Name</strong>
                		</td>
                		<td style="text-align: center;width: 25%;border: 1px solid black;">
                			<strong>Quantity</strong>
                		</td>
                		<td style="text-align: center;width: 25%;border: 1px solid black;">
                			<strong>Tax</strong>
                		</td>
                		<td style="text-align: center;width: 25%;border: 1px solid black;">
                			<strong>Item Price</strong>
                		</td>
					</tr>';
                    foreach($orderItems as $items){
                    	$string .='<tr>
						      <td style="text-align: center;width: 25%;border: 1px solid black;">'.$items->itemname.'</td>
						      
						      <td style="text-align: center;width: 25%;border: 1px solid black;">'.$items->qty.'</td>
						      <td style="text-align: center;width: 25%;border: 1px solid black;">0%</td>
						      <td style="text-align: right;width: 25%;border: 1px solid black;">'.$items->final_total.'</td>
						     
						    </tr>';
					}
					$string .='<tr>
						      <td colspan="3" style="text-align: right;width: 25%;border: 1px solid black;">Total Price</td>
						      
						      <td style="text-align: right;width: 25%;border: 1px solid black;">'.number_format((float)$order->total_price, 2, '.', '').'</td>
						     
						    </tr>';
                    $string .='</table>';

        $EmailTemplatesObj = Emailtemplates::find(3);
        $msg=nl2br($EmailTemplatesObj->email_temp_body);
        $msg=str_replace("<ORDERDETAIL>",$string,$msg);
        
        /* Start admin mail */
        MailService::send('email.template',$data=['msg'=>$msg], $admin_email, $admin_email, $EmailTemplatesObj->email_temp_subject);
        MailService::send('email.template',$data=['msg'=>$msg], $admin_email, $admin_email='ketulpatel@webmynesystems.com', $EmailTemplatesObj->email_temp_subject);

        /* end admin mail */

        /* Start User mail*/
        MailService::send('email.template',$data=['msg'=>$msg], $admin_email, $user_email, $EmailTemplatesObj->email_temp_subject);
        MailService::send('email.template',$data=['msg'=>$msg], $admin_email, $user_email='ketulpatel@webmynesystems.com', $EmailTemplatesObj->email_temp_subject);
        /* End User mail */

        /* Start Supplier mail*/
        if($Payment_type == 'card' && $supplier_email != ''){
        	MailService::send('email.template',$data=['msg'=>$msg], $admin_email, $supplier_email, $EmailTemplatesObj->email_temp_subject);

        	MailService::send('email.template',$data=['msg'=>$msg], $admin_email, $supplier_email='ketulpatel@webmynesystems.com', $EmailTemplatesObj->email_temp_subject);
        }
        /* End Supplier mail*/
    }
    public static function getsupplierOrdermail($id)
    {
        $order = Order::find($id);
        $user_email=$order->user->email;

        $footer = Footer::find(1);
        $admin_email = $footer->email;
        $supplier_email='';
        $orderItems = OrderItem::select('id', 'item_id', 'price','qty','final_total' ,'type', 'selling_price' ,'supplier_id')->where('order_id' , $order->id)->with('kitItem')->with('kit')->with('course')->with('supplier')->orderBy('id', 'desc')->get();
    
	    foreach ($orderItems as $value) {
	      if($value->type == 'Course'){
	        $value->itemname = $value->course->title_en;
	      }elseif($value->type == 'Kit'){
	        $value->itemname = $value->kit->name_en;
	      }elseif($value->type == 'Item'){
	        $value->itemname = $value->kitItem->items->name_en;
	      }
	      if($supplier_email == ''){
	      		$supplier_email=$value->supplier->email;
	      }
	    }
	    $string ='';
        $string .='<table width="100%" style="border-collapse: collapse;border: 1px solid black;">
        			<tr>
                        <td colspan="4" style="text-align: left;width: 100%;border: 1px solid black;">
                			<strong>OrderID : '.$order->id.'</strong>
                		</td>
                		
					</tr>
					<tr>
                        <td colspan="4" style="text-align: center;width: 100%;border: 1px solid black;">
                			<strong>OrderDetail</strong>
                		</td>
                		
					</tr>
        			<tr>
                        <td style="text-align: center;width: 25%;border: 1px solid black;">
                			<strong>Product Name</strong>
                		</td>
                		<td style="text-align: center;width: 25%;border: 1px solid black;">
                			<strong>Quantity</strong>
                		</td>
                		<td style="text-align: center;width: 25%;border: 1px solid black;">
                			<strong>Tax</strong>
                		</td>
                		<td style="text-align: center;width: 25%;border: 1px solid black;">
                			<strong>Item Price</strong>
                		</td>
					</tr>';
                    foreach($orderItems as $items){
                    	$string .='<tr>
						      <td style="text-align: center;width: 25%;border: 1px solid black;">'.$items->itemname.'</td>
						      
						      <td style="text-align: center;width: 25%;border: 1px solid black;">'.$items->qty.'</td>
						      <td style="text-align: center;width: 25%;border: 1px solid black;">0%</td>
						      <td style="text-align: right;width: 25%;border: 1px solid black;">'.$items->final_total.'</td>
						     
						    </tr>';
					}
					$string .='<tr>
						      <td colspan="3" style="text-align: right;width: 25%;border: 1px solid black;">Total Price</td>
						      
						      <td style="text-align: right;width: 25%;border: 1px solid black;">'.number_format((float)$order->total_price, 2, '.', '').'</td>
						     
						    </tr>';
                    $string .='</table>';

        $EmailTemplatesObj = Emailtemplates::find(3);
        $msg=nl2br($EmailTemplatesObj->email_temp_body);
        $msg=str_replace("<ORDERDETAIL>",$string,$msg);
        if($supplier_email != '')
        {
        	MailService::send('email.template',$data=['msg'=>$msg], $admin_email, $supplier_email, $EmailTemplatesObj->email_temp_subject);
        	MailService::send('email.template',$data=['msg'=>$msg], $admin_email, $supplier_email='ketulpatel@webmynesystems.com', $EmailTemplatesObj->email_temp_subject);
    	}
        
    }
	

}