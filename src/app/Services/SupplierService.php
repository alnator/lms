<?php

namespace App\Services; 
use App\Item;
use App\SupplierItem;
use App\Order;
use Auth;
use App\OrderItem;
use App\OrderStates;
class SupplierService {

	public static function getSupplyingItemsData($filter,  $id){
		$index = $filter ? $filter['pageIndex'] : 0; 
		$item = SupplierItem::select('id','item_id', 'supplier_id', 'price' ,'qty' , 'active')->with('item')->where('supplier_id' , $id);

		if (!empty($filter['id'])){
			$item->where('id' , $filter['id']);
		}
		if (!empty($filter['item']['name_en'])){
			$item->whereHas('item' ,function($query) use ($filter){
				$query->where('name_en' , 'like', '%'.$filter['item']['name_en'].'%');
			});
		}
		if (!empty($filter['item']['name_ar'])){
			$item->whereHas('item' ,function($query)use ($filter){
				$query->where('name_ar' , 'like', '%'.$filter['item']['name_ar'].'%');
			});
		}
		if (!empty($filter['price'])){
			$item->where('price' ,$filter['price']);
		}
		if (!empty($filter['qty'] )){
			$item->where('qty', $filter['qty']);
		}


		$item->orderBy('id','desc');
		$result['total'] = $item->count();

		$skip = ($index == 1) ? 0 : ($index-1)*10 ;
		$result['data']=$item->take(10)->skip($skip)->get();
		
		return $result;			
	}
	public static  function CreateUpdateSupplyingItem($request, $id){
		$item = SupplierItem::updateOrCreate(['item_id'=> $request->item_id , 'supplier_id' => $id] , [
			'price' => $request->price,
			'qty' => $request->qty,
			'active' => $request->active=='on' ? 1 : 0
		]);
		return $item;
	} 


	public static function getWaitingData($filter){
		$index = $filter ? $filter['pageIndex'] : 0;

		/*$orders = OrderItem::join('order', 'order.id', '=', 'order_item.id')
		->where('order_item.supplier_id' , Auth::id())
		->where('order_item.packed' , 0)
		->where('order.status' , '=','paid')->with('item')->with('kit')->with(['order'=> function ($query){
			$query->with('user');
		}]);*/
		$orders = OrderItem::select('order_item.id','order_item.order_id','order_item.price','order_item.type','order_item.item_id','order_item.price','order_item.packed','order.status','order_item.supplier_id')
		->join('order', 'order.id', '=', 'order_item.order_id')
		->where('order_item.supplier_id' , Auth::id())
		->where(function ($query) {
		    $query->where('order.status', '=', 'paid')
		          ->orWhere('order.status', '=', 'unpaid');
		})->where('order_item.packed' , 0)
		->where(function ($query) {
		    $query->where('order_item.type', '=', 'Kit')
		          ->orWhere('order_item.type', '=', 'Item');
		})->with('kitItem')->with('kit')->with(['order'=> function ($query){
			$query->with('user');
		}]);
		



		if (!empty($filter['id'])){
			$orders->where('id' , $filter['id']);
		}
		if (!empty($filter['order_id'])){
			$orders->where('order_id' , $filter['order_id']);
		}
		if (!empty($filter['item']['id'])){
			$orders->whereHas('item' , function($query) use ($filter){
				$query->where('id' , $filter['item']['id']);
			});
		}
		if (!empty($filter['item']['name_en'])){
			$orders->whereHas('item' , function($query) use ($filter){
				$query->where('name_en' , 'like' , '%'. $filter['item']['name_en'].'%');
			});
		}
		if (!empty($filter['price'] )){
			$orders->where('price' , $filter['price']);
		}
		if (!empty($filter['order']['user']['email'])){
			$orders->whereHas('order' , function($query) use ($filter){
				$query->whereHas('user' , function($query) use ($filter){
					$query->where('email' , 'like', '%'.$filter['order']['user']['email'].'%');
				});
			});
		}

		$orders->orderBy('order_item.order_id','desc');
		$result['total'] = $orders->count();

		$skip = ($index == 1) ? 0 : ($index-1)*10 ;
		$order = $orders->take(10)->skip($skip)->get();
		//dd($order);
		foreach ($order as $value) {
			if($value->type == 'Kit'){
				$value->itemid = $value->kit->id;
				$value->itemname = $value->kit->name_en;
			}elseif($value->type == 'Item'){
				//dd($value);
				$value->itemid = $value->kitItem->items->id;
				$value->itemname = $value->kitItem->items->name_en;
			}
		}
		$result['data']=$order;

		return $result;
	}


	public static function getShippingListData($filter){
		$index = $filter ? $filter['pageIndex'] : 0;

		$orders = Order::select('order.*')->join('order_item', 'order_item.order_id', '=', 'order.id')
		->where('order_item.supplier_id' , Auth::id())
		->where('state' , '=' , OrderStates::PACKED)
		->with([
			'items' => function($query){
				$query->with('item');
			}, 'user'
		]);
		//dd($orders);

		if (!empty($filter['id'])){
			$orders->where('id' , $filter);
		}
		if (!empty($filter['user.email'])){
			$orders->whereHas('user' , function($query) use ($filter){
				$query->where('email' , $filter['user.email']);
			});
		}

		$orders->groupBy('order.id')->orderBy('order.id','desc');
		$result['total'] = $orders->count();

		$skip = ($index == 1) ? 0 : ($index-1)*10 ;
		$result['data']=$orders->take(10)->skip($skip)->get();

		return $result;
	}
}



