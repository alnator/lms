<?php
namespace App\Services;

use App\Slug;
use App\Footer;
use App;
class SlugService
{
	public static function cleanIt($string){
        $string = str_replace ('&lt;' , '<', $string);
        $string = str_replace ('&gt;' , '>', $string);
        $string = str_replace ('&nbsp;' , ' ', $string);
        return $string;
    }

    public static function getSlugs(){
    	$slugs =  Slug::where('active', '=', 1)->orderBy('order','asc')->get();
        if (App::getLocale() == 'ar'){
            foreach ($slugs as $slug) {
                $slug->title_en = $slug->title_ar;
            }
        }
        return $slugs;
    }
    public static function getFooter(){
    	return Footer::find(1);
    }
}
   