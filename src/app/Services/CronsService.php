<?php

namespace App\Services;

use Illuminate\Http\Request; 
use App\User;
use App\video;
use App\Star;
use App\UserProgress;
use App\Course;

class CronsService {

    public static function eurikianStars(){
        $stars = [];
        $users = User::get();
        $userprogress = [];
        foreach ($users as $user) {
            $userCount = 0;
            $progress = UserProgress::where('user_id', '=', $user->id)->get();
            foreach ($progress as $prog) {
                if (isset($userprogress[$prog->course_id])){
                    $userprogress[$prog->course_id] ++;
                }
                else {
                    $userprogress[$prog->course_id] = 1;   
                }
            }
            foreach ($userprogress as $key => $value) {
                $courseCount = Video::where('course_id', '=' , $key)->get()->count();
                if ($value == $courseCount ){
                    $userCount ++;
                }       
            }
            $stars[$user->id] = $userCount;
        }
        asort($stars);
        $counter =0 ;
        Star::whereNotNull('id')->delete();
        foreach ($stars as $key => $val) {
            if ($counter == 10 )
                break;
            $user = User::find($key);
            Star::updateOrCreate(['id' => 0], [
                'user_id' => $user->id,
                'image_path' => $user->image_path
            ]);
            $counter++;
        }
    }





}
