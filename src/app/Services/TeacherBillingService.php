<?php

namespace App\Services; 

use Illuminate\Support\Facades\DB;
use App\Payment;
use App\Course;
Use App\User;
use App\PaymentMethod;
use App\Subscription;
use App\SubscriptionModel;

use App\UserProgress;

class TeacherBillingService{

  public static function calculateTeacherBilling($teacherId ,$from , $to , $AWSStream , $singleCourse = false){
    $methods = PaymentMethod::all();
    $singleCourseIds = [];
    $subscriptionIds = [];
    $subscriptionAmount =0 ;
    $singlecoursesAmount = 0;
    if ($singleCourse === "true"){
        foreach ($methods as $method) {
          $amount = self::calculateSingleCourseTotal($from , $to , $method,$teacherId);
          $singlecoursesAmount += $amount['amount'];
          foreach ($amount['ids'] as $id) {
            array_push($singleCourseIds, $id);
          }
        }
    }
    else { 
      foreach ($methods as $method) {
        $amount = self::calculateSubscriptionsTotal($from , $to, $method , $teacherId);
        $subscriptionAmount +=  $amount['amount'];
        foreach ($amount['ids'] as $id) {
          array_push($subscriptionIds,$id);
        }
      }

    }

    $fromDateTime = new \DateTime($from);
    $toDateTime = new \DateTime($to);
    $diff = $fromDateTime->diff($toDateTime);
    $diff = $diff->format('%y') * 12 + $diff->format('%m');

    $AWSStream = empty($AWSStream) ? ($diff+1) * 900 : $AWSStream;
    $amounts = self::deductAWSStreamingCost($AWSStream,$singlecoursesAmount,$subscriptionAmount);

    $teacherViews = self::getViewsByTeacher($teacherId , $from  , $to);
    $subscriptionAmount = self::getTeacherSubscriptionAmount($teacherId , $amounts['subscriptionAmount'] , $teacherViews);
    $singlecoursesAmount = self::getTeacherSingleCourseAmount($teacherId , $amounts['oneCourseAmount']); 
    $teacherPercentage = User::find($teacherId)->fees;
    return [
      'teacherViews' => $teacherViews,
      'teacherId' => $teacherId,
      'singlecoursesAmount' => $singlecoursesAmount,
      'singleCourseIds'=> $singleCourseIds,
      'teacherPercentage' => $teacherPercentage,
      'subscriptionAmount' => $subscriptionAmount,
      'data' => $subscriptionIds,
      'count' => count($subscriptionIds)
    ];
  }
  
  private static function getTeacherCourses($teacherId){
    return Course::where('user_id', '=' , $teacherId)->pluck('id');
  }

  private static function calculateSingleCourseTotal($from , $to , $paymentMethod ,$teacherId){
    //courses ids
    $courses = self::getTeacherCourses($teacherId);
    //payment query
    $payments = Payment::where('payment_method_id' ,'=', $paymentMethod->id)
    ->whereHas('subscription' , function ($query) use ($courses){
      $query->where('subscription_model_id' ,'=',5 )
      ->whereIn('course_id', $courses);
    })->with(['subscription' => function ($query){

      $query->with(['subscriptionModel' ,'user', 'course'])
      ->with('course')
      ->with('user');
    } , 'paymentMethod'] )
    ->where('amount' ,'>', 0)
    ->whereBetween('created_at', [$from, $to]);
    // values
    $amount = $payments->sum('amount');
    $count = $payments->count('id');
    $ids = $payments->get();
    //fees


    $amount = self::deductPaymentGatewayFees($amount , $count , $paymentMethod->fixed_fees, $paymentMethod->percent_fees);

    return ['amount'=>$amount  ,'ids' => $ids];
  }


  private static function calculateSubscriptionsTotal($from , $to , $paymentMethod , $teacherId){
    $payments = Payment::where('payment_method_id' , '=' , $paymentMethod->id)
    ->whereHas('subscription', function ($query){
       $query->where('subscription_model_id','!=',5);
    })->with(['subscription' => function ($query){
      $query->with('course');
      $query->with('user');

      $query->with(['subscriptionModel', 'user']);
    } , 'paymentMethod'])->where('amount' , '>', 0)
    ->whereBetween('created_at', [$from,$to]);


    $amount = $payments->sum('amount');
    $count = $payments->count('id');
    $ids = $payments->get();

    $amount = self::deductPaymentGatewayFees($amount,$count,$paymentMethod->fix_fees,$paymentMethod->percent_fees);
     return ['amount' => $amount,'ids' => $ids];
  }
  private static function deductPaymentGatewayFees($amount , $count , $fixedFees ,$percentageFees){
    $fees = 0;
    if ($amount > 0){
       if($percentageFees > 0){
        $fees = $amount - ($amount * (floatval($percentageFees) / 100)) ;
      }
      if($fixedFees > 0){
        $fees = $fees - (number_format($fixedFees, 2) * $count);
      }
      return $fees;
    }
  }
  private static function deductAWSStreamingCost($awsStreaming,$oneCourseAmount,$subscriptionAmount){
    if(!empty($oneCourseAmount)){
      $oneCourseAmount = $oneCourseAmount - (($oneCourseAmount * 100) / $awsStreaming);
    }

    if(!empty($subscriptionAmount)){
      $subscriptionAmount = $subscriptionAmount - (($subscriptionAmount * 100) / $awsStreaming);
    }

    return ['subscriptionAmount' => $subscriptionAmount,'oneCourseAmount' => $oneCourseAmount];
  }

   private static function getTeacherSingleCourseAmount($teacherId,$singleCourseAmount){
    $teacherPercentage = User::find($teacherId)->fees;
    return  $singleCourseAmount * $teacherPercentage;
  }
  private static function getTeacherSubscriptionAmount($teacherId,$subscriptionAmount,$teacherViews){
    $teacherPercentage = User::find($teacherId)->fees;
    return ($teacherViews * $subscriptionAmount) * $teacherPercentage;
  }

  private static function getViewsByTeacher($teacherId,$from,$to){
    $views =  UserProgress::select(DB::raw('DISTINCT course.user_id as teacherId,count(user_progress.id) as views'))
    ->join('course','course.id','=','user_progress.course_id');

    if(!empty($from) && !empty($to)){
      $views->whereBetween(DB::raw('user_progress.created_at'),array($from, $to));
    }

    $views = $views->groupBy(DB::raw('course.user_id'))
    ->get();
    $viewPercentage = [];
    $sumViews = 0;
    // sum of all views
    foreach ($views as $view) {
      $sumViews = $sumViews + $view->views;
    }

    // view Percentage for each teacher
    foreach ($views as $view) {
      $viewPercentage[$view->teacherId] =  $view->views / $sumViews;
    }
    return !empty($viewPercentage[$teacherId]) ? $viewPercentage[$teacherId] : 0;
  }

}
