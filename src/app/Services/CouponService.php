<?php

namespace App\Services;

use App\Coupon;
use App\GroupUser;
/**
* Coupons Service
*/
class CouponService 
{
	
	public static function validateCoupon($code , $userId , $courseId = 0)
	{
		$coupon = Coupon::where('code' , $code)->where('popup' , 0)->where('qty', '!=' , 0)->first();

		
		if (!isset($coupon)) 
			return false;
		if ($coupon->user_id == 0 || $coupon->user_id == $userId)
		{
			if (isset($coupon->course_id)){
				if ($courseId == $coupon->course_id)
				{
					return true;
				}
				else {
					return false;
				}
			}
			else {
				return true;
			}
		}
		else {
			if (isset($coupon->group_id))
			{
				$user = GroupUser::where('user_id' , $userId)->where('group_id',  $coupon->group_id)->first();
				if (isset($user) && $user instanceof GroupUser)
				{
					return true;
				}
				else {
					return false;
				}
			}
			else 
			{
				return false;
			}
		}
	}


	public static function useCoupon($code) // Must be validated 
	{
		$coupon = Coupon::where('code', $code)->first();
		if ($coupon->qty > 1)
		{
			$coupon->qty -- ;
			$coupon->save();
		} 
		else {
			$coupon->qty--;
			$coupon->popup = 1;
			$coupon->save();
		}
	}

	public static function getCouponAmount($code) // Must be validated
	{
		$coupon = Coupon::where('code', $code )->first();
		if ($coupon->discount_type_id == 1)
			return ['amount' => $coupon->amount , 'type' => 'f'];
		else {
			return ['amount' => $coupon->amount , 'type' => 'p'];
		}
	}
}