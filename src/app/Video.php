<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Session;
use App\User;
use App\Course;

class Video extends Model
{
    // here
    const CLOUDFRONTDOMAIN = 'https://d31z2wsdi58g5n.cloudfront.net';

    protected $table ='video';

    protected $fillable = [
      'name_ar',
      'description_ar',
      'name_en',
      'description_en',
      'image_path',
      'course_id',
      'user_id',
      'estimated_time',
      'seo_meta',
      'kit_id',
      'session_id',
      'order',
      'path',
      'url_identifier',
      'active',
      'is_youtube',
      'created_by',
      'updated_by',
      'free'
    ];

    public function session()
    {
      return $this->belongsToMany(Session::class ,'course_id', 'id');
    }

      public function course(){
      return $this->belongsTo(Course::class , 'course_id', 'id');
    }



    public function updatedBy()
    {
      return $this->belongsTo(User::class , 'updated_by' , 'id');
    }
    public function createdBy()
    {
      return $this->belongsTo(User::class , 'created_by' , 'id');
    }
    public function sessionbyid()
    {
      return $this->belongsTo(Session::class ,'session_id', 'id');
    }

}
