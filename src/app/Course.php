<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Session;
use App\Level;
use App\User;
use App\Downloadable;
use App\Kit;
class Course extends Model
{
  protected $table = "course";

  protected $fillable = [
      'level_id',
      'lang_id',
      'title_ar',
      'overview_ar',
      'title_en',
      'overview_en',
      'price',
      'user_id',
      'seo_meta_title_ar',
      'seo_meta_title_en',
      'description_ar',
      'description_en',
      'text_en',
      'text_ar',
      'url_identifier',
      'image_path',
      'kit_id',
      'course_estimated_time',
      'intro_video_path',
      'active',
      'is_youtube',
      'slug',
      'created_by',
      'updated_by',
      'emonth'
  ];

  public function session()
  {
    return $this->hasMany(Session::class , 'course_id', 'id');
  }
  public function lession()
  {
    return $this->hasMany(Video::class , 'course_id', 'id');
  }
  public function level(){
    return $this->belongsTo(Level::class , 'level_id', 'id'); 
  }
  public function updatedBy()
  {
    return $this->belongsTo(User::class , 'updated_by' , 'id');
  }
  public function createdBy()
  {
    return $this->belongsTo(User::class , 'created_by' , 'id');
  }

  public function downloadable()
  {
    return $this->belongsTo(Downloadable::class ,'id' , 'course_id');
  }
  public function getkit(){
    return $this->belongsTo(Kit::class , 'kit_id', 'id'); 
  }
}
