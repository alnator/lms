<?php

namespace App;
use App\User;
use App\Course;
use App\SubscriptionModel;
use App\Payment;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
  protected $table = 'subscription';

  protected $fillable = [
      'user_id',
      'payment_id',
      'payment_method_id',
      'course_id',
      'start_date',
      'end_date',
      'status',
      'stripe_subscription_id',
      'subscription_model_id',
      'percentage_progress'
  ];


  public function user(){
    return $this->belongsTo(User::class, 'user_id', 'id');
  }
  public function course(){
    return $this->belongsTo(Course::class, 'course_id', 'id');
  }
  public function subscriptionModel(){
    return $this->belongsTo(SubscriptionModel::class , 'subscription_model_id', 'id');
  }

  public function payment(){
    return $this->belongsTo(Payment::class, 'payment_id', 'id');
  }
}
