<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Kit;
use App\Course;
use App\KitItem;
use App\SupplierItem;
use App\Services\OrderService;
class Cartdetail extends Model
{
    protected $table= 'cart_detail';
    protected $fillable = ['cart_id', 'product_id', 'type', 'price', 'qty', 'final_total','supplierid'];

    public function getkit(){
    	return $this->belongsTo(Kit::class , 'product_id', 'id'); 
  	}
  	public function getCourse(){
    	return $this->belongsTo(Course::class , 'product_id', 'id'); 
  	}
  	public function getKitItem(){
    	return $this->belongsTo(KitItem::class , 'product_id', 'id'); 
  	}
    public function getKitAvailable($kitid){
      $kitprice =OrderService::getKitMinimumPrice($kitid);
      return $kitprice['available'];
    }
    public function getItemAvailable($itemid,$supplierid){
      $supplieritem =SupplierItem::where('item_id','=',$itemid)->where('supplier_id','=',$supplierid)->where('active','=','1')->get()->first();
      $qty = 0;
      if(isset($supplieritem->qty))
      {
        $qty = $supplieritem->qty;
      }else{
        $qty = 0;
      }
      return $qty;
    }

}
