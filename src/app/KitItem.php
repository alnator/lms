<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Kit;
use App\Item;
use App\ItemImage;

class KitItem extends Model
{
    protected $table = 'kit_item';
    protected $fillable = ['id','kit_id','item_id'];

    public function items(){
    	return $this->belongsTo(Item::class , 'item_id', 'id');
    }

    public function supplierItem(){
    	return $this->hasMany(SupplierItem::class , 'item_id', 'item_id');
    }
    
    public function kit(){
    	return  $this->belongsToMany(Kit::class , 'id' , 'kit_id');
    }

    public function images(){
        return $this->hasMany(ItemImage::class , 'item_id', 'item_id'); 
    }
}
