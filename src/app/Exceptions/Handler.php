<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Services\SlugService;
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {

      if ($exception instanceof HttpResponseException) {
          
            return $exception->getResponse();
        
        } elseif ($exception instanceof ModelNotFoundException) {
            
            
            $exception = new NotFoundHttpException($e->getMessage(), $exception);
        
        } elseif ($exception instanceof AuthenticationException) {
            return $this->unauthenticated($request, $exception);
            
        
        } elseif ($exception instanceof AuthorizationException) {
            
            
            $exception = new HttpException(403, $e->getMessage());
        
        } elseif ($exception instanceof ValidationException && $exception->getResponse()) {
            
           
            return $exception->getResponse();
        }else{
            // dd($exception);
           //   self::langService();

        $slugs = SlugService::getSlugs();
       //  dd($slugs);
        $footer = SlugService::getFooter();
//              return view ('client.pages.500')->withSlugs($slugs)
//            ->withFooter($footer);
        }

        return parent::render($request, $exception);
    }
}
