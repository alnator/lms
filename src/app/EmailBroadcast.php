<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailBroadcast extends Model
{
    protected $table='email_broadcast';
    protected $fillable = ['from' , 'subject', 'template'];
}
