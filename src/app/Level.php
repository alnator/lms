<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Course;
use App\User;
class Level extends Model
{
  protected $table = "level";

  protected $fillable = [
      'name_ar',
      'name_en',
      'image_path_ar',
      'image_path_en',
      'icon_path',
      'description_ar',
      'description_en',
      'slug',
      'active',
      'created_by',
      'updated_by'
  ];
  public function course(){
  	return $this->hasMany(Course::class,'level_id', 'id');
  }
  public function updatedBy()
  {
    return $this->belongsTo(User::class , 'updated_by' , 'id');
  }
  public function createdBy()
  {
    return $this->belongsTo(User::class , 'created_by' , 'id');
  }
}
