<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Bannerimages extends Model
{
  protected $table = "banner_images";

  protected $fillable = [
      'display_area',
      'image_path',
      'display_size',
      'created_by',
      'updated_by'
  ];
  
  public function updatedBy()
  {
    return $this->belongsTo(User::class , 'updated_by' , 'id');
  }
  public function createdBy()
  {
    return $this->belongsTo(User::class , 'created_by' , 'id');
  }
}
