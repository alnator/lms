<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StarsText extends Model
{
    protected $table = 'eurekian_stars_text';
    protected $fillable = [ 'id', 'created_at', 'updated_at','text', 'text_ar'];
    
}
