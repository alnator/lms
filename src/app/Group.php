<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\GroupUser;
class Group extends Model
{
    protected $table = 'groups';
    protected $fillable = ['name' , 'created_by' , 'updated_by'];

    public function groups_users(){
    	return $this->blongsToMany(GroupUser::class);
    }
}
