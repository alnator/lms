<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Star extends Model
{
    protected $table = 'stars';

    protected $fillable = ['id', 'user_id', 'image_path', 'created_at', 'updated_at'];

    public function user(){
    	return $this->belongsTo(User::class, 'user_id','id');
    } 
}
