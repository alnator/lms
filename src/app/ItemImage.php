<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Item;
class ItemImage extends Model
{
    protected $table = 'item_img';
    protected $fillable = ['img_src' , 'item_id'];

    public function item(){
    	return $this->belongsTo(Item::class, 'id', 'item_id');
    }
}
