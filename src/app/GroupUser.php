<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Group;
use App\User;

class GroupUser extends Model
{
    protected $table = 'groups_users';
    protected $fillable = ['group_id', 'user_id'];


    public function group(){
    	return $this->HasMany(Group::class, 'id' , 'group_id' );
    }
    public function user(){
    	return $this->belongsTo(User::class , 'user_id' , 'id');
    }
}
