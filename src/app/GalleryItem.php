<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Album;

class GalleryItem extends Model
{
    public const IMAGE = 1;
    public const VIDEO = 2;

    protected $table= 'gallery';

    protected $fillable = [
    	'id', 
    	'title_en',
    	'title_ar',
    	'overview_en',
    	'overview_ar',
    	'img_src',
    	'youtube_embed',
    	'type',
        'album_id'
    ];
     
    public function album()
    {
        return $this->belongsTo(Album::class);
    }
}
