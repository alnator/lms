<?php 

namespace App\Helper;
use App\Footer;
use App\Subscription;
use Carbon\Carbon;
use Auth;

class Helper
{
    CONST vimeoToken = 'f79c4a0561ff57eb45da08a9c1ae0903';

	 public static function showCurrency($c_price)
     {

     	$footerobj=Footer::find(1);


     	if($footerobj->currency_position == 'after')
     	{
     		$price=$c_price." ".$footerobj->currency_symbol;
     	}else{
     		$price=$footerobj->currency_symbol." ".$c_price;
     	}	

     	return $price;
        
     }


      public static function showPlanStatus($courseid)
     {

          $expiryobj = Subscription::whereDate('end_date','<=',Carbon::today())->where('user_id',  '=' , Auth::id())->where('course_id',$courseid)->where('status' ,'active')->get()->first();
        if(isset($expiryobj)){

          return "Expired";
        }else{
            return "";
        }

        
     }



      public static function showpurchase($courseid)
     {

       

           $subscriptions = Subscription::whereDate('end_date','<=',Carbon::today())->where('user_id',  '=' , Auth::id())->where('course_id' ,$courseid)->where('status' ,'active')->get()->first();


        if(isset($subscriptions)){

          return "0";
        }else{
            return "1";
        }

        
     }


     public static function showvimeo($vid){

      $imgid = $vid;
      return self::vimeoVideoDuration($imgid);
     }

    public static function vimeoVideoDuration($video_url) {

        $authorization = self::vimeoToken;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.vimeo.com/videos/{$video_url}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer {$authorization}",
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        if (empty($err)) {
            $info = json_decode($response);

            if(isset($info->thumbnail_medium)){
                return (int)$info->thumbnail_medium;
            }
        }
        return false;
    }


     


}