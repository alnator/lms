<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Video;
use App\Course;
use App\User;
class Session extends Model
{
    protected $table = 'session';

    protected $fillable = ['id', 'session_number', 'session_number_ar', 'order', 'course_id', 'created_at', 'updated_at', 'created_by', 'updated_by'];
    public function video(){
    	return $this->hasMany(Video::class ,'session_id', 'id');
    }
    public function course(){
    	return $this->belongsTo(Course::class , 'course_id', 'id');
    }

    public function updatedBy()
    {
      return $this->belongsTo(User::class , 'updated_by' , 'id');
    }
    public function createdBy()
    {
      return $this->belongsTo(User::class , 'created_by' , 'id');
    }
}
