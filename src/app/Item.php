<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\KitItem;
use App\SupplierItem;
use App\ItemImage;
class Item extends Model
{
    protected $table= 'item';
    protected $fillable = ['id'	,'name_en','name_ar' , 'description' ,'updated_by' , 'created_by'];

    public function kit(){
    	return $this->hasMany(KitItem::class , 'item_id' , 'id');
   	}    

   	public function supplier(){
   		return $this->hasMany(SupplierItem::class , 'item_id', 'id');
   	}

   	public function images(){
   		return $this->hasMany(ItemImage::class , 'item_id', 'id'); 
   	}
}
