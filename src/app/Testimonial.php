<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Testimonial extends Model
{
  protected $table = "testimonial";

  protected $fillable = [
      'name_ar',
      'name_en',
      'title_ar',
      'title_en',
      'description_ar',
      'description_en',
      'image_path_ar',
      'image_path_en',
      'icon_path',
      'slug',
      'active',
      'type',
      'course_id',
      'created_by',
      'updated_by'
  ];
  
  public function updatedBy()
  {
    return $this->belongsTo(User::class , 'updated_by' , 'id');
  }
  public function createdBy()
  {
    return $this->belongsTo(User::class , 'created_by' , 'id');
  }
}
