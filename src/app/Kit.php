<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\KitItem;
use App\KitImage;

class Kit extends Model
{
    protected $table = 'kit';
    protected $fillable = [	'id','name_en','name_ar','description_ar' , 'description_en','created_by' ,'updated_by' ,'price','active','created_at','updated_at'];

    public function items(){
    	return $this->hasMany(KitItem::class , 'kit_id', 'id');
    }

    public function  images(){
    	return $this->hasMany(KitImage::class ,'kit_id', 'id');
    }

   
}
