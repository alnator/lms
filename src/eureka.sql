-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 22, 2020 at 12:39 PM
-- Server version: 5.7.20-0ubuntu0.17.10.1
-- PHP Version: 7.1.17-0ubuntu0.17.10.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eureka`
--

-- --------------------------------------------------------

--
-- Table structure for table `abandoned_carts`
--

CREATE TABLE `abandoned_carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `subscription_model_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `payment_method_id` int(11) DEFAULT NULL,
  `coupon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abandoned_carts`
--

INSERT INTO `abandoned_carts` (`id`, `user_id`, `subscription_model_id`, `course_id`, `payment_method_id`, `coupon`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 6, 1, 'qwe', '2018-06-13 08:58:55', '2018-06-13 08:58:55');

-- --------------------------------------------------------

--
-- Table structure for table `achievements`
--

CREATE TABLE `achievements` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_ar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_date` date NOT NULL,
  `img_src` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `achievements`
--

INSERT INTO `achievements` (`id`, `title_en`, `title_ar`, `description_en`, `description_ar`, `event_date`, `img_src`, `created_at`, `updated_at`) VALUES
(1, 'Intel ISEF International Competition', 'test', 'Participated successfully in Intel ISEF International Competition -2015.', 'test', '2014-10-13', NULL, '2019-11-04 07:27:45', '2019-11-06 00:30:42'),
(2, 'test 2', 'test', 'trerar', 'rarwar', '2019-11-02', NULL, '2019-11-04 12:57:04', '2019-11-04 12:57:04'),
(3, 'fgbfg', 'fyty', 'drgvdf', 'xfdvdfs', '2019-12-04', NULL, '2019-11-10 11:00:29', '2019-11-10 11:00:29'),
(4, 'fgbfg', 'fyty', 'xdfvsdvsd', 'xc xc x', '2019-12-03', NULL, '2019-11-10 11:01:06', '2019-11-10 11:01:06'),
(5, 'sertgsert4t3 43', 'fgbfg', 'ghngh', 'fdvf', '2019-12-06', NULL, '2019-11-10 11:21:34', '2019-11-10 11:21:34'),
(6, 'sertgsert4t3 43', 'fgbfg', 'dfvfd', 'fgbgf', '2019-12-07', NULL, '2019-11-10 11:23:09', '2019-11-10 11:23:09'),
(7, 'fgbfg', 'fgbfg', 'dfvdf', 'sdcs', '2019-12-07', 'http://dn734b9758kwp.cloudfront.net/images/1573392367error.JPG', '2019-11-10 11:26:07', '2019-11-10 11:26:20'),
(9, 'achievement1', 'achievement1', 'achievement1', 'achievement1', '2020-04-29', 'http://dn734b9758kwp.cloudfront.net/images/1585893067star-1.png', '2020-04-03 00:21:07', '2020-04-03 00:21:10'),
(10, 'Achievements', 'الإنجازات', 'Achievements are things you did that had a lasting impact for your company or client. It is a result that you personally bring about while fulfilling a particular role. Typically they are things that you created, built, designed, sold or initiated.', 'الإنجازات هي أشياء فعلتها وكان لها تأثير دائم على شركتك أو عميلك. إنها نتيجة تجلبها شخصيًا أثناء أداء دور معين. عادةً ما تكون الأشياء التي قمت بإنشائها أو إنشائها أو تصميمها أو بيعها أو بدءها.', '2020-05-09', NULL, '2020-04-16 23:52:36', '2020-04-16 23:52:36'),
(11, 'Achievements', 'الإنجازات', 'Achievements are things you did that had a lasting impact for your company or client. It is a result that you personally bring about while fulfilling a particular role. Typically they are things that you created, built, designed, sold or initiated.', 'الإنجازات هي أشياء فعلتها وكان لها تأثير دائم على شركتك أو عميلك. إنها نتيجة تجلبها شخصيًا أثناء أداء دور معين. عادةً ما تكون الأشياء التي قمت بإنشائها أو إنشائها أو تصميمها أو بيعها أو بدءها.', '2020-05-09', 'http://dn734b9758kwp.cloudfront.net/images/1587101515Energeyimg-1.jpg', '2020-04-16 23:54:06', '2020-04-17 00:01:57');

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `album_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `img_src` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `name_en`, `name_ar`, `filter`, `album_date`, `created_at`, `updated_at`, `img_src`) VALUES
(1, 'bright innovator graduation', 'bright innovator graduation', 'brightinnovatorgraduation', '2020-04-03', '2019-11-06 13:43:37', '2020-05-21 05:24:58', 'http://dn734b9758kwp.cloudfront.net/images/1590058496Energeyimg-1.jpg'),
(3, 'Little innovator', 'Little innovator', 'littleinnovator', '2020-04-03', '2019-11-06 13:43:37', '2020-05-21 05:25:13', 'http://dn734b9758kwp.cloudfront.net/images/1590058511Energeyimg-2.jpg'),
(4, 'kit', 'kit', 'kit', '2020-06-01', '2020-05-21 02:23:42', '2020-05-21 05:25:36', 'http://dn734b9758kwp.cloudfront.net/images/1590058533card.jpg'),
(5, 'kit', 'kit', 'kit', '2020-05-21', '2020-05-21 02:23:58', '2020-05-21 05:26:09', 'http://dn734b9758kwp.cloudfront.net/images/1590058567kit-1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `banner_images`
--

CREATE TABLE `banner_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `display_area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `display_size` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banner_images`
--

INSERT INTO `banner_images` (`id`, `image_path`, `display_area`, `display_size`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(47, '/uploads/login-pic.jpg', 'login', '600 x 700', NULL, '2020-04-14 08:13:17', 1, 1),
(48, '/uploads/login-pic.jpg', 'register', '600 x 700', NULL, '2020-04-14 23:17:57', 1, 1),
(49, '/uploads/course-banner.jpg', 'About', '1366 x 308', NULL, '2020-04-14 08:10:32', 1, 1),
(50, '/uploads/course-banner.jpg', 'contact-us', '1366 x 308', NULL, '2020-04-14 08:08:27', 1, 1),
(51, '/uploads/course-banner.jpg', 'policies', '1366 x 308', NULL, '2020-04-14 08:08:27', 1, 1),
(52, '/uploads/course-banner.jpg', 'terms-of-user', '1366 x 308', NULL, '2020-04-14 08:08:27', 1, 1),
(53, '/uploads/course-banner.jpg', 'help', '1366 x 308', NULL, '2020-04-14 08:08:27', 1, 1),
(54, '/uploads/course-banner.jpg', 'careers', '1366 x 308', NULL, '2020-04-14 08:08:27', 1, 1),
(55, '/uploads/course-banner.jpg', 'press', '1366 x 308', NULL, '2020-04-14 08:08:27', 1, 1),
(56, '/uploads/course-banner.jpg', 'become-a-teacher', '1366 x 308', NULL, '2020-04-14 08:08:27', 1, 1),
(57, '/uploads/course-banner.jpg', 'become-a-member', '1366 x 308', NULL, '2020-04-14 08:08:27', 1, 1),
(58, '/uploads/course-banner.jpg', 'free-classes', '1366 x 308', NULL, '2020-04-14 08:08:27', 1, 1),
(59, '/uploads/course-banner.jpg', 'refer-a-friend', '1366 x 308', NULL, '2020-04-14 08:08:27', 1, 1),
(60, '/uploads/course-banner.jpg', 'highlights', '1366 x 308', NULL, '2020-04-14 08:08:27', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `uid` varchar(250) NOT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `coupon` decimal(10,2) DEFAULT '0.00',
  `coupon_code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `uid`, `total`, `created_at`, `updated_at`, `coupon`, `coupon_code`) VALUES
(70, '11', '762.50', '2020-06-10 05:09:37', '2020-06-11 13:02:09', '0.00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart_detail`
--

CREATE TABLE `cart_detail` (
  `id` int(11) NOT NULL,
  `cart_id` varchar(250) NOT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `supplierid` int(11) DEFAULT NULL,
  `type` enum('Course','Kit','Item') DEFAULT 'Item',
  `price` decimal(10,2) DEFAULT '0.00',
  `qty` int(11) DEFAULT '0',
  `final_total` decimal(10,2) DEFAULT '0.00',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `cart_detail`
--

INSERT INTO `cart_detail` (`id`, `cart_id`, `product_id`, `supplierid`, `type`, `price`, `qty`, `final_total`, `created_at`, `updated_at`) VALUES
(109, '70', '5', 1, 'Kit', '230.00', 1, '230.00', '2020-06-10 05:09:37', '2020-06-10 05:09:37'),
(112, '70', '4', 1, 'Kit', '517.50', 1, '517.50', '2020-06-11 12:19:06', '2020-06-11 12:19:06'),
(113, '70', '25', NULL, 'Course', '15.00', 1, '15.00', '2020-06-11 13:00:10', '2020-06-11 13:00:10');

-- --------------------------------------------------------

--
-- Table structure for table `choices`
--

CREATE TABLE `choices` (
  `id` int(10) UNSIGNED NOT NULL,
  `choice_statement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `choice_statement_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correct` tinyint(1) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `choices`
--

INSERT INTO `choices` (`id`, `choice_statement`, `choice_statement_ar`, `correct`, `quiz_id`, `created_at`, `updated_at`) VALUES
(1, 'This is a Test Choice Choose the one that mentioned at top', 'dsd', 1, 1, '2018-07-25 10:03:18', '2020-04-16 08:46:46'),
(2, 'This is a Test Choice Choose the one that mentioned at top Correct', 'dsd', 0, 1, '2018-07-25 10:04:08', '2020-04-16 08:46:50'),
(3, 'This is a Test Choice Choose the one that mentioned at top', '', 0, 1, '2018-07-25 10:04:20', '2018-07-25 10:04:20'),
(4, 'This is a Test Choice Choose the one that mentioned at top', '', 0, 1, '2018-07-25 10:04:27', '2018-07-25 10:04:27'),
(5, 'This is a Test Choice Choose the one that mentioned at top', 'sdsd', 1, 2, '2018-07-25 10:03:18', '2020-04-16 08:46:35'),
(6, 'This is a Test Choice Choose the one that mentioned at top', 'dsd', 0, 2, '2018-07-25 10:04:08', '2020-04-16 08:46:31'),
(7, 'This is a Test Choice Choose the one that mentioned at top Correct', 'ds', 0, 2, '2018-07-25 10:04:20', '2020-04-16 08:46:24'),
(8, 'This is a Test Choice Choose the one that mentioned at top', '', 0, 2, '2018-07-25 10:04:27', '2018-07-25 10:04:27'),
(9, 'This is a Test Choice Choose the one that mentioned at top', 'ds', 1, 3, '2018-07-25 10:03:18', '2020-04-16 08:45:49'),
(10, 'This is a Test Choice Choose the one that mentioned at top', 'dsd', 0, 3, '2018-07-25 10:04:08', '2020-04-16 08:45:46'),
(11, 'This is a Test Choice Choose the one that mentioned at top', '', 0, 3, '2018-07-25 10:04:20', '2018-07-25 10:04:20'),
(12, 'This is a Test Choice Choose the one that mentioned at top Correct', 'ds', 0, 3, '2018-07-25 10:04:27', '2020-04-16 08:46:11'),
(13, 'This is a Test Choice Choose the one that mentioned at top Correct', 'ds', 1, 4, '2018-07-25 10:03:18', '2020-04-16 08:45:29'),
(14, 'This is a Test Choice Choose the one that mentioned at top', 'ds', 0, 4, '2018-07-25 10:04:08', '2020-04-16 08:45:25'),
(15, 'This is a Test Choice Choose the one that mentioned at top', '', 0, 4, '2018-07-25 10:04:20', '2018-07-25 10:04:20'),
(16, 'This is a Test Choice Choose the one that mentioned at top', '', 0, 4, '2018-07-25 10:04:27', '2018-07-25 10:04:27'),
(17, 'This is a Test Choice Choose the one that mentioned at top', 'ds', 1, 5, '2018-07-25 10:03:18', '2020-04-16 08:45:12'),
(18, 'This is a Test Choice Choose the one that mentioned at top Correct', 'dsd', 1, 5, '2018-07-25 10:04:08', '2020-04-16 08:45:08'),
(19, 'This is a Test Choice Choose the one that mentioned at top', '', 0, 5, '2018-07-25 10:04:20', '2018-07-25 10:04:20'),
(20, 'This is a Test Choice Choose the one that mentioned at top', '', 0, 5, '2018-07-25 10:04:27', '2018-07-25 10:04:27'),
(21, 'This is a Test Choice Choose the one that mentioned at top', '', 0, 6, '2018-07-25 10:03:18', '2018-07-25 10:03:18'),
(22, 'This is a Test Choice Choose the one that mentioned at top', '', 1, 6, '2018-07-25 10:04:08', '2018-07-25 10:04:08'),
(23, 'This is a Test Choice Choose the one that mentioned at top', '', 0, 6, '2018-07-25 10:04:20', '2018-07-25 10:04:20'),
(24, 'This is a Test Choice Choose the one that mentioned at top', '', 0, 6, '2018-07-25 10:04:27', '2018-07-25 10:04:27'),
(63, 'First Choice', 'ffdf', 1, 17, '2018-07-29 10:51:30', '2020-04-16 09:00:24'),
(64, 'Second Choice', 'fdf', 0, 17, '2018-07-29 10:51:37', '2020-04-16 09:00:28'),
(65, 'Third Choice', 'fdf', 0, 17, '2018-07-29 10:51:49', '2020-04-16 09:00:35'),
(66, 'Fourth Choice', 'fd', 0, 17, '2018-07-29 10:51:56', '2020-04-16 09:00:41'),
(67, 'First Choice', 'fdf', 1, 18, '2018-07-29 10:52:40', '2020-04-16 08:59:58'),
(68, 'Second Choice', '', 0, 18, '2018-07-29 10:52:50', '2018-07-29 11:12:16'),
(70, 'Third Choice', 'fdf', 0, 18, '2018-07-29 10:53:01', '2020-04-16 09:00:06'),
(71, 'Fourth Choice', '', 0, 18, '2018-07-29 10:53:08', '2018-07-29 11:12:32'),
(72, 'First Choice', 'fddf', 1, 19, '2018-07-29 10:54:15', '2020-04-16 08:59:41'),
(73, 'Second Choice', 'fdfdf', 0, 19, '2018-07-29 10:54:22', '2020-04-16 08:59:44'),
(74, 'Third Choice', '', 0, 19, '2018-07-29 10:54:30', '2018-07-29 11:11:55'),
(75, 'Fourth Choice', '', 0, 19, '2018-07-29 10:54:38', '2018-07-29 11:11:57'),
(76, 'First Choice', 'fd', 1, 20, '2018-07-29 10:55:21', '2020-04-16 08:59:15'),
(77, 'Second Choice', 'fd', 0, 20, '2018-07-29 10:55:30', '2020-04-16 08:59:18'),
(78, 'Third Choice', 'fdf', 0, 20, '2018-07-29 10:55:39', '2020-04-16 08:59:24'),
(79, 'Fourth Choice', 'fdf', 0, 20, '2018-07-29 10:55:46', '2020-04-16 08:59:28'),
(80, 'First Choice', 'fsf', 1, 21, '2018-07-29 10:56:20', '2020-04-16 08:58:58'),
(81, 'Second Choice', '', 0, 21, '2018-07-29 10:56:29', '2018-07-29 11:11:16'),
(82, 'Third Choice', '', 0, 21, '2018-07-29 10:56:36', '2018-07-29 11:11:21'),
(83, 'Fourth Choice', '', 0, 21, '2018-07-29 10:56:45', '2018-07-29 11:11:23'),
(84, 'this is a wrong answer', 'ljkl', 1, 22, '2018-08-13 10:36:42', '2020-04-16 09:05:33'),
(85, 'this is a correct answer', 'jkl', 0, 22, '2018-08-13 10:36:55', '2020-04-16 09:05:38'),
(86, 'this is another wrong answer', '', 0, 22, '2018-08-13 10:37:22', '2018-08-13 10:37:22'),
(87, 'this is the last wrong answer', '', 0, 22, '2018-08-13 10:37:36', '2018-08-13 10:37:36'),
(88, 'this is a correct answer', 'lkjl', 1, 23, '2018-08-13 10:38:13', '2020-04-16 09:05:21'),
(89, 'This is  a wrong answer', '', 0, 23, '2018-08-13 10:38:26', '2018-08-13 10:38:26'),
(90, 'this is another wrong answer', '', 0, 23, '2018-08-13 10:38:38', '2018-08-13 10:38:38'),
(91, 'this is the last wrong answer', '', 0, 23, '2018-08-13 10:38:52', '2018-08-13 10:38:52'),
(92, 'this is a wrong answer', 'khjk', 1, 24, '2018-08-13 10:39:29', '2020-04-16 09:05:07'),
(93, 'dont touch this choice (it has a bomb)', '', 0, 24, '2018-08-13 10:40:05', '2018-08-13 10:40:05'),
(94, 'this is a wrong answer', '', 0, 24, '2018-08-13 10:40:25', '2018-08-13 10:40:25'),
(95, 'this is the correct answer', 'khjk', 0, 24, '2018-08-13 10:40:41', '2020-04-16 09:05:12'),
(96, 'this is a choice that will eat your brain', 'hkh', 1, 25, '2018-08-13 10:41:27', '2020-04-16 09:04:48'),
(97, 'this is  a correct answer', 'khj', 0, 25, '2018-08-13 10:41:38', '2020-04-16 09:04:52'),
(98, 'Trap', '', 0, 25, '2018-08-13 10:41:46', '2018-08-13 10:41:46'),
(99, 'the last wrong answer', '', 0, 25, '2018-08-13 10:42:03', '2018-08-13 10:42:03'),
(100, 'in logic\'s function', 'in logic\'s function', 1, 26, '2018-08-13 10:42:37', '2018-11-05 17:04:08'),
(101, 'at Lego\'s Configurations', 'at Lego\'s Configurations', 0, 26, '2018-08-13 10:42:50', '2020-04-16 09:04:15'),
(102, 'in physics function', 'in physics function', 0, 26, '2018-08-13 10:43:03', '2020-04-16 09:04:23'),
(103, 'no where', 'no where', 0, 26, '2018-08-13 10:43:20', '2020-04-16 09:04:28'),
(104, 'this is a test choice don\'t touch it', '', 0, 27, '2018-08-13 10:45:25', '2018-08-13 10:45:25'),
(105, 'its a trap don\'t choose me', '', 0, 27, '2018-08-13 10:45:43', '2018-08-13 10:45:43'),
(106, 'this is a correct', '', 1, 27, '2018-08-13 10:45:55', '2018-08-13 10:45:55'),
(107, 'leave me', '', 0, 27, '2018-08-13 10:46:03', '2018-08-13 10:46:03'),
(108, 'this is a choice', '', 0, 28, '2018-08-13 10:46:32', '2018-08-13 10:46:32'),
(109, 'this is the quiz you will fail', '', 0, 28, '2018-08-13 10:46:53', '2018-08-13 10:46:53'),
(110, 'all choices is wrong', '', 0, 28, '2018-08-13 10:47:03', '2018-08-13 10:47:03'),
(111, 'I\'ve told you this is a trap', '', 0, 28, '2018-08-13 10:47:37', '2018-08-13 10:47:37'),
(112, 'this is test choice (wrong)', '', 0, 29, '2018-08-13 10:48:07', '2018-08-13 10:48:07'),
(113, 'this is a correct choice', '', 1, 29, '2018-08-13 10:48:17', '2018-08-13 10:48:28'),
(114, 'wrong', '', 0, 29, '2018-08-13 10:48:23', '2018-08-13 10:48:23'),
(115, 'this is a wrong choice', '', 0, 29, '2018-08-13 10:48:46', '2018-08-13 10:48:46'),
(116, 'this is a choice', '', 0, 30, '2018-08-13 10:49:13', '2018-08-13 10:49:13'),
(117, 'this is a choice', '', 0, 30, '2018-08-13 10:49:21', '2018-08-13 10:49:21'),
(118, 'this is a choice', '', 0, 30, '2018-08-13 10:49:31', '2018-08-13 10:49:31'),
(119, 'this is a correct choice', '', 1, 30, '2018-08-13 10:49:42', '2018-08-13 10:49:42'),
(120, 'this is choice', '', 1, 31, '2018-08-13 10:50:03', '2018-08-13 10:50:03'),
(121, 'this is choice', '', 1, 31, '2018-08-13 10:50:09', '2018-08-13 10:50:09'),
(122, 'this is choice', '', 1, 31, '2018-08-13 10:50:16', '2018-08-13 10:50:16'),
(123, 'all choices are right', '', 1, 31, '2018-08-13 10:50:26', '2018-08-13 10:51:38'),
(124, 'qwe', 'ضصث', 1, 32, '2018-09-05 11:16:20', '2018-09-05 11:16:20'),
(125, '123', 'qwe', 0, 32, '2018-09-05 11:16:27', '2018-09-05 11:16:27'),
(126, 'lorem', 'ضصيسشسي', 1, 33, '2018-09-05 11:24:28', '2020-04-17 01:12:06'),
(127, 'Arduino Mega', 'Arduino Mega', 0, 34, '2019-10-28 21:11:40', '2019-10-28 21:11:40'),
(128, 'Arduino Nano', 'Arduino Nano', 0, 34, '2019-10-28 21:11:57', '2019-10-28 21:11:57'),
(129, 'Arduino Uno', 'Arduino Uno', 1, 34, '2019-10-28 21:12:10', '2019-10-28 21:12:10'),
(130, 'Arduino Due', 'Arduino Due', 0, 34, '2019-10-28 21:12:39', '2019-10-28 21:12:39'),
(131, '0 - 1023', '0 - 1023', 1, 35, '2019-10-28 21:27:16', '2019-10-28 21:27:16'),
(132, '0 - 255', '0 - 255', 0, 35, '2019-10-28 21:27:36', '2019-10-28 21:27:36'),
(133, '0 - 1', '0 - 1', 0, 35, '2019-10-28 21:27:50', '2019-10-28 21:27:50'),
(134, '10 - 150', '10 - 150', 0, 35, '2019-10-28 21:28:03', '2019-10-28 21:28:03'),
(135, 'C++', 'C++', 0, 36, '2019-10-28 21:33:52', '2019-10-28 21:33:52'),
(136, 'C#', 'C#', 0, 36, '2019-10-28 21:34:00', '2019-10-28 21:34:00'),
(137, 'JavaScript', 'JavaScript', 0, 36, '2019-10-28 21:34:13', '2019-10-28 21:35:36'),
(138, 'Arduino C', 'Arduino C', 1, 36, '2019-10-28 21:34:28', '2019-10-28 21:34:28'),
(139, 'Send the code for the Arduino', 'إرسال البرمجة إلى الاردوينو', 0, 37, '2019-10-28 21:40:02', '2019-10-28 21:40:02'),
(140, 'Check if the code is error free', 'يتأكد من عدم وجود أخطاء في البرمجة', 1, 37, '2019-10-28 21:40:51', '2019-10-28 21:40:51'),
(141, 'Check if the sky is blue!', 'يتأكد من أن لون السماء أزرق!', 0, 37, '2019-10-28 21:41:26', '2019-10-28 21:41:26'),
(142, 'Checks if the code is logicly ture', 'يتأكد من صحة البرمجة من ناحية المنطق', 0, 37, '2019-10-28 21:43:20', '2019-10-28 21:43:20'),
(144, 'intger val = 6;', 'intger val = 6;', 0, 39, '2019-10-29 20:50:06', '2019-10-29 20:50:06'),
(145, 'int val = 6', 'int val = 6', 0, 39, '2019-10-29 20:50:19', '2019-10-29 20:50:19'),
(146, 'int val = 6;', 'int val = 6;', 1, 39, '2019-10-29 20:50:27', '2019-10-29 20:50:27'),
(147, 'intger val -> 6;', 'intger val -> 6;', 0, 39, '2019-10-29 20:50:51', '2019-10-29 20:50:51'),
(148, 'digitalWrite(8, HIGH);', 'digitalWrite(8, HIGH);', 1, 40, '2019-10-29 21:03:38', '2019-10-29 21:03:38'),
(149, 'digitalwrite(8, HIGH);', 'digitalwrite(8, HIGH);', 0, 40, '2019-10-29 21:03:48', '2019-10-29 21:03:48'),
(150, 'digitalWrite(8; HIGH),', 'digitalWrite(8; HIGH),', 0, 40, '2019-10-29 21:04:05', '2019-10-29 21:04:05'),
(151, 'digitalWrite(8, 1)', 'digitalWrite(8, 1)', 0, 40, '2019-10-29 21:04:21', '2019-10-29 21:04:21'),
(152, 'digitalWrite();', 'digitalWrite();', 0, 42, '2019-10-29 21:27:47', '2019-10-29 21:27:47'),
(153, 'digitalRead();', 'digitalRead();', 1, 42, '2019-10-29 21:28:02', '2019-10-29 21:28:02'),
(154, 'analogRead();', 'analogRead();', 0, 42, '2019-10-29 21:36:41', '2019-10-29 21:36:41'),
(155, 'analogWrite();', 'analogWrite();', 0, 42, '2019-10-29 21:36:51', '2019-10-29 21:36:51'),
(156, 'A', 'A', 1, 43, '2020-03-26 22:47:11', '2020-03-26 22:47:11'),
(159, 'A', 'A', 0, 44, '2020-03-26 23:39:56', '2020-03-26 23:39:56'),
(160, 'B', 'B', 1, 44, '2020-03-26 23:40:07', '2020-03-26 23:40:07'),
(161, 'Lorem Ipsum is simply dummy', 'Lorem Ipsum is simply dummy', 1, 45, '2020-04-15 00:28:36', '2020-04-16 23:57:01'),
(162, 'Lorem Ipsum is simply dummy', 'Lorem Ipsum is simply dummy', 0, 45, '2020-04-15 00:29:00', '2020-04-16 23:57:07'),
(163, 'Lorem Ipsum', 'Lorem Ipsum', 1, 46, '2020-04-15 00:30:24', '2020-04-16 23:55:52'),
(164, 'Lorem Ipsum', 'Lorem Ipsum', 0, 46, '2020-04-15 00:30:36', '2020-04-16 23:56:02'),
(165, 'Lorem Ipsum', 'Lorem Ipsum', 1, 47, '2020-04-15 01:58:19', '2020-04-16 23:54:48'),
(166, 'Lorem Ipsum', 'Lorem Ipsum', 0, 47, '2020-04-15 01:58:28', '2020-04-16 23:54:57'),
(167, 'yes', 'yes_Ar', 1, 48, '2020-04-15 02:09:07', '2020-04-15 02:09:07'),
(168, 'no', 'no_ar', 0, 48, '2020-04-15 02:09:26', '2020-04-15 02:09:26'),
(169, 'yes', 'yes_Ar', 1, 49, '2020-04-15 02:15:58', '2020-04-15 02:15:58'),
(170, 'no', 'no_ar', 0, 49, '2020-04-15 02:16:08', '2020-04-15 02:16:08'),
(171, 'yes', 'y', 0, 60, '2020-04-15 03:42:53', '2020-04-15 03:42:53'),
(172, 'no', 'n', 1, 60, '2020-04-15 03:43:24', '2020-04-15 03:43:24'),
(173, 'yes', 'y', 0, 61, '2020-04-15 03:44:23', '2020-04-15 03:44:23'),
(174, 'no', 'n', 1, 61, '2020-04-15 03:44:30', '2020-04-15 03:44:30'),
(175, 'yes', 'y', 0, 62, '2020-04-15 03:46:14', '2020-04-15 03:46:14'),
(176, 'no', 'n', 1, 62, '2020-04-15 03:46:22', '2020-04-15 03:46:22'),
(177, 'yes', 'y', 0, 63, '2020-04-15 03:50:23', '2020-04-15 03:50:23'),
(178, 'no', 'n', 1, 63, '2020-04-15 03:50:33', '2020-04-15 03:50:33'),
(179, 'yes', 'y', 0, 64, '2020-04-15 03:51:22', '2020-04-15 03:51:22'),
(180, 'no', 'n', 1, 64, '2020-04-15 03:51:30', '2020-04-15 03:51:30'),
(181, 'Lorem Ipsum', 'Lorem Ipsum', 1, 65, '2020-04-15 23:08:35', '2020-04-16 05:26:43'),
(182, 'Lorem Ipsum', 'Lorem Ipsum', 0, 65, '2020-04-15 23:08:43', '2020-04-16 05:23:46'),
(183, 'Lorem Ipsum', 'Lorem Ipsum', 1, 59, '2020-04-16 00:07:26', '2020-04-16 23:50:18'),
(184, 'Lorem Ipsum', 'Lorem Ipsum', 0, 59, '2020-04-16 00:07:35', '2020-04-16 23:50:13'),
(185, 'Lorem Ipsum', 'Lorem Ipsum', 1, 58, '2020-04-16 00:08:03', '2020-04-16 23:53:48'),
(186, 'Lorem Ipsum', 'Lorem Ipsum', 0, 58, '2020-04-16 00:08:13', '2020-04-16 23:53:56'),
(187, 'Lorem is simply', 'Lorem is simply', 0, 65, '2020-04-16 05:02:58', '2020-04-16 05:24:08'),
(188, 'simply', 'simply', 0, 65, '2020-04-16 05:03:08', '2020-04-16 05:26:55'),
(189, 'yes', 'yes', 1, 66, '2020-04-16 05:12:03', '2020-04-16 05:12:03'),
(190, 'no', 'no', 0, 66, '2020-04-16 05:12:09', '2020-04-16 05:12:09'),
(191, 'yes', 'yes', 1, 67, '2020-04-16 05:12:32', '2020-04-16 05:12:32'),
(192, 'no', 'no', 0, 67, '2020-04-16 05:12:38', '2020-04-16 05:12:38'),
(193, 'yes', 'yes', 1, 68, '2020-04-16 05:13:00', '2020-04-16 05:13:00'),
(194, 'no', 'no', 0, 68, '2020-04-16 05:13:08', '2020-04-16 05:13:08'),
(195, 'yes', 'yes', 1, 69, '2020-04-16 05:13:33', '2020-04-16 05:13:33'),
(196, 'no', 'ni', 0, 69, '2020-04-16 05:13:41', '2020-04-16 05:13:41'),
(197, 'yes', 'yes', 1, 70, '2020-04-16 05:14:09', '2020-04-16 05:14:09'),
(198, 'no', 'no', 0, 70, '2020-04-16 05:14:16', '2020-04-16 05:14:16'),
(199, 'yes', 'yes', 1, 71, '2020-04-16 08:12:20', '2020-04-16 08:12:20'),
(200, 'no', 'no', 0, 71, '2020-04-16 08:12:27', '2020-04-16 08:12:27'),
(201, 'yes', 'yes', 1, 72, '2020-04-16 08:12:50', '2020-04-16 08:12:54'),
(202, 'no', 'no', 0, 72, '2020-04-16 08:13:01', '2020-04-16 08:13:01'),
(203, 'yes', 'yes', 1, 73, '2020-04-16 08:13:28', '2020-04-16 08:13:28'),
(204, 'no', 'no', 0, 73, '2020-04-16 08:13:36', '2020-04-16 08:13:36'),
(205, 'yes', 'yes', 1, 74, '2020-04-16 08:14:55', '2020-04-16 08:14:55'),
(206, 'no', 'no', 0, 74, '2020-04-16 08:15:03', '2020-04-16 08:15:03'),
(207, 'yes', 'yes', 1, 75, '2020-04-16 08:18:16', '2020-04-16 08:18:16'),
(208, 'no', 'no', 0, 75, '2020-04-16 08:18:29', '2020-04-16 08:18:29'),
(209, 'Lorem Ipsum', 'Lorem Ipsum', 0, 59, '2020-04-16 23:50:26', '2020-04-16 23:50:26'),
(210, 'Lorem Ipsum', 'Lorem Ipsum', 0, 59, '2020-04-16 23:50:34', '2020-04-16 23:50:34'),
(211, 'Lorem Ipsum', 'Lorem Ipsum', 0, 58, '2020-04-16 23:54:06', '2020-04-16 23:54:06'),
(212, 'Lorem Ipsum', 'Lorem Ipsum', 0, 58, '2020-04-16 23:54:13', '2020-04-16 23:54:13'),
(213, 'Lorem Ipsum', 'Lorem Ipsum', 0, 47, '2020-04-16 23:55:03', '2020-04-16 23:55:03'),
(214, 'Lorem Ipsum', 'Lorem Ipsum', 0, 47, '2020-04-16 23:55:11', '2020-04-16 23:55:11'),
(215, 'Lorem Ipsum', 'Lorem Ipsum', 0, 46, '2020-04-16 23:56:13', '2020-04-16 23:56:13'),
(216, 'Lorem Ipsum', 'Lorem Ipsum', 0, 46, '2020-04-16 23:56:23', '2020-04-16 23:56:23'),
(217, 'Lorem Ipsum is simply dummy', 'Lorem Ipsum is simply dummy', 0, 45, '2020-04-16 23:57:16', '2020-04-16 23:57:16'),
(218, 'Lorem Ipsum is simply dummy', 'Lorem Ipsum is simply dummy', 0, 45, '2020-04-16 23:57:26', '2020-04-16 23:57:26'),
(219, 'lorem ipsum', 'lorem ipsum', 1, 76, '2020-04-17 01:10:03', '2020-04-17 01:10:03'),
(220, 'lorem ipsum', 'lorem ipsum', 0, 76, '2020-04-17 01:10:11', '2020-04-17 01:10:11'),
(221, 'lorem ipsum', 'lorem ipsum', 0, 76, '2020-04-17 01:10:18', '2020-04-17 01:10:18'),
(222, 'lorem ipsum', 'lorem ipsum', 0, 76, '2020-04-17 01:10:25', '2020-04-17 01:10:25'),
(223, 'lorem ipsum', 'lorem ipsum', 0, 33, '2020-04-17 01:12:21', '2020-04-17 01:12:21'),
(224, 'India', 'fdsfsd', 0, 32, '2020-04-17 01:13:02', '2020-04-17 01:13:02'),
(225, 'lorem ipsum', 'lorem', 1, 77, '2020-04-17 01:23:26', '2020-04-17 01:23:26'),
(226, 'lorem ipsum', 'lorem', 0, 77, '2020-04-17 01:23:47', '2020-04-17 01:23:47'),
(227, 'Lorem Ipsum', 'Lorem Ipsum', 1, 78, '2020-04-17 01:24:14', '2020-04-17 01:24:14'),
(228, 'Lorem Ipsum', 'Lorem Ipsum', 0, 78, '2020-04-17 01:24:20', '2020-04-17 01:24:20'),
(229, 'Lorem Ipsum', 'Lorem Ipsum', 0, 78, '2020-04-17 01:24:27', '2020-04-17 01:24:27'),
(230, 'yes', 'yes', 1, 79, '2020-04-17 02:25:28', '2020-04-17 02:25:28'),
(231, 'no', 'no', 0, 79, '2020-04-17 02:25:35', '2020-04-17 02:25:35'),
(232, 'yes', 'yes', 1, 80, '2020-04-17 02:25:55', '2020-04-17 02:25:55'),
(233, 'no', 'no', 0, 80, '2020-04-17 02:26:03', '2020-04-17 02:26:03'),
(234, 'yes', 'yes', 1, 81, '2020-04-17 02:26:31', '2020-04-17 02:26:31'),
(235, 'no', 'no', 0, 81, '2020-04-17 02:26:40', '2020-04-17 02:26:40'),
(236, 'yes', 'yes', 1, 82, '2020-04-17 02:27:04', '2020-04-17 02:27:04'),
(237, 'no', 'no', 0, 82, '2020-04-17 02:27:12', '2020-04-17 02:27:12'),
(238, 'yes', 'yes', 1, 83, '2020-04-17 02:30:21', '2020-04-17 02:30:21'),
(239, 'no', 'no', 0, 83, '2020-04-17 02:30:31', '2020-04-17 02:30:31'),
(240, 'Lorem Ipsum', 'Lorem Ipsum', 1, 88, '2020-04-17 02:39:30', '2020-04-17 02:39:30'),
(241, 'Lorem Ipsum', 'Lorem Ipsum', 0, 88, '2020-04-17 02:39:38', '2020-04-17 02:39:38'),
(242, 'Lorem Ipsum', 'Lorem Ipsum', 1, 87, '2020-04-17 02:39:51', '2020-04-17 02:39:51'),
(243, 'Lorem Ipsum', 'Lorem Ipsum', 0, 87, '2020-04-17 02:39:59', '2020-04-17 02:39:59'),
(244, 'Lorem Ipsum', 'Lorem Ipsum', 1, 86, '2020-04-17 02:40:16', '2020-04-17 02:40:16'),
(245, 'Lorem Ipsum', 'Lorem Ipsum', 0, 86, '2020-04-17 02:40:26', '2020-04-17 02:40:26'),
(246, 'Lorem Ipsum', 'Lorem Ipsum', 1, 85, '2020-04-17 02:40:51', '2020-04-17 02:40:51'),
(247, 'Lorem Ipsum', 'Lorem Ipsum', 0, 85, '2020-04-17 02:41:10', '2020-04-17 02:41:10'),
(248, 'Lorem Ipsum', 'Lorem Ipsum', 1, 84, '2020-04-17 02:41:30', '2020-04-17 02:41:30'),
(249, 'Lorem Ipsum', 'Lorem Ipsum', 0, 84, '2020-04-17 02:41:38', '2020-04-17 02:41:38'),
(250, 'yes', 'yes', 1, 89, '2020-05-11 04:21:33', '2020-05-11 04:21:33'),
(251, 'no', 'no', 0, 89, '2020-05-11 04:21:41', '2020-05-11 04:21:41'),
(252, 'yes', 'yes', 1, 90, '2020-05-11 04:22:17', '2020-05-11 04:22:17'),
(253, 'no', 'no', 0, 90, '2020-05-11 04:22:23', '2020-05-11 04:22:23'),
(254, 'yes', 'yes', 1, 91, '2020-05-11 04:22:45', '2020-05-11 04:22:45'),
(255, 'no', 'no', 0, 91, '2020-05-11 04:22:52', '2020-05-11 04:22:52'),
(256, 'yes', 'yes', 1, 92, '2020-05-11 04:23:20', '2020-05-11 04:23:20'),
(257, 'no', 'no', 0, 92, '2020-05-11 04:23:27', '2020-05-11 04:23:27'),
(258, 'yes', 'yes', 1, 93, '2020-05-11 04:23:55', '2020-05-11 04:23:55'),
(259, 'no', 'no', 0, 93, '2020-05-11 04:24:01', '2020-05-11 04:24:01'),
(260, 'yes1', 'yes1', 1, 96, '2020-05-27 02:26:21', '2020-05-27 02:32:02'),
(261, 'no1', 'no1', 0, 96, '2020-05-27 02:26:39', '2020-05-27 02:32:05'),
(262, 'yes1', 'yes1', 1, 98, '2020-05-27 03:34:53', '2020-05-27 03:34:53'),
(263, 'no1', 'no1', 0, 98, '2020-05-27 03:35:03', '2020-05-27 03:35:03'),
(264, 'yes1', 'yes', 1, 97, '2020-05-27 03:35:36', '2020-05-27 03:35:36'),
(265, 'no', 'no', 0, 97, '2020-05-27 03:35:43', '2020-05-27 03:35:43'),
(266, 'yes', 'yes', 1, 99, '2020-05-27 03:36:05', '2020-05-27 03:36:05'),
(267, 'no', 'no', 1, 99, '2020-05-27 03:36:15', '2020-05-27 03:36:15'),
(268, 'yes', 'yes', 1, 100, '2020-05-27 03:37:28', '2020-05-27 03:37:28'),
(269, 'no', 'no', 0, 100, '2020-05-27 03:37:34', '2020-05-27 03:37:34'),
(270, 'yes', 'yes', 1, 101, '2020-05-27 03:38:09', '2020-05-27 03:38:09'),
(271, 'no', 'no', 0, 101, '2020-05-27 03:38:15', '2020-05-27 03:38:15');

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `payment_method_ids` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `subscription_model_ids` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `discount_type_id` int(11) DEFAULT '1',
  `amount` double(12,3) DEFAULT '0.000',
  `user_id` int(11) DEFAULT '0',
  `course_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `image_path_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image_path_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `popup` tinyint(1) DEFAULT '0',
  `qty` int(11) DEFAULT '1',
  `start_at` timestamp NULL DEFAULT NULL,
  `end_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`id`, `code`, `payment_method_ids`, `subscription_model_ids`, `discount_type_id`, `amount`, `user_id`, `course_id`, `group_id`, `image_path_ar`, `image_path_en`, `popup`, `qty`, `start_at`, `end_at`, `created_at`, `updated_at`) VALUES
(2, 'ZXCASDQ', '', '', 1, 123.000, 1, NULL, NULL, '', '', 0, 123, NULL, NULL, '2018-06-13 10:37:36', '2018-06-13 10:37:36'),
(3, 'DEo13WEWS', '', '', 1, 190.000, 1, NULL, NULL, '', '', 0, 142, NULL, NULL, '2018-06-13 10:37:58', '2018-06-13 10:37:58'),
(7, 'qweYBPITNI', '', '', 1, 1234.000, 0, 6, NULL, '', '', 0, 12, NULL, NULL, '2018-09-24 10:57:26', '2018-09-24 11:20:35'),
(8, 'GRP--939IUHOYPW', '', '', 1, 120.000, 2, 6, NULL, '', '', 0, 1, NULL, NULL, '2018-09-26 06:26:24', '2018-09-26 06:29:15'),
(9, 'test-D9RDUA8WVM6Z2FZ', '', '', 1, 20.000, 1, 25, NULL, '', '', 0, 1, NULL, NULL, '2019-09-29 20:23:59', '2020-03-25 04:42:34'),
(10, 'SIDD', '', '', 1, 10.000, 21, 0, NULL, '', '', 1, -4, NULL, NULL, '2020-05-14 04:32:37', '2020-05-25 03:54:46'),
(11, 'BHARAT', '', '', 1, 10.000, 11, 0, NULL, '', '', 0, 8, NULL, NULL, '2020-05-14 04:32:37', '2020-06-09 23:30:46');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(10) UNSIGNED NOT NULL,
  `level_id` int(11) DEFAULT '0',
  `lang_id` int(11) DEFAULT '0',
  `title_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `overview_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `title_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `overview_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `price` double(12,3) DEFAULT '0.000',
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT '0',
  `seo_meta_title_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `seo_meta_title_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `description_ar` text COLLATE utf8mb4_unicode_ci,
  `description_en` text COLLATE utf8mb4_unicode_ci,
  `text_en` text COLLATE utf8mb4_unicode_ci,
  `text_ar` text COLLATE utf8mb4_unicode_ci,
  `url_identifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `is_youtube` tinyint(1) NOT NULL DEFAULT '0',
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `wide_image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kit_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `course_estimated_time` double(12,3) DEFAULT '0.000',
  `intro_video_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `emonth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `level_id`, `lang_id`, `title_ar`, `overview_ar`, `title_en`, `overview_en`, `price`, `slug`, `user_id`, `seo_meta_title_ar`, `seo_meta_title_en`, `description_ar`, `description_en`, `text_en`, `text_ar`, `url_identifier`, `is_youtube`, `image_path`, `wide_image_path`, `kit_id`, `course_estimated_time`, `intro_video_path`, `active`, `created_at`, `updated_at`, `created_by`, `updated_by`, `emonth`) VALUES
(6, 8, 1, 'روبوتيكس', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non, vestibulum orci. Lorem ipsum dolor sit amet', 'Robotics Level One', 'Introduction to Robotics for beginners', 123.000, 'robotics', 2, '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non, vestibulum orci. Lorem ipsum dolor sit amet', 'This level aims to teach participants how to design and program robots using the educational robots “NXT”. They learn and apply the basic concepts of robots design and programming, including STEM (Science, Technology, Engineering, and Math) well- known and applied worldwide. This level prepares the students to participate in local and international competitions.', '<div class=\"you-will-learn\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<h4>you will learn:</h4>\r\n\r\n<ul class=\"list-unstyled\">\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>', '<div class=\"you-will-learn\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<h4>سوف تتعلم:</h4>\r\n\r\n<ul class=\"list-unstyled\">\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>', '', 0, '/uploads/robotics.png', '/uploads/course-banner.jpg', '4', 123.000, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/OOfXhz4In_w\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 1, '2018-06-10 06:18:30', '2020-04-21 04:03:47', NULL, 1, NULL),
(7, 6, 1, 'شي تايتل', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non, vestibulum orci. Lorem ipsum dolor sit amet', 'Electrical Engineering', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non, vestibulum orci. Lorem ipsum dolor sit amet', 12.000, 'electrical-engineering', 2, '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non, vestibulum orci. Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non, vestibulum orci. Lorem ipsum dolor sit amet', '<div class=\"you-will-learn\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n<h4>you will learn:</h4>\n\n<ul class=\"list-unstyled\">\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n</ul>\n</div>\n</div>\n</div>', '<div class=\"you-will-learn\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n<h4>سوف تتعلم:</h4>\n\n<ul class=\"list-unstyled\">\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n</ul>\n</div>\n</div>\n</div>', '', 0, '/images/levels/electrical_engineering.png', '/uploads/course-banner.jpg', '', 30.000, NULL, 0, '2018-06-18 05:32:41', '2018-08-13 10:06:17', NULL, NULL, NULL),
(16, 6, 1, 'تطبيقات الموبايل', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non, vestibulum orci. Lorem ipsum dolor sit amet', 'Mobile Application', 'English Overview\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur,', 20.000, 'mobile-application', 2, '', '', 'الديسكربشن الانكليزي مترجم', 'Description\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non,', '<div class=\"you-will-learn\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n<h4>you will learn:</h4>\n\n<ul class=\"list-unstyled\">\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n</ul>\n</div>\n</div>\n</div>', '<div class=\"you-will-learn\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n<h4>سوف تتعلم:</h4>\n\n<ul class=\"list-unstyled\">\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n</ul>\n</div>\n</div>\n</div>', '', 0, '/uploads/mobile_applications@2x.png', '/uploads/course-banner.jpg', '', 20.000, NULL, 0, '2018-06-24 05:56:53', '2018-08-13 10:06:01', NULL, NULL, NULL),
(17, 6, 1, 'تطوير الالعاب', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non, vestibulum orci. Lorem ipsum dolor sit amet', 'Games Development', 'English Overview\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur,', 25.000, 'games-development', 1, '', '', 'الديكربشن اللي بالانكليزي بس مترجم', 'English Description\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non, vestibulum u', '<div class=\"you-will-learn\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<h4>you will learn:</h4>\r\n\r\n<ul class=\"list-unstyled\">\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>', '<div class=\"you-will-learn\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<h4>سوف تتعلم:</h4>\r\n\r\n<ul class=\"list-unstyled\">\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>', '', 0, '/uploads/games_development@2x.png', '/uploads/course-banner.jpg', '10', 20.000, NULL, 1, '2018-06-24 06:06:56', '2020-05-11 03:16:38', NULL, 1, NULL),
(18, 8, 1, 'روبوتكس', 'الروبوتية المستوى الاول', 'Robotics Level Two', 'Robotics for beginners', 80.000, 'robotics-2', 0, '', '', 'ديسكريبشن', 'This level aims to teach participants how to design and program robots using the educational robots “NXT”. They learn and apply the basic concepts of robots design and programming, including STEM (Science, Technology, Engineering, and Math) well- known and applied worldwide. This level prepares the students to participate in local and international competitions.', '<div class=\"you-will-learn\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n<h4>you will learn:</h4>\n\n<ul class=\"list-unstyled\">\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n</ul>\n</div>\n</div>\n</div>', '<div class=\"you-will-learn\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n<h4>سوف تتعلم:</h4>\n\n<ul class=\"list-unstyled\">\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n</ul>\n</div>\n</div>\n</div>', '', 0, '/uploads/Robotics2.jpg', '/uploads/course-banner.jpg', '4', 12.000, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/OOfXhz4In_w\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 1, '2018-11-05 15:27:45', '2018-11-05 15:28:25', 1, 1, NULL),
(19, 8, 1, 'روبتيكس', 'اوفر فيو', 'Robotics Level Three', 'Robotics for Intermediate', 15.000, 'robotics-3', 0, '', '', 'ديسكربشن', 'Some Description', '<div class=\"you-will-learn\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n<h4>you will learn:</h4>\n\n<ul class=\"list-unstyled\">\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n</ul>\n</div>\n</div>\n</div>', '<div class=\"you-will-learn\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n<h4>سوف تتعلم:</h4>\n\n<ul class=\"list-unstyled\">\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n</ul>\n</div>\n</div>\n</div>', '', 0, '/uploads/robot-expo-1024x500.jpg', '/uploads/course-banner.jpg', '4', 12.000, NULL, 1, '2018-11-05 16:42:59', '2018-11-05 16:42:59', 1, NULL, NULL),
(21, 8, 1, 'روبتيكس', 'اوفر فيو', 'Robotics Level Four', 'Robotics for Intermediate', 15.000, 'robotics-4', 0, '', '', 'ديسكربشن', 'Some Description', '<div class=\"you-will-learn\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n<h4>you will learn:</h4>\n\n<ul class=\"list-unstyled\">\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n</ul>\n</div>\n</div>\n</div>', '<div class=\"you-will-learn\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n<h4>سوف تتعلم:</h4>\n\n<ul class=\"list-unstyled\">\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n</ul>\n</div>\n</div>\n</div>', '', 0, '/uploads/robots.jpg', '/uploads/course-banner.jpg', '4', 12.000, NULL, 1, '2018-11-05 16:44:32', '2018-11-05 16:44:32', 1, NULL, NULL),
(22, 8, 1, 'روبتيكس', 'اوفر فيو', 'Robotics Level Five', 'Robotics for Intermediate', 15.000, 'robotics-5', 1, '', '', 'ديسكربشن', 'Some Description', '<div class=\"you-will-learn\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<h4>you will learn:</h4>\r\n\r\n<ul class=\"list-unstyled\">\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>', '<div class=\"you-will-learn\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<h4>سوف تتعلم:</h4>\r\n\r\n<ul class=\"list-unstyled\">\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>', '', 0, '/uploads/webcast_2017-02-23-1500_intelligent-tax-automation-using-robotics-software.jpg', '/uploads/course-banner.jpg', '4', 12.000, NULL, 1, '2018-11-05 16:45:45', '2020-05-26 01:36:48', 1, 1, '3'),
(23, 8, 1, 'روبتيكس', 'اوفر فيو', 'Robotics Level Six', 'Robotics for Experts', 15.000, 'robotics-6', 1, '', '', 'ديسكربشن', 'Some Description', '<div class=\"you-will-learn\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<h4>you will learn:</h4>\r\n\r\n<ul class=\"list-unstyled\">\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>', '<div class=\"you-will-learn\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<h4>سوف تتعلم:</h4>\r\n\r\n<ul class=\"list-unstyled\">\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>', '', 0, '/uploads/Robotics-Company.jpg', '/uploads/course-banner.jpg', '4', 12.000, NULL, 1, '2018-11-05 16:47:32', '2020-05-26 01:48:31', 1, 1, '1'),
(24, 8, 1, 'روبتيكس', 'اوفر فيو', 'Robotics Level Seven', 'Robotics for Experts', 15.000, 'robotics-7', 0, '', '', 'ديسكربشن', 'Some Description', '<div class=\"you-will-learn\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n<h4>you will learn:</h4>\n\n<ul class=\"list-unstyled\">\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n</ul>\n</div>\n</div>\n</div>', '<div class=\"you-will-learn\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n<h4>سوف تتعلم:</h4>\n\n<ul class=\"list-unstyled\">\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n</ul>\n</div>\n</div>\n</div>', '', 0, '/uploads/AdobeStock_121058469-1-1024x821.jpeg', '/uploads/course-banner.jpg', '4', 12.000, NULL, 1, '2018-11-05 16:48:20', '2018-11-05 16:48:20', 1, NULL, NULL),
(25, 8, 1, 'روبتيكس', 'اوفر فيو', 'Robotics Level Eight', 'Robotics for Experts', 15.000, 'robotics-8', 0, '', '', 'ديسكربشن', 'Some Description', '<div class=\"you-will-learn\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n<h4>you will learn:</h4>\n\n<ul class=\"list-unstyled\">\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n</ul>\n</div>\n</div>\n</div>', '<div class=\"you-will-learn\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n<h4>سوف تتعلم:</h4>\n\n<ul class=\"list-unstyled\">\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n</ul>\n</div>\n</div>\n</div>', '', 0, '/uploads/course-1.jpg', '/uploads/course-banner.jpg', '4', 12.000, NULL, 1, '2018-11-05 16:49:41', '2020-04-01 05:09:58', 1, 1, NULL),
(26, 8, 1, 'روبتيكس', 'اوفر فيو', 'Robotics Level Nine', 'Robotics for Experts', 15.000, 'robotics-9', 0, '', '', 'ديسكربشن', 'Some Description', '<div class=\"you-will-learn\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n<h4>you will learn:</h4>\n\n<ul class=\"list-unstyled\">\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\n</ul>\n</div>\n</div>\n</div>', '<div class=\"you-will-learn\">\n<div class=\"row\">\n<div class=\"col-md-12\">\n<h4>سوف تتعلم:</h4>\n\n<ul class=\"list-unstyled\">\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\n</ul>\n</div>\n</div>\n</div>', '', 0, '/uploads/course-1.jpg', '/uploads/course-banner.jpg', '4', 12.000, NULL, 1, '2018-11-05 16:50:39', '2020-04-01 05:09:40', 1, 1, NULL),
(80, 8, 1, 'روبوتيكس متقدمة', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non, vestibulum orci. Lorem ipsum dolor sit amet', 'Advanced Robotics', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non, vestibulum orci. Lorem ipsum dolor sit amet', 7.000, 'advanced-robotics', 1, '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non, vestibulum orci. Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non, vestibulum orci. Lorem ipsum dolor sit amet', '<div class=\"you-will-learn\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<h4>you will learn:</h4>\r\n\r\n<ul class=\"list-unstyled\">\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>', '<div class=\"you-will-learn\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<h4>سوف تتعلم:</h4>\r\n\r\n<ul class=\"list-unstyled\">\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>', '', 0, '/uploads/course-1.jpg', '/uploads/course-banner.jpg', '10', 60.000, NULL, 1, '2018-06-18 05:52:13', '2020-04-27 06:35:41', NULL, 1, NULL),
(82, 7, 1, 'الإلكترونيات 101', NULL, 'Electronics 101', NULL, 20.000, 'electronics101', 1, '', '', NULL, NULL, '<div class=\"you-will-learn\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<h4>you will learn:</h4>\r\n\r\n<ul class=\"list-unstyled\">\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n	<li>With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>', '<div class=\"you-will-learn\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<h4>سوف تتعلم:</h4>\r\n\r\n<ul class=\"list-unstyled\">\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n	<li>مع برنامج حواري ناجح وكتب مبيعة ، نيل تايسون هو واحد من أكثر الشخصيات شعبية في العلوم الحديثة.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>', '4sBgu_tUpiI', 1, '/uploads/course-1.jpg', '/uploads/course-banner.jpg', '6', 1500.000, NULL, 1, '2019-10-26 10:12:51', '2020-06-05 07:24:54', 1, 1, '4'),
(83, 7, 1, 'test course arasd', 'dasd', 'test course endasdd', 'dsadasd', 20.000, 'test', 1, '', '', 'dsad', 'dsadsad', '<h1>This is a sample</h1>\r\n\r\n<p>testing in-line Styles</p>', '<h1>مثال</h1>\r\n\r\n<p>مثال</p>', 'j48LtUkZRjU', 1, '/uploads/course-list.jpg', '/uploads/course-banner.jpg', '4', 120.000, 'ddasdasdsad', 1, '2020-06-05 07:28:05', '2020-06-05 07:28:05', 1, NULL, '2');

-- --------------------------------------------------------

--
-- Table structure for table `course_category`
--

CREATE TABLE `course_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `course_category`
--

INSERT INTO `course_category` (`id`, `name_ar`, `name_en`, `created_at`, `updated_at`) VALUES
(2, 'zxc', 'zxc', '2018-06-10 11:08:21', '2018-06-10 11:08:21'),
(7, 'qwe', 'asd', '2018-06-10 11:44:39', '2018-06-10 11:44:47');

-- --------------------------------------------------------

--
-- Table structure for table `course_status`
--

CREATE TABLE `course_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `course_type`
--

CREATE TABLE `course_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `downloadable`
--

CREATE TABLE `downloadable` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `file_path_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `file_path_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `video_id` int(11) DEFAULT '0',
  `course_id` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `downloadable`
--

INSERT INTO `downloadable` (`id`, `name_ar`, `name_en`, `file_path_ar`, `file_path_en`, `video_id`, `course_id`, `created_at`, `updated_at`) VALUES
(10, NULL, NULL, '', 'http://dn734b9758kwp.cloudfront.net/files/15874620581.zip', 0, 6, '2020-04-21 04:10:59', '2020-04-21 04:10:59'),
(11, NULL, NULL, '', '', 0, 82, '2020-04-21 04:55:29', '2020-04-21 04:55:29'),
(12, NULL, NULL, '', '', 0, 80, '2020-04-27 06:35:41', '2020-04-27 06:35:41'),
(13, NULL, NULL, '', '', 0, 17, '2020-05-11 03:16:38', '2020-05-11 03:16:38'),
(14, NULL, NULL, '', '', 0, 22, '2020-05-26 01:36:48', '2020-05-26 01:36:48'),
(15, NULL, NULL, '', '', 0, 23, '2020-05-26 01:48:31', '2020-05-26 01:48:31'),
(16, NULL, NULL, '', '', 0, 83, '2020-06-05 07:28:05', '2020-06-05 07:28:05');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `subject`, `text`, `created_at`, `updated_at`) VALUES
(1, 'Moustafa Al-Lahham', 'iteng.moustafa@gmail.com', 'this is an awesome website', 'This is a message to test EmailService', '2018-09-05 05:22:59', '2018-09-05 05:22:59'),
(2, 'ketul patel', 'ketulpatel@webmynesystems.com', 'Contact', 'xc hvh zxvchxh vcxv', '2020-06-01 02:11:58', '2020-06-01 02:11:58'),
(3, 'ketul patel', 'ketulpatel@webmynesystems.com', 'Contact', 'vbcv vcvnvcbnvcbnzvnbcv', '2020-06-01 04:33:03', '2020-06-01 04:33:03'),
(4, 'ketul patel', 'ketulpatel@webmynesystems.com', 'Contact', 'hjdhcghchxbvncxbvncbx', '2020-06-01 04:39:16', '2020-06-01 04:39:16'),
(5, 'ketul patel', 'ketulpatel@webmynesystems.com', 'Contact', 'bdbs fdsb fsdbf bsdnfbndsbf', '2020-06-01 05:32:23', '2020-06-01 05:32:23'),
(6, 'ketul patel', 'ketulpatel@webmynesystems.com', 'Contact', 'ndjfndjsnf f hdsjnfjdsn fnsdnm,dnfm,dnsf', '2020-06-01 05:33:07', '2020-06-01 05:33:07'),
(7, 'ketul patel', 'ketulpatel@webmynesystems.com', 'Contact', 'vgh hvnvnb vbnvnb vvnbv', '2020-06-01 05:37:03', '2020-06-01 05:37:03'),
(8, 'hghg', 'sid@gmail.com', 'Contact', 'fhgfghf', '2020-06-10 01:21:11', '2020-06-10 01:21:11'),
(9, 'dsdsd', 'sid@gmail.com', 'Contact', 'dsd', '2020-06-10 01:37:04', '2020-06-10 01:37:04'),
(10, 'sa', 'sid@gmail.com', 'Contact', 'sa', '2020-06-10 01:59:30', '2020-06-10 01:59:30'),
(11, 'sasa', 'sid@gmail.com', 'Contact', 'Sas', '2020-06-10 03:50:04', '2020-06-10 03:50:04'),
(12, 'asas', 'sid@gmail.com', 'Contact', 'xsas', '2020-06-10 03:50:47', '2020-06-10 03:50:47');

-- --------------------------------------------------------

--
-- Table structure for table `email_broadcast`
--

CREATE TABLE `email_broadcast` (
  `id` int(10) UNSIGNED NOT NULL,
  `from` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `template` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_broadcast`
--

INSERT INTO `email_broadcast` (`id`, `from`, `subject`, `template`, `created_at`, `updated_at`) VALUES
(1, 'email@email.com', 'Subject', 'some-template', '2019-05-03 17:05:25', '2019-05-03 17:05:25'),
(2, 'ketulpatel@webmynesystems.com', 'testing by test', 'some-template', '2019-09-23 20:05:42', '2020-03-27 00:22:53');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `email_temp_id` int(11) UNSIGNED NOT NULL,
  `email_temp_receiver` char(1) DEFAULT NULL,
  `email_temp_name` varchar(100) DEFAULT NULL,
  `email_temp_desc` varchar(150) DEFAULT NULL,
  `email_temp_subject` varchar(100) DEFAULT NULL,
  `email_temp_body` text,
  `email_temp_status` char(1) DEFAULT NULL,
  `email_temp_type` char(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `sms_notification_temp_body` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`email_temp_id`, `email_temp_receiver`, `email_temp_name`, `email_temp_desc`, `email_temp_subject`, `email_temp_body`, `email_temp_status`, `email_temp_type`, `created_at`, `updated_at`, `sms_notification_temp_body`) VALUES
(1, 'A', 'Email Template sample 1(Html)', 'Email Template sample1', 'Email Template sample1', '<DATE>,\r\n<SITENAME> Admin,\r\n  The new user has signed up on the site whose personal details are as follows :\r\n\r\nPersonal Information :\r\n\r\nUsername : <EMAIL>\r\nPassword : <PASSWORD>\r\nName : <NAME>\r\nAddress : <ADDRESS>\r\nCity : <CITY>\r\nState : <STATE>\r\nCountry : <COUNTRY>\r\nZipcode : <ZIPCODE>\r\nPhone : <PHONE>\r\n\r\n\r\nThanking You,\r\n<SITENAME>', 'A', 'H', NULL, '2020-05-18 09:13:09', 'SMS <SITENAME>'),
(3, NULL, 'Create Order Email', 'Create Order Email', 'Create Order Email', 'Dear,\nYour Order successfully generated :\n\n<ORDERDETAIL>\n\nThanking You\nEureka', NULL, NULL, '2020-05-18 09:46:52', '2020-05-19 04:50:24', NULL),
(4, NULL, 'Reset Password', 'Reset Password', 'Reset Password', 'Hello!\r\nYou are receiving this email because we received a password reset request for your account.\r\n<RESETPASSWORDLINK>\r\n\r\nIf you did not request a password reset, no further action is required.\r\nRegards,\r\nEureka', NULL, NULL, '2020-05-22 13:27:50', '2020-06-04 05:35:23', NULL),
(5, NULL, 'Contact Us', 'Contact Us', 'Contact Us', 'Dear Admin,\r\n\r\nContact Detail:\r\n\r\nName :       <FULLNAME>\r\nEmail :        <EMAIL>\r\nMessage:   <MESSAGE>\r\n\r\nThanking You\r\nEureka', NULL, NULL, '2020-06-01 09:51:50', '2020-06-01 10:04:11', NULL),
(6, NULL, 'Sign Up For User', 'Sign Up For User', 'Sign Up', 'Dear <USER>,\r\n\r\nThank You For Register.\r\n\r\nThanking You\r\nEureka', NULL, NULL, '2020-06-03 05:13:20', '2020-06-03 05:13:20', NULL),
(7, NULL, 'Sign Up For Admin', 'Sign Up For Admin', 'Sign Up', 'Dear Admin,\r\n\r\nUser Sign Up Details:\r\n\r\nFull Name : <FULLNAME>\r\nEmail : <EMAIL>\r\nAge : <AGE>\r\nPhone No: <PHONE>\r\n\r\nThanking You\r\nEureka', NULL, NULL, '2020-06-03 05:14:15', '2020-06-03 07:22:39', NULL),
(8, NULL, 'Order Confirmed', 'Order Confirmed', 'Order Confirmed', '<DATE>,\r\nYour Order is Confirmed Order Id : #<ORDER>\r\nTransaction Id: <TRANSACTION>\r\n\r\n\r\nThanking You,\r\nEureka', 'A', 'H', NULL, '2020-06-03 07:18:05', 'SMS <SITENAME>'),
(9, NULL, 'Course subscription started', 'Course subscription started', 'Course subscription started', 'Course : <NAME> \r\nYour Course subscription is started : <DATE>\r\n\r\nThanking You,\r\nEureka', 'A', 'H', NULL, '2020-06-04 05:23:53', 'SMS <SITENAME>'),
(10, NULL, 'Course subscription Ended', 'Course subscription Ended', 'Course subscription Ended', 'Course : <NAME> \r\nYour Course subscription is ended: <DATE>\r\n\r\nThanking You,\r\nEureka', 'A', 'H', NULL, '2020-06-03 12:12:43', 'SMS <SITENAME>');

-- --------------------------------------------------------

--
-- Table structure for table `eurekian_stars_text`
--

CREATE TABLE `eurekian_stars_text` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_ar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `eurekian_stars_text`
--

INSERT INTO `eurekian_stars_text` (`id`, `text`, `text_ar`, `created_at`, `updated_at`) VALUES
(1, '<p>Eurekian Stars are the best Innovators of the week, We consider them depending on the progress they made during the week.<br>What are you waiting for ?! Go ahead and be an Eurekian Star.</p>', '<p>Lorem Ipsum This is a long Paragraph <i>with نص عربي</i></p>', '2018-07-29 09:26:16', '2020-03-25 22:51:18');

-- --------------------------------------------------------

--
-- Table structure for table `footer`
--

CREATE TABLE `footer` (
  `id` int(10) UNSIGNED NOT NULL,
  `number1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `margin` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `currency_symbol` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_conversation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_position` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `footer`
--

INSERT INTO `footer` (`id`, `number1`, `number2`, `email`, `facebook`, `twitter`, `instagram`, `youtube`, `margin`, `created_at`, `updated_at`, `currency_symbol`, `currency_conversation`, `currency_position`) VALUES
(1, '+962799638583', '+96265530086', 'info@eurekatechacademy.com', 'https://www.facebook.com', 'https://www.twitter.com', 'https://www.instagram.com', 'https://www.youtube.com', 15, '2018-08-07 10:03:25', '2020-06-01 06:39:51', 'JOD', '0.00696', 'after');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `overview_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `overview_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_src` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_embed` text COLLATE utf8mb4_unicode_ci,
  `type` smallint(6) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `title_en`, `title_ar`, `overview_en`, `overview_ar`, `img_src`, `youtube_embed`, `type`, `created_at`, `updated_at`, `album_id`) VALUES
(3, 'item1', 'item1', 'item1', 'item1', 'http://dn734b9758kwp.cloudfront.net/images/1585809745chapter-img.jpg', NULL, 1, '2020-04-02 01:12:25', '2020-04-02 01:12:28', 1),
(4, 'item2', 'item2', 'item2', 'item2', 'http://dn734b9758kwp.cloudfront.net/images/1585809745chapter-img.jpg', NULL, 1, '2020-04-02 01:12:25', '2020-04-02 01:12:28', 1),
(5, 'item3', 'item3', 'item3', 'item3', 'http://dn734b9758kwp.cloudfront.net/images/1585809745chapter-img.jpg', NULL, 1, '2020-04-02 01:12:25', '2020-04-02 01:12:28', 1),
(6, 'item4', 'item4', 'item4', 'item4', 'http://dn734b9758kwp.cloudfront.net/images/1585809745chapter-img.jpg', NULL, 1, '2020-04-02 01:12:25', '2020-04-02 01:12:28', 1),
(7, 'item1', 'item1', 'item1', 'item1', NULL, 'PFFoCx2WINY', 2, '2020-04-02 01:12:25', '2020-05-21 05:29:52', 3),
(8, 'item2', 'item2', 'item2', 'item2', 'http://dn734b9758kwp.cloudfront.net/images/1585809745chapter-img.jpg', NULL, 1, '2020-04-02 01:12:25', '2020-04-02 01:12:28', 3),
(9, 'item3', 'item3', 'item3', 'item3', 'http://dn734b9758kwp.cloudfront.net/images/1585809745chapter-img.jpg', NULL, 1, '2020-04-02 01:12:25', '2020-04-02 01:12:28', 3),
(10, 'item4', 'item4', 'item4', 'item4', 'http://dn734b9758kwp.cloudfront.net/images/1585809745chapter-img.jpg', NULL, 1, '2020-04-02 01:12:25', '2020-04-02 01:12:28', 3);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(5, 'Group6', '2018-09-22 08:06:39', '2018-09-22 08:06:39', NULL, NULL),
(6, 'Excel Group', '2018-09-23 09:15:02', '2018-09-23 09:15:02', NULL, NULL),
(7, 'qweqwe', '2018-09-24 06:01:59', '2018-09-24 06:01:59', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups_users`
--

CREATE TABLE `groups_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups_users`
--

INSERT INTO `groups_users` (`id`, `user_id`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 1, 5, '2018-09-26 09:24:29', '2018-09-26 09:24:29'),
(2, 7, 7, '2018-09-26 08:35:42', '2018-09-26 08:35:42'),
(3, 7, 7, '2018-09-26 08:37:07', '2018-09-26 08:37:07'),
(4, 7, 7, '2018-09-26 08:38:00', '2018-09-26 08:38:00'),
(6, 9, 7, '2018-09-26 08:38:01', '2018-09-26 08:38:01'),
(8, 7, 7, '2018-09-26 08:38:43', '2018-09-26 08:38:43'),
(10, 9, 7, '2018-09-26 08:38:44', '2018-09-26 08:38:44'),
(12, 7, 7, '2018-09-26 08:40:40', '2018-09-26 08:40:40'),
(13, 8, 7, '2018-09-26 08:40:40', '2018-09-26 08:40:40'),
(14, 9, 7, '2018-09-26 08:40:40', '2018-09-26 08:40:40'),
(16, 7, 7, '2018-09-26 08:41:26', '2018-09-26 08:41:26'),
(17, 8, 7, '2018-09-26 08:41:26', '2018-09-26 08:41:26'),
(18, 9, 7, '2018-09-26 08:41:26', '2018-09-26 08:41:26'),
(19, 10, 7, '2018-09-26 08:41:26', '2018-09-26 08:41:26'),
(20, 7, 7, '2018-09-26 08:43:26', '2018-09-26 08:43:26'),
(21, 8, 7, '2018-09-26 08:43:26', '2018-09-26 08:43:26'),
(22, 9, 7, '2018-09-26 08:43:26', '2018-09-26 08:43:26'),
(23, 10, 7, '2018-09-26 08:43:27', '2018-09-26 08:43:27'),
(24, 7, 7, '2018-09-26 08:43:53', '2018-09-26 08:43:53'),
(25, 8, 7, '2018-09-26 08:43:53', '2018-09-26 08:43:53'),
(26, 9, 7, '2018-09-26 08:43:53', '2018-09-26 08:43:53'),
(27, 10, 7, '2018-09-26 08:43:54', '2018-09-26 08:43:54'),
(28, 7, 7, '2018-09-26 08:44:25', '2018-09-26 08:44:25'),
(29, 8, 7, '2018-09-26 08:44:25', '2018-09-26 08:44:25'),
(30, 9, 7, '2018-09-26 08:44:25', '2018-09-26 08:44:25'),
(31, 10, 7, '2018-09-26 08:44:25', '2018-09-26 08:44:25'),
(32, 7, 7, '2018-09-26 08:44:44', '2018-09-26 08:44:44'),
(33, 8, 7, '2018-09-26 08:44:44', '2018-09-26 08:44:44'),
(34, 9, 7, '2018-09-26 08:44:44', '2018-09-26 08:44:44'),
(35, 10, 7, '2018-09-26 08:44:44', '2018-09-26 08:44:44'),
(36, 7, 7, '2018-09-26 08:45:23', '2018-09-26 08:45:23'),
(37, 8, 7, '2018-09-26 08:45:23', '2018-09-26 08:45:23'),
(38, 9, 7, '2018-09-26 08:45:24', '2018-09-26 08:45:24'),
(39, 10, 7, '2018-09-26 08:45:24', '2018-09-26 08:45:24'),
(40, 7, 7, '2018-09-26 08:48:59', '2018-09-26 08:48:59'),
(41, 8, 7, '2018-09-26 08:49:00', '2018-09-26 08:49:00'),
(42, 9, 7, '2018-09-26 08:49:00', '2018-09-26 08:49:00'),
(43, 10, 7, '2018-09-26 08:49:00', '2018-09-26 08:49:00'),
(44, 7, 7, '2018-09-26 08:50:11', '2018-09-26 08:50:11'),
(45, 8, 7, '2018-09-26 08:50:11', '2018-09-26 08:50:11'),
(46, 9, 7, '2018-09-26 08:50:11', '2018-09-26 08:50:11'),
(47, 10, 7, '2018-09-26 08:50:11', '2018-09-26 08:50:11'),
(48, 7, 7, '2018-09-26 08:50:29', '2018-09-26 08:50:29'),
(49, 8, 7, '2018-09-26 08:50:29', '2018-09-26 08:50:29'),
(50, 9, 7, '2018-09-26 08:50:30', '2018-09-26 08:50:30'),
(51, 10, 7, '2018-09-26 08:50:30', '2018-09-26 08:50:30'),
(52, 7, 7, '2018-09-26 08:50:43', '2018-09-26 08:50:43'),
(53, 8, 7, '2018-09-26 08:50:43', '2018-09-26 08:50:43'),
(54, 9, 7, '2018-09-26 08:50:44', '2018-09-26 08:50:44'),
(55, 10, 7, '2018-09-26 08:50:44', '2018-09-26 08:50:44'),
(56, 7, 7, '2018-09-26 08:51:10', '2018-09-26 08:51:10'),
(57, 8, 7, '2018-09-26 08:51:10', '2018-09-26 08:51:10'),
(58, 9, 7, '2018-09-26 08:51:10', '2018-09-26 08:51:10'),
(59, 10, 7, '2018-09-26 08:51:11', '2018-09-26 08:51:11'),
(60, 7, 7, '2018-09-26 08:51:51', '2018-09-26 08:51:51'),
(61, 8, 7, '2018-09-26 08:51:51', '2018-09-26 08:51:51'),
(62, 9, 7, '2018-09-26 08:51:51', '2018-09-26 08:51:51'),
(63, 10, 7, '2018-09-26 08:51:51', '2018-09-26 08:51:51'),
(64, 7, 7, '2018-09-26 08:52:36', '2018-09-26 08:52:36'),
(65, 8, 7, '2018-09-26 08:52:36', '2018-09-26 08:52:36'),
(66, 9, 7, '2018-09-26 08:52:36', '2018-09-26 08:52:36'),
(67, 10, 7, '2018-09-26 08:52:36', '2018-09-26 08:52:36'),
(68, 7, 7, '2018-09-26 08:59:53', '2018-09-26 08:59:53'),
(69, 8, 7, '2018-09-26 08:59:53', '2018-09-26 08:59:53'),
(70, 9, 7, '2018-09-26 08:59:53', '2018-09-26 08:59:53'),
(71, 10, 7, '2018-09-26 08:59:53', '2018-09-26 08:59:53'),
(72, 7, 7, '2018-09-26 09:00:18', '2018-09-26 09:00:18'),
(73, 8, 7, '2018-09-26 09:00:19', '2018-09-26 09:00:19'),
(74, 9, 7, '2018-09-26 09:00:19', '2018-09-26 09:00:19'),
(75, 10, 7, '2018-09-26 09:00:19', '2018-09-26 09:00:19'),
(76, 7, 7, '2018-09-26 09:01:21', '2018-09-26 09:01:21'),
(77, 8, 7, '2018-09-26 09:01:21', '2018-09-26 09:01:21'),
(78, 9, 7, '2018-09-26 09:01:21', '2018-09-26 09:01:21'),
(79, 10, 7, '2018-09-26 09:01:22', '2018-09-26 09:01:22'),
(80, 7, 7, '2018-09-26 09:01:42', '2018-09-26 09:01:42'),
(81, 8, 7, '2018-09-26 09:01:42', '2018-09-26 09:01:42'),
(82, 9, 7, '2018-09-26 09:01:43', '2018-09-26 09:01:43'),
(83, 10, 7, '2018-09-26 09:01:43', '2018-09-26 09:01:43'),
(84, 7, 7, '2018-09-26 09:02:01', '2018-09-26 09:02:01'),
(85, 8, 7, '2018-09-26 09:02:01', '2018-09-26 09:02:01'),
(86, 9, 7, '2018-09-26 09:02:02', '2018-09-26 09:02:02'),
(87, 10, 7, '2018-09-26 09:02:02', '2018-09-26 09:02:02'),
(88, 7, 7, '2018-09-26 09:03:17', '2018-09-26 09:03:17'),
(89, 8, 7, '2018-09-26 09:03:18', '2018-09-26 09:03:18'),
(90, 9, 7, '2018-09-26 09:03:18', '2018-09-26 09:03:18'),
(91, 10, 7, '2018-09-26 09:03:18', '2018-09-26 09:03:18'),
(92, 7, 7, '2018-09-26 09:05:04', '2018-09-26 09:05:04'),
(93, 8, 7, '2018-09-26 09:05:04', '2018-09-26 09:05:04'),
(94, 9, 7, '2018-09-26 09:05:04', '2018-09-26 09:05:04'),
(95, 10, 7, '2018-09-26 09:05:04', '2018-09-26 09:05:04'),
(96, 7, 7, '2018-09-26 09:07:04', '2018-09-26 09:07:04'),
(97, 8, 7, '2018-09-26 09:07:04', '2018-09-26 09:07:04'),
(98, 9, 7, '2018-09-26 09:07:04', '2018-09-26 09:07:04'),
(99, 10, 7, '2018-09-26 09:07:04', '2018-09-26 09:07:04'),
(100, 7, 7, '2018-09-26 09:07:21', '2018-09-26 09:07:21'),
(101, 8, 7, '2018-09-26 09:07:21', '2018-09-26 09:07:21'),
(102, 9, 7, '2018-09-26 09:07:21', '2018-09-26 09:07:21'),
(103, 10, 7, '2018-09-26 09:07:22', '2018-09-26 09:07:22'),
(104, 23, 7, '2020-04-21 01:05:31', '2020-04-21 01:05:31'),
(105, 24, 7, '2020-04-21 01:05:31', '2020-04-21 01:05:31'),
(106, 23, 6, '2020-04-21 03:29:49', '2020-04-21 03:29:49'),
(107, 25, 6, '2020-04-21 03:30:26', '2020-04-21 03:30:26'),
(108, 26, 7, '2020-04-23 07:33:01', '2020-04-23 07:33:01'),
(109, 26, 7, '2020-04-23 07:33:48', '2020-04-23 07:33:48'),
(110, 26, 7, '2020-04-23 07:35:37', '2020-04-23 07:35:37'),
(111, 26, 7, '2020-04-23 07:38:19', '2020-04-23 07:38:19'),
(112, 34, 7, '2020-04-23 08:07:49', '2020-04-23 08:07:49'),
(113, 50, 5, '2020-04-24 00:02:28', '2020-04-24 00:02:28'),
(114, 51, 5, '2020-04-24 00:02:28', '2020-04-24 00:02:28'),
(115, 50, 5, '2020-04-24 00:10:29', '2020-04-24 00:10:29'),
(116, 51, 5, '2020-04-24 00:10:29', '2020-04-24 00:10:29');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `name_en`, `name_ar`, `description`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'tool', 'تول', 'q', '2018-10-08 08:10:12', '2018-10-22 09:15:28', NULL, 1),
(2, 'another tool', 'تول', NULL, '2018-10-08 08:10:12', '2020-04-08 04:11:11', NULL, 1),
(3, 'qwe', 'qwe', 'qwe', '2018-10-13 12:33:52', '2018-10-24 05:42:33', NULL, 1),
(4, '220 Ohm Resistor', 'مقاومة 220 أوم', NULL, '2019-11-02 15:45:12', '2020-03-26 04:30:11', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `item_img`
--

CREATE TABLE `item_img` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(11) NOT NULL,
  `img_src` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_img`
--

INSERT INTO `item_img` (`id`, `item_id`, `img_src`, `created_at`, `updated_at`) VALUES
(2, 3, '/uploads/amCharts.jpg', '2018-10-24 05:43:34', '2018-10-24 05:43:34'),
(3, 1, 'https://d242j21808nrcn.cloudfront.net/images/payfort_logo.png', '2018-10-25 07:11:37', '2018-10-25 07:11:37'),
(4, 2, 'https://d242j21808nrcn.cloudfront.net/images/payfort_logo.png', '2018-10-25 07:11:41', '2018-10-25 07:11:41'),
(5, 4, 'http://dn734b9758kwp.cloudfront.net/images/1572709512J44A0417-Edit.jpg', '2019-11-02 15:45:12', '2019-11-02 15:45:12'),
(6, 4, 'http://dn734b9758kwp.cloudfront.net/images/1572709512J44A0417.jpg', '2019-11-02 15:45:13', '2019-11-02 15:45:13'),
(7, 5, 'http://dn734b9758kwp.cloudfront.net/images/1572710416shutterstock_190713134.jpg', '2019-11-02 16:00:16', '2019-11-02 16:00:16'),
(8, 6, 'http://dn734b9758kwp.cloudfront.net/images/1585216863electrical_engineering_blur.png', '2020-03-26 04:31:06', '2020-03-26 04:31:06'),
(9, 7, 'http://dn734b9758kwp.cloudfront.net/images/1585217384electrical_engineering_blur.png', '2020-03-26 04:39:50', '2020-03-26 04:39:50'),
(10, 4, 'http://dn734b9758kwp.cloudfront.net/images/1586338838lessons-slider-pic2.png', '2020-04-08 04:10:40', '2020-04-08 04:10:40'),
(11, 3, 'http://dn734b9758kwp.cloudfront.net/images/1586338855lessons-slider-pic2.png', '2020-04-08 04:10:57', '2020-04-08 04:10:57'),
(12, 2, 'http://dn734b9758kwp.cloudfront.net/images/1586338869lessons-slider-pic2.png', '2020-04-08 04:11:11', '2020-04-08 04:11:11');

-- --------------------------------------------------------

--
-- Table structure for table `item_qty`
--

CREATE TABLE `item_qty` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kit`
--

CREATE TABLE `kit` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description_ar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kit`
--

INSERT INTO `kit` (`id`, `name_en`, `name_ar`, `active`, `created_at`, `updated_at`, `description_ar`, `description_en`, `created_by`, `updated_by`) VALUES
(4, 'Robotics Kit', 'Kit', 1, '2018-10-14 10:49:38', '2020-04-09 06:03:35', '\"يوريكا\" هو برنامج علمي متخصص في التعليم التكنولوجي للابتكار والهندسة. تعتبر يوريكا أول أكاديمية في الأردن والعالم العربي تعمل على تطوير القدرات الإبداعية للأطفال في مجالات التكنولوجيا والهندسة. تعلم يوريكا الأطفال المفاهيم الأساسية للإنجو حيث يمكنهم تحويل الأفكار إلى منتجات مفيدة', '“Eureka” is a scientific program specialized in technological education of innovation and engineering. Eureka is considered the first academy in Jordan and the Arab world to develop the innovative capabilities of children in the areas of technology and engineering. Eureka teaches children the basic concepts of engo they can transform ideas to useful products and', NULL, NULL),
(5, 'Advanced Robotics Kit', 'Kit', 1, '2018-10-14 10:49:38', '2020-04-27 06:01:09', '\"يوريكا\" هو برنامج علمي متخصص في التعليم التكنولوجي للابتكار والهندسة. تعتبر يوريكا أول أكاديمية في الأردن والعالم العربي تعمل على تطوير القدرات الإبداعية للأطفال في مجالات التكنولوجيا والهندسة. تعلم يوريكا الأطفال المفاهيم الأساسية للإنجو حيث يمكنهم تحويل الأفكار إلى منتجات مفيدة', '“Eureka” is a scientific program specialized in technological education of innovation and engineering. Eureka is considered the first academy in Jordan and the Arab world to develop the innovative capabilities of children in the areas of technology and engineering. Eureka teaches children the basic concepts of engo they can transform ideas to useful products and', NULL, NULL),
(6, 'Electronics Kit', 'Kit', 1, '2018-10-14 10:49:38', '2020-04-09 06:02:50', '\"يوريكا\" هو برنامج علمي متخصص في التعليم التكنولوجي للابتكار والهندسة. تعتبر يوريكا أول أكاديمية في الأردن والعالم العربي تعمل على تطوير القدرات الإبداعية للأطفال في مجالات التكنولوجيا والهندسة. تعلم يوريكا الأطفال المفاهيم الأساسية للإنجو حيث يمكنهم تحويل الأفكار إلى منتجات مفيدة', '“Eureka” is a scientific program specialized in technological education of innovation and engineering. Eureka is considered the first academy in Jordan and the Arab world to develop the innovative capabilities of children in the areas of technology and engineering. Eureka teaches children the basic concepts of engo they can transform ideas to useful products and', NULL, NULL),
(7, 'Mechanism Kit', 'Kit', 1, '2018-10-14 10:49:38', '2020-04-27 06:01:35', '\"يوريكا\" هو برنامج علمي متخصص في التعليم التكنولوجي للابتكار والهندسة. تعتبر يوريكا أول أكاديمية في الأردن والعالم العربي تعمل على تطوير القدرات الإبداعية للأطفال في مجالات التكنولوجيا والهندسة. تعلم يوريكا الأطفال المفاهيم الأساسية للإنجو حيث يمكنهم تحويل الأفكار إلى منتجات مفيدة', '“Eureka” is a scientific program specialized in technological education of innovation and engineering. Eureka is considered the first academy in Jordan and the Arab world to develop the innovative capabilities of children in the areas of technology and engineering. Eureka teaches children the basic concepts of engo they can transform ideas to useful products and', NULL, NULL),
(8, 'Advanced Electronics Kit', 'Kit', 1, '2018-10-14 10:49:38', '2020-04-09 06:01:53', '\"يوريكا\" هو برنامج علمي متخصص في التعليم التكنولوجي للابتكار والهندسة. تعتبر يوريكا أول أكاديمية في الأردن والعالم العربي تعمل على تطوير القدرات الإبداعية للأطفال في مجالات التكنولوجيا والهندسة. تعلم يوريكا الأطفال المفاهيم الأساسية للإنجو حيث يمكنهم تحويل الأفكار إلى منتجات مفيدة', '“Eureka” is a scientific program specialized in technological education of innovation and engineering. Eureka is considered the first academy in Jordan and the Arab world to develop the innovative capabilities of children in the areas of technology and engineering. Eureka teaches children the basic concepts of engo they can transform ideas to useful products and', NULL, NULL),
(10, 'testEN', 'testAR', 1, '2020-03-26 03:09:51', '2020-04-09 06:01:30', '\"يوريكا\" هو برنامج علمي متخصص في التعليم التكنولوجي للابتكار والهندسة. تعتبر يوريكا أول أكاديمية في الأردن والعالم العربي تعمل على تطوير القدرات الإبداعية للأطفال في مجالات التكنولوجيا والهندسة. تعلم يوريكا الأطفال المفاهيم الأساسية للإنجو حيث يمكنهم تحويل الأفكار إلى منتجات مفيدة', '“Eureka” is a scientific program specialized in technological education of innovation and engineering. Eureka is considered the first academy in Jordan and the Arab world to develop the innovative capabilities of children in the areas of technology and engineering. Eureka teaches children the basic concepts of engo they can transform ideas to useful products and', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kit_img`
--

CREATE TABLE `kit_img` (
  `id` int(10) UNSIGNED NOT NULL,
  `kit_id` int(11) NOT NULL,
  `img_src` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kit_img`
--

INSERT INTO `kit_img` (`id`, `kit_id`, `img_src`, `created_at`, `updated_at`) VALUES
(4, 10, 'http://dn734b9758kwp.cloudfront.net/images/1586174266lessons-slider-pic2.png', '2020-04-06 06:27:49', '2020-04-06 06:27:49'),
(6, 4, 'http://dn734b9758kwp.cloudfront.net/images/1586174516lessons-slider-pic2.png', '2020-04-06 06:31:58', '2020-04-06 06:31:58'),
(7, 8, 'http://dn734b9758kwp.cloudfront.net/images/1586174657lessons-slider-pic2.png', '2020-04-06 06:34:19', '2020-04-06 06:34:19'),
(8, 6, 'http://dn734b9758kwp.cloudfront.net/images/1586338807kit-1.jpg', '2020-04-08 04:10:10', '2020-04-08 04:10:10'),
(9, 5, 'http://dn734b9758kwp.cloudfront.net/images/1586431535kit-1.jpg', '2020-04-09 05:55:38', '2020-04-09 05:55:38'),
(10, 7, 'http://dn734b9758kwp.cloudfront.net/images/1586431574kit-1.jpg', '2020-04-09 05:56:16', '2020-04-09 05:56:16');

-- --------------------------------------------------------

--
-- Table structure for table `kit_item`
--

CREATE TABLE `kit_item` (
  `id` int(10) UNSIGNED NOT NULL,
  `kit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kit_item`
--

INSERT INTO `kit_item` (`id`, `kit_id`, `item_id`, `created_at`, `updated_at`) VALUES
(7, 4, 1, '2018-10-14 10:49:53', '2018-10-14 10:49:53'),
(8, 4, 2, '2018-10-14 10:49:53', '2018-10-14 10:49:53'),
(10, 8, 3, '2019-10-23 07:30:21', '2019-10-23 07:30:21'),
(11, 6, 4, '2019-11-02 15:45:35', '2019-11-02 15:45:35'),
(12, 8, 4, '2020-03-25 05:33:22', '2020-03-25 05:33:22'),
(13, 10, 2, '2020-03-26 03:10:19', '2020-03-26 03:10:19'),
(14, 10, 3, '2020-03-26 03:10:28', '2020-03-26 03:10:28'),
(15, 4, 4, '2020-04-07 03:16:03', '2020-04-07 03:16:03'),
(16, 4, 3, '2020-04-07 03:16:13', '2020-04-07 03:16:13'),
(18, 10, 4, '2020-04-07 03:16:45', '2020-04-07 03:16:45'),
(19, 8, 1, '2020-04-07 03:17:00', '2020-04-07 03:17:00'),
(20, 8, 2, '2020-04-07 03:17:00', '2020-04-07 03:17:00'),
(21, 6, 1, '2020-04-07 03:17:16', '2020-04-07 03:17:16'),
(22, 6, 2, '2020-04-07 03:17:16', '2020-04-07 03:17:16'),
(23, 6, 3, '2020-04-07 03:17:16', '2020-04-07 03:17:16'),
(24, 5, 2, '2020-04-27 06:00:43', '2020-04-27 06:00:43'),
(25, 5, 4, '2020-04-27 06:00:43', '2020-04-27 06:00:43'),
(26, 7, 1, '2020-04-27 06:01:21', '2020-04-27 06:01:21'),
(27, 7, 3, '2020-04-27 06:01:21', '2020-04-27 06:01:21');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `image_path_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image_path_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `icon_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `description_en` text COLLATE utf8mb4_unicode_ci,
  `description_ar` text COLLATE utf8mb4_unicode_ci,
  `icon_path_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_path_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id`, `name_ar`, `name_en`, `slug`, `active`, `image_path_ar`, `image_path_en`, `icon_path`, `created_at`, `updated_at`, `created_by`, `updated_by`, `description_en`, `description_ar`, `icon_path_en`, `icon_path_ar`) VALUES
(6, 'البرمجة', 'Developments', 'developments', 1, '/uploads/games_development@2x.png', '/uploads/ab-2.png', '/uploads/Development.png', '2018-06-10 11:09:52', '2020-04-21 00:03:13', NULL, 1, 'With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science. works and how he connects with audiences With a hit talk nces With a hit talk show and bestselling books, Neil connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences connects with audiences', 'With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science. works and how he connects with audiences With a hit talk nces With a hit talk show and bestselling books, Neil connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences connects with audiences', NULL, NULL),
(7, 'الإلكترونيات', 'Electronics', 'electronics', 1, '/uploads/electrical_engineering.png', '/uploads/ab-1.png', '/uploads/Electronics.png', '2018-07-04 06:49:42', '2020-04-21 00:03:22', NULL, 1, 'With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science. works and how he connects with audiences With a hit talk nces With a hit talk show and bestselling books, Neil connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences connects with audiences', 'With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science. works and how he connects with audiences With a hit talk nces With a hit talk show and bestselling books, Neil connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences connects with audiences', NULL, NULL),
(8, 'الروبوتات', 'Robotics', 'robotics', 1, '', '/uploads/ab-3.png', '/uploads/Robotics.png', '2018-07-04 06:52:35', '2020-04-21 00:02:43', NULL, 1, 'With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science. works and how he connects with audiences With a hit talk nces With a hit talk show and bestselling books, Neil connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences connects with audiences', 'With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science. works and how he connects with audiences With a hit talk nces With a hit talk show and bestselling books, Neil connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences connects with audiences', NULL, NULL),
(10, 'الطاقة المتجددة', 'Renewable Energy', 'renewable-energy', 1, '', '/uploads/ab-4.png', '/uploads/Energy.png', '2018-11-05 14:45:57', '2020-04-21 00:03:33', 1, 1, 'With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science. works and how he connects with audiences With a hit talk nces With a hit talk show and bestselling books, Neil connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences connects with audiences', 'With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science. works and how he connects with audiences With a hit talk nces With a hit talk show and bestselling books, Neil connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences connects with audiences', NULL, NULL),
(11, 'تكنولوجيا ريادة الأعمال', 'Tech-entrepreneurship', 'tech-entrepreneurship', 1, '', '/uploads/ab-1.png', '/uploads/Electronics.png', '2018-11-05 14:50:11', '2020-04-21 00:04:20', 1, 1, 'مع برنامج حواري ناجح وكتب مبيعة ، يعد Neil deGrasse Tyson أحد أشهر الشخصيات في العلوم الحديثة. يعمل وكيف يتواصل مع الجمهور من خلال برنامج حواري ناجح وكتب مبيعة ، يعد Neil deGrasse Tyson أحد أشهر الشخصيات في العلوم الحديثة. نيل دي جراس تايسون هو أحد أكثر الشخصيات شعبية في العلوم الحديثة. يعمل وكيف يتواصل مع الجمهور يتواصل مع الجمهور', 'With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science. works and how he connects with audiences With a hit talk nces With a hit talk show and bestselling books, Neil connects with audiences With a hit talk show and bestselling books, Neil deGrasse Tyson is one of the most popular figures in modern science.  works and how he connects with audiences connects with audiences', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 2),
(3, '2018_05_16_211938_create_abandoned_carts_table', 2),
(4, '2018_05_16_212127_create_coupon_table', 2),
(5, '2018_05_16_213126_create_course_table', 2),
(6, '2018_05_19_135458_create_course_category_table', 2),
(7, '2018_05_19_135602_create_course_status_table', 2),
(8, '2018_05_19_135948_create_course_type_table', 2),
(9, '2018_05_19_140547_create_downloadable_table', 2),
(10, '2018_05_19_140748_create_level_table', 2),
(11, '2018_05_19_140921_create_payment_table', 2),
(12, '2018_05_19_141235_create_payment_method_table', 2),
(13, '2018_05_19_141520_create_stripe_customer_table', 2),
(14, '2018_05_19_141701_create_stripe_plan_table', 2),
(15, '2018_05_19_142046_create_subscription_table', 3),
(16, '2018_05_19_144942_create_subcription_model_table', 3),
(17, '2018_05_20_103236_create_teacher_fees_table', 3),
(18, '2018_05_20_103419_create_video_table', 3),
(19, '2018_06_13_121527_make_index_for_username', 4),
(20, '2018_06_19_070200_add_phone_age_to_user', 5),
(21, '2018_06_24_080907_add_lesson_table', 6),
(22, '2018_06_24_081642_add_session_table', 7),
(23, '2018_06_24_095158_add_free_to_lesson_table', 7),
(24, '2018_06_24_124441_add_lesson_id_to_video', 8),
(25, '2018_06_24_124548_add_user_progress_table', 8),
(26, '2018_06_24_125239_add_image_to_users', 9),
(27, '2018_06_25_104710_add_eurekian_stars_table', 10),
(28, '2018_06_25_114012_make_price_nullable_for_subsc_model', 11),
(29, '2018_06_25_133106_drop_lesson_table', 12),
(30, '2018_06_25_133528_add_lesson_id_to_video', 13),
(31, '2018_06_26_075526_change_session_number_to_varchar', 14),
(32, '2018_06_26_081053_add_free_and_intro_to_video', 14),
(33, '2018_06_26_081407_video_free_intro_default_value', 14),
(34, '2018_06_26_082502_drop_intro_from_video', 14),
(35, '2018_06_26_115530_change_lesson_id_to_video_id', 15),
(36, '2018_06_26_143132_add_wide_pic_to_course', 16),
(37, '2018_06_28_090705_add_progress_to_subscription', 17),
(38, '2018_06_28_103743_add_order_to_sessions', 18),
(39, '2018_07_14_074404_add_texts_table', 19),
(40, '2018_07_14_080537_change_text_string_to_text', 20),
(41, '2018_07_14_092312_drop_href_col', 21),
(42, '2018_07_14_094026_add_menu_bar_table', 22),
(43, '2018_07_14_100532_change_menu_items', 23),
(44, '2018_07_14_120146_slugs_menu_items', 24),
(45, '2018_07_14_120535_remove_type_from_text', 25),
(46, '2018_07_14_121502_rename_texts_to_slugs', 26),
(47, '2018_07_14_125846_remove_slugs_add_it_to_menu_items', 27),
(48, '2018_07_14_140304_add_active_to_slugs', 28),
(49, '2018_07_25_092237_add_quiz_table', 29),
(50, '2018_07_25_093223_add_quiz_choices_table', 30),
(51, '2018_07_26_121404_add_order_to_slugs', 31),
(52, '2018_07_26_142430_add_active_col_to_level', 32),
(53, '2018_07_29_090756_eurekian_stars_text', 33),
(54, '2018_07_31_131814_add_quiz_report_table', 34),
(55, '2018_08_05_095528_add_slug_to_video_', 35),
(56, '2018_08_05_100218_make_slug_in_course_unique', 36),
(57, '2018_08_05_103401_add_slug_to_level', 37),
(58, '2018_08_05_103624_make_slug_in_level_unique', 38),
(59, '2018_08_07_093337_create_footer_table', 39),
(60, '2018_08_07_103423_create_email_table', 40),
(61, '2018_08_07_115829_add_admin_to_user_tabke', 41),
(62, '2018_08_29_104343_add_teacher_to_user', 42),
(63, '2018_08_29_105257_add_teacher_fees_to_user', 43),
(64, '2018_08_29_141346_add_payment_method_to_payment', 44),
(65, '2018_08_30_105652_add_course_id_to_user_progress', 45),
(66, '2018_09_05_102426_add_text_ar_to_slug_table', 46),
(67, '2018_09_05_102720_add_text_ar_to_eurekian_stars_text_table', 47),
(68, '2018_09_05_123103_add_session_number_ar_to_session_table', 48),
(69, '2018_09_05_140659_add_statement_ar_to_quiz_table', 49),
(70, '2018_09_05_140820_add_choice_statement_to_choices_table', 50),
(71, '2018_09_22_090723_create_groups_table', 51),
(72, '2018_09_22_090909_create_groups_users_table', 52),
(73, '2018_09_24_100203_add_name_to_subscription_model_table', 53),
(74, '2018_09_24_122450_add_course_id_and_group_id_to_coupon_table', 54),
(75, '2018_09_26_083350_add_active_to_user_table', 55),
(76, '2018_10_04_100128_add_seen_video_to_user_progress_table', 56),
(77, '2018_10_04_123225_create_kit_table', 57),
(78, '2018_10_04_123235_create_item_table', 57),
(79, '2018_10_04_123242_create_kit_item_table', 57),
(80, '2018_10_04_123252_create_item_count_table', 57),
(81, '2018_10_04_123305_create_supplier_item_table', 57),
(82, '2018_10_04_123328_add_supplier_to_user_table', 57),
(83, '2018_10_04_132148_create_order_table', 57),
(84, '2018_10_04_132203_create_order_item_table', 57),
(85, '2018_10_07_220816_change_kit_item_table', 58),
(86, '2018_10_08_103224_make_kit_id_order_table_nullable', 59),
(87, '2018_10_13_143638_create_item_img_table', 59),
(88, '2018_10_13_143712_add_description_to_item_table', 60),
(89, '2018_10_13_210131_create_kits_image_talbe', 61),
(90, '2018_10_13_215557_add_description_to_kit_table', 61),
(91, '2018_10_14_114434_drop_price_column_from_kits_table', 62),
(92, '2018_10_16_081237_add_created_by_to_order_table', 63),
(93, '2018_10_16_083343_add_creatd_by_to_main_tables', 64),
(94, '2018_10_22_095918_add_price_2_to_order_and_order_item_table', 65),
(95, '2018_10_22_124308_add_packed_to_order_item_table', 66),
(96, '2019_03_07_112244_add_lines_and_account_number_to_order_table', 67),
(97, '2019_03_07_115727_add_person_name_to_order_table', 67),
(98, '2019_03_07_121459_add_country_code_to_order_table', 67),
(99, '2019_05_03_151218_create_email_broadcast_table', 68),
(100, '2019_05_03_162157_remove_to_from_emails_broadcast_table', 69),
(101, '2019_05_06_233918_create_supplier_info_table', 70),
(102, '2019_05_07_000239_modify_order_table', 71),
(103, '2019_05_07_013316_add_supplier_id_to_order_table', 72),
(104, '2019_05_07_014448_add_volume_to_order_table', 73),
(105, '2019_05_07_021642_add_shipment_receipts_to_order_table', 74),
(106, '2019_05_31_011415_remove_account_pin_from_order_table', 75),
(107, '2019_10_24_212440_create_gallery_table', 76),
(108, '2019_11_02_091044_create_albums_table', 77),
(109, '2019_11_03_175122_create_achievements_table', 78);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `kit_id` int(11) DEFAULT NULL,
  `total_price` double(8,3) NOT NULL,
  `selling_price` double(8,2) NOT NULL DEFAULT '0.00',
  `state` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `contact_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `line1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `line2` text COLLATE utf8mb4_unicode_ci,
  `line3` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `volume` int(11) DEFAULT NULL,
  `shipment_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipment_label_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `payment_method` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('unpaid','paid','pending','canceled','codrequest') COLLATE utf8mb4_unicode_ci DEFAULT 'pending',
  `coupon_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_amount` decimal(10,2) DEFAULT '0.00',
  `post_data` text COLLATE utf8mb4_unicode_ci,
  `response_data` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `user_id`, `kit_id`, `total_price`, `selling_price`, `state`, `created_at`, `updated_at`, `updated_by`, `contact_name`, `contact_email`, `line1`, `line2`, `line3`, `phone`, `country_code`, `province`, `postal_code`, `phone_number`, `city`, `supplier_id`, `volume`, `shipment_id`, `shipment_label_url`, `weight`, `payment_method`, `status`, `coupon_code`, `coupon_amount`, `post_data`, `response_data`) VALUES
(67, 11, 0, 2147.500, 0.00, 3, '2020-05-12 06:42:11', '2020-05-26 08:16:41', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '7894567891235', 'BE', NULL, NULL, NULL, 'Vadodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'paid', NULL, '0.00', NULL, NULL),
(69, 21, 0, 1460.000, 0.00, 1, '2020-05-13 04:41:50', '2020-05-13 04:41:50', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '09998972498', 'AX', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(70, 21, 0, 612.500, 0.00, 1, '2020-05-13 05:09:01', '2020-05-13 05:09:01', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '09998972498', 'AF', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(71, 21, 0, 597.500, 0.00, 1, '2020-05-13 05:18:37', '2020-05-13 05:18:37', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '09998972498', 'AX', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'pending', NULL, '0.00', NULL, NULL),
(72, 21, 0, 597.500, 0.00, 1, '2020-05-13 05:38:46', '2020-05-13 05:38:46', NULL, NULL, NULL, 'vadodara', 'vadodaar', NULL, '09998972498', 'AX', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'pending', NULL, '0.00', NULL, NULL),
(73, 21, 0, 80.000, 0.00, 1, '2020-05-13 05:48:36', '2020-05-13 05:48:36', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '09998972498', 'AL', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'pending', NULL, '0.00', NULL, NULL),
(74, 21, 0, 80.000, 0.00, 1, '2020-05-13 06:52:10', '2020-05-13 06:52:10', NULL, NULL, NULL, 'vadodara', 'g', NULL, '09998972498', 'AX', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'pending', NULL, '0.00', NULL, NULL),
(75, 21, 0, 80.000, 0.00, 1, '2020-05-13 07:48:44', '2020-05-13 07:48:44', NULL, NULL, NULL, 'vadodara', 'fd', NULL, '09998972498', 'AL', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'pending', NULL, '0.00', NULL, NULL),
(76, 21, 0, 80.000, 0.00, 1, '2020-05-14 00:56:12', '2020-05-14 00:56:12', NULL, NULL, NULL, 'vadodara', 'saAa', NULL, '09998972498', 'AS', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'pending', NULL, '0.00', NULL, NULL),
(77, 21, 0, 532.500, 0.00, 1, '2020-05-14 01:07:05', '2020-05-14 01:07:05', NULL, NULL, NULL, 'vadodara', 'vad', NULL, '09998972498', 'AS', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'pending', NULL, '0.00', NULL, NULL),
(78, 21, 0, 542.500, 0.00, 1, '2020-05-14 01:15:39', '2020-05-14 01:15:39', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '09998972498', 'AS', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'pending', NULL, '0.00', NULL, NULL),
(79, 21, 0, 7.000, 0.00, 1, '2020-05-14 01:19:14', '2020-05-14 01:19:14', NULL, NULL, NULL, 'vadodara', 'cccc', NULL, '09998972498', 'AS', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'pending', NULL, '0.00', NULL, NULL),
(80, 21, 0, 532.500, 0.00, 1, '2020-05-14 05:03:36', '2020-05-14 05:03:36', NULL, NULL, NULL, 'vadodara', 'fdsfdsf', NULL, '09998972498', 'AS', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'pending', NULL, '0.00', NULL, NULL),
(81, 21, 0, 532.500, 0.00, 5, '2020-05-14 07:07:39', '2020-06-10 08:41:39', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '09998972498', 'AS', NULL, NULL, NULL, 'vadaodara', NULL, NULL, '45267534572', 'https://ws.aramex.net/content/rpt_cache/5171c91224704deab93e51b13583869f.pdf', NULL, 'card', 'paid', NULL, '0.00', NULL, NULL),
(82, 21, 0, 15.000, 0.00, 1, '2020-05-14 07:25:07', '2020-05-15 00:22:39', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '09998972498', 'AS', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'paid', NULL, '0.00', NULL, NULL),
(83, 21, 0, 15.000, 0.00, 1, '2020-05-14 07:26:35', '2020-05-14 07:41:02', NULL, NULL, NULL, 'vadodara', 'cdc', NULL, '09998972498', 'AS', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'paid', NULL, '0.00', NULL, NULL),
(84, 21, 0, 1050.000, 0.00, 1, '2020-05-15 07:38:54', '2020-05-15 07:38:54', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '09998972498', 'AX', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'pending', 'SIDD', '10.00', NULL, NULL),
(85, 21, 0, 70.000, 0.00, 1, '2020-05-15 07:54:08', '2020-05-15 07:54:08', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '09998972498', 'AX', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'pending', 'SIDD', '10.00', NULL, NULL),
(86, 21, 0, 70.000, 0.00, 1, '2020-05-15 08:01:48', '2020-05-15 08:02:49', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '09998972498', 'AX', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'paid', 'SIDD', '10.00', NULL, NULL),
(87, 21, 0, 5.000, 0.00, 1, '2020-05-15 08:07:17', '2020-05-15 08:11:45', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '09998972498', 'AX', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'paid', 'SIDD', '10.00', NULL, NULL),
(88, 21, 0, 5.000, 0.00, 1, '2020-05-15 08:23:17', '2020-05-15 08:24:07', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '09998972498', 'AS', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'paid', 'SIDD', '10.00', NULL, NULL),
(89, 11, 0, 762.500, 0.00, 1, '2020-05-19 03:39:29', '2020-05-19 03:39:29', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '7894567891235', 'HT', NULL, NULL, NULL, 'Vadodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(90, 11, 0, 762.500, 0.00, 1, '2020-05-19 03:40:09', '2020-05-19 03:40:09', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '7894567891235', 'AF', NULL, NULL, NULL, 'Vadodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(91, 11, 0, 762.500, 0.00, 1, '2020-05-19 03:41:36', '2020-05-19 03:41:36', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '7894567891235', 'AX', NULL, NULL, NULL, 'Vadodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(92, 11, 0, 920.000, 0.00, 1, '2020-05-19 03:46:16', '2020-05-19 03:46:16', NULL, NULL, NULL, 'vadodara', 'vadodara', 'vadodara', '7894567891235', 'AL', NULL, NULL, NULL, 'Vadodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(93, 11, 0, 230.000, 0.00, 1, '2020-05-19 03:50:44', '2020-05-19 03:50:44', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '12345678963', 'AX', NULL, NULL, NULL, 'Vadodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(94, 11, 0, 115.000, 0.00, 1, '2020-05-19 04:02:45', '2020-05-19 04:02:45', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '12345678963', 'AX', NULL, NULL, NULL, 'Vadodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(95, 11, 0, 230.000, 0.00, 1, '2020-05-19 04:12:13', '2020-05-19 04:12:13', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '12345678963', 'AX', NULL, NULL, NULL, 'Vadodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(96, 11, 0, 230.000, 0.00, 1, '2020-05-19 04:13:08', '2020-05-19 04:13:08', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '12345678963', 'AX', NULL, NULL, NULL, 'Vadodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(97, 1, 0, 632.500, 0.00, 1, '2020-05-21 08:19:13', '2020-05-21 08:19:13', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '12345678963', 'AF', NULL, NULL, NULL, 'Vadodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(98, 21, 0, 737.500, 0.00, 1, '2020-05-22 05:52:17', '2020-05-22 07:37:00', NULL, NULL, NULL, 'vadodara', 'vadodara', NULL, '09998972498', 'AS', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'paid', 'SIDD', '10.00', NULL, NULL),
(99, 21, 0, 532.500, 0.00, 1, '2020-05-22 07:41:49', '2020-05-22 07:41:49', NULL, NULL, NULL, 'tretrewtr', 'fsfdfa', 'sfsfa', '0265656615', 'AL', NULL, NULL, NULL, 'ttqret ewrt', NULL, NULL, NULL, NULL, NULL, 'card', 'pending', NULL, '0.00', NULL, NULL),
(100, 21, 0, 5.000, 0.00, 1, '2020-05-22 07:45:59', '2020-05-22 07:45:59', NULL, NULL, NULL, 'vadodara', 'gorwa', NULL, '09998972498', 'AS', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'pending', 'SIDD', '10.00', NULL, NULL),
(101, 1, 0, 762.500, 0.00, 4, '2020-05-22 07:57:57', '2020-05-27 23:55:55', NULL, NULL, NULL, 'test address', 'sadsa', NULL, '2342432432', 'AL', NULL, NULL, NULL, 'Vadodara', NULL, NULL, '45267625804', 'https://ws.aramex.net/content/rpt_cache/4220b358284f4db1a47654030407ecd8.pdf', NULL, 'cod', 'paid', NULL, '0.00', NULL, NULL),
(102, 11, 0, 15.000, 0.00, 1, '2020-05-22 08:04:05', '2020-05-22 08:20:48', NULL, NULL, NULL, 'vadodara', 'gorwa', NULL, '09998972498', 'AS', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'paid', NULL, '0.00', NULL, NULL),
(103, 11, 0, 517.500, 0.00, 1, '2020-05-22 08:25:14', '2020-05-22 08:25:14', NULL, NULL, NULL, 'vadodara', 'gorwa', NULL, '09998972498', 'AS', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(104, 11, 0, 517.500, 0.00, 1, '2020-05-22 08:48:13', '2020-05-22 08:48:13', NULL, NULL, NULL, 'vadodara', 'vcxvcx', NULL, '09998972498', 'AS', NULL, NULL, NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(105, 23, 0, 172.500, 0.00, 1, '2020-05-25 00:15:18', '2020-05-25 00:15:18', NULL, 'ketul patel', 'ketulpatel@webmynesystems.com', 'vadodara', 'vadodara2', 'vadodara3', '12345678963', 'India', 'vadodara', '390007', NULL, 'Vadodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(106, 23, 0, 115.000, 0.00, 1, '2020-05-25 00:20:09', '2020-05-25 00:20:09', NULL, 'ketul patel', 'ketulpatel@webmynesystems.com', 'vadodara', 'vadodara2', 'vadodara3', '789456789456', 'India', 'vadodara', '390007', '789456789456', 'Vadodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(107, 21, 0, 680.000, 0.00, 1, '2020-05-25 00:55:20', '2020-05-25 00:55:20', NULL, 'sid r raj', 'sid@gmail.com', 'gorwa', 'laxmipura', NULL, '09998972498', 'American S', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'pending', 'SIDD', '10.00', NULL, NULL),
(108, 23, 0, 517.500, 0.00, 1, '2020-05-25 01:24:06', '2020-05-25 01:24:06', NULL, 'ketul patel', 'ketulpatel@webmynesystems.com', 'vadodara', 'vadodara', 'vadodara3', '789456789456', 'Albania', 'vadodara', '390007', '789456789456', 'Vadodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(109, 21, 0, 507.500, 0.00, 3, '2020-05-25 02:13:17', '2020-05-26 23:24:53', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'gorwa', NULL, '09998972498', 'American S', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'paid', 'SIDD', '10.00', NULL, NULL),
(110, 21, 0, 230.000, 0.00, 1, '2020-05-25 03:58:51', '2020-05-25 03:58:51', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'gorwa', NULL, '09998972498', 'American S', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(111, 23, 0, 747.500, 0.00, 1, '2020-05-26 00:18:51', '2020-05-26 00:18:51', NULL, 'ketul patel', 'ketulpatel@webmynesystems.com', 'vadodara', 'vadodara', 'vadodara3', '789456789456', 'India', 'vadodara', '390007', '789456789456', 'Vadodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(112, 11, 0, 15.000, 0.00, 1, '2020-05-26 01:33:01', '2020-05-26 01:34:23', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'laxmipura', NULL, '09998972498', 'American S', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'paid', NULL, '0.00', NULL, NULL),
(113, 11, 0, 15.000, 0.00, 1, '2020-05-26 01:37:19', '2020-05-26 01:38:21', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'vad', NULL, '09998972498', 'American S', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'paid', NULL, '0.00', NULL, NULL),
(114, 11, 0, 15.000, 0.00, 1, '2020-05-26 01:49:02', '2020-05-26 01:49:02', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'vad', NULL, '09998972498', 'American S', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', NULL, '0.00', NULL, NULL),
(115, 11, 0, 15.000, 0.00, 1, '2020-05-26 03:57:50', '2020-05-26 03:58:48', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'vadodara', NULL, '09998972498', 'American S', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'paid', NULL, '0.00', NULL, NULL),
(116, 11, 0, 15.000, 0.00, 1, '2020-05-26 04:19:47', '2020-05-26 04:20:38', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'czc', NULL, '09998972498', 'American S', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'paid', NULL, '0.00', NULL, NULL),
(117, 11, 0, 517.500, 0.00, 3, '2020-05-26 04:39:54', '2020-06-09 01:17:02', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'vc', NULL, '09998972498', 'American S', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'paid', NULL, '0.00', '{\"ClientInfo\":{\"AccountCountryCode\":\"JO\",\"AccountEntity\":\"AMM\",\"AccountNumber\":\"20016\",\"AccountPin\":\"543543\",\"UserName\":\"Nabily@aramex.com\",\"Password\":\"Aramex123$\",\"Version\":\"v1\"},\"Transaction\":{\"Reference1\":\"117\",\"Reference2\":\"\",\"Reference3\":\"\",\"Reference4\":\"\"},\"LabelInfo\":{\"ReportID\":9201,\"ReportType\":\"URL\"},\"Shipments\":{\"Shipment\":{\"Shipper\":{\"Reference1\":\"some ref\",\"AccountNumber\":\"20016\",\"Contact\":{\"PersonName\":\"Moustafa2\",\"CompanyName\":\"Eureka Tech Academy\",\"PhoneNumber1\":\"+96399475326\",\"CellPhone\":\"+963115489872\",\"EmailAddress\":\"test@email.com\"},\"PartyAddress\":{\"Line1\":\"test address line\",\"Line2\":\"address line2\",\"Line3\":null,\"City\":\"Amman\",\"StateOrProvinceCode\":null,\"PostCode\":null,\"CountryCode\":\"JO\",\"Longitude\":\"0\",\"Latitude\":\"0\"}},\"Consignee\":{\"Reference1\":\"Ref 333333\",\"Reference2\":\"Ref 444444\",\"AccountNumber\":\"20016\",\"Contact\":{\"PersonName\":\"bharat\",\"CompanyName\":\"bharat\",\"PhoneNumber1\":\"123\",\"CellPhone\":\"123\",\"EmailAddress\":\"bharat@gmail.com\"},\"PartyAddress\":{\"Line1\":\"vadodara\",\"Line2\":\"vc\",\"Line3\":null,\"CountryCode\":\"American S\",\"City\":\"vadaodara\",\"PostCode\":\"390016\"}},\"ShippingDateTime\":1591973509,\"DueDate\":1592166927,\"Comments\":null,\"PickupLocation\":null,\"Attachments\":null,\"ForeignHAWB\":null,\"TransportType\":\"0\",\"PickupGUID\":null,\"Number\":null,\"Details\":{\"ActualWeight\":{\"Value\":2,\"Unit\":\"Kg\"},\"ProductGroup\":\"DOM\",\"ProductType\":\"ONP\",\"PaymentType\":\"P\",\"PaymentOptions\":\"ARCC\",\"NumberOfPieces\":1,\"DescriptionOfGoods\":\"Box of Electronic tools and stuffs\",\"GoodsOriginCountry\":\"Jo\",\"CollectAmount\":{\"Value\":0,\"CurrencyCode\":\"JOD\"},\"Items\":{\"PackageType\":\"Box\",\"Quantity\":1,\"Weight\":{\"Value\":2,\"Unit\":\"Kg\"},\"Comments\":\"Electronic tools and stuffs\",\"Reference\":\"\"}}}},\"Pickup\":{\"Reference1\":\"123123\",\"PickupLocation\":\"here\",\"Status\":\"Pending\",\"PickupDate\":1591876800,\"ReadyTime\":1591876800,\"LastPickupTime\":1591973509,\"ClosingTime\":1591979924,\"PickupContact\":{\"PersonName\":\"Moustafa2\",\"CompanyName\":\"Eureka Tech Academy\",\"PhoneNumber1\":\"+96399475326\",\"CellPhone\":\"+963115489872\",\"EmailAddress\":\"test@email.com\"},\"PickupAddress\":{\"Line1\":\"test address line\",\"Line2\":\"address line2\",\"Line3\":null,\"CountryCode\":\"JO\",\"City\":\"Amman\"},\"PickupItems\":{\"PickupItemDetail\":{\"Reference1\":\"123123\",\"ProductGroup\":\"EXP\",\"Payment\":\"C\",\"ProductType\":\"EPX\",\"NumberOfPieces\":\"1\",\"ShipmentWeight\":{\"Value\":\"2\",\"Unit\":\"Kg\"},\"NumberOfShipments\":1,\"ShipmentVolume\":{\"Value\":\"50\",\"Unit\":\"Cm3\"}}}}}', '{\"Transaction\":{\"Reference1\":\"117\",\"Reference2\":\"\",\"Reference3\":\"\",\"Reference4\":\"\",\"Reference5\":null},\"Notifications\":{},\"HasErrors\":true,\"Shipments\":{\"ProcessedShipment\":{\"ID\":null,\"Reference1\":null,\"Reference2\":null,\"Reference3\":null,\"ForeignHAWB\":null,\"HasErrors\":true,\"Notifications\":{\"Notification\":{\"Code\":\"ERR04\",\"Message\":\"Consignee.PartyAddress - Invalid Country Code\"}},\"ShipmentLabel\":null}}}'),
(118, 1, 0, 517.500, 0.00, 5, '2020-05-27 01:01:52', '2020-06-10 08:41:41', NULL, 'ketul patel', 'user@user.com', 'vadodara', 'vadodara', 'vadodara3', '789456789456', 'AE', 'vadodara', '390007', '789456789456', 'Vadodara', NULL, NULL, '45267607070', 'https://ws.aramex.net/content/rpt_cache/a1a7bcdeb51c4d47b835584b721f2149.pdf', NULL, 'cod', 'paid', NULL, '0.00', NULL, NULL),
(119, 21, 0, 15.000, 0.00, 1, '2020-05-28 00:24:11', '2020-05-28 00:32:11', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'gorwa', NULL, '09998972498', 'AS', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'paid', NULL, '0.00', NULL, NULL),
(120, 11, 0, 15.000, 0.00, 1, '2020-05-28 00:43:04', '2020-05-28 00:44:12', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'gorwa', NULL, '09998972498', 'AS', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'paid', NULL, '0.00', NULL, NULL),
(121, 11, 0, 15.000, 0.00, 1, '2020-05-28 00:48:38', '2020-05-28 00:50:10', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'gorwa', NULL, '09998972498', 'AS', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'canceled', NULL, '0.00', NULL, NULL),
(122, 11, 0, 15.000, 0.00, 1, '2020-05-28 01:01:32', '2020-05-28 01:02:15', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'gorwa', NULL, '09998972498', 'AS', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'canceled', NULL, '0.00', NULL, NULL),
(123, 1, 0, 7877.500, 0.00, 4, '2020-06-02 23:53:18', '2020-06-03 00:38:00', NULL, 'ketul patel', 'ketulpatel@webmynesystems.com', 'vadodara', 'vadodara', 'vadodara3', '789456789456', 'JO', 'aman', '11190', '789456789456', 'aman', NULL, NULL, '45267723325', 'https://ws.aramex.net/content/rpt_cache/e7b3139f58564f75a7ae3f9f2a5eba10.pdf', NULL, 'cod', 'paid', NULL, '0.00', '{\"ClientInfo\":{\"AccountCountryCode\":\"JO\",\"AccountEntity\":\"AMM\",\"AccountNumber\":\"20016\",\"AccountPin\":\"543543\",\"UserName\":\"Nabily@aramex.com\",\"Password\":\"Aramex123$\",\"Version\":\"v1\"},\"Transaction\":{\"Reference1\":\"123\",\"Reference2\":\"\",\"Reference3\":\"\",\"Reference4\":\"\"},\"LabelInfo\":{\"ReportID\":9201,\"ReportType\":\"URL\"},\"Shipments\":{\"Shipment\":{\"Shipper\":{\"Reference1\":\"some ref\",\"AccountNumber\":\"20016\",\"Contact\":{\"PersonName\":\"Moustafa2\",\"CompanyName\":\"Eureka Tech Academy\",\"PhoneNumber1\":\"+96399475326\",\"CellPhone\":\"+963115489872\",\"EmailAddress\":\"test@email.com\"},\"PartyAddress\":{\"Line1\":\"test address line\",\"Line2\":\"address line2\",\"Line3\":null,\"City\":\"Amman\",\"StateOrProvinceCode\":null,\"PostCode\":null,\"CountryCode\":\"JO\",\"Longitude\":\"0\",\"Latitude\":\"0\"}},\"Consignee\":{\"Reference1\":\"Ref 333333\",\"Reference2\":\"Ref 444444\",\"AccountNumber\":\"20016\",\"Contact\":{\"PersonName\":\"Admin\",\"CompanyName\":\"Admin\",\"PhoneNumber1\":\"789456789456\",\"CellPhone\":\"+963000000000\",\"EmailAddress\":\"user@user.com\"},\"PartyAddress\":{\"Line1\":\"vadodara\",\"Line2\":\"vadodara\",\"Line3\":\"vadodara3\",\"CountryCode\":\"JO\",\"City\":\"aman\",\"PostCode\":\"11190\"}},\"ShippingDateTime\":1591455109,\"DueDate\":null,\"Comments\":null,\"PickupLocation\":null,\"Attachments\":null,\"ForeignHAWB\":null,\"TransportType\":\"0\",\"PickupGUID\":null,\"Number\":null,\"Details\":{\"ActualWeight\":{\"Value\":2,\"Unit\":\"Kg\"},\"ProductGroup\":\"DOM\",\"ProductType\":\"OND\",\"PaymentType\":\"C\",\"PaymentOptions\":\"ARCC\",\"NumberOfPieces\":1,\"DescriptionOfGoods\":\"Box of Electronic tools and stuffs\",\"GoodsOriginCountry\":\"Jo\",\"CollectAmount\":{\"Value\":0,\"CurrencyCode\":\"JOD\"},\"Items\":{\"PackageType\":\"Box\",\"Quantity\":1,\"Weight\":{\"Value\":2,\"Unit\":\"Kg\"},\"Comments\":\"Electronic tools and stuffs\",\"Reference\":\"\"},\"Services\":\"CODS\",\"CashonDelivery\":{\"CurrencyCode\":\"USD\",\"Value\":54.827}}}},\"Pickup\":{\"Reference1\":\"123123\",\"PickupLocation\":\"here\",\"Status\":\"Pending\",\"PickupDate\":1591358400,\"ReadyTime\":1591358400,\"LastPickupTime\":1591455109,\"ClosingTime\":1591461524,\"PickupContact\":{\"PersonName\":\"Moustafa2\",\"CompanyName\":\"Eureka Tech Academy\",\"PhoneNumber1\":\"+96399475326\",\"CellPhone\":\"+963115489872\",\"EmailAddress\":\"test@email.com\"},\"PickupAddress\":{\"Line1\":\"test address line\",\"Line2\":\"address line2\",\"Line3\":null,\"CountryCode\":\"JO\",\"City\":\"Amman\"},\"PickupItems\":{\"PickupItemDetail\":{\"Reference1\":\"123123\",\"ProductGroup\":\"EXP\",\"Payment\":\"C\",\"ProductType\":\"EPX\",\"NumberOfPieces\":\"1\",\"ShipmentWeight\":{\"Value\":\"2\",\"Unit\":\"Kg\"},\"NumberOfShipments\":1,\"ShipmentVolume\":{\"Value\":\"50\",\"Unit\":\"Cm3\"}}}}}', '{\"Transaction\":{\"Reference1\":\"123\",\"Reference2\":\"\",\"Reference3\":\"\",\"Reference4\":\"\",\"Reference5\":null},\"Notifications\":{},\"HasErrors\":false,\"Shipments\":{\"ProcessedShipment\":{\"ID\":\"45267723325\",\"Reference1\":null,\"Reference2\":null,\"Reference3\":null,\"ForeignHAWB\":null,\"HasErrors\":false,\"Notifications\":{},\"ShipmentLabel\":{\"LabelURL\":\"https:\\/\\/ws.aramex.net\\/content\\/rpt_cache\\/e7b3139f58564f75a7ae3f9f2a5eba10.pdf\",\"LabelFileContents\":\"\"}}}}'),
(124, 21, 0, 230.000, 0.00, 1, '2020-06-03 01:49:52', '2020-06-03 01:49:52', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'gorwa', NULL, '09998972498', 'AS', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'pending', NULL, '0.00', NULL, NULL),
(125, 11, 0, 15.000, 0.00, 5, '2020-06-03 01:54:23', '2020-06-03 07:30:35', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'gorwa', NULL, '09998972498', 'AS', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'paid', NULL, '0.00', NULL, NULL),
(126, 11, 0, 15.000, 0.00, 5, '2020-06-03 01:56:10', '2020-06-10 08:41:43', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'gorwa', NULL, '09998972498', 'AS', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, '45267946986', 'https://ws.aramex.net/content/rpt_cache/82d63d9270904c4bbd20105206117b62.pdf', NULL, 'cod', 'codrequest', NULL, '0.00', '{\"Shipments\":[{\"Shipper\":{\"Reference1\":\"Ref 111111\",\"Reference2\":\"Ref 222222\",\"AccountNumber\":\"20016\",\"PartyAddress\":{\"Line1\":\"test address line\",\"Line2\":\"address line2\",\"Line3\":null,\"City\":\"Amman\",\"StateOrProvinceCode\":null,\"PostCode\":null,\"CountryCode\":\"JO\"},\"Contact\":{\"Department\":\"\",\"PersonName\":\"Moustafa2\",\"Title\":\"\",\"CompanyName\":\"Aramex\",\"PhoneNumber1\":\"+96399475326\",\"PhoneNumber1Ext\":\"125\",\"PhoneNumber2\":\"4444444\",\"PhoneNumber2Ext\":\"456\",\"FaxNumber\":\"\",\"CellPhone\":\"+963115489872\",\"EmailAddress\":\"test@email.com\",\"Type\":\"\"}},\"Consignee\":{\"Reference1\":\"Ref 333333\",\"Reference2\":\"Ref 444444\",\"AccountNumber\":\"20016\",\"PartyAddress\":{\"Line1\":\"vadodara\",\"Line2\":\"gorwa\",\"Line3\":null,\"City\":\"vadaodara\",\"StateOrProvinceCode\":\"\",\"PostCode\":\"390016\",\"CountryCode\":\"AS\"},\"Contact\":{\"Department\":\"\",\"PersonName\":\"bharat\",\"Title\":\"\",\"CompanyName\":\"bharat\",\"PhoneNumber1\":\"123\",\"PhoneNumber1Ext\":\"155\",\"PhoneNumber2\":\"789456\",\"PhoneNumber2Ext\":\"456\",\"FaxNumber\":\"\",\"CellPhone\":\"123\",\"EmailAddress\":\"bharat@gmail.com\",\"Type\":\"\"}},\"ThirdParty\":{\"Reference1\":\"\",\"Reference2\":\"\",\"AccountNumber\":\"\",\"PartyAddress\":{\"Line1\":\"\",\"Line2\":\"\",\"Line3\":\"\",\"City\":\"\",\"StateOrProvinceCode\":\"\",\"PostCode\":\"\",\"CountryCode\":\"\"},\"Contact\":{\"Department\":\"\",\"PersonName\":\"\",\"Title\":\"\",\"CompanyName\":\"\",\"PhoneNumber1\":\"\",\"PhoneNumber1Ext\":\"\",\"PhoneNumber2\":\"\",\"PhoneNumber2Ext\":\"\",\"FaxNumber\":\"\",\"CellPhone\":\"\",\"EmailAddress\":\"\",\"Type\":\"\"}},\"Reference1\":\"Shpt 0001\",\"Reference2\":\"\",\"Reference3\":\"\",\"ForeignHAWB\":\"\",\"TransportType\":0,\"ShippingDateTime\":\"\\/Date(1591878214418)\\/\",\"DueDate\":\"\\/Date(1591878311127)\\/\",\"PickupLocation\":\"\",\"PickupGUID\":\"\",\"Comments\":\"Shpt 0001\",\"AccountingInstrcutions\":\"\",\"OperationsInstructions\":\"\",\"Details\":{\"Dimensions\":{\"Length\":10,\"Width\":10,\"Height\":10,\"Unit\":\"cm\"},\"ActualWeight\":{\"Value\":2,\"Unit\":\"Kg\"},\"ProductGroup\":\"DOM\",\"ProductType\":\"ONP\",\"PaymentType\":\"P\",\"PaymentOptions\":\"\",\"Services\":\"CODS\",\"NumberOfPieces\":1,\"DescriptionOfGoods\":\"Money Collection\",\"GoodsOriginCountry\":\"Jo\",\"ChargeableWeight\":null,\"CashOnDeliveryAmount\":{\"CurrencyCode\":\"JOD\",\"Value\":15},\"InsuranceAmount\":{\"Value\":0,\"CurrencyCode\":\"\"},\"CollectAmount\":{\"Value\":0,\"CurrencyCode\":\"\"},\"CashAdditionalAmount\":{\"Value\":0,\"CurrencyCode\":\"\"},\"CashAdditionalAmountDescription\":\"\",\"CustomsValueAmount\":{\"Value\":0,\"CurrencyCode\":\"\"},\"Items\":[]}}],\"ClientInfo\":{\"AccountCountryCode\":\"JO\",\"AccountEntity\":\"AMM\",\"AccountNumber\":\"20016\",\"AccountPin\":\"543543\",\"UserName\":\"Nabily@aramex.com\",\"Password\":\"Aramex123$\",\"Version\":\"v1\",\"Source\":\"24\"},\"Transaction\":{\"Reference1\":\"126\",\"Reference2\":\"\",\"Reference3\":\"\",\"Reference4\":\"\",\"Reference5\":\"\"},\"LabelInfo\":{\"ReportID\":9729,\"ReportType\":\"URL\"}}', '{\"Transaction\":{\"Reference1\":\"126\",\"Reference2\":{},\"Reference3\":{},\"Reference4\":{},\"Reference5\":{}},\"Notifications\":{},\"HasErrors\":\"false\",\"Shipments\":{\"ProcessedShipment\":{\"ID\":\"45267946986\",\"Reference1\":\"Shpt 0001\",\"Reference2\":{},\"Reference3\":{},\"ForeignHAWB\":{},\"HasErrors\":\"false\",\"Notifications\":{},\"ShipmentLabel\":{\"LabelURL\":\"https:\\/\\/ws.aramex.net\\/content\\/rpt_cache\\/82d63d9270904c4bbd20105206117b62.pdf\",\"LabelFileContents\":{}},\"ShipmentDetails\":{\"Origin\":\"AMM\",\"Destination\":{},\"ChargeableWeight\":{\"Unit\":\"KG\",\"Value\":\"2\"},\"DescriptionOfGoods\":\"Money Collection\",\"GoodsOriginCountry\":\"Jo\",\"NumberOfPieces\":\"1\",\"ProductGroup\":\"DOM\",\"ProductType\":\"ONP\",\"PaymentType\":\"P\",\"PaymentOptions\":{},\"CustomsValueAmount\":{\"CurrencyCode\":{},\"Value\":\"0\"},\"CashOnDeliveryAmount\":{\"CurrencyCode\":\"JOD\",\"Value\":\"15\"},\"InsuranceAmount\":{\"CurrencyCode\":{},\"Value\":\"0\"},\"CashAdditionalAmount\":{\"CurrencyCode\":{},\"Value\":\"0\"},\"CollectAmount\":{\"CurrencyCode\":{},\"Value\":\"0\"},\"Services\":\"CODS\"},\"ShipmentAttachments\":{}}}}'),
(127, 1, 0, 517.500, 0.00, 4, '2020-06-03 03:15:31', '2020-06-03 03:54:52', NULL, 'ketul patel', 'user@user.com', 'vadodara', 'vadodara', 'vadodara3', '789456789456', 'JO', 'jordan', '11190', '1234567895', 'Jordan', NULL, NULL, '45267728004', 'https://ws.aramex.net/content/rpt_cache/568196063cc74f4f93f2ee544bee1b18.pdf', NULL, 'cod', 'paid', NULL, '0.00', '{\"ClientInfo\":{\"AccountCountryCode\":\"JO\",\"AccountEntity\":\"AMM\",\"AccountNumber\":\"20016\",\"AccountPin\":\"543543\",\"UserName\":\"Nabily@aramex.com\",\"Password\":\"Aramex123$\",\"Version\":\"v1\"},\"Transaction\":{\"Reference1\":\"127\",\"Reference2\":\"\",\"Reference3\":\"\",\"Reference4\":\"\"},\"LabelInfo\":{\"ReportID\":9201,\"ReportType\":\"URL\"},\"Shipments\":{\"Shipment\":{\"Shipper\":{\"Reference1\":\"some ref\",\"AccountNumber\":\"20016\",\"Contact\":{\"PersonName\":\"Moustafa2\",\"CompanyName\":\"Eureka Tech Academy\",\"PhoneNumber1\":\"+96399475326\",\"CellPhone\":\"+963115489872\",\"EmailAddress\":\"test@email.com\"},\"PartyAddress\":{\"Line1\":\"test address line\",\"Line2\":\"address line2\",\"Line3\":null,\"City\":\"Amman\",\"StateOrProvinceCode\":null,\"PostCode\":null,\"CountryCode\":\"JO\",\"Longitude\":\"0\",\"Latitude\":\"0\"}},\"Consignee\":{\"Reference1\":\"Ref 333333\",\"Reference2\":\"Ref 444444\",\"AccountNumber\":\"20016\",\"Contact\":{\"PersonName\":\"Admin\",\"CompanyName\":\"Admin\",\"PhoneNumber1\":\"1234567895\",\"CellPhone\":\"+963000000000\",\"EmailAddress\":\"user@user.com\"},\"PartyAddress\":{\"Line1\":\"vadodara\",\"Line2\":\"vadodara\",\"Line3\":\"vadodara3\",\"CountryCode\":\"JO\",\"City\":\"Jordan\",\"PostCode\":\"11190\"}},\"ShippingDateTime\":1591455109,\"DueDate\":null,\"Comments\":null,\"PickupLocation\":null,\"Attachments\":null,\"ForeignHAWB\":null,\"TransportType\":\"0\",\"PickupGUID\":null,\"Number\":null,\"Details\":{\"ActualWeight\":{\"Value\":2,\"Unit\":\"Kg\"},\"ProductGroup\":\"DOM\",\"ProductType\":\"OND\",\"PaymentType\":\"C\",\"PaymentOptions\":\"ARCC\",\"NumberOfPieces\":1,\"DescriptionOfGoods\":\"Box of Electronic tools and stuffs\",\"GoodsOriginCountry\":\"Jo\",\"CollectAmount\":{\"Value\":0,\"CurrencyCode\":\"JOD\"},\"Items\":{\"PackageType\":\"Box\",\"Quantity\":1,\"Weight\":{\"Value\":2,\"Unit\":\"Kg\"},\"Comments\":\"Electronic tools and stuffs\",\"Reference\":\"\"},\"Services\":\"CODS\",\"CashOnDeliveryAmount\":{\"CurrencyCode\":\"USD\",\"Value\":3.602}}}},\"Pickup\":{\"Reference1\":\"123123\",\"PickupLocation\":\"here\",\"Status\":\"Pending\",\"PickupDate\":1591358400,\"ReadyTime\":1591358400,\"LastPickupTime\":1591455109,\"ClosingTime\":1591461524,\"PickupContact\":{\"PersonName\":\"Moustafa2\",\"CompanyName\":\"Eureka Tech Academy\",\"PhoneNumber1\":\"+96399475326\",\"CellPhone\":\"+963115489872\",\"EmailAddress\":\"test@email.com\"},\"PickupAddress\":{\"Line1\":\"test address line\",\"Line2\":\"address line2\",\"Line3\":null,\"CountryCode\":\"JO\",\"City\":\"Amman\"},\"PickupItems\":{\"PickupItemDetail\":{\"Reference1\":\"123123\",\"ProductGroup\":\"EXP\",\"Payment\":\"C\",\"ProductType\":\"EPX\",\"NumberOfPieces\":\"1\",\"ShipmentWeight\":{\"Value\":\"2\",\"Unit\":\"Kg\"},\"NumberOfShipments\":1,\"ShipmentVolume\":{\"Value\":\"50\",\"Unit\":\"Cm3\"}}}}}', '{\"Transaction\":{\"Reference1\":\"127\",\"Reference2\":\"\",\"Reference3\":\"\",\"Reference4\":\"\",\"Reference5\":null},\"Notifications\":{},\"HasErrors\":false,\"Shipments\":{\"ProcessedShipment\":{\"ID\":\"45267728004\",\"Reference1\":null,\"Reference2\":null,\"Reference3\":null,\"ForeignHAWB\":null,\"HasErrors\":false,\"Notifications\":{},\"ShipmentLabel\":{\"LabelURL\":\"https:\\/\\/ws.aramex.net\\/content\\/rpt_cache\\/568196063cc74f4f93f2ee544bee1b18.pdf\",\"LabelFileContents\":\"\"}}}}'),
(128, 1, 0, 230.000, 0.00, 4, '2020-06-03 03:57:41', '2020-06-03 03:59:00', NULL, 'ketul patel', 'ketulpatel@webmynesystems.com', 'vadodara', 'vadodara', 'vadodara3', '789456789456', 'JO', 'jordan', '11190', '7894567896', 'Jordan', NULL, NULL, '45267728074', 'https://ws.aramex.net/content/rpt_cache/bd9e180f43f84ba1a6da90f103664444.pdf', NULL, 'cod', 'unpaid', NULL, '0.00', '', ''),
(129, 11, 0, 15.000, 0.00, 1, '2020-06-04 00:14:31', '2020-06-04 00:14:31', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'laxmipura', 'gorwa', '09998972498', 'AS', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'unpaid', NULL, '0.00', NULL, NULL),
(130, 11, 0, 522.500, 0.00, 1, '2020-06-09 23:27:58', '2020-06-09 23:27:58', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'gorwa', NULL, '09998972498', 'AS', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'cod', 'pending', 'BHARAT', '10.00', NULL, NULL),
(131, 11, 0, 235.000, 0.00, 1, '2020-06-09 23:30:46', '2020-06-09 23:31:43', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'gorwa', NULL, '09998972498', 'AS', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'unpaid', 'BHARAT', '10.00', NULL, NULL),
(132, 11, 0, 230.000, 0.00, 1, '2020-06-09 23:34:33', '2020-06-09 23:35:56', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'rtrg', NULL, '09998972498', 'AS', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'unpaid', NULL, '0.00', NULL, NULL),
(133, 11, 0, 230.000, 0.00, 1, '2020-06-09 23:37:47', '2020-06-09 23:38:50', NULL, 'sid r raj', 'sid@gmail.com', 'vadodara', 'ddd', NULL, '09998972498', 'DE', 'Gujarat', '390016', NULL, 'vadaodara', NULL, NULL, NULL, NULL, NULL, 'card', 'unpaid', NULL, '0.00', NULL, NULL),
(134, 1, 0, 517.500, 0.00, 4, '2020-06-10 01:42:00', '2020-06-10 06:38:17', NULL, 'ketul patel', 'ketulpatel@webmynesystems.com', 'Irbid', 'Irbid', 'Irbid', '789456789456', 'JO', 'Irbid', '11190', '456789123456', 'Irbid', NULL, NULL, '45267946776', 'https://ws.aramex.net/content/rpt_cache/09fa2604fba346c9b790475bdccf077f.pdf', NULL, 'cod', 'unpaid', NULL, '0.00', '{\"Shipments\":[{\"Shipper\":{\"Reference1\":\"Ref 111111\",\"Reference2\":\"Ref 222222\",\"AccountNumber\":\"20016\",\"PartyAddress\":{\"Line1\":\"test address line\",\"Line2\":\"address line2\",\"Line3\":null,\"City\":\"Amman\",\"StateOrProvinceCode\":null,\"PostCode\":null,\"CountryCode\":\"JO\"},\"Contact\":{\"Department\":\"\",\"PersonName\":\"Moustafa2\",\"Title\":\"\",\"CompanyName\":\"Aramex\",\"PhoneNumber1\":\"+96399475326\",\"PhoneNumber1Ext\":\"125\",\"PhoneNumber2\":\"7777777\",\"PhoneNumber2Ext\":\"123\",\"FaxNumber\":\"\",\"CellPhone\":\"+963115489872\",\"EmailAddress\":\"test@email.com\",\"Type\":\"\"}},\"Consignee\":{\"Reference1\":\"Ref 333333\",\"Reference2\":\"Ref 444444\",\"AccountNumber\":\"20016\",\"PartyAddress\":{\"Line1\":\"Irbid\",\"Line2\":\"Irbid\",\"Line3\":\"Irbid\",\"City\":\"Irbid\",\"StateOrProvinceCode\":\"\",\"PostCode\":\"11190\",\"CountryCode\":\"JO\"},\"Contact\":{\"Department\":\"\",\"PersonName\":\"Admin\",\"Title\":\"\",\"CompanyName\":\"Admin\",\"PhoneNumber1\":\"456789123456\",\"PhoneNumber1Ext\":\"155\",\"PhoneNumber2\":\"123456\",\"PhoneNumber2Ext\":\"123\",\"FaxNumber\":\"\",\"CellPhone\":\"+963000000000\",\"EmailAddress\":\"user@user.com\",\"Type\":\"\"}},\"ThirdParty\":{\"Reference1\":\"\",\"Reference2\":\"\",\"AccountNumber\":\"\",\"PartyAddress\":{\"Line1\":\"\",\"Line2\":\"\",\"Line3\":\"\",\"City\":\"\",\"StateOrProvinceCode\":\"\",\"PostCode\":\"\",\"CountryCode\":\"\"},\"Contact\":{\"Department\":\"\",\"PersonName\":\"\",\"Title\":\"\",\"CompanyName\":\"\",\"PhoneNumber1\":\"\",\"PhoneNumber1Ext\":\"\",\"PhoneNumber2\":\"\",\"PhoneNumber2Ext\":\"\",\"FaxNumber\":\"\",\"CellPhone\":\"\",\"EmailAddress\":\"\",\"Type\":\"\"}},\"Reference1\":\"Shpt 0001\",\"Reference2\":\"\",\"Reference3\":\"\",\"ForeignHAWB\":\"\",\"TransportType\":0,\"ShippingDateTime\":\"\\/Date(1591876800000)\\/\",\"DueDate\":\"\\/Date(1591877090127)\\/\",\"PickupLocation\":\"\",\"PickupGUID\":\"\",\"Comments\":\"Shpt 0001\",\"AccountingInstrcutions\":\"\",\"OperationsInstructions\":\"\",\"Details\":{\"Dimensions\":{\"Length\":10,\"Width\":10,\"Height\":10,\"Unit\":\"cm\"},\"ActualWeight\":{\"Value\":2,\"Unit\":\"Kg\"},\"ProductGroup\":\"DOM\",\"ProductType\":\"ONP\",\"PaymentType\":\"P\",\"PaymentOptions\":\"ARCC\",\"Services\":\"CODS\",\"NumberOfPieces\":1,\"DescriptionOfGoods\":\"Box of Electronic tools and stuffs\",\"GoodsOriginCountry\":\"Jo\",\"ChargeableWeight\":null,\"CashOnDeliveryAmount\":{\"CurrencyCode\":\"JOD\",\"Value\":517.5},\"InsuranceAmount\":{\"Value\":0,\"CurrencyCode\":\"\"},\"CollectAmount\":{\"Value\":0,\"CurrencyCode\":\"JOD\"},\"CashAdditionalAmount\":{\"Value\":0,\"CurrencyCode\":\"\"},\"CashAdditionalAmountDescription\":\"\",\"CustomsValueAmount\":{\"Value\":0,\"CurrencyCode\":\"\"},\"Items\":{\"Weight\":{\"Value\":2}}}}],\"ClientInfo\":{\"AccountCountryCode\":\"JO\",\"AccountEntity\":\"AMM\",\"AccountNumber\":\"20016\",\"AccountPin\":\"543543\",\"UserName\":\"Nabily@aramex.com\",\"Password\":\"Aramex123$\",\"Version\":\"v1\"},\"Transaction\":{\"Reference1\":\"134\",\"Reference2\":\"\",\"Reference3\":\"\",\"Reference4\":\"\",\"Reference5\":\"\"},\"LabelInfo\":{\"ReportID\":9729,\"ReportType\":\"URL\"},\"Pickup\":{\"Reference1\":\"123123\",\"PickupLocation\":\"here\",\"Status\":\"Pending\",\"Comments\":\"\",\"Vehicle\":\"\",\"PickupDate\":\"\\/Date(1591877290000)\\/\",\"ReadyTime\":\"\\/Date(1591876896709)\\/\",\"LastPickupTime\":\"\\/Date(1591876896709)\\/\",\"ClosingTime\":\"\\/Date(1591876903124)\\/\",\"PickupContact\":{\"PersonName\":\"Moustafa2\",\"CompanyName\":\"Eureka Tech Academy\",\"PhoneNumber1\":\"+96399475326\",\"PhoneNumber2\":\"\",\"CellPhone\":\"+963115489872\",\"EmailAddress\":\"test@email.com\",\"Type\":\"\"},\"PickupAddress\":{\"Line1\":\"test address line\",\"Line2\":\"address line2\",\"Line3\":null,\"CountryCode\":\"JO\",\"City\":\"Amman\",\"PostCode\":null},\"PickupItems\":[{\"Reference1\":\"123123\",\"ProductGroup\":\"EXP\",\"Payment\":\"C\",\"ProductType\":\"EPX\",\"NumberOfPieces\":\"1\",\"PackageType\":\"\",\"ShipmentWeight\":{\"Value\":\"2\",\"Unit\":\"Kg\"},\"NumberOfShipments\":1,\"ShipmentVolume\":{\"Value\":\"50\",\"Unit\":\"Cm3\"},\"CashAmount\":null,\"ExtraCharges\":null,\"ShipmentDimensions\":{\"Length\":0,\"Width\":0,\"Height\":0,\"Unit\":\"cm\"},\"Comments\":\"\"}]}}', '{\"Transaction\":{\"Reference1\":\"134\",\"Reference2\":{},\"Reference3\":{},\"Reference4\":{},\"Reference5\":{}},\"Notifications\":{},\"HasErrors\":\"false\",\"Shipments\":{\"ProcessedShipment\":{\"ID\":\"45267946776\",\"Reference1\":\"Shpt 0001\",\"Reference2\":{},\"Reference3\":{},\"ForeignHAWB\":{},\"HasErrors\":\"false\",\"Notifications\":{},\"ShipmentLabel\":{\"LabelURL\":\"https:\\/\\/ws.aramex.net\\/content\\/rpt_cache\\/09fa2604fba346c9b790475bdccf077f.pdf\",\"LabelFileContents\":{}},\"ShipmentDetails\":{\"Origin\":\"AMM\",\"Destination\":\"AMM\",\"ChargeableWeight\":{\"Unit\":\"KG\",\"Value\":\"2\"},\"DescriptionOfGoods\":\"Box of Electronic tools and stuffs\",\"GoodsOriginCountry\":\"Jo\",\"NumberOfPieces\":\"1\",\"ProductGroup\":\"DOM\",\"ProductType\":\"ONP\",\"PaymentType\":\"P\",\"PaymentOptions\":\"ARCC\",\"CustomsValueAmount\":{\"CurrencyCode\":{},\"Value\":\"0\"},\"CashOnDeliveryAmount\":{\"CurrencyCode\":\"JOD\",\"Value\":\"517.5\"},\"InsuranceAmount\":{\"CurrencyCode\":{},\"Value\":\"0\"},\"CashAdditionalAmount\":{\"CurrencyCode\":{},\"Value\":\"0\"},\"CollectAmount\":{\"CurrencyCode\":\"JOD\",\"Value\":\"0\"},\"Services\":\"CODS\"},\"ShipmentAttachments\":{}}}}'),
(135, 1, 0, 747.500, 0.00, 4, '2020-06-11 01:22:00', '2020-06-11 06:43:14', NULL, 'ketul patel', 'ketulpatel@webmynesystems.com', 'Irbid', 'Irbid', 'Irbid', '789456789456', 'JO', 'Irbid', '11190', '123456789', 'Irbid', NULL, NULL, '45267971766', 'https://ws.aramex.net/content/rpt_cache/d0cd50293080402a83168d61f7ba4f28.pdf', NULL, 'cod', 'unpaid', NULL, '0.00', '{\"Shipments\":[{\"Shipper\":{\"Reference1\":\"Ref 111111\",\"Reference2\":\"Ref 222222\",\"AccountNumber\":\"20016\",\"PartyAddress\":{\"Line1\":\"test address line\",\"Line2\":\"address line2\",\"Line3\":null,\"City\":\"Amman\",\"StateOrProvinceCode\":null,\"PostCode\":null,\"CountryCode\":\"JO\"},\"Contact\":{\"Department\":\"\",\"PersonName\":\"Moustafa2\",\"Title\":\"\",\"CompanyName\":\"Aramex\",\"PhoneNumber1\":\"+96399475326\",\"PhoneNumber1Ext\":\"125\",\"PhoneNumber2\":\"7777777\",\"PhoneNumber2Ext\":\"123\",\"FaxNumber\":\"\",\"CellPhone\":\"+963115489872\",\"EmailAddress\":\"test@email.com\",\"Type\":\"\"}},\"Consignee\":{\"Reference1\":\"Ref 333333\",\"Reference2\":\"Ref 444444\",\"AccountNumber\":\"20016\",\"PartyAddress\":{\"Line1\":\"Irbid\",\"Line2\":\"Irbid\",\"Line3\":\"Irbid\",\"City\":\"Irbid\",\"StateOrProvinceCode\":\"\",\"PostCode\":\"11190\",\"CountryCode\":\"JO\"},\"Contact\":{\"Department\":\"\",\"PersonName\":\"Admin\",\"Title\":\"\",\"CompanyName\":\"Admin\",\"PhoneNumber1\":\"123456789\",\"PhoneNumber1Ext\":\"155\",\"PhoneNumber2\":\"123456\",\"PhoneNumber2Ext\":\"123\",\"FaxNumber\":\"\",\"CellPhone\":\"+963000000000\",\"EmailAddress\":\"user@user.com\",\"Type\":\"\"}},\"ThirdParty\":{\"Reference1\":\"\",\"Reference2\":\"\",\"AccountNumber\":\"\",\"PartyAddress\":{\"Line1\":\"\",\"Line2\":\"\",\"Line3\":\"\",\"City\":\"\",\"StateOrProvinceCode\":\"\",\"PostCode\":\"\",\"CountryCode\":\"\"},\"Contact\":{\"Department\":\"\",\"PersonName\":\"\",\"Title\":\"\",\"CompanyName\":\"\",\"PhoneNumber1\":\"\",\"PhoneNumber1Ext\":\"\",\"PhoneNumber2\":\"\",\"PhoneNumber2Ext\":\"\",\"FaxNumber\":\"\",\"CellPhone\":\"\",\"EmailAddress\":\"\",\"Type\":\"\"}},\"Reference1\":\"Shpt 0001\",\"Reference2\":\"\",\"Reference3\":\"\",\"ForeignHAWB\":\"\",\"TransportType\":0,\"ShippingDateTime\":\"\\/Date(1591963200000)\\/\",\"DueDate\":\"\\/Date(1591963490127)\\/\",\"PickupLocation\":\"\",\"PickupGUID\":\"\",\"Comments\":\"Shpt 0001\",\"AccountingInstrcutions\":\"\",\"OperationsInstructions\":\"\",\"Details\":{\"Dimensions\":{\"Length\":10,\"Width\":10,\"Height\":10,\"Unit\":\"cm\"},\"ActualWeight\":{\"Value\":2,\"Unit\":\"Kg\"},\"ProductGroup\":\"DOM\",\"ProductType\":\"ONP\",\"PaymentType\":\"P\",\"PaymentOptions\":\"ACCT\",\"Services\":\"CODS\",\"NumberOfPieces\":1,\"DescriptionOfGoods\":\"Box of Electronic tools and stuffs\",\"GoodsOriginCountry\":\"Jo\",\"ChargeableWeight\":null,\"CashOnDeliveryAmount\":{\"CurrencyCode\":\"JOD\",\"Value\":747.5},\"InsuranceAmount\":{\"Value\":0,\"CurrencyCode\":\"\"},\"CollectAmount\":{\"Value\":0,\"CurrencyCode\":\"JOD\"},\"CashAdditionalAmount\":{\"Value\":0,\"CurrencyCode\":\"\"},\"CashAdditionalAmountDescription\":\"\",\"CustomsValueAmount\":{\"Value\":0,\"CurrencyCode\":\"\"},\"Items\":{\"Weight\":{\"Value\":2}}}}],\"ClientInfo\":{\"AccountCountryCode\":\"JO\",\"AccountEntity\":\"AMM\",\"AccountNumber\":\"20016\",\"AccountPin\":\"543543\",\"UserName\":\"Nabily@aramex.com\",\"Password\":\"Aramex123$\",\"Version\":\"v1\"},\"Transaction\":{\"Reference1\":\"135\",\"Reference2\":\"\",\"Reference3\":\"\",\"Reference4\":\"\",\"Reference5\":\"\"},\"LabelInfo\":{\"ReportID\":9729,\"ReportType\":\"URL\"},\"Pickup\":{\"Reference1\":\"123123\",\"PickupLocation\":\"here\",\"Status\":\"Pending\",\"Comments\":\"\",\"Vehicle\":\"\",\"PickupDate\":\"\\/Date(1591963988000)\\/\",\"ReadyTime\":\"\\/Date(1591963296709)\\/\",\"LastPickupTime\":\"\\/Date(1591963296709)\\/\",\"ClosingTime\":\"\\/Date(1591963303124)\\/\",\"PickupContact\":{\"PersonName\":\"Moustafa2\",\"CompanyName\":\"Eureka Tech Academy\",\"PhoneNumber1\":\"+96399475326\",\"PhoneNumber2\":\"\",\"CellPhone\":\"+963115489872\",\"EmailAddress\":\"test@email.com\",\"Type\":\"\"},\"PickupAddress\":{\"Line1\":\"test address line\",\"Line2\":\"address line2\",\"Line3\":null,\"CountryCode\":\"JO\",\"City\":\"Amman\",\"PostCode\":null},\"PickupItems\":[{\"Reference1\":\"123123\",\"ProductGroup\":\"EXP\",\"Payment\":\"C\",\"ProductType\":\"EPX\",\"NumberOfPieces\":\"1\",\"PackageType\":\"\",\"ShipmentWeight\":{\"Value\":\"2\",\"Unit\":\"Kg\"},\"NumberOfShipments\":1,\"ShipmentVolume\":{\"Value\":\"50\",\"Unit\":\"Cm3\"},\"CashAmount\":null,\"ExtraCharges\":null,\"ShipmentDimensions\":{\"Length\":0,\"Width\":0,\"Height\":0,\"Unit\":\"cm\"},\"Comments\":\"\"}]}}', '{\"Transaction\":{\"Reference1\":\"135\",\"Reference2\":{},\"Reference3\":{},\"Reference4\":{},\"Reference5\":{}},\"Notifications\":{},\"HasErrors\":\"false\",\"Shipments\":{\"ProcessedShipment\":{\"ID\":\"45267971766\",\"Reference1\":\"Shpt 0001\",\"Reference2\":{},\"Reference3\":{},\"ForeignHAWB\":{},\"HasErrors\":\"false\",\"Notifications\":{},\"ShipmentLabel\":{\"LabelURL\":\"https:\\/\\/ws.aramex.net\\/content\\/rpt_cache\\/d0cd50293080402a83168d61f7ba4f28.pdf\",\"LabelFileContents\":{}},\"ShipmentDetails\":{\"Origin\":\"AMM\",\"Destination\":\"AMM\",\"ChargeableWeight\":{\"Unit\":\"KG\",\"Value\":\"2\"},\"DescriptionOfGoods\":\"Box of Electronic tools and stuffs\",\"GoodsOriginCountry\":\"Jo\",\"NumberOfPieces\":\"1\",\"ProductGroup\":\"DOM\",\"ProductType\":\"ONP\",\"PaymentType\":\"P\",\"PaymentOptions\":\"ACCT\",\"CustomsValueAmount\":{\"CurrencyCode\":{},\"Value\":\"0\"},\"CashOnDeliveryAmount\":{\"CurrencyCode\":\"JOD\",\"Value\":\"747.5\"},\"InsuranceAmount\":{\"CurrencyCode\":{},\"Value\":\"0\"},\"CashAdditionalAmount\":{\"CurrencyCode\":{},\"Value\":\"0\"},\"CollectAmount\":{\"CurrencyCode\":\"JOD\",\"Value\":\"0\"},\"Services\":\"CODS\"},\"ShipmentAttachments\":{}}}}');

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `type` enum('Course','Kit','Item') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Item',
  `mainid` text COLLATE utf8mb4_unicode_ci,
  `supplier_id` int(11) DEFAULT NULL,
  `price` double(8,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `final_total` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `selling_price` double(8,2) NOT NULL,
  `packed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`id`, `order_id`, `item_id`, `type`, `mainid`, `supplier_id`, `price`, `qty`, `final_total`, `selling_price`, `packed`, `created_at`, `updated_at`) VALUES
(8, 17, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 1, '2018-10-22 07:52:40', '2019-05-18 00:52:50'),
(9, 17, 2, 'Item', '', 1, 5.00, 0, '', 9.00, 0, '2018-10-22 07:52:40', '2018-10-22 07:52:40'),
(10, 18, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 1, '2018-10-22 07:54:21', '2018-10-22 11:25:09'),
(11, 18, 2, 'Item', '', 1, 5.00, 0, '', 9.00, 0, '2018-10-22 07:54:21', '2018-10-22 07:54:21'),
(12, 18, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 1, '2018-10-22 07:54:21', '2018-10-22 11:25:07'),
(13, 19, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 1, '2018-10-31 14:48:55', '2019-05-18 00:52:46'),
(14, 19, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 1, '2018-10-31 14:48:55', '2019-05-17 01:33:30'),
(15, 19, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 1, '2018-10-31 14:48:55', '2019-05-17 01:33:08'),
(16, 20, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 1, '2019-03-07 10:26:04', '2019-05-17 01:32:05'),
(17, 20, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 1, '2019-03-07 10:26:04', '2019-05-17 01:08:52'),
(18, 20, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 1, '2019-03-07 10:26:04', '2019-05-06 23:39:38'),
(19, 20, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 1, '2019-03-07 10:26:04', '2019-05-17 01:08:52'),
(20, 21, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 00:57:23', '2019-05-19 00:57:23'),
(21, 21, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 00:57:23', '2019-05-19 00:57:23'),
(22, 21, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 00:57:23', '2019-05-19 00:57:23'),
(23, 21, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 00:57:23', '2019-05-19 00:57:23'),
(24, 21, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 00:57:23', '2019-05-19 00:57:23'),
(25, 21, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 00:57:23', '2019-05-19 00:57:23'),
(26, 21, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 00:57:23', '2019-05-19 00:57:23'),
(27, 21, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 00:57:23', '2019-05-19 00:57:23'),
(28, 21, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 00:57:23', '2019-05-19 00:57:23'),
(29, 21, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 00:57:23', '2019-05-19 00:57:23'),
(30, 21, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 00:57:23', '2019-05-19 00:57:23'),
(31, 21, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 00:57:23', '2019-05-19 00:57:23'),
(32, 21, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 00:57:23', '2019-05-19 00:57:23'),
(33, 22, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:00:45', '2019-05-19 01:00:45'),
(34, 22, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:00:45', '2019-05-19 01:00:45'),
(35, 22, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:00:45', '2019-05-19 01:00:45'),
(36, 22, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:00:45', '2019-05-19 01:00:45'),
(37, 22, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:00:45', '2019-05-19 01:00:45'),
(38, 22, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:00:45', '2019-05-19 01:00:45'),
(39, 22, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:00:45', '2019-05-19 01:00:45'),
(40, 22, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:00:45', '2019-05-19 01:00:45'),
(41, 22, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:00:45', '2019-05-19 01:00:45'),
(42, 22, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:00:45', '2019-05-19 01:00:45'),
(43, 22, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:00:45', '2019-05-19 01:00:45'),
(44, 22, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:00:45', '2019-05-19 01:00:45'),
(45, 22, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:00:45', '2019-05-19 01:00:45'),
(46, 23, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:01:34', '2019-05-19 01:01:34'),
(47, 23, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:01:34', '2019-05-19 01:01:34'),
(48, 23, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:01:34', '2019-05-19 01:01:34'),
(49, 23, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:01:34', '2019-05-19 01:01:34'),
(50, 23, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:01:35', '2019-05-19 01:01:35'),
(51, 23, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:01:35', '2019-05-19 01:01:35'),
(52, 23, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:01:35', '2019-05-19 01:01:35'),
(53, 23, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:01:35', '2019-05-19 01:01:35'),
(54, 23, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:01:35', '2019-05-19 01:01:35'),
(55, 23, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:01:35', '2019-05-19 01:01:35'),
(56, 23, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:01:35', '2019-05-19 01:01:35'),
(57, 23, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:01:35', '2019-05-19 01:01:35'),
(58, 23, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:01:35', '2019-05-19 01:01:35'),
(59, 24, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:22', '2019-05-19 01:02:22'),
(60, 24, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:22', '2019-05-19 01:02:22'),
(61, 24, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:22', '2019-05-19 01:02:22'),
(62, 24, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:22', '2019-05-19 01:02:22'),
(63, 24, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:22', '2019-05-19 01:02:22'),
(64, 24, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:22', '2019-05-19 01:02:22'),
(65, 24, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:22', '2019-05-19 01:02:22'),
(66, 24, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:22', '2019-05-19 01:02:22'),
(67, 24, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 1, '2019-05-19 01:02:22', '2020-05-15 00:50:48'),
(68, 24, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:22', '2019-05-19 01:02:22'),
(69, 24, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:22', '2019-05-19 01:02:22'),
(70, 24, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:02:22', '2019-05-19 01:02:22'),
(71, 24, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:02:22', '2019-05-19 01:02:22'),
(72, 25, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 1, '2019-05-19 01:02:36', '2020-05-15 01:16:13'),
(73, 25, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:36', '2019-05-19 01:02:36'),
(74, 25, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:36', '2019-05-19 01:02:36'),
(75, 25, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:36', '2019-05-19 01:02:36'),
(76, 25, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:36', '2019-05-19 01:02:36'),
(77, 25, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:37', '2019-05-19 01:02:37'),
(78, 25, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:37', '2019-05-19 01:02:37'),
(79, 25, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:37', '2019-05-19 01:02:37'),
(80, 25, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:02:37', '2019-05-19 01:02:37'),
(81, 25, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 1, '2019-05-19 01:02:37', '2020-05-15 00:40:35'),
(82, 25, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 1, '2019-05-19 01:02:37', '2020-05-15 00:40:40'),
(83, 25, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 1, '2019-05-19 01:02:37', '2020-05-15 00:40:43'),
(84, 25, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:02:37', '2019-05-19 01:02:37'),
(85, 26, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:03:19', '2019-05-19 01:03:19'),
(86, 26, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:03:19', '2019-05-19 01:03:19'),
(87, 26, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:03:19', '2019-05-19 01:03:19'),
(88, 27, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:04:36', '2019-05-19 01:04:36'),
(89, 27, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:04:36', '2019-05-19 01:04:36'),
(90, 27, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:04:36', '2019-05-19 01:04:36'),
(91, 28, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:05:32', '2019-05-19 01:05:32'),
(92, 28, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:05:32', '2019-05-19 01:05:32'),
(93, 28, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:05:32', '2019-05-19 01:05:32'),
(94, 29, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:06:17', '2019-05-19 01:06:17'),
(95, 29, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:06:17', '2019-05-19 01:06:17'),
(96, 29, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:06:17', '2019-05-19 01:06:17'),
(97, 30, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:06:44', '2019-05-19 01:06:44'),
(98, 30, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:06:44', '2019-05-19 01:06:44'),
(99, 30, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:06:44', '2019-05-19 01:06:44'),
(100, 31, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 0, '2019-05-19 01:07:12', '2019-05-19 01:07:12'),
(101, 31, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:07:12', '2019-05-19 01:07:12'),
(102, 31, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 0, '2019-05-19 01:07:12', '2019-05-19 01:07:12'),
(103, 32, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 1, '2019-05-19 01:07:31', '2020-04-07 06:19:54'),
(104, 32, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 1, '2019-05-19 01:07:31', '2020-04-07 06:19:41'),
(105, 32, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 1, '2019-05-19 01:07:31', '2020-04-07 06:20:08'),
(106, 33, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 1, '2019-05-19 01:10:58', '2020-04-07 06:14:36'),
(107, 33, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 1, '2019-05-19 01:10:58', '2019-09-23 21:44:37'),
(108, 33, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 1, '2019-05-19 01:10:58', '2019-10-27 10:08:22'),
(109, 34, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 1, '2019-05-30 22:57:31', '2019-09-23 21:44:31'),
(110, 34, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 1, '2019-05-30 22:57:31', '2019-09-23 21:41:37'),
(111, 34, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 1, '2019-05-30 22:57:31', '2019-08-27 17:50:41'),
(112, 35, 1, 'Item', '', 1, 12.00, 0, '', 12.00, 1, '2019-08-28 10:54:43', '2019-09-12 07:12:01'),
(113, 35, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 1, '2019-08-28 10:54:43', '2019-09-12 07:12:17'),
(114, 35, 2, 'Item', '', 1, 9.00, 0, '', 9.00, 1, '2019-08-28 10:54:43', '2019-09-12 07:12:19'),
(127, 67, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 3, '1552.50', 517.50, 1, '2020-05-12 06:42:11', '2020-05-26 08:16:41'),
(128, 67, 7, 'Item', NULL, 1, 100.00, 2, '345.00', 172.50, 1, '2020-05-12 06:42:11', '2020-05-26 08:16:19'),
(129, 67, 82, 'Course', NULL, NULL, 20.00, 1, '20.00', 20.00, 0, '2020-05-12 06:42:12', '2020-05-12 06:42:12'),
(130, 69, 18, 'Course', NULL, NULL, 80.00, 1, '80.00', 80.00, 0, '2020-05-13 04:41:51', '2020-05-13 04:41:51'),
(131, 69, 6, 'Kit', '[{\"item_id\":4},{\"item_id\":1},{\"item_id\":2},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-05-13 04:41:51', '2020-05-13 04:41:51'),
(132, 69, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-05-13 04:41:51', '2020-05-13 04:41:51'),
(133, 70, 18, 'Course', NULL, NULL, 80.00, 1, '80.00', 80.00, 0, '2020-05-13 05:09:01', '2020-05-13 05:09:01'),
(134, 70, 6, 'Kit', '[{\"item_id\":4},{\"item_id\":1},{\"item_id\":2},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-05-13 05:09:01', '2020-05-13 05:09:01'),
(135, 70, 21, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-13 05:09:01', '2020-05-13 05:09:01'),
(136, 71, 18, 'Course', NULL, NULL, 80.00, 1, '80.00', 80.00, 0, '2020-05-13 05:18:37', '2020-05-13 05:18:37'),
(137, 71, 6, 'Kit', '[{\"item_id\":4},{\"item_id\":1},{\"item_id\":2},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-05-13 05:18:38', '2020-05-13 05:18:38'),
(138, 72, 18, 'Course', NULL, NULL, 80.00, 1, '80.00', 80.00, 0, '2020-05-13 05:38:46', '2020-05-13 05:38:46'),
(139, 72, 6, 'Kit', '[{\"item_id\":4},{\"item_id\":1},{\"item_id\":2},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-05-13 05:38:47', '2020-05-13 05:38:47'),
(140, 73, 18, 'Course', NULL, NULL, 80.00, 1, '80.00', 80.00, 0, '2020-05-13 05:48:37', '2020-05-13 05:48:37'),
(141, 74, 18, 'Course', NULL, NULL, 80.00, 1, '80.00', 80.00, 0, '2020-05-13 06:52:11', '2020-05-13 06:52:11'),
(142, 75, 18, 'Course', NULL, NULL, 80.00, 1, '80.00', 80.00, 0, '2020-05-13 07:48:44', '2020-05-13 07:48:44'),
(143, 76, 18, 'Course', NULL, NULL, 80.00, 1, '80.00', 80.00, 0, '2020-05-14 00:56:12', '2020-05-14 00:56:12'),
(144, 77, 22, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-14 01:07:05', '2020-05-14 01:07:05'),
(145, 77, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-05-14 01:07:05', '2020-05-14 01:07:05'),
(146, 78, 17, 'Course', NULL, NULL, 25.00, 1, '25.00', 25.00, 0, '2020-05-14 01:15:39', '2020-05-14 01:15:39'),
(147, 78, 10, 'Kit', '[{\"item_id\":2},{\"item_id\":3},{\"item_id\":1},{\"item_id\":4}]', 1, 517.50, 1, '517.50', 517.50, 1, '2020-05-14 01:15:39', '2020-05-14 04:28:51'),
(148, 79, 80, 'Course', NULL, NULL, 7.00, 1, '7.00', 7.00, 0, '2020-05-14 01:19:15', '2020-05-14 01:19:15'),
(149, 80, 23, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-14 05:03:36', '2020-05-14 05:03:36'),
(150, 80, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-05-14 05:03:36', '2020-05-14 05:03:36'),
(151, 81, 24, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-14 07:07:39', '2020-05-14 07:07:39'),
(152, 81, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 1, '2020-05-14 07:07:39', '2020-05-15 04:55:14'),
(153, 82, 19, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-14 07:25:07', '2020-05-14 07:25:07'),
(154, 83, 21, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-14 07:26:35', '2020-05-14 07:26:35'),
(155, 84, 23, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-15 07:38:55', '2020-05-15 07:38:55'),
(156, 84, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 2, '1035.00', 517.50, 0, '2020-05-15 07:38:55', '2020-05-15 07:38:55'),
(157, 85, 18, 'Course', NULL, NULL, 80.00, 1, '80.00', 80.00, 0, '2020-05-15 07:54:08', '2020-05-15 07:54:08'),
(158, 86, 18, 'Course', NULL, NULL, 80.00, 1, '80.00', 80.00, 0, '2020-05-15 08:01:48', '2020-05-15 08:01:48'),
(159, 87, 23, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-15 08:07:17', '2020-05-15 08:07:17'),
(160, 88, 25, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-15 08:23:17', '2020-05-15 08:23:17'),
(161, 91, 6, 'Kit', '[{\"item_id\":4},{\"item_id\":1},{\"item_id\":2},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-05-19 03:41:36', '2020-05-19 03:41:36'),
(162, 91, 21, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-19 03:41:36', '2020-05-19 03:41:36'),
(163, 92, 6, 'Kit', '[{\"item_id\":4},{\"item_id\":1},{\"item_id\":2},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-05-19 03:46:16', '2020-05-19 03:46:16'),
(164, 95, 11, 'Item', NULL, 1, 100.00, 2, '230.00', 115.00, 0, '2020-05-19 04:12:13', '2020-05-19 04:12:13'),
(165, 96, 11, 'Item', NULL, 1, 100.00, 2, '230.00', 115.00, 0, '2020-05-19 04:13:09', '2020-05-19 04:13:09'),
(166, 97, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-05-21 08:19:13', '2020-05-21 08:19:13'),
(167, 97, 10, 'Item', NULL, 1, 100.00, 1, '115.00', 115.00, 0, '2020-05-21 08:19:13', '2020-05-21 08:19:13'),
(168, 98, 5, 'Kit', '[{\"item_id\":2},{\"item_id\":4}]', 1, 230.00, 1, '230.00', 230.00, 0, '2020-05-22 05:52:18', '2020-05-22 05:52:18'),
(169, 98, 6, 'Kit', '[{\"item_id\":4},{\"item_id\":1},{\"item_id\":2},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-05-22 05:52:18', '2020-05-22 05:52:18'),
(170, 99, 6, 'Kit', '[{\"item_id\":4},{\"item_id\":1},{\"item_id\":2},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-05-22 07:41:50', '2020-05-22 07:41:50'),
(171, 99, 26, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-22 07:41:50', '2020-05-22 07:41:50'),
(172, 100, 26, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-22 07:45:59', '2020-05-22 07:45:59'),
(173, 101, 5, 'Kit', '[{\"item_id\":2},{\"item_id\":4}]', 1, 230.00, 1, '230.00', 230.00, 1, '2020-05-22 07:57:58', '2020-05-27 01:44:34'),
(174, 101, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 1, '2020-05-22 07:57:58', '2020-05-27 01:44:41'),
(175, 102, 19, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-22 08:04:05', '2020-05-22 08:04:05'),
(176, 103, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-05-22 08:25:14', '2020-05-22 08:25:14'),
(177, 104, 6, 'Kit', '[{\"item_id\":4},{\"item_id\":1},{\"item_id\":2},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-05-22 08:48:13', '2020-05-22 08:48:13'),
(178, 105, 21, 'Item', NULL, 1, 150.00, 1, '172.50', 172.50, 0, '2020-05-25 00:15:19', '2020-05-25 00:15:19'),
(179, 106, 11, 'Item', NULL, 1, 100.00, 1, '115.00', 115.00, 0, '2020-05-25 00:20:10', '2020-05-25 00:20:10'),
(180, 107, 5, 'Kit', '[{\"item_id\":2},{\"item_id\":4}]', 1, 230.00, 3, '690.00', 230.00, 0, '2020-05-25 00:55:20', '2020-05-25 00:55:20'),
(181, 108, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-05-25 01:24:07', '2020-05-25 01:24:07'),
(182, 109, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 1, '2020-05-25 02:13:17', '2020-05-26 23:24:53'),
(183, 110, 5, 'Kit', '[{\"item_id\":2},{\"item_id\":4}]', 1, 230.00, 1, '230.00', 230.00, 0, '2020-05-25 03:58:51', '2020-05-25 03:58:51'),
(184, 111, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-05-26 00:18:52', '2020-05-26 00:18:52'),
(185, 111, 8, 'Item', NULL, 1, 100.00, 2, '230.00', 115.00, 0, '2020-05-26 00:18:52', '2020-05-26 00:18:52'),
(186, 112, 21, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-26 01:33:01', '2020-05-26 01:33:01'),
(187, 113, 22, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-26 01:37:19', '2020-05-26 01:37:19'),
(188, 114, 23, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-26 01:49:02', '2020-05-26 01:49:02'),
(189, 115, 23, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-26 03:57:50', '2020-05-26 03:57:50'),
(190, 116, 23, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-26 04:19:47', '2020-05-26 04:19:47'),
(191, 117, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 1, '2020-05-26 04:39:54', '2020-06-09 00:42:02'),
(192, 118, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 1, '2020-05-27 01:01:53', '2020-05-27 01:07:55'),
(193, 101, 23, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-26 04:19:47', '2020-05-26 04:19:47'),
(194, 119, 26, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-28 00:24:11', '2020-05-28 00:24:11'),
(195, 120, 24, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-28 00:43:05', '2020-05-28 00:43:05'),
(196, 121, 25, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-28 00:48:38', '2020-05-28 00:48:38'),
(197, 122, 24, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-05-28 01:01:32', '2020-05-28 01:01:32'),
(198, 123, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 15, '7762.50', 517.50, 1, '2020-06-02 23:53:19', '2020-06-02 23:55:06'),
(199, 123, 10, 'Item', NULL, 1, 100.00, 1, '115.00', 115.00, 1, '2020-06-02 23:53:19', '2020-06-02 23:55:11'),
(200, 124, 5, 'Kit', '[{\"item_id\":2},{\"item_id\":4}]', 1, 230.00, 1, '230.00', 230.00, 0, '2020-06-03 01:49:53', '2020-06-03 01:49:53'),
(201, 125, 24, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-06-03 01:54:23', '2020-06-03 01:54:23'),
(202, 126, 24, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-06-03 01:56:10', '2020-06-03 01:56:10'),
(203, 127, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 1, '2020-06-03 03:15:31', '2020-06-03 03:16:55'),
(204, 128, 5, 'Kit', '[{\"item_id\":2},{\"item_id\":4}]', 1, 230.00, 1, '230.00', 230.00, 1, '2020-06-03 03:57:41', '2020-06-03 03:58:40'),
(205, 129, 25, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-06-04 00:14:31', '2020-06-04 00:14:31'),
(206, 130, 25, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-06-09 23:27:58', '2020-06-09 23:27:58'),
(207, 130, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 0, '2020-06-09 23:27:58', '2020-06-09 23:27:58'),
(208, 131, 26, 'Course', NULL, NULL, 15.00, 1, '15.00', 15.00, 0, '2020-06-09 23:30:46', '2020-06-09 23:30:46'),
(209, 131, 5, 'Kit', '[{\"item_id\":2},{\"item_id\":4}]', 1, 230.00, 1, '230.00', 230.00, 0, '2020-06-09 23:30:47', '2020-06-09 23:30:47'),
(210, 132, 5, 'Kit', '[{\"item_id\":2},{\"item_id\":4}]', 1, 230.00, 1, '230.00', 230.00, 0, '2020-06-09 23:34:33', '2020-06-09 23:34:33'),
(211, 133, 5, 'Kit', '[{\"item_id\":2},{\"item_id\":4}]', 1, 230.00, 1, '230.00', 230.00, 0, '2020-06-09 23:37:47', '2020-06-09 23:37:47'),
(212, 134, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 1, '2020-06-10 01:42:00', '2020-06-10 01:43:37'),
(213, 135, 4, 'Kit', '[{\"item_id\":1},{\"item_id\":2},{\"item_id\":4},{\"item_id\":3}]', 1, 517.50, 1, '517.50', 517.50, 1, '2020-06-11 01:22:00', '2020-06-11 01:23:04'),
(214, 135, 5, 'Kit', '[{\"item_id\":2},{\"item_id\":4}]', 1, 230.00, 1, '230.00', 230.00, 1, '2020-06-11 01:22:00', '2020-06-11 01:23:09');

-- --------------------------------------------------------

--
-- Table structure for table `order_tracking`
--

CREATE TABLE `order_tracking` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `waybillnumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updatecode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updatedescription` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updatedatetime` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updatelocation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comments` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `problemcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_tracking`
--

INSERT INTO `order_tracking` (`id`, `order_id`, `waybillnumber`, `updatecode`, `updatedescription`, `updatedatetime`, `updatelocation`, `comments`, `problemcode`, `created_at`, `updated_at`) VALUES
(1, 81, '45267534572', 'SH203', 'Record Created', '2020-05-18T08:49:00', 'Amman, Jordan', '', '', '2020-05-27 00:22:37', '2020-05-27 00:22:37'),
(2, 81, '45267534572', 'SH278', 'Data received.', '2020-05-18T08:49:00', 'Amman, Jordan', ' ', '', '2020-05-27 00:22:37', '2020-05-27 00:22:37'),
(3, 118, '45267607070', 'SH203', 'Record Created', '2020-05-27T09:38:00', 'Amman, Jordan', '', '', '2020-05-27 01:12:05', '2020-05-27 01:12:05'),
(4, 118, '45267607070', 'SH278', 'Data received.', '2020-05-27T09:38:00', 'Amman, Jordan', ' ', '', '2020-05-27 01:12:05', '2020-05-27 01:12:05'),
(5, 101, '45267625804', 'SH278', 'Data received.', '2020-05-28T08:26:00', 'Amman, Jordan', ' ', '', '2020-06-01 00:56:42', '2020-06-01 00:56:42'),
(6, 101, '45267625804', 'SH203', 'Record Created', '2020-05-28T08:26:00', 'Amman, Jordan', '', '', '2020-06-01 00:56:42', '2020-06-01 00:56:42'),
(7, 123, '45267723325', 'SH203', 'Record Created', '2020-06-03T09:08:00', 'Amman, Jordan', '', '', '2020-06-03 04:04:21', '2020-06-03 04:04:21'),
(8, 123, '45267723325', 'SH278', 'Data received.', '2020-06-03T09:08:00', 'Amman, Jordan', ' ', '', '2020-06-03 04:04:22', '2020-06-03 04:04:22'),
(9, 127, '45267728004', 'SH203', 'Record Created', '2020-06-03T12:25:00', 'Amman, Jordan', '', '', '2020-06-03 04:04:23', '2020-06-03 04:04:23'),
(10, 127, '45267728004', 'SH278', 'Data received.', '2020-06-03T12:25:00', 'Amman, Jordan', ' ', '', '2020-06-03 04:04:23', '2020-06-03 04:04:23'),
(11, 128, '45267728074', 'SH203', 'Record Created', '2020-06-03T12:29:00', 'Amman, Jordan', '', '', '2020-06-03 04:04:24', '2020-06-03 04:04:24'),
(12, 128, '45267728074', 'SH278', 'Data received.', '2020-06-03T12:29:00', 'Amman, Jordan', ' ', '', '2020-06-03 04:04:24', '2020-06-03 04:04:24'),
(13, 126, '45267946986', 'SH278', 'Data received.', '2020-06-10T15:20:00', 'Amman, Jordan', ' ', '', '2020-06-10 07:03:31', '2020-06-10 07:03:31'),
(14, 126, '45267946986', 'SH203', 'Record Created', '2020-06-10T15:20:00', 'Amman, Jordan', '', '', '2020-06-10 07:03:31', '2020-06-10 07:03:31'),
(15, 134, '45267946776', 'SH203', 'Record Created', '2020-06-10T15:08:00', 'Amman, Jordan', '', '', '2020-06-10 07:03:34', '2020-06-10 07:03:34'),
(16, 134, '45267946776', 'SH278', 'Data received.', '2020-06-10T15:08:00', 'Amman, Jordan', ' ', '', '2020-06-10 07:03:34', '2020-06-10 07:03:34');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('bharat@gmail.com', '$2y$10$TzBcsiZ6IinORkFQ2qtGj.iebehF1a7tldjpeO7zWbPFa6iAMyNC2', '2020-05-22 01:43:08'),
('ketulpatel@webmynesystems.com', '$2y$10$gl8FQMXeq70WIynmWXV0R.HvTg/FynCUZ.F87wMdgnLZM3kgK.7Z.', '2020-05-22 08:10:43'),
('ketulpatel@webmynesystems.com', 'iVqPJYj6stuerMcKDv7FyN33SLhCuVsVTQWtMKSEqgNljj2ftPvAfAucaEA2', NULL),
('ketulpatel@webmynesystems.com', 'cEc0nqGoUJPf6YiNTbZzgakuvquy8d3qvVQboC6GXiLXjdNma4QCn3hPMEVl', NULL),
('ketulpatel@webmynesystems.com', 'ojhnohdBL9VJmn3iMXejRy83PkOVPHSyZ471Rq7hWGcICchjKcj0p0mKvE75', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT '0',
  `amount` double(12,3) DEFAULT '0.000',
  `invoice_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `payment_method_id` int(11) NOT NULL,
  `coupon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `user_id`, `amount`, `invoice_id`, `payment_method_id`, `coupon`, `created_at`, `updated_at`) VALUES
(3, 1, 1234.000, '1', 1, 'OAWDOAS', '2018-06-13 09:59:11', '2018-06-13 09:59:11'),
(4, 1, 198.000, '03', 1, '939203r7h Edit', '2018-06-13 09:59:34', '2018-06-13 10:01:36'),
(12, 1, 123.000, '', 1, '', '2018-06-18 04:41:09', '2018-06-18 04:41:09'),
(24, 1, 30.000, '-1', 1, '', '2018-07-13 08:54:42', '2018-08-13 08:54:42'),
(25, 1, 10.000, '-1', 1, '', '2018-07-13 09:07:02', '2018-08-13 09:07:02'),
(26, 1, 200.000, '-1', 1, '', '2018-07-13 09:09:00', '2018-08-13 09:09:00'),
(27, 1, 300.000, '-1', 1, '', '2018-07-13 09:09:40', '2018-08-13 09:09:40'),
(28, 1, 500.000, '-1', 1, '', '2018-07-13 09:11:47', '2018-08-13 09:11:47'),
(29, 1, 0.000, '-1', 1, '', '2018-08-13 09:13:22', '2018-08-13 09:13:22'),
(30, 1, 30.000, '-1', 1, '', '2018-08-13 09:13:52', '2018-08-13 09:13:52'),
(31, 1, 2100.000, '-1', 1, '', '2018-08-13 09:27:53', '2018-08-13 09:27:53'),
(32, 1, 200.000, '-1', 1, '', '2018-08-13 09:28:26', '2018-08-13 09:28:26'),
(33, 1, 200.000, '-1', 1, '', '2018-08-29 09:46:37', '2018-08-29 09:46:37'),
(34, 1, 30.000, '-1', 1, '', '2018-09-05 05:37:24', '2018-09-05 05:37:24'),
(35, 1, 30.000, '-1', 1, '', '2018-09-05 05:37:41', '2018-09-05 05:37:41'),
(36, 1, 200.000, '-1', 1, '', '2018-09-05 06:16:59', '2018-09-05 06:16:59'),
(37, 7, 120.000, '0', 3, '', '2018-09-26 08:18:22', '2018-09-26 08:18:22'),
(38, 7, 120.000, '0', 3, '', '2018-09-26 08:21:38', '2018-09-26 08:21:38'),
(39, 7, 120.000, '0', 3, '', '2018-09-26 08:21:55', '2018-09-26 08:21:55'),
(40, 7, 120.000, '0', 3, '', '2018-09-26 08:23:52', '2018-09-26 08:23:52'),
(41, 7, 120.000, '0', 3, '', '2018-09-26 08:24:13', '2018-09-26 08:24:13'),
(42, 7, 120.000, '0', 3, '', '2018-09-26 08:27:06', '2018-09-26 08:27:06'),
(43, 7, 120.000, '0', 3, '', '2018-09-26 08:35:03', '2018-09-26 08:35:03'),
(44, 7, 120.000, '0', 3, '', '2018-09-26 08:35:42', '2018-09-26 08:35:42'),
(45, 7, 120.000, '0', 3, '', '2018-09-26 08:37:07', '2018-09-26 08:37:07'),
(46, 7, 120.000, '0', 3, '', '2018-09-26 08:38:00', '2018-09-26 08:38:00'),
(47, 8, 130.000, '0', 4, '', '2018-09-26 08:38:00', '2018-09-26 08:38:00'),
(48, 9, 120.000, '0', 3, '', '2018-09-26 08:38:01', '2018-09-26 08:38:01'),
(49, 10, 110.000, '0', 4, '', '2018-09-26 08:38:01', '2018-09-26 08:38:01'),
(50, 7, 120.000, '0', 3, '', '2018-09-26 08:38:43', '2018-09-26 08:38:43'),
(51, 8, 130.000, '0', 4, '', '2018-09-26 08:38:44', '2018-09-26 08:38:44'),
(52, 9, 120.000, '0', 3, '', '2018-09-26 08:38:44', '2018-09-26 08:38:44'),
(53, 10, 110.000, '0', 4, '', '2018-09-26 08:38:44', '2018-09-26 08:38:44'),
(54, 7, 120.000, '0', 3, '', '2018-09-26 08:40:40', '2018-09-26 08:40:40'),
(55, 8, 130.000, '0', 4, '', '2018-09-26 08:40:40', '2018-09-26 08:40:40'),
(56, 9, 120.000, '0', 3, '', '2018-09-26 08:40:40', '2018-09-26 08:40:40'),
(57, 10, 110.000, '0', 4, '', '2018-09-26 08:40:40', '2018-09-26 08:40:40'),
(58, 7, 120.000, '0', 3, '', '2018-09-26 08:41:26', '2018-09-26 08:41:26'),
(59, 8, 130.000, '0', 4, '', '2018-09-26 08:41:26', '2018-09-26 08:41:26'),
(60, 9, 120.000, '0', 3, '', '2018-09-26 08:41:26', '2018-09-26 08:41:26'),
(61, 10, 110.000, '0', 4, '', '2018-09-26 08:41:26', '2018-09-26 08:41:26'),
(62, 7, 120.000, '0', 3, '', '2018-09-26 08:43:26', '2018-09-26 08:43:26'),
(63, 8, 130.000, '0', 4, '', '2018-09-26 08:43:26', '2018-09-26 08:43:26'),
(64, 9, 120.000, '0', 3, '', '2018-09-26 08:43:26', '2018-09-26 08:43:26'),
(65, 10, 110.000, '0', 4, '', '2018-09-26 08:43:27', '2018-09-26 08:43:27'),
(66, 7, 120.000, '0', 3, '', '2018-09-26 08:43:53', '2018-09-26 08:43:53'),
(67, 8, 130.000, '0', 4, '', '2018-09-26 08:43:53', '2018-09-26 08:43:53'),
(68, 9, 120.000, '0', 3, '', '2018-09-26 08:43:53', '2018-09-26 08:43:53'),
(69, 10, 110.000, '0', 4, '', '2018-09-26 08:43:54', '2018-09-26 08:43:54'),
(70, 7, 120.000, '0', 3, '', '2018-09-26 08:44:24', '2018-09-26 08:44:24'),
(71, 8, 130.000, '0', 4, '', '2018-09-26 08:44:25', '2018-09-26 08:44:25'),
(72, 9, 120.000, '0', 3, '', '2018-09-26 08:44:25', '2018-09-26 08:44:25'),
(73, 10, 110.000, '0', 4, '', '2018-09-26 08:44:25', '2018-09-26 08:44:25'),
(74, 7, 120.000, '0', 3, '', '2018-09-26 08:44:44', '2018-09-26 08:44:44'),
(75, 8, 130.000, '0', 4, '', '2018-09-26 08:44:44', '2018-09-26 08:44:44'),
(76, 9, 120.000, '0', 3, '', '2018-09-26 08:44:44', '2018-09-26 08:44:44'),
(77, 10, 110.000, '0', 4, '', '2018-09-26 08:44:44', '2018-09-26 08:44:44'),
(78, 7, 120.000, '0', 3, '', '2018-09-26 08:45:23', '2018-09-26 08:45:23'),
(79, 8, 130.000, '0', 4, '', '2018-09-26 08:45:23', '2018-09-26 08:45:23'),
(80, 9, 120.000, '0', 3, '', '2018-09-26 08:45:24', '2018-09-26 08:45:24'),
(81, 10, 110.000, '0', 4, '', '2018-09-26 08:45:24', '2018-09-26 08:45:24'),
(82, 7, 120.000, '0', 3, '', '2018-09-26 08:48:59', '2018-09-26 08:48:59'),
(83, 8, 130.000, '0', 4, '', '2018-09-26 08:48:59', '2018-09-26 08:48:59'),
(84, 9, 120.000, '0', 3, '', '2018-09-26 08:49:00', '2018-09-26 08:49:00'),
(85, 10, 110.000, '0', 4, '', '2018-09-26 08:49:00', '2018-09-26 08:49:00'),
(86, 7, 120.000, '0', 3, '', '2018-09-26 08:50:11', '2018-09-26 08:50:11'),
(87, 8, 130.000, '0', 4, '', '2018-09-26 08:50:11', '2018-09-26 08:50:11'),
(88, 9, 120.000, '0', 3, '', '2018-09-26 08:50:11', '2018-09-26 08:50:11'),
(89, 10, 110.000, '0', 4, '', '2018-09-26 08:50:11', '2018-09-26 08:50:11'),
(90, 7, 120.000, '0', 3, '', '2018-09-26 08:50:29', '2018-09-26 08:50:29'),
(91, 8, 130.000, '0', 4, '', '2018-09-26 08:50:29', '2018-09-26 08:50:29'),
(92, 9, 120.000, '0', 3, '', '2018-09-26 08:50:29', '2018-09-26 08:50:29'),
(93, 10, 110.000, '0', 4, '', '2018-09-26 08:50:30', '2018-09-26 08:50:30'),
(94, 7, 120.000, '0', 3, '', '2018-09-26 08:50:43', '2018-09-26 08:50:43'),
(95, 8, 130.000, '0', 4, '', '2018-09-26 08:50:43', '2018-09-26 08:50:43'),
(96, 9, 120.000, '0', 3, '', '2018-09-26 08:50:43', '2018-09-26 08:50:43'),
(97, 10, 110.000, '0', 4, '', '2018-09-26 08:50:44', '2018-09-26 08:50:44'),
(98, 7, 120.000, '0', 3, '', '2018-09-26 08:51:10', '2018-09-26 08:51:10'),
(99, 8, 130.000, '0', 4, '', '2018-09-26 08:51:10', '2018-09-26 08:51:10'),
(100, 9, 120.000, '0', 3, '', '2018-09-26 08:51:10', '2018-09-26 08:51:10'),
(101, 10, 110.000, '0', 4, '', '2018-09-26 08:51:10', '2018-09-26 08:51:10'),
(102, 7, 120.000, '0', 3, '', '2018-09-26 08:51:50', '2018-09-26 08:51:50'),
(103, 8, 130.000, '0', 4, '', '2018-09-26 08:51:51', '2018-09-26 08:51:51'),
(104, 9, 120.000, '0', 3, '', '2018-09-26 08:51:51', '2018-09-26 08:51:51'),
(105, 10, 110.000, '0', 4, '', '2018-09-26 08:51:51', '2018-09-26 08:51:51'),
(106, 7, 120.000, '0', 3, '', '2018-09-26 08:52:36', '2018-09-26 08:52:36'),
(107, 8, 130.000, '0', 4, '', '2018-09-26 08:52:36', '2018-09-26 08:52:36'),
(108, 9, 120.000, '0', 3, '', '2018-09-26 08:52:36', '2018-09-26 08:52:36'),
(109, 10, 110.000, '0', 4, '', '2018-09-26 08:52:36', '2018-09-26 08:52:36'),
(110, 7, 120.000, '0', 3, '', '2018-09-26 08:59:53', '2018-09-26 08:59:53'),
(111, 8, 130.000, '0', 4, '', '2018-09-26 08:59:53', '2018-09-26 08:59:53'),
(112, 9, 120.000, '0', 3, '', '2018-09-26 08:59:53', '2018-09-26 08:59:53'),
(113, 10, 110.000, '0', 4, '', '2018-09-26 08:59:53', '2018-09-26 08:59:53'),
(114, 7, 120.000, '0', 3, '', '2018-09-26 09:00:18', '2018-09-26 09:00:18'),
(115, 8, 130.000, '0', 4, '', '2018-09-26 09:00:18', '2018-09-26 09:00:18'),
(116, 9, 120.000, '0', 3, '', '2018-09-26 09:00:19', '2018-09-26 09:00:19'),
(117, 10, 110.000, '0', 4, '', '2018-09-26 09:00:19', '2018-09-26 09:00:19'),
(118, 7, 120.000, '0', 3, '', '2018-09-26 09:01:21', '2018-09-26 09:01:21'),
(119, 8, 130.000, '0', 4, '', '2018-09-26 09:01:21', '2018-09-26 09:01:21'),
(120, 9, 120.000, '0', 3, '', '2018-09-26 09:01:21', '2018-09-26 09:01:21'),
(121, 10, 110.000, '0', 4, '', '2018-09-26 09:01:21', '2018-09-26 09:01:21'),
(122, 7, 120.000, '0', 3, '', '2018-09-26 09:01:42', '2018-09-26 09:01:42'),
(123, 8, 130.000, '0', 4, '', '2018-09-26 09:01:42', '2018-09-26 09:01:42'),
(124, 9, 120.000, '0', 3, '', '2018-09-26 09:01:42', '2018-09-26 09:01:42'),
(125, 10, 110.000, '0', 4, '', '2018-09-26 09:01:43', '2018-09-26 09:01:43'),
(126, 7, 120.000, '0', 3, '', '2018-09-26 09:02:01', '2018-09-26 09:02:01'),
(127, 8, 130.000, '0', 4, '', '2018-09-26 09:02:01', '2018-09-26 09:02:01'),
(128, 9, 120.000, '0', 3, '', '2018-09-26 09:02:01', '2018-09-26 09:02:01'),
(129, 10, 110.000, '0', 4, '', '2018-09-26 09:02:02', '2018-09-26 09:02:02'),
(130, 7, 120.000, '0', 3, '', '2018-09-26 09:03:17', '2018-09-26 09:03:17'),
(131, 8, 130.000, '0', 4, '', '2018-09-26 09:03:17', '2018-09-26 09:03:17'),
(132, 9, 120.000, '0', 3, '', '2018-09-26 09:03:18', '2018-09-26 09:03:18'),
(133, 10, 110.000, '0', 4, '', '2018-09-26 09:03:18', '2018-09-26 09:03:18'),
(134, 7, 120.000, '0', 3, '', '2018-09-26 09:05:03', '2018-09-26 09:05:03'),
(135, 8, 130.000, '0', 4, '', '2018-09-26 09:05:04', '2018-09-26 09:05:04'),
(136, 9, 120.000, '0', 3, '', '2018-09-26 09:05:04', '2018-09-26 09:05:04'),
(137, 10, 110.000, '0', 4, '', '2018-09-26 09:05:04', '2018-09-26 09:05:04'),
(138, 7, 120.000, '0', 3, '', '2018-09-26 09:07:04', '2018-09-26 09:07:04'),
(139, 8, 130.000, '0', 4, '', '2018-09-26 09:07:04', '2018-09-26 09:07:04'),
(140, 9, 120.000, '0', 3, '', '2018-09-26 09:07:04', '2018-09-26 09:07:04'),
(141, 10, 110.000, '0', 4, '', '2018-09-26 09:07:04', '2018-09-26 09:07:04'),
(142, 7, 120.000, '0', 3, '', '2018-09-26 09:07:21', '2018-09-26 09:07:21'),
(143, 8, 130.000, '0', 4, '', '2018-09-26 09:07:21', '2018-09-26 09:07:21'),
(144, 9, 120.000, '0', 3, '', '2018-09-26 09:07:21', '2018-09-26 09:07:21'),
(145, 10, 110.000, '0', 4, '', '2018-09-26 09:07:21', '2018-09-26 09:07:21'),
(146, 1, 30000.000, '201904161955255670000000000', 4, '', '2019-04-16 17:00:42', '2019-04-16 17:00:42'),
(147, 1, 30000.000, '201904161955255670000000000', 4, '', '2019-04-16 17:01:50', '2019-04-16 17:01:50'),
(148, 1, 30000.000, '201904161955255670000000000', 4, '', '2019-04-16 17:10:50', '2019-04-16 17:10:50'),
(149, 1, 30000.000, '201904161955255670000000000', 4, '', '2019-04-16 17:11:30', '2019-04-16 17:11:30'),
(150, 1, 30000.000, 'null', 4, '', '2019-08-27 17:49:29', '2019-08-27 17:49:29'),
(151, 1, 30000.000, '2019082817183410000000000', 4, '', '2019-08-28 11:18:55', '2019-08-28 11:18:55'),
(152, 1, NULL, NULL, 4, '', '2019-12-04 07:11:24', '2019-12-04 07:11:24'),
(153, NULL, 80000.000, 'null', 4, '', '2020-02-14 01:17:07', '2020-02-14 01:17:07'),
(155, 1, 200000.000, '202002140954522870000000000', 4, '', '2020-02-14 01:25:02', '2020-02-14 01:25:02'),
(156, 3, 1230.000, '', 3, '', '2020-03-26 00:41:13', '2020-03-26 00:41:13'),
(157, 1, 100.000, '', 3, '', '2020-03-26 00:41:34', '2020-03-26 00:41:34'),
(158, 21, 2000.000, '', 3, '', '2020-03-26 00:47:33', '2020-03-26 00:47:33'),
(159, 1, 1000.000, '', 3, '', '2020-03-26 23:28:13', '2020-03-26 23:28:13'),
(160, 21, 123000.000, '202004150933349690000000000', 4, '', '2020-04-15 01:03:53', '2020-04-15 01:03:53'),
(161, 11, 25000.000, '202004161307324470000000000', 4, '', '2020-04-16 04:37:51', '2020-04-16 04:37:51'),
(162, 11, 123000.000, '20200416130838820000000000', 4, '', '2020-04-16 04:38:56', '2020-04-16 04:38:56'),
(163, 11, 80000.000, '202004171045365880000000000', 4, '', '2020-04-17 02:15:54', '2020-04-17 02:15:54'),
(164, 21, 80000.000, '202005140928554770000000000', 4, '', '2020-05-14 00:59:14', '2020-05-14 00:59:14'),
(165, 21, 532500.000, '202005140937536310000000000', 4, '', '2020-05-14 01:08:18', '2020-05-14 01:08:18'),
(166, 21, 542500.000, '202005140946332950000000000', 4, '', '2020-05-14 01:16:52', '2020-05-14 01:16:52'),
(167, 21, 7000.000, '202005140951147060000000000', 4, '', '2020-05-14 01:21:32', '2020-05-14 01:21:32'),
(168, 21, 532500.000, '202005141538128030000000000', 4, '', '2020-05-14 07:08:31', '2020-05-14 07:08:31'),
(169, 21, 15000.000, '20200514161044270000000000', 4, '', '2020-05-14 07:41:01', '2020-05-14 07:41:01'),
(170, 21, 15.000, '82', 3, '', '2020-05-15 00:18:59', '2020-05-15 00:18:59'),
(171, 21, 15.000, '82', 3, '', '2020-05-15 00:21:36', '2020-05-15 00:21:36'),
(172, 21, 15.000, '82', 3, '', '2020-05-15 00:22:39', '2020-05-15 00:22:39'),
(173, 11, 2147.500, '67', 3, '', '2020-05-15 00:30:24', '2020-05-15 00:30:24'),
(174, 21, 70000.000, '202005151632313620000000000', 4, '', '2020-05-15 08:02:49', '2020-05-15 08:02:49'),
(175, 21, 5000.000, '202005151641271910000000000', 4, '', '2020-05-15 08:11:45', '2020-05-15 08:11:45'),
(176, 21, 5000.000, '202005151653494170000000000', 4, 'SIDD', '2020-05-15 08:24:07', '2020-05-15 08:24:07'),
(177, NULL, 5000.000, '202005151653494170000000000', 4, '', '2020-05-18 00:09:43', '2020-05-18 00:09:43'),
(178, 11, 230.000, '96', 3, '', '2020-05-20 00:11:48', '2020-05-20 00:11:48'),
(179, 21, 737500.000, '20200522160642350000000000', 4, 'SIDD', '2020-05-22 07:36:59', '2020-05-22 07:36:59'),
(180, 11, 15000.000, '202005221650306430000000000', 4, '', '2020-05-22 08:20:48', '2020-05-22 08:20:48'),
(181, 11, 15000.000, '202005221650306430000000000', 4, '', '2020-05-22 08:22:55', '2020-05-22 08:22:55'),
(182, 11, 15000.000, '202005221650306430000000000', 4, '', '2020-05-22 08:24:08', '2020-05-22 08:24:08'),
(183, 21, 507500.000, '202005251215045570000000000', 4, 'SIDD', '2020-05-25 03:45:22', '2020-05-25 03:45:22'),
(184, 21, 507500.000, '202005251215045570000000000', 4, 'SIDD', '2020-05-25 03:48:44', '2020-05-25 03:48:44'),
(185, 21, 507500.000, '202005251215045570000000000', 4, 'SIDD', '2020-05-25 03:50:33', '2020-05-25 03:50:33'),
(186, 21, 507500.000, '202005251215045570000000000', 4, 'SIDD', '2020-05-25 03:50:59', '2020-05-25 03:50:59'),
(187, 21, 507500.000, '202005251215045570000000000', 4, 'SIDD', '2020-05-25 03:54:46', '2020-05-25 03:54:46'),
(188, 11, 15000.000, '202005261004052780000000000', 4, '', '2020-05-26 01:34:23', '2020-05-26 01:34:23'),
(189, 11, 15000.000, '202005261008031680000000000', 4, '', '2020-05-26 01:38:21', '2020-05-26 01:38:21'),
(190, 11, 15.000, '114', 3, '', '2020-05-26 01:50:50', '2020-05-26 01:50:50'),
(191, 11, 15000.000, '20200526122830420000000000', 4, '', '2020-05-26 03:58:48', '2020-05-26 03:58:48'),
(192, 11, 15000.000, '202005261250203120000000000', 4, '', '2020-05-26 04:20:38', '2020-05-26 04:20:38'),
(193, 11, 15000.000, '202005261250203120000000000', 4, '', '2020-05-26 04:23:25', '2020-05-26 04:23:25'),
(194, 11, 15000.000, '202005261250203120000000000', 4, '', '2020-05-26 04:23:52', '2020-05-26 04:23:52'),
(195, 11, 15000.000, '202005261250203120000000000', 4, '', '2020-05-26 04:26:41', '2020-05-26 04:26:41'),
(196, 11, 517500.000, '202005261310396640000000000', 4, '', '2020-05-26 04:40:57', '2020-05-26 04:40:57'),
(197, 1, 517.500, '118', 3, '', '2020-05-27 01:02:43', '2020-05-27 01:02:43'),
(198, 1, 517.500, '118', 3, '', '2020-05-27 01:03:00', '2020-05-27 01:03:00'),
(199, 1, 517.500, '118', 3, '', '2020-05-27 01:07:24', '2020-05-27 01:07:24'),
(200, 1, 747.500, '101', 3, '', '2020-05-27 01:44:00', '2020-05-27 01:44:00'),
(201, 21, 15000.000, 'null', 4, '', '2020-05-28 00:32:11', '2020-05-28 00:32:11'),
(202, 21, 15000.000, 'null', 4, '', '2020-05-28 00:38:43', '2020-05-28 00:38:43'),
(203, 11, 15000.000, 'null', 4, '', '2020-05-28 00:44:12', '2020-05-28 00:44:12'),
(204, 1, 7877.500, '123', 3, '', '2020-06-02 23:54:28', '2020-06-02 23:54:28'),
(205, 1, 517.500, '127', 3, '', '2020-06-03 03:16:34', '2020-06-03 03:16:34'),
(206, 11, 15.000, '126', 3, '', '2020-06-03 07:19:36', '2020-06-03 07:19:36'),
(207, 11, 15.000, '126', 3, '', '2020-06-03 07:20:00', '2020-06-03 07:20:00'),
(208, 11, 15.000, '126', 3, '', '2020-06-03 07:21:38', '2020-06-03 07:21:38'),
(209, 11, 15.000, '125', 3, '', '2020-06-03 07:30:35', '2020-06-03 07:30:35');

-- --------------------------------------------------------

--
-- Table structure for table `payment_method`
--

CREATE TABLE `payment_method` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `logo_image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `percent_fees` double(12,3) DEFAULT '0.000',
  `fixed_fees` double(12,3) DEFAULT '0.000',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_method`
--

INSERT INTO `payment_method` (`id`, `name`, `logo_image_path`, `percent_fees`, `fixed_fees`, `created_at`, `updated_at`) VALUES
(1, 'paypal', '', 4.900, 0.300, '2018-08-29 10:15:41', '2018-08-29 10:15:41'),
(2, 'stripe', '', 2.900, 0.300, '2018-08-29 10:15:53', '2018-08-29 10:15:53'),
(3, 'Manually', '', 0.000, 0.000, '2019-09-23 21:39:03', '2019-09-23 21:39:03'),
(4, 'Sts PayOne', '', 0.000, 0.000, '2019-09-23 21:39:03', '2019-09-23 21:39:03');

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `id` int(10) UNSIGNED NOT NULL,
  `video_id` int(11) NOT NULL,
  `quiz_statement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quiz_statement_ar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` enum('Pending','Active','Inactive') COLLATE utf8mb4_unicode_ci DEFAULT 'Pending',
  `type` enum('video','session') COLLATE utf8mb4_unicode_ci DEFAULT 'video'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`id`, `video_id`, `quiz_statement`, `quiz_statement_ar`, `created_at`, `updated_at`, `status`, `type`) VALUES
(1, 2, 'This is a TEst Statement Choose Second Choice To Pass', '\"تيست\"', '2018-07-25 09:56:14', '2020-04-16 08:44:42', 'Active', 'video'),
(2, 2, 'This is a Test Statement Choose Third Choice To Pass', '', '2018-07-25 09:56:38', '2020-04-16 08:44:42', 'Active', 'video'),
(3, 2, 'This is a Test Statement Choose Fourth Choice To Pass', '', '2018-07-25 09:57:03', '2020-04-16 08:44:42', 'Active', 'video'),
(4, 2, 'This is a Test Statement Choose First Choice To Pass', '', '2018-07-25 09:57:14', '2020-04-16 08:44:42', 'Active', 'video'),
(5, 2, 'This is a Test Statement Choose Fourth Choice To Pass', 'This is a Test Statement Choose Fourth Choice To Pass', '2018-07-25 09:57:19', '2020-04-16 08:44:42', 'Active', 'video'),
(17, 3, 'This is a Seconf test Choose the First Choice to pass', 'fdf', '2018-07-29 10:51:22', '2020-05-27 04:18:01', 'Active', 'video'),
(18, 3, 'This is another Quiz Statement Choose Third Choice', '', '2018-07-29 10:52:31', '2020-05-27 04:18:01', 'Active', 'video'),
(19, 3, 'This is another statement just for make sure you are understanded the video , Choose th second statement to pass', '', '2018-07-29 10:54:06', '2020-05-27 04:18:01', 'Active', 'video'),
(20, 3, 'i\'am bored of typing quizes just choose any choice to pass', '', '2018-07-29 10:55:13', '2020-05-27 04:18:01', 'Active', 'video'),
(21, 3, 'choose first again to pass', '', '2018-07-29 10:56:09', '2020-05-27 04:18:01', 'Active', 'video'),
(22, 6, 'This is a quiz statement', '', '2018-08-13 10:36:27', '2020-04-16 09:04:09', 'Active', 'video'),
(23, 6, 'This is  a Quiz Statement', '', '2018-08-13 10:37:57', '2020-04-16 09:04:09', 'Active', 'video'),
(24, 6, 'This is  a quiz statement', '', '2018-08-13 10:39:12', '2020-04-16 09:04:09', 'Active', 'video'),
(25, 6, 'this is a quiz statement', '', '2018-08-13 10:41:03', '2020-04-16 09:04:09', 'Active', 'video'),
(26, 6, 'Where can we automate Robot\'s wheel speed  ?', 'Where can we automate Robot\'s wheel speed', '2018-08-13 10:42:21', '2020-04-16 09:04:09', 'Active', 'video'),
(27, 4, 'this is a quiz', '', '2018-08-13 10:45:07', '2018-08-13 10:45:07', 'Pending', 'video'),
(28, 4, 'this is a quiz', '', '2018-08-13 10:46:17', '2018-08-13 10:46:17', 'Pending', 'video'),
(29, 4, 'All Choices is correct', '', '2018-08-13 10:47:54', '2018-08-13 10:47:54', 'Pending', 'video'),
(30, 4, 'this is a quiz statement', '', '2018-08-13 10:49:04', '2018-08-13 10:49:04', 'Pending', 'video'),
(31, 4, 'this is a quiz', '', '2018-08-13 10:49:55', '2018-08-13 10:49:55', 'Pending', 'video'),
(32, 13, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2018-09-05 11:16:17', '2020-04-17 01:24:10', 'Active', 'video'),
(33, 13, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2018-09-05 11:18:03', '2020-04-17 01:24:10', 'Active', 'video'),
(34, 42, 'What is the name of the Arduino board we will use in this course?', 'ما اسم لوحة الاردوينو التي سنستعملها خلال هذه السلسلة؟', '2019-10-28 21:11:37', '2019-10-28 21:22:09', 'Pending', 'video'),
(35, 44, 'What is the rage for the analog pins in Arduino?', 'ما هو مدى القيم للمنافذ التماثلية في الاردوينو؟', '2019-10-28 21:27:00', '2019-10-28 21:29:03', 'Pending', 'video'),
(36, 45, 'What is the name of the programming language used for Arduino?', 'ما هي لغة البرمجة المستعملة للاردوينو؟', '2019-10-28 21:33:44', '2019-10-28 21:33:44', 'Pending', 'video'),
(37, 46, 'What verify command do?', 'ما هو عمل أمر Verify؟', '2019-10-28 21:38:32', '2019-10-28 21:38:32', 'Pending', 'video'),
(39, 47, 'What is the right statement to define an integer variable?', 'ما هي الجملة الصحيحة لتعريف متغير من نوع Integer?', '2019-10-29 20:49:24', '2019-10-29 20:49:45', 'Pending', 'video'),
(40, 48, 'If we have a LED in digital pin 8, what is the right statement to turn this LED on?', 'لو كان لدينا LED على المنفذ رقم 8, ماهي الجملة المناسبة لتشغيل هذا LED؟', '2019-10-29 21:02:26', '2019-10-29 21:02:26', 'Pending', 'video'),
(41, 49, 'What is the statement that we used to read data from a digital sensor?', 'ما هي الجملة التي نستعملها لقراءة معلومات من حساس Digital؟', '2019-10-29 21:26:42', '2019-10-29 21:26:42', 'Pending', 'video'),
(42, 49, 'What is the statement that we used to read data from a digital sensor?', 'ما هي الجملة التي نستعملها لقراءة معلومات من حساس Digital؟', '2019-10-29 21:26:44', '2019-10-29 21:26:44', 'Pending', 'video'),
(43, 2, 'testEN', 'testAR', '2020-03-26 22:46:36', '2020-04-16 08:44:42', 'Active', 'video'),
(44, 68, 'testA', 'TeatB', '2020-03-26 23:39:44', '2020-03-26 23:39:44', 'Pending', 'video'),
(45, 24, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-15 00:11:09', '2020-04-17 02:30:09', 'Active', 'video'),
(46, 24, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-15 00:30:20', '2020-04-17 02:30:09', 'Active', 'video'),
(47, 24, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-15 01:57:59', '2020-04-17 02:30:09', 'Active', 'video'),
(58, 24, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-15 02:27:20', '2020-04-17 02:30:09', 'Active', 'video'),
(59, 24, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-15 02:27:39', '2020-04-17 02:30:09', 'Active', 'video'),
(60, 11, 'test1', 'test1', '2020-04-15 03:42:47', '2020-04-15 03:51:12', 'Active', 'video'),
(61, 11, 'test2', 'test2', '2020-04-15 03:44:07', '2020-04-15 03:51:12', 'Active', 'video'),
(62, 11, 'test3', 'test3', '2020-04-15 03:46:04', '2020-04-15 03:51:12', 'Active', 'video'),
(63, 11, 'test4', 'test4', '2020-04-15 03:49:53', '2020-04-15 03:51:12', 'Active', 'video'),
(64, 11, 'test5', 'test5', '2020-04-15 03:51:12', '2020-04-15 03:51:12', 'Active', 'video'),
(65, 24, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-15 23:08:24', '2020-04-17 02:30:09', 'Active', 'video'),
(66, 21, 'test1', 'test1', '2020-04-16 05:11:52', '2020-04-16 05:13:58', 'Active', 'video'),
(67, 21, 'test2', 'test2', '2020-04-16 05:12:24', '2020-04-16 05:13:58', 'Active', 'video'),
(68, 21, 'test3', 'test3', '2020-04-16 05:12:48', '2020-04-16 05:13:58', 'Active', 'video'),
(69, 21, 'test4', 'tedt4', '2020-04-16 05:13:20', '2020-04-16 05:13:58', 'Active', 'video'),
(70, 21, 'test5', 'test 5', '2020-04-16 05:13:58', '2020-04-16 05:13:58', 'Active', 'video'),
(71, 10, 'test1', 'test1', '2020-04-16 08:12:07', '2020-04-16 08:18:02', 'Active', 'video'),
(72, 10, 'test2', 'test2', '2020-04-16 08:12:38', '2020-04-16 08:18:02', 'Active', 'video'),
(73, 10, 'test3', 'test3', '2020-04-16 08:13:16', '2020-04-16 08:18:02', 'Active', 'video'),
(74, 10, 'test4', 'test4', '2020-04-16 08:14:51', '2020-04-16 08:18:02', 'Active', 'video'),
(75, 10, 'test5', 'test5', '2020-04-16 08:18:02', '2020-04-16 08:18:02', 'Active', 'video'),
(76, 13, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-17 01:09:45', '2020-04-17 01:24:10', 'Active', 'video'),
(77, 13, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-17 01:21:35', '2020-04-17 01:24:10', 'Active', 'video'),
(78, 13, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-17 01:24:10', '2020-04-17 01:24:10', 'Active', 'video'),
(79, 70, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-17 02:25:18', '2020-05-27 02:31:48', 'Active', 'video'),
(80, 70, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-17 02:25:43', '2020-05-27 02:31:48', 'Active', 'video'),
(81, 70, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-17 02:26:19', '2020-05-27 02:31:48', 'Active', 'video'),
(82, 70, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-17 02:27:02', '2020-05-27 02:31:48', 'Active', 'video'),
(83, 24, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-17 02:30:09', '2020-04-17 02:30:09', 'Active', 'video'),
(84, 69, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-17 02:38:18', '2020-04-17 02:39:01', 'Active', 'video'),
(85, 69, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-17 02:38:31', '2020-04-17 02:39:01', 'Active', 'video'),
(86, 69, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-17 02:38:41', '2020-04-17 02:39:01', 'Active', 'video'),
(87, 69, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-17 02:38:52', '2020-04-17 02:39:01', 'Active', 'video'),
(88, 69, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2020-04-17 02:39:01', '2020-04-17 02:39:01', 'Active', 'video'),
(89, 71, 'test1', 'test1', '2020-05-11 04:21:22', '2020-05-11 04:23:44', 'Active', 'video'),
(90, 71, 'test 2', 'test 2', '2020-05-11 04:22:08', '2020-05-11 04:23:44', 'Active', 'video'),
(91, 71, 'test3', 'test3', '2020-05-11 04:22:37', '2020-05-11 04:23:44', 'Active', 'video'),
(92, 71, 'test4', 'test4', '2020-05-11 04:23:11', '2020-05-11 04:23:44', 'Active', 'video'),
(93, 71, 'test5', 'test5', '2020-05-11 04:23:44', '2020-05-11 04:23:44', 'Active', 'video'),
(96, 70, 'test3', 'test3', '2020-05-27 02:22:52', '2020-05-27 02:31:48', 'Active', 'video'),
(97, 1, 'session1', 'session1', '2020-05-27 02:53:55', '2020-05-27 03:37:50', 'Active', 'session'),
(98, 1, 'session2', 'session2', '2020-05-27 03:34:21', '2020-05-27 03:37:50', 'Active', 'session'),
(99, 1, 'session3', 'session3', '2020-05-27 03:35:56', '2020-05-27 03:37:50', 'Active', 'session'),
(100, 1, 'session4', 'session4', '2020-05-27 03:37:19', '2020-05-27 03:37:50', 'Active', 'session'),
(101, 1, 'session5', 'session5', '2020-05-27 03:37:50', '2020-05-27 03:37:50', 'Active', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_report`
--

CREATE TABLE `quiz_report` (
  `id` int(10) UNSIGNED NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `choice_id` int(11) NOT NULL,
  `correct` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quiz_report`
--

INSERT INTO `quiz_report` (`id`, `quiz_id`, `user_id`, `video_id`, `course_id`, `choice_id`, `correct`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, 6, 2, 1, '2018-08-01 06:24:51', '2018-08-01 06:24:51'),
(2, 2, 1, 2, 6, 6, 1, '2018-08-01 06:24:55', '2018-08-01 06:24:55'),
(3, 3, 1, 2, 6, 9, 0, '2018-08-01 06:24:57', '2018-08-01 06:24:57'),
(4, 4, 1, 2, 6, 15, 0, '2018-08-01 06:24:59', '2018-08-01 06:24:59'),
(5, 5, 1, 2, 6, 20, 0, '2018-08-01 06:25:01', '2018-08-01 06:25:01'),
(6, 17, 1, 3, 6, 64, 1, '2018-08-01 06:31:05', '2018-08-01 06:31:05'),
(7, 18, 1, 3, 6, 70, 1, '2018-08-01 06:31:09', '2018-08-01 06:31:09'),
(8, 19, 1, 3, 6, 73, 1, '2018-08-01 06:31:12', '2018-08-01 06:31:12'),
(9, 20, 1, 3, 6, 77, 1, '2018-08-01 06:31:19', '2018-08-01 06:31:19'),
(10, 21, 1, 3, 6, 80, 1, '2018-08-01 06:31:23', '2018-08-01 06:31:23'),
(11, 22, 1, 6, 6, 85, 1, '2018-08-13 10:43:51', '2018-08-13 10:43:51'),
(12, 23, 1, 6, 6, 88, 1, '2018-08-13 10:43:56', '2018-08-13 10:43:56'),
(13, 24, 1, 6, 6, 94, 0, '2018-08-13 10:44:01', '2018-08-13 10:44:01'),
(14, 25, 1, 6, 6, 97, 1, '2018-08-13 10:44:06', '2018-08-13 10:44:06'),
(15, 26, 1, 6, 6, 103, 1, '2018-08-13 10:44:10', '2018-08-13 10:44:10'),
(16, 27, 1, 4, 6, 106, 1, '2018-08-13 10:50:50', '2018-08-13 10:50:50'),
(17, 28, 1, 4, 6, 110, 0, '2018-08-13 10:50:55', '2018-08-13 10:50:55'),
(18, 29, 1, 4, 6, 113, 1, '2018-08-13 10:51:02', '2018-08-13 10:51:02'),
(19, 30, 1, 4, 6, 119, 1, '2018-08-13 10:51:08', '2018-08-13 10:51:08'),
(20, 31, 1, 4, 6, 121, 1, '2018-08-13 10:51:16', '2018-08-13 10:51:16'),
(21, 27, 1, 4, 6, 106, 1, '2018-10-04 06:43:38', '2018-10-04 06:43:38'),
(22, 28, 1, 4, 6, 111, 0, '2018-10-04 06:43:49', '2018-10-04 06:43:49'),
(23, 29, 1, 4, 6, 113, 1, '2018-10-04 06:43:59', '2018-10-04 06:43:59'),
(24, 30, 1, 4, 6, 119, 1, '2018-10-04 06:44:07', '2018-10-04 06:44:07'),
(25, 31, 1, 4, 6, 122, 1, '2018-10-04 06:44:11', '2018-10-04 06:44:11'),
(26, 22, 1, 6, 6, 85, 1, '2018-10-04 06:45:14', '2018-10-04 06:45:14'),
(27, 23, 1, 6, 6, 88, 1, '2018-10-04 06:45:19', '2018-10-04 06:45:19'),
(28, 24, 1, 6, 6, 95, 1, '2018-10-04 06:45:27', '2018-10-04 06:45:27'),
(29, 25, 1, 6, 6, 97, 1, '2018-10-04 06:45:37', '2018-10-04 06:45:38'),
(30, 26, 1, 6, 6, 101, 1, '2018-10-04 06:45:43', '2018-10-04 06:45:44'),
(31, 1, 1, 2, 6, 2, 1, '2018-10-04 07:20:18', '2018-10-04 07:20:18'),
(32, 2, 1, 2, 6, 7, 1, '2018-10-04 07:20:21', '2018-10-04 07:20:21'),
(33, 3, 1, 2, 6, 12, 1, '2018-10-04 07:20:23', '2018-10-04 07:20:24'),
(34, 4, 1, 2, 6, 13, 1, '2018-10-04 07:20:26', '2018-10-04 07:20:26'),
(35, 1, 1, 2, 6, 2, 1, '2018-10-04 07:20:31', '2018-10-04 07:20:31'),
(36, 2, 1, 2, 6, 7, 1, '2018-10-04 07:20:32', '2018-10-04 07:20:32'),
(37, 3, 1, 2, 6, 12, 1, '2018-10-04 07:20:32', '2018-10-04 07:20:32'),
(38, 1, 1, 2, 6, 4, 0, '2018-10-04 07:20:40', '2018-10-04 07:20:40'),
(39, 2, 1, 2, 6, 8, 0, '2018-10-04 07:20:43', '2018-10-04 07:20:43'),
(40, 3, 1, 2, 6, 11, 0, '2018-10-04 07:20:45', '2018-10-04 07:20:45'),
(41, 4, 1, 2, 6, 15, 0, '2018-10-04 07:20:48', '2018-10-04 07:20:48'),
(42, 5, 1, 2, 6, 19, 0, '2018-10-04 07:20:50', '2018-10-04 07:20:50'),
(43, 1, 1, 2, 6, 2, 1, '2018-10-04 07:21:14', '2018-10-04 07:21:15'),
(44, 2, 1, 2, 6, 7, 1, '2018-10-04 07:21:16', '2018-10-04 07:21:17'),
(45, 3, 1, 2, 6, 12, 1, '2018-10-04 07:21:19', '2018-10-04 07:21:19'),
(46, 4, 1, 2, 6, 13, 1, '2018-10-04 07:21:21', '2018-10-04 07:21:21'),
(47, 1, 1, 2, 6, 3, 0, '2018-10-04 07:21:25', '2018-10-04 07:21:25'),
(48, 2, 1, 2, 6, 7, 1, '2018-10-04 07:21:27', '2018-10-04 07:21:27'),
(49, 3, 1, 2, 6, 11, 0, '2018-10-04 07:21:29', '2018-10-04 07:21:29'),
(50, 4, 1, 2, 6, 15, 0, '2018-10-04 07:21:31', '2018-10-04 07:21:31'),
(51, 5, 1, 2, 6, 19, 0, '2018-10-04 07:21:33', '2018-10-04 07:21:33'),
(52, 27, 1, 4, 6, 106, 1, '2018-10-04 07:26:43', '2018-10-04 07:26:43'),
(53, 28, 1, 4, 6, 108, 0, '2018-10-04 07:26:56', '2018-10-04 07:26:56'),
(54, 29, 1, 4, 6, 113, 1, '2018-10-04 07:26:59', '2018-10-04 07:27:00'),
(55, 30, 1, 4, 6, 119, 1, '2018-10-04 07:27:03', '2018-10-04 07:27:03'),
(56, 31, 1, 4, 6, 123, 1, '2018-10-04 07:27:06', '2018-10-04 07:27:07'),
(57, 1, 1, 2, 6, 2, 1, '2018-10-04 07:34:40', '2018-10-04 07:34:40'),
(58, 2, 1, 2, 6, 7, 1, '2018-10-04 07:34:43', '2018-10-04 07:34:43'),
(59, 3, 1, 2, 6, 12, 1, '2018-10-04 07:34:45', '2018-10-04 07:34:45'),
(60, 4, 1, 2, 6, 13, 1, '2018-10-04 07:34:47', '2018-10-04 07:34:47'),
(61, 5, 1, 2, 6, 18, 1, '2018-10-04 07:34:49', '2018-10-04 07:34:49'),
(62, 17, 1, 3, 6, 63, 1, '2018-10-04 07:35:03', '2018-10-04 07:35:03'),
(63, 18, 1, 3, 6, 70, 1, '2018-10-04 07:35:07', '2018-10-04 07:35:07'),
(64, 19, 1, 3, 6, 73, 1, '2018-10-04 07:35:11', '2018-10-04 07:35:11'),
(65, 20, 1, 3, 6, 77, 1, '2018-10-04 07:35:15', '2018-10-04 07:35:15'),
(66, 21, 1, 3, 6, 80, 1, '2018-10-04 07:35:19', '2018-10-04 07:35:19'),
(67, 27, 1, 4, 6, 106, 1, '2018-11-04 12:55:04', '2018-11-04 12:55:04'),
(68, 28, 1, 4, 6, 111, 0, '2018-11-04 12:55:13', '2018-11-04 12:55:13'),
(69, 29, 1, 4, 6, 113, 1, '2018-11-04 12:55:20', '2018-11-04 12:55:20'),
(70, 30, 1, 4, 6, 119, 1, '2018-11-04 12:55:24', '2018-11-04 12:55:24'),
(71, 31, 1, 4, 6, 123, 1, '2018-11-04 12:55:28', '2018-11-04 12:55:28'),
(72, 22, 1, 6, 6, 85, 1, '2018-11-04 12:55:54', '2018-11-04 12:55:54'),
(73, 22, 1, 6, 6, 85, 1, '2018-11-04 12:56:01', '2018-11-04 12:56:01'),
(74, 23, 1, 6, 6, 88, 1, '2018-11-04 12:56:06', '2018-11-04 12:56:06'),
(75, 24, 1, 6, 6, 95, 1, '2018-11-04 12:56:14', '2018-11-04 12:56:14'),
(76, 25, 1, 6, 6, 97, 1, '2018-11-04 12:56:18', '2018-11-04 12:56:18'),
(77, 26, 1, 6, 6, 101, 1, '2018-11-04 12:56:26', '2018-11-04 12:56:26'),
(78, 22, 1, 6, 6, 86, 0, '2018-11-05 17:05:47', '2018-11-05 17:05:47'),
(79, 23, 1, 6, 6, 90, 0, '2018-11-05 17:05:49', '2018-11-05 17:05:49'),
(80, 24, 1, 6, 6, 93, 0, '2018-11-05 17:05:51', '2018-11-05 17:05:51'),
(81, 25, 1, 6, 6, 98, 0, '2018-11-05 17:05:56', '2018-11-05 17:05:56'),
(82, 22, 1, 6, 6, 86, 0, '2018-11-05 17:08:55', '2018-11-05 17:08:55'),
(83, 23, 1, 6, 6, 90, 0, '2018-11-05 17:08:58', '2018-11-05 17:08:58'),
(84, 24, 1, 6, 6, 94, 0, '2018-11-05 17:08:59', '2018-11-05 17:08:59'),
(85, 25, 1, 6, 6, 99, 0, '2018-11-05 17:09:02', '2018-11-05 17:09:02'),
(86, 27, 1, 4, 6, 106, 1, '2019-08-27 17:44:31', '2019-08-27 17:44:31'),
(87, 28, 1, 4, 6, 111, 0, '2019-08-27 17:44:40', '2019-08-27 17:44:40'),
(88, 29, 1, 4, 6, 113, 1, '2019-08-27 17:44:45', '2019-08-27 17:44:45'),
(89, 30, 1, 4, 6, 119, 1, '2019-08-27 17:44:50', '2019-08-27 17:44:50'),
(90, 31, 1, 4, 6, 123, 1, '2019-08-27 17:44:55', '2019-08-27 17:44:55'),
(91, 1, 1, 2, 6, 2, 1, '2019-08-28 12:03:27', '2019-08-28 12:03:27'),
(92, 2, 1, 2, 6, 6, 1, '2019-08-28 12:03:30', '2019-08-28 12:03:30'),
(93, 3, 1, 2, 6, 12, 1, '2019-08-28 12:03:33', '2019-08-28 12:03:33'),
(94, 4, 1, 2, 6, 14, 1, '2019-08-28 12:03:36', '2019-08-28 12:03:36'),
(95, 5, 1, 2, 6, 19, 0, '2019-08-28 12:03:39', '2019-08-28 12:03:39'),
(96, 34, 1, 42, 82, 127, 0, '2019-10-28 21:13:26', '2019-10-28 21:13:26'),
(97, 34, 1, 42, 82, 127, 0, '2019-10-28 21:13:32', '2019-10-28 21:13:32'),
(98, 34, 1, 42, 82, 128, 0, '2019-10-28 21:13:39', '2019-10-28 21:13:39'),
(99, 34, 1, 42, 82, 129, 1, '2019-10-28 21:13:46', '2019-10-28 21:13:46'),
(100, 34, 1, 42, 82, 130, 0, '2019-10-28 21:13:52', '2019-10-28 21:13:52'),
(101, 34, 1, 42, 82, 128, 0, '2019-10-28 21:14:41', '2019-10-28 21:14:41'),
(102, 34, 1, 42, 82, 129, 1, '2019-10-28 21:14:46', '2019-10-28 21:14:46'),
(103, 34, 1, 42, 82, 130, 0, '2019-10-28 21:14:50', '2019-10-28 21:14:50'),
(104, 34, 1, 42, 82, 130, 0, '2019-10-28 21:15:32', '2019-10-28 21:15:32'),
(105, 35, 1, 44, 82, 131, 1, '2019-10-28 21:30:36', '2019-10-28 21:30:36'),
(106, 36, 1, 45, 82, 138, 1, '2019-10-28 21:36:22', '2019-10-28 21:36:22'),
(107, 37, 1, 46, 82, 140, 1, '2019-10-28 21:43:38', '2019-10-28 21:43:38'),
(108, 34, 1, 42, 82, 129, 1, '2019-10-29 11:00:54', '2019-10-29 11:00:54'),
(109, 39, 1, 47, 82, 146, 1, '2019-10-29 20:51:21', '2019-10-29 20:51:21'),
(110, 34, 1, 42, 82, 127, 0, '2019-10-31 11:42:14', '2019-10-31 11:42:14'),
(111, 34, 1, 42, 82, 128, 0, '2019-10-31 11:42:59', '2019-10-31 11:42:59'),
(112, 34, 1, 42, 82, 128, 0, '2019-11-01 20:30:43', '2019-11-01 20:30:43'),
(113, 34, 1, 42, 82, 129, 1, '2019-11-01 20:30:47', '2019-11-01 20:30:47'),
(114, 34, 1, 42, 82, 130, 0, '2019-11-01 20:30:50', '2019-11-01 20:30:50'),
(115, 34, 1, 42, 82, 127, 0, '2019-11-01 20:30:53', '2019-11-01 20:30:53'),
(116, 40, 1, 48, 82, 149, 0, '2019-11-02 15:21:45', '2019-11-02 15:21:45'),
(117, 34, 1, 42, 82, 129, 1, '2019-11-02 15:22:17', '2019-11-02 15:22:17'),
(118, 34, 1, 42, 82, 129, 1, '2019-11-02 15:23:15', '2019-11-02 15:23:15'),
(119, 34, 1, 42, 82, 130, 0, '2019-11-02 15:23:20', '2019-11-02 15:23:20'),
(120, 34, 1, 42, 82, 127, 0, '2019-11-02 21:16:16', '2019-11-02 21:16:16'),
(121, 34, 1, 42, 82, 128, 0, '2019-11-02 21:16:20', '2019-11-02 21:16:20'),
(122, 34, 1, 42, 82, 129, 1, '2019-11-02 21:17:33', '2019-11-02 21:17:33'),
(123, 34, 1, 42, 82, 129, 1, '2019-11-02 21:18:14', '2019-11-02 21:18:14'),
(124, 34, 1, 42, 82, 128, 0, '2019-11-02 21:19:00', '2019-11-02 21:19:00'),
(125, 34, 1, 42, 82, 129, 1, '2019-11-02 21:19:03', '2019-11-02 21:19:03'),
(126, 34, 1, 42, 82, 127, 0, '2019-11-02 21:44:49', '2019-11-02 21:44:49'),
(127, 34, 1, 42, 82, 127, 0, '2019-11-02 21:44:56', '2019-11-02 21:44:56'),
(128, 35, 1, 44, 82, 131, 1, '2019-11-02 22:21:15', '2019-11-02 22:21:15'),
(129, 35, 1, 44, 82, 132, 0, '2019-11-02 22:21:19', '2019-11-02 22:21:19'),
(130, 36, 1, 45, 82, 137, 0, '2019-11-04 13:20:07', '2019-11-04 13:20:07'),
(131, 34, 1, 42, 82, 128, 0, '2019-11-04 13:57:57', '2019-11-04 13:57:57'),
(132, 35, 1, 44, 82, 133, 0, '2019-11-04 13:58:27', '2019-11-04 13:58:27'),
(133, 39, 1, 47, 82, 144, 0, '2019-11-07 13:29:02', '2019-11-07 13:29:02'),
(134, 39, 1, 47, 82, 147, 0, '2019-11-07 13:29:09', '2019-11-07 13:29:09'),
(135, 39, 1, 47, 82, 146, 1, '2019-11-07 13:29:13', '2019-11-07 13:29:13'),
(136, 35, 1, 44, 82, 131, 1, '2019-11-07 13:30:00', '2019-11-07 13:30:00'),
(137, 34, 1, 42, 82, 129, 1, '2019-11-07 13:51:38', '2019-11-07 13:51:38'),
(138, 27, 1, 4, 6, 105, 0, '2020-02-14 01:20:09', '2020-02-14 01:20:09'),
(139, 28, 1, 4, 6, 109, 0, '2020-02-14 01:20:13', '2020-02-14 01:20:13'),
(140, 29, 1, 4, 6, 115, 0, '2020-02-14 01:20:15', '2020-02-14 01:20:15'),
(141, 30, 1, 4, 6, 119, 1, '2020-02-14 01:20:17', '2020-02-14 01:20:17'),
(142, 31, 1, 4, 6, 123, 1, '2020-02-14 01:20:19', '2020-02-14 01:20:19'),
(143, 60, 21, 11, 6, 171, 0, '2020-04-15 03:57:18', '2020-04-15 03:57:18'),
(144, 63, 21, 11, 6, 178, 1, '2020-04-15 03:58:16', '2020-04-15 03:58:16'),
(145, 64, 21, 11, 6, 180, 1, '2020-04-15 03:58:21', '2020-04-15 03:58:22'),
(146, 65, 21, 24, 6, 181, 1, '2020-04-15 23:49:58', '2020-04-15 23:49:58'),
(147, 47, 21, 24, 6, 165, 1, '2020-04-15 23:50:31', '2020-04-15 23:50:31'),
(148, 45, 21, 24, 6, 162, 0, '2020-04-15 23:50:37', '2020-04-15 23:50:37'),
(149, 65, 21, 24, 6, 181, 1, '2020-04-15 23:55:51', '2020-04-15 23:55:51'),
(150, 46, 21, 24, 6, 163, 1, '2020-04-15 23:56:04', '2020-04-15 23:56:04'),
(151, 47, 21, 24, 6, 165, 1, '2020-04-15 23:56:19', '2020-04-15 23:56:20'),
(152, 45, 21, 24, 6, 161, 1, '2020-04-15 23:56:25', '2020-04-15 23:56:25'),
(153, 45, 21, 24, 6, 161, 1, '2020-04-15 23:57:42', '2020-04-15 23:57:42'),
(154, 65, 21, 24, 6, 181, 1, '2020-04-16 00:03:27', '2020-04-16 00:03:27'),
(155, 45, 21, 24, 6, 161, 1, '2020-04-16 00:03:34', '2020-04-16 00:03:34'),
(156, 47, 21, 24, 6, 165, 1, '2020-04-16 00:04:40', '2020-04-16 00:04:40'),
(157, 47, 21, 24, 6, 165, 1, '2020-04-16 00:05:33', '2020-04-16 00:05:34'),
(158, 46, 21, 24, 6, 163, 1, '2020-04-16 00:05:40', '2020-04-16 00:05:40'),
(159, 45, 21, 24, 6, 161, 1, '2020-04-16 00:07:03', '2020-04-16 00:07:03'),
(160, 59, 21, 24, 6, 183, 1, '2020-04-16 00:07:46', '2020-04-16 00:07:47'),
(161, 47, 21, 24, 6, 165, 1, '2020-04-16 00:10:29', '2020-04-16 00:10:30'),
(162, 65, 21, 24, 6, 182, 0, '2020-04-16 00:10:34', '2020-04-16 00:10:34'),
(163, 45, 21, 24, 6, 162, 0, '2020-04-16 00:10:37', '2020-04-16 00:10:37'),
(164, 59, 21, 24, 6, 184, 0, '2020-04-16 00:10:39', '2020-04-16 00:10:39'),
(165, 58, 21, 24, 6, 186, 0, '2020-04-16 00:10:54', '2020-04-16 00:10:54'),
(166, 46, 21, 24, 6, 164, 0, '2020-04-16 00:10:57', '2020-04-16 00:10:57'),
(167, 46, 21, 24, 6, 164, 0, '2020-04-16 00:12:54', '2020-04-16 00:12:54'),
(168, 47, 21, 24, 6, 166, 0, '2020-04-16 00:14:32', '2020-04-16 00:14:32'),
(169, 45, 21, 24, 6, 161, 1, '2020-04-16 00:14:34', '2020-04-16 00:14:34'),
(170, 59, 21, 24, 6, 184, 0, '2020-04-16 00:14:35', '2020-04-16 00:14:35'),
(171, 46, 21, 24, 6, 164, 0, '2020-04-16 00:14:40', '2020-04-16 00:14:40'),
(172, 65, 21, 24, 6, 182, 0, '2020-04-16 00:14:42', '2020-04-16 00:14:42'),
(173, 45, 21, 24, 6, 162, 0, '2020-04-16 00:14:54', '2020-04-16 00:14:54'),
(174, 47, 21, 24, 6, 166, 0, '2020-04-16 00:14:56', '2020-04-16 00:14:56'),
(175, 46, 21, 24, 6, 164, 0, '2020-04-16 00:14:59', '2020-04-16 00:14:59'),
(176, 65, 21, 24, 6, 182, 0, '2020-04-16 00:15:03', '2020-04-16 00:15:03'),
(177, 58, 21, 24, 6, 186, 0, '2020-04-16 00:15:05', '2020-04-16 00:15:05'),
(178, 59, 21, 24, 6, 184, 0, '2020-04-16 00:15:07', '2020-04-16 00:15:07'),
(179, 45, 21, 24, 6, 162, 0, '2020-04-16 00:15:50', '2020-04-16 00:15:50'),
(180, 59, 21, 24, 6, 184, 0, '2020-04-16 00:15:54', '2020-04-16 00:15:54'),
(181, 58, 21, 24, 6, 186, 0, '2020-04-16 00:15:57', '2020-04-16 00:15:57'),
(182, 47, 21, 24, 6, 166, 0, '2020-04-16 00:15:59', '2020-04-16 00:15:59'),
(183, 65, 21, 24, 6, 182, 0, '2020-04-16 00:16:02', '2020-04-16 00:16:02'),
(184, 46, 21, 24, 6, 164, 0, '2020-04-16 00:16:05', '2020-04-16 00:16:05'),
(185, 47, 21, 24, 6, 165, 1, '2020-04-16 00:24:54', '2020-04-16 00:24:54'),
(186, 45, 21, 24, 6, 161, 1, '2020-04-16 00:24:56', '2020-04-16 00:24:56'),
(187, 46, 21, 24, 6, 163, 1, '2020-04-16 00:24:59', '2020-04-16 00:24:59'),
(188, 65, 21, 24, 6, 181, 1, '2020-04-16 00:25:01', '2020-04-16 00:25:01'),
(189, 59, 21, 24, 6, 183, 1, '2020-04-16 00:25:05', '2020-04-16 00:25:05'),
(190, 58, 21, 24, 6, 185, 1, '2020-04-16 00:25:07', '2020-04-16 00:25:07'),
(191, 65, 21, 24, 6, 181, 1, '2020-04-16 00:38:57', '2020-04-16 00:38:58'),
(192, 47, 21, 24, 6, 165, 1, '2020-04-16 00:39:00', '2020-04-16 00:39:00'),
(193, 45, 21, 24, 6, 161, 1, '2020-04-16 00:39:02', '2020-04-16 00:39:02'),
(194, 59, 21, 24, 6, 183, 1, '2020-04-16 00:39:04', '2020-04-16 00:39:04'),
(195, 58, 21, 24, 6, 185, 1, '2020-04-16 00:39:06', '2020-04-16 00:39:06'),
(196, 46, 21, 24, 6, 163, 1, '2020-04-16 00:39:08', '2020-04-16 00:39:08'),
(197, 47, 21, 24, 6, 166, 0, '2020-04-16 00:55:51', '2020-04-16 00:55:51'),
(198, 65, 21, 24, 6, 181, 1, '2020-04-16 01:08:27', '2020-04-16 01:08:27'),
(199, 47, 21, 24, 6, 165, 1, '2020-04-16 01:08:30', '2020-04-16 01:08:30'),
(200, 45, 21, 24, 6, 162, 0, '2020-04-16 01:08:34', '2020-04-16 01:08:34'),
(201, 46, 21, 24, 6, 163, 1, '2020-04-16 01:08:37', '2020-04-16 01:08:37'),
(202, 59, 21, 24, 6, 184, 0, '2020-04-16 01:08:39', '2020-04-16 01:08:39'),
(203, 58, 21, 24, 6, 185, 1, '2020-04-16 01:08:42', '2020-04-16 01:08:42'),
(204, 46, 21, 24, 6, 164, 0, '2020-04-16 01:11:11', '2020-04-16 01:11:11'),
(205, 65, 21, 24, 6, 182, 0, '2020-04-16 01:11:13', '2020-04-16 01:11:13'),
(206, 59, 21, 24, 6, 184, 0, '2020-04-16 01:11:15', '2020-04-16 01:11:15'),
(207, 45, 21, 24, 6, 162, 0, '2020-04-16 01:11:17', '2020-04-16 01:11:17'),
(208, 58, 21, 24, 6, 186, 0, '2020-04-16 01:11:19', '2020-04-16 01:11:19'),
(209, 47, 21, 24, 6, 166, 0, '2020-04-16 01:11:20', '2020-04-16 01:11:20'),
(210, 45, 21, 24, 6, 162, 0, '2020-04-16 01:13:16', '2020-04-16 01:13:16'),
(211, 58, 21, 24, 6, 186, 0, '2020-04-16 01:13:17', '2020-04-16 01:13:17'),
(212, 65, 21, 24, 6, 182, 0, '2020-04-16 01:13:21', '2020-04-16 01:13:21'),
(213, 46, 21, 24, 6, 164, 0, '2020-04-16 01:13:23', '2020-04-16 01:13:23'),
(214, 47, 21, 24, 6, 166, 0, '2020-04-16 01:13:25', '2020-04-16 01:13:25'),
(215, 59, 21, 24, 6, 184, 0, '2020-04-16 01:13:27', '2020-04-16 01:13:27'),
(216, 47, 21, 24, 6, 166, 0, '2020-04-16 01:18:39', '2020-04-16 01:18:39'),
(217, 59, 21, 24, 6, 184, 0, '2020-04-16 01:18:42', '2020-04-16 01:18:42'),
(218, 45, 21, 24, 6, 162, 0, '2020-04-16 01:26:57', '2020-04-16 01:26:57'),
(219, 58, 21, 24, 6, 186, 0, '2020-04-16 01:26:59', '2020-04-16 01:26:59'),
(220, 47, 21, 24, 6, 166, 0, '2020-04-16 01:27:01', '2020-04-16 01:27:01'),
(221, 65, 21, 24, 6, 182, 0, '2020-04-16 01:27:04', '2020-04-16 01:27:04'),
(222, 59, 21, 24, 6, 184, 0, '2020-04-16 01:27:07', '2020-04-16 01:27:07'),
(223, 46, 21, 24, 6, 164, 0, '2020-04-16 01:27:10', '2020-04-16 01:27:10'),
(224, 59, 21, 24, 6, 184, 0, '2020-04-16 01:37:43', '2020-04-16 01:37:43'),
(225, 45, 21, 24, 6, 161, 1, '2020-04-16 01:37:49', '2020-04-16 01:37:49'),
(226, 58, 21, 24, 6, 185, 1, '2020-04-16 01:37:51', '2020-04-16 01:37:51'),
(227, 47, 21, 24, 6, 165, 1, '2020-04-16 01:37:53', '2020-04-16 01:37:53'),
(228, 46, 21, 24, 6, 163, 1, '2020-04-16 01:37:55', '2020-04-16 01:37:55'),
(229, 65, 21, 24, 6, 181, 1, '2020-04-16 01:37:57', '2020-04-16 01:37:57'),
(230, 65, 21, 24, 6, 182, 0, '2020-04-16 01:45:44', '2020-04-16 01:45:44'),
(231, 65, 11, 24, 6, 181, 1, '2020-04-16 04:39:45', '2020-04-16 04:39:45'),
(232, 45, 11, 24, 6, 161, 1, '2020-04-16 04:39:47', '2020-04-16 04:39:47'),
(233, 58, 11, 24, 6, 185, 1, '2020-04-16 04:39:50', '2020-04-16 04:39:50'),
(234, 47, 11, 24, 6, 165, 1, '2020-04-16 04:39:53', '2020-04-16 04:39:53'),
(235, 46, 11, 24, 6, 163, 1, '2020-04-16 04:39:55', '2020-04-16 04:39:55'),
(236, 59, 11, 24, 6, 183, 1, '2020-04-16 04:39:58', '2020-04-16 04:39:58'),
(237, 45, 11, 24, 6, 161, 1, '2020-04-16 04:53:33', '2020-04-16 04:53:33'),
(238, 58, 11, 24, 6, 185, 1, '2020-04-16 05:04:03', '2020-04-16 05:04:04'),
(239, 47, 11, 24, 6, 165, 1, '2020-04-16 05:04:06', '2020-04-16 05:04:06'),
(240, 46, 11, 24, 6, 163, 1, '2020-04-16 05:04:12', '2020-04-16 05:04:12'),
(241, 45, 11, 24, 6, 161, 1, '2020-04-16 05:04:14', '2020-04-16 05:04:14'),
(242, 65, 11, 24, 6, 188, 0, '2020-04-16 05:06:00', '2020-04-16 05:06:00'),
(243, 46, 11, 24, 6, 163, 1, '2020-04-16 05:06:39', '2020-04-16 05:06:39'),
(244, 65, 11, 24, 6, 181, 1, '2020-04-16 05:06:52', '2020-04-16 05:06:52'),
(245, 45, 11, 24, 6, 161, 1, '2020-04-16 05:07:04', '2020-04-16 05:07:04'),
(246, 47, 11, 24, 6, 165, 1, '2020-04-16 05:07:06', '2020-04-16 05:07:06'),
(247, 58, 11, 24, 6, 185, 1, '2020-04-16 05:07:08', '2020-04-16 05:07:08'),
(248, 59, 11, 24, 6, 183, 1, '2020-04-16 05:07:11', '2020-04-16 05:07:11'),
(249, 58, 11, 24, 6, 185, 1, '2020-04-16 05:10:30', '2020-04-16 05:10:30'),
(250, 45, 11, 24, 6, 161, 1, '2020-04-16 05:10:32', '2020-04-16 05:10:32'),
(251, 59, 11, 24, 6, 183, 1, '2020-04-16 05:10:33', '2020-04-16 05:10:34'),
(252, 47, 11, 24, 6, 165, 1, '2020-04-16 05:10:36', '2020-04-16 05:10:36'),
(253, 46, 11, 24, 6, 163, 1, '2020-04-16 05:10:37', '2020-04-16 05:10:37'),
(254, 65, 11, 24, 6, 181, 1, '2020-04-16 05:10:39', '2020-04-16 05:10:39'),
(255, 66, 11, 21, 6, 189, 1, '2020-04-16 05:14:31', '2020-04-16 05:14:31'),
(256, 67, 11, 21, 6, 191, 1, '2020-04-16 05:14:33', '2020-04-16 05:14:33'),
(257, 68, 11, 21, 6, 193, 1, '2020-04-16 05:14:35', '2020-04-16 05:14:35'),
(258, 69, 11, 21, 6, 195, 1, '2020-04-16 05:14:37', '2020-04-16 05:14:37'),
(259, 70, 11, 21, 6, 197, 1, '2020-04-16 05:14:39', '2020-04-16 05:14:39'),
(260, 47, 21, 24, 6, 166, 0, '2020-04-16 05:19:27', '2020-04-16 05:19:27'),
(261, 47, 21, 24, 6, 166, 0, '2020-04-16 05:24:51', '2020-04-16 05:24:51'),
(262, 59, 21, 24, 6, 183, 1, '2020-04-16 05:24:52', '2020-04-16 05:24:52'),
(263, 46, 11, 24, 6, 163, 1, '2020-04-16 05:24:53', '2020-04-16 05:24:53'),
(264, 59, 11, 24, 6, 183, 1, '2020-04-16 05:24:55', '2020-04-16 05:24:55'),
(265, 45, 11, 24, 6, 161, 1, '2020-04-16 05:24:56', '2020-04-16 05:24:56'),
(266, 58, 11, 24, 6, 185, 1, '2020-04-16 05:25:33', '2020-04-16 05:25:33'),
(267, 59, 11, 24, 6, 183, 1, '2020-04-16 05:25:35', '2020-04-16 05:25:35'),
(268, 47, 21, 24, 6, 165, 1, '2020-04-16 05:25:40', '2020-04-16 05:25:40'),
(269, 71, 11, 10, 6, 199, 1, '2020-04-16 08:18:46', '2020-04-16 08:18:46'),
(270, 72, 11, 10, 6, 201, 1, '2020-04-16 08:18:48', '2020-04-16 08:18:48'),
(271, 73, 11, 10, 6, 203, 1, '2020-04-16 08:18:49', '2020-04-16 08:18:49'),
(272, 74, 11, 10, 6, 205, 1, '2020-04-16 08:18:50', '2020-04-16 08:18:50'),
(273, 75, 11, 10, 6, 207, 1, '2020-04-16 08:18:52', '2020-04-16 08:18:52'),
(274, 71, 11, 10, 6, 199, 1, '2020-04-16 08:19:40', '2020-04-16 08:19:41'),
(275, 72, 11, 10, 6, 201, 1, '2020-04-16 08:19:45', '2020-04-16 08:19:45'),
(276, 73, 11, 10, 6, 203, 1, '2020-04-16 08:19:47', '2020-04-16 08:19:47'),
(277, 74, 11, 10, 6, 205, 1, '2020-04-16 08:19:48', '2020-04-16 08:19:49'),
(278, 75, 11, 10, 6, 207, 1, '2020-04-16 08:19:50', '2020-04-16 08:19:50'),
(279, 71, 11, 10, 6, 199, 1, '2020-04-16 08:26:38', '2020-04-16 08:26:39'),
(280, 72, 11, 10, 6, 201, 1, '2020-04-16 08:26:40', '2020-04-16 08:26:40'),
(281, 73, 11, 10, 6, 203, 1, '2020-04-16 08:26:41', '2020-04-16 08:26:41'),
(282, 74, 11, 10, 6, 205, 1, '2020-04-16 08:26:43', '2020-04-16 08:26:43'),
(283, 75, 11, 10, 6, 207, 1, '2020-04-16 08:26:44', '2020-04-16 08:26:44'),
(284, 3, 11, 2, 6, 9, 0, '2020-04-16 08:42:41', '2020-04-16 08:42:41'),
(285, 2, 11, 2, 6, 5, 0, '2020-04-16 08:42:42', '2020-04-16 08:42:42'),
(286, 5, 11, 2, 6, 17, 0, '2020-04-16 08:42:45', '2020-04-16 08:42:45'),
(287, 4, 11, 2, 6, 13, 1, '2020-04-16 08:42:46', '2020-04-16 08:42:46'),
(288, 43, 11, 2, 6, 156, 1, '2020-04-16 08:42:48', '2020-04-16 08:42:48'),
(289, 3, 11, 2, 6, 9, 1, '2020-04-16 08:47:03', '2020-04-16 08:47:03'),
(290, 2, 11, 2, 6, 5, 1, '2020-04-16 08:47:04', '2020-04-16 08:47:04'),
(291, 43, 11, 2, 6, 156, 1, '2020-04-16 08:47:06', '2020-04-16 08:47:06'),
(292, 5, 11, 2, 6, 17, 1, '2020-04-16 08:47:09', '2020-04-16 08:47:09'),
(293, 1, 11, 2, 6, 1, 1, '2020-04-16 08:47:10', '2020-04-16 08:47:10'),
(294, 27, 11, 4, 6, 104, 0, '2020-04-16 09:00:53', '2020-04-16 09:00:53'),
(295, 28, 11, 4, 6, 108, 0, '2020-04-16 09:00:55', '2020-04-16 09:00:55'),
(296, 29, 11, 4, 6, 112, 0, '2020-04-16 09:00:56', '2020-04-16 09:00:56'),
(297, 30, 11, 4, 6, 116, 0, '2020-04-16 09:00:58', '2020-04-16 09:00:58'),
(298, 31, 11, 4, 6, 120, 1, '2020-04-16 09:01:00', '2020-04-16 09:01:00'),
(299, 17, 11, 3, 6, 63, 1, '2020-04-16 09:01:42', '2020-04-16 09:01:42'),
(300, 18, 11, 3, 6, 67, 1, '2020-04-16 09:01:44', '2020-04-16 09:01:44'),
(301, 19, 11, 3, 6, 72, 1, '2020-04-16 09:01:46', '2020-04-16 09:01:46'),
(302, 20, 11, 3, 6, 76, 1, '2020-04-16 09:01:47', '2020-04-16 09:01:47'),
(303, 21, 11, 3, 6, 80, 1, '2020-04-16 09:01:49', '2020-04-16 09:01:49'),
(304, 1, 11, 2, 6, 1, 1, '2020-04-16 09:03:27', '2020-04-16 09:03:27'),
(305, 5, 11, 2, 6, 17, 1, '2020-04-16 09:03:29', '2020-04-16 09:03:29'),
(306, 3, 11, 2, 6, 9, 1, '2020-04-16 09:03:30', '2020-04-16 09:03:30'),
(307, 2, 11, 2, 6, 5, 1, '2020-04-16 09:03:31', '2020-04-16 09:03:31'),
(308, 43, 11, 2, 6, 156, 1, '2020-04-16 09:03:34', '2020-04-16 09:03:34'),
(309, 4, 11, 2, 6, 13, 1, '2020-04-16 09:03:35', '2020-04-16 09:03:35'),
(310, 22, 11, 6, 6, 84, 1, '2020-04-16 09:05:51', '2020-04-16 09:05:51'),
(311, 23, 11, 6, 6, 88, 1, '2020-04-16 09:05:52', '2020-04-16 09:05:52'),
(312, 24, 11, 6, 6, 92, 1, '2020-04-16 09:05:53', '2020-04-16 09:05:53'),
(313, 25, 11, 6, 6, 96, 1, '2020-04-16 09:05:55', '2020-04-16 09:05:55'),
(314, 26, 11, 6, 6, 100, 1, '2020-04-16 09:05:57', '2020-04-16 09:05:57'),
(315, 45, 11, 24, 6, 161, 1, '2020-04-16 23:45:17', '2020-04-16 23:45:18'),
(316, 47, 11, 24, 6, 165, 1, '2020-04-16 23:45:19', '2020-04-16 23:45:20'),
(317, 46, 11, 24, 6, 163, 1, '2020-04-16 23:45:26', '2020-04-16 23:45:26'),
(318, 47, 11, 24, 6, 166, 0, '2020-04-17 00:03:04', '2020-04-17 00:03:04'),
(319, 58, 11, 24, 6, 211, 0, '2020-04-17 00:03:06', '2020-04-17 00:03:06'),
(320, 47, 11, 24, 6, 165, 1, '2020-04-17 00:03:11', '2020-04-17 00:03:11'),
(321, 65, 11, 24, 6, 181, 1, '2020-04-17 00:03:17', '2020-04-17 00:03:17'),
(322, 47, 11, 24, 6, 166, 0, '2020-04-17 00:04:19', '2020-04-17 00:04:19'),
(323, 65, 11, 24, 6, 182, 0, '2020-04-17 00:04:20', '2020-04-17 00:04:20'),
(324, 58, 11, 24, 6, 186, 0, '2020-04-17 00:04:24', '2020-04-17 00:04:24'),
(325, 46, 11, 24, 6, 164, 0, '2020-04-17 00:04:26', '2020-04-17 00:04:26'),
(326, 46, 11, 24, 6, 163, 1, '2020-04-17 00:09:53', '2020-04-17 00:09:53'),
(327, 59, 11, 24, 6, 183, 1, '2020-04-17 00:09:56', '2020-04-17 00:09:56'),
(328, 47, 11, 24, 6, 165, 1, '2020-04-17 00:09:57', '2020-04-17 00:09:57'),
(329, 58, 11, 24, 6, 185, 1, '2020-04-17 00:11:12', '2020-04-17 00:11:12'),
(330, 46, 11, 24, 6, 163, 1, '2020-04-17 00:11:15', '2020-04-17 00:11:15'),
(331, 65, 11, 24, 6, 181, 1, '2020-04-17 00:11:17', '2020-04-17 00:11:17'),
(332, 59, 11, 24, 6, 183, 1, '2020-04-17 00:11:23', '2020-04-17 00:11:24'),
(333, 45, 11, 24, 6, 161, 1, '2020-04-17 00:11:26', '2020-04-17 00:11:26'),
(334, 47, 11, 24, 6, 165, 1, '2020-04-17 00:11:28', '2020-04-17 00:11:28'),
(335, 45, 11, 24, 6, 161, 1, '2020-04-17 00:30:38', '2020-04-17 00:30:38'),
(336, 59, 11, 24, 6, 183, 1, '2020-04-17 00:30:41', '2020-04-17 00:30:41'),
(337, 47, 11, 24, 6, 165, 1, '2020-04-17 00:32:00', '2020-04-17 00:32:00'),
(338, 45, 11, 24, 6, 161, 1, '2020-04-17 00:32:04', '2020-04-17 00:32:04'),
(339, 65, 11, 24, 6, 181, 1, '2020-04-17 00:32:09', '2020-04-17 00:32:09'),
(340, 45, 11, 24, 6, 161, 1, '2020-04-17 00:34:18', '2020-04-17 00:34:18'),
(341, 65, 11, 24, 6, 181, 1, '2020-04-17 00:34:20', '2020-04-17 00:34:20'),
(342, 58, 11, 24, 6, 185, 1, '2020-04-17 00:34:22', '2020-04-17 00:34:22'),
(343, 47, 11, 24, 6, 165, 1, '2020-04-17 00:34:24', '2020-04-17 00:34:24'),
(344, 46, 11, 24, 6, 163, 1, '2020-04-17 00:34:26', '2020-04-17 00:34:26'),
(345, 65, 11, 24, 6, 187, 0, '2020-04-17 00:34:26', '2020-04-17 00:34:26'),
(346, 59, 11, 24, 6, 183, 1, '2020-04-17 00:34:30', '2020-04-17 00:34:30'),
(347, 46, 11, 24, 6, 215, 0, '2020-04-17 00:34:31', '2020-04-17 00:34:31'),
(348, 45, 11, 24, 6, 162, 0, '2020-04-17 00:34:32', '2020-04-17 00:34:32'),
(349, 58, 11, 24, 6, 185, 1, '2020-04-17 00:34:34', '2020-04-17 00:34:34'),
(350, 47, 11, 24, 6, 166, 0, '2020-04-17 00:34:36', '2020-04-17 00:34:36'),
(351, 59, 11, 24, 6, 184, 0, '2020-04-17 00:34:38', '2020-04-17 00:34:38'),
(352, 84, 11, 69, 18, 248, 1, '2020-04-17 02:41:57', '2020-04-17 02:41:57'),
(353, 85, 11, 69, 18, 246, 1, '2020-04-17 02:41:58', '2020-04-17 02:41:58'),
(354, 86, 11, 69, 18, 244, 1, '2020-04-17 02:42:00', '2020-04-17 02:42:00'),
(355, 87, 11, 69, 18, 242, 1, '2020-04-17 02:42:03', '2020-04-17 02:42:03'),
(356, 88, 11, 69, 18, 240, 1, '2020-04-17 02:42:06', '2020-04-17 02:42:06'),
(357, 84, 11, 69, 18, 248, 1, '2020-04-17 02:46:29', '2020-04-17 02:46:29'),
(358, 85, 11, 69, 18, 246, 1, '2020-04-17 02:46:30', '2020-04-17 02:46:30'),
(359, 86, 11, 69, 18, 244, 1, '2020-04-17 02:46:31', '2020-04-17 02:46:31'),
(360, 87, 11, 69, 18, 242, 1, '2020-04-17 02:46:33', '2020-04-17 02:46:33'),
(361, 88, 11, 69, 18, 240, 1, '2020-04-17 02:46:35', '2020-04-17 02:46:35'),
(362, 84, 1, 69, 18, 248, 1, '2020-04-17 05:57:49', '2020-04-17 05:57:49'),
(363, 85, 1, 69, 18, 246, 1, '2020-04-17 05:57:51', '2020-04-17 05:57:51'),
(364, 86, 1, 69, 18, 244, 1, '2020-04-17 05:57:53', '2020-04-17 05:57:53'),
(365, 87, 1, 69, 18, 242, 1, '2020-04-17 05:57:55', '2020-04-17 05:57:55'),
(366, 88, 1, 69, 18, 240, 1, '2020-04-17 05:57:57', '2020-04-17 05:57:57'),
(367, 89, 21, 71, 17, 250, 1, '2020-05-11 04:25:52', '2020-05-11 04:25:52'),
(368, 90, 21, 71, 17, 252, 1, '2020-05-11 04:25:53', '2020-05-11 04:25:53'),
(369, 91, 21, 71, 17, 254, 1, '2020-05-11 04:25:55', '2020-05-11 04:25:55'),
(370, 92, 21, 71, 17, 256, 1, '2020-05-11 04:25:57', '2020-05-11 04:25:57'),
(371, 93, 21, 71, 17, 258, 1, '2020-05-11 04:26:00', '2020-05-11 04:26:00'),
(372, 71, 21, 10, 6, 199, 1, '2020-05-27 03:53:58', '2020-05-27 03:53:58'),
(373, 97, 21, 1, 6, 264, 1, '2020-05-27 04:07:53', '2020-05-27 04:07:53'),
(374, 98, 21, 1, 6, 262, 1, '2020-05-27 04:07:57', '2020-05-27 04:07:57'),
(375, 99, 21, 1, 6, 266, 1, '2020-05-27 04:07:58', '2020-05-27 04:07:58'),
(376, 100, 21, 1, 6, 268, 1, '2020-05-27 04:08:00', '2020-05-27 04:08:00'),
(377, 101, 21, 1, 6, 270, 1, '2020-05-27 04:08:02', '2020-05-27 04:08:02');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(11) NOT NULL,
  `session_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_number_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) DEFAULT '100',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `course_id`, `session_number`, `session_number_ar`, `order`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 6, 'one', 'الاولى', 100, 1, 1, NULL, '2020-03-25 03:44:18'),
(2, 6, 'two', 'الثانية', 100, 1, 1, NULL, '2018-09-05 09:36:03'),
(3, 6, 'three', 'الثالثة', 100, NULL, NULL, '2018-06-25 21:00:00', '2018-09-05 09:36:16'),
(7, 17, 'one', '', 1, NULL, 1, '2018-06-28 08:26:55', '2018-06-28 08:26:55'),
(8, 17, 'two', '', 2, NULL, NULL, '2018-06-28 08:27:09', '2018-06-28 08:27:09'),
(12, 82, 'Part 1', 'الجزء الأول', 1, 1, NULL, '2019-10-26 10:14:29', '2019-10-26 10:14:29'),
(13, 82, 'Part 2', 'الجزء الثاني', 2, 1, NULL, '2019-10-28 21:45:00', '2019-10-28 21:45:00'),
(14, 82, 'Part 3', 'الجزء الثالث', 3, 1, NULL, '2019-11-03 20:31:24', '2019-11-03 20:31:24'),
(15, 82, 'Part 4', 'الجزء الرابع', 4, 1, NULL, '2019-11-04 18:52:53', '2019-11-04 18:52:53'),
(16, 82, 'Part 5', 'الجزء الخامس', 5, 1, NULL, '2019-11-04 19:32:07', '2019-11-04 19:32:07'),
(17, 82, 'Part 6', 'الجزء السادس', 6, 1, NULL, '2019-11-05 21:00:50', '2019-11-05 21:00:50'),
(18, 82, 'one l', 'test', 12, 1, NULL, '2020-03-26 00:51:03', '2020-03-26 00:51:03'),
(19, 18, 'one', 'one', 1, 1, NULL, '2020-04-17 02:18:43', '2020-04-17 02:18:43'),
(20, 80, 'one', 'test', 1, 1, NULL, '2020-04-27 06:37:49', '2020-04-27 06:37:50');

-- --------------------------------------------------------

--
-- Table structure for table `slugs`
--

CREATE TABLE `slugs` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_ar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permission` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `href` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slugs`
--

INSERT INTO `slugs` (`id`, `text`, `text_ar`, `order`, `created_at`, `updated_at`, `title_en`, `title_ar`, `permission`, `href`, `parent`, `slug`, `active`) VALUES
(1, 'This is some sample content.', '', 0, '2018-07-15 03:16:16', '2018-07-15 03:18:31', 'Home', 'الصفحة الرئيسية', NULL, '/home', NULL, 'Home', 1),
(3, '<h1 class=\"comntitle\">Contact Us</h1>\r\n<section class=\"contact\" id=\"contact\">\r\n<div class=\"container-fluid p-0\">\r\n<div class=\"row no-gutters\">\r\n<div class=\"col-lg-8 col-xl-7\">\r\n<div class=\"images-wrapper\">\r\n<div class=\"row no-gutters\">\r\n<div class=\"col-sm-6\">\r\n<div class=\"row no-gutters\">\r\n<div class=\"col-6 col-sm-6\"><img class=\"img-fluid\" src=\"../images/last_section_images/1st_image.png\" style=\"padding-right: 3px;padding-bottom: 3px;\" /></div>\r\n\r\n<div class=\"col-6 col-sm-6\"><img class=\"img-fluid\" src=\"../images/last_section_images/2nd_image.png\" style=\"padding-right: 3px;padding-bottom: 3px;\" /></div>\r\n\r\n<div class=\"col-sm-12\"><img class=\"img-fluid\" src=\"../images/last_section_images/4th_image.png\" style=\"padding-right: 3px; padding-top: 3px;\" /></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-sm-6\">\r\n<div class=\"row no-gutters\">\r\n<div class=\"col-sm-12\"><img class=\"img-fluid\" src=\"../images/last_section_images/3rd_image.png\" style=\"padding-left: 3px;padding-bottom: 3px;\" /></div>\r\n\r\n<div class=\"col-6 col-sm-6\"><img class=\"img-fluid\" src=\"../images/last_section_images/5th_image.png\" style=\"padding-left: 3px;padding-top: 3px;\" /></div>\r\n\r\n<div class=\"col-6 col-sm-6\"><img class=\"img-fluid\" src=\"../images/last_section_images/6th_image.png\" style=\"padding-left: 3px;padding-top: 3px;\" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-lg-4 col-xl-5\">\r\n<div class=\"contact-wrapper\">\r\n<div class=\"title\"><span class=\"line\">Cont</span>act Us</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<div class=\"form-group row\"><label class=\"col-md-2 col-lg-3 col-form-label\" for=\"contact-name\">Name</label>\r\n\r\n<div class=\"col-md-10 col-lg-9\"><input class=\"form-control\" id=\"contact-name\" name=\"name\" type=\"text\" /></div>\r\n</div>\r\n\r\n<div class=\"form-group row\"><label class=\"col-md-2 col-lg-3 col-form-label\" for=\"contact-phone\">E-Mail</label>\r\n\r\n<div class=\"col-md-10 col-lg-9\"><input class=\"form-control\" id=\"contact-email\" name=\"email\" type=\"email\" /></div>\r\n</div>\r\n\r\n<div class=\"form-group row\"><label class=\"col-md-2 col-lg-3 col-form-label\" for=\"contact-subject\">Subject</label>\r\n\r\n<div class=\"col-md-10 col-lg-9\"><input class=\"form-control\" id=\"contact-subject\" name=\"subject\" type=\"text\" /></div>\r\n</div>\r\n\r\n<div class=\"form-group row\"><!--\r\n							<label for=\"contact-msg\" class=\"col-md-3 col-lg-4 col-form-label\">Message</label>\r\n							<div class=\"col-md-9 col-lg-8\">\r\n								<input type=\"text\" class=\"form-control\" id=\"contact-msg\" name=\"message\">\r\n							</div>\r\n-->\r\n<div class=\"col-12\"><textarea class=\"form-control mt-3\" id=\"contact-message\" name=\"message\" placeholder=\"Your message...\" rows=\"2\"></textarea></div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-4 offset-md-8\"><button class=\"btn white-border-btn submit w-100 mt-2\" onclick=\"submitContactUsForm();\" type=\"submit\">Send</button></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>', '<h1 class=\"comntitle\">Contact Us</h1>\r\n<section class=\"contact\" id=\"contact\">\r\n<div class=\"container-fluid p-0\">\r\n<div class=\"row no-gutters\">\r\n<div class=\"col-lg-8 col-xl-7\">\r\n<div class=\"images-wrapper\">\r\n<div class=\"row no-gutters\">\r\n<div class=\"col-sm-6\">\r\n<div class=\"row no-gutters\">\r\n<div class=\"col-6 col-sm-6\"><img class=\"img-fluid\" src=\"../images/last_section_images/1st_image.png\" style=\"padding-right: 3px;padding-bottom: 3px;\" /></div>\r\n\r\n<div class=\"col-6 col-sm-6\"><img class=\"img-fluid\" src=\"../images/last_section_images/2nd_image.png\" style=\"padding-right: 3px;padding-bottom: 3px;\" /></div>\r\n\r\n<div class=\"col-sm-12\"><img class=\"img-fluid\" src=\"../images/last_section_images/4th_image.png\" style=\"padding-right: 3px; padding-top: 3px;\" /></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-sm-6\">\r\n<div class=\"row no-gutters\">\r\n<div class=\"col-sm-12\"><img class=\"img-fluid\" src=\"../images/last_section_images/3rd_image.png\" style=\"padding-left: 3px;padding-bottom: 3px;\" /></div>\r\n\r\n<div class=\"col-6 col-sm-6\"><img class=\"img-fluid\" src=\"../images/last_section_images/5th_image.png\" style=\"padding-left: 3px;padding-top: 3px;\" /></div>\r\n\r\n<div class=\"col-6 col-sm-6\"><img class=\"img-fluid\" src=\"../images/last_section_images/6th_image.png\" style=\"padding-left: 3px;padding-top: 3px;\" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-lg-4 col-xl-5\">\r\n<div class=\"contact-wrapper\">\r\n<div class=\"title\"><span class=\"line\">تواصل&nbsp;</span>معنا</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<div class=\"form-group row\"><label class=\"col-md-2 col-lg-3 col-form-label\" for=\"contact-name\">الاسم</label>\r\n\r\n<div class=\"col-md-10 col-lg-9\"><input class=\"form-control\" id=\"contact-name\" name=\"name\" type=\"text\" /></div>\r\n</div>\r\n\r\n<div class=\"form-group row\"><label class=\"col-md-2 col-lg-3 col-form-label\" for=\"contact-phone\">البريد الالكتروني</label>\r\n\r\n<div class=\"col-md-10 col-lg-9\"><input class=\"form-control\" id=\"contact-email\" name=\"email\" type=\"email\" /></div>\r\n</div>\r\n\r\n<div class=\"form-group row\"><label class=\"col-md-2 col-lg-3 col-form-label\" for=\"contact-subject\">الموضوع</label>\r\n\r\n<div class=\"col-md-10 col-lg-9\"><input class=\"form-control\" id=\"contact-subject\" name=\"subject\" type=\"text\" /></div>\r\n</div>\r\n\r\n<div class=\"form-group row\"><!--\r\n							<label for=\"contact-msg\" class=\"col-md-3 col-lg-4 col-form-label\">Message</label>\r\n							<div class=\"col-md-9 col-lg-8\">\r\n								<input type=\"text\" class=\"form-control\" id=\"contact-msg\" name=\"message\">\r\n							</div>\r\n-->\r\n<div class=\"col-12\"><textarea class=\"form-control mt-3\" id=\"contact-message\" name=\"message\" placeholder=\"ادخل رسالتك هنا ...\" rows=\"2\"></textarea></div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-4 offset-md-8\"><button class=\"btn white-border-btn submit w-100 mt-2\" onclick=\"submitContactUsForm();\" type=\"submit\">ارسال</button></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>', 0, '2018-07-14 06:14:32', '2020-04-02 01:26:19', 'Contact', 'تواصل معنا', NULL, NULL, NULL, 'contact-us', 0),
(5, '<p>This is some sample content.</p>', '<p>qwe</p>', 0, '2018-07-15 03:20:18', '2019-11-03 13:58:29', 'Achievements', 'الانجازات', NULL, '/achievements', NULL, 'Achievements', 0),
(6, '<p>This is some sample content.</p>', '<p>asd</p>', 0, '2018-07-15 03:20:43', '2019-10-26 17:22:07', 'Gallery', 'المعرض', NULL, '/gallery', NULL, 'gallery', 0),
(7, '<h1 class=\"comntitle\">About Eureka</h1>\r\n\r\n<p>&ldquo;Eureka&rdquo; is a scientific program specialized in technological education of innovation and engineering. Eureka is considered the first academy in Jordan and the Arab world to develop the innovative capabilities of children in the areas of technology and engineering. Eureka teaches children the basic concepts of engineering and invention so they can transform ideas to useful products and services.</p>', '<h1 class=\"comntitle\">حوليوريكا</h1>\r\n\r\n<p>هذا نص ل&quot;حول يوريكا&quot; بالعربي</p>', 0, '2018-07-15 03:20:43', '2020-03-25 05:10:34', 'about', 'حول يوريكا', NULL, NULL, NULL, 'About-Eureka', 0),
(8, 'This is some sample content.', '', 0, '2018-07-15 03:46:24', '2018-07-26 04:52:40', 'Subscriptions', 'الاشتراكات', NULL, '/subscription', NULL, 'subscriptions', 0),
(9, '<p><span><b>Terms and Conditions &ndash; Sample Template</b></span></p>\r\n\r\n<p>In using this website you are deemed to have read and agreed to the following terms and conditions:</p>\r\n\r\n<p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and any or all Agreements:</p>\r\n\r\n<p>&quot;Client&quot;, &ldquo;You&rdquo; and &ldquo;Your&rdquo; refers to you, the person accessing this website and accepting the Company&rsquo;s terms and conditions. &quot;The Company&quot;, &ldquo;Ourselves&rdquo;, &ldquo;We&rdquo; and &quot;Us&quot;, refers to our Company. &ldquo;Party&rdquo;, &ldquo;Parties&rdquo;, or &ldquo;Us&rdquo;, refers to both the Client and ourselves, or either the Client or ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner, whether by formal meetings of a fixed duration, or any other means, for the express purpose of meeting the Client&rsquo;s needs in respect of provision of the Company&rsquo;s stated services/products, in accordance with and subject to, prevailing [Country Law]. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.&nbsp;&nbsp;</p>\r\n\r\n<p><span><b>Privacy Statement </b></span></p>\r\n\r\n<p>We are committed to protecting your privacy. Authorized employees within the company on a need to know basis only use any information collected from individual customers. We constantly review our systems and data to ensure the best possible service to our customers.</p>\r\n\r\n<p><span><b>Confidentiality </b></span></p>\r\n\r\n<p>Client records are regarded as confidential and therefore will not be divulged to any third party, other than [our manufacturer/supplier(s) and] if legally required to do so to the appropriate authorities. We will not sell, share, or rent your personal information to any third party or use your e-mail address for unsolicited mail. Any emails sent by this Company will only be in connection with the provision of agreed services and products.</p>\r\n\r\n<p><span><b>Disclaimer Exclusions and Limitations</b></span></p>\r\n\r\n<p>The information on this web site is provided on an &quot;as is&quot; basis. To the fullest extent permitted by law, this Company:</p>\r\n\r\n<p>Excludes all representations and warranties relating to this website and its contents or which is or may be provided by any affiliates or any other third party, including in relation to any inaccuracies or commissions in this website and/or the Company&rsquo;s literature; and excludes all liability for damages arising out of or in connection with your use of this website. This includes, without limitation, direct loss, loss of business or profits (whether or not the loss of such profits was foreseeable, arose in the normal course of things or you have advised this Company of the possibility of such potential loss), damage caused to your computer, computer software, systems and programs and the data thereon or any other direct or indirect, consequential and incidental damages.&nbsp; This Company does not however exclude liability for death or personal injury caused by its negligence. The above exclusions and limitations apply only to the extent permitted by law. None of your statutory rights as a consumer are affected.&nbsp;</p>\r\n\r\n<p><span><b>Availability </b></span></p>\r\n\r\n<p>Unless otherwise stated, the services featured on this website are only available within the [country], or in relation to postings from the [country]. All advertising is intended solely for the [country]<b> </b>market. You are solely responsible for evaluating the fitness for a particular purpose of any downloads, programs and text available through this site. Redistribution or republication of any part of this site or its content is prohibited, including such by framing or other similar or any other means, without the express written consent of the Company. The Company does not warrant that the service from this site will be uninterrupted, timely or error free, although it is provided to the best ability. By using this service you thereby indemnify this Company, its employees, agents and affiliates against any loss or damage, in whatever manner, howsoever caused.</p>\r\n\r\n<p><span><b>Cookies </b></span></p>\r\n\r\n<p>Like most interactive web sites this Company&rsquo;s website [or ISP] uses cookies to enable us to retrieve user details for each visit. Cookies are used in our site to enable the functionality of this area and ease of use for those people visiting. Some of our affiliate partners may also use cookies. [If you do not use cookies, delete this clause]</p>\r\n\r\n<p><span><b>Links to this website</b></span></p>\r\n\r\n<p>You may not create a link to any page of this website without our prior written consent. If you do create a link to a page of this website you do so at your own risk and the exclusions and limitations set out above will apply to your use of this website by linking to it.&nbsp;</p>\r\n\r\n<p><span><b>Links from this website</b></span></p>\r\n\r\n<p>We do not monitor or review the content of other party&rsquo;s websites which are linked to from this website. Opinions expressed or material appearing on such websites is not necessarily shared or endorsed by us and should not be regarded as the publisher of such opinions or material. Please be aware that we are not responsible for the privacy practices, or content, of these sites. We encourage our users to be aware when they leave our site &amp; to read the privacy statements of these sites. You should evaluate the security and trustworthiness of any other site connected to this site or accessed through this site yourself, before disclosing any personal information to them. This Company will not accept any responsibility for any loss or damage in whatever manner, howsoever caused, resulting from your disclosure to third parties of personal information.&nbsp;&nbsp;</p>\r\n\r\n<p><span><b>Copyright Notice</b></span></p>\r\n\r\n<p>Copyright and other relevant intellectual property rights exist on all text relating to the Company&rsquo;s services and the Full content of this website. &nbsp;</p>\r\n\r\n<p>This Company&rsquo;s logo is a registered trademark of this Company in the [country]. The brand names and specific services of this Company featured on this web site are trademarked [delete this paragraphed clause if no registered trademark exists].</p>\r\n\r\n<p><span><b>Communication </b></span></p>\r\n\r\n<p>We have several different e-mail addresses for different queries. These, &amp; other contact information, can be found on our {*}</p>\r\n\r\n<p><span><b>Contact Us</b></span></p>\r\n\r\n<p>Link on our website or via Company literature or via the Company&rsquo;s stated telephone, facsimile or mobile telephone numbers.</p>\r\n\r\n<p>[Only need to state this if Limited Company, otherwise proprietors‟/partners‟ home/trading address must be shown, without use of the term: registered]</p>\r\n\r\n<p><span><b>Force Majeure </b></span></p>\r\n\r\n<p>Neither party shall be liable to the other for any failure to perform any obligation under any Agreement which is due to an event beyond the control of such party including but not limited to any terrorism, war, Political insurgence, insurrection, riot, civil unrest, act of civil or military authority, uprising, earthquake, flood or any other natural or manmade eventuality outside of our control, which causes the termination of an agreement or contract entered into, nor which could have been reasonably foreseen. Any Party affected by such event shall forthwith inform the other Party of the same and shall use all reasonable endeavors to comply with the terms and conditions of any Agreement contained herein.&nbsp;&nbsp;</p>\r\n\r\n<p><span><b>General </b></span></p>\r\n\r\n<p>The laws of [country]<b> </b>govern these terms and conditions. By accessing this website [and using our services/buying our products] you consent to these terms and conditions and to the exclusive jurisdiction of the [country] courts in all disputes arising out of such access. If any of these terms are deemed invalid or unenforceable for any reason (including, but not limited to the exclusions and limitations set out above), then the invalid or unenforceable provision will be severed from these terms and the remaining terms will continue to apply. Failure of the Company to enforce any of the provisions set out in these Terms and Conditions and any Agreement, or failure to exercise any option to terminate, shall not be construed as waiver of such provisions and shall not affect the validity of these Terms and Conditions or of any Agreement or any part thereof, or the right thereafter to enforce each and every provision. These Terms and Conditions shall not be amended, modified, varied or supplemented except in writing and signed by duly authorized representatives of the Company.</p>\r\n\r\n<p<span>><b>Notification of Changes</b></span></p>\r\n\r\n<p>The Company reserves the right to change these conditions from time to time as it sees fit and your continued use of the site will signify your acceptance of any adjustment to these terms. If there are any changes to our privacy policy, we will announce that these changes have been made on our home page and on other key pages on our site. If there are any changes in how we use our site customers‟ Personally Identifiable Information, notification by e-mail or postal mail will be made to those affected by this change. Any changes to our privacy policy will be posted on our web site [x] days prior to these changes taking place. You are therefore advised to re-read this statement on a regular basis&nbsp;</p>\r\n\r\n<p>These terms and conditions form part of the Agreement between the Client and us. You&rsquo;re accessing of this website and/or undertaking of a booking or Agreement indicates your understanding, agreement to and acceptance, of the Disclaimer Notice and the full Terms and Conditions contained herein. Your statutory Consumer Rights are unaffected.</p>\r\n\r\n<p>&copy; Eureka Online 2019 All Rights Reserved</p>', '<p><span><b>Terms and Conditions &ndash; Sample Template</b></span></p>\r\n\r\n<p>In using this website you are deemed to have read and agreed to the following terms and conditions:</p>\r\n\r\n<p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and any or all Agreements:</p>\r\n\r\n<p>&quot;Client&quot;, &ldquo;You&rdquo; and &ldquo;Your&rdquo; refers to you, the person accessing this website and accepting the Company&rsquo;s terms and conditions. &quot;The Company&quot;, &ldquo;Ourselves&rdquo;, &ldquo;We&rdquo; and &quot;Us&quot;, refers to our Company. &ldquo;Party&rdquo;, &ldquo;Parties&rdquo;, or &ldquo;Us&rdquo;, refers to both the Client and ourselves, or either the Client or ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner, whether by formal meetings of a fixed duration, or any other means, for the express purpose of meeting the Client&rsquo;s needs in respect of provision of the Company&rsquo;s stated services/products, in accordance with and subject to, prevailing [Country Law]. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.&nbsp;&nbsp;</p>\r\n\r\n<p><span><b>Privacy Statement </b></span></p>\r\n\r\n<p>We are committed to protecting your privacy. Authorized employees within the company on a need to know basis only use any information collected from individual customers. We constantly review our systems and data to ensure the best possible service to our customers.</p>\r\n\r\n<p><span><b>Confidentiality </b></span></p>\r\n\r\n<p>Client records are regarded as confidential and therefore will not be divulged to any third party, other than [our manufacturer/supplier(s) and] if legally required to do so to the appropriate authorities. We will not sell, share, or rent your personal information to any third party or use your e-mail address for unsolicited mail. Any emails sent by this Company will only be in connection with the provision of agreed services and products.</p>\r\n\r\n<p><span><b>Disclaimer Exclusions and Limitations</b></span></p>\r\n\r\n<p>The information on this web site is provided on an &quot;as is&quot; basis. To the fullest extent permitted by law, this Company:</p>\r\n\r\n<p>Excludes all representations and warranties relating to this website and its contents or which is or may be provided by any affiliates or any other third party, including in relation to any inaccuracies or commissions in this website and/or the Company&rsquo;s literature; and excludes all liability for damages arising out of or in connection with your use of this website. This includes, without limitation, direct loss, loss of business or profits (whether or not the loss of such profits was foreseeable, arose in the normal course of things or you have advised this Company of the possibility of such potential loss), damage caused to your computer, computer software, systems and programs and the data thereon or any other direct or indirect, consequential and incidental damages.&nbsp; This Company does not however exclude liability for death or personal injury caused by its negligence. The above exclusions and limitations apply only to the extent permitted by law. None of your statutory rights as a consumer are affected.&nbsp;</p>\r\n\r\n<p><span><b>Availability </b></span></p>\r\n\r\n<p>Unless otherwise stated, the services featured on this website are only available within the [country], or in relation to postings from the [country]. All advertising is intended solely for the [country]<b> </b>market. You are solely responsible for evaluating the fitness for a particular purpose of any downloads, programs and text available through this site. Redistribution or republication of any part of this site or its content is prohibited, including such by framing or other similar or any other means, without the express written consent of the Company. The Company does not warrant that the service from this site will be uninterrupted, timely or error free, although it is provided to the best ability. By using this service you thereby indemnify this Company, its employees, agents and affiliates against any loss or damage, in whatever manner, howsoever caused.</p>\r\n\r\n<p><span><b>Cookies </b></span></p>\r\n\r\n<p>Like most interactive web sites this Company&rsquo;s website [or ISP] uses cookies to enable us to retrieve user details for each visit. Cookies are used in our site to enable the functionality of this area and ease of use for those people visiting. Some of our affiliate partners may also use cookies. [If you do not use cookies, delete this clause]</p>\r\n\r\n<p><span><b>Links to this website</b></span></p>\r\n\r\n<p>You may not create a link to any page of this website without our prior written consent. If you do create a link to a page of this website you do so at your own risk and the exclusions and limitations set out above will apply to your use of this website by linking to it.&nbsp;</p>\r\n\r\n<p><span><b>Links from this website</b></span></p>\r\n\r\n<p>We do not monitor or review the content of other party&rsquo;s websites which are linked to from this website. Opinions expressed or material appearing on such websites is not necessarily shared or endorsed by us and should not be regarded as the publisher of such opinions or material. Please be aware that we are not responsible for the privacy practices, or content, of these sites. We encourage our users to be aware when they leave our site &amp; to read the privacy statements of these sites. You should evaluate the security and trustworthiness of any other site connected to this site or accessed through this site yourself, before disclosing any personal information to them. This Company will not accept any responsibility for any loss or damage in whatever manner, howsoever caused, resulting from your disclosure to third parties of personal information.&nbsp;&nbsp;</p>\r\n\r\n<p><span><b>Copyright Notice</b></span></p>\r\n\r\n<p>Copyright and other relevant intellectual property rights exist on all text relating to the Company&rsquo;s services and the Full content of this website. &nbsp;</p>\r\n\r\n<p>This Company&rsquo;s logo is a registered trademark of this Company in the [country]. The brand names and specific services of this Company featured on this web site are trademarked [delete this paragraphed clause if no registered trademark exists].</p>\r\n\r\n<p><span><b>Communication </b></span></p>\r\n\r\n<p>We have several different e-mail addresses for different queries. These, &amp; other contact information, can be found on our {*}</p>\r\n\r\n<p><span><b>Contact Us</b></span></p>\r\n\r\n<p>Link on our website or via Company literature or via the Company&rsquo;s stated telephone, facsimile or mobile telephone numbers.</p>\r\n\r\n<p>[Only need to state this if Limited Company, otherwise proprietors‟/partners‟ home/trading address must be shown, without use of the term: registered]</p>\r\n\r\n<p><span><b>Force Majeure </b></span></p>\r\n\r\n<p>Neither party shall be liable to the other for any failure to perform any obligation under any Agreement which is due to an event beyond the control of such party including but not limited to any terrorism, war, Political insurgence, insurrection, riot, civil unrest, act of civil or military authority, uprising, earthquake, flood or any other natural or manmade eventuality outside of our control, which causes the termination of an agreement or contract entered into, nor which could have been reasonably foreseen. Any Party affected by such event shall forthwith inform the other Party of the same and shall use all reasonable endeavors to comply with the terms and conditions of any Agreement contained herein.&nbsp;&nbsp;</p>\r\n\r\n<p><span><b>General </b></span></p>\r\n\r\n<p>The laws of [country]<b> </b>govern these terms and conditions. By accessing this website [and using our services/buying our products] you consent to these terms and conditions and to the exclusive jurisdiction of the [country] courts in all disputes arising out of such access. If any of these terms are deemed invalid or unenforceable for any reason (including, but not limited to the exclusions and limitations set out above), then the invalid or unenforceable provision will be severed from these terms and the remaining terms will continue to apply. Failure of the Company to enforce any of the provisions set out in these Terms and Conditions and any Agreement, or failure to exercise any option to terminate, shall not be construed as waiver of such provisions and shall not affect the validity of these Terms and Conditions or of any Agreement or any part thereof, or the right thereafter to enforce each and every provision. These Terms and Conditions shall not be amended, modified, varied or supplemented except in writing and signed by duly authorized representatives of the Company.</p>\r\n\r\n<p<span>><b>Notification of Changes</b></span></p>\r\n\r\n<p>The Company reserves the right to change these conditions from time to time as it sees fit and your continued use of the site will signify your acceptance of any adjustment to these terms. If there are any changes to our privacy policy, we will announce that these changes have been made on our home page and on other key pages on our site. If there are any changes in how we use our site customers‟ Personally Identifiable Information, notification by e-mail or postal mail will be made to those affected by this change. Any changes to our privacy policy will be posted on our web site [x] days prior to these changes taking place. You are therefore advised to re-read this statement on a regular basis&nbsp;</p>\r\n\r\n<p>These terms and conditions form part of the Agreement between the Client and us. You&rsquo;re accessing of this website and/or undertaking of a booking or Agreement indicates your understanding, agreement to and acceptance, of the Disclaimer Notice and the full Terms and Conditions contained herein. Your statutory Consumer Rights are unaffected.</p>\r\n\r\n<p>&copy; Eureka Online 2019 All Rights Reserved</p>', 0, '2019-01-27 09:56:34', '2020-05-28 04:20:27', 'Policies', 'شروط الاستخدام', NULL, NULL, NULL, 'policies', 0),
(10, '<p><span><b>Eureka Online Academy - About Us </b></span></p>\r\n\r\n<p><span><b>Who We Are</b></span></p>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-xl-8 col-lg-8 col-md-8\">\r\n<p>Eureka Academy is a Technological Institution that is focused on ensuring that young people get a good head start in their quest to garner knowledge of the technological world. The Eureka Academy is based in Amman, Jordan and Eureka Online Academy is an offshoot of this respectable establishment. The Eureka Academy started in Jordan in 2014 as a mission to ensure that youngsters get the best and affordable education in the technology.</p>\r\n</div>\r\n\r\n<div class=\"col-xl-4 col-lg-4 col-md-4\">\r\n<p><img alt=\"\" src=\"http://dn734b9758kwp.cloudfront.net/images/157307960945288659_1634388970000185_5760324607416467456_o.jpg\" /></p>\r\n</div>\r\n</div>\r\n\r\n<p>At Eureka Academy, we are focused on shifting the educational system to take on tasks like teaching technology and engineering as an integral part of school curriculums.</p>\r\n\r\n<p><span><b>About Eureka Online Academy</b></span></p>\r\n\r\n<p>Eureka Online Academy is the recent brainchild of this wonderful citadel of learning and is specifically created to bring quality learning in the tech sector to youngsters at their remote locations without having to enroll for physical trainings. This online academy is unarguably the first of its kind in the Arab World, being the first online Academy to come out of the Arab World.</p>\r\n\r\n<p>The Eureka Online Academy is fully integrated to ensure the all-round development of the innovative, technological and engineering skills of young people of all ages. Our platform is well-equipped with apparatuses they can adapt to quickly. Eureka Academy serves the purpose of equipping these young ones with the knowledge of the concepts of engineering by implementing an intriguing and engrossing system which consists of 10 Themes which are: Robotics, Electrical Engineering, Electronics, Control Systems and IOT, Renewable Energy, Games Development, Mobile Applications 1&amp;2, Entrepreneurship and Graduation Project.</p>\r\n\r\n<p>The Online Academy utilizes several effective structures of learning to ensure the students have a well encapsulating experience on the platform and to ensure that they are well imparted with the knowledge of the course areas they choose to major in. One exceptional thing about this Online Academy is that it focuses on giving children the information they need to engage the world technologically.</p>\r\n\r\n<p><span><b>Our Mission</b></span></p>\r\n\r\n<p>At Eureka Online Academy, our core mission is to ensure that knowledge of the use and implementation of technology in everyday life is imparted in the youngsters of the Arab world. We seek to make knowledge of technology and engineering so easy and affordable in the entire Arab World.</p>\r\n\r\n<p><span><b>Our Vision</b></span></p>\r\n\r\n<p>At Eureka Online Academy, vision is to make learning about technology and engineering so easily comprehensible that even children of 6 years of age can grasp it. This we have achieved through immersive visual and practical learning environments. We are working towards ensuring a Smart Arab Generation and raising an #Innovative_Arab_Generation.</p>', '<p><span><b>Eureka Online Academy - About Us </b></span></p>\r\n\r\n<p><span><b>Who We Are</b></span></p>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-xl-8 col-lg-8 col-md-8\">\r\n<p>Eureka Academy is a Technological Institution that is focused on ensuring that young people get a good head start in their quest to garner knowledge of the technological world. The Eureka Academy is based in Amman, Jordan and Eureka Online Academy is an offshoot of this respectable establishment. The Eureka Academy started in Jordan in 2014 as a mission to ensure that youngsters get the best and affordable education in the technology.</p>\r\n</div>\r\n\r\n<div class=\"col-xl-4 col-lg-4 col-md-4\">\r\n<p><img alt=\"\" src=\"http://dn734b9758kwp.cloudfront.net/images/157307960945288659_1634388970000185_5760324607416467456_o.jpg\" /></p>\r\n</div>\r\n</div>\r\n\r\n<p>At Eureka Academy, we are focused on shifting the educational system to take on tasks like teaching technology and engineering as an integral part of school curriculums.</p>\r\n\r\n<p><span><b>About Eureka Online Academy</b></span></p>\r\n\r\n<p>Eureka Online Academy is the recent brainchild of this wonderful citadel of learning and is specifically created to bring quality learning in the tech sector to youngsters at their remote locations without having to enroll for physical trainings. This online academy is unarguably the first of its kind in the Arab World, being the first online Academy to come out of the Arab World.</p>\r\n\r\n<p>The Eureka Online Academy is fully integrated to ensure the all-round development of the innovative, technological and engineering skills of young people of all ages. Our platform is well-equipped with apparatuses they can adapt to quickly. Eureka Academy serves the purpose of equipping these young ones with the knowledge of the concepts of engineering by implementing an intriguing and engrossing system which consists of 10 Themes which are: Robotics, Electrical Engineering, Electronics, Control Systems and IOT, Renewable Energy, Games Development, Mobile Applications 1&amp;2, Entrepreneurship and Graduation Project.</p>\r\n\r\n<p>The Online Academy utilizes several effective structures of learning to ensure the students have a well encapsulating experience on the platform and to ensure that they are well imparted with the knowledge of the course areas they choose to major in. One exceptional thing about this Online Academy is that it focuses on giving children the information they need to engage the world technologically.</p>\r\n\r\n<p><span><b>Our Mission</b></span></p>\r\n\r\n<p>At Eureka Online Academy, our core mission is to ensure that knowledge of the use and implementation of technology in everyday life is imparted in the youngsters of the Arab world. We seek to make knowledge of technology and engineering so easy and affordable in the entire Arab World.</p>\r\n\r\n<p><span><b>Our Vision</b></span></p>\r\n\r\n<p>At Eureka Online Academy, vision is to make learning about technology and engineering so easily comprehensible that even children of 6 years of age can grasp it. This we have achieved through immersive visual and practical learning environments. We are working towards ensuring a Smart Arab Generation and raising an #Innovative_Arab_Generation.</p>', 3, '2019-10-16 15:51:37', '2020-05-28 04:23:34', 'About', 'عن يوريكا', NULL, NULL, NULL, 'About', 1),
(12, '<section class=\"banner\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-xl-5 col-lg-5 col-md-6\">\r\n<div class=\"captiontext\">\r\n<aside>Learn what you love at your own</aside>\r\n\r\n<p>Anyone can join our community to learn cutting-edge skills. Pursue your hobby or jump-start your career. View the collection of courses offered and get started in no time.</p>\r\n<a href=\"courses\">Get started now</a> <a href=\"free-classes\">Try Free Lessons</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>', '<section class=\"banner\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-xl-5 col-lg-5 col-md-6\">\r\n<div class=\"captiontext\">\r\n<aside>تعلم ما تحب بنفسك</aside>\r\n\r\n<p>يمكن لأي شخص الانضمام إلى مجتمعنا لتعلم المهارات المتطورة. تابع هوايتك أو ابدأ حياتك المهنية. عرض مجموعة الدورات المقدمة والبدء في لمح البصر.</p>\r\n<a href=\"courses\">نبدأ الآن</a>\r\n<a href=\"free-classes\">جرب دروس مجانية</a></div>\r\n</div>\r\n</div>\r\n</section>', 0, '2020-03-30 06:13:18', '2020-05-25 04:15:40', 'Home Banner', 'Home Banner', NULL, NULL, NULL, 'home-banner', 0);
INSERT INTO `slugs` (`id`, `text`, `text_ar`, `order`, `created_at`, `updated_at`, `title_en`, `title_ar`, `permission`, `href`, `parent`, `slug`, `active`) VALUES
(13, '<p><span><b>Terms and Conditions</b></span></p>\r\n\r\n<p>In using this website you are deemed to have read and agreed to the following terms and conditions:</p>\r\n\r\n<p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and any or all Agreements:</p>\r\n\r\n<p>&quot;Client&quot;, &ldquo;You&rdquo; and &ldquo;Your&rdquo; refers to you, the person accessing this website and accepting the Company&rsquo;s terms and conditions. &quot;The Company&quot;, &ldquo;Ourselves&rdquo;, &ldquo;We&rdquo; and &quot;Us&quot;, refers to our Company. &ldquo;Party&rdquo;, &ldquo;Parties&rdquo;, or &ldquo;Us&rdquo;, refers to both the Client and ourselves, or either the Client or ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner, whether by formal meetings of a fixed duration, or any other means, for the express purpose of meeting the Client&rsquo;s needs in respect of provision of the Company&rsquo;s stated services/products, in accordance with and subject to, prevailing [Country Law]. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p>\r\n\r\n<p><span><b>Privacy Statement</b></span></p>\r\n\r\n<p>We are committed to protecting your privacy. Authorized employees within the company on a need to know basis only use any information collected from individual customers. We constantly review our systems and data to ensure the best possible service to our customers.</p>\r\n\r\n<p><span><b>Confidentiality</b></span></p>\r\n\r\n<p>Client records are regarded as confidential and therefore will not be divulged to any third party, other than [our manufacturer/supplier(s) and] if legally required to do so to the appropriate authorities. We will not sell, share, or rent your personal information to any third party or use your e-mail address for unsolicited mail. Any emails sent by this Company will only be in connection with the provision of agreed services and products.</p>\r\n\r\n<p><span><b>Disclaimer Exclusions and Limitations</b></span></p>\r\n\r\n<p>The information on this web site is provided on an &quot;as is&quot; basis. To the fullest extent permitted by law, this Company:</p>\r\n\r\n<p>Excludes all representations and warranties relating to this website and its contents or which is or may be provided by any affiliates or any other third party, including in relation to any inaccuracies or commissions in this website and/or the Company&rsquo;s literature; and excludes all liability for damages arising out of or in connection with your use of this website. This includes, without limitation, direct loss, loss of business or profits (whether or not the loss of such profits was foreseeable, arose in the normal course of things or you have advised this Company of the possibility of such potential loss), damage caused to your computer, computer software, systems and programs and the data thereon or any other direct or indirect, consequential and incidental damages. This Company does not however exclude liability for death or personal injury caused by its negligence. The above exclusions and limitations apply only to the extent permitted by law. None of your statutory rights as a consumer are affected.</p>\r\n\r\n<p><span><b>Cookies</b></span></p>\r\n\r\n<p>Like most interactive web sites this Company&rsquo;s website uses cookies to enable us to retrieve user details for each visit. Cookies are used in our site to enable the functionality of this area and ease of use for those people visiting. Some of our affiliate partners may also use cookies.</p>\r\n\r\n<p><span><b>Links to this website</b></span></p>\r\n\r\n<p>You may not create a link to any page of this website without our prior written consent. If you do create a link to a page of this website you do so at your own risk and the exclusions and limitations set out above will apply to your use of this website by linking to it.</p>\r\n\r\n<p><span><b>Links from this website</b></span></p>\r\n\r\n<p>We do not monitor or review the content of other party&rsquo;s websites which are linked to from this website. Opinions expressed or material appearing on such websites is not necessarily shared or endorsed by us and should not be regarded as the publisher of such opinions or material. Please be aware that we are not responsible for the privacy practices, or content, of these sites. We encourage our users to be aware when they leave our site &amp; to read the privacy statements of these sites. You should evaluate the security and trustworthiness of any other site connected to this site or accessed through this site yourself, before disclosing any personal information to them. This Company will not accept any responsibility for any loss or damage in whatever manner, howsoever caused, resulting from your disclosure to third parties of personal information.</p>\r\n\r\n<p><span><b>Copyright Notice</b></span></p>\r\n\r\n<p>Copyright and other relevant intellectual property rights exist on all text relating to the Company&rsquo;s services and the Full content of this website.</p>\r\n\r\n<p>This Company&rsquo;s logo is a registered trademark of this Company. The brand names and specific services of this Company featured on this web site are trademarked.</p>\r\n\r\n<p><span><b>Communication </b></span></p>\r\n\r\n<p>We have several different e-mail addresses for different queries. These, &amp; other contact information, can be found on our contact us page.</p>\r\n\r\n<p><span><b>Contact Us</b></span></p>\r\n\r\n<p>Link on our website or via Company literature or via the Company&rsquo;s stated telephone, facsimile or mobile telephone numbers.</p>\r\n\r\n<p><span><b>Force Majeure </b></span></p>\r\n\r\n<p>Neither party shall be liable to the other for any failure to perform any obligation under any Agreement which is due to an event beyond the control of such party including but not limited to any terrorism, war, Political insurgence, insurrection, riot, civil unrest, act of civil or military authority, uprising, earthquake, flood or any other natural or manmade eventuality outside of our control, which causes the termination of an agreement or contract entered into, nor which could have been reasonably foreseen. Any Party affected by such event shall forthwith inform the other Party of the same and shall use all reasonable endeavors to comply with the terms and conditions of any Agreement contained herein.</p>\r\n\r\n<p><span><b>General </b></span></p>\r\n\r\n<p>The laws of Jordan govern these terms and conditions. By accessing this website and using our services/buying our products, you consent to these terms and conditions and to the exclusive jurisdiction of Jordan courts in all disputes arising out of such access. If any of these terms are deemed invalid or unenforceable for any reason (including, but not limited to the exclusions and limitations set out above), then the invalid or unenforceable provision will be severed from these terms and the remaining terms will continue to apply. Failure of the Company to enforce any of the provisions set out in these Terms and Conditions and any Agreement, or failure to exercise any option to terminate, shall not be construed as waiver of such provisions and shall not affect the validity of these Terms and Conditions or of any Agreement or any part thereof, or the right thereafter to enforce each and every provision. These Terms and Conditions shall not be amended, modified, varied or supplemented except in writing and signed by duly authorized representatives of the Company.</p>\r\n\r\n<p><span><b>Notification of Changes</b></span></p>\r\n\r\n<p>The Company reserves the right to change these conditions from time to time as it sees fit and your continued use of the site will signify your acceptance of any adjustment to these terms. If there are any changes to our privacy policy, we will announce that these changes have been made on our home page and on other key pages on our site. If there are any changes in how we use our site customers‟ Personally Identifiable Information, notification by e-mail or postal mail will be made to those affected by this change. Any changes to our privacy policy will be posted on our web site days prior to these changes taking place. You are therefore advised to re-read this statement on a regular basis.</p>\r\n\r\n<p>These terms and conditions form part of the Agreement between the Client and us. You&rsquo;re accessing of this website and/or undertaking of a booking or Agreement indicates your understanding, agreement to and acceptance, of the Disclaimer Notice and the full Terms and Conditions contained herein. Your statutory Consumer Rights are unaffected.</p>\r\n\r\n<p><span><b>Refund Policy</b></span></p>\r\n\r\n<p>In order to be eligible for a refund, you have to return the product within 1 calendar day of your purchase. The product must be in the same condition that you receive it and undamaged in any way.</p>\r\n\r\n<p>After we receive your item, our team of professionals will inspect it and process your refund. The money will be refunded to the original payment method you&rsquo;ve used during the purchase. For credit card payments it may take 5 to 10 business days for a refund to show up on your credit card statement.</p>\r\n\r\n<p>If the product is damaged in any way, or you have initiated the return after 1 calendar day have passed, you will not be eligible for a refund.</p>\r\n\r\n<p>If anything is unclear or you have more questions feel free to contact our customer support team.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Shipping Policy</h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span><b>Shipment processing time</b></span></p>\r\n\r\n<p>All orders are processed within 3-5 business days. Orders are not shipped or delivered on weekends or holidays.</p>\r\n\r\n<p>If we are experiencing a high volume of orders, shipments may be delayed by a few days. Please allow additional days in transit for delivery. If there will be a significant delay in shipment of your order, we will contact you via email or telephone.</p>\r\n\r\n<p><span><b>Shipping rates &amp; delivery estimates</b></span></p>\r\n\r\n<p>Shipping charges for your order will be calculated and displayed at checkout.</p>\r\n\r\n<div class=\"table-responsive\">\r\n<table class=\"table\">\r\n	<thead>\r\n		<tr>\r\n			<th>Shipment method</th>\r\n			<th>Estimated delivery time</th>\r\n			<th>Shipment cost</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Standard</td>\r\n			<td>3-5 business days</td>\r\n			<td>3 JD</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>\r\n\r\n<p>Delivery delays can occasionally occur</p>\r\n\r\n<p><span><b>Shipment confirmation &amp; Order tracking</b></span></p>\r\n\r\n<p>You will receive a Shipment Confirmation email once your order has shipped.</p>\r\n\r\n<p><span><b>Damages</b></span></p>\r\n\r\n<p>Eureka tech is not liable for any products damaged or lost during shipping. If you received your order damaged, please contact the shipment carrier to file a claim.</p>\r\n\r\n<p>Please save all packaging materials and damaged goods before filing a claim.</p>\r\n\r\n<p>&copy; Eureka tech.org 2020 All Rights Reserved</p>', '<p><span><b>الأحكام والشروط</b></span></p>\r\n\r\n<p>عند استخدامك لهذا الموقع الإلكتروني ، تعتبر أنك قد قرأت ووافقت على الشروط والأحكام التالية: </p>\r\n\r\n<p>تنطبق الشروط التالية على هذه الشروط والأحكام وبيان الخصوصية وإخلاء المسؤولية وأي أو كل الاتفاقيات: </p>\r\n\r\n<p>يشير \"العميل\" و \"أنت\" و \"الخاص بك\" إليك ، الشخص الذي يدخل إلى هذا الموقع ويقبل بنود وشروط الشركة. تشير \"الشركة\" و \"أنفسنا\" و \"نحن\" و \"نحن\" إلى شركتنا. يشير مصطلح \"الطرف\" أو \"الأطراف\" أو \"نحن\" إلى كل من العميل وأنفسنا أو إما العميل أو أنفسنا. تشير جميع الشروط إلى العرض والقبول والنظر في الدفع اللازم لإجراء عملية مساعدتنا للعميل بالطريقة الأنسب ، سواء من خلال الاجتماعات الرسمية لمدة محددة ، أو أي وسيلة أخرى ، لغرض صريح من تلبية احتياجات العميل فيما يتعلق بتقديم الخدمات / المنتجات المعلنة للشركة ، وفقًا لـ [قانون البلد] السائد والخاضع له. أي استخدام للمصطلحات المذكورة أعلاه أو كلمات أخرى في صيغة المفرد والجمع والرسملة و / أو هو أو هي ، يتم اعتبارها قابلة للتبادل وبالتالي تشير إلى نفس الشيء.</p>\r\n\r\n<p><span><b>بيان الخصوصية</b></span></p>\r\n<p>ونحن ملتزمون بحماية خصوصيتك. الموظفون المعتمدون داخل الشركة عند الحاجة إلى المعرفة يستخدمون فقط أي معلومات يتم جمعها من العملاء الأفراد. نحن نراجع باستمرار أنظمتنا وبياناتنا لضمان أفضل خدمة ممكنة لعملائنا. </p>\r\n\r\n<p><span><b>سرية</b></span></p>\r\n<p>تعتبر سجلات العملاء سرية وبالتالي لن يتم الكشف عنها لأي طرف ثالث ، بخلاف [الشركة المصنعة / المورد (الموردين) لدينا] و / إذا طلب منها قانونًا القيام بذلك إلى السلطات المختصة. لن نبيع أو نشارك أو نؤجر معلوماتك الشخصية لأي طرف ثالث أو نستخدم عنوان بريدك الإلكتروني للبريد غير المرغوب فيه. ستكون أي رسائل بريد إلكتروني مرسلة من قبل هذه الشركة مرتبطة فقط بتقديم الخدمات والمنتجات المتفق عليها.</p>\r\n\r\n\r\n\r\n\r\n<p><span><b>استثناءات وحدود إخلاء المسؤولية</b></span></p>\r\n<p>يتم توفير المعلومات الموجودة على موقع الويب هذا على أساس \"كما هي\". إلى أقصى حد يسمح به القانون ، فإن هذه الشركة: </p>\r\n<p>يستثني جميع الإقرارات والضمانات المتعلقة بهذا الموقع ومحتوياته أو التي يتم توفيرها أو قد يتم توفيرها من قبل أي من الشركات التابعة أو أي طرف ثالث آخر ، بما في ذلك فيما يتعلق بأي أخطاء أو عمولات في هذا الموقع و / أو مطبوعات الشركة ؛ ويستثني كل المسؤولية عن الأضرار الناشئة عن أو فيما يتعلق باستخدامك لهذا الموقع. وهذا يشمل ، على سبيل المثال لا الحصر ، الخسارة المباشرة أو الخسارة في الأعمال أو الأرباح (سواء كان من المتوقع توقع خسارة هذه الأرباح أم لا ، نشأت في سياق الأشياء العادي أو أبلغت هذه الشركة بإمكانية مثل هذه الخسارة المحتملة) ، الضرر الناتج إلى جهاز الكمبيوتر الخاص بك وبرامج الكمبيوتر والأنظمة والبرامج والبيانات المتعلقة بها أو أي أضرار مباشرة أو غير مباشرة أو تبعية أو عرضية. ومع ذلك ، لا تستبعد هذه الشركة المسؤولية عن الوفاة أو الإصابة الشخصية الناجمة عن إهمالها. تنطبق الاستثناءات والقيود المذكورة أعلاه فقط إلى الحد الذي يسمح به القانون. لا يتأثر أي من حقوقك القانونية كمستهلك. </p>\r\n\r\n\r\n<p><span><b>بسكويت</b></span></p>\r\n<p>مثل معظم مواقع الويب التفاعلية ، يستخدم موقع الويب الخاص بهذه الشركة ملفات تعريف الارتباط لتمكيننا من استرداد تفاصيل المستخدم لكل زيارة. يتم استخدام ملفات تعريف الارتباط في موقعنا لتمكين وظائف هذه المنطقة وسهولة الاستخدام لأولئك الأشخاص الذين يزورونها. بعض شركائنا التابعون لنا يستخدمون كعك الكوكيز أيضآ. </p>\r\n\r\n\r\n<p><span><b>روابط لهذا الموقع</b></span></p>\r\n<p>لا يجوز لك إنشاء رابط إلى أي صفحة من هذا الموقع دون موافقتنا الخطية المسبقة. إذا قمت بإنشاء رابط لصفحة من هذا الموقع ، فأنت تفعل ذلك على مسؤوليتك الخاصة ، وستنطبق الاستثناءات والقيود المبينة أعلاه على استخدامك لهذا الموقع عن طريق الارتباط به.</p>\r\n\r\n<p><span><b>روابط من هذا الموقع</b></span></p>\r\n<p>نحن لا نراقب أو نراجع محتوى مواقع الطرف الآخر المرتبطة من هذا الموقع. الآراء التي يتم التعبير عنها أو المواد التي تظهر على مثل هذه المواقع الإلكترونية لا تتم مشاركتها أو إقرارها بالضرورة من قبلنا ولا يجب اعتبارها ناشرًا لهذه الآراء أو المواد. يرجى العلم أننا لسنا مسؤولين عن ممارسات الخصوصية أو المحتوى لهذه المواقع. نشجع مستخدمينا على الانتباه عند مغادرتهم لموقعنا وقراءة بيانات الخصوصية لهذه المواقع. يجب عليك تقييم الأمن والثقة في أي موقع آخر متصل بهذا الموقع أو يتم الوصول إليه من خلال هذا الموقع بنفسك ، قبل الكشف عن أي معلومات شخصية لهم. لن تقبل هذه الشركة أي مسؤولية عن أي خسارة أو ضرر بأي طريقة كانت ، مهما كان سببها ، ناتجة عن إفشاء معلومات شخصية لأطراف ثالثة.</p>\r\n\r\n<p><span><b>حقوق التأليف</b></span></p>\r\n<p>حقوق الطبع والنشر وحقوق الملكية الفكرية الأخرى ذات الصلة موجودة على جميع النصوص المتعلقة بخدمات الشركة والمحتوى الكامل لهذا الموقع.  </p>\r\n<p>شعار هذه الشركة هو علامة تجارية مسجلة لهذه الشركة. الأسماء التجارية والخدمات المحددة لهذه الشركة المعروضة على هذا الموقع هي علامات تجارية. </p>\r\n\r\n<p><span><b>الاتصالات </b></span></p>\r\n<p>لدينا العديد من عناوين البريد الإلكتروني المختلفة لطلبات البحث المختلفة. هذه ، ومعلومات الاتصال الأخرى ، يمكن العثور عليها في صفحة الاتصال بنا. </p>\r\n\r\n<p><span><b>اتصل بنا</b></span></p>\r\n<p>يمكنك الربط على موقعنا الإلكتروني أو عبر مطبوعات الشركة أو عبر أرقام الهاتف أو الفاكس أو الهاتف المحمول للشركة.</p>\r\n\r\n<p><span><b>قوة قهرية </b></span></p>\r\n<p>لا يتحمل أي طرف مسؤولية الطرف الآخر عن أي إخفاق في أداء أي التزام بموجب أي اتفاقية ناتجة عن حدث خارج عن سيطرة هذا الطرف بما في ذلك على سبيل المثال لا الحصر أي إرهاب أو حرب أو تمرد سياسي أو تمرد أو أعمال شغب أو اضطرابات مدنية ، فعل سلطة مدنية أو عسكرية أو انتفاضة أو زلزال أو فيضان أو أي احتمال آخر طبيعي أو من صنع الإنسان خارج سيطرتنا ، مما يؤدي إلى إنهاء اتفاق أو عقد مبرم ، ولا يمكن توقعه بشكل معقول. يجب على أي طرف متضرر من هذا الحدث إبلاغ الطرف الآخر على الفور بنفسه ويجب عليه بذل كل المساعي المعقولة للامتثال لشروط وأحكام أي اتفاقية واردة في هذه الاتفاقية. </p>\r\n\r\n\r\n<p><span><b>جنرال لواء </b></span></p>\r\n<p>تحكم قوانين الأردن هذه الشروط والأحكام. عن طريق الوصول إلى هذا الموقع واستخدام خدماتنا / شراء منتجاتنا ، فإنك توافق على هذه الشروط والأحكام والولاية القضائية الحصرية للمحاكم الأردنية في جميع النزاعات الناشئة عن هذا الوصول. إذا تم اعتبار أي من هذه الشروط غير صالح أو غير قابل للتنفيذ لأي سبب من الأسباب (بما في ذلك ، على سبيل المثال لا الحصر ، الاستثناءات والقيود المنصوص عليها أعلاه) ، فسيتم فصل البند غير الصالح أو غير القابل للتنفيذ من هذه الشروط وستستمر الشروط المتبقية في التطبيق. لا يُفسر فشل الشركة في تطبيق أي من الأحكام المنصوص عليها في هذه الشروط والأحكام وأي اتفاقية ، أو عدم ممارسة أي خيار لإنهاء ، على أنه تنازل عن هذه الأحكام ولن يؤثر على صحة هذه الشروط والأحكام أو أي اتفاقية أو أي جزء منها ، أو الحق بعد ذلك في تطبيق كل بند. لا يجوز تعديل هذه الشروط والأحكام أو تعديلها أو تغييرها أو استكمالها إلا في الكتابة وتوقيعها من قبل ممثلي الشركة المفوضين حسب الأصول.</p>\r\n\r\n\r\n<p><span><b>الإخطار بالتغييرات</b></span></p>\r\n<p>تحتفظ الشركة بالحق في تغيير هذه الشروط من وقت لآخر حسبما تراه مناسبًا وسوف يشير استمرار استخدامك للموقع إلى موافقتك على أي تعديل لهذه الشروط. إذا كانت هناك أي تغييرات على سياسة الخصوصية الخاصة بنا ، فسوف نعلن أن هذه التغييرات قد تم إجراؤها على صفحتنا الرئيسية والصفحات الرئيسية الأخرى على موقعنا. إذا كانت هناك أي تغييرات في كيفية استخدام عملائنا لموقعنا ‟معلومات التعريف الشخصية ، سيتم إرسال إشعار بالبريد الإلكتروني أو البريد إلى أولئك المتأثرين بهذا التغيير. سيتم نشر أي تغييرات على سياسة الخصوصية الخاصة بنا على موقعنا قبل أيام من حدوث هذه التغييرات. لذا ننصحك بإعادة قراءة هذا البيان على أساس منتظم.  </p>\r\n<p>تشكل هذه الشروط والأحكام جزءًا من الاتفاقية المبرمة بيننا وبين العميل. إنك تدخل إلى هذا الموقع و / أو إجراء حجز أو اتفاقية تشير إلى فهمك وموافقتك وقبولك لإشعار إخلاء المسؤولية والأحكام والشروط الكاملة الواردة فيه. حقوق المستهلك القانونية الخاصة بك لا تتأثر.</p>\r\n\r\n<p><span><b>سياسة الاسترجاع</b></span></p>\r\n<p>لكي تكون مؤهلاً لاسترداد الأموال ، يجب عليك إعادة المنتج في غضون يوم تقويمي واحد من عملية الشراء. يجب أن يكون المنتج في نفس الحالة التي تتلقاها فيه وغير تالفة بأي شكل من الأشكال.</p>\r\n<p>بعد استلام البند الخاص بك ، سيقوم فريق المحترفين لدينا بفحصه ومعالجة استرداد أموالك. سيتم رد الأموال إلى طريقة الدفع الأصلية التي استخدمتها أثناء الشراء. بالنسبة إلى مدفوعات بطاقة الائتمان ، قد يستغرق الأمر من 5 إلى 10 أيام عمل حتى تظهر الأموال المستردة في كشف حساب بطاقتك الائتمانية.</p>\r\n<p>إذا كان المنتج تالفًا بأي شكل من الأشكال ، أو إذا بدأت عملية الإرجاع بعد مرور يوم تقويمي واحد ، فلن تكون مؤهلاً لاسترداد الأموال. </p>\r\n<p>إذا كان أي شيء غير واضح أو لديك المزيد من الأسئلة فلا تتردد في الاتصال بفريق دعم العملاء.</p>\r\n\r\n\r\n\r\n<p><span><h2>سياسة الشحن</h2></span></p>\r\n\r\n<p><span><b>وقت معالجة الشحنة</b></span></p>\r\n<p>تتم معالجة جميع الطلبات في غضون 3-5 أيام عمل. لا يتم شحن الطلبات أو تسليمها في عطلات نهاية الأسبوع أو العطلات.</p>\r\n<p>إذا واجهنا حجمًا كبيرًا من الطلبات ، فقد تتأخر الشحنات لبضعة أيام. يرجى السماح بأيام إضافية في العبور للتسليم. إذا كان هناك تأخير كبير في شحن طلبك ، فسنتصل بك عبر البريد الإلكتروني أو الهاتف.\r\n</p>\r\n\r\n<p><span><b>أسعار الشحن وتقديرات التسليم</b></span></p>\r\n<p>سيتم احتساب رسوم الشحن لطلبك وعرضها عند الدفع.</p>\r\n<div class=\"table-responsive\">\r\n	<table class=\"table\">\r\n		<thead>\r\n			<tr>\r\n				<th>طريقة الشحن</th>\r\n				<th>يقدر وقت التسليم</th>\r\n				<th>تكلفة الشحن</th>\r\n			</tr>\r\n		</thead>\r\n		<tbody>\r\n			<tr>\r\n				<td>اساسي</td>\r\n				<td>3-5 أيام عمل</td>\r\n				<td>3 دينار</td>\r\n				\r\n			</tr>\r\n		</tbody>\r\n	</table>\r\n</div>\r\n<p>يمكن أن يحدث تأخير التسليم في بعض الأحيان</p>\r\n\r\n\r\n<p><span><b>تأكيد الشحن وتتبع الطلب</b></span></p>\r\n<p>ستتلقى بريدًا إلكترونيًا لتأكيد الشحن بمجرد شحن طلبك.</p>\r\n\r\n<p><span><b>الأضرار</b></span></p>\r\n<p>لا تتحمل Eureka tech أي مسؤولية عن أي منتجات تالفة أو مفقودة أثناء الشحن. إذا تلقيت طلبك تالفًا ، فيرجى الاتصال بشركة الشحن لتقديم مطالبة.</p>\r\n<p>يرجى حفظ جميع مواد التعبئة والتغليف والبضائع التالفة قبل تقديم مطالبة.\r\n</p>\r\n\r\n\r\n<p>© Eureka tech.org 2020 جميع الحقوق محفوظة</p>', 0, '2020-04-13 03:17:47', '2020-05-29 05:20:13', 'Term of User', 'مصطلح المستخدم', NULL, NULL, NULL, 'terms-of-user', 0),
(14, '<p><span><b>Help</b></span></p>\r\n<p>help</p>', '<p><span><b>Help</b></span></p>\r\n<p>help</p>', 0, '2020-04-13 03:24:13', '2020-05-28 04:25:28', 'Help', 'مساعدة', NULL, NULL, NULL, 'help', 0),
(15, '<p><span><b>Careers</b></span></p>\r\n<p>Careers</p>', '<h1 class=\"comntitle\">وظائف</h1>\r\n\r\n<p>وظائف</p>', 0, '2020-04-13 03:24:13', '2020-05-28 04:26:45', 'Careers', 'وظائف', NULL, NULL, NULL, 'careers', 0),
(16, '<p><span><b>Become a Teacher</b></span></p>\r\n<p>Become a Teacher</p>', '<p><span><b>Become a Teacher</b></span></p>\r\n<p>Become a Teacher</p>', 0, '2020-04-13 03:24:13', '2020-05-28 04:28:45', 'Become a Teacher', 'يصبح مدرسا', NULL, NULL, NULL, 'become-a-Teacher', 0),
(17, '<p><span><b>Press</b></span></p>\r\n<p>Press</p>', '<p><span><b>Press</b></span></p>\r\n<p>Press</p>', 0, '2020-04-13 03:24:13', '2020-05-28 04:27:40', 'Press', 'صحافة', NULL, NULL, NULL, 'press', 0),
(18, '<p><span><b>Become a Member</b></span></p>\r\n<p>Become a Member</p>', '<p><span><b>Become a Member</b></span></p>\r\n<p>Become a Member</p>', 0, '2020-04-13 03:24:13', '2020-05-28 04:29:53', 'Become a Member', 'أصبح عضوا', NULL, NULL, NULL, 'become-a-member', 0),
(19, '<h1 class=\"comntitle\">Free Classes</h1>\r\n\r\n<p>Free Classes</p>', '<h1 class=\"comntitle\">دروس مجانية</h1>\r\n\r\n<p>دروس مجانية</p>', 0, '2020-04-13 03:24:13', '2020-04-13 03:41:29', 'Free Classes', 'دروس مجانية', NULL, NULL, NULL, 'free-classes', 0),
(20, '<p><span><b>Refer a Friend</b></span></p>\r\n<p>Refer a Friend</p>', '<p><span><b>Refer a Friend</b></span></p>\r\n<p>Refer a Friend</p>', 0, '2020-04-13 03:24:13', '2020-05-28 04:30:58', 'Refer a Friend', 'أوص أحد الأصدقاء', NULL, NULL, NULL, 'refer-a-friend', 0),
(21, 'This is some sample content.', '', 1, '2018-07-15 03:16:16', '2018-07-15 03:18:31', 'Courses', 'الدورات', NULL, '/courses', NULL, 'Courses', 1),
(22, 'This is some sample content.', '', 2, '2018-07-15 03:16:16', '2018-07-15 03:18:31', 'Store', 'متجر', NULL, '/stores', NULL, 'Store', 1),
(23, '<p><span><b>Highlights</b></span></p>\r\n<p>Highlights</p>', '<p><span><b>Highlights</b></span></p>\r\n<p>Highlights</p>', 0, '2020-04-13 03:24:13', '2020-05-28 04:32:06', 'Highlights', 'يسلط الضوء\'', NULL, NULL, NULL, 'highlights', 0);

-- --------------------------------------------------------

--
-- Table structure for table `stars`
--

CREATE TABLE `stars` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stars`
--

INSERT INTO `stars` (`id`, `user_id`, `image_path`, `created_at`, `updated_at`) VALUES
(12, 1, '/uploads/star-1.png', '2018-07-24 09:05:23', '2020-04-02 04:24:29'),
(14, 3, '/uploads/child-1871104_960_720.jpg', '2018-11-05 15:43:17', '2018-11-05 15:45:10'),
(15, 4, '/uploads/Should-your-child-be-dairy-free-iStock_64414757.jpg', '2018-11-05 15:54:47', '2018-11-05 15:54:47'),
(17, 6, '/uploads/cr_home_background_child.jpg', '2018-11-05 15:54:48', '2018-11-05 15:54:48'),
(18, 11, '/uploads/download.png', '2018-11-05 15:57:42', '2020-04-17 01:59:22'),
(19, 13, '/uploads/WrULeZm1RWk.jpg', '2018-11-05 15:59:01', '2018-11-05 15:59:01'),
(21, 16, '/uploads/mentally-prepare-child-for-school-year.jpg', '2018-11-05 16:03:33', '2018-11-05 16:05:08'),
(23, 19, '/images/logo.png', '2019-10-13 20:42:07', '2019-10-13 20:42:07'),
(26, 15, '/uploads/addressing-child-safety_banner_edit.jpg', '2020-04-17 04:23:30', '2020-04-17 04:23:30'),
(27, 14, '/uploads/images.jpeg', '2020-04-17 07:03:29', '2020-04-17 07:03:29');

-- --------------------------------------------------------

--
-- Table structure for table `stripe_customer`
--

CREATE TABLE `stripe_customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT '0',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `card_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stripe_customer`
--

INSERT INTO `stripe_customer` (`id`, `user_id`, `email`, `stripe_id`, `card_id`, `created_at`, `updated_at`) VALUES
(2, 1, 'qwe@qwe.qwe', '123', '123', '2018-06-12 09:46:30', '2018-06-12 09:46:30');

-- --------------------------------------------------------

--
-- Table structure for table `stripe_plan`
--

CREATE TABLE `stripe_plan` (
  `id` int(10) UNSIGNED NOT NULL,
  `subscription_model_id` int(11) DEFAULT '0',
  `course_id` int(11) DEFAULT '0',
  `stripe_plan_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stripe_plan`
--

INSERT INTO `stripe_plan` (`id`, `subscription_model_id`, `course_id`, `stripe_plan_id`, `created_at`, `updated_at`) VALUES
(1, 2, 6, 'qsdq123', '2018-06-12 10:23:51', '2018-06-12 10:23:51');

-- --------------------------------------------------------

--
-- Table structure for table `subcription_model`
--

CREATE TABLE `subcription_model` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `display_price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `price` double(12,3) DEFAULT '0.000',
  `period_in_days` int(11) DEFAULT '0',
  `full_access` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcription_model`
--

INSERT INTO `subcription_model` (`id`, `name_ar`, `name_en`, `display_price`, `price`, `period_in_days`, `full_access`, `name`, `created_at`, `updated_at`) VALUES
(3, 'اشتراك سنوي', 'Annual Subscription', '200 JOD', 200.000, 365, 1, 'annual', '2018-06-24 21:00:00', '2018-06-24 21:00:00'),
(4, 'اشتراك شهري', 'Monthly Subscription', '30 JOD', 30.000, 30, 1, 'monthly', '2018-06-24 21:00:00', '2018-06-24 21:00:00'),
(5, 'اشتراك دورة', 'Course Subscription', NULL, 0.000, 182, 0, 'course', '2018-06-24 21:00:00', '2020-03-25 04:34:07');

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE `subscription` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT '0',
  `subscription_model_id` int(11) DEFAULT '0',
  `payment_id` int(11) DEFAULT '0',
  `payment_method_id` int(11) DEFAULT '0',
  `course_id` int(11) DEFAULT '0',
  `percentage_progress` int(11) NOT NULL DEFAULT '0',
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `stripe_subscription_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscription`
--

INSERT INTO `subscription` (`id`, `user_id`, `subscription_model_id`, `payment_id`, `payment_method_id`, `course_id`, `percentage_progress`, `start_date`, `end_date`, `status`, `stripe_subscription_id`, `created_at`, `updated_at`) VALUES
(22, 2, 4, 34, 1, 0, 100, '2018-09-06 09:24:21', '2018-11-04 22:00:00', 'active', '0', '2018-09-05 05:37:24', '2018-09-05 05:37:24'),
(25, 1, 3, 39, -1, 0, 100, '2019-10-15 13:48:18', '2030-09-25 21:00:00', 'active', '0', '2018-09-26 08:21:55', '2018-09-26 08:21:55'),
(26, 7, 3, 40, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:23:52', '2018-09-26 08:23:52'),
(27, 7, 3, 41, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:24:13', '2018-09-26 08:24:13'),
(28, 7, 3, 42, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:27:06', '2018-09-26 08:27:06'),
(29, 7, 3, 43, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:35:03', '2018-09-26 08:35:03'),
(30, 7, 3, 44, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:35:42', '2018-09-26 08:35:42'),
(31, 7, 3, 45, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:37:07', '2018-09-26 08:37:07'),
(32, 7, 3, 46, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:38:00', '2018-09-26 08:38:00'),
(33, 8, 4, 47, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:38:00', '2018-09-26 08:38:00'),
(34, 9, 3, 48, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:38:01', '2018-09-26 08:38:01'),
(35, 10, 4, 49, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:38:01', '2018-09-26 08:38:01'),
(36, 7, 3, 50, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:38:43', '2018-09-26 08:38:43'),
(37, 8, 4, 51, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:38:44', '2018-09-26 08:38:44'),
(38, 9, 3, 52, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:38:44', '2018-09-26 08:38:44'),
(39, 10, 4, 53, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:38:44', '2018-09-26 08:38:44'),
(40, 7, 3, 54, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:40:40', '2018-09-26 08:40:40'),
(41, 8, 4, 55, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:40:40', '2018-09-26 08:40:40'),
(42, 9, 3, 56, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:40:40', '2018-09-26 08:40:40'),
(43, 10, 4, 57, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:40:40', '2018-09-26 08:40:40'),
(44, 7, 3, 58, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:41:26', '2018-09-26 08:41:26'),
(45, 8, 4, 59, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:41:26', '2018-09-26 08:41:26'),
(46, 9, 3, 60, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:41:26', '2018-09-26 08:41:26'),
(47, 10, 4, 61, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:41:26', '2018-09-26 08:41:26'),
(48, 7, 3, 62, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:43:26', '2018-09-26 08:43:26'),
(49, 8, 4, 63, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:43:26', '2018-09-26 08:43:26'),
(50, 9, 3, 64, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:43:26', '2018-09-26 08:43:26'),
(51, 10, 4, 65, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:43:27', '2018-09-26 08:43:27'),
(52, 7, 3, 66, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:43:53', '2018-09-26 08:43:53'),
(53, 8, 4, 67, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:43:53', '2018-09-26 08:43:53'),
(54, 9, 3, 68, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:43:53', '2018-09-26 08:43:53'),
(55, 10, 4, 69, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:43:54', '2018-09-26 08:43:54'),
(56, 7, 3, 70, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:44:25', '2018-09-26 08:44:25'),
(57, 8, 4, 71, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:44:25', '2018-09-26 08:44:25'),
(58, 9, 3, 72, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:44:25', '2018-09-26 08:44:25'),
(59, 10, 4, 73, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:44:25', '2018-09-26 08:44:25'),
(60, 7, 3, 74, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:44:44', '2018-09-26 08:44:44'),
(61, 8, 4, 75, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:44:44', '2018-09-26 08:44:44'),
(62, 9, 3, 76, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:44:44', '2018-09-26 08:44:44'),
(63, 10, 4, 77, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:44:44', '2018-09-26 08:44:44'),
(64, 7, 3, 78, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:45:23', '2018-09-26 08:45:23'),
(65, 8, 4, 79, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:45:23', '2018-09-26 08:45:23'),
(66, 9, 3, 80, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:45:24', '2018-09-26 08:45:24'),
(67, 10, 4, 81, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:45:24', '2018-09-26 08:45:24'),
(68, 7, 3, 82, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:48:59', '2018-09-26 08:48:59'),
(69, 8, 4, 83, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:48:59', '2018-09-26 08:48:59'),
(70, 9, 3, 84, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:49:00', '2018-09-26 08:49:00'),
(71, 10, 4, 85, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:49:00', '2018-09-26 08:49:00'),
(72, 7, 3, 86, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:50:11', '2018-09-26 08:50:11'),
(73, 8, 4, 87, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:50:11', '2018-09-26 08:50:11'),
(74, 9, 3, 88, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:50:11', '2018-09-26 08:50:11'),
(75, 10, 4, 89, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:50:11', '2018-09-26 08:50:11'),
(76, 7, 3, 90, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:50:29', '2018-09-26 08:50:29'),
(77, 8, 4, 91, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:50:29', '2018-09-26 08:50:29'),
(78, 9, 3, 92, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:50:29', '2018-09-26 08:50:29'),
(79, 10, 4, 93, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:50:30', '2018-09-26 08:50:30'),
(80, 7, 3, 94, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:50:43', '2018-09-26 08:50:43'),
(81, 8, 4, 95, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:50:43', '2018-09-26 08:50:43'),
(82, 9, 3, 96, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:50:44', '2018-09-26 08:50:44'),
(83, 10, 4, 97, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:50:44', '2018-09-26 08:50:44'),
(84, 7, 3, 98, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:51:10', '2018-09-26 08:51:10'),
(85, 8, 4, 99, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:51:10', '2018-09-26 08:51:10'),
(86, 9, 3, 100, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:51:10', '2018-09-26 08:51:10'),
(87, 10, 4, 101, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:51:10', '2018-09-26 08:51:10'),
(88, 7, 3, 102, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:51:50', '2018-09-26 08:51:50'),
(89, 8, 4, 103, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:51:51', '2018-09-26 08:51:51'),
(90, 9, 3, 104, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:51:51', '2018-09-26 08:51:51'),
(91, 10, 4, 105, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:51:51', '2018-09-26 08:51:51'),
(92, 7, 3, 106, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:52:36', '2018-09-26 08:52:36'),
(93, 8, 4, 107, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:52:36', '2018-09-26 08:52:36'),
(94, 9, 3, 108, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:52:36', '2018-09-26 08:52:36'),
(95, 10, 4, 109, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:52:36', '2018-09-26 08:52:36'),
(96, 7, 3, 110, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:59:53', '2018-09-26 08:59:53'),
(97, 8, 4, 111, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:59:53', '2018-09-26 08:59:53'),
(98, 9, 3, 112, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 08:59:53', '2018-09-26 08:59:53'),
(99, 10, 4, 113, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 08:59:53', '2018-09-26 08:59:53'),
(100, 7, 3, 114, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 09:00:18', '2018-09-26 09:00:18'),
(101, 8, 4, 115, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 09:00:19', '2018-09-26 09:00:19'),
(102, 9, 3, 116, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 09:00:19', '2018-09-26 09:00:19'),
(103, 10, 4, 117, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 09:00:19', '2018-09-26 09:00:19'),
(104, 7, 3, 118, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 09:01:21', '2018-09-26 09:01:21'),
(105, 8, 4, 119, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 09:01:21', '2018-09-26 09:01:21'),
(106, 9, 3, 120, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 09:01:21', '2018-09-26 09:01:21'),
(107, 10, 4, 121, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 09:01:21', '2018-09-26 09:01:21'),
(108, 7, 3, 122, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 09:01:42', '2018-09-26 09:01:42'),
(109, 8, 4, 123, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 09:01:42', '2018-09-26 09:01:42'),
(110, 9, 3, 124, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 09:01:43', '2018-09-26 09:01:43'),
(111, 10, 4, 125, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 09:01:43', '2018-09-26 09:01:43'),
(112, 7, 3, 126, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 09:02:01', '2018-09-26 09:02:01'),
(113, 8, 4, 127, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 09:02:01', '2018-09-26 09:02:01'),
(114, 9, 3, 128, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 09:02:01', '2018-09-26 09:02:01'),
(115, 10, 4, 129, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 09:02:02', '2018-09-26 09:02:02'),
(116, 7, 3, 130, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 09:03:17', '2018-09-26 09:03:17'),
(117, 8, 4, 131, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 09:03:18', '2018-09-26 09:03:18'),
(118, 9, 3, 132, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 09:03:18', '2018-09-26 09:03:18'),
(119, 10, 4, 133, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 09:03:18', '2018-09-26 09:03:18'),
(120, 7, 3, 134, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 09:05:03', '2018-09-26 09:05:03'),
(121, 8, 4, 135, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 09:05:04', '2018-09-26 09:05:04'),
(122, 9, 3, 136, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 09:05:04', '2018-09-26 09:05:04'),
(123, 10, 4, 137, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 09:05:04', '2018-09-26 09:05:04'),
(124, 7, 3, 138, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 09:07:04', '2018-09-26 09:07:04'),
(125, 8, 4, 139, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 09:07:04', '2018-09-26 09:07:04'),
(126, 9, 3, 140, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 09:07:04', '2018-09-26 09:07:04'),
(127, 10, 4, 141, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 09:07:04', '2018-09-26 09:07:04'),
(128, 7, 3, 142, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 09:07:21', '2018-09-26 09:07:21'),
(129, 8, 4, 143, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 09:07:21', '2018-09-26 09:07:21'),
(130, 9, 3, 144, -1, 0, 100, '2018-09-25 21:00:00', '2019-09-25 21:00:00', 'active', '0', '2018-09-26 09:07:21', '2018-09-26 09:07:21'),
(131, 10, 4, 145, -1, 0, 100, '2018-09-25 21:00:00', '2018-10-25 22:00:00', 'active', '0', '2018-09-26 09:07:21', '2018-09-26 09:07:21'),
(132, 1, 3, 155, 4, NULL, 100, '2020-02-14 01:25:02', '2021-02-13 01:25:02', 'active', '0', '2020-02-14 01:25:02', '2020-02-14 01:25:02'),
(133, 3, 3, 156, 0, 82, 0, '2020-03-26 00:41:13', '2021-03-26 00:41:13', 'active', '', '2020-03-26 00:41:13', '2020-03-26 00:41:13'),
(134, 1, 3, 157, 0, 82, 0, '2020-03-26 00:41:34', '2021-03-26 00:41:34', 'active', '', '2020-03-26 00:41:34', '2020-03-26 00:41:34'),
(135, 21, 3, 158, 0, 82, 0, '2020-03-26 00:47:33', '2021-03-26 00:47:33', 'active', '', '2020-03-26 00:47:33', '2020-03-26 00:47:33'),
(136, 1, 3, 159, 3, 80, 0, '2020-03-26 23:28:13', '2021-03-26 23:28:13', 'active', '', '2020-03-26 23:28:13', '2020-03-26 23:28:13'),
(137, 21, 5, 160, 4, 6, 100, '2020-04-15 01:03:53', '2020-10-14 01:03:53', 'active', '0', '2020-04-15 01:03:53', '2020-04-15 01:03:53'),
(138, 11, 5, 161, 4, 17, 100, '2020-04-16 04:37:51', '2020-10-15 04:37:51', 'active', '0', '2020-04-16 04:37:51', '2020-04-16 04:37:51'),
(139, 11, 5, 162, 4, 6, 100, '2020-04-16 04:38:56', '2020-10-15 04:38:56', 'active', '0', '2020-04-16 04:38:56', '2020-04-16 04:38:56'),
(140, 11, 5, 163, 4, 18, 100, '2020-04-17 02:15:54', '2020-10-16 02:15:54', 'active', '0', '2020-04-17 02:15:54', '2020-04-17 02:15:54'),
(141, 21, 5, 164, 4, 76, 100, '2020-05-14 00:59:14', '2020-11-12 00:59:14', 'active', '0', '2020-05-14 00:59:14', '2020-05-14 00:59:14'),
(142, 21, 5, 165, 4, 22, 100, '2020-05-14 01:08:18', '2020-11-12 01:08:18', 'active', '0', '2020-05-14 01:08:18', '2020-05-14 01:08:18'),
(143, 21, 5, 165, 4, 4, 100, '2020-05-14 01:08:18', '2020-11-12 01:08:18', 'active', '0', '2020-05-14 01:08:18', '2020-05-14 01:08:18'),
(144, 21, 5, 166, 4, 17, 100, '2020-05-14 01:16:52', '2020-11-12 01:16:52', 'active', '0', '2020-05-14 01:16:52', '2020-05-14 01:16:52'),
(145, 21, 5, 166, 4, 10, 100, '2020-05-14 01:16:52', '2020-11-12 01:16:52', 'active', '0', '2020-05-14 01:16:52', '2020-05-14 01:16:52'),
(146, 21, 5, 167, 4, 80, 100, '2020-05-14 01:21:32', '2020-11-12 01:21:32', 'active', '0', '2020-05-14 01:21:32', '2020-05-14 01:21:32'),
(147, 21, 5, 168, 4, 24, 100, '2020-05-14 07:08:31', '2020-11-12 07:08:31', 'active', '0', '2020-05-14 07:08:31', '2020-05-14 07:08:31'),
(148, 21, 5, 169, 4, 21, 100, '2020-05-14 07:41:02', '2020-11-12 07:41:02', 'active', '0', '2020-05-14 07:41:02', '2020-05-14 07:41:02'),
(149, 1, NULL, 170, 3, 19, 100, '2020-05-15 00:18:59', '2020-11-13 00:18:59', 'active', '0', '2020-05-15 00:18:59', '2020-05-15 00:18:59'),
(150, 21, 5, 172, 3, 19, 100, '2020-05-15 00:22:39', '2020-11-13 00:22:39', 'active', '0', '2020-05-15 00:22:39', '2020-05-15 00:22:39'),
(151, 11, 5, 173, 3, 82, 100, '2020-05-15 00:30:25', '2020-11-13 00:30:25', 'active', '0', '2020-05-15 00:30:25', '2020-05-15 00:30:25'),
(152, 21, 5, 174, 4, 18, 100, '2020-05-15 08:02:49', '2020-11-13 08:02:49', 'active', '0', '2020-05-15 08:02:49', '2020-05-15 08:02:49'),
(153, 21, 5, 175, 4, 23, 100, '2020-05-15 08:11:45', '2020-11-13 08:11:45', 'active', '0', '2020-05-15 08:11:45', '2020-05-15 08:11:45'),
(154, 21, 5, 176, 4, 25, 100, '2020-05-15 08:24:07', '2020-11-13 08:24:07', 'active', '0', '2020-05-15 08:24:07', '2020-05-15 08:24:07'),
(155, 11, 5, 180, 4, 19, 100, '2020-05-22 08:20:48', '2020-11-20 08:20:48', 'active', '0', '2020-05-22 08:20:48', '2020-05-22 08:20:48'),
(156, 11, 5, 181, 4, 19, 100, '2020-05-22 08:22:55', '2020-11-20 08:22:55', 'active', '0', '2020-05-22 08:22:55', '2020-05-22 08:22:55'),
(157, 11, 5, 182, 4, 19, 100, '2020-05-22 08:24:08', '2020-11-20 08:24:08', 'active', '0', '2020-05-22 08:24:08', '2020-05-22 08:24:08'),
(158, 11, 5, 188, 4, 21, 100, '2020-05-26 01:34:23', '0000-00-00 00:00:00', 'active', '0', '2020-05-26 01:34:23', '2020-05-26 01:34:23'),
(159, 11, 5, 189, 4, 22, 100, '2020-05-26 01:38:21', '2020-08-26 01:38:21', 'active', '0', '2020-05-26 01:38:22', '2020-05-26 01:38:22'),
(160, 11, 5, 190, 3, 23, 100, '2020-05-26 09:56:42', '2020-05-12 01:50:50', 'inactive', '0', '2020-05-26 01:50:50', '2020-05-26 04:26:42'),
(161, 11, 5, 191, 4, 23, 100, '2020-05-26 09:56:42', '2020-06-26 03:58:48', 'inactive', '0', '2020-05-26 03:58:48', '2020-05-26 04:26:42'),
(162, 11, 5, 195, 4, 23, 100, '2020-05-26 04:26:42', '2020-06-26 04:26:42', 'active', '0', '2020-05-26 04:26:42', '2020-05-26 04:26:42'),
(163, 21, 5, 201, 4, 26, 100, '2020-05-28 06:08:44', '0000-00-00 00:00:00', 'inactive', '0', '2020-05-28 00:32:11', '2020-05-28 00:38:44'),
(164, 21, 5, 202, 4, 26, 100, '2020-05-28 00:38:44', '0000-00-00 00:00:00', 'active', '0', '2020-05-28 00:38:44', '2020-05-28 00:38:44'),
(166, 11, 5, 206, 3, 24, 100, '2020-06-10 14:11:43', '0000-00-00 00:00:00', 'inactive', '0', '2020-06-03 07:19:36', '2020-06-10 08:41:43'),
(167, 11, 5, 207, 3, 24, 100, '2020-06-10 14:11:43', '0000-00-00 00:00:00', 'inactive', '0', '2020-06-03 07:20:00', '2020-06-10 08:41:43'),
(168, 11, 5, 208, 3, 24, 100, '2020-06-10 14:11:43', '0000-00-00 00:00:00', 'inactive', '0', '2020-06-03 07:21:38', '2020-06-10 08:41:43'),
(169, 11, 5, 209, 3, 24, 100, '2020-06-10 14:11:43', '0000-00-00 00:00:00', 'inactive', '0', '2020-06-03 07:30:35', '2020-06-10 08:41:43'),
(170, 11, 5, 126, 5, 24, 100, '2020-06-10 08:41:43', '0000-00-00 00:00:00', 'active', '0', '2020-06-10 08:41:43', '2020-06-10 08:41:43');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_info`
--

CREATE TABLE `supplier_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cell_phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_line1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_line2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_line3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supplier_info`
--

INSERT INTO `supplier_info` (`id`, `user_id`, `city`, `name`, `phone_number`, `cell_phone_number`, `email`, `province_code`, `post_code`, `country_code`, `address_line1`, `address_line2`, `address_line3`, `created_at`, `updated_at`) VALUES
(1, 1, 'Amman', 'Moustafa2', '+96399475326', '+963115489872', 'test@email.com', NULL, NULL, 'JO', 'test address line', 'address line2', NULL, '2019-05-07 01:52:58', '2019-05-08 23:19:12');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_item`
--

CREATE TABLE `supplier_item` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `price` double(8,2) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supplier_item`
--

INSERT INTO `supplier_item` (`id`, `item_id`, `supplier_id`, `price`, `active`, `qty`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 150.00, 1, 168, '2018-10-08 08:16:14', '2020-06-11 01:22:00'),
(4, 2, 1, 100.00, 1, 57, '2018-10-08 12:22:11', '2020-06-11 01:22:00'),
(6, 4, 1, 100.00, 1, 156, '2020-04-07 07:25:24', '2020-06-11 01:22:00'),
(7, 3, 1, 100.00, 1, 65, '2020-04-07 07:25:44', '2020-06-11 01:22:00');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_fees`
--

CREATE TABLE `teacher_fees` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `percent_fees` double(12,3) DEFAULT '0.000',
  `fixed_fees` double(12,3) DEFAULT '0.000',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teacher_fees`
--

INSERT INTO `teacher_fees` (`id`, `user_id`, `percent_fees`, `fixed_fees`, `created_at`, `updated_at`) VALUES
(1, 1, 32.000, 15.200, NULL, NULL),
(3, 1, 12.000, 1212.000, '2018-06-12 07:36:07', '2018-06-12 07:41:25');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `title_ar` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `type` enum('Site','Course') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Site',
  `course_id` int(11) DEFAULT NULL,
  `image_path_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image_path_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `icon_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `name_ar`, `name_en`, `title_ar`, `title_en`, `description_ar`, `description_en`, `active`, `type`, `course_id`, `image_path_ar`, `image_path_en`, `icon_path`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(16, 'شهادات شهادات لوحة المعلومات', 'Dashboard ', 'شهادات لوحة القيادة', 'certificates Title', 'شهادات لوحة القيادة الوصف', 'Dashboard certificates Description', 1, 'Site', 0, '', '/uploads/user.png', '', '2020-03-30 01:47:31', '2020-03-30 01:56:01', 1, 1),
(18, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:27:56', '2020-03-30 03:27:56', 1, NULL),
(19, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 1, 'Site', 0, '', '/uploads/user.png', '', '2020-03-30 03:35:54', '2020-03-30 03:36:39', 1, 1),
(20, 'Wael Mamdouh', 'Wael Mamdouh', 'niversity', 'niversity', 'Eureka helped me gain it interesting and fun.', 'Eureka helped me gain it interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:37:42', '2020-03-30 03:37:42', 1, NULL),
(21, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain interesting and fun.', 'Eureka helped me gain interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:38:41', '2020-03-30 03:38:41', 1, NULL),
(22, 'شهادات شهادات لوحة المعلومات', 'Dashboard', 'شهادات لوحة القيادة', 'certificates Title', 'شهادات لوحة القيادة الوصف', 'Dashboard certificates Description', 1, 'Site', 0, '', '/uploads/user.png', '', '2020-03-30 01:47:31', '2020-03-30 01:56:01', 1, 1),
(23, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:27:56', '2020-03-30 03:27:56', 1, NULL),
(24, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 1, 'Site', 0, '', '/uploads/user.png', '', '2020-03-30 03:35:54', '2020-03-30 03:36:39', 1, 1),
(25, 'Wael Mamdouh', 'Wael Mamdouh', 'niversity', 'niversity', 'Eureka helped me gain it interesting and fun.', 'Eureka helped me gain it interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:37:42', '2020-03-30 03:37:42', 1, NULL),
(26, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain interesting and fun.', 'Eureka helped me gain interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:38:41', '2020-03-30 03:38:41', 1, NULL),
(27, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 1, 'Site', 0, '', '/uploads/user.png', '', '2020-03-30 03:35:54', '2020-03-30 03:36:39', 1, 1),
(28, 'Wael Mamdouh', 'Wael Mamdouh', 'niversity', 'niversity', 'Eureka helped me gain it interesting and fun.', 'Eureka helped me gain it interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:37:42', '2020-03-30 03:37:42', 1, NULL),
(29, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain interesting and fun.', 'Eureka helped me gain interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:38:41', '2020-03-30 03:38:41', 1, NULL),
(30, 'شهادات شهادات لوحة المعلومات', 'Dashboard ', 'شهادات لوحة القيادة', 'certificates Title', 'شهادات لوحة القيادة الوصف', 'Dashboard certificates Description', 1, 'Site', 0, '', '/uploads/user.png', '', '2020-03-30 01:47:31', '2020-03-30 01:56:01', 1, 1),
(31, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:27:56', '2020-03-30 03:27:56', 1, NULL),
(32, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 1, 'Site', 0, '', '/uploads/user.png', '', '2020-03-30 03:35:54', '2020-03-30 03:36:39', 1, 1),
(33, 'Wael Mamdouh', 'Wael Mamdouh', 'niversity', 'niversity', 'Eureka helped me gain it interesting and fun.', 'Eureka helped me gain it interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:37:42', '2020-03-30 03:37:42', 1, NULL),
(34, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain interesting and fun.', 'Eureka helped me gain interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:38:41', '2020-03-30 03:38:41', 1, NULL),
(35, 'شهادات شهادات لوحة المعلومات', 'Dashboard', 'شهادات لوحة القيادة', 'certificates Title', 'شهادات لوحة القيادة الوصف', 'Dashboard certificates Description', 1, 'Site', 0, '', '/uploads/user.png', '', '2020-03-30 01:47:31', '2020-03-30 01:56:01', 1, 1),
(36, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:27:56', '2020-03-30 03:27:56', 1, NULL),
(37, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 1, 'Site', 0, '', '/uploads/user.png', '', '2020-03-30 03:35:54', '2020-03-30 03:36:39', 1, 1),
(38, 'Wael Mamdouh', 'Wael Mamdouh', 'niversity', 'niversity', 'Eureka helped me gain it interesting and fun.', 'Eureka helped me gain it interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:37:42', '2020-03-30 03:37:42', 1, NULL),
(39, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain interesting and fun.', 'Eureka helped me gain interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:38:41', '2020-03-30 03:38:41', 1, NULL),
(40, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 1, 'Site', 0, '', '/uploads/user.png', '', '2020-03-30 03:35:54', '2020-03-30 03:36:39', 1, 1),
(41, 'Wael Mamdouh', 'Wael Mamdouh', 'niversity', 'niversity', 'Eureka helped me gain it interesting and fun.', 'Eureka helped me gain it interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:37:42', '2020-03-30 03:37:42', 1, NULL),
(42, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain interesting and fun.', 'Eureka helped me gain interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:38:41', '2020-03-30 03:38:41', 1, NULL),
(43, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain interesting and fun.', 'Eureka helped me gain interesting and fun.', 1, 'Site', 0, '', '/uploads/chapter-img.jpg', '', '2020-03-30 03:38:41', '2020-04-21 00:21:40', 1, 1),
(44, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 'Eureka helped me gain skills in things I’m passioante about while keeping it interesting and fun. Eureka while keeping it interesting and fun.', 1, 'Site', 0, '', '/uploads/user.png', '', '2020-03-30 03:35:54', '2020-03-30 03:36:39', 1, 1),
(45, 'Wael Mamdouh', 'Wael Mamdouh', 'niversity', 'niversity', 'Eureka helped me gain it interesting and fun.', 'Eureka helped me gain it interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:37:42', '2020-03-30 03:37:42', 1, NULL),
(46, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain interesting and fun.', 'Eureka helped me gain interesting and fun.', 1, 'Site', 0, '', '', '', '2020-03-30 03:38:41', '2020-04-23 00:42:01', 1, 1),
(47, 'Wael Mamdouh', 'Wael Mamdouh', 'phd in Michatronics university', 'phd in Michatronics university', 'Eureka helped me gain interesting and fun.', 'Eureka helped me gain interesting and fun.', 1, 'Course', 26, '', '', '', '2020-04-23 00:46:45', '2020-04-23 03:34:35', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '/images/logo.png',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `teacher` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `fees` double(12,3) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `supplier` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `age`, `phone`, `email`, `country`, `image_path`, `password`, `remember_token`, `admin`, `teacher`, `active`, `fees`, `created_at`, `updated_at`, `supplier`) VALUES
(1, 'Admin', 128, '+963000000000', 'user@user.com', 'Jordan', '/uploads/star-1.png', '$2y$10$AHZI/BWizJpWE2VS9L6zReilk8eX6HVn0LuZgplT9.hGZmvREWAcK', 'DqUBFNQUSbCLBlWGp5Fgyt5RJDyKhhVKnEkl5F21ndK3agem9TivK87NgiO9', 1, 0, 1, NULL, '2018-06-06 06:53:26', '2020-04-17 05:03:14', 1),
(2, 'qwe', 123123, '123123', 'Email@email.com', NULL, '/images/user.png', '$2y$10$AHZI/BWizJpWE2VS9L6zReilk8eX6HVn0LuZgplT9.hGZmvREWAcK', 'YSXHBSGNRVJO5RwmL4q7q1i70pObwdIYNf0V4wvwGHRUF0IMD9YT7RxPiIwN', 0, 1, 1, 0.140, '2018-06-19 05:41:02', '2018-10-15 05:04:21', 1),
(3, 'Eureka User', 16, '96215581355', 'test@eureka.com', 'Jordan', '/uploads/child-1871104_960_720.jpg', '$2y$10$AHZI/BWizJpWE2VS9L6zReilk8eX6HVn0LuZgplT9.hGZmvREWAcK', 'sNUEtuqHUSFvVn41egSnlH0gC9k45rtV1NpjlBaCLz8A65EytmtDKhNep8rX', 0, 1, 1, NULL, '2018-07-25 10:21:01', '2020-04-17 05:03:05', 0),
(4, 'username1', 12, '09948728051', 'email@gmail.com', 'Jordan', '/uploads/Should-your-child-be-dairy-free-iStock_64414757.jpg', '$2y$10$CPJ8dBqCx0Ou2GvG.oFt3O9AcflLm1nN7vnZHEa7C82DzIODXGW/K', 'Xoil9nPURYaDgRHN7YGSwXoTIy1KmJr11xUY6yx5cs8l0qsJs0biEpvZxAAu', 0, 0, 1, NULL, '2018-07-26 10:02:46', '2020-04-17 05:02:56', 1),
(5, 'tet', 12, '12', 'test@teset.com', NULL, '/images/logo.png', '$2y$10$Ct9mbmBE69kXTnuUvUmud.UJahTEoP6eJxqVTBl3Yl/mNSzxSgr2u', NULL, 0, 1, 1, NULL, '2018-07-26 10:19:40', '2020-03-26 04:41:04', 1),
(6, 'annualtest', 123, '123', 'annual@email.com', 'Jordan', '/uploads/cr_home_background_child.jpg', '$2y$10$NVKjdkVAkBF9hsr1.a2AV.GTuSPg8nrum/RHFc9NwTnuZsupxbj5O', 'MfT6V02oZyzrMX6Fuia5XL4St4QUzY5GsMBkfCamnOBVzzXi1SvLBwYtG8id', 0, 0, 1, NULL, '2018-08-29 09:46:30', '2020-04-17 05:02:47', 0),
(7, 'test1', NULL, '963994728051', 'test1@gmail.com', NULL, '/images/logo.png', '$2y$10$La.b/JPOZfjEOz3zv6h/A.hf1FOkz4ycYRkhDO6kxsCU7lANOCKcy', NULL, 0, 0, 1, NULL, '2018-09-26 08:18:21', '2018-09-26 08:18:21', 0),
(8, 'test2', NULL, '963994728051', 'test2@gmail.com', NULL, '/images/logo.png', '$2y$10$nBvCrfMxWNqb4WIyisv3AehOEk/4KSST9Pdub88Xj8DMOSkVRMvEC', NULL, 0, 0, 1, NULL, '2018-09-26 08:38:00', '2020-03-26 04:41:19', 1),
(9, 'test3', NULL, '963994728051', 'test3@gmail.com', NULL, '/images/logo.png', '$2y$10$llFG4cxZ5XiLPrwObZi0veJPJBgZbGFwEic1gKruPrlqgfkyurX82', NULL, 0, 0, 1, NULL, '2018-09-26 08:38:01', '2018-09-26 08:38:01', 0),
(10, 'test4', NULL, '963994728051', 'test4@gmail.com', NULL, '/images/logo.png', '$2y$10$uwxQVxXrZ1DDpWhzEpt4jOsNk8mnYOcQ5MZm5lOCwY/ZArKY7BT2C', NULL, 0, 0, 1, NULL, '2018-09-26 08:38:01', '2018-09-26 08:38:01', 0),
(11, 'bharat', 123, '123', 'bharat@gmail.com', 'Jordan', '/uploads/download.png', '$2y$10$vfUE.N.uF1VifvMjJbsbxe8AS6beriwpcVKzULNi9eGrAKYWLOExa', 'eP6Wyf63PrUBobz0RMRARITleeT72PTrgbH5GFf9DFSDzUTHXJnYEFCxuY4l', 0, 0, 1, NULL, '2018-11-05 15:56:02', '2020-06-10 08:32:55', 0),
(13, 'star@emai2l.com', 123, '123', 'star2@email.com', 'Jordan', '/uploads/WrULeZm1RWk.jpg', '$2y$10$Q23htBQn5cFDu67tTXFPmON/xdyTq1wjFPCSLKaAAGG97o900Kvpu', 'xVxutfVJvm441sjfzMgPPvu6daUSjcSxqGkgasknRvrMT8Ys7QYj8HBsYGCe', 0, 0, 1, NULL, '2018-11-05 15:58:28', '2020-04-17 05:02:29', 0),
(14, 'star2@emai2l.com', 123, '123', 'star3@email.com', 'Jordan', '/uploads/images.jpeg', '$2y$10$y.kH8O3X7Cjo99kN2W0MmeK0l2v7kuVm3c1F7P7F/ZVr8bumDS4yO', 'DRU8Q3pi7fSf7JAIQFIm1g6Lf7brxOBgqlXAXRtVDuRe7MHgEieHhcwjZVUW', 0, 0, 1, NULL, '2018-11-05 15:59:34', '2020-04-17 05:02:20', 0),
(15, 'star2@emai2l.com2', 123, '123', 'star4@email.com', 'Jordan', '/uploads/addressing-child-safety_banner_edit.jpg', '$2y$10$Z9eQJF50EKby4IY1u7wET.aw3oOK2LGvpa45.eXMjMUECAgPK1WhG', 'P0Cu9b9g8GNmiwKqP2KVZBx665adx3NkiLvfZJmt4FKfxYIwZzlAcvdOSyAX', 0, 0, 1, NULL, '2018-11-05 16:02:21', '2020-04-17 05:02:11', 0),
(16, 'star2@emai2l.com1', 123, '123', 'star5@email.com', 'Jordan', '/uploads/mentally-prepare-child-for-school-year.jpg', '$2y$10$9dJuxUeUPakJpVgSWREvvOsrwZXuVAGHcCngRFjJHyNtwmHD04AE2', 'LOz62uPACyX5f90dlHDMvccQbY77SslOBFb1ytiDSpv1mGqyOxqmyrL53RJB', 0, 0, 1, NULL, '2018-11-05 16:03:22', '2020-04-17 05:00:46', 0),
(17, 'Supplier', 12, '0956321568432156', 'email@wim.com', NULL, '/images/logo.png', '$2y$10$EFOK/uiLRQ4GJErnyEmXxuKRGZe8uPokdJ8QVH89BaZLD/bfLuNQy', NULL, 0, 0, 1, NULL, '2019-05-08 22:30:36', '2019-05-08 22:30:36', 1),
(20, 'HamzaDaoud', 20, '0778911371', 'hamzadaoud99@gmail.com', NULL, '/images/logo.png', '$2y$10$/ayJhHRxTE4Qr7/27hkboeZv.slkwobzk6ggC89Qs3BLOiMmbdXqK', 'jJKWC5wSd9Jbug2WtMQcNicfFh3L8v744xPAeWZAO4icohY5xdybaZkv4kiR', 0, 1, 1, NULL, '2019-10-21 13:29:53', '2020-03-25 03:58:16', 0),
(21, 'sid', 30, '919998972498', 'sid@gmail.com', NULL, '/uploads/download.png', '$2y$10$Ivgk4K1GJsPjyIbkq2vySep2TMrwUKZthS4/bMwgwwjhWtHiCcEmK', '7GhuAznuTDzbzAxDCH6uC6GTBLcJwOUCyT9OvpnbC6QRaIcNdcF8RcqCPpiH', 0, 0, 1, NULL, '2020-03-25 04:35:16', '2020-04-06 04:54:10', 0),
(22, 'test', 20, '1234567890', 'test@gmail.com', NULL, '/images/logo.png', '$2y$10$a20SeTdch2z55/Q0h/628eTLdfQIUYraX65S14GE1F3a2nWqLW/tO', '6hMhggMxfNlg6xsKoKpzr6K693dFoFJyafa9oeSlxu0imBbfxKreLB6XKRKg', 0, 0, 1, NULL, '2020-04-09 01:21:59', '2020-04-09 01:21:59', 0),
(23, 'ketulpatel', 30, '4567894568', 'ketulpatel@webmynesystems.com', 'India', '/images/logo.png', '$2y$10$oiIUQ2ru36NDILkQJZ7NFeF/BrbEenPkv45bClsE3dJF6PCm9fw1O', '7aAelVeq4rWw19NXgPtFPt4ZDuy8n3ve061e1cajdXotgMArcR0NNjPojCjt', 0, 0, 1, NULL, '2020-04-10 00:14:48', '2020-05-22 05:13:55', 0),
(24, 'siddhraj', NULL, '7894567896', 'siddhraj@webmynesystems.com', NULL, '/images/logo.png', '$2y$10$ri6Vu0.OyYYhvUR7zZe60.Nvy4e7EohhRsKAK4BEvfAcrOq2ycGuO', NULL, 0, 0, 1, NULL, '2020-04-21 01:05:31', '2020-04-21 01:05:31', 0),
(25, 'siddhraj raulji', NULL, '1234657896', 'siddhraj@gmail.com', NULL, '/images/logo.png', '$2y$10$1u5itkELTiZ4AuZtYMhYGuqwJlbu54S0wRThxCRPWFxBC3pk9Noy6', NULL, 0, 0, 1, NULL, '2020-04-21 03:30:26', '2020-04-21 03:30:26', 0),
(26, 'ketul', NULL, '7894567896', 'ketulpatel1@webmynesystems.com', 'india', '/images/logo.png', '$2y$10$FtXmNUi/g7EndRiIkO8rxuHUL1thwhmioNDt8Fe89/OKodJKURXua', NULL, 0, 0, 1, NULL, '2020-04-23 07:33:01', '2020-04-23 07:33:01', 0),
(34, 'RAVISH R KUMAR', NULL, '7894567891235', 'master@gmail.com', 'India', '/images/logo.png', '$2y$10$tbjz3qyMLGnfb5UQ3NsTY.Xgp5r7Vd9Yb0dnTed.ZbvEkbO4dZ4oK', NULL, 0, 0, 1, NULL, '2020-04-23 08:07:49', '2020-04-23 08:07:49', 0),
(50, 'ketul1', NULL, '7894567896', 'ketulpatel123@webmynesystems.com', 'india', '/images/logo.png', '$2y$10$vOi.kzyB3uaaz8pxHGPehuf7TRxdcI9/ZUpYLwrGglk6ZmYl7hKsm', NULL, 0, 0, 1, NULL, '2020-04-24 00:02:28', '2020-04-24 00:02:28', 0),
(51, 'siddhraj1', NULL, '7894567896', 'siddhraj123@webmynesystems.com', 'india', '/images/logo.png', '$2y$10$APcXr5SJp2Kn1rVooU0CMuvWKe2ytdWIVq3I2/fscWzPHDTK6lQWK', NULL, 0, 0, 1, NULL, '2020-04-24 00:02:28', '2020-04-24 00:02:28', 0),
(52, 'ketul patel', 30, '7878784578', 'ketul.ketul.patel1@gmail.com', NULL, '/images/logo.png', '$2y$10$eik9RalgaQM5sLqXiSU66ullAsi/uLn5Vgn0rKdjVDr5/vFxTeEHa', 'g3EhxvH9eVb8NjDu8zhtRb1acJJ17HhX0ByIpP0qlP5ttfhLaTqSTuvDkqmr', 0, 0, 1, NULL, '2020-06-03 01:45:47', '2020-06-03 01:45:47', 0),
(53, 'ketul patel', 23, '789456789456', 'ketul.ketul.patel@gmail.com', NULL, '/images/logo.png', '$2y$10$SgZlZ4on5SHnJJTNjvdiiu35q3gNGXj1XoRx1NsFbHntqN8OiIEU.', 'be9xABsw957Wtme6tRyq5BnKW55GH5ja5QCH3MnHWOMJ8eKEDbVUpIxc6VhD', 0, 0, 1, NULL, '2020-06-03 01:49:56', '2020-06-03 01:49:56', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_progress`
--

CREATE TABLE `user_progress` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL DEFAULT '0',
  `quiz_passed` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `video_seen` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_progress`
--

INSERT INTO `user_progress` (`id`, `user_id`, `video_id`, `course_id`, `quiz_passed`, `created_at`, `updated_at`, `video_seen`) VALUES
(16, 1, 11, 6, 1, '2018-10-04 07:34:10', '2020-05-29 06:28:41', 1),
(17, 1, 2, 6, 1, '2018-10-04 07:34:33', '2018-10-04 07:34:49', 1),
(18, 1, 3, 6, 1, '2018-10-04 07:35:19', '2018-10-04 07:35:35', 1),
(19, 1, 6, 6, 1, '2018-10-04 07:35:28', '2018-11-04 12:56:26', 1),
(20, 1, 10, 6, 1, '2018-10-16 10:08:56', '2020-05-29 05:42:21', 0),
(21, 1, 4, 0, 1, '2019-08-27 17:44:55', '2019-08-27 17:44:55', 0),
(22, 1, 16, 0, 1, '2019-10-16 12:11:49', '2019-10-16 12:11:49', 0),
(23, 1, 23, 0, 1, '2019-10-17 06:07:44', '2019-10-17 06:07:44', 0),
(24, 1, 25, 0, 1, '2019-10-17 19:39:40', '2019-10-17 19:39:40', 0),
(25, 1, 26, 0, 0, '2019-10-20 06:13:23', '2019-10-20 06:13:23', 1),
(26, 1, 27, 0, 0, '2019-10-20 06:17:48', '2019-10-20 06:17:48', 1),
(27, 1, 30, 0, 0, '2019-10-20 06:34:14', '2019-10-20 06:34:14', 1),
(28, 1, 22, 0, 0, '2019-10-21 22:59:04', '2019-10-21 22:59:04', 1),
(29, 1, 42, 0, 0, '2019-10-26 17:00:52', '2019-10-26 17:00:52', 1),
(30, 1, 44, 0, 0, '2019-10-28 21:30:51', '2019-10-28 21:30:51', 1),
(31, 1, 47, 0, 0, '2019-10-29 19:57:27', '2019-10-29 19:57:27', 1),
(32, 1, 45, 0, 0, '2019-11-04 13:18:53', '2019-11-04 13:18:53', 1),
(33, 11, 11, 6, 1, '2020-04-15 04:48:24', '2020-04-16 00:48:28', 1),
(34, 11, 10, 6, 1, '2020-04-16 04:39:13', '2020-04-16 08:26:44', 1),
(35, 11, 24, 6, 1, '2020-04-16 04:39:38', '2020-04-16 04:39:38', 1),
(36, 11, 21, 6, 1, '2020-04-16 05:14:39', '2020-04-16 05:14:39', 1),
(37, 11, 2, 6, 1, '2020-04-16 08:47:10', '2020-04-16 08:47:10', 1),
(38, 11, 3, 6, 1, '2020-04-16 09:01:49', '2020-04-16 09:01:49', 1),
(39, 11, 6, 6, 1, '2020-04-16 09:05:57', '2020-04-16 09:05:57', 1),
(40, 11, 12, 17, 1, '2020-04-17 01:36:58', '2020-04-17 01:36:58', 0),
(41, 11, 23, 6, 1, '2020-04-17 01:43:56', '2020-04-17 01:43:56', 0),
(42, 11, 13, 17, 1, '2020-04-17 01:49:01', '2020-04-17 01:49:01', 0),
(43, 11, 69, 18, 1, '2020-04-17 02:24:07', '2020-04-17 02:42:06', 1),
(50, 1, 69, 18, 1, '2020-04-17 05:15:51', '2020-04-17 05:57:57', 1),
(51, 1, 70, 18, 1, '2020-04-17 05:19:07', '2020-04-17 05:19:07', 0),
(52, 21, 69, 18, 1, '2020-05-11 03:13:55', '2020-05-11 03:13:55', 0),
(53, 21, 12, 17, 1, '2020-05-11 03:28:03', '2020-05-11 03:28:03', 0),
(54, 21, 13, 17, 1, '2020-05-11 03:44:18', '2020-05-11 03:44:18', 0),
(55, 21, 70, 18, 1, '2020-05-11 03:46:29', '2020-05-11 03:46:29', 0),
(56, 21, 71, 17, 1, '2020-05-11 04:25:45', '2020-05-11 04:26:00', 1),
(57, 21, 10, 6, 1, '2020-05-27 03:45:27', '2020-05-27 04:08:02', 1),
(58, 21, 24, 6, 1, '2020-05-29 04:59:33', '2020-05-29 04:59:33', 0),
(59, 21, 72, 6, 1, '2020-05-29 05:01:55', '2020-05-29 05:01:55', 0),
(60, 1, 21, 6, 1, '2020-05-29 06:28:35', '2020-05-29 06:28:35', 0),
(61, 1, 12, 17, 1, '2020-06-11 04:09:01', '2020-06-11 04:09:01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `description_ar` text COLLATE utf8mb4_unicode_ci,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `description_en` text COLLATE utf8mb4_unicode_ci,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `course_id` int(11) DEFAULT '0',
  `session_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT '0',
  `estimated_time` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '0.00',
  `seo_meta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `kit_id` int(11) DEFAULT '0',
  `order` int(11) DEFAULT '0',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `url_identifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `is_youtube` tinyint(1) NOT NULL DEFAULT '0',
  `free` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `name_ar`, `description_ar`, `name_en`, `description_en`, `image_path`, `course_id`, `session_id`, `user_id`, `estimated_time`, `seo_meta`, `kit_id`, `order`, `path`, `url_identifier`, `active`, `is_youtube`, `free`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(2, 'الدرس الثري', 'Arabic ديسكريبشن', 'Lesson Three', 'English Description\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non,', '', 6, 2, 1, '480', '', 0, 1, 'https://d3ezpvziuomw66.cloudfront.net/output/hls-input/Introduction-EurekaTechAcademy.ts.m3u8', '77091771', 1, 1, 1, '2018-06-10 08:14:29', '2018-10-16 08:16:45', NULL, 1),
(3, 'Lesson Four بالعربي', 'Description in ارابيك', 'Lesson Four', 'English Description\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non,', '', 6, 2, 1, '420', '', 0, 2, 'https://d3ezpvziuomw66.cloudfront.net/output/hls-input/testing.ts.m3u8', '77091771', 1, 1, 1, '2018-06-10 08:17:56', '2018-08-12 10:49:15', NULL, NULL),
(4, 'Lesson Six بالعربي', 'English Description بس بالعربي', 'Lesson Six', 'This lesson will teach you how to configure wheels and connect it to main item. have fun !', '', 6, 3, 1, '600', '', 0, 1, 'https://d3ezpvziuomw66.cloudfront.net/output/hls-input/last-test-video-audio.ts.m3u8', '77091771', 1, 1, 1, '2018-06-10 11:46:31', '2018-11-05 17:07:49', NULL, 1),
(6, 'Lesson Five بالعربي', 'Description بالعربي', 'Lesson Five', 'This lesson will teach you how to configure wheels and connect it to main item. have fun !', '', 6, 2, 1, '575', '', 0, 3, 'https://d3ezpvziuomw66.cloudfront.net/output/hls-input/EurekaTechAcademy.ts.m3u8', '77091771', 1, 1, 1, '2018-06-10 11:48:06', '2018-11-05 17:08:40', NULL, 1),
(10, 'ليسن ون', 'ليسن ون', 'Lesson One', 'English Description\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non,', NULL, 6, 1, 1, '152', '', 0, 1, '', '77091771', 1, 1, 1, '2018-07-09 10:06:57', '2019-10-15 12:58:18', NULL, 1),
(11, 'Lesson Two بالعربي', 'Lesson Two Description بس بالعربي', 'LessonTwo', 'English Description\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a nisi eu tortor bibendum consequat sit amet vitae augue. Maecenas in interdum neque, in aliquam orci. Donec a ante efficitur, molestie turpis non,', '', 6, 1, 1, '180', '', 0, 2, 'https://d3ezpvziuomw66.cloudfront.net/output/hls-input/test3.ts.m3u8', '77091771', 1, 1, 0, '2018-07-09 10:08:25', '2018-07-23 10:30:55', NULL, NULL),
(12, 'Lesson Seven', 'Description', 'game - Lesson one', 'Description', NULL, 17, 7, 1, '90', '', 0, 1, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/1571703387MyVideo.ts.m3u8', '77091771', 1, 1, 0, '2018-07-12 07:26:12', '2020-04-17 01:35:57', NULL, 1),
(13, 'Lesson Seven', 'Description', 'game - Lesson two', 'Description', '/uploads/course-list.jpg', 17, 8, 1, '780', '', 0, 1, 'https://d3ezpvziuomw66.cloudfront.net/output/hls-input/test.ts.m3u8', '77091771', 1, 1, 0, '2018-07-12 07:28:34', '2020-04-17 01:36:24', NULL, 1),
(20, 'test', 'teset', 'test', 'test', '', 81, 11, 19, '150', '', 0, 3, '', '77091771', 1, 1, 0, '2019-10-16 19:27:19', '2019-10-16 19:27:19', 1, NULL),
(21, 'تست', 'something', 'test 2', 'something', '', 6, 1, 1, '240', '', 0, 3, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/lorem.ts.m3u8', '77091771', 1, 1, 0, '2019-10-17 05:52:00', '2019-10-19 21:41:36', 1, 1),
(22, 'Large File Test', 'وصف', 'Large Files Test', 'something', '', 6, 3, 1, '260', '', 0, 2, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/1571703387MyVideo.ts.m3u8', '77091771', 1, 1, 0, '2019-10-17 06:04:41', '2019-10-22 00:54:40', 1, 1),
(23, 'فيديو مجاني', 'الدرس المجاني يجب أن يكون لينك يوتوب', 'youtube embed', 'free lesson needs to be a youtube video embed', '', 6, 3, 1, '280', '', 0, 3, '', '77091771', 1, 1, 0, '2019-10-17 06:06:54', '2019-10-17 06:06:54', 1, NULL),
(24, 'تست', 'اي توصيف بيمشي حتى لو نقطة\r\nو الفكرة لازم تكون معمول ال levels -> courses - > sessions\r\nلان كل فيديو تابع لهدول ال 3  و اذا ما في  \r\nsession \r\nكمان رح يعطي خطا', 'test', 'كل المربعات النصية لازم تتعبا و الا رح يعطي خطا \r\nال teacher  بيحط مشان تنحسب لكل استاذ ئدي بيطلعلو ع الكورس بس يسجلو بايميلاتن ممكن يتغير من هون \r\nال session  بحدد وين بيطلع الفيديو', '/uploads/Energeyimg-1.jpg', 6, 1, 1, '350', '', 0, 15, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/lorem.ts.m3u8', '77091771', 1, 1, 0, '2019-10-17 19:27:08', '2020-05-29 04:40:29', 1, 1),
(26, 'تست', '.', 'test 2', '.', '', 81, 11, 1, '390', '', 0, 3, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/SampleVideo_1280x720_1mb.ts.m3u8', '77091771', 1, 1, 0, '2019-10-20 06:12:36', '2019-10-20 06:12:36', 1, NULL),
(27, 'تست', '.', 'test 3', '.', '', 81, 11, 1, '400', '', 0, 4, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/SampleVideo_1280x720_1mb.ts.m3u8', '77091771', 1, 1, 0, '2019-10-20 06:15:41', '2019-10-20 06:16:57', 1, 1),
(28, 'تست', '.', 'test 4', '.', '', 81, 11, 1, '421', '', 0, 5, '', '77091771', 1, 1, 0, '2019-10-20 06:27:39', '2019-10-20 06:27:39', 1, NULL),
(29, 'تست', '.', 'test 4', '.', '', 81, 11, 1, '470', '', 0, 5, '', '77091771', 1, 1, 0, '2019-10-20 06:28:14', '2019-10-20 06:29:19', 1, 1),
(30, 'تست', '.', 'test 5', '.', '', 81, 11, 1, '452', '', 0, 6, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/SampleVideo_1280x720_1mb.ts.m3u8', '77091771', 1, 1, 0, '2019-10-20 06:30:13', '2019-10-20 06:33:17', 1, 1),
(31, 'testetet', 'tettette', 'test555', 'test', '', 81, 11, 13, '190', '', 0, 1, '', '77091771', 1, 1, 0, '2019-10-20 16:51:13', '2019-10-20 16:51:13', 1, NULL),
(34, 'qqqqqqqqqqq', 'qqqqqqqqqqq', 'qqqqqq', 'qqqqqqqqqqq', '', 81, 11, 1, '785', '', 0, 1, '', '77091771', 1, 1, 0, '2019-10-21 19:27:16', '2019-10-21 19:27:16', 1, NULL),
(42, 'مقدمة إلى الاردوينو الجزء الأول', 'سيكون هذا الدرس مقدمة إلى الأردوينو والمنافذ عليها.', 'Introduction To Arduino Part (1)', 'This video will be an introduction to Arduino and the pins.', '', 82, 12, 20, '652', '', 0, 1, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15720855711A.ts.m3u8', '77091771', 1, 1, 0, '2019-10-26 10:26:11', '2019-10-26 10:26:15', 1, NULL),
(44, 'مقدمة إلى الاردوينو الجزء الثاني', 'في هذا الفيديو سنتحدث عن بقية المنافذ في الاردوينو.', 'Introduction To Arduino Part (2)', 'In this video, we will talk about the rest of the pins in Arduino.', '', 82, 12, 20, '644', '', 0, 2, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15721287561B.ts.m3u8', '77091771', 1, 1, 0, '2019-10-26 22:25:56', '2019-10-26 22:26:01', 1, NULL),
(45, 'بيئة برمجة الاردوينو', 'في هذا الفيديو سنتعلم كيفية تنزيل بيئة برمجة الاردوينو.', 'Arduino IDE', 'In This video, we will learn how to download Arduino IDE.', '', 82, 12, 20, '741', '', 0, 3, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15722028942A.ts.m3u8', '77091771', 1, 1, 0, '2019-10-27 19:01:34', '2019-10-27 19:01:37', 1, NULL),
(46, 'نظرة عامة على بيئة برمجة الاردوينو', 'في هذا الفيديو سنتحدث عن بيئة برمجة الأردوينو وكيفية استعمالها.', 'Overview on Arduino IDE', 'In this video, we will talk about Arduino IDE and how to use it.', '', 82, 12, 20, '159', '', 0, 4, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15722074852B.ts.m3u8', '77091771', 1, 1, 0, '2019-10-27 20:18:05', '2019-10-27 20:18:06', 1, NULL),
(47, 'البرمجة بالاردوينو الجزء الأول', 'في هذا الدرس سنتحدث عن بعض الجمل البرمجية بالاردوينو.', 'Programming In Arduino Part (1)', 'In this video, we will discuss some programming statements in Arduino C.', '', 82, 13, 20, '987', '', 0, 1, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/1588569625lessons.ts.m3u8', '77091771', 1, 1, 0, '2019-10-29 19:55:48', '2020-05-03 23:50:35', 1, 1),
(48, 'البرمجة بالاردوينو الجزء الثاني', 'في هذا الدرس ستنتحدث عن مزيد من الجمل البرمجية بالاردوينو.', 'Programming In Arduino Part (2)', 'In this video, we will discuss more programming statements in Arduino C.', '', 82, 13, 20, '785', '', 0, 2, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15723799463B.ts.m3u8', '77091771', 1, 1, 0, '2019-10-29 20:12:26', '2019-10-29 20:12:30', 1, NULL),
(49, 'مقدمة إلى الاردوينو الجزء الثالث', 'في هذا الدرس ستنتحدث عن مزيد من الجمل البرمجية بالاردوينو.', 'Programming In Arduino Part (3)', 'In this video, we will discuss more programming statements in Arduino C.', '', 82, 13, 20, '854', '', 0, 3, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15723811223C.ts.m3u8', '77091771', 1, 1, 0, '2019-10-29 20:32:02', '2019-10-29 20:32:06', 1, NULL),
(50, 'التحكم بالليد باستعمال الاردوينو الجزء الأول', 'في هذا الدرس سنبدأ العمل على أول مثال تطبيقي على الاردوينو.', 'Controlling LED Using Arduino Part (1)', 'In this video, we will work on the first activity on Arduino.', '', 82, 13, 20, '125', '', 0, 4, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15723815744A.ts.m3u8', '77091771', 1, 1, 0, '2019-10-29 20:39:34', '2019-10-29 20:39:38', 1, NULL),
(51, 'التحكم بالليد باستعمال الاردوينو الجزء الثاني', 'في هذا الدرس سنكتب البرمجة الخاصة بالمثال الذي بدأنا به الدرس السابق.', 'Controlling LED Using Arduino Part (2)', 'In this video, we will write the code for Blink activity.', '', 82, 13, 20, '156', '', 0, 5, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15723817224B.ts.m3u8', '77091771', 1, 1, 0, '2019-10-29 20:42:02', '2019-10-29 20:42:05', 1, NULL),
(52, 'Serial Monitor', 'في هذا الفيديو سنتحدث عن طريقة تواصل الاردوينو والكومبيوتر وعن شاشة العرض.', 'Serial Monitor', 'In this video, we will talk about Serial Commination and Serial Monitor.', '', 82, 14, 1, '789', '', 0, 1, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15728130465A.ts.m3u8', '77091771', 1, 1, 0, '2019-11-03 20:30:46', '2019-11-03 20:33:01', 1, 1),
(53, 'تطبيق عملي على Serial Monitor.', 'في هذا الفيديو سنقوم باستعمال Serial Monitor لطباعة متغيرات.', 'Serial Monitor Activity', 'In this video, we will use Serial Monitor to print variables.', '', 82, 14, 20, '745', '', 0, 2, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15728138105B.ts.m3u8', '77091771', 1, 1, 0, '2019-11-03 20:43:30', '2019-11-03 20:43:32', 1, NULL),
(54, 'المفاتيح الكهربائية مع الاردوينو', 'في هذا الفيديو سنتحدث عن المفاتيح الكهربائية وكيفية استعمالها مع الاردوينو.', 'Pushbuttons With Arduino', 'In this video, we will talk about pushbutton and how to use it with Arduino.', '', 82, 14, 20, '785', '', 0, 3, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15728932536A.ts.m3u8', '77091771', 1, 1, 0, '2019-11-04 18:47:33', '2019-11-04 18:47:42', 1, NULL),
(55, 'جملة IF الشرطية', 'في هذا الفيديو سنتناول جملة IF الشرطية.', 'IF Statement', 'In this video, we will talk about IF Statement.', '', 82, 14, 20, '654', '', 0, 4, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15728936166B.ts.m3u8', '77091771', 1, 1, 0, '2019-11-04 18:53:36', '2019-11-04 18:53:40', 1, NULL),
(56, 'التحكم بضوء LED عن طريق مفتاح كهربائي', 'في هذا الفيديو سنجعل المفتاح الكهربائي يتحكم بضوء LED.', 'Controlling LED By Pushbutton', 'In this video, we will learn how to make pushbutton turn on and off a LED.', '', 82, 14, 20, '456', '', 0, 5, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15728938856C.ts.m3u8', '77091771', 1, 1, 0, '2019-11-04 18:58:05', '2019-11-04 18:58:09', 1, NULL),
(57, 'المقاومة الضوئية', 'في هذا الفيديو سنتعلم كيفية استعمال المقاومة الضوئية مع الاردوينو.', 'LDR', 'In this video, we will learn how to use LDR with Arduino.', '', 82, 15, 20, '354', '', 0, 1, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15728939857A.ts.m3u8', '77091771', 1, 1, 0, '2019-11-04 18:59:45', '2019-11-04 18:59:48', 1, NULL),
(58, 'طباعة قراءات المقاومة الضوئية على شاشة السيريال', 'في هذا الفيديو سنتعلم كيفية طباعة قراءات المقاومة الضوئية على شاشة السيريال.', 'Printing LDR Values On Serial Monitor', 'In this video, we will learn how to print LDR values on Serial Monitor.', '', 82, 15, 20, '365', '', 0, 2, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15728945217B.ts.m3u8', '77091771', 1, 1, 0, '2019-11-04 19:08:41', '2019-11-04 19:08:47', 1, NULL),
(59, 'التحكم بضوء LED عن طريق المقاومة الضوئية', 'في هذا الفيديو سنتعلم كيفية استعمال جملة Map.', 'Controlling LED By LDR', 'In this video, we will learn how to use Map statement.', '', 82, 15, 20, '564', '', 0, 3, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15728947767C.ts.m3u8', '77091771', 1, 1, 0, '2019-11-04 19:12:56', '2019-11-04 19:13:04', 1, NULL),
(60, 'المقاومة المتغيرة مع الاردوينو', 'في هذا الفيديو سنتعلم كيفية استعمال المقاومة المتغيرة مع الاردوينو.', 'Potentiometer with Arduino', 'In this video, we will work on Potentiometer with Arduino.', '', 82, 15, 20, '541', '', 0, 4, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15728957908.ts.m3u8', '77091771', 1, 1, 0, '2019-11-04 19:29:50', '2019-11-04 19:29:54', 1, NULL),
(61, 'For Loop', 'في هذا الفيديو سنتعلم جملة برمجية جديدة وهي For loop.', 'For Loop', 'In this video, we will learn a new programming statement (For Loop).', '', 82, 16, 20, '521', '', 0, 2, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/157289694610A.ts.m3u8', '77091771', 1, 1, 0, '2019-11-04 19:49:06', '2019-11-04 19:49:10', 1, NULL),
(62, 'RGB LED', 'في هذا الفيديو سنتعلم كيفية استعمال RGB LED مع الاردوينو.', 'RGB LED', 'In this video, we will about RGB LED and how to use it with Arduino.', '', 82, 16, 20, '245', '', 0, 1, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/15728969769.ts.m3u8', '77091771', 1, 1, 0, '2019-11-04 19:49:36', '2019-11-04 19:49:46', 1, NULL),
(63, 'تشغيل LED واطفاءه تدريجياً', 'في هذا الفيديو سنجعل ضوء LED يشتغل ويطفئ تدريجياً.', 'LED Fading', 'In this video, we will make the LED fading in & out.', '', 82, 16, 20, '520', '', 0, 3, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/157289739610B.ts.m3u8', '77091771', 1, 1, 0, '2019-11-04 19:56:36', '2019-11-04 19:56:41', 1, NULL),
(64, 'حساس الحرارة LM35 مع الاردوينو', 'في هذا الفيديو سنتناول حساس الحرارة LM35.', 'LM35 Sensor With Arduino', 'In this video, we will take a look at temperature sensor LM35.', '', 82, 17, 20, '841', '', 0, 1, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/157298794611A.ts.m3u8', '77091771', 1, 1, 0, '2019-11-05 21:05:46', '2019-11-05 21:05:47', 1, NULL),
(65, 'طباعة قراءات حساس الحرارة على شاشة السيريال', 'في هذا الفيديو سنعرض قراءات حساس الحرارة LM35 على شاشة السيريال.', 'LM35 Sensor And Serial Monitor', 'In this video, we will use serial monitor to view temperature values using LM35.', '', 82, 17, 20, '611', '', 0, 2, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/157298841611B.ts.m3u8', '77091771', 1, 1, 0, '2019-11-05 21:13:36', '2019-11-05 21:13:42', 1, NULL),
(66, 'Servo Motors', 'في هذا الفيديو سنتحدث عن Servo Motors .', 'Servo Motors', 'In this video, we will talk about Servo Motors.', '', 82, 17, 20, '612', '', 0, 3, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/157298866812A.ts.m3u8', '77091771', 1, 1, 0, '2019-11-05 21:17:48', '2019-11-05 21:17:51', 1, NULL),
(67, 'Sweep', 'في هذا الفيديو سنجعل Servo Motor يتحرك باستعمال جملة For.', 'Sweep', 'In this video, we will make Servo Motor sweep using For loop.', '', 82, 17, 20, '632', '', 0, 5, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/157298890512c.ts.m3u8', '77091771', 1, 1, 0, '2019-11-05 21:21:45', '2019-11-05 21:21:48', 1, NULL),
(68, 'Knob', 'في هذا الفيديو سنتعلم كيفية التحكم بدرجة إلتفاف Servo Motor باستعمال المقاومة المتغيرة.', 'Knob', 'In this video,  we will know how to control Servo Motor using potentiometer.', '', 82, 17, 1, '521', '', 0, 4, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/157298905112B.ts.m3u8', '77091771', 1, 1, 0, '2019-11-05 21:24:11', '2020-03-25 03:47:48', 1, 1),
(69, 'RBL-2 ONE', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'RBL-2 ONE', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '/uploads/Energeyimg-2.jpg', 18, 19, 1, '520', '', 0, 1, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/1587109943course-list.jpg', '77091771', 1, 1, 0, '2020-04-17 02:22:23', '2020-04-17 04:07:09', 1, 1),
(70, 'RBD-2 TWO', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'RBD-2 TWO', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '/uploads/Energeyimg-1.jpg', 18, 19, 1, '741', '', 0, 2, 'https://d31z2wsdi58g5n.cloudfront.net/output/hls-input/1588337317lessons.ts.m3u8', '77091771', 1, 1, 0, '2020-04-17 02:23:49', '2020-05-08 04:24:30', 1, 1),
(71, 'recos', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the rel', 'recos', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the rel', '/uploads/course-1.jpg', 17, 8, 1, '611', '', 0, 1, '', '77091771', 1, 1, 0, '2020-05-08 04:45:31', '2020-05-11 04:25:40', 1, 1),
(72, 'test', 'test', 'test', 'test', '/uploads/Energeyimg-1.jpg', 6, 2, 1, '250', '', 0, 1, '', '77091771', 1, 1, 0, '2020-05-29 04:59:04', '2020-05-29 05:00:29', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vision`
--

CREATE TABLE `vision` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `active` tinyint(1) DEFAULT NULL,
  `image_path_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `image_path_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vision`
--

INSERT INTO `vision` (`id`, `name_ar`, `name_en`, `active`, `image_path_ar`, `image_path_en`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(49, 'vision1', 'vision1', 1, '', '/uploads/vision-5.jpg', '2020-06-02 02:09:42', '2020-06-03 00:57:39', 1, 1),
(50, 'vision2', 'vision2', 1, '', '/uploads/vision-4.jpg', '2020-06-02 02:10:47', '2020-06-03 00:57:32', 1, 1),
(51, 'v3', 'vision3', 1, '', '/uploads/vision-3.jpg', '2020-06-02 02:13:34', '2020-06-03 00:57:25', 1, 1),
(52, 'v4', 'vision4', 1, '', '/uploads/vision-2.jpg', '2020-06-02 02:13:48', '2020-06-03 00:57:18', 1, 1),
(53, 'v5', 'vision5', 1, '', '/uploads/vision-1.jpg', '2020-06-02 02:14:03', '2020-06-03 00:57:12', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abandoned_carts`
--
ALTER TABLE `abandoned_carts`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `achievements`
--
ALTER TABLE `achievements`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `banner_images`
--
ALTER TABLE `banner_images`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `cart_detail`
--
ALTER TABLE `cart_detail`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `choices`
--
ALTER TABLE `choices`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `course_slug_unique` (`slug`) USING BTREE;

--
-- Indexes for table `course_category`
--
ALTER TABLE `course_category`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `course_status`
--
ALTER TABLE `course_status`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `course_type`
--
ALTER TABLE `course_type`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `downloadable`
--
ALTER TABLE `downloadable`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `email_broadcast`
--
ALTER TABLE `email_broadcast`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`email_temp_id`) USING BTREE;

--
-- Indexes for table `eurekian_stars_text`
--
ALTER TABLE `eurekian_stars_text`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `footer`
--
ALTER TABLE `footer`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `groups_users`
--
ALTER TABLE `groups_users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `item_img`
--
ALTER TABLE `item_img`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `item_qty`
--
ALTER TABLE `item_qty`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `kit`
--
ALTER TABLE `kit`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `kit_img`
--
ALTER TABLE `kit_img`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `kit_item`
--
ALTER TABLE `kit_item`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `level_slug_unique` (`slug`) USING BTREE;

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `order_tracking`
--
ALTER TABLE `order_tracking`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`) USING BTREE;

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `quiz_report`
--
ALTER TABLE `quiz_report`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `slugs`
--
ALTER TABLE `slugs`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `stars`
--
ALTER TABLE `stars`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `stripe_customer`
--
ALTER TABLE `stripe_customer`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `stripe_plan`
--
ALTER TABLE `stripe_plan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `subcription_model`
--
ALTER TABLE `subcription_model`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `supplier_info`
--
ALTER TABLE `supplier_info`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `supplier_item`
--
ALTER TABLE `supplier_item`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `teacher_fees`
--
ALTER TABLE `teacher_fees`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `users_email_unique` (`email`) USING BTREE;

--
-- Indexes for table `user_progress`
--
ALTER TABLE `user_progress`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `vision`
--
ALTER TABLE `vision`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abandoned_carts`
--
ALTER TABLE `abandoned_carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `achievements`
--
ALTER TABLE `achievements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `banner_images`
--
ALTER TABLE `banner_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `cart_detail`
--
ALTER TABLE `cart_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT for table `choices`
--
ALTER TABLE `choices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=272;
--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `course_category`
--
ALTER TABLE `course_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `course_status`
--
ALTER TABLE `course_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `course_type`
--
ALTER TABLE `course_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `downloadable`
--
ALTER TABLE `downloadable`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `email_broadcast`
--
ALTER TABLE `email_broadcast`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `email_temp_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `eurekian_stars_text`
--
ALTER TABLE `eurekian_stars_text`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `footer`
--
ALTER TABLE `footer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `groups_users`
--
ALTER TABLE `groups_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;
--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `item_img`
--
ALTER TABLE `item_img`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `item_qty`
--
ALTER TABLE `item_qty`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kit`
--
ALTER TABLE `kit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `kit_img`
--
ALTER TABLE `kit_img`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `kit_item`
--
ALTER TABLE `kit_item`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215;
--
-- AUTO_INCREMENT for table `order_tracking`
--
ALTER TABLE `order_tracking`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=210;
--
-- AUTO_INCREMENT for table `payment_method`
--
ALTER TABLE `payment_method`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `quiz_report`
--
ALTER TABLE `quiz_report`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=378;
--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `slugs`
--
ALTER TABLE `slugs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `stars`
--
ALTER TABLE `stars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `stripe_customer`
--
ALTER TABLE `stripe_customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stripe_plan`
--
ALTER TABLE `stripe_plan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subcription_model`
--
ALTER TABLE `subcription_model`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `subscription`
--
ALTER TABLE `subscription`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;
--
-- AUTO_INCREMENT for table `supplier_info`
--
ALTER TABLE `supplier_info`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `supplier_item`
--
ALTER TABLE `supplier_item`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `teacher_fees`
--
ALTER TABLE `teacher_fees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `user_progress`
--
ALTER TABLE `user_progress`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `vision`
--
ALTER TABLE `vision`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
