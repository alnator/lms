(function ($){
  // herf =  window.location.href;
  herf = window.location.href;

  link = herf.replace('/admin' , '/admin');
  loadProduct(link);
})(jQuery);

function loadProduct(link){
  $("#groups-details-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: link,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "user.id"       , title: 'ID'         , type: "text", width: 5},
            {name: "user.name"  , title: 'User Name'     , type: "text", width: 5},            
            {name: "user.phone"  , title: 'Phone'     , type: "text", width: 5},            
            {name: "user.email"  , title: 'Email'     , type: "text", width: 5},            
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $details = $('<a class="btn btn-block btn-default btn-sm">Details</a>');
                      $details.attr('href',siteurl+`/admin/users/${item.user.id}/courses`);
                  return $result.add($details);
              },
            },
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-danger btn-sm">Remove from group</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      deleteGroup(item.group_id , item.user_id);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function deleteGroup(groupId, userId){
    swal({
    title: "Are you sure?",
    text: "Are you sure you want to remove this member form this group!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No"
  },
  function(){
    $.ajax({
        type: "POST",
        url: siteurl+`/admin/groups/${groupId}/member/${userId}/remove`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response > 0){
        swal("Removed!", "Member removed successfully.", "success");
        $('#groups-details-grid').jsGrid('render');
      }
    });
  });
}

