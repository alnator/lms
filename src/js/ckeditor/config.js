/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	// config.disallowedContent = '';
	config.allowedContent = true;
	config.removeFormatAttributes = '';
	config.height = 600;
	CKEDITOR.editorConfig = function( config ) {
	  // config.extraPlugins = 'imageuploader';
	};
	// config.width = 1400; 
};
