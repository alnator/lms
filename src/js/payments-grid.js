(function ($){
  loadPayments();
})(jQuery);

function loadPayments(){
  $("#payments-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $('#filter-data').val(JSON.stringify(filter));
                $.ajax({
                    type: "POST",
                    url: siteurl+`/admin/payments`,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"        , title: 'ID'         , type: "text", width: 5},
            {name: "user.email"   , title: 'User email'    , type: "text", width: 5},
            {name: "amount"    , title: 'Amount'     , type: "text", width: 5},
            {name: "payment_method.name"    , title: 'Payment Method'     , type: "text", width: 5},
            {name: "invoice_id", title: 'Invoice ID' , type: "text", width: 5},
            {name: "coupon"    , title: 'Coupon'     , type: "text", width: 5},
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-default btn-sm">Edit</a>');
                      $edit.attr('href',siteurl+`/admin/payments/`+item.id+`/edit`);
                  return $result.add($edit);
              },
            },
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-danger btn-sm">Delete</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      deleteLevel(item.id);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function deleteLevel(LevelId){
    swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this Payment!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No"
  },
  function(){
    $.ajax({
        type: "POST",
        url: siteurl+`/admin/payments/delete/${LevelId}`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response > 0){
        swal("Deleted!", "Payment deleted successfully.", "success");
        $('#payments-grid').jsGrid('render');
      }
    });
  });
}

$('#pdf-export-btn').click(function(){
  filter = $('#filter-data').val();
  link = siteurl+`/admin/payments/pdf?filter=${filter}`;
  window.location.href = link;
});

$('#excel-export-btn').click(function(){
  filter = $('#filter-data').val();
  link = siteurl+`/admin/payments/excel/xlsx?filter=${filter}`;
  window.location.href = link;
});

$('#csv-export-btn').click(function(){
  filter = $('#filter-data').val();
  link = siteurl+`/admin/payments/excel/csv?filter=${filter}`;
  window.location.href = link;
});


