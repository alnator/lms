link = "";
(function ($){
  str = window.location.href;
  /*prefix = "";
  suffix = "";

  flag =0 ;
  for (i=1 ; i < str.length ; i++){
    if (str[i] == '/' && str[i-1] == '/')
    {
      flag = 1;
    }
    else if ( str[i] == '/' && flag == 1 ) 
    {
      prefix = str.substr(0,i+1);
      suffix = str.substr(i+1 , str.length);
      break;
    }
    
  } 

  link = prefix + "api/" +suffix;*/
  var items = str.split('admin');
  console.log(items);
  link = items[0]+'api/admin'+items[1];
  //link = siteurl+"/api/admin/courses/18/users";
  loadUsers();

})(jQuery);

function loadUsers(){
  $("#course-users-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: link, 
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"       , title: 'ID'         , type: "text", width: 5},
            {name: "user_id"  , title: 'User ID'     , type: "text", width: 5},
            {name: "payment_id"  , title: 'Payment ID'     , type: "text", width: 5},
            {name: "start_date"  , title: 'Start Date'     , type: "text", width: 5},
            {name: "end_date"  , title: 'End Date'     , type: "text", width: 5},
            {name: "status"  , title: 'Status'     , type: "text", width: 5},
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-default btn-sm">See Progress</a>');
                      $edit.attr('href',siteurl+`/admin/users/`+item.user_id+`/courses/`+item.course_id);
                  return $result.add($edit);
              },
            },
            
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-danger btn-sm">Delete</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      deleteUser(item.id);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function deleteUser(SubsId){
    swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this User!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No"
  },
  function(){
    $.ajax({
        type: "POST",
        url: link + `/delete/${SubsId}`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response > 0){
        swal("Deleted!", "User deleted successfully.", "success");
        $('#course-users-grid').jsGrid('render');
      }
    });
  });
}

