(function ($){
  link = window.location.href;
  loadKitItems(link);
})(jQuery);

function loadKitItems(link){
  $("#shipping-list-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: link,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"        , title: 'ID'         , type: "text", width: 5},
            {name: "user.email"   , title: 'User'    , type: "text", width: 5},
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-info btn-sm">Show Items</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      openModal(item.id);
                  });
                  return $result.add($del);
                },
            },
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-success btn-sm">Ship</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      shipItem(item.id);
                  });
                return $result.add($del);
              },
            },
        ]
    });
}


function shipItem(itemId){
  swal({
    title: "Choose Pickup Date",
    html:'<div class="input-group date"><input type="text" class="form-control"><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span></div>',
    type: "warning",
    onOpen: function() {
        $('.input-group.date input').datepicker({
            startDate: "tomorrow",
            format: 'yyyy-mm-dd 12:00:00',
            maxViewMode: 0,
            clearBtn: true,
            forceParse: false,
            // daysOfWeekDisabled: "5,6",
            calendarWeeks: true,
            todayHighlight: true
        });
    }
}).then(function(){ 
    $.ajax({
        type: "POST",
        url: site_url+`/supplier/shipping-list/${itemId}/ship`,
        data: { 'date' : $('.input-group.date input').val() },
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response == 'ok'){
        swal("Pickup Created !", "Pickup Date Confirmed.", "success");
        $('#waiting-list-grid').jsGrid('render');
      }
      else {
        swal("Error" , response , 'error');
      }
    });
  });
}

function openModal(item_id){
    $('#edit-create-modal').modal('show');
    $('.modal-backdrop').css('z-index' , '1000');
    $('.modal-backdrop').addClass('show');
    $('body').addClass('modal-open');
    $('#edit-create-modal').addClass('show');
    $('#edit-create-modal').css('display' , 'block');
    /*console.log(items);
    itemsDetails = [];
    itemsIds =[];
    for (var i = items.length - 1; i >= 0; i--) {
        if (itemsDetails[items[i].item_id] != null){
            itemsDetails[items[i].item_id].count += 1;
        }
        else {
            itemsDetails[items[i].item_id] = {};
            itemsDetails[items[i].item_id].count = 1;
            itemsDetails[items[i].item_id].name = items[i].item.name_en;            
        }
    }
*/
$('#fill-here').empty();
$.ajax({
        type: "GET",
        url: site_url+`/supplier/shipping-list/${item_id}/shipdetails`,
        data: { 'date' : $('.input-group.date input').val() },
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
        $('#fill-here').append(response);

      });
    
    str = '';
    /*for (var key in itemsDetails) {
        str += '<tr>';
        name = itemsDetails[key].name;
        count = itemsDetails[key].count;
        str += `<td>${name}</td><td>${count}</td>`;
        str+='</tr>';
    }*/
    

}

function closeModal(){
    $('body').removeClass('modal-open');
    $('.modal-backdrop').removeClass('show');
    $('.modal-backdrop').css('z-index' , '-1000');
    $('#edit-create-modal').removeClass('show');
    $('#edit-create-modal').css('display' , 'none');
    $('#edit-create-modal').modal('hide');
}