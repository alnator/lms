(function ($){
    id= $('#supplier_id').val();
    loadItems(id);
})(jQuery);

function loadItems(id){
  $("#items-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: site_url+'/supplier/items-supplied',
                    dataType: "json",
                    data: {filter , id},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"        , title: 'ID'         , type: "text", width: 5},
            {name: "item.name_en"   , title: 'Name (EN)'    , type: "text", width: 5},
            {name: "item.name_ar"   , title: 'Name (AR)'    , type: "text", width: 5},
            {name: "price"   , title: 'Price'    , type: "text", width: 5},
            {name: "qty"   , title: 'Quantity'    , type: "text", width: 5},
            {name: "active"   , title: 'Active'    , type: "checkbox", width: 5},
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-default btn-sm">Edit</a>');
                      $edit.attr('href',site_url+`/supplier/items-supplied/${item.id}/update`);
                  return $result.add($edit);
              },
            }   
            //,
            // {
            //     type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
            //     itemTemplate: function (value, item) {
            //       var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
            //       var $del = $('<a class="btn btn-block btn-danger btn-sm">Delete</a>');
            //       $del.on('click', function (e) {
            //           e.stopPropagation();
            //           e.preventDefault();
            //           deleteItem(item.id);
            //       });
            //     return $result.add($del);
            //   },
            // },
        ]
    });
}

// function deleteItem(ItemId){
//     swal({
//     title: "Are you sure?",
//     text: "Are you sure you want to Remove this Item!",
//     type: "warning",
//     showCancelButton: true,
//     confirmButtonColor: "#DD6B55",
//     confirmButtonText: "Yes",
//     cancelButtonText: "No"
//   },
//   function(){
//     $.ajax({
//         type: "POST",
//         url: `/api/supplier/items-supplied/${ItemId}/delete`,
//         headers: {
//             "x-csrf-token": $("[name=_token]").val()
//         },
//     }).done(response => {
//       if(response > 0){
//         swal("Removed!", "Item Removed successfully.", "success");
//         $('#items-grid').jsGrid('render');
//       }
//     });
//   });
// }
