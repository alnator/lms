(function ($){
  loadEmails();

})(jQuery);

function loadEmails(){
  $("#emails-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: siteurl+`/api/admin/emails`,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"           , title: 'ID'        , type: "text", width: 5},
            {name: "name"         , title: 'Names'     , type: "text", width: 5},
            {name: "subject"      , title: 'Subjects'  , type: "text", width: 5},
            {name: "email"        , title: 'Emails'    , type: "text", width: 5},
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-info btn-sm">See Email</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      
                      showEmail(item);

                  });
                return $result.add($del);
              },
            },
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-danger btn-sm">Delete</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      deleteProduct(item.id);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function deleteProduct(emailId){
    swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this Email!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No"
  },
  function(){
    $.ajax({
        type: "POST",
        url: siteurl+`/api/admin/emails/delete/${emailId}`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response > 0){
        swal("Deleted!", "Email deleted successfully.", "success");
        $('#emails-grid').jsGrid('render');
      }
    });
  });
}

function showEmail(item){
  $('#email').val(item.email);
  $('#name').val(item.name);
  $('#subject').val(item.subject);
  $('#text').text(item.text);
  $('#date').text(item.created_at);
  $('#display').addClass('display');
}