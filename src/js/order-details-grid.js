link = "";
(function ($){
  str = window.location.href;
  link = str;
  // .replace('/admin' , '/api/admin');
  loadItems();
})(jQuery);

function loadItems(){
  $("#order-details-grid").jsGrid({
        filtering: false,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: link,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"                       , title: 'ID'         , type: "text", width: 5},
            {name: "supplier.email"           , title: 'Supplier Email'         , type: "text", width: 5},
            {name: "price"                    , title: 'Price'         , type: "text", width: 5},
            {name: "qty"                    , title: 'Qty'         , type: "text", width: 5},
            {name: "final_total"                    , title: 'Total Price'         , type: "text", width: 5},
            //{name: "selling_price"            , title: 'Selling Price' , type: "text", width: 5},
            {name: "type"            , title: 'Type' , type: "text", width: 5},
            {name: "itemname"             , title: 'Item Name'         , type: "text", width: 5},

        ]
    });
}

