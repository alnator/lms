function showBillingSingleCourse(){
	teacherId = $('#teacher-select').val();
	fromDate = $('#from-date').val();
	toDate = $('#to-date').val();
	aws = $('#aws-bill').val();
	loadSingleCourseAmount(teacherId ,fromDate , toDate ,aws , renderAmountSingleCourse );	
	
}
function renderAmountSingleCourse(response , callback){
	$('#single-course-div').removeClass('d-none');
	$('#single-course').html(response.singlecoursesAmount + " $");
  callback();
}
function showBillingSubscription(){

	teacherId = $('#teacher-select').val();
	fromDate = $('#from-date').val();
	toDate = $('#to-date').val();
	aws = $('#aws-bill').val();

	loadSubscriptionAmount(teacherId , fromDate , toDate , aws); //params
}
function renderAmountSubscription(response){

	$("#subscription-div").removeClass('d-none');
	$('#subscriptions').html(response.subscriptionAmount +" $");
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var grid ;
function loadSingleCourseAmount(teacherId,from,to,aws,callback){
  $('#course-grid').jsGrid({
        filtering: false,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: false,
        editing: false,
        inserting: false,
        subGrid: false,
        pagerFormat: "Pages: {prev} {next} {pageIndex} of {pageCount}",
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                 $.ajax({
				    type: "POST",
			        url: siteurl+`/admin/teacher/billing`,
			        data: {'teacher_id' : teacherId , 'from_date' : from , 'to_date' : to , 'aws':aws , 'single_course' : true},
			        headers: {
			            "x-csrf-token": $("[name=_token]").val()
			        },
				}).done(response =>  {
          $('#teacher-views').val(response.teacherViews);
          $('#teacher-percentage').val(response.teacherPercentage);
          data.resolve({ data: response.data, itemsCount: [response.total] });
					callback(response ,showBillingSubscription );
				});
                return data.promise();
            },
        },

        fields: [
          {name: "subscriptionIds.subscription.user.email", title: 'User', type: "text", width: 15},
          {name: "subscriptionIds.course.title_ar", title: 'Course', type: "text", width: 15},
          {name: "amount", title: 'Amount', type: "number", width: 5},
          {name: "amountWithOutFees", title: 'Amount With out Fees', type: "number",
          itemTemplate: function(value,item){
            amount = item.amount;
            percentFees =item.payment_method.percent_fees;
            fixedFees =item.payment_method.fix_fees;
            netAmount = 0;
            fixedNetAmount = 0;
            percentNetAmount = 0;
            if(percentFees > 0){
              percentNetAmount =  amount * (percentFees / 100);
            }
            if(fixedFees > 0){
              amount = amount - fixedFees;
            }
            return (amount - percentNetAmount).toFixed(2);
          }, width: 5},
          {name: "profit", title: 'Profit', type: "number",
          itemTemplate: function(value,item){
            amount = item.amount;
            teacherView = $('#teacher-views').val();
            teacherPercentage = $('#teacher-percentage').val();
            percentFees =item.payment_method.percent_fees;
            fixedFees =item.payment_method.fix_fees;
            netAmount = 0;
            fixedNetAmount = 0;
            percentNetAmount = 0;
            if(percentFees > 0){
              percentNetAmount =  amount * (percentFees / 100);
            }
            if(fixedFees > 0){
              amount = amount - fixedFees;
            }
            amount = amount - percentNetAmount;
            amount =  amount * teacherPercentage;
            return (amount - (amount * teacherView)).toFixed(2);
          }, width: 5},
        ]
    });
}
function loadSubscriptionAmount(teacherId,from,to,aws){
  teacherId  = teacherId || 0;
  grid = $('#subscriptions-grid').jsGrid({
        filtering: false,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        pagerFormat: "Pages: {prev} {next} {pageIndex} of {pageCount}",
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
        				    type: "POST",
        			        url: siteurl+`/admin/teacher/billing`,
        			        data: {'teacher_id' : teacherId , 'from_date' : from , 'to_date' : to , 'aws':aws , 'single_course' : false},
                      datatype:'json',
        			        headers: {
        			            "x-csrf-token": $("[name=_token]").val()
        			        },
        				}).done(response =>  {
                  $('#teacher-views').val(response.teacherViews);
                  $('#teacher-percentage').val(response.teacherPercentage);
                  data.resolve({data: response.data, itemsCount:[response.count]});
                  renderAmountSubscription(response);
        				});
                return data.promise();
            },
        },

        fields: [
            {name: "id", title: 'ID', type: "number", width: 1},
            {name: "subscription.user.email", title: 'User', type: "text", width: 15},
            {name: "amount", title: 'Amount', type: "number", width: 5},
            {name: "amountWithOutFees", title: 'Amount With out Fees', type: "number",
            itemTemplate: function(value,item){
              amount = item.amount;
              percentFees =item.payment_method.percent_fees;
              fixedFees =item.payment_method.fix_fees;
              netAmount = 0;
              fixedNetAmount = 0;
              percentNetAmount = 0;
              if(percentFees > 0){
                percentNetAmount =  amount * (percentFees / 100);
              }
              if(fixedFees > 0){
                amount = amount - fixedFees;
              }
              return (amount - percentNetAmount).toFixed(2);
            }, width: 5},
            {name: "profit", title: 'Profit', type: "number",
            itemTemplate: function(value,item){
              amount = item.amount;
              teacherView = $('#teacher-views').val();
              teacherPercentage = $('#teacher-percentage').val();
              percentFees =item.payment_method.percent_fees;
              fixedFees =item.payment_method.fix_fees;
              netAmount = 0;
              fixedNetAmount = 0;
              percentNetAmount = 0;
              if(percentFees > 0){
                percentNetAmount =  amount * (percentFees / 100);
              }
              if(fixedFees > 0){
                amount = amount - fixedFees;
              }
              amount = amount - percentNetAmount;
              amount =  amount * teacherPercentage;
              return (amount - (amount * teacherView)).toFixed(2);
            }, width: 5},
            {name: "subscription.subscription_model.name_en", title: 'Subscription Model', type: "text", width: 15},
        ]
    });
}


$("#export").on("click", function(){
  var data = $('#subscriptions-grid').jsGrid('option', 'data');
  JSONToCSVConvertor(data, "Teacher Billing Report (Annual/Month Subscriptions)", true);
  return ;
});

$("#export1").on("click", function(){
  var data = $('#course-grid').jsGrid('option', 'data');
  JSONToCSVConvertor(data, "Teacher Billing Report (Courses Subscriptions)", true);
  return ;
});





function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
  //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
  var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

  var CSV = '';
  //Set Report title in first row or line

  CSV += ReportTitle + '\r\n\n';

  //This condition will generate the Label/Header
  if (ShowLabel) {
    var row = "";

    //This loop will extract the label from 1st index of on array
    for (var index in arrData[0]) {

      //Now convert each value to string and comma-seprated
      row += index + ',';
    }

    row = row.slice(0, -1);

    //append Label row with line break
    CSV += row + '\r\n';
  }

  //1st loop is to extract each row
  for (var i = 0; i < arrData.length; i++) {
    var row = "";

    //2nd loop will extract each column and convert it in string comma-seprated
    for (var index in arrData[i]) {
      if (arrData[i][index] != '[object Object]')
        row += '"' + arrData[i][index] + '",';
    }

    row.slice(0, row.length - 1);

    //add a line break after each row
    CSV += row + '\r\n';
  }

  if (CSV == '') {
    alert("Invalid data");
    return;
  }

  //Generate a file name
  var fileName = "";
  //this will remove the blank-spaces from the title and replace it with an underscore
  fileName += ReportTitle.replace(/ /g, "_");

  //Initialize file format you want csv or xls
  var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

  // Now the little tricky part.
  // you can use either>> window.open(uri);
  // but this will not work in some browsers
  // or you will not get the correct file extension    

  //this trick will generate a temp <a /> tag
  var link = document.createElement("a");
  link.href = uri;

  //set the visibility hidden so it will not effect on your web-layout
  link.style = "visibility:hidden";
  link.download = fileName + ".csv";

  //this part will append the anchor tag and remove it after automatic click
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
}