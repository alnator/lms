(function ($){
  loadUsers();

})(jQuery);

function loadUsers(){
  $("#users-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: siteurl+`/admin/users`,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"       , title: 'ID'         , type: "text", width: 5},
            {name: "name"  , title: 'Username'     , type: "text", width: 5},
            {name: "email"  , title: 'Email'     , type: "text", width: 5},
            {name: "active"  , title: 'Active'     , type: "checkbox", width: 5},          
            {name: "star"  , title: 'Star'     , type: "checkbox", width: 3,
             itemTemplate: function(value, item) {
                    return $("<input>").attr("type", "checkbox")
                        .attr("checked", item.star || item.Checked)
                        .on("change", function() {
                          $.ajax({
                                type: "POST",
                                url: siteurl+`/admin/users/star/${item.id}`,
                                headers: {
                                    "x-csrf-token": $("[name=_token]").val()
                                },
                            }).done(response => {
                                if(response == 'ok'){
                                  swal("Added!", "This User added to Stars successfully.", "success");
                                  // $('#product-grid').jsGrid('render');
                                }
                                else {
                                 swal("Removed!", "This User removed from Stars successfully.", "success");
                                }
                              });
                        });
              },
            },
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  console.log(item.supplier);
                  if (item.supplier == 1){
                    var $span = $(`<strong>Supplier</strong>`)
                    return $result.add($span);
                  }
                  else if (item.teacher == 1){
                    var $span = $(`<strong>Teacher</strong>`)
                    return $result.add($span);
                  }
                  else {
                    var $edit = $('<a class="btn btn-block btn-default btn-sm">See Courses</a>');
                        $edit.attr('href',siteurl+`/admin/users/`+item.id+`/courses`);
                    return $result.add($edit);  
                  }
                  
              },
            },
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-succes btn-sm">Edit User</a>');
                      $edit.attr('href',siteurl+`/admin/user/`+item.id+`/edit`);
                  return $result.add($edit);
              },
            },
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-danger btn-sm">Delete</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      deleteUser(item.id);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function deleteUser(UserId){
    swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this User!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No"
  },
  function(){
    $.ajax({
        type: "POST",
        url: siteurl+`/admin/users/delete/${UserId}`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response > 0){
        swal("Deleted!", "User deleted successfully.", "success");
        $('#users-grid').jsGrid('render');
      }
    });
  });
}

