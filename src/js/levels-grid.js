(function ($){
  loadProduct();

})(jQuery);

function loadProduct(){
	
  $("#levels-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: siteurl+`/api/admin/levels`,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"       , title: 'ID'         , type: "text", width: 5},
            {name: "name_en"  , title: 'English Name'     , type: "text", width: 5},
            {name: "name_ar"  , title: 'Arabic Name'     , type: "text", width: 5},
            {name: "created_by.email"    , title: 'Created By'     , type: "text", width: 5},
            {name: "updated_by.email"    , title: 'updated By'     , type: "text", width: 5},
         
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-default btn-sm">Edit</a>');
                      $edit.attr('href',siteurl+`/admin/levels/`+item.id+`/edit`);
                  return $result.add($edit);
              },
            },
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-danger btn-sm">Delete</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      deleteLevel(item.id);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function deleteLevel(LevelId){
    swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this Level!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No"
  },
  function(){
    $.ajax({
        type: "POST",
        url: siteurl+`/api/admin/levels/delete/${LevelId}`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response > 0){
        swal("Deleted!", "Level deleted successfully.", "success");
        $('#levels-grid').jsGrid('render');
      }
    });
  });
}

