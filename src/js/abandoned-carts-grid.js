(function ($){
  loadCarts();

})(jQuery);

function loadCarts(){
  $("#abandoned-carts-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: `/api/admin/abandoned-carts/`,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"            , title: 'ID'         , type: "text", width: 5},
            {name: "user_id"         , title: 'User ID'     , type: "text", width: 5},
            {name: "subscription_model_id", title: 'Subscription Model ID'     , type: "text", width: 5},
            {name: "course_id"         , title: 'Course ID'     , type: "text", width: 5},
            {name: "payment_method_id"         , title: 'Paymnet Method ID'     , type: "text", width: 5},
            {name: "coupon"         , title: 'Coupon'     , type: "text", width: 5},
            
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-default btn-sm">Edit</a>');
                      $edit.attr('href',`/admin/abandoned-carts/`+item.id+`/edit`);
                  return $result.add($edit);
              },
            },
            
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-danger btn-sm">Delete</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      deleteCart(item.id);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function deleteCart(CourseId){
    swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this Cart!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No"
  },
  function(){
    $.ajax({
        type: "POST",
        url: `/api/admin/abandoned-carts/delete/${CourseId}`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response > 0){
        swal("Deleted!", "Cart deleted successfully.", "success");
        $('#abandoned-carts-grid').jsGrid('render');
      }
    });
  });
}

