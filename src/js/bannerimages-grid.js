(function ($){
  loadProduct();

})(jQuery);

function loadProduct(){
	
  $("#levels-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: siteurl+`/api/admin/bannerimagesapi`,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       

       fields: [
            {name: "id"       , title: 'ID'         , type: "text", width: 5},
           {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $image_path = $('<img width="100px" />');
                      $image_path.attr('src',siteurl+`/public`+item.image_path);
                  return $result.add($image_path);
              },
            },
            {name: "display_area"  , title: 'display area'     , type: "text", width: 5},
            {name: "display_size"  , title: 'display size'     , type: "hidden", width: 5},
           

            {name: "created_by.email"    , title: 'Created By'     , type: "text", width: 5},
            {name: "updated_by.email"    , title: 'updated By'     , type: "text", width: 5},
         
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-default btn-sm">Edit</a>');
                      $edit.attr('href',siteurl+`/admin/bannerimages/`+item.id+`/edit`);
                  return $result.add($edit);
              },
            },
            

        ]
    });
}



