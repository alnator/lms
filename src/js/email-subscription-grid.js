(function ($){
  loadEmails();
})(jQuery);

function loadEmails(){
  $("#emails-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: ``,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"           , title: 'ID'        , type: "text", width: 5},
            {name: "from"         , title: 'From'     , type: "text", width: 5},
            {name: "subject"      , title: 'Subject'  , type: "text", width: 5},
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $(`<a class="btn btn-block btn-primary btn-sm" href="`+siteurl+`/admin/emails/${item.id}/send">Send Emails</a>`);
                return $result.add($del);
              },
            },
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {

                      
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-default btn-sm">Edit</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      editEmail(item.id,item.from,item.subject);

                  });
                return $result.add($del);
              },
            },
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-danger btn-sm">Delete</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      deleteProduct(item.id);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function deleteProduct(emailId){
    swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this Email!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No"
  },
  function(){
    $.ajax({
        type: "POST",
        url: siteurl+`/admin/email/${emailId}/delete`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response > 0){
        swal("Deleted!", "Email deleted successfully.", "success");
        $('#emails-grid').jsGrid('render');
      }
    });
  });
}

function editEmail(itemid,itemfrom,itemsubject) {

  $('#email-id').val(itemid);
  $('#email-from').val(itemfrom);
  $('#email-subject').val(itemsubject);
  $('#submit-btn').html("Update");
  $('#submit-link').attr('action', siteurl+'/admin/emails/update');
  $('#edit-create-modal').modal('show');
}