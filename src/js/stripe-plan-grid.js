(function ($){
  loadStripeCustomers();

})(jQuery);

function loadStripeCustomers(){
  $("#stripe-plan-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: `/api/admin/stripe-plan`,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"                   , title: 'ID'         , type: "text", width: 5},
            {name: "subscription_model_id", title: 'Subscription ID'     , type: "text", width: 5},
            {name: "course_id"            , title: 'Course ID'     , type: "text", width: 5},
            {name: "stripe_plan_id"       , title: 'Stripe Plan ID'     , type: "text", width: 5},
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-default btn-sm">Edit</a>');
                      $edit.attr('href',`/admin/stripe-plan/`+item.id+`/edit`);
                  return $result.add($edit);
              },
            },
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-danger btn-sm">Delete</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      deleteStripe(item.id);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function deleteStripe(PlanId){
    swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this Stripe Plan!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No"
  },
  function(){
    $.ajax({
        type: "POST",
        url: `/api/admin/stripe-plan/delete/${PlanId}`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response > 0){
        swal("Deleted!", "Stripe Plan deleted successfully.", "success");
        $('#stripe-plan-grid').jsGrid('render');
      }
    });
  });
}

