(function ($){
  index = 0;
  str = window.location.href;
  /*index = str.indexOf("//") +4;
  link = '';
  for (i=index ;i < str.length ; i++ ){
    if (str[i] == '/'){
      link= str.substring(0,i) + '/api' + str.substring(i , str.length);
      break; 
    }
  }*/
  var items = str.split('admin');
  console.log(items);

  var typeobj = items[1].split('/');
  var type=typeobj[1];

  
  link = items[0]+'api/admin'+items[1];
  loadQuizes(type);
})(jQuery);

function loadQuizes(type){


  $("#quizes-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: link,
                    dataType: "json",
                    data: {filter,type:type},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"        , title: 'ID'           , type: "text", width: 5},
            {name: "quiz_statement", title: 'Statement', type: "text", width: 5},
            {name: "video_name"        , title: type  , type: "text", width: 5},
            {name: "course_id"        , title: 'Course', type: "text", width: 5},
            {name: "status"        , title: 'status', type: "text", width: 5},
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-default btn-sm">Edit</a>');
                      $edit.attr('href',siteurl+`/admin/`+type+`/`+item.video_id+`/quiz/`+item.id+`/edit`);
                  return $result.add($edit);
              },
            },

            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-danger btn-sm">Delete</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      deleteQuiz(item.id);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function deleteQuiz(QuizId){
    swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this Quiz!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No"
  },
  function(){
    $.ajax({
        type: "POST",
        url: siteurl+`/api/admin/quiz/${QuizId}/destroy`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response == 'ok'){
        swal("Deleted!", "Quiz deleted successfully.", "success");
        $('#quizes-grid').jsGrid('render');
      }
    });
  });
}

