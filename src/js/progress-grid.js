link = "";
(function ($){
  // str = window.location.href;
  // prefix = "";
  // suffix = "";

  // flag =0 ;
  // for (i=1 ; i < str.length ; i++){
  //   if (str[i] == '/' && str[i-1] == '/')
  //   {
  //     flag = 1;
  //   }
  //   else if ( str[i] == '/' && flag == 1 ) 
  //   {
  //     prefix = str.substr(0,i+1);
  //     suffix = str.substr(i+1 , str.length);
  //     break;
  //   }
    
  // }
  // link = prefix + "api/" +suffix;
  loadProduct();

})(jQuery);

function loadProduct(){
  $("#progress-grid").jsGrid({
        filtering: false,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: link,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"            , title: 'ID'         , type: "text", width: 5},
            {name: "session_number"         , title: 'Session'     , type: "text", width: 5},
            {name: "done"         , title: 'Done'     , type: "text", width: 5},
            {name: "progress"         , title: 'Progress'     , type: "text", width: 5},
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-info btn-sm">Details</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      // console.log(item.video);
                      fillTable(item.video);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function fillTable(data){
  // console.log(data[0]['done']);
  var html = '<tbody>';
  $('#dataTable tbody').remove();
  $('#display').addClass('display');
  for (var key=0, size=data.length; key<size;key++) {
    if ( data[key]['done'] == 1 || data[key]['done'] == 'YES' ){
      data[key]['done'] = 'YES';
    }
    else {
     data[key]['done'] = 'NOT YET'; 
    }
    html += '<tr class="table"><td>'
               + data[key]['id']
               + '</td><td >'
               + data[key]['name_en']
               + '</td><td >'
               + data[key]['estimated_time']
               + '</td><td style="'+(data[key]['done'] =='YES' ? 'color: green;' : '' ) +'">'
               + data[key]['done']
               + '</td></tr>';
    }
    html+= '</tbody>'
  $('#dataTable').append(html);
}

