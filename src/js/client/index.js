

	function toPayment(){

		$('.home-content .header-section-wrapper').addClass("d-none");
		$('#payment').removeClass('d-none');

	}
	function toSubscribe(){	
		
		$('.home-content .header-section-wrapper').addClass("d-none");
		$('#subscribe').removeClass('d-none');

	}
	function gotoPayment(obj){
		parent = $(obj).parent().parent().parent();
		
		id = $(parent).data('id');
		name = $(parent).data('name');
		price = $(parent).data('price');
		displayPrice = $(parent).data('display-price')
		fullAccess = $(parent).data('full-access');
		period = $(parent).data('period');
	
		$('#subscription-name').html(name);
		$('#price').html(displayPrice);
		$('#number-of-courses').html(fullAccess);
		$('#period').html(period);
		$('#payment-data').data('id' , id);
		$('#payment-data').data('price' , price);

		toPayment();
	}
	function submitPayment(){
			model_id =$('#payment-data').data('id');
			$('#model-id-form').val(model_id);
			$('#pay').submit();
		}
	function submitPayfort(){
		model_id =$('#payment-data').data('id');
		$('#model_id_form_fort').val(model_id);
		$('#sts').submit();
	}