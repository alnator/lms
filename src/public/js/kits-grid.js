(function ($){
  loadKits();
})(jQuery);

function loadKits(){
  $("#kits-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: `/admin/kits`,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"        , title: 'ID'         , type: "text", width: 5},
            {name: "name_en"   , title: 'Name (EN)'    , type: "text", width: 5},
            {name: "name_ar"   , title: 'Name (AR)'    , type: "text", width: 5},
            {name: "selling_price"   , title: 'Selling Price' , type: "text", width: 5},
            {name: "min_price"   , title: 'Buying Price' , type: "text", width: 5},
            {name: "active"   , title: 'Active' , type: "checkbox", width: 5},

            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-default btn-sm">Kit Items</a>');
                      $edit.attr('href',`/admin/kits/${item.id}/items`);
                  return $result.add($edit);
              },
            },
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-default btn-sm">Edit</a>');
                      $edit.attr('href',`/admin/kits/${item.id}/edit`);
                  return $result.add($edit);
              },
            },

            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-danger btn-sm">Delete</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      deleteCoupon(item.id);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function deleteCoupon(CopuonId){
    swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this Kit!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No"
  },
  function(){
    $.ajax({
        type: "POST",
        url: `/admin/kits/${CopuonId}/delete`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response > 0){
        swal("Deleted!", "Kit deleted successfully.", "success");
        $('#kits-grid').jsGrid('render');
      }
    });
  });
}

