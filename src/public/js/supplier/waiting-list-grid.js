(function ($){
  link = window.location.href;
  loadKitItems(link);
})(jQuery);

function loadKitItems(link){
  $("#waiting-list-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: link,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"        , title: 'ID'         , type: "text", width: 5},
            {name: "order_id"   , title: 'Order ID'    , type: "text", width: 5},
            {name: "item.id"   , title: 'Item ID'    , type: "text", width: 5},
            {name: "item.name_en"   , title: 'Item Name'    , type: "text", width: 5},
            {name: "price"   , title: 'Price'    , type: "text", width: 5},
            {name: "order.user.email"   , title: 'User'    , type: "text", width: 5},
            
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-success btn-sm">Pack</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      packItem(item.id);
                  });
                return $result.add($del);
              },
            },
        ]
    });
}


function packItem(itemId){
    $.ajax({
        type: "POST",
        url: `/supplier/waiting-items/${itemId}/confirm`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response > 0){
        swal("Packed !", "Item packed successfully.", "success");
        $('#waiting-list-grid').jsGrid('render');
      }
    });
}


