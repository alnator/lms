link = "";
(function ($){
  loadProduct();

})(jQuery);

function loadProduct(){
  $("#user-courses-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: ``,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"            , title: 'ID'         , type: "text", width: 5},
            {name: "lang_id"         , title: 'Language'     , type: "text", width: 5},
            {name: "title_ar"         , title: 'Arabic Title'     , type: "text", width: 5},
            {name: "title_en"         , title: 'English Title'     , type: "text", width: 5},
            {
              type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
              itemTemplate: function (value, item) {
                var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                var $edit = $('<a class="btn btn-block btn-default btn-sm">See Progress</a>');
                    $edit.attr('href',`/admin/users/${userId}/courses/${item.id}`);
                return $result.add($edit);
              },
            },
        ]
    });
}
