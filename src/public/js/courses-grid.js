(function ($){
  loadProduct();

})(jQuery);

function loadProduct(){
  $("#courses-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: `/api/admin/courses`,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"            , title: 'ID'         , type: "text", width: 5},
            {name: "title_ar"         , title: 'Arabic Title'     , type: "text", width: 5},
            {name: "title_en"         , title: 'English Title'     , type: "text", width: 5},
            {name: "created_by.email"         , title: 'Created By'     , type: "text", width: 5},
            {name: "updated_by.email"         , title: 'updated By'     , type: "text", width: 5},
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $users = $('<a class="btn btn-block btn-success btn-sm">See Details</a>');
                      $users.attr('href',`/admin/courses/`+item.id);
                  return $result.add($users);
              },
            },   
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $users = $('<a class="btn btn-block btn-info btn-sm">See Users</a>');
                      $users.attr('href',`/admin/courses/`+item.id+`/users`);
                  return $result.add($users);
              },
            },
              
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-default btn-sm">Edit</a>');
                      $edit.attr('href',`/admin/courses/`+item.id+`/edit`);
                  return $result.add($edit);
              },
            },
            
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-danger btn-sm">Delete</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      deleteProduct(item.id);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function deleteProduct(CourseId){
    swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this Course!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No"
  },
  function(){
    $.ajax({
        type: "POST",
        url: `/api/admin/courses/delete/${CourseId}`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response > 0){
        swal("Deleted!", "Course deleted successfully.", "success");
        $('#courses-grid').jsGrid('render');
      }
    });
  });
}

