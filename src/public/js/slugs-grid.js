(function ($){
  loadCategory();

})(jQuery);

function loadCategory(){
  $("#slugs-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: `/admin/slugs`,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"      , title: 'ID'       , type: "text", width: 5},
            {name: "title_en", title: 'Title EN' , type: "text", width: 5},
            {name: "title_ar", title: 'Title AR' , type: "text", width: 5},
            {name: "href"    , title: 'URI'      , type: "text", width: 5},
            {name: "slug"    , title: 'SLUG'     , type: "text", width: 5},
            {name: "active"   , title: 'Active'     , type: "checkbox", width: 5},

            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-default btn-sm">Edit</a>');
                      $edit.attr('href',`/admin/slugs/`+item.id+`/edit`);
                  return $result.add($edit);
              },
            },
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-danger btn-sm">Delete</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      deleteCategory(item.id);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function deleteCategory(SlugId){
    swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this Slug!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No"
  },
  function(){
    $.ajax({
        type: "POST",
        url: `/admin/slugs/delete/${SlugId}`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response > 0){
        swal("Deleted!", "Slug deleted successfully.", "success");
        $('#slugs-grid').jsGrid('render');
      }
    });
  });
}

