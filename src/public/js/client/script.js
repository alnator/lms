/*jslint browser: true*/
/*global $, alert, console*/

$(function () {

	//align course name vertically
	$(".courses .course .name").each(function () {
//		console.log($(this).closest(".overlay").height());
		$(this).css("top", ($(this).closest(".overlay").innerHeight() - $(this).height()) / 2);
	});

	//align home content vertically
	$(".home .home-content").css("padding-top", ($("header.all-nav").height() + $(".home .home-content").height() * 0.2));

	//align navbar-toggler right
	if ($(window).width() > 992 || $(this).scrollTop() <= 200) {
		$(".header-nav .navbar-toggler").css("left", ($(".header-nav").width() - $(".header-nav .navbar-toggler").outerWidth()));
	} else {
		if ($("body").hasClass("home-page")) {
			$(".header-nav .navbar-toggler").removeAttr("style");
		} else {
			$(".header-nav .navbar-toggler").css("left", ($(".header-nav").width() - $(".header-nav .navbar-toggler").outerWidth()));
		}
	}

	$(window).resize(function () {
		//align course name vertically
		$(".courses .course .name").each(function () {
			$(this).css("top", ($(this).closest(".overlay").innerHeight() - $(this).height()) / 2);
			$(".home .home-content").css("padding-top", ($("header.all-nav").height() + $(".home .home-content").height() * 0.2));
		});

		//align home content vertically
		$(".home .home-content").css("padding-top", ($("header.all-nav").height() + $(".home .home-content").height() * 0.2));

		//align navbar-toggler right
		if ($(window).width() > 992 || $(this).scrollTop() <= 200) {
			$(".header-nav .navbar-toggler").css("left", ($(".header-nav").width() - $(".header-nav .navbar-toggler").outerWidth()));
		} else {
			if ($("body").hasClass("home-page")) {
				$(".header-nav .navbar-toggler").removeAttr("style");
			} else {
				$(".header-nav .navbar-toggler").css("left", ($(".header-nav").width() - $(".header-nav .navbar-toggler").outerWidth()));
			}
		}
	});

	//fixed top navbar
	$(window).scroll(function () {
		if ($("body").hasClass("home-page")) {
			if ($(this).scrollTop() > 200) {
				$(".header-nav .navbar-toggler").removeAttr("style");
				$("header.all-nav .header-nav").addClass("fixed-nav");
				$("header.all-nav .header-nav .navbar-toggler").addClass("fixed-toggler");
				$("header.all-nav .header-nav .navbar-brand").removeClass("d-none");
				$("header.all-nav .header-nav .navbar-nav").removeClass("ml-auto").addClass("m-auto");
			} else {
				$("header.all-nav .header-nav").removeClass("fixed-nav");
				$("header.all-nav .header-nav .navbar-toggler").removeClass("fixed-toggler");
				$("header.all-nav .header-nav .navbar-brand").addClass("d-none");
				$("header.all-nav .header-nav .navbar-nav").removeClass("m-auto").addClass("ml-auto");
				$(".header-nav .navbar-toggler").css("left", ($(".header-nav").width() - $(".header-nav .navbar-toggler").outerWidth()));
			}
		}
	});

//$("#quiz-modal").modal("show");
	
});
