(function ($){
  loadProduct();

})(jQuery);

function loadProduct(){
  $("#session-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: '/api/admin/sessions',
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"            , title: 'ID'         , type: "text", width: 5},
            {name: "session_number"         , title: 'Session Name (EN)'     , type: "text", width: 5},
            {name: "session_number_ar"         , title: 'Session Name (AR)'     , type: "text", width: 5},
            {name: "order"         , title: 'Order'     , type: "text", width: 5},
            {name: "course.title_en"         , title: 'Course ID'     , type: "text", width: 5},
            {name: "created_by.email"         , title: 'Created By'     , type: "text", width: 5},
            {name: "updated_by.email"         , title: 'updated By'     , type: "text", width: 5},
         
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-default btn-sm">Edit</a>');
                      $edit.attr('href',`/admin/sessions/`+item.id+`/edit`);
                  return $result.add($edit);
              },
            },
            
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-danger btn-sm">Delete</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      deleteSession(item.id);
                  });
                return $result.add($del);
              },
            },
   
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-info btn-sm">See Lessons</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      fillTable(item['video']);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function deleteSession(SessionId){
    swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this Session!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No"
  },
  function(){
    $.ajax({
        type: "POST",
        url: `/api/admin/sessions/delete/${SessionId}`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response > 0){
        swal("Deleted!", "Session deleted successfully.", "success");
        $('#session-grid').jsGrid('render');
      }
    });
  });
}

function fillTable(data){
  // console.log(data);
  var html = '<tbody>';
  $('#dataTable tbody').remove();
  $('#display').addClass('display');
  for (var key=0, size=data.length; key<size;key++) {
    if ( data[key]['done'] == 1 || data[key]['done'] == 'YES' ){
      data[key]['done'] = 'YES';
    }
    else {
     data[key]['done'] = 'NOT YET'; 
    }
    html += '<tr class="table"><td>'
               + data[key]['id'] 
               + '</td><td >'
               + data[key]['name_en']
               + '</td><td >'
               + data[key]['estimated_time'] + ' Min'
               + '</td><td >'
               + data[key]['order'] 
               + '</td></tr>';
    }
    html+= '</tbody>'
  $('#dataTable').append(html);
}

