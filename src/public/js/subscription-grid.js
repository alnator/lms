(function ($){
  loadSubscription();

})(jQuery);

function loadSubscription(){
  $("#subscription-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: `/admin/subscription`,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"       , title: 'ID'         , type: "text", width: 5},
            {name: "name_en"  , title: 'English Name'     , type: "text", width: 5},
            {name: "name_ar"  , title: 'Arabic Name'     , type: "text", width: 5},
            {name: "display_price"  , title: 'Display Price'     , type: "text", width: 5},
            {name: "price"  , title: 'Price'     , type: "text", width: 5},
            {name: "period_in_days"  , title: 'Period (Days)'     , type: "text", width: 5},
            {name: "full_access"  , title: 'Full Access'     , type: "text", width: 5},
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-default btn-sm">Edit</a>');
                      $edit.attr('href',`/admin/subscription/`+item.id+`/edit`);
                  return $result.add($edit);
              },
            },
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-danger btn-sm">Delete</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      deleteSubscription(item.id);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function deleteSubscription(SubscriptionId){
    swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this Subscription!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No"
  },
  function(){
    $.ajax({
        type: "POST",
        url: `/admin/subscription/delete/${SubscriptionId}`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response > 0){
        swal("Deleted!", "Subscription deleted successfully.", "success");
        $('#subscription-grid').jsGrid('render');
      }
    });
  });
}

