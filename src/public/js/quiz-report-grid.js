(function ($){
  loadQuizReports();
})(jQuery);

function loadQuizReports(){
  $("#quizes-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: '/api/admin/quizes/reports',
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "user_id"  , title: 'User'     , type: "text", width: 5},
            {name: "user.group_user.0.group.0.name"  , title: 'Group'   , type: "text", width: 5},
            {name: "quiz_id"  , title: 'Statement', type: "text", width: 5},
            {name: "video_id" , title: 'Video'    , type: "text", width: 5},
            {name: "course_id", title: 'Course'   , type: "text", width: 5},
            {name: "choice_id", title: 'Choice'   , type: "text", width: 5},
            {name: "correct"  , title: 'Correct'   , type: "text", width: 5},
           
        ]
    });
}


