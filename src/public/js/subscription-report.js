$(document).ready(function() {
    $('.js-example-basic-multiple').select2();

    model = $('#subscription-model-filter-all').val();
    year = $('#overall-year').val();
    $.ajax({
        url : '/admin/subscription/report' ,
        type : "POST",
        data: {'model' : model , 'year' : year},
        headers: {
                    "x-csrf-token": $("[name=_token]").val()
                },
    }).done(response => {
        renderChart(response[0] , "chart" , "MM-YYYY" , "month");
        $('#all-total').text(response[1]);
        LoadAnnualSubscriptionGrid(model);
        
    });
});
$('#month-btn').click(function (){
    filterMonth(LoadMonthSubscriptionGrid);
});
$('#income-btn').click(function(){
    incomeChart();
    LoadPaymentGrid();
});

function renderChart(data , divId , dateFormat , categoryField , parseDate = true){
    datastr =[];
    data = JSON.parse(data);
    for (var x in data ){
        datastr.push(data[x]);
    } 
    chart = AmCharts.makeChart(divId , {
        "type": "serial",
        "theme": "light",
        "marginTop":0,
        "marginRight": 80,
        "dataProvider": datastr,
        "valueAxes": [{
            "axisAlpha": 0,
            "position": "left"
        }],
        "graphs": [{
            "id":"g1",
            "balloonText": "New Subscriptions<br><b><span style='font-size:14px;'>[[value]]</span></b>",
            "bullet": "round",
            "bulletSize": 8,
            "lineColor": "#878787",
            "lineThickness": 2,
            "negativeLineColor": "#637bb6",
            "type": "smoothedLine",
            "valueField": "value"
        } , {
            "id":"g2",
            "balloonText": "Active Accounts<br><b><span style='font-size:14px;'>[[value]]</span></b>",
            "bullet": "round",
            "bulletSize": 8,
            "lineColor": "#d1655d",
            "lineThickness": 2,
            "negativeLineColor": "#637bb6",
            "type": "smoothedLine",
            "valueField": "value2"
        }    
        ,{
            "id":"g3",
            "balloonText": "Registered Users<br><b><span style='font-size:14px;'>[[value]]</span></b>",
            "bullet": "round",
            "bulletSize": 8,
            "lineColor": "#50429c",
            "lineThickness": 2,
            "negativeLineColor": "#637bb6",
            "type": "smoothedLine",
            "valueField": "value3"
        },
        {
            "id":"g4",
            "balloonText": "Income<br><b><span style='font-size:14px;'>[[value]]</span></b>",
            "bullet": "round",
            "bulletSize": 8,
            "lineColor": "#50429c",
            "lineThickness": 2,
            "negativeLineColor": "#637bb6",
            "type": "smoothedLine",
            "valueField": "month_income"
        }

         ],
        
        "chartScrollbar":
                {
                    "graph":"g1",
                    "gridAlpha":0,
                    "color":"#888888",
                    "scrollbarHeight":55,
                    "backgroundAlpha":0,
                    "selectedBackgroundAlpha":0.1,
                    "selectedBackgroundColor":"#888888",
                    "graphFillAlpha":0,
                    "autoGridCount":true,
                    "selectedGraphFillAlpha":0,
                    "graphLineAlpha":1,
                    "graphLineColor":"#c2c2c2",
                    "selectedGraphLineColor":"#888888",
                    "selectedGraphLineAlpha":1
                },
        "chartCursor": {
            "categoryBalloonDateFormat": dateFormat,
            "cursorAlpha": 0,
            "valueLineEnabled":true,
            "valueLineBalloonEnabled":true,
            "valueLineAlpha":0.5,
            "fullWidth":true
        },
        "dataDateFormat": dateFormat,
        "categoryField": categoryField,
        "categoryAxis": {
            "minPeriod": "MM",
            "parseDates": parseDate,
            "minorGridAlpha": 0.1,
            "minorGridEnabled": true
        },
        "export": {
            "enabled": true
        }
    });
    chart.addListener("rendered", zoomChart);
    if(chart.zoomChart){
        chart.zoomChart();
    }

    function zoomChart(){
        chart.zoomToIndexes(Math.round(chart.dataProvider.length * 0.4), Math.round(chart.dataProvider.length * 0.55));
    }

}

function filterMonth(callback){
    month= $('#month-filter').val();
    year = $('#year').val();
    model= $('#subscription-model-filter').val();
    $.ajax({
        url : '/admin/subscription/report/month-filter' ,
        type : "POST",
        data: {'month' : month , 'year' : year  , 'model' : model},
        headers: {
                    "x-csrf-token": $("[name=_token]").val()
                },
    }).done(response => {
        renderChart(response[0] , "monthChart" , "D" , "day", false);
        $('#monthChart').css('height' ,'25em');
        $('#total').text(response[1] +" Subscriptions");
        $('#month-total').removeClass('d-none');
        callback(model , month , year);
    });
}

function incomeChart(){
    month = $('#income-month-filter').val();
    year = $('#income-year').val();
    subscription = $('#income-subscription-model-filter').val();
    $.ajax({
        url : '/admin/income/report' ,
        type : "POST",
        data: {'month' : month , 'year' :year , 'subscription' : subscription},
        headers: {
                    "x-csrf-token": $("[name=_token]").val()
                },
    }).done(response => {
        renderChart(response[0] , "months-income" , "MM-YYYY" , "month");
        $('#income-total').removeClass('d-none');
        $('#months-income').css('height' ,'25em');
        $('#income-total-val').text(response[1]);
    });
}

function LoadMonthSubscriptionGrid(Su ,Mo , Ye){
    $("#subscription-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $('#subscription-filter').val(JSON.stringify(filter));
                $.ajax({
                    type: "POST",
                    url: `/admin/suscription/${Su}/report/${Mo}/${Ye}`,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"       , title: 'ID'         , type: "text", width: 5},
            {name: "user.email"  , title: 'User Email'     , type: "text", width: 5},
            {name: "payment.amount"  , title: 'Amount'     , type: "text", width: 5},
            {name: "course.title_en"  , title: 'Course'     , type: "text", width: 5},
            {name: "subscription_model.name_en"  , title: 'Subscription Model Name'     , type: "text", width: 5 , filtering: false},
        ]
    });
}

function LoadAnnualSubscriptionGrid(Su){
    $("#annual-subscription-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $('#annual-subscription-filter').val(JSON.stringify(filter));
                $.ajax({
                    type: "POST",
                    url: `/admin/suscription/${Su}/report/0/0`,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"       , title: 'ID'         , type: "text", width: 5},
            {name: "user.email"  , title: 'User Email'     , type: "text", width: 5},
            {name: "payment.amount"  , title: 'Amount'     , type: "text", width: 5},
            {name: "course.title_en"  , title: 'Course'     , type: "text", width: 5},
            {name: "subscription_model.name_en"  , title: 'Subscription Model Name'     , type: "text", width: 5 , filtering: false},
        ]
    });
}



function LoadPaymentGrid(){
    month = $('#income-month-filter').val();
    year = $('#income-year').val();
    subscription = $('#income-subscription-model-filter').val();
    headerData = [];
    headerData['month']= month;
    headerData['year'] =  year;
    headerData['subscription']= subscription;

    $("#income-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                filter['month'] = month;
                filter['year'] = year;
                filter['subscription'] = subscription;
                $('#income-filter').val(JSON.stringify(filter));
                $.ajax({
                    type: "POST",
                    url: `/admin/payment/report`,
                    dataType: "json",
                    data: {"filter" : filter },
                    headers: {
                        "x-csrf-token": $("[name=_token]").val(), 
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id", title: 'ID' , type: "text", width: 5},
            {name: "user.email", title: 'User Email' , type: "text", width: 5},
            {name: "amount", title: 'Amount' , type: "text", width: 5},
            {name: "subscription_model.name_en" , title: 'Subscription Model Name', type: "text", width: 5 , filtering: false}
        ]
    });
}


$('#annual-btn').click(function(){
    model = $('#subscription-model-filter-all').val();
    year = $('#overall-year').val();
    $.ajax({
        url : '/admin/subscription/report' ,
        type : "POST",
        data: {'model' : model , 'year' :year},
        headers: {
                    "x-csrf-token": $("[name=_token]").val()
                },
    }).done(response => {
        renderChart(response[0] , "chart" , "MM-YYYY" , "month");
        $('#all-total').text(response[1]);
        LoadAnnualSubscriptionGrid(model);
    });
});


            // PDF
function getPDF(){
    model = $('#subscription-model-filter-all').val();
    year = $('#overall-year').val();
    link = $('#subscription-pdf-link').val();
    filter = $('#annual-subscription-filter').val();
    link += `?model=${model}&year=${year}&filter=${filter}`;
    window.location.href = link;
}

function getMonthPDF(){
    model = $('#subscription-model-filter').val();
    year = $('#year').val();
    month = $('#month-filter').val();
    filter = $('#subscription-filter').val();
    link = $('#subscription-pdf-link').val();
    link += `?model=${model}&year=${year}&month=${month}&filter=${filter}`;
    window.location.href = link;
}
function getIncomePDF(){
    month = $('#income-month-filter').val();
    year = $('#income-year').val();
    subscription = $('#income-subscription-model-filter').val();
    filter = $('#income-filter').val();
    link = $('#income-pdf-link').val();
    link += `?model=${subscription}&year=${year}&month=${month}&filter=${filter}`;
    window.location.href = link;
}

                    //EXCEL XLSX , CSV
function getExcel(type){
    model = $('#subscription-model-filter-all').val();
    year = $('#overall-year').val();
    link = $('#subscription-excel-link').val();
    filter = $('#annual-subscription-filter').val();
    link += `?model=${model}&year=${year}&filter=${filter}&type=${type}`;
    window.location.href = link;   
}
function getMonthExcel(type){
    model = $('#subscription-model-filter').val();
    year = $('#year').val();
    month = $('#month-filter').val();
    filter = $('#subscription-filter').val();
    link = $('#subscription-excel-link').val();
    link += `?model=${model}&year=${year}&month=${month}&filter=${filter}&type=${type}`;
    window.location.href = link;
}
function getIncomeExcel(type){
    month = $('#income-month-filter').val();
    year = $('#income-year').val();
    subscription = $('#income-subscription-model-filter').val();
    filter = $('#income-filter').val();
    link = $('#income-excel-link').val();
    link += `?model=${subscription}&year=${year}&month=${month}&filter=${filter}&type=${type}`;
    window.location.href = link;
}
