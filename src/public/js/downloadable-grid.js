(function ($){
  loadFiles();
})(jQuery);

function loadFiles(){
  $("#downloadable-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: `/api/admin/files`,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields:[
            {name: "id"            , title: 'ID'         , type: "text", width: 5},
            {name: "name_ar"       , title: 'Arabic Name'     , type: "text", width: 5},
            {name: "name_en"       , title: 'English Name'     , type: "text", width: 5},
            {name: "course_id"     , title: 'Course'     , type: "text", width: 5},
            {name: "video_id"      , title: 'Video'     , type: "text", width: 5},
            {name: "file_path_en"  , title: 'Link (EN)'     , type: "text", width: 5},
            {name: "file_path_ar"  , title: 'Link (AR)'     , type: "text", width: 5},
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-default btn-sm">Edit</a>');
                      $edit.attr('href',`/admin/files/`+item.id+`/edit`);
                  return $result.add($edit);
              },
            },
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $del = $('<a class="btn btn-block btn-danger btn-sm">Delete</a>');
                  $del.on('click', function (e) {
                      e.stopPropagation();
                      e.preventDefault();
                      deleteFile(item.id);
                  });
                return $result.add($del);
              },
            },

        ]
    });
}

function deleteFile(FileId){
    swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this File!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    cancelButtonText: "No"
  },
  function(){
    $.ajax({
        type: "POST",
        url: `/api/admin/files/delete/${FileId}`,
        headers: {
            "x-csrf-token": $("[name=_token]").val()
        },
    }).done(response => {
      if(response > 0){
        swal("Deleted!", "File deleted successfully.", "success");
        $('#downloadable-grid').jsGrid('render');
      }
    });
  });
}

