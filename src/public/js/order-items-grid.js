link = "";
(function ($){
  str = window.location.href;
  link = str.replace('/admin' , '/admin');
  loadItems();
})(jQuery);

function loadItems(){
  $("#order-items-grid").jsGrid({
        filtering: false,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: link,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"            , title: 'ID'         , type: "text", width: 5},
            // {name: "user.email"            , title: 'ID'         , type: "text", width: 5},
            {name: "supplier.email"            , title: 'Supplier Email'         , type: "text", width: 5},
            {name: "price"            , title: 'Price'         , type: "text", width: 5},
            {name: "order_id"            , title: 'Order ID'         , type: "text", width: 5},
            {name: "item.name_en"            , title: 'Item ID'         , type: "text", width: 5},

        ]
    });
}

