(function ($){
  loadSuppliers();
})(jQuery);

function loadSuppliers(){
  $("#suppliers-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: `/admin/suppliers`,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"       , title: 'ID'         , type: "text", width: 5},
            {name: "email"    , title: 'Email' , type: "text" , width: 5},
            {name: "phone"    , title: 'Phone'     , type: "text", width: 5},
            {
                type: "control", width: 10, editButton: false, modeSwitchButton: false, deleteButton: false,
                itemTemplate: function (value, item) {
                  var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                  var $edit = $('<a class="btn btn-block btn-default btn-sm">View items</a>');
                      $edit.attr('href',`/admin/suppliers/${item.id}/items`);
                  return $result.add($edit);
              },
            }
        ]
    });
}
