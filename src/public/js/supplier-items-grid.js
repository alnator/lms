(function ($){
  link = window.location.href;
  url = link.replace('/admin' , '/admin');
  loadSuppliers(url);

})(jQuery);

function loadSuppliers(url){
  $("#supplier-items-grid").jsGrid({
        filtering: true,
        width: '100%',
        height: 'auto',
        autoload: true,
        paging: true,
        pageSize: 10,
        pageIndex: 1,
        pageLoading: true,
        editing: false,
        inserting: false,
        subGrid: false,
        controller: {
            updateItem: $.noop,
            loadData: function (filter) {
                var data = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {filter},
                    headers: {
                        "x-csrf-token": $("[name=_token]").val()
                    },
                }).done((response) => data.resolve({ data: response.data, itemsCount: [response.total] }));
                return data.promise();
            },
        },
       fields: [
            {name: "id"       , title: 'ID'         , type: "text", width: 5},
            {name: "name_en"    , title: 'Name (EN)' , type: "text" , width: 5},
            {name: "name_ar"    , title: 'Name (AR)'     , type: "text", width: 5},
            {name: "supplier.0.price"    , title: 'Price'     , type: "text", width: 5},
            {name: "supplier.0.qty"    , title: 'Qty'     , type: "text", width: 5},
        ]
    });
}
